#pragma once

#include <engine_core.h>
#include <concurrency/thread_pool.h>

namespace framework
{
	class ForwardRenderer
	{
	public:
		ForwardRenderer();
		~ForwardRenderer();

		void initialize(::framework::gpu::api_type api_type);

		void destroy();

		void update(double deltaT);

		class ::framework::gpu::device * get_device();

	private:
		class ::framework::platform::window * window;
		::framework::gpu::instance * instance;
		::std::uint32_t					physical_device_count;
		::std::uint32_t					m_physicalDeviceIndex;
		::framework::gpu::physical_device ** physical_devices;
		::std::vector<::framework::gpu::queue_family_properties>	queue_family_properties;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		class ::framework::gpu::device * device;
		class ::framework::gpu::surface * surface;
		::std::uint32_t					width, height;
		::std::uint32_t					swap_chain_image;
		class ::framework::gpu::swap_chain * swap_chain;
		::std::vector<class ::framework::gpu::image *>			swap_chain_images;
		class ::framework::gpu::queue * queue;

		struct thread_data
		{
			struct CommandBufferData
			{
				bool			is_command_buffer_open;
				::std::uint32_t		drawCallCount;
			};

			class ::framework::gpu::command_pool * command_pool;
			::std::vector<class ::framework::gpu::command_buffer *>		commandBuffers;
			::std::vector<CommandBufferData>	commandBufferData;
		};
		void * mappedData;
		class ::framework::gpu::buffer * unsortedData, * unsortedValues, * unsortedDataUpload, * unsortedDataReadback;
		class ::framework::gpu::device_memory * unsortedDataMemory, * unsortedValuesMemory, * unsortedDataUploadMemory, * unsortedDataReadbackMemory;
		struct UboData
		{
			::glm::mat4 projectionMatrix;
			::glm::mat4 modelMatrix;
			::glm::mat4 viewMatrix;
			::glm::vec4 camPos;
			::glm::vec4 lightPos;
		};
		::std::uint32_t					thread_count;
		::std::vector<thread_data>		thread_data[3];
		concurrency::job_system thread_pool[3];
		::framework::gpu::command_pool * primary_command_pool;
		::framework::gpu::command_pool * m_pSecondaryCommandPool[3];
		::framework::gpu::command_buffer * m_pPrimaryCommandBuffers[3];
		//CommandBuffer*				m_pSecondaryCommandBuffers[3][NUM_OBJECTS];
		::framework::gpu::image_view ** image_views;
		::framework::gpu::frame_buffer ** frame_buffers;
		::framework::gpu::image * depth_stencil_image;
		::framework::gpu::device_memory * depth_stencil_device_memory;
		::framework::gpu::image_view * depth_stencil_image_view;
		::framework::gpu::render_pass * render_pass;
		::framework::gpu::buffer * ssbo;
		::framework::gpu::device_memory * m_pSsboDeviceMemory;
		::framework::gpu::buffer_view * m_pSSBOView;
		::framework::gpu::shader_module * pShaderModuleVertex;
		::framework::gpu::shader_module * pShaderModuleGeometry;
		::framework::gpu::shader_module * pShaderModuleFragment;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline * m_pGraphicsPipeline;
		::framework::gpu::fence * d3d12_fence[3];
		::framework::gpu::semaphore * render_complete_semaphores[3];
		::framework::gpu::semaphore * present_complete_semaphore;
		::framework::gpu::semaphore * m_pPathTracingCompleteSemaphore;
		::framework::gpu::descriptor_set_layout * m_pDescriptorSetLayout;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_set * m_pDescriptorSet;
		::framework::gpu::device_memory * m_pUboDeviceMemory;
		::framework::gpu::buffer * m_ubo;
		::framework::gpu::image * image;
		::framework::gpu::device_memory * device_memory;
		::framework::gpu::image_view * image_view;
		::framework::gpu::sampler * sampler;
		// For Path tracing
		//CPathTracingATRBVH* pTracer;
		::framework::gpu::shader_module * m_pFullScreenQuadVertexShaderModule;
		::framework::gpu::shader_module * m_pFullScreenQuadFragmentShaderModule;
		::framework::gpu::pipeline * m_pFullScreenQuadGraphicsPipeline;
		::framework::gpu::render_pass * m_pFullScreenQuadRenderPass;
		::framework::gpu::descriptor_set_layout * m_pFullScreenQuadDescriptorSetLayout;
		::framework::gpu::pipeline_layout * m_pFullScreenQuadPipelineLayout;
		::framework::gpu::descriptor_set * m_pFullScreenQuadDescriptorSet;
		//CEntity						m_mainCamera;
		::glm::vec4					m_lightPos;
	};
}