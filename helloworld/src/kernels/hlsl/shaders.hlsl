tbuffer ssboColor : register(t0)
{
	float4 ssboColor[2];
};

//Buffer<float4> ssboColor : register(t0);
//RWBuffer<float4> ssboColor : register(u1);
Texture2D g_texture : register(t1);
SamplerState g_sampler : register(s1);

cbuffer UBO : register(b0)
{
	float4x4 projectionMatrix;
	float4x4 modelMatrix;
	float4x4 viewMatrix;
	float4 camPos;
	float4 lightPos;
};

cbuffer Constants : register(b1)
{
	float4 color;
}

struct VS_INPUT
{
	float3 vInPositionOS		: LOCATION0;
	float3 vInNormalOS			: LOCATION1;
	float3 vInBinormalOS		: LOCATION2;
	float3 vInTangentOS			: LOCATION3;
	float2 vInTexCoord			: LOCATION4;
};

struct PSInput
{
	float4 position				: SV_POSITION;
	float3 vWorldPos			: POSITION;
	float3 vNormal				: NORMAL;
	float3 vBinormal			: BINORMAL;
	float3 vTangent				: TANGENT;
	float2 texCoord				: TEXCOORD0;
};

PSInput VSMain(VS_INPUT i)
{
	PSInput result;

	float4 transformPosition = mul(modelMatrix, float4(i.vInPositionOS, 1.0f));
	transformPosition = mul(viewMatrix, transformPosition);
	transformPosition = mul(projectionMatrix, transformPosition);
	result.position = transformPosition;
	result.vWorldPos = mul(modelMatrix, float4(i.vInPositionOS, 1.0f));
	result.vNormal = mul(modelMatrix, i.vInNormalOS);
	result.vBinormal = i.vInBinormalOS;
	result.vTangent = i.vInTangentOS;
	result.texCoord = i.vInTexCoord;

	//float4 curColor = ssboColor[0];
	//curColor.b += 0.0001f;

	//ssboColor[0] = curColor;

	return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
	//const float4 lightPos = float4(10.0f, 5.0f, 2.0f, 1.0f);
	const float3 lightDir = normalize(lightPos.xyz - input.vWorldPos);
	const float dotNL = dot(input.vNormal, lightDir);
//	const float3 reflect = reflect(lightDir, input.vNormal);
//	const float3 dotNV = reflect(lightDir, input.vNormal);
//	const float spec = pow(dot(reflect), 16.0f);

	return g_texture.Sample(g_sampler, input.texCoord) * dotNL + 0;
	//float4 meshColor = ssboColor[0] * color;
	//return float4(meshColor.rgb * (max(0.0, dot(float3(0.556433320f, 0.489277601f, 0.671557486f), normalize(input.vNormal)))), 1.0f);
	//return float4(input.vNormal, 1.0f);
	//return ssboColor[0] * color;
}