struct push_constants_t
{
	uint32_t count;
	uint32_t width;
	uint32_t height;
	uint32_t pad0;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);
[[vk::binding(0, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);
[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> features_buffer : register(u1, space0);
[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);
//[[vk::binding(3, 0)]] RWStructuredBuffer<uint8_t> pdf_plot_buffer : register(u3, space0); // won't compile on hlsl as it doesn't support uint8_t
[[vk::binding(4, 0)]] RWStructuredBuffer<float> target_buffer : register(u4, space0);
[[vk::binding(5, 0)]] Texture2D<float4> target_image : register(t0, space0);
[[vk::binding(6, 0)]] Texture2D<float4> samples_image : register(t1, space0);
[[vk::binding(7, 0)]] RWStructuredBuffer<ELEMENT_TYPE> spatial_features_buffer : register(u1, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= push_constants.count)
	{
		return;
	}

	const uint x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;

	const float2 test_sample = float2(global_invocation_id % push_constants.width, global_invocation_id / push_constants.width) / float2(push_constants.width, push_constants.height);

	x_buffer[x_a_index] = ELEMENT_TYPE(test_sample.x);
	x_buffer[x_b_index] = ELEMENT_TYPE(test_sample.y);

	const uint feature_count_stride = 4u;
	const uint feature_offset = global_invocation_id * feature_count_stride;
	spatial_features_buffer[feature_offset + 0] = ELEMENT_TYPE(1.0);
	spatial_features_buffer[feature_offset + 1] = ELEMENT_TYPE(1.0);
	spatial_features_buffer[feature_offset + 2] = ELEMENT_TYPE(1.0);

	features_buffer[global_invocation_id + 44 * push_constants.count] = ELEMENT_TYPE(test_sample.x);
}
