cbuffer UBO : register(b0)
{
	float4x4 projectionMatrix;
	float4x4 modelMatrix;
	float4x4 viewMatrix;
};

struct VS_INPUT
{
	float3 vInPositionOS		: LOCATION0;
	float3 vInNormalOS			: LOCATION1;
	float3 vInBinormalOS		: LOCATION2;
	float3 vInTangentOS			: LOCATION3;
	float2 vInTexCoord			: LOCATION4;
};

struct VS_OUTPUT
{
	float4 position				: SV_POSITION;
	float3 vWorldPos			: POSITION;
	float3 vNormal				: NORMAL;
	float3 vBinormal			: BINORMAL;
	float3 vTangent				: TANGENT;
	float2 texCoord				: TEXCOORD0;
};

VS_OUTPUT main(VS_INPUT i)
{
	VS_OUTPUT result;

	float4 transformPosition = mul(modelMatrix, float4(i.vInPositionOS, 1.0f));
	transformPosition = mul(viewMatrix, transformPosition);
	transformPosition = mul(projectionMatrix, transformPosition);
	result.position = transformPosition;
	result.vWorldPos = i.vInPositionOS;
	result.vNormal = i.vInNormalOS;
	result.vBinormal = i.vInBinormalOS;
	result.vTangent = i.vInTangentOS;
	result.texCoord = i.vInTexCoord;

	return result;
}