#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (input_attachment_index = 0, binding = 0) uniform subpassInput sampler_shadow_map;

layout (location = 0) in vec2 in_uv;

layout (location = 0) out vec4 out_fragment_color;

void main() 
{
	const float depth = subpassLoad(sampler_shadow_map).r;

	out_fragment_color = vec4(depth * 100.0f);
}