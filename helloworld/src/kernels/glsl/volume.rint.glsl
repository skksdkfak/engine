#version 460

#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_EXT_buffer_reference2 : enable
#extension GL_EXT_scalar_block_layout : enable

struct vertex_device_layout
{
    vec3 position;
    vec3 normal;
    vec2 uv;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer material_buffer
{
    uint diffuse_texture;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer index_buffer
{
    uvec3 indices[];
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer vertex_buffer
{
    vertex_device_layout vertices[];
};

layout(shaderRecordEXT) buffer shader_record_buffer
{
    material_buffer material_buffer;
    index_buffer index_buffer;
    vertex_buffer vertex_buffer;
} shader_record;

layout(set = 0, binding = 4) uniform sampler linear_sampler;

layout(set = 0, binding = 5) uniform texture2D textures[];

struct Ray
{
    vec3 origin;
    vec3 direction;
};
struct Sphere
{
    vec3  center;
    float radius;
};

// Ray-Sphere intersection
// http://viclw17.github.io/2018/07/16/raytracing-ray-sphere-intersection/
float hitSphere(const Sphere s, const Ray r)
{
    vec3  oc = r.origin - s.center;
    float a = dot(r.direction, r.direction);
    float b = 2.0 * dot(oc, r.direction);
    float c = dot(oc, oc) - s.radius * s.radius;
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0)
    {
        return -1.0;
    }
    else
    {
        return (-b - sqrt(discriminant)) / (2.0 * a);
    }
}

void main()
{
    Ray ray;
    ray.origin = gl_WorldRayOriginEXT;
    ray.direction = gl_WorldRayDirectionEXT;

    float tHit = -1;
    Sphere sphere;

    sphere.center = vec3(0,0,0);
    sphere.radius = 10.0;

    tHit = hitSphere(sphere, ray);
    if (tHit > 0)
    reportIntersectionEXT(tHit, 0);
}
