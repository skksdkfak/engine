#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (input_attachment_index = 0, binding = 0) uniform subpassInput samplerPosition;
layout (input_attachment_index = 1, binding = 1) uniform subpassInput samplerNormal;
layout (input_attachment_index = 2, binding = 2) uniform subpassInput samplerAlbedo;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

void main() 
{
	const vec3 pos = subpassLoad(samplerPosition).rgb;
	const vec3 normal = subpassLoad(samplerNormal).rgb;
	const vec4 color = subpassLoad(samplerAlbedo);
	const vec3 lightPos = vec3(10.0f, 5.0f, 2.0f);
	const vec3 lightDir = normalize(lightPos - pos);
	const float dotNL = dot(normal, lightDir);

	outFragColor = color * dotNL;
}