#version 460

#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_EXT_buffer_reference2 : enable
#extension GL_EXT_scalar_block_layout : enable

struct ray_payload_t
{
    vec3 color;
    vec3 normal;
    float distance;
};

layout(location = 0) rayPayloadInEXT ray_payload_t ray_payload;

struct vertex_device_layout
{
    vec3 position;
    vec3 normal;
    vec2 uv;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer material_buffer
{
    uint diffuse_texture;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer index_buffer
{
    uvec3 indices[];
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer vertex_buffer
{
    vertex_device_layout vertices[];
};

layout(shaderRecordEXT) buffer shader_record_buffer
{
    material_buffer material_buffer;
    index_buffer index_buffer;
    vertex_buffer vertex_buffer;
} shader_record;

layout(set = 0, binding = 4) uniform sampler linear_sampler;

layout(set = 0, binding = 5) uniform texture2D textures[];

hitAttributeEXT vec3 attribs;

uint xxhash32(in uint p)
{
    const uint prime32_2 = 2246822519u, prime32_3 = 3266489917u;
    const uint prime32_4 = 668265263u, prime32_5 = 374761393u;

    uint h32 = p + prime32_5;
    h32 = prime32_4 * ((h32 << 17) | (h32 >> (32 - 17)));
    h32 = prime32_2 * (h32 ^ (h32 >> 15));
    h32 = prime32_3 * (h32 ^ (h32 >> 13));

    return h32 ^ (h32 >> 16);
}

vec3 hash31(uint x)
{
    uint n = xxhash32(x);
    uvec3 rz = uvec3(n, n * 16807U, n * 48271U); //see: http://random.mat.sbg.ac.at/results/karl/server/node4.html
    return vec3((rz >> 1) & uvec3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU)) / float(0x7fffffff);
}

void main()
{
    ray_payload.color = vec3(1, 0, 0); return;
}
