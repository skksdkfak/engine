#version 460

#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_buffer_reference2 : enable
#extension GL_EXT_scalar_block_layout : enable

#include "raytrace.glsl"

uint rand_state;

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return float(word) / 4294967295.0f;
}

void update_reservoir(inout reservoir_t r, sample_t s, float w)
{
	r.w += w;
	++r.M;
	if (rand(rand_state) < w / r.w)
	{
		r.z = s;
	}
}

void merge_reservoir(inout reservoir_t r0, in reservoir_t r1, float p_hat)
{
	uint M0 = r0.M;
	update_reservoir(r0, r1.z, p_hat * r1.W * r1.M);
	r0.M = M0 + r1.M;
}

layout(set = 0, binding = 0) uniform accelerationStructureEXT acceleration_structure;

layout(set = 0, binding = 1) uniform CameraProperties
{
	mat4 viewInverse;
	mat4 projInverse;
	float ray_eps;
	uint seed;
	uint sample_count;
} camera;

layout(set = 0, binding = 2) buffer initial_samples_buffer
{
	reservoir_t g_initial_samples[];
};

layout(set = 0, binding = 3) buffer resampled_samples_buffer
{
	reservoir_t g_resampled_samples[];
};

layout(set = 0, binding = 4, rgba8) uniform image2D accumulator;

layout(set = 1, binding = 0, rgba8) uniform image2D image;

vec3 uniform_sample_hemisphere(vec2 u)
{
	const float z = u.x;
	const float r = sqrt(max(0, 1. - z * z));
	const float phi = 6.2831853 * u.y;
	return vec3(r * cos(phi), z, r * sin(phi));
}

float uniform_hemisphere_pdf()
{
	return 0.15915494309189533577;
}

// https://graphics.pixar.com/library/OrthonormalB/paper.pdf
void get_ortho_basis(in vec3 n, out vec3 b1, out vec3 b2)
{
	float sign = (n.z >= 0.0f ? 1.0f : -1.0f);
	float a = -1.0f / (sign + n.z);
	float b = n.x * n.y * a;

	b1 = vec3(1.0f + sign * n.x * n.x * a, sign * b, -sign * n.x);
	b2 = vec3(b, sign + n.y * n.y * a, -n.y);
}

float luminance(vec3 color)
{
	return dot(color, vec3(0.299, 0.587, 0.114));
}

layout(location = 0) rayPayloadInEXT ray_payload_t ray_payload;

layout (binding = 6) uniform samplerCube sampler_cube_map;

void main()
{
    //ray_payload.color = vec3(1.0, 1.0, 1.0);
    ray_payload.color = textureLod(sampler_cube_map, normalize(gl_WorldRayDirectionEXT), 0).rgb;
    ray_payload.distance = -1.0;
}