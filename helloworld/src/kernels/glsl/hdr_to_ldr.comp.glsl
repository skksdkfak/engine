#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_shader_explicit_arithmetic_types_int8 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable

layout(local_size_x = 128) in;

layout(std140, push_constant) uniform push_constants
{
	uint pixel_count;
	uint input_stride;
	uint output_stride;
};

layout(std430, set = 0, binding = 0) readonly buffer input_buffer_t
{
	ELEMENT_TYPE input_buffer[];
};

layout(std430, set = 0, binding = 1) writeonly buffer output_buffer_t
{
	uint8_t output_buffer[];
};

void main()
{
	const uint global_invocation_id = gl_GlobalInvocationID.x;
	if (global_invocation_id >= pixel_count)
	{
		return;
	}

	const uint input_pixel_index = global_invocation_id * input_stride;
	const uint output_pixel_index = global_invocation_id * output_stride;
	const vec3 hdr_color = vec3(input_buffer[input_pixel_index + 0], input_buffer[input_pixel_index + 1], input_buffer[input_pixel_index + 2]);
	const u8vec3 ldr_color = u8vec3(pow(max(min(hdr_color, vec3(1.0f)), vec3(0.0f)), vec3(1.0f / 2.2f)) * 255.0f + 0.5f);
	output_buffer[output_pixel_index + 0] = ldr_color.x;
	output_buffer[output_pixel_index + 1] = ldr_color.y;
	output_buffer[output_pixel_index + 2] = ldr_color.z;
}
