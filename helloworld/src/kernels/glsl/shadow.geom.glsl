#version 450

#define GS_INVOCATION_COUNT 1

layout (triangles, invocations = GS_INVOCATION_COUNT) in;
layout (triangle_strip, max_vertices = 3) out;

layout (binding = 0) uniform UBO 
{
	mat4 wvp[GS_INVOCATION_COUNT];
} ubo;

layout (location = 0) out vec3 out_position;

void main()
{
    for (int i = 0; i < gl_in.length(); i++)
    {
        gl_Layer = gl_InvocationID;
	    gl_Position = ubo.wvp[gl_InvocationID] * gl_Position;
	    EmitVertex();
	}
	EndPrimitive();
}