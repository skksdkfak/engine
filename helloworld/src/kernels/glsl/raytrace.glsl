struct ray_payload_t
{
	vec3 color;
	vec3 direction;
	float distance;
	vec3 normal;
	vec3 geometric_normal;
	bool refracted;
};

struct vertex_device_layout
{
    vec3 position;
    vec3 normal;
    vec2 uv;
};

struct sample_t
{
	vec3 xv;
	vec3 nv;
	vec3 xs;
	vec3 ns;
	vec3 radiance;
};

struct reservoir_t
{
	sample_t z;
	float w;
	uint M;
	float W;
	// todo: remove
	vec3 visible_point_throughput;
	vec3 position;
	vec3 normal;
	float ray_length;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer material_buffer
{
    uint diffuse_texture;
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer index_buffer
{
    uvec3 indices[];
};

layout(buffer_reference, buffer_reference_align = 8, scalar) buffer vertex_buffer
{
    vertex_device_layout vertices[];
};

layout(shaderRecordEXT) buffer shader_record_buffer
{
    material_buffer material_buffer;
    index_buffer index_buffer;
    vertex_buffer vertex_buffer;
} shader_record;
