#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable

layout(local_size_x = 128) in;

layout(std140, push_constant) uniform push_constants
{
	uint width;
	uint height;
	uint batch_size;
	uint padded_input_size;
	uint input_stride;
	uint output_stride;
	uint seed;
	int matrix_layout;
};

layout(set = 0, binding = 0) uniform sampler2D image;

layout(std430, set = 0, binding = 1) writeonly buffer uvs_buffer_t
{
	ELEMENT_TYPE uvs_buffer[];
};

layout(std430, set = 0, binding = 2) writeonly buffer image_texels_buffer_t
{
	float image_texels_buffer[];
};

uint coord_to_offset(uint i, uint j, uint stride, int matrix_layout)
{
	return matrix_layout == gl_CooperativeMatrixLayoutColumnMajor ? (stride * j + i) : (stride * i + j);
}

void main()
{
	const uint global_invocation_id = gl_GlobalInvocationID.x;
	if (global_invocation_id >= batch_size)
	{
		return;
	}

	const vec2 uv = (vec2(global_invocation_id % width, global_invocation_id / width) + 0.5f) / vec2(width, height);

	uvs_buffer[coord_to_offset(0, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(uv.x);
	uvs_buffer[coord_to_offset(1, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(uv.y);
	for (uint i = 2; i < padded_input_size; ++i)
	{
		uvs_buffer[coord_to_offset(i, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(1.0);
	}
}