#version 460

#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_buffer_reference2 : enable
#extension GL_EXT_scalar_block_layout : enable

#include "raytrace.glsl"

uint rand_state;

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return float(word) / 4294967295.0f;
}

void update_reservoir(inout reservoir_t r, sample_t s, float w)
{
	r.w += w;
	++r.M;
	if (rand(rand_state) < w / r.w)
	{
		r.z = s;
	}
}

void merge_reservoir(inout reservoir_t r0, in reservoir_t r1, float p_hat)
{
	uint M0 = r0.M;
	update_reservoir(r0, r1.z, p_hat * r1.W * r1.M);
	r0.M = M0 + r1.M;
}

layout(set = 0, binding = 0) uniform accelerationStructureEXT acceleration_structure;

layout(set = 0, binding = 1) uniform CameraProperties
{
	mat4 viewInverse;
	mat4 projInverse;
	float ray_eps;
	uint seed;
	uint sample_count;
} camera;

layout(set = 0, binding = 2) buffer initial_samples_buffer
{
	reservoir_t g_initial_samples[];
};

layout(set = 0, binding = 3) buffer resampled_samples_buffer
{
	reservoir_t g_resampled_samples[];
};

layout(set = 0, binding = 4, rgba8) uniform image2D accumulator;

layout(set = 1, binding = 0, rgba8) uniform image2D image;

layout(location = 0) rayPayloadEXT ray_payload_t ray_payload;

vec3 uniform_sample_hemisphere(vec2 u)
{
	const float z = u.x;
	const float r = sqrt(max(0, 1. - z * z));
	const float phi = 6.2831853 * u.y;
	return vec3(r * cos(phi), z, r * sin(phi));
}

float uniform_hemisphere_pdf()
{
	return 0.15915494309189533577;
}

// https://graphics.pixar.com/library/OrthonormalB/paper.pdf
void get_ortho_basis(in vec3 n, out vec3 b1, out vec3 b2)
{
	float sign = (n.z >= 0.0f ? 1.0f : -1.0f);
	float a = -1.0f / (sign + n.z);
	float b = n.x * n.y * a;

	b1 = vec3(1.0f + sign * n.x * n.x * a, sign * b, -sign * n.x);
	b2 = vec3(b, sign + n.y * n.y * a, -n.y);
}

float luminance(vec3 color)
{
	return dot(color, vec3(0.299, 0.587, 0.114));
}

void main() 
{
	const uint gid = gl_LaunchIDEXT.y * gl_LaunchSizeEXT.x + gl_LaunchIDEXT.x;
	const vec2 pixelCenter = vec2(gl_LaunchIDEXT.xy) + vec2(0.5);
	const vec2 inUV = pixelCenter / vec2(gl_LaunchSizeEXT.xy);
	vec2 d = inUV * 2.0 - 1.0;

	rand_state = gid + camera.seed;
	vec3 color = vec3(0.0, 0.0, 0.0);

	float tmin = 0.001;
	float tmax = 10000.0;

	const uint samples_per_pixel = 1;
	for (uint j = 0; j < samples_per_pixel; j++)
	{
		vec3 path_radiance = vec3(0.0, 0.0, 0.0);
		vec3 path_throughput = vec3(1.0, 1.0, 1.0);
		vec3 visible_point_throughput = vec3(1.0, 1.0, 1.0);
		vec4 origin = camera.viewInverse * vec4(0, 0, 0, 1);
		vec4 target = camera.projInverse * vec4(d.x, d.y, 1, 1);
		vec4 direction = camera.viewInverse * vec4(normalize(target.xyz), 0);
		// Initial Sampling
		reservoir_t r;

		bool valid_sample = false;
		float source_pdf;
		float target_pdf;

		do
		{
			// Find visible point xv and normal nv

			traceRayEXT(acceleration_structure, gl_RayFlagsNoneEXT, 0xff, 0, 1, 0, origin.xyz, tmin, direction.xyz, tmax, 0);

			if (ray_payload.distance < 0.0)
			{
				color = ray_payload.color;
				break;
			}
			else
			{
				// Sample random ray direction wi from source PDF pq
				vec3 t, b;
				get_ortho_basis(ray_payload.normal, t, b);
				mat3 tnb = { t, ray_payload.normal, b };

				origin.xyz += direction.xyz * ray_payload.distance + ray_payload.normal * 0.001f;
				direction.xyz = tnb * uniform_sample_hemisphere(vec2(rand(rand_state), rand(rand_state)));
				source_pdf = uniform_hemisphere_pdf();
				
				visible_point_throughput = (ray_payload.color * dot(direction.xyz, ray_payload.normal)) / source_pdf;

				r.z.xv = origin.xyz; // todo: should it be with offset?
				r.z.nv = ray_payload.normal;
				r.position = origin.xyz;
				r.normal = ray_payload.normal;
				r.ray_length = ray_payload.distance;
			}

			// Trace ray to find sample point xs and normal ns
			// Estimate outgoing radiance Lo at xs

			traceRayEXT(acceleration_structure, gl_RayFlagsNoneEXT, 0xff, 0, 1, 0, origin.xyz, tmin, direction.xyz, tmax, 0);

			if (ray_payload.distance < 0.0)
			{
				path_radiance += /*visible_point_throughput * */ray_payload.color;
				break;
			}
			else
			{
				vec3 t, b;
				get_ortho_basis(ray_payload.normal, t, b);
				mat3 tnb = { t, ray_payload.normal, b };

				origin.xyz += direction.xyz * ray_payload.distance + ray_payload.normal * 0.001f;
				direction.xyz = tnb * uniform_sample_hemisphere(vec2(rand(rand_state), rand(rand_state)));

				path_throughput = (ray_payload.color * dot(direction.xyz, ray_payload.normal)) / uniform_hemisphere_pdf();

				r.z.xs = origin.xyz; // todo: should it be with offset?
				r.z.ns = ray_payload.normal;
			}

			valid_sample = true;

			for (uint i = 2; i < 5; i++)
			{
				traceRayEXT(acceleration_structure, gl_RayFlagsNoneEXT, 0xff, 0, 1, 0, origin.xyz, tmin, direction.xyz, tmax, 0);

				if (ray_payload.distance < 0.0)
				{
					path_radiance += path_throughput * ray_payload.color;
					break;
				}

				vec3 t, b;
				get_ortho_basis(ray_payload.normal, t, b);
				mat3 tnb = { t, ray_payload.normal, b };

				origin.xyz += direction.xyz * ray_payload.distance + ray_payload.normal * 0.001f;
				direction.xyz = tnb * uniform_sample_hemisphere(vec2(rand(rand_state), rand(rand_state)));

				path_throughput *= (ray_payload.color * dot(direction.xyz, ray_payload.normal)) / uniform_hemisphere_pdf();
			}
		} while (false);

		target_pdf = luminance(path_radiance);

		r.z.radiance = path_radiance;
		r.visible_point_throughput = visible_point_throughput;

		r.w = 0;
		r.M = 0;
		r.W = 0;
		
		float W = target_pdf / source_pdf;
		
		update_reservoir(r, r.z, W);

		float denom = (r.M * target_pdf);
		r.W = denom > 0 ? r.w / denom : 0;

		color += visible_point_throughput * path_radiance/* * r.W*/;
		
		if (!valid_sample)
		{
			r.M = 0;
		}

		g_initial_samples[gid] = r;
	}

	vec4 output_color = vec4(color / samples_per_pixel, 0.0);
	if (camera.sample_count > 1)
	{
		vec4 accumulated = imageLoad(accumulator, ivec2(gl_LaunchIDEXT.xy)) + output_color;
		imageStore(accumulator, ivec2(gl_LaunchIDEXT.xy), accumulated);
		output_color = accumulated / camera.sample_count;
	}
	else
	{
		imageStore(accumulator, ivec2(gl_LaunchIDEXT.xy), output_color);
	}
	
	imageStore(image, ivec2(gl_LaunchIDEXT.xy), output_color);
}
