#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inBinormal;
layout (location = 3) in vec3 inTangent;
layout (location = 4) in vec2 inUV;

layout (binding = 0) uniform UBO 
{
	mat4 projectionMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
	vec4 camPos;
	vec4 lightPos;
} ubo;

//layout( std430, binding = 1 ) buffer ssbo
//{
//    vec4 ssboColor[];
//};

layout (location = 0) out vec3 outWorldPos;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec3 outBinormal;
layout (location = 3) out vec3 outTangent;
layout (location = 4) out vec2 outUV;

void main()
{
	gl_Position = ubo.projectionMatrix * ubo.viewMatrix * ubo.modelMatrix * vec4(inPos, 1.0f);
	outWorldPos = (ubo.modelMatrix * vec4(inPos, 1.0f)).xyz;
	outNormal = normalize(mat3(ubo.modelMatrix) * inNormal);
	outBinormal = inBinormal;
	outTangent = inTangent;
	outUV = inUV;

	//vec4 curColor = ssboColor[0];
	//curColor.b += 0.0001f;		

	//ssboColor[0] = curColor;
}