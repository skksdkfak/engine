#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int8 : enable

layout(local_size_x = 128) in;

layout(std140, push_constant) uniform push_constants
{
	uint width;
	uint height;
	uint batch_size;
	uint padded_input_size;
	uint input_stride;
	uint output_stride;
	uint seed;
	int matrix_layout;
};

layout(set = 0, binding = 0) uniform sampler2D image;

layout(std430, set = 0, binding = 1) writeonly buffer uvs_buffer_t
{
	ELEMENT_TYPE uvs_buffer[];
};

layout(std430, set = 0, binding = 2) writeonly buffer image_texels_buffer_t
{
	float image_texels_buffer[];
};

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return uintBitsToFloat((word >> 9) | 0x3f800000u) - 1.0f;
}

uint coord_to_offset(uint i, uint j, uint stride, int matrix_layout)
{
	return matrix_layout == gl_CooperativeMatrixLayoutColumnMajor ? (stride * j + i) : (stride * i + j);
}

void main()
{
	const uint global_invocation_id = gl_GlobalInvocationID.x;
	if (global_invocation_id >= batch_size)
	{
		return;
	}

	uint rand_state0 = seed + global_invocation_id * 2 + 0;
	uint rand_state1 = seed + global_invocation_id * 2 + 1;
	const vec2 uv = vec2(rand(rand_state0), rand(rand_state1));
	const vec3 value = texture(image, uv).rgb;
	const uint values_output_index = global_invocation_id * output_stride;

	uvs_buffer[coord_to_offset(0, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(uv.x);
	uvs_buffer[coord_to_offset(1, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(uv.y);
	for (uint i = 2; i < padded_input_size; ++i)
	{
		uvs_buffer[coord_to_offset(i, global_invocation_id, input_stride, matrix_layout)] = ELEMENT_TYPE(1.0);
	}
	image_texels_buffer[values_output_index + 0] = value.r;
	image_texels_buffer[values_output_index + 1] = value.g;
	image_texels_buffer[values_output_index + 2] = value.b;
	for (uint i = 3; i < output_stride; ++i)
	{
		image_texels_buffer[values_output_index + i] = 1.0f;
	}
}