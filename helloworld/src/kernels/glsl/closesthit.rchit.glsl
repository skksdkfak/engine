#version 460

#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_nonuniform_qualifier : enable
#extension GL_EXT_buffer_reference2 : enable
#extension GL_EXT_scalar_block_layout : enable

#include "raytrace.glsl"

layout(location = 0) rayPayloadInEXT ray_payload_t ray_payload;

layout(set = 0, binding = 5) uniform sampler linear_sampler;

layout(set = 0, binding = 7) uniform texture2D textures[];

hitAttributeEXT vec3 attribs;

uint xxhash32(in uint p)
{
    const uint prime32_2 = 2246822519u, prime32_3 = 3266489917u;
    const uint prime32_4 = 668265263u, prime32_5 = 374761393u;

    uint h32 = p + prime32_5;
    h32 = prime32_4 * ((h32 << 17) | (h32 >> (32 - 17)));
    h32 = prime32_2 * (h32 ^ (h32 >> 15));
    h32 = prime32_3 * (h32 ^ (h32 >> 13));

    return h32 ^ (h32 >> 16);
}

vec3 hash31(uint x)
{
    uint n = xxhash32(x);
    uvec3 rz = uvec3(n, n * 16807U, n * 48271U); //see: http://random.mat.sbg.ac.at/results/karl/server/node4.html
    return vec3((rz >> 1) & uvec3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU)) / float(0x7fffffff);
}

void main()
{
    const vec3 barycentric_coords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
    //hitValue = shaderRec.c.rgb;
    //hitValue = hash31(shader_record.material_buffer.diffuse_texture);

    uvec3 indices = shader_record.index_buffer.indices[gl_PrimitiveID];

    vertex_device_layout v0 = shader_record.vertex_buffer.vertices[indices.x];
    vertex_device_layout v1 = shader_record.vertex_buffer.vertices[indices.y];
    vertex_device_layout v2 = shader_record.vertex_buffer.vertices[indices.z];

    vec3 normal = v0.normal * barycentric_coords.x + v1.normal * barycentric_coords.y + v2.normal * barycentric_coords.z;
    vec2 uv = v0.uv * barycentric_coords.x + v1.uv * barycentric_coords.y + v2.uv * barycentric_coords.z;
    uv.y = 1.0 - uv.y;
    
	vec3 geometric_normal = normalize(cross(normalize(v1.position - v0.position), normalize(v2.position - v0.position)));

    vec3 color = shader_record.material_buffer.diffuse_texture == -1 ? vec3(1.0) : textureLod(sampler2D(textures[shader_record.material_buffer.diffuse_texture], linear_sampler), uv, 0).rgb;

    ray_payload.color = color;
    ray_payload.normal = normal;
    ray_payload.distance = gl_HitTEXT;
    ray_payload.geometric_normal = geometric_normal;
    ray_payload.refracted = false;
}
