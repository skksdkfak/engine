#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inBinormal;
layout (location = 3) in vec3 inTangent;
layout (location = 4) in vec2 inUV;

layout (binding = 0) uniform UBO
{
	mat4 projectionMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
	vec4 camPos;
	vec4 lightPos;
} ubo;

//layout(std140, binding = 0) uniform buf
//{
//	vec4 color;
//} ubuf;

layout (std140, push_constant) uniform PushConsts 
{
	vec4 color;
} pushConsts;

layout (set = 0, binding = 1) uniform samplerBuffer myUniformTexelBuffer;
layout (set = 0, binding = 2) uniform sampler2D samplerColor;
//layout (set = 0, binding = 1, rgba32f) readonly uniform imageBuffer myStorageTexelBuffer;

layout(location = 0) out vec4 oColor;

const float PI = 3.14159265359;

vec3 materialcolor()
{
	return texture(samplerColor, inUV).xyz;
}

// Normal Distribution function --------------------------------------
float D_GGX(float dotNH, float roughness)
{
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
	return (alpha2)/(PI * denom*denom);
}

// Geometric Shadowing function --------------------------------------
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;
	float GL = dotNL / (dotNL * (1.0 - k) + k);
	float GV = dotNV / (dotNV * (1.0 - k) + k);
	return GL * GV;
}

// Fresnel function ----------------------------------------------------
vec3 F_Schlick(float cosTheta, float metallic)
{
	vec3 F0 = mix(vec3(0.04), materialcolor(), metallic); // * material.specular
	vec3 F = F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
	return F;
}

// Specular BRDF composition --------------------------------------------

vec3 BRDF(vec3 L, vec3 V, vec3 N, float metallic, float roughness)
{
	// Precalculate vectors and dot products
	vec3 H = normalize (V + L);
	float dotNV = clamp(dot(N, V), 0.0, 1.0);
	float dotNL = clamp(dot(N, L), 0.0, 1.0);
	float dotLH = clamp(dot(L, H), 0.0, 1.0);
	float dotNH = clamp(dot(N, H), 0.0, 1.0);

	// Light color fixed
	vec3 lightColor = vec3(1.0);

	vec3 color = vec3(0.0);

	if (dotNL > 0.0)
	{
		float rroughness = max(0.05, roughness);
		// D = Normal distribution (Distribution of the microfacets)
		float D = D_GGX(dotNH, roughness);
		// G = Geometric shadowing term (Microfacets shadowing)
		float G = G_SchlicksmithGGX(dotNL, dotNV, roughness);
		// F = Fresnel factor (Reflectance depending on angle of incidence)
		vec3 F = F_Schlick(dotNV, metallic);

		vec3 spec = D * F * G / (4.0 * dotNL * dotNV);

		color += spec * dotNL * lightColor;
	}

	return color;
}

void main()
{
	const vec3 lightPos = vec3(10.0f, 10.0f, 10.0f);
//	const vec3 lightDir = normalize(lightPos.xyz - inPos);
//	float atten = 100.0f / (pow(length(lightPos.xyz - inPos), 2.0) + 1.0);
//	const vec3 viewDir = normalize(ubo.camPos.xyz - inPos);
//	const float dotNL = max(0.0f, dot(inNormal, lightDir));
//	const vec3 reflect = reflect(-lightDir, inNormal);
//	const vec3 spec = pow(max(0.0f, dot(reflect, viewDir)), 16.0f) * atten * vec3(1,1,1);
//	oColor = texture(samplerColor, inUV) * dotNL + vec4(spec, 1.0f);
	//oColor = pushConsts.color;
	//vec4 meshColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//oColor = vec4(meshColor.rgb * (max(0.0f, dot(vec3(0.556433320f, 0.489277601f, 0.671557486f), normalize(inNormal)))), 1.0f);
	//oColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//oColor = imageLoad(myStorageTexelBuffer, 1);

	//region PBR
	vec3 N = normalize(inNormal);
	vec3 V = normalize(ubo.camPos.xyz - inPos);
	vec3 L = normalize(lightPos.xyz - inPos);
	float roughness = 0.3f;
	float metallic = 1.0f;

	// Combine with ambient
	vec3 color = materialcolor() * 0.02;
	color += BRDF(L, V, N, metallic, roughness);

	// Gamma correct
	color = pow(color, vec3(0.4545));

	oColor = vec4(color, 1.0f);
	//endregion
}