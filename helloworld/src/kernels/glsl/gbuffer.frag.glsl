#version 450

#extension GL_EXT_nonuniform_qualifier : enable

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inBinormal;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec2 inUV;

layout(location = 0) out vec4 outPosition;
layout(location = 1) out vec4 outNormal;
layout(location = 2) out vec4 outAlbedo;

layout(binding = 1) uniform material_layout
{
	uint diffuse_texture;
} material;

//layout(std140, push_constant) uniform PushConsts
//{
//	vec4 color;
//} pushConsts;

//layout(set = 0, binding = 1) uniform samplerBuffer myUniformTexelBuffer;
layout(set = 0, binding = 2) uniform sampler linear_sampler;
layout(set = 0, binding = 3) uniform texture2D textures[];
//layout (set = 0, binding = 1, rgba32f) readonly uniform imageBuffer myStorageTexelBuffer;

layout(constant_id = 0) const float NEAR_PLANE = 0.1f;
layout(constant_id = 1) const float FAR_PLANE = 1024.0f;

float linearDepth(float depth)
{
	float z = depth;
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));
}

void main()
{
	//oColor = pushConsts.color;
	//vec4 meshColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//outColor = vec4(1,0.5,0, 1.0f);
	outPosition = vec4(inPos, linearDepth(gl_FragCoord.z));
	outNormal = vec4(inNormal, 1.0f);
	outAlbedo = vec4(texture(sampler2D(textures[material.diffuse_texture], linear_sampler), inUV).rgb, 1.0f);
	//oColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//oColor = imageLoad(myStorageTexelBuffer, 1);
}