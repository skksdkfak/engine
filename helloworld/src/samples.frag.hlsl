struct push_constants_t
{
	uint32_t pad_0;
	uint32_t pad_1;
	uint32_t pad_2;
	uint32_t pad_3;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);
[[vk::binding(0, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);
[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);
[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);
//[[vk::binding(3, 0)]] RWStructuredBuffer<uint8_t> pdf_plot_buffer : register(u3, space0); // won't compile on hlsl as it doesn't support uint8_t
[[vk::binding(4, 0)]] RWStructuredBuffer<float> target_buffer : register(u4, space0);
[[vk::binding(5, 0)]] Texture2D<float4> target_image : register(t0, space0);
[[vk::binding(6, 0)]] Texture2D<float4> samples_image : register(t1, space0);

struct ps_input_t
{
	float4 position : SV_POSITION;
};

[shader("pixel")]
float4 main(ps_input_t input) : SV_TARGET0
{
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
}