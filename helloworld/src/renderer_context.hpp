#pragma once

#include "command_context.hpp"
#include "engine_core.h"
#include "gpu/core.hpp"
#include "gpu_log.hpp"
#include "gpu_log.hpp"
#include "profiler.hpp"
#include <memory>
#include <cstdint>
#include <vector>

namespace framework::graphics
{
	class renderer_context
	{
	public:
		renderer_context();

		virtual ~renderer_context();

		void initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool);

		virtual void resize(::std::uint32_t width, ::std::uint32_t height);

		::std::uint32_t get_width() const noexcept;

		::std::uint32_t get_height() const noexcept;

		::framework::gpu_context * get_gpu_context() const noexcept;

		::framework::command_buffer_manager * get_command_buffer_manager() const noexcept;

		::framework::profiler * get_profiler() const noexcept;

		::framework::gpu_log * get_gpu_log() const noexcept;

	protected:
		void create_swap_chain();

		::framework::gpu::api_type api_type;
		::framework::platform::window * window;
		::framework::gpu::instance * instance;
		::std::uint32_t physical_device_count;
		::framework::gpu::physical_device ** physical_devices;
		::framework::gpu::surface * surface;
		::std::uint32_t width, height;
		::framework::gpu::swap_chain * swap_chain;
		::framework::gpu::format swap_chain_format;
		::framework::gpu::color_space swap_chain_color_space;
		::std::vector<::framework::gpu::image *> swap_chain_images;
		::std::vector<::framework::gpu::image_view *> swap_chain_image_views;
		::std::unique_ptr<::framework::gpu_context> gpu_context;
		::std::unique_ptr<::framework::linear_allocator_page_manager> linear_allocator_page_manager;
		::std::unique_ptr<::framework::command_buffer_manager> command_buffer_manager;
		::std::unique_ptr<::framework::profiler> profiler;
		::std::unique_ptr<::framework::gpu_log> gpu_log;
	};
}

#include "renderer_context.inl"