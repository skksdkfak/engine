inline ::std::uint32_t (::framework::graphics::renderer_context::get_width)() const noexcept
{
	return this->width;
}

inline ::std::uint32_t(::framework::graphics::renderer_context::get_height)() const noexcept
{
	return this->height;
}

inline ::framework::gpu_context * (::framework::graphics::renderer_context::get_gpu_context)() const noexcept
{
	return this->gpu_context.get();
}

inline ::framework::command_buffer_manager * (::framework::graphics::renderer_context::get_command_buffer_manager)() const noexcept
{
	return this->command_buffer_manager.get();
}

inline ::framework::profiler * ::framework::graphics::renderer_context::get_profiler() const noexcept
{
	return this->profiler.get();
}

inline ::framework::gpu_log * ::framework::graphics::renderer_context::get_gpu_log() const noexcept
{
	return this->gpu_log.get();
}
