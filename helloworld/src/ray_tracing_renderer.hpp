#pragma once

#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/renderer_2d.hpp"
#include "graphics/text_renderer.hpp"
#include "graphics/ui.hpp"
#include "memory/linear_allocator.hpp"
#include "renderer_context.hpp"
#include <engine_core.h>
#include <memory>

namespace framework::coroutine
{
	class static_thread_pool;
}

namespace framework::graphics
{
	class ray_tracing_renderer : public ::framework::graphics::renderer_context
	{
	public:
		ray_tracing_renderer();

		~ray_tracing_renderer() override;

		void initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool, typename ::framework::resource::resource_manager & resource_manager);

		void destroy();

		virtual void resize(::std::uint32_t width, ::std::uint32_t height);

		void update(double deltaT);

		::framework::graphics::scene_renderer * get_scene_renderer() const noexcept;

		::framework::graphics::aov * get_aov() const noexcept;

		::framework::graphics::text_renderer * get_text_renderer() { return this->text_renderer; }

		::framework::graphics::renderer_2d * get_renderer_2d() { return this->renderer_2d; }

		::framework::graphics::ui * get_ui() { return this->ui; }

		::framework::graphics::scene_view * get_scene_view() const { return this->scene_view; }

	private:
		void create_screen_resources();

		::framework::graphics::scene * scene;
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;
		::framework::gpu::pipeline_cache * pipeline_cache;
		struct
		{
			::framework::gpu::command_buffer * begin_frame_command_buffer;
			::framework::gpu::command_buffer * command_buffer;
			::framework::gpu::fence * frame_fence;
			::framework::gpu::semaphore * present_complete_semaphore;
		} frame_resources[3];
		::framework::gpu::command_pool * graphics_queue_command_pool;
		::framework::gpu::command_pool * transfer_queue_command_pool;
		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::graphics::text_renderer * text_renderer;
		::framework::graphics::renderer_2d * renderer_2d;
		::framework::graphics::ui * ui;
		::framework::graphics::scene_view * scene_view;
		::framework::memory::linear_allocator single_frame_allocator;
		::framework::gpu::descriptor_pool * host_descriptor_pool;
		::framework::gpu::descriptor_pool * device_descriptor_pool;
		::framework::gpu::descriptor_set_layout * attachments_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * gpu_log_host_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * gpu_log_device_descriptor_set_layout;
		::framework::gpu::descriptor_set * attachments_descriptor_set;
		::framework::gpu::descriptor_set * gpu_log_host_descriptor_set;
		::framework::gpu::descriptor_set * gpu_log_device_descriptor_set;
		::framework::gpu::image * render_target_image;
		::framework::gpu::device_memory * render_target_device_memory;
		::framework::gpu::image_view * render_target_image_view;
		::framework::gpu::host_descriptor_handle render_target_host_descriptor_handle;
		::framework::gpu::image * ui_depth_image;
		::framework::gpu::device_memory * ui_depth_device_memory;
		::framework::gpu::image_view * ui_depth_image_view;
		::framework::gpu::host_descriptor_handle gpu_log_counter_host_descriptor_handle;
		::framework::gpu::device_descriptor_handle gpu_log_counter_device_descriptor_handle;
		::framework::gpu::host_descriptor_handle ui_depth_host_descriptor_handle;
		::framework::gpu::semaphore * begin_frame_semaphore;
		::framework::gpu::semaphore * frame_semaphore;
		::framework::gpu::semaphore * ready_to_present_semaphore;
		::std::uint64_t frame_index;
		::std::uint32_t buffered_frame_count;
		bool enable_accumulation;
		bool show_profiler_statistics;
		::std::uint32_t profiler_statistics_line_offset;
		::std::uint32_t profiler_statistics_max_depth;
		::std::uint32_t nis_visualization_2d_primitive_index;
		::std::uint32_t ground_truth_distribution_2d_primitive_index;
	};
}

#include "ray_tracing_renderer.inl"