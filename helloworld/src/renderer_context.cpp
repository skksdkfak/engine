#include "coroutine/sync_wait.hpp"
#include "gpu/utility.hpp"
#include "renderer_context.hpp"
#include <iterator>
#include <vector>

::framework::graphics::renderer_context::renderer_context() :
	swap_chain(nullptr)
{
}

::framework::graphics::renderer_context::~renderer_context()
{
	::framework::gpu::instance::destroy_instance(this->api_type, this->instance, nullptr);
}

void ::framework::graphics::renderer_context::initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool)
{
	this->api_type = api_type;

	::framework::platform::window_create_info window_create_info;
	window_create_info.x = 0;
	window_create_info.y = 0;
	window_create_info.width = this->width;
	window_create_info.height = this->height;
	window_create_info.border_width = 0;
	window_create_info.full_screen = false;
	::framework::engine_core::connection->create_window(&window_create_info, &this->window);
	
	if (window_create_info.full_screen)
	{
		::framework::engine_core::connection->set_process_dpi_awareness();
	}

	this->width = this->window->get_width();
	this->height = this->window->get_height();

	::std::vector<const char *> enabled_layer_names =
	{
#ifdef _DEBUG
		::framework::gpu::layer_standard_validation_layer_name
#endif
	};

	::std::vector<const char *> enabled_extension_names =
	{
		::framework::gpu::surface_extension_name,
#if defined(_WIN32)
		::framework::gpu::win32_surface_extension_name,
#elif defined(__linux__)
		::framework::gpu::xcb_surface_extension_name,
#endif
#ifdef _DEBUG
		::framework::gpu::debug_utils_extension_name,
		//::framework::gpu::validation_features_extension_name
#endif
	};

	::framework::gpu::application_info application_info;
	application_info.application_name = "test app";
	application_info.application_version = 1;
	application_info.engine_name = "engine";
	application_info.engine_version = 1;
	application_info.api_version = 1;

	::framework::gpu::instance_create_info instance_create_info;
	instance_create_info.application_info = &application_info;
	instance_create_info.enabled_layer_count = static_cast<::std::uint32_t>(enabled_layer_names.size());
	instance_create_info.enabled_layer_names = enabled_layer_names.data();
	instance_create_info.enabled_extension_count = static_cast<::std::uint32_t>(enabled_extension_names.size());
	instance_create_info.enabled_extension_names = enabled_extension_names.data();
	::framework::gpu::instance::create_instance(api_type, &instance_create_info, nullptr, &this->instance);

	::framework::gpu::surface_create_info surface_create_info;
	surface_create_info.window = this->window;
	this->instance->create_surface(&surface_create_info, &surface);

	this->instance->enumerate_physical_devices(&this->physical_device_count, nullptr);
	assert(this->physical_device_count > 0);

	this->physical_devices = new ::framework::gpu::physical_device * [this->physical_device_count];
	this->instance->enumerate_physical_devices(&this->physical_device_count, this->physical_devices);

	::std::uint32_t physical_device_index;
	if (auto found_physical_device = ::std::find_if(this->physical_devices, this->physical_devices + this->physical_device_count,
		[](::framework::gpu::physical_device * physical_device)
		{
			::framework::gpu::physical_device_properties properties;
			physical_device->get_properties(&properties);			
			return properties.device_type == ::framework::gpu::physical_device_type::discrete_gpu;
		}); found_physical_device != this->physical_devices + this->physical_device_count)
	{
		physical_device_index = ::std::distance(this->physical_devices, found_physical_device);
	}
	else
	{
		physical_device_index = 0;
	}
	::framework::gpu::physical_device * physical_device = this->physical_devices[physical_device_index];

	::std::uint32_t queue_family_property_count;
	physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	physical_device->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	// Iterate over each queue to learn whether it supports presenting:
	// Find a queue with present support
	// Will be used to present the swap chain images to the windowing system
	::std::vector<::framework::gpu::bool32_t> supports_present(queue_family_property_count);
	for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
	{
		physical_device->get_surface_support(i, surface, &supports_present[i]);
	}

	struct
	{
		::std::uint32_t graphics;
		::std::uint32_t compute;
		::std::uint32_t transfer;
	}
	queue_family_indices
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	float const default_queue_priority(0.0f);
	::std::vector<::framework::gpu::device_queue_create_info> device_queue_create_infos;

	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = queue_family_indices.graphics;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &default_queue_priority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	if (queue_family_indices.compute != queue_family_indices.graphics)
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = queue_family_indices.compute;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &default_queue_priority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	if ((queue_family_indices.transfer != queue_family_indices.graphics) && (queue_family_indices.transfer != queue_family_indices.compute))
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = queue_family_indices.transfer;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &default_queue_priority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}
	::framework::gpu::physical_device_features physical_device_features;
	physical_device->get_features(&physical_device_features);

	::framework::gpu::device * device;

	::std::vector<const char *> enabled_device_extension_names =
	{
		::framework::gpu::cooperative_matrix_extension_name
	};

	::framework::gpu::device_create_info device_create_info;
	device_create_info.flags = ::framework::gpu::device_create_flags::none;
	device_create_info.device_idx = 0;
	device_create_info.queue_create_info_count = static_cast<::std::uint32_t>(device_queue_create_infos.size());
	device_create_info.queue_create_infos = device_queue_create_infos.data();
	device_create_info.enabled_layer_count = 0;
	device_create_info.enabled_layer_names = nullptr;
	device_create_info.enabled_extension_count = static_cast<::std::uint32_t>(enabled_device_extension_names.size());
	device_create_info.enabled_extension_names = enabled_device_extension_names.data();
	device_create_info.enabled_features = &physical_device_features;
	assert_framework_gpu_result(physical_device->create_device(&device_create_info, nullptr, &device));

	::framework::gpu_context_create_info gpu_context_create_info;
	gpu_context_create_info.physical_device = physical_device;
	gpu_context_create_info.device = device;
	gpu_context_create_info.graphics_queue_family_index = queue_family_indices.graphics;
	gpu_context_create_info.compute_queue_family_index = queue_family_indices.compute;
	gpu_context_create_info.transfer_queue_family_index = queue_family_indices.transfer;
	this->gpu_context = ::std::make_unique<::framework::gpu_context>(gpu_context_create_info);
	this->linear_allocator_page_manager = ::std::make_unique<::framework::linear_allocator_page_manager>(this->gpu_context.get(), ::framework::linear_allocation_page::type::cpu_write);
	this->command_buffer_manager = ::std::make_unique<::framework::command_buffer_manager>(this->gpu_context.get(), this->linear_allocator_page_manager.get(), static_thread_pool);

	this->create_swap_chain();
}

void ::framework::graphics::renderer_context::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->width = width;
	this->height = height;

	::framework::coroutine::sync_wait(this->command_buffer_manager->wait_idle());

	this->create_swap_chain();
}

void ::framework::graphics::renderer_context::create_swap_chain()
{
	::framework::gpu::swap_chain * old_swap_chain = this->swap_chain;

	::framework::gpu::surface_capabilities surface_capabilities;
	assert_framework_gpu_result(this->gpu_context->get_physical_device()->get_surface_capabilities(this->surface, &surface_capabilities));

	this->width = surface_capabilities.current_extent.width;
	this->height = surface_capabilities.current_extent.height;

	::std::uint32_t surface_format_count;
	::std::vector<::framework::gpu::surface_format> surface_formats;
	assert_framework_gpu_result(this->gpu_context->get_physical_device()->get_surface_formats(this->surface, &surface_format_count, nullptr));

	surface_formats.resize(surface_format_count);
	assert_framework_gpu_result(this->gpu_context->get_physical_device()->get_surface_formats(this->surface, &surface_format_count, surface_formats.data()));

	bool found_b8g8r8a8_unorm = false;
	for (auto && surface_format : surface_formats)
	{
		if (surface_format.format == ::framework::gpu::format::b8g8r8a8_unorm)
		{
			this->swap_chain_format = surface_format.format;
			this->swap_chain_color_space = surface_format.color_space;
			found_b8g8r8a8_unorm = true;
			break;
		}
	}

	if (!found_b8g8r8a8_unorm)
	{
		this->swap_chain_format = surface_formats[0].format;
		this->swap_chain_color_space = surface_formats[0].color_space;
	}

	::framework::gpu::swap_chain_create_info swap_chain_create_info;
	swap_chain_create_info.flags = ::framework::gpu::swap_chain_create_flags::none;
	swap_chain_create_info.surface = this->surface;
	swap_chain_create_info.min_image_count = 3;
	swap_chain_create_info.image_format = this->swap_chain_format;
	swap_chain_create_info.image_color_space = this->swap_chain_color_space;
	swap_chain_create_info.image_extent = { this->width, this->height };
	swap_chain_create_info.image_array_layers = 1;
	swap_chain_create_info.image_usage = ::framework::gpu::image_usage_flags::transfer_dst_bit | ::framework::gpu::image_usage_flags::sampled_bit; // todo: sampled_bit and image view are unused so far, remove it
	swap_chain_create_info.image_sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	swap_chain_create_info.queue_family_index_count = 0;
	swap_chain_create_info.queue_family_indices = nullptr;
	swap_chain_create_info.pre_transform = surface_capabilities.current_transform;
	swap_chain_create_info.composite_alpha = ::framework::gpu::composite_alpha_flags::opaque_bit;
	swap_chain_create_info.present_mode = ::framework::gpu::present_mode::fifo;
	swap_chain_create_info.clipped = true;
	swap_chain_create_info.old_swap_chain = old_swap_chain;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_swap_chain(&swap_chain_create_info, nullptr, &this->swap_chain));

	if (old_swap_chain)
	{
		for (::std::size_t i = 0; i < this->swap_chain_images.size(); i++)
		{
			this->gpu_context->get_device()->destroy_image_view(this->swap_chain_image_views[i], nullptr);
		}
		this->gpu_context->get_device()->destroy_swap_chain(old_swap_chain, nullptr);
	}

	::std::uint32_t swap_chain_image_count;
	this->gpu_context->get_device()->get_swap_chain_images(this->swap_chain, &swap_chain_image_count, nullptr);

	this->swap_chain_images.resize(swap_chain_image_count);
	this->gpu_context->get_device()->get_swap_chain_images(this->swap_chain, &swap_chain_image_count, this->swap_chain_images.data());

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = this->swap_chain_format;
	image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
	image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
	image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
	image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;

	this->swap_chain_image_views.resize(this->swap_chain_images.size());
	for (::std::uint32_t i = 0; i < this->swap_chain_images.size(); ++i)
	{
		image_view_create_info.image = this->swap_chain_images[i];
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->swap_chain_image_views[i]));
	}
}