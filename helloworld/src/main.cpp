#include <resource/resource_manager.hpp>
#include <input/my_input_listner.hpp>
#include <engine_core.h>
#include "physics/color_space.hpp"
#include "acp/ecs/scene.hpp"
#include "acp/ecs/node.hpp"
#include "coroutine/shared_task.hpp"
#include "coroutine/sync_wait.hpp"
#include "deferred_renderer.hpp"
#include "ray_tracing_renderer.hpp"
#include "graphics/deferred_shading/scene.hpp"
#include "graphics/text_renderer.hpp"
#include "graphics/renderer_2d.hpp"
#include "gpu/core.hpp"
#include "gpu/utility.hpp"
#include "gpu_log.hpp"
#include "camera/scene_view_controller.h"
#include "test/test_base.hpp"
#include "test/neural_network.hpp"
#include "test/normalizing_flow.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <memory>
#include <locale>
#include <codecvt>
#include <string>
#include <utility>

class my_application final : public ::framework::application
{
public:
	my_application();

	void startup(void) override;

	void cleanup(void) override;

	bool is_done(void) override;

	void update(float deltaT) override;

private:
	void load_scene_async();

	::framework::coroutine::thread_pool_task<void> load_scene_sync(::framework::coroutine::static_thread_pool & thread_pool, ::std::string file_name);

	void on_click();

	::std::unique_ptr<::framework::resource::resource_manager> resource_manager;
	::framework::coroutine::static_thread_pool thread_pool;
	::framework::graphics::scene * scene;
	::framework::graphics::point_light * point_light;
	::framework::graphics::image_infinite_light * image_infinite_light;
	::glm::vec3 point_light_position;
	float point_light_scale;
	float image_infinite_light_scale;
	::framework::graphics::image * image_infinite_light_image;
	enum
	{
		snap_to_view,
		static_position,
		animation
	} light_behavior;
	::framework::graphics::ray_tracing_renderer graphics_core;
	//::framework::graphics::deferred_renderer graphics_core;
	::framework::camera::scene_view_controller * scene_view_controller;
	double time;
	::std::unique_ptr<::framework::acp::ecs::scene> entity_node0;
	::framework::coroutine::thread_pool_task<void> load_scene_coroutine;
	::framework::gpu::buffer * aov_readback_buffer;
	::framework::gpu::device_memory * aov_readback_device_memory;
	void * aov_readback_mapped_data;
};

CREATE_APPLICATION(::my_application)

::my_application::my_application() :
	thread_pool(),
	time(0.0),
	point_light_position(3.f, -3.f, 3.f),
	point_light_scale(5.0f),
	image_infinite_light_scale(4e-3f),
	light_behavior(static_position)
{
}

void ::my_application::startup(void)
{
	int device_type_index;
	::std::cout << "Choose graphics API type:\n0 - Vulkan;\n1 - DirectX 12;\n";
	::std::cin >> device_type_index;
	::framework::gpu::api_type api_type;
	::std::vector<::framework::resource::shader_language> frontend_shader_language_preferences;
	::framework::resource::resource_manager_create_info resource_manager_create_info;
	switch (device_type_index)
	{
	case 0:
		api_type = ::framework::gpu::api_type::vulkan;
		frontend_shader_language_preferences =
		{
			::framework::resource::shader_language::slang,
			::framework::resource::shader_language::glsl,
			::framework::resource::shader_language::hlsl
		};
		resource_manager_create_info.shader_language = ::framework::resource::shader_language::spirv_1_5;
		resource_manager_create_info.shader_environment = ::framework::resource::shader_environment::vulkan_1_3;
		::std::cout << "Vulkan api has been chosen" << ::std::endl << "Glslang shader compiler is using" << ::std::endl;
		break;
	case 1:
		api_type = ::framework::gpu::api_type::d3d12;
		frontend_shader_language_preferences =
		{
			::framework::resource::shader_language::slang,
			::framework::resource::shader_language::hlsl
		};
		resource_manager_create_info.shader_language = ::framework::resource::shader_language::dxil;
		resource_manager_create_info.shader_environment = ::framework::resource::shader_environment::d3d12;
		::std::cout << "DirectX 12 api has been chosen" << ::std::endl << "Dxc shader compiler is using" << ::std::endl;
		break;
	default:
		api_type = ::framework::gpu::api_type::vulkan;
		::std::cout << "Index out of range\n";
		::std::cout << "Vulkan api has been chosen by default\n";
		break;
	}
	resource_manager_create_info.frontend_shader_language_preference_count = static_cast<::std::uint32_t>(frontend_shader_language_preferences.size());
	resource_manager_create_info.frontend_shader_language_preferences = frontend_shader_language_preferences.data();
	this->resource_manager = ::std::make_unique<::framework::resource::resource_manager>(resource_manager_create_info);

	resource_manager->add_search_directory("./"); // root directory to obtain kernels from sources
	resource_manager->add_search_directory("../"); // root directory to obtain kernels from sources
	resource_manager->add_search_directory("../data/shaders/helloworld/");
	resource_manager->add_search_directory("../data/shaders/helloworld/glsl/spv/");
	resource_manager->add_search_directory("../core/src/");
	resource_manager->add_search_directory("../core/src/kernels/slang/"); // todo: order is matter because slang compiler will prioritize e.g. hlsl directory, trying to load shaders with the same name from it
	resource_manager->add_search_directory("../core/src/kernels/hlsl/");
	resource_manager->add_search_directory("../core/src/kernels/glsl/");
	resource_manager->add_search_directory("../helloworld/src/");
	resource_manager->add_search_directory("../helloworld/src/kernels/hlsl/");
	resource_manager->add_search_directory("../helloworld/src/kernels/glsl/");
	resource_manager->add_search_directory("../helloworld/src/kernels/slang/");
	resource_manager->add_search_directory("../data/shaders/");
	resource_manager->add_search_directory("../data/models/");
	resource_manager->add_search_directory("../data/models/Bistro_v5_2/");
	resource_manager->add_search_directory("../data/models/Bistro_v5_2/Textures/");
	resource_manager->add_search_directory("../data/scenes/");
	resource_manager->add_search_directory("../data/sounds/");
	resource_manager->add_search_directory("../data/textures/");
	resource_manager->add_search_directory("../data/levels/");

	this->graphics_core.initialize(api_type, &this->thread_pool, *this->resource_manager);

	if (this->graphics_core.get_gpu_context()->get_physical_device_features().cooperative_matrix && false)
	{
		bool result;
		if (false)
		{
			::framework::test::neural_network_with_grid_encoding neural_network_test(this->graphics_core.get_gpu_context(), this->resource_manager.get(), this->graphics_core.get_command_buffer_manager(), this->graphics_core.get_profiler(), this->graphics_core.get_gpu_log());
			result = neural_network_test.test_fully_fused_mlp(false);
			assert(result);
			result = neural_network_test.test_fully_fused_mlp(true);
			assert(result);
		}
		if (true)
		{
			::framework::test::normalizing_flow_test normalizing_flow_test(this->graphics_core.get_gpu_context(), this->resource_manager.get(), this->graphics_core.get_command_buffer_manager(), this->graphics_core.get_profiler(), this->graphics_core.get_gpu_log());
			result = normalizing_flow_test.test_normalizing_flow(false, true);
			assert(result);
			result = normalizing_flow_test.test_normalizing_flow(true, true);
			assert(result);
		}
		if (false)
		{
			::framework::test::test_base tests(this->graphics_core.get_gpu_context(), this->resource_manager.get(), this->graphics_core.get_command_buffer_manager(), this->graphics_core.get_profiler(), this->graphics_core.get_gpu_log());
			//result = tests.test_mma(true);
			//assert(result);
			result = tests.test_gemm(false, false, false);
			assert(result);
			result = tests.test_gemm(true, false, false);
			assert(result);
			//result = tests.test_scan(true);
			//assert(result);
			//result = tests.test_shuffle(true);
			//assert(result);
			//result = tests.test_mlp();
			//assert(result);
		}
		::std::exit(0);
	}

	::framework::graphics::scene_create_info scene_create_info;
	this->scene = this->graphics_core.get_scene_renderer()->create_scene(&scene_create_info);
	this->point_light = this->scene->create_point_light();
	//::framework::physics::rgb_unbounded_spectrum spectrum(*::framework::physics::rgb_color_space::srgb, ::glm::vec3(0.0f, 0.0f, 10.0f));
	//::framework::physics::rgb_illuminant_spectrum spectrum(*::framework::physics::rgb_color_space::srgb, ::glm::vec3(0.0f, 1.0f, 1.0f));
	::framework::physics::blackbody_spectrum spectrum(5500.0f);
	this->point_light->set_intensity(spectrum);
	this->point_light->set_scale(this->point_light_scale);

	// Load image_infinite_light image
	auto load_image_infinite_light_image = [&](::framework::coroutine::static_thread_pool & thread_pool) -> ::framework::coroutine::thread_pool_task<::framework::graphics::image *>
	{
		struct texture_load_callback : public ::framework::resource::texture_load_callback
		{
			explicit texture_load_callback(::framework::graphics::scene_renderer * scene_renderer) :
				scene_renderer(scene_renderer)
			{
			}

			::framework::coroutine::lazy_task<void> operator()(::framework::resource::texture_load_callback::texture_load_info const & texture_load_info) override
			{
				::std::vector<::framework::graphics::texture_subresource_info> subresource_infos(texture_load_info.subresource_count);
				for (::std::uint32_t i = 0; i < texture_load_info.subresource_count; i++)
				{
					subresource_infos[i].data = texture_load_info.subresource_data[i].data;
					subresource_infos[i].row_pitch = texture_load_info.subresource_data[i].row_pitch;
					subresource_infos[i].slice_pitch = texture_load_info.subresource_data[i].slice_pitch;
					subresource_infos[i].mip_level = texture_load_info.subresource_data[i].mip_level;
					subresource_infos[i].array_layer = texture_load_info.subresource_data[i].array_layer;
				}

				::framework::graphics::image_create_info image_create_info;
				image_create_info.width = texture_load_info.width;
				image_create_info.height = texture_load_info.height;
				image_create_info.format = texture_load_info.format;
				image_create_info.mip_levels = texture_load_info.mip_levels;
				image_create_info.subresource_count = texture_load_info.subresource_count;
				image_create_info.subresource_infos = subresource_infos.data();
				this->image = this->scene_renderer->create_image(image_create_info);
				co_await this->image->get_initialization_shared_task();
			};

			::framework::graphics::scene_renderer * scene_renderer;
			::framework::graphics::image * image;
		};

		struct texture_load_callback texture_load_callback_instance(this->graphics_core.get_scene_renderer());

		::framework::resource::texture_load_info texture_load_info;
		texture_load_info.file_name = "kloppenheim_06_puresky_4k.hdr";
		texture_load_info.texture_load_callback = &texture_load_callback_instance;
		co_await this->resource_manager->load_texture(texture_load_info);
		co_return texture_load_callback_instance.image;
	} (this->thread_pool);
	this->image_infinite_light_image = ::framework::coroutine::sync_wait(load_image_infinite_light_image);
	this->image_infinite_light = this->scene->create_image_infinite_light();
	this->image_infinite_light->set_scale(this->image_infinite_light_scale);
	this->image_infinite_light->set_image(this->image_infinite_light_image);
	::framework::graphics::medium * medium = this->graphics_core.get_scene_renderer()->create_medium();
	::framework::coroutine::sync_wait(medium->get_initialization_shared_task());
	this->graphics_core.get_scene_renderer()->set_scene(this->scene);
	this->scene_view_controller = new ::framework::camera::scene_view_controller(this->graphics_core.get_scene_view(), ::framework::engine_core::input_listner);

	if (true)
	{
		this->load_scene_coroutine = this->load_scene_sync(this->thread_pool, "Bistro_v5_2/BistroExterior.fbx");
	}
	else
	{
		this->load_scene_async();

		::std::function<void(::framework::acp::ecs::node * node)> scale_scene_geometry = [&scale_scene_geometry](::framework::acp::ecs::node * node) -> void
		{
			::framework::graphics::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry = node->get_scene_geometry();
			if (triangle_mesh_scene_geometry)
			{
				::glm::mat4 transform = node->get_transform();
				transform = ::glm::scale(transform, ::glm::vec3(0.25f));
				triangle_mesh_scene_geometry->set_transform(transform);
			}

			for (auto & node1 : node->get_child_nodes())
			{
				scale_scene_geometry(node1);
			}
		};
		scale_scene_geometry(this->entity_node0->get_root_node());
	}

	::framework::graphics::button_info button_info
	{
		-0.85f,
		0.95f,
		0.0f,
		0.0f,
		"Load Scene",
		[&]
		{
			::std::wstring file_path;
			::framework::engine_core::connection->open_file_dialog(file_path);
			this->load_scene_coroutine = this->load_scene_sync(this->thread_pool, ::std::wstring_convert<::std::codecvt_utf8<wchar_t>>().to_bytes(file_path));
		}
	};

	this->graphics_core.get_ui()->add_button(button_info);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->graphics_core.get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->graphics_core.get_gpu_context()->get_device()->create_buffer(&buffer_create_info, nullptr, &this->aov_readback_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->aov_readback_buffer;
		this->graphics_core.get_gpu_context()->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->graphics_core.get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->aov_readback_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->graphics_core.get_gpu_context()->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->aov_readback_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->aov_readback_buffer;
		bind_buffer_memory_info.memory = this->aov_readback_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->graphics_core.get_gpu_context()->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->graphics_core.get_gpu_context()->get_device()->map_memory(this->aov_readback_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->aov_readback_mapped_data));
	}
}

void ::my_application::cleanup(void)
{
	this->graphics_core.get_gpu_context()->get_device()->destroy_buffer(this->aov_readback_buffer, nullptr);
	this->graphics_core.get_gpu_context()->get_device()->free_memory(this->aov_readback_device_memory, nullptr);
}

bool ::my_application::is_done(void)
{
	return !::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_escape);
}

void ::my_application::update(float delta_time)
{
	this->time += delta_time;

	this->scene_view_controller->update(delta_time);

	if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_l))
	{
		this->light_behavior = static_cast<decltype(this->light_behavior)>((this->light_behavior + 1) % 3);

		if (this->light_behavior == ::my_application::static_position)
		{
			this->point_light_position = this->graphics_core.get_scene_view()->get_eye();
		}
	}

	if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_minus))
	{
		this->point_light_scale /= 1.25f;
		this->point_light->set_scale(this->point_light_scale);
	}

	if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_equals))
	{
		this->point_light_scale *= 1.25f;
		this->point_light->set_scale(this->point_light_scale);
	}

	if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_left_bracket))
	{
		this->image_infinite_light_scale /= 1.25f;
		this->image_infinite_light->set_scale(this->image_infinite_light_scale);
	}

	if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_right_bracket))
	{
		this->image_infinite_light_scale *= 1.25f;
		this->image_infinite_light->set_scale(this->image_infinite_light_scale);
	}

	switch (this->light_behavior)
	{
	case ::my_application::snap_to_view:
		this->point_light_position = this->graphics_core.get_scene_view()->get_eye();
		break;
	case ::my_application::animation:
		this->point_light_position.y = ::std::sin(this->time) * 10.0f;
		break;
	default:
		break;
	}

	this->point_light->set_position(this->point_light_position);

	this->graphics_core.update(delta_time);

	if (::framework::engine_core::input_listner->is_button_first_pressed(::framework::input::button::button_2))
	{
		this->on_click();
	}
	if (::framework::engine_core::input_listner->get_button_state(::framework::input::button::button_1))
	{
		::std::int16_t mouse_x;
		::std::int16_t mouse_y;
		::framework::engine_core::input_listner->get_mouse_position(mouse_x, mouse_y);
		static_cast<::framework::graphics::ray_tracing::scene_renderer *>(this->graphics_core.get_scene_renderer())->set_nis_visualization_screen_coordinates(::glm::uvec2(mouse_x, mouse_y));
	}
	else
	{
		static_cast<::framework::graphics::ray_tracing::scene_renderer *>(this->graphics_core.get_scene_renderer())->set_nis_visualization_screen_coordinates(::glm::uvec2(-1));
	}
}

void ::my_application::load_scene_async()
{
	this->entity_node0 = ::std::make_unique<::framework::acp::ecs::scene>(*this->scene, *this->graphics_core.get_scene_renderer(), *this->resource_manager, this->thread_pool);

	//this->resource_manager->load_fbx_scene(this->entity_node0.get(), "head.fbx");
	this->resource_manager->load_fbx_scene(this->entity_node0.get(), "dragon.fbx", &this->thread_pool);
	//this->resource_manager->load_fbx_scene(entity_node1.get(), "CornellBox-Water.fbx");
	//this->resource_manager->load_fbx_scene(entity_node1.get(), "cornell_box.fbx");
	//this->resource_manager->load_fbx_scene(this->entity_node0.get(), "sponza.fbx");
	//this->resource_manager->load_fbx_scene(this->entity_node0.get(), "Bistro_v5_2/BistroInterior.fbx");
	//this->resource_manager->load_fbx_scene(entity_node2.get(), "Bistro_v5_2/BistroInterior_Wine.fbx");
	//this->resource_manager->load_fbx_scene(entity_node3.get(), "Bistro_v5_2/BistroExterior.fbx");
}

::framework::coroutine::thread_pool_task<void>(::my_application::load_scene_sync)(::framework::coroutine::static_thread_pool & thread_pool, ::std::string file_name)
{
	this->entity_node0 = ::std::make_unique<::framework::acp::ecs::scene>(*this->scene, *this->graphics_core.get_scene_renderer(), *this->resource_manager, this->thread_pool);
	this->resource_manager->load_fbx_scene(this->entity_node0.get(), file_name.c_str(), &thread_pool);

	co_return;
}

void ::my_application::on_click()
{
	::std::int16_t mouse_x;
	::std::int16_t mouse_y;
	::framework::engine_core::input_listner->get_mouse_position(mouse_x, mouse_y);

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		::framework::command_context * const graphics_command_context = co_await this->graphics_core.get_command_buffer_manager()->get_graphics_queue()->acquire_command_context();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

		::framework::gpu::buffer_image_copy buffer_image_copy;
		buffer_image_copy.buffer_offset = 0;
		buffer_image_copy.buffer_row_length = 4;
		buffer_image_copy.buffer_image_height = 1;
		buffer_image_copy.image_subresource = { ::framework::gpu::image_aspect_flags::color_bit, 0, 0, 1 };
		buffer_image_copy.image_offset = { mouse_x, mouse_y, 0 };
		buffer_image_copy.image_extent = { 1, 1, 1 };

		::framework::gpu::copy_image_to_buffer_info copy_image_to_buffer_info;
		copy_image_to_buffer_info.src_image = this->graphics_core.get_aov()->get_image(::framework::graphics::aov::type::geometry_instance_id);
		copy_image_to_buffer_info.src_image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
		copy_image_to_buffer_info.dst_buffer = this->aov_readback_buffer;
		copy_image_to_buffer_info.region_count = 1;
		copy_image_to_buffer_info.regions = &buffer_image_copy;
		graphics_command_context->get_command_buffer()->copy_image_to_buffer(&copy_image_to_buffer_info);

		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = this->graphics_core.get_scene_renderer()->get_frame_semaphore();
		wait_semaphore_info.value = this->graphics_core.get_scene_renderer()->get_last_frame_index();
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit; // todo
		wait_semaphore_info.device_index = 0;

		co_await this->graphics_core.get_command_buffer_manager()->get_graphics_queue()->submit_and_wait(graphics_command_context, 1, &wait_semaphore_info, 0, nullptr);
		co_await this->graphics_core.get_command_buffer_manager()->get_graphics_queue()->release_command_context(graphics_command_context);
	}());

	::framework::gpu::mapped_memory_range mapped_memory_range;
	mapped_memory_range.memory = this->aov_readback_device_memory;
	mapped_memory_range.offset = 0;
	mapped_memory_range.size = sizeof(::std::uint32_t);
	assert_framework_gpu_result(this->graphics_core.get_gpu_context()->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));
	::std::uint32_t const geometry_id = *static_cast<::std::uint32_t *>(this->aov_readback_mapped_data);

	::std::function<::std::optional<::std::pair<::framework::graphics::triangle_mesh_scene_geometry *, ::std::uint32_t>>(::framework::acp::ecs::node * node)> find_geometry;
	find_geometry = [&](::framework::acp::ecs::node * node) -> ::std::optional<::std::pair<::framework::graphics::triangle_mesh_scene_geometry *, ::std::uint32_t>>
	{
		if (!node)
		{
			return ::std::nullopt;
		}

		::framework::graphics::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry = node->get_scene_geometry();
		if (triangle_mesh_scene_geometry)
		{
			::framework::graphics::geometry * const geometry = triangle_mesh_scene_geometry->get_geometry();
			::std::uint32_t const geometry_count = geometry->get_geometry_count();
			for (::std::uint32_t i = 0; i < geometry_count; i++)
			{
				::framework::graphics::geometry_instance_id const geometry_instance_id = triangle_mesh_scene_geometry->get_geometry_instance_id(i);
				if (geometry_instance_id.index == geometry_id)
				{
					return ::std::optional<::std::pair<::framework::graphics::triangle_mesh_scene_geometry *, ::std::uint32_t>>(::std::make_pair(triangle_mesh_scene_geometry, i));
				}
			}
		}

		for (auto & node1 : node->get_child_nodes())
		{
			::std::optional<::std::pair<::framework::graphics::triangle_mesh_scene_geometry *, ::std::uint32_t>> const result = find_geometry(node1);
			if (result)
			{
				return result;
			}
		}

		return ::std::nullopt;
	};

	::std::optional<::std::pair<::framework::graphics::triangle_mesh_scene_geometry *, ::std::uint32_t>> const result = find_geometry(this->entity_node0->get_root_node());
	if (result)
	{
		::framework::graphics::material * material = result->first->get_material(result->second);
		material->set_emissive_image(::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_left_shift) ? nullptr : this->image_infinite_light_image);
		//material->set_roughness(::glm::vec2(1.0f));
	}
}
