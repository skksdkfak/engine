struct vs_input_t
{
	float2 position : LOCATION0;
};

struct vs_output_t
{
	float4 position : SV_POSITION;
};

[shader("vertex")]
vs_output_t main(vs_input_t input, uint VertexID : SV_VertexID)
{
	vs_output_t output;

	output.position = float4(input.position * 2.0f - 1.0f, 0.0, 1.0);

	return output;
}
