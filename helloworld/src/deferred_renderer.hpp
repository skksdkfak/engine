#pragma once

#include "graphics/scene_renderer.hpp"
#include "graphics/text_renderer.hpp"
#include "graphics/renderer_2d.hpp"
#include "renderer_context.hpp"
#include "memory/linear_allocator.hpp"
#include <engine_core.h>

namespace framework::coroutine
{
	class static_thread_pool;
}

namespace framework::graphics::deferred_shading
{
	class scene_renderer;
}

namespace framework::graphics
{
	class deferred_renderer : public renderer_context
	{
	public:
		deferred_renderer();
		virtual ~deferred_renderer();

		void initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool, typename ::framework::resource::resource_manager & resource_manager);

		void destroy();

		virtual void resize(::std::uint32_t width, ::std::uint32_t height);

		void update(double deltaT);

		::framework::graphics::scene_renderer * get_scene_renderer();

		::framework::graphics::text_renderer * get_text_renderer() { return this->text_renderer; }

		::framework::graphics::renderer_2d * get_renderer_2d() { return this->renderer_2d; }

		::framework::graphics::scene_view * get_scene_view() const { return this->scene_view; }

	private:
		::glm::vec4 m_lightPos;
		::framework::graphics::scene * scene;
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;
		::framework::gpu::pipeline_cache * pipeline_cache;
		struct
		{
			::framework::gpu::command_buffer * command_buffer;
			::framework::gpu::semaphore * deferred_render_complete_semaphore;
			::framework::gpu::semaphore * text_render_complete_semaphore;
			::framework::gpu::image * render_target_image;
			::framework::gpu::device_memory * render_target_device_memory;
			::framework::gpu::image_view * render_target_image_view;
		} frame_resources[3];
		::framework::gpu::semaphore * present_complete_semaphore;
		::framework::gpu::command_pool * graphics_queue_command_pool;
		::framework::gpu::command_pool * transfer_queue_command_pool;
		::framework::graphics::deferred_shading::scene_renderer * scene_renderer;
		::framework::graphics::text_renderer * text_renderer;
		::framework::graphics::renderer_2d * renderer_2d;
		::framework::graphics::scene_view * scene_view;
		::framework::memory::linear_allocator single_frame_allocator;
		::framework::coroutine::static_thread_pool * thread_pool;
		::std::uint32_t frame_index;
		::std::uint32_t buffered_frame_count;
	};
}