#pragma once

#include "algorithm/xavier_uniform.hpp"
#include "command_context.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "linalg.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "nn/losses/cross_entropy.hpp"
#include "nn/losses/relative_l2.hpp"
#include "nn/mlp/network.hpp"
#include "nn/optimizers/adam.hpp"
#include "nn/optimizers/sgd.hpp"
#include "nn/grid/encoding.hpp"
#include "nn/one_blob/encoding.hpp"
#include "profiler.hpp"
#include "resource/resource_manager.hpp"
#include "test/test_base.hpp"

namespace framework::test
{
	class neural_network : public ::framework::test::test_base
	{
	public:
		neural_network(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log, ::std::uint32_t encoded_input_size);

		~neural_network();

		bool test_fully_fused_mlp(bool indirect);

	protected:
		virtual ::framework::coroutine::immediate_task<void> inference(::framework::command_context * const graphics_command_context, bool indirect);

		virtual ::framework::coroutine::immediate_task<void> forward(::framework::command_context * const graphics_command_context, bool indirect);

		virtual ::framework::coroutine::immediate_task<void> backward(::framework::command_context * const graphics_command_context, bool indirect);

		virtual ::framework::coroutine::immediate_task<void> optimize_step(::framework::command_context * const graphics_command_context, bool indirect);

		virtual ::framework::coroutine::immediate_task<void> initialize_device_resources(::framework::command_context * const graphics_command_context);

		struct ::framework::test::test_base::image_dataset image_dataset;
		::framework::linalg::matrix_layout const input_layout = ::framework::linalg::matrix_layout::column_major;
		::framework::linalg::matrix_layout const network_input_layout = ::framework::linalg::matrix_layout::row_major;
		::framework::linalg::matrix_layout const output_layout = ::framework::linalg::matrix_layout::column_major;
		::framework::gpu::component_type const component_type = ::framework::gpu::component_type::float16;
		::framework::gpu::component_type const pdf_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type const target_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type const accumulator_type = ::framework::gpu::component_type::float32;
		float const learning_rate = 0.01f;
		::std::uint32_t const batch_alignment = 256u;
		::std::uint32_t const inference_batch_alignment = 256u;
		::std::uint32_t const batch_size = 1u << 15;
		::std::uint32_t const inference_batch_size = 512u * 512u;
		::std::uint32_t const one_blob_encoding_bins = 32;
		::std::uint32_t const input_size = 2;
		::std::uint32_t const output_size = 3;
		::std::uint32_t const padded_input_size = 2u;
		::std::uint32_t const padded_output_size = 16u;
		::std::uint32_t const hidden_width = 64;
		::std::uint32_t const hidden_layer_count = 1;
		::std::uint32_t const channel_count = 3ul;
		::std::uint32_t const channel_stride = 3ul;
		::framework::gpu::device_size batch_size_buffer_alignment;
		::framework::gpu::device_size dispatch_indirect_command_aligned_offset;
		bool const indirect = true;
		::std::unique_ptr<::framework::algorithm::xavier_uniform> xavier_uniform;
		::std::unique_ptr<::framework::nn::fully_fused_mlp::execution_policy> neural_network_execution_policy;
		::std::unique_ptr<::framework::nn::fully_fused_mlp::network> network;
		::std::unique_ptr<::framework::nn::adam> optimizer;
		::std::unique_ptr<::framework::nn::relative_l2> loss;
		::framework::gpu::descriptor_set_layout * evaluate_image_descriptor_set_layout;
		::framework::gpu::pipeline_layout * evaluate_image_pipeline_layout;
		::framework::gpu::pipeline * evaluate_image_pipeline;
		::framework::gpu::pipeline * generate_test_uvs_pipeline;
		::framework::gpu::shader_module * generate_test_uvs_shader_module;
		::framework::gpu::shader_module * evaluate_image_shader_module;
		::framework::gpu::descriptor_set_layout * hdr_to_ldr_descriptor_set_layout;
		::framework::gpu::pipeline_layout * hdr_to_ldr_pipeline_layout;
		::framework::gpu::pipeline * hdr_to_ldr_pipeline;
		::framework::gpu::shader_module * hdr_to_ldr_shader_module;
		::framework::gpu::sampler * sampler;
		::framework::gpu::buffer * batch_size_buffer;
		::framework::gpu::device_memory * batch_size_device_memory;
		::framework::gpu::buffer * dispatch_indirect_command_buffer;
		::framework::gpu::device_memory * dispatch_indirect_command_device_memory;
		::framework::gpu::buffer * dloss_dy_buffer;
		::framework::gpu::device_memory * dloss_dy_device_memory;
		::framework::gpu::buffer * input_buffer;
		::framework::gpu::device_memory * input_device_memory;
		::framework::gpu::buffer * encoded_input_buffer;
		::framework::gpu::device_memory * encoded_input_device_memory;
		::framework::gpu::buffer * target_buffer;
		::framework::gpu::device_memory * target_device_memory;
		::framework::gpu::buffer * loss_buffer;
		::framework::gpu::device_memory * loss_device_memory;
		::framework::gpu::buffer * ldr_readback_buffer;
		::framework::gpu::device_memory * ldr_readback_device_memory;
		void * ldr_readback_mapped_data;
		::framework::gpu::descriptor_set * network_descriptor_set;
		::framework::gpu::descriptor_set * network_initialize_parameters_descriptor_set;
		::framework::gpu::descriptor_set * network_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * loss_descriptor_set;
		::framework::gpu::descriptor_set * optimizer_descriptor_set;
		::framework::gpu::descriptor_set * evaluate_image_descriptor_set;
		::framework::gpu::descriptor_set * hdr_to_ldr_descriptor_set;
	};

	class neural_network_with_grid_encoding : public ::framework::test::neural_network
	{
	public:
		neural_network_with_grid_encoding(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log);

		~neural_network_with_grid_encoding();

	protected:
		virtual ::framework::coroutine::immediate_task<void> inference(::framework::command_context * const graphics_command_context, bool indirect) override;

		virtual ::framework::coroutine::immediate_task<void> forward(::framework::command_context * const graphics_command_context, bool indirect) override;

		virtual ::framework::coroutine::immediate_task<void> backward(::framework::command_context * const graphics_command_context, bool indirect) override;

		virtual ::framework::coroutine::immediate_task<void> optimize_step(::framework::command_context * const graphics_command_context, bool indirect) override;

		virtual ::framework::coroutine::immediate_task<void> initialize_device_resources(::framework::command_context * const graphics_command_context) override;

		static ::framework::gpu::component_type const grid_gradient_type = ::framework::gpu::component_type::float32;
		static ::std::uint32_t const feature_count = 64u;
		static ::framework::nn::grid::grid_type const grid_type = ::framework::nn::grid::grid_type::hash;
		static ::framework::nn::grid::hash_type const hash_type = ::framework::nn::grid::hash_type::coherent_prime;
		static ::framework::nn::grid::interpolation_type const interpolation_type = ::framework::nn::grid::interpolation_type::linear;
		static ::std::uint32_t const log2_hashmap_size = 21u;
		static ::std::uint32_t const base_resolution = 16u;
		float const per_level_scale = 1.5f;
		static bool const stochastic_interpolation = false;
		static ::std::uint32_t const features_per_level = 2u;
		static ::std::uint32_t const pos_dims = 2u;
		::std::unique_ptr<::framework::nn::encoding_execution_policy> encoding_execution_policy;
		::std::unique_ptr<::framework::nn::encoding> encoding;
		::std::unique_ptr<::framework::nn::adam> encoding_optimizer;
		::framework::gpu::descriptor_set * xavier_uniform_descriptor_set;
		::framework::gpu::descriptor_set * encoding_descriptor_set;
		::framework::gpu::descriptor_set * encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * encoding_optimizer_descriptor_set;
	};
}