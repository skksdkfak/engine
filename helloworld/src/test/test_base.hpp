#pragma once

#include "command_context.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "profiler.hpp"
#include "resource/resource_manager.hpp"

namespace framework::test
{
	class test_base
	{
	public:
		test_base(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager,::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log);

		bool test_mma(bool validate);
		
		bool test_gemm(bool indirect, bool split_k, bool validate);

		bool test_scan(bool validate);

		bool test_shuffle(bool validate);

		bool test_mlp();

	protected:
		struct image_dataset
		{
			::std::uint32_t	width;
			::std::uint32_t	height;
			::framework::gpu::image * image;
			::framework::gpu::device_memory * device_memory;
			::framework::gpu::image_view * image_view;
		};

		struct ::framework::test::test_base::image_dataset load_train_dataset(char const * file_name);

		::framework::gpu_context * gpu_context;
		::framework::resource::resource_manager * resource_manager;
		::framework::command_buffer_manager * command_buffer_manager;
		::framework::profiler * profiler;
		::framework::gpu_log * gpu_log;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_set_layout * gpu_log_descriptor_set_layout;
		::framework::gpu::descriptor_set * gpu_log_descriptor_set;

	};
}