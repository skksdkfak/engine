#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "gpu/utility.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include "test/normalizing_flow.hpp"
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>
#include <algorithm>
#include <array>
#include <cinttypes>
#include <cstdio>
#include <format>
#include <iostream>
#include <numeric>
#include <random>

namespace framework::local
{
	namespace
	{
		::std::filesystem::path const tests_directory = "tests";

		struct generate_train_data_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t seed;
			::std::uint32_t pad0;
			::std::uint32_t pad1;
		};

		struct generate_forward_test_samples_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t width;
			::std::uint32_t height;
			::std::uint32_t pad0;
		};

		struct plot_pdf_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t stride;
			float exposure;
			float gamma;
		};
	}
}

::framework::test::normalizing_flow_test::normalizing_flow_test(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log) :
	::framework::test::test_base(gpu_context, resource_manager, command_buffer_manager, profiler, gpu_log)
{
	this->image_dataset = this->load_train_dataset("test0.png");

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(this->max_batch_size, this->max_inference_batch_size) * this->spatial_feature_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &features_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = features_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? features_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &features_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = features_buffer;
		bind_buffer_memory_info.memory = features_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->max_batch_size * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &target_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = target_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &target_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = target_buffer;
		bind_buffer_memory_info.memory = target_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = samples_image_width * samples_image_height * samples_channel_count * sizeof(::std::uint8_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &samples_readback_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = samples_readback_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &samples_readback_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = samples_readback_buffer;
		bind_buffer_memory_info.memory = samples_readback_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(samples_readback_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &samples_readback_mapped_data));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = pdf_image_width * pdf_image_height * pdf_channel_count * sizeof(::std::uint8_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &pdf_readback_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = pdf_readback_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &pdf_readback_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = pdf_readback_buffer;
		bind_buffer_memory_info.memory = pdf_readback_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(pdf_readback_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &pdf_readback_mapped_data));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ((sizeof(::std::uint32_t) + this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1)) * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->batch_size_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->batch_size_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->batch_size_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &batch_size_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->batch_size_buffer;
		bind_buffer_memory_info.memory = batch_size_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = sample_image_format;
		image_create_info.extent.width = samples_image_width;
		image_create_info.extent.height = samples_image_height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::sampled_bit | ::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::transfer_src_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &samples_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = samples_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.image = samples_image;
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &samples_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = samples_image;
		bind_image_memory_info.memory = samples_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = samples_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = sample_image_format;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->samples_image_view));
	}

	this->normalizing_flow = ::std::make_unique<::framework::normalizing_flows::normalizing_flow>(this->gpu_context, this->resource_manager, this->command_buffer_manager, this->profiler, this->gpu_log, this->descriptor_pool, transform_type, coupling_layer_count, spatial_feature_count, spatial_feature_stride, spatial_feature_count, spatial_feature_stride, hidden_width, hidden_layer_count, learning_rate, this->max_batch_size, this->max_inference_batch_size, batch_alignment, inference_batch_alignment, parameter_type, target_type, target_type, accumulator_type, features_buffer, features_buffer, nullptr, target_buffer, this->batch_size_buffer);

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[8];
		// x_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// features_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// log_jacobian_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// pdf_plot_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// target_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// target_image
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// samples_image
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// spatial_features_buffer
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[7].hlsl_register_space = 0;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->descriptor_set_layout,
			this->gpu_log_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = 16;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	::framework::resource::preprocessor_define const preprocessor_defines[]
	{
		{ "ELEMENT_TYPE", type_name }
	};

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "generate_nis_train_data.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		generate_train_data_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "generate_forward_test_samples.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		generate_forward_test_samples_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "generate_backward_test_samples.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		generate_backward_test_samples_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::slang_source_info slang_source_info;
		slang_source_info.file_name = "plot_pdf.comp.slang";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = &slang_source_info;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		plot_pdf_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "samples.vert.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		point_vertex_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "samples.frag.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		point_fragment_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = generate_train_data_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &generate_train_data_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = generate_forward_test_samples_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &generate_forward_test_samples_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = generate_backward_test_samples_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &generate_backward_test_samples_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = plot_pdf_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &plot_pdf_pipeline));
	}

	{
		::framework::gpu::attachment_reference color_reference;
		color_reference.attachment = 0;
		color_reference.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::subpass_description subpass_description;
		subpass_description.flags = ::framework::gpu::subpass_description_flags::none;
		subpass_description.pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
		subpass_description.input_attachment_count = 0;
		subpass_description.input_attachments = nullptr;
		subpass_description.color_attachment_count = 1;
		subpass_description.color_attachments = &color_reference;
		subpass_description.resolve_attachments = nullptr;
		subpass_description.depth_stencil_attachment = nullptr;
		subpass_description.preserve_attachment_count = 0;
		subpass_description.preserve_attachments = nullptr;

		::framework::gpu::attachment_description color_attachments_description;
		color_attachments_description.flags = ::framework::gpu::attachment_description_flags::none;
		color_attachments_description.format = sample_image_format;
		color_attachments_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		color_attachments_description.load_op = ::framework::gpu::attachment_load_op::clear;
		color_attachments_description.store_op = ::framework::gpu::attachment_store_op::store;
		color_attachments_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		color_attachments_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		color_attachments_description.initial_layout = ::framework::gpu::image_layout_flags::undefined_bit | ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;
		color_attachments_description.final_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
		render_pass_create_info.attachment_count = 1;
		render_pass_create_info.attachments = &color_attachments_description;
		render_pass_create_info.subpass_count = 1;
		render_pass_create_info.subpasses = &subpass_description;
		render_pass_create_info.dependency_count = 0;
		render_pass_create_info.dependencies = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_render_pass(&render_pass_create_info, nullptr, &draw_samples_render_pass));

		::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
		render_pass_state_create_info.render_pass = draw_samples_render_pass;
		render_pass_state_create_info.max_clear_value_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_render_pass_state(&render_pass_state_create_info, nullptr, &draw_samples_render_pass_state));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
		pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
		pipeline_shader_stage_create_infos[0].module = point_vertex_shader_module;
		pipeline_shader_stage_create_infos[0].name = "main";
		pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

		pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
		pipeline_shader_stage_create_infos[1].module = point_fragment_shader_module;
		pipeline_shader_stage_create_infos[1].name = "main";
		pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

		::framework::gpu::viewport viewport;
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = static_cast<float>(samples_image_width);
		viewport.height = static_cast<float>(samples_image_height);
		viewport.min_depth = 0.0f;
		viewport.max_depth = 1.0f;

		::framework::gpu::rect_2d scissor;
		scissor.offset.x = 0;
		scissor.offset.y = 0;
		scissor.extent.width = samples_image_width;
		scissor.extent.height = samples_image_height;

		::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
		pipeline_viewport_state_create_info.viewport_count = 1;
		pipeline_viewport_state_create_info.scissors = &scissor;
		pipeline_viewport_state_create_info.scissor_count = 1;
		pipeline_viewport_state_create_info.viewports = &viewport;

		::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
		pipeline_rasterization_state_create_info.depth_clamp_enable = false;
		pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
		pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::point;
		pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
		pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::clockwise;
		pipeline_rasterization_state_create_info.depth_bias_enable = false;
		pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
		pipeline_rasterization_state_create_info.line_width = 1.0f;

		::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
		pipeline_color_blend_attachment_state.blend_enable = true;
		pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::src_alpha;
		pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
		pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
		pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
		pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
		pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
		pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

		::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
		pipeline_color_blend_state_create_info.logic_op_enable = false;
		pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
		pipeline_color_blend_state_create_info.attachment_count = 1;
		pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
		pipeline_color_blend_state_create_info.blend_constants[0] = 0;
		pipeline_color_blend_state_create_info.blend_constants[1] = 0;
		pipeline_color_blend_state_create_info.blend_constants[2] = 0;
		pipeline_color_blend_state_create_info.blend_constants[3] = 0;

		::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[1];
		vertex_input_binding_descriptions[0].binding = 0;
		vertex_input_binding_descriptions[0].stride = 4;
		vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::instance;

		::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[1];
		// Location 0 : Position
		vertex_input_attribute_descriptions[0].location = 0;
		vertex_input_attribute_descriptions[0].binding = 0;
		vertex_input_attribute_descriptions[0].format = sample_vertex_format;
		vertex_input_attribute_descriptions[0].offset = 0;

		::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
		vertex_input_state.vertex_binding_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_binding_descriptions));
		vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
		vertex_input_state.vertex_attribute_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_attribute_descriptions));
		vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

		::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
		pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::point_list;
		pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

		::framework::gpu::pipeline_multisample_state_create_info pipeline_multisample_state_create_info;
		pipeline_multisample_state_create_info.flags = ::framework::gpu::pipeline_multisample_state_create_flags::none;
		pipeline_multisample_state_create_info.rasterization_samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		pipeline_multisample_state_create_info.sample_shading_enable = false;
		pipeline_multisample_state_create_info.min_sample_shading = 0.0f;
		pipeline_multisample_state_create_info.sample_mask = nullptr;
		pipeline_multisample_state_create_info.alpha_to_coverage_enable = false;
		pipeline_multisample_state_create_info.alpha_to_one_enable = false;

		::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
		pipeline_depth_stencil_state_create_info.depth_test_enable = false;
		pipeline_depth_stencil_state_create_info.depth_write_enable = false;
		pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::never;
		pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
		pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
		pipeline_depth_stencil_state_create_info.back.fail_op = ::framework::gpu::stencil_op::keep;
		pipeline_depth_stencil_state_create_info.back.pass_op = ::framework::gpu::stencil_op::keep;
		pipeline_depth_stencil_state_create_info.back.depth_fail_op = ::framework::gpu::stencil_op::keep;
		pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::never;
		pipeline_depth_stencil_state_create_info.back.compare_mask = 0x0;
		pipeline_depth_stencil_state_create_info.back.write_mask = 0x0;
		pipeline_depth_stencil_state_create_info.back.reference = 0;
		pipeline_depth_stencil_state_create_info.front = pipeline_depth_stencil_state_create_info.back;
		pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
		pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

		::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
		graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		graphics_pipeline_create_info.stage_count = static_cast<::std::uint32_t>(::std::size(pipeline_shader_stage_create_infos));
		graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
		graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
		graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
		graphics_pipeline_create_info.tessellation_state = nullptr;
		graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
		graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
		graphics_pipeline_create_info.multisample_state = &pipeline_multisample_state_create_info;
		graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
		graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
		graphics_pipeline_create_info.dynamic_state = nullptr;
		graphics_pipeline_create_info.layout = this->pipeline_layout;
		graphics_pipeline_create_info.render_pass = draw_samples_render_pass;
		graphics_pipeline_create_info.subpass = 0;
		graphics_pipeline_create_info.base_pipeline_index = -1;
		graphics_pipeline_create_info.base_pipeline = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_graphics_pipelines(nullptr, 1, &graphics_pipeline_create_info, nullptr, &draw_samples_pipeline));
	}

	{
		::framework::gpu::attachment_view_and_descriptor color_attachment;
		color_attachment.view = this->samples_image_view;
		color_attachment.descriptor_handle = {}; // todo

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
		frame_buffer_create_info.render_pass = draw_samples_render_pass;
		frame_buffer_create_info.width = samples_image_width;
		frame_buffer_create_info.height = samples_image_height;
		frame_buffer_create_info.layers = 1;
		frame_buffer_create_info.attachment_count = 1;
		frame_buffer_create_info.attachments = &color_attachment;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_frame_buffer(&frame_buffer_create_info, nullptr, &samples_frame_buffer));
	}

	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set));
	}

	{
		::framework::gpu::descriptor_image_info descriptor_image_infos[2];
		// target_image
		descriptor_image_infos[0].sampler = nullptr;
		descriptor_image_infos[0].image_view = this->image_dataset.image_view;
		descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;
		// samples_image
		descriptor_image_infos[1].sampler = nullptr;
		descriptor_image_infos[1].image_view = this->samples_image_view;
		descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::general_bit;

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[6];
		// x_buffer
		descriptor_buffer_infos[0].buffer = this->normalizing_flow->get_x_buffer();
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;
		// features_buffer
		descriptor_buffer_infos[1].buffer = this->normalizing_flow->get_features_buffer();
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;
		// log_jacobian_buffer
		descriptor_buffer_infos[2].buffer = this->normalizing_flow->get_log_jacobian_buffer();
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;
		// pdf_plot_buffer
		descriptor_buffer_infos[3].buffer = pdf_readback_buffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = 2;
		// target_buffer
		descriptor_buffer_infos[4].buffer = target_buffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = 2;
		// spatial_features_buffer
		descriptor_buffer_infos[5].buffer = features_buffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[5].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[8];
		// x_buffer
		write_descriptor_sets[0].dst_set = this->descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// features_buffer
		write_descriptor_sets[1].dst_set = this->descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// log_jacobian_buffer
		write_descriptor_sets[2].dst_set = this->descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// pdf_plot_buffer
		write_descriptor_sets[3].dst_set = this->descriptor_set;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		// target_buffer
		write_descriptor_sets[4].dst_set = this->descriptor_set;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].image_info = nullptr;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[4].texel_buffer_view = nullptr;
		write_descriptor_sets[4].acceleration_structures = nullptr;
		// target_image
		write_descriptor_sets[5].dst_set = this->descriptor_set;
		write_descriptor_sets[5].dst_binding = 5;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		write_descriptor_sets[5].image_info = &descriptor_image_infos[0];
		write_descriptor_sets[5].buffer_info = nullptr;
		write_descriptor_sets[5].texel_buffer_view = nullptr;
		write_descriptor_sets[5].acceleration_structures = nullptr;
		// samples_image
		write_descriptor_sets[6].dst_set = this->descriptor_set;
		write_descriptor_sets[6].dst_binding = 6;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		write_descriptor_sets[6].image_info = &descriptor_image_infos[1];
		write_descriptor_sets[6].buffer_info = nullptr;
		write_descriptor_sets[6].texel_buffer_view = nullptr;
		write_descriptor_sets[6].acceleration_structures = nullptr;
		// spatial_features_buffer
		write_descriptor_sets[7].dst_set = this->descriptor_set;
		write_descriptor_sets[7].dst_binding = 7;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].image_info = nullptr;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[5];
		write_descriptor_sets[7].texel_buffer_view = nullptr;
		write_descriptor_sets[7].acceleration_structures = nullptr;

		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
	}
}

::framework::test::normalizing_flow_test::~normalizing_flow_test()
{
	//this->gpu_context->get_device()->destroy_buffer(this->this->batch_size_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->dispatch_indirect_command_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->dloss_dy_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->input_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->encoded_input_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->target_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->loss_buffer, nullptr);
	//this->gpu_context->get_device()->destroy_buffer(this->ldr_readback_buffer, nullptr);
	//this->gpu_context->get_device()->free_memory(this->batch_size_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->dispatch_indirect_command_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->dloss_dy_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->input_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->encoded_input_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->target_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->loss_device_memory, nullptr);
	//this->gpu_context->get_device()->free_memory(this->ldr_readback_device_memory, nullptr);
	//::framework::gpu::descriptor_set * const descriptor_sets[]
	//{
	//	this->network_descriptor_set,
	//	this->network_initialize_parameters_descriptor_set,
	//	this->network_initialize_dispatch_indirect_command_descriptor_set,
	//	this->loss_descriptor_set,
	//	this->optimizer_descriptor_set,
	//	this->evaluate_image_descriptor_set,
	//	this->hdr_to_ldr_descriptor_set
	//};
	//this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets);
}

bool ::framework::test::normalizing_flow_test::test_normalizing_flow(bool indirect, bool validate)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		{
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));
			co_await this->normalizing_flow->initialize(graphics_command_context, this->max_batch_size, this->max_inference_batch_size);
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());

			co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);
		}

		::std::random_device random_device;
		::std::default_random_engine default_random_engine(random_device());
		::std::uniform_int_distribution<::std::uint32_t> uniform_int_distribution;
		::std::vector<float> average_time;
		::std::uint32_t const iterations = 10000;
		for (::std::uint32_t iteration = 0; iteration < iterations; iteration++)
		{
			::std::uint32_t const epoch = iteration + 1;
			bool const do_validation = epoch % 1000 == 0 || !validate;
			bool const print_epoch_statistics = epoch % 100 == 0;

			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

			this->profiler->reset(command_buffer, 0);
			this->gpu_log->reset(command_buffer, 0, {}, {});

			::std::uint32_t const inference_batch_size_dynamic_offset = 0;
			::std::uint32_t const batch_size_dynamic_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

			co_await graphics_command_context->update_buffer(this->batch_size_buffer, &this->max_inference_batch_size, inference_batch_size_dynamic_offset, sizeof(::std::uint32_t));
			co_await graphics_command_context->update_buffer(this->batch_size_buffer, &this->max_batch_size, batch_size_dynamic_offset, sizeof(::std::uint32_t));

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			command_buffer->fill_buffer(this->normalizing_flow->get_features_buffer(), 0, this->max_batch_size * this->normalizing_flow->get_features_stride() * this->parameter_type_size, 0, 0, {}, {});

			::framework::local::generate_train_data_push_constants const push_constants
			{
				.count = this->max_batch_size,
				.seed = uniform_int_distribution(random_device)
			};

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, generate_train_data_pipeline);
			command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
			command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
			command_buffer->dispatch((this->max_batch_size + 128 - 1) / 128, 1, 1);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			
			command_buffer->fill_buffer(this->normalizing_flow->get_log_jacobian_buffer(), 0, ::framework::gpu::whole_size, 0, 0, {}, {});

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			if (indirect)
			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::initialize_dispatch_indirect_command(training)");

				this->normalizing_flow->initialize_dispatch_indirect_command(command_buffer, batch_size_dynamic_offset, false);
			}

			if (indirect)
			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::forward_indirect");

				this->normalizing_flow->forward_indirect(command_buffer);
			}
			else
			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::forward_indirect");

				this->normalizing_flow->forward(command_buffer, this->max_batch_size);
			}

			if (do_validation)
			{
				{
					::framework::local::generate_forward_test_samples_push_constants push_constants
					{
						.count = this->forward_inference_batch_size,
						.width = pdf_image_width,
						.height = pdf_image_height
					};

					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
					command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, generate_forward_test_samples_pipeline);
					command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
					command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
					command_buffer->dispatch((this->forward_inference_batch_size + 128 - 1) / 128, 1, 1);
				}

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->fill_buffer(this->normalizing_flow->get_log_jacobian_buffer(), 0, ::framework::gpu::whole_size, 0, 0, {}, {});
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}

				if (indirect)
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::initialize_dispatch_indirect_command(inference)");

					this->normalizing_flow->initialize_dispatch_indirect_command(command_buffer, inference_batch_size_dynamic_offset, true);
				}

				if (indirect)
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::forward_without_gradient_indirect");

					this->normalizing_flow->forward_without_gradient_indirect(command_buffer);
				}
				else
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::forward_without_gradient");

					this->normalizing_flow->forward_without_gradient(command_buffer, this->forward_inference_batch_size);
				}

				{
					::framework::local::plot_pdf_push_constants push_constants
					{
						.count = this->forward_inference_batch_size,
						.stride = this->normalizing_flow->get_output_stride(),
						.exposure = exposure,
						.gamma = gamma
					};

					::framework::gpu::descriptor_set * const descriptor_sets[]
					{
						this->descriptor_set,
						this->gpu_log_descriptor_set
					};

					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
					command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, plot_pdf_pipeline);
					command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
					command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
					command_buffer->dispatch((this->forward_inference_batch_size + 128 - 1) / 128, 1, 1);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
				}

				{
					::framework::local::generate_train_data_push_constants push_constants
					{
						.count = this->backward_inference_batch_size,
						.seed = uniform_int_distribution(random_device)
					};
				
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				
						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
					command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, generate_backward_test_samples_pipeline);
					command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
					command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
					command_buffer->dispatch((this->backward_inference_batch_size + 128 - 1) / 128, 1, 1);
				}
				
				command_buffer->fill_buffer(this->normalizing_flow->get_log_jacobian_buffer(), 0, ::framework::gpu::whole_size, 0, 0, {}, {});
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				
					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				
				if (indirect)
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::backward_without_gradient_indirect");
				
					this->normalizing_flow->backward_without_gradient_indirect(command_buffer);
				}
				else
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "normalizing_flow::backward_without_gradient");
				
					this->normalizing_flow->backward_without_gradient(command_buffer, this->backward_inference_batch_size);
				}
				
				{
					::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
					command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
					command_buffer_begin_info.inheritance_info = nullptr;
				
					::framework::gpu::clear_value clear_value;
					clear_value.color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
				
					::framework::gpu::render_pass_state_info render_pass_state_info;
					render_pass_state_info.frame_buffer = samples_frame_buffer;
					render_pass_state_info.render_area.offset.x = 0;
					render_pass_state_info.render_area.offset.y = 0;
					render_pass_state_info.render_area.extent.width = samples_image_width;
					render_pass_state_info.render_area.extent.height = samples_image_height;
					render_pass_state_info.clear_value_count = 1;
					render_pass_state_info.clear_values = &clear_value;
					draw_samples_render_pass_state->set_state(render_pass_state_info);
				
					::framework::gpu::render_pass_begin_info render_pass_begin_info;
					render_pass_begin_info.render_pass_state = draw_samples_render_pass_state;
				
					::framework::gpu::device_size const vertex_buffer_offset = 0;
					::framework::gpu::buffer * const vertex_buffer = this->normalizing_flow->get_x_buffer();
				
					command_buffer->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::inline_);
					command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, draw_samples_pipeline);
					command_buffer->bind_descriptor_pool(this->descriptor_pool);
					command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
					command_buffer->bind_vertex_buffers(0, 1, &vertex_buffer, &vertex_buffer_offset);
					command_buffer->draw(1, this->backward_inference_batch_size, 0, 0);
					command_buffer->end_render_pass();
				
					::framework::gpu::buffer_image_copy buffer_image_copy;
					buffer_image_copy.buffer_offset = 0;
					buffer_image_copy.buffer_row_length = 0;
					buffer_image_copy.buffer_image_height = 0;
					buffer_image_copy.image_subresource.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
					buffer_image_copy.image_subresource.mip_level = 0;
					buffer_image_copy.image_subresource.base_array_layer = 0;
					buffer_image_copy.image_subresource.layer_count = 1;
					buffer_image_copy.image_offset.x = 0;
					buffer_image_copy.image_offset.y = 0;
					buffer_image_copy.image_offset.z = 0;
					buffer_image_copy.image_extent.width = samples_image_width;
					buffer_image_copy.image_extent.height = samples_image_height;
					buffer_image_copy.image_extent.depth = 1;
				
					::framework::gpu::copy_image_to_buffer_info copy_image_to_buffer_info;
					copy_image_to_buffer_info.src_image = samples_image;
					copy_image_to_buffer_info.src_image_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;
					copy_image_to_buffer_info.dst_buffer = samples_readback_buffer;
					copy_image_to_buffer_info.region_count = 1;
					copy_image_to_buffer_info.regions = &buffer_image_copy;
					command_buffer->copy_image_to_buffer(&copy_image_to_buffer_info);
				}
			}

			this->profiler->copy_query_pool_results(command_buffer);
			this->gpu_log->copy_gpu_log(command_buffer);

			assert_framework_gpu_result(command_buffer->end_command_buffer());

			co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);

			this->profiler->get_query_pool_results(0);
			this->gpu_log->print_gpu_log(0);

			auto & statistics = this->profiler->get_statistics(0);
			if (average_time.size() < statistics.size())
			{
				average_time.resize(statistics.size());
			}

			for (::std::size_t i = 0; i < statistics.size(); i++)
			{
				average_time[i] += statistics[i].delta_timestamp * 1e-3;
			}

			if (print_epoch_statistics)
			{
				::std::string statistics_string;
				for (auto & statistic : statistics)
				{
					statistics_string += ::std::format("NIS epoch {} {} {:05.2f} microseconds\r\n", epoch, statistic.label, statistic.delta_timestamp * 1e-3);
				}
				::std::cout << statistics_string << ::std::endl;

				if (do_validation)
				{
					for (::std::size_t i = 0; i < average_time.size(); i++)
					{
						::std::cout << ::std::format("average {:05.2f} microseconds\r\n", average_time[i] / epoch) << ::std::endl;
					}
				}
			}

			if (validate && do_validation)
			{
				{
					::framework::gpu::mapped_memory_range mapped_memory_range;
					mapped_memory_range.memory = samples_readback_device_memory;
					mapped_memory_range.offset = 0;
					mapped_memory_range.size = ::framework::gpu::whole_size;
					assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

					::std::filesystem::path const output_path = ::framework::local::tests_directory / ::std::format("nis_samples_{}.jpg", epoch);
					::stbi_write_jpg(output_path.string().c_str(), samples_image_width, samples_image_height, samples_channel_count, samples_readback_mapped_data, 100);
				}
				{
					::framework::gpu::mapped_memory_range mapped_memory_range;
					mapped_memory_range.memory = pdf_readback_device_memory;
					mapped_memory_range.offset = 0;
					mapped_memory_range.size = ::framework::gpu::whole_size;
					assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

					::std::filesystem::path const output_path = ::framework::local::tests_directory / ::std::format("nis_pdf_{}.jpg", epoch);
					::stbi_write_jpg(output_path.string().c_str(), pdf_image_width, pdf_image_height, pdf_channel_count, pdf_readback_mapped_data, 100);
				}
			}
		}

		co_return true;
	}());
}