#include "algorithm/gemm.hpp"
#include "algorithm/mma.hpp"
#include "algorithm/scan.hpp"
#include "algorithm/shuffle.hpp"
#include "algorithm/xavier_uniform.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "gpu/utility.hpp"
#include "nn/grid/encoding.hpp"
#include "nn/one_blob/encoding.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "nn/losses/cross_entropy.hpp"
#include "nn/losses/relative_l2.hpp"
#include "nn/mlp/network.hpp"
#include "nn/optimizers/adam.hpp"
#include "nn/optimizers/sgd.hpp"
#include "normalizing_flows/normalizing_flow.hpp"
#include "numeric"
#include "resource/resource_manager.hpp"
#include "test/test_base.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>
#include <algorithm>
#include <array>
#include <cinttypes>
#include <cstdio>
#include <format>
#include <iostream>
#include <numeric>
#include <random>

namespace framework::local
{
	namespace
	{
		::std::filesystem::path const tests_directory = "tests";

		struct evaluate_image_push_constants
		{
			::std::uint32_t width;
			::std::uint32_t height;
			::std::uint32_t batch_size;
			::std::uint32_t padded_input_size;
			::std::uint32_t input_stride;
			::std::uint32_t output_stride;
			::std::uint32_t seed;
			::std::uint32_t input_layout;
		};
	}
}

::framework::test::test_base::test_base(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log) :
	gpu_context(gpu_context), resource_manager(resource_manager), command_buffer_manager(command_buffer_manager), profiler(profiler), gpu_log(gpu_log)
{
	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[0].descriptor_count = 1 << 16;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
	descriptor_pool_create_info.max_sets = 1 << 11;
	descriptor_pool_create_info.pool_size_count = static_cast<::std::uint32_t>(::std::size(descriptor_pool_sizes));
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->descriptor_pool));

	assert_framework_gpu_result(this->gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::none, 5, ::framework::gpu::shader_stage_flags::all, this->gpu_log_descriptor_set_layout));

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &this->gpu_log_descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->gpu_log_descriptor_set));

	this->gpu_log->write_descriptor_set(this->gpu_log_descriptor_set);

	::std::filesystem::create_directories(::framework::local::tests_directory);
}

bool ::framework::test::test_base::test_mma(bool validate)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		::framework::algorithm::mma mma(this->gpu_context, this->resource_manager, ::framework::gpu::component_type::float32, ::framework::gpu::component_type::float32, ::framework::gpu::component_type::float32, ::framework::gpu::component_type::float32);

		float alpha = 1.0f;
		float beta = 1.0f;
		::std::uint32_t M = 256;
		::std::uint32_t N = 256;
		::std::uint32_t K = 256;
		::framework::algorithm::mma::matrix_layout matrix_layout_a = ::framework::algorithm::mma::matrix_layout::row_major;
		::framework::algorithm::mma::matrix_layout matrix_layout_b = ::framework::algorithm::mma::matrix_layout::row_major;
		::framework::algorithm::mma::matrix_layout matrix_layout_c = ::framework::algorithm::mma::matrix_layout::row_major;
		::framework::algorithm::mma::matrix_layout matrix_layout_d = ::framework::algorithm::mma::matrix_layout::row_major;
		::framework::algorithm::mma::executor gemm_executor(mma, this->gpu_context, M, N, K, matrix_layout_a, matrix_layout_b, matrix_layout_c, matrix_layout_d, alpha, beta);

		::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();

		::std::mt19937 gen;
		::std::uniform_real_distribution<float> dist(-1.0f, 1.0f);

		struct matrix_desc
		{
			::std::uint32_t rows;
			::std::uint32_t cols;
			::framework::gpu::component_type type;
			bool col_major;
			::framework::gpu::buffer * buffer;
			::framework::gpu::device_memory * device_memory;
			void * mapped_data;

			void set_data(::std::uint32_t index, float value)
			{
				if (this->type == ::framework::gpu::component_type::float32)
				{
					((float *)this->mapped_data)[index] = value;
				}
				else
				{
					::std::uint32_t as_int = *(::std::uint32_t *)&value;
					int sign = (as_int & 0x80000000) >> 31;
					int exp = ((as_int & 0x7f800000) >> 23) - 127;
					int mantissa = (as_int & 0x7FFFFF);

					sign = sign << 15;
					exp = (exp + 15) << 10;
					mantissa = mantissa >> (23 - 10);

					if (as_int != 0)
					{
						as_int = sign | exp | mantissa;
					}

					((::std::uint16_t *)this->mapped_data)[index] = as_int;
				}
			}

			float get_data(::std::uint32_t index) const
			{
				if (this->type == ::framework::gpu::component_type::float32)
				{
					return ((float *)this->mapped_data)[index];
				}
				else
				{
					::std::uint32_t as_int = ((::std::uint16_t *)this->mapped_data)[index];
					int sign = (as_int & 0x8000) >> 15;
					int exp = ((as_int & 0x7c00) >> 10) - 15;
					int mantissa = (as_int & 0x3FF);

					sign = sign << 31;
					exp = (exp + 127) << 23;
					mantissa = mantissa << (23 - 10);

					if (as_int != 0)
					{
						as_int = sign | exp | mantissa;
					}

					return *(float *)&as_int;
				}
			}

			float get_data(int m, int n) const
			{
				return this->get_data(this->col_major ? (n * this->rows + m) : (m * this->cols + n));
			}
		};

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

		auto create_matrix = [&](matrix_desc & matrix_desc) -> ::framework::coroutine::immediate_task<void>
			{
				::framework::gpu::buffer_create_info buffer_create_info;
				buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
				buffer_create_info.size = matrix_desc.rows * matrix_desc.cols * 4;
				buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
				if (validate)
				{
					buffer_create_info.usage |= ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
				}
				buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
				buffer_create_info.queue_family_index_count = 0;
				buffer_create_info.queue_family_indices = nullptr;
				buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
				buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_create_info.opaque_capture_address = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &matrix_desc.buffer));

				::framework::gpu::memory_requirements memory_requirements;
				::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
				buffer_memory_requirements_info.buffer = matrix_desc.buffer;
				this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

				::framework::gpu::memory_property_flags memory_property_flags = ::framework::gpu::memory_property_flags::device_local_bit;
				if (validate)
				{
					memory_property_flags = ::framework::gpu::memory_property_flags::host_visible_read_bit | ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_cached_bit;
				}

				::framework::gpu::memory_allocate_info memory_allocate_info;
				memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
				memory_allocate_info.device_mask = 0;
				memory_allocate_info.allocation_size = memory_requirements.size;
				memory_allocate_info.allocation_alignment = memory_requirements.alignment;
				memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, memory_property_flags);
				memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? matrix_desc.buffer : nullptr;
				memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
				memory_allocate_info.opaque_capture_address = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &matrix_desc.device_memory));

				::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
				bind_buffer_memory_info.buffer = matrix_desc.buffer;
				bind_buffer_memory_info.memory = matrix_desc.device_memory;
				bind_buffer_memory_info.memory_offset = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

				::std::vector<float> data;
				if (validate)
				{
					assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(matrix_desc.device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&matrix_desc.mapped_data)));
				}
				else
				{
					data.resize(matrix_desc.rows * matrix_desc.cols);
					matrix_desc.mapped_data = data.data();
				}

				for (::std::uint32_t i = 0; i < matrix_desc.rows * matrix_desc.cols; i++)
				{
					matrix_desc.set_data(i, ((float)(rand() & 0x3) - 1.0f) / 2.0f);
				}

				if (!validate)
				{
					co_await graphics_command_context->update_buffer(matrix_desc.buffer, data.data(), 0, buffer_create_info.size);
				}
			};

		matrix_desc matrices[4]
		{
			{ M, K, ::framework::gpu::component_type::float32, false },
			{ K, N, ::framework::gpu::component_type::float32, false },
			{ M, N, ::framework::gpu::component_type::float32, false },
			{ M, N, ::framework::gpu::component_type::float32, false }
		};

		co_await create_matrix(matrices[0]);
		co_await create_matrix(matrices[1]);
		co_await create_matrix(matrices[2]);
		co_await create_matrix(matrices[3]);

		::framework::gpu::descriptor_set_layout * const descriptor_set_layout = mma.get_descriptor_set_layout();
		::framework::gpu::descriptor_set * descriptor_set;

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, &descriptor_set));

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
		descriptor_buffer_infos[0].buffer = matrices[0].buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;

		descriptor_buffer_infos[1].buffer = matrices[1].buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;

		descriptor_buffer_infos[2].buffer = matrices[2].buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;

		descriptor_buffer_infos[3].buffer = matrices[3].buffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
		// a
		write_descriptor_sets[0].dst_set = descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// b
		write_descriptor_sets[1].dst_set = descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// c
		write_descriptor_sets[2].dst_set = descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// d
		write_descriptor_sets[3].dst_set = descriptor_set;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

		gemm_executor.execute(mma, graphics_command_context->get_command_buffer(), descriptor_set);
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
		co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);

		bool pass = true;
		if (validate)
		{
			for (::std::uint32_t i = 0; i < matrices[0].rows; ++i)
			{
				for (::std::uint32_t j = 0; j < matrices[1].cols; ++j)
				{
					float ref = 0.0f;
					for (::std::uint32_t k = 0; k < K; ++k)
					{
						ref += matrices[0].get_data(i, k) * matrices[1].get_data(k, j);
					}

					ref = alpha * ref + beta * matrices[2].get_data(i, j);

					float d_ij = matrices[3].get_data(i, j);
					if (ref != d_ij)
					{
						pass = false;
						::std::printf("error %d %d %d != %d\n", i, j, ref, d_ij);
					}
				}
			}
		}

		this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, 1, &descriptor_set);

		co_return pass;
	}());
}

bool ::framework::test::test_base::test_gemm(bool indirect, bool split_k, bool validate)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		::framework::algorithm::gemm gemm(this->gpu_context, this->resource_manager, ::framework::gpu::component_type::float16, ::framework::gpu::component_type::float16, ::framework::gpu::component_type::float16, ::framework::gpu::component_type::float16, ::framework::gpu::component_type::float16, 0, nullptr, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_hlsl_path, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_glsl_path);

		float constexpr alpha = 1.0f;
		float constexpr beta = 0.0f;
		::std::uint32_t const M = validate ? 256 + 16 : 32 + 16;
		::std::uint32_t const N = validate ? 256 : 1u << 22;
		::std::uint32_t const K = validate ? 1024 : 128;
		::std::uint32_t const tile_m = 32u;
		::std::uint32_t const tile_n = 256u;
		::std::uint32_t const tile_k = 32;
		::std::uint32_t const offset_m_0 = 0u;
		::std::uint32_t const offset_n_0 = 0u;
		::std::uint32_t const offset_m_1 = ::framework::numeric::previous_multiple(M, tile_m);
		::std::uint32_t const offset_n_1 = 0u;
		::std::uint32_t const split_k_slices = split_k ? K / ::std::min(K, validate ? 1u << 7 : 1u << 12) : 1;
		::framework::linalg::matrix_layout constexpr matrix_layout_a = ::framework::linalg::matrix_layout::column_major;
		::framework::linalg::matrix_layout constexpr matrix_layout_b = ::framework::linalg::matrix_layout::row_major;
		::framework::linalg::matrix_layout constexpr matrix_layout_c = ::framework::linalg::matrix_layout::row_major;
		::framework::linalg::matrix_layout constexpr matrix_layout_d = ::framework::linalg::matrix_layout::row_major;
		::framework::algorithm::gemm::executor gemm_executor_0(gemm, this->gpu_context, offset_m_0, offset_n_0, tile_m, tile_n, tile_k, split_k, matrix_layout_a, matrix_layout_b, matrix_layout_c, matrix_layout_d);
		::framework::algorithm::gemm::executor gemm_executor_1(gemm, this->gpu_context, offset_m_1, offset_n_1, 16, tile_n, tile_k, split_k, matrix_layout_a, matrix_layout_b, matrix_layout_c, matrix_layout_d);
		::framework::algorithm::gemm::reduce_split_k_executor reduce_split_k_executor(gemm, this->gpu_context, matrix_layout_c, matrix_layout_d);

		::std::mt19937 gen;
		::std::uniform_real_distribution<float> dist(-1.0f, 1.0f);

		struct matrix_desc
		{
			::std::uint32_t rows;
			::std::uint32_t cols;
			bool col_major;
			::framework::gpu::buffer * buffer;
			::framework::gpu::device_memory * device_memory;
			::std::vector<::std::byte> data;

			void set_data(::std::uint32_t index, float value) const
			{
				::std::uint32_t as_int = *(::std::uint32_t *)&value;
				int sign = (as_int & 0x80000000) >> 31;
				int exp = ((as_int & 0x7f800000) >> 23) - 127;
				int mantissa = (as_int & 0x7FFFFF);

				sign = sign << 15;
				exp = (exp + 15) << 10;
				mantissa = mantissa >> (23 - 10);

				if (as_int != 0)
				{
					as_int = sign | exp | mantissa;
				}

				((::std::uint16_t *)this->data.data())[index] = as_int;
			}

			float get_data(::std::uint32_t index) const
			{
				::std::uint32_t as_int = ((::std::uint16_t *)this->data.data())[index];
				int sign = (as_int & 0x8000) >> 15;
				int exp = ((as_int & 0x7c00) >> 10) - 15;
				int mantissa = (as_int & 0x3FF);

				sign = sign << 31;
				exp = (exp + 127) << 23;
				mantissa = mantissa << (23 - 10);

				if (as_int != 0)
				{
					as_int = sign | exp | mantissa;
				}

				return *(float *)&as_int;
			}

			float get_data(int m, int n) const
			{
				return this->get_data(this->col_major ? (n * this->rows + m) : (m * this->cols + n));
			}
		};

		auto create_matrix = [&](matrix_desc & matrix_desc)
			{
				::framework::gpu::buffer_create_info buffer_create_info;
				buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
				buffer_create_info.size = matrix_desc.rows * matrix_desc.cols * 2;
				buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
				buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
				buffer_create_info.queue_family_index_count = 0;
				buffer_create_info.queue_family_indices = nullptr;
				buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
				buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_create_info.opaque_capture_address = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &matrix_desc.buffer));

				::framework::gpu::memory_requirements memory_requirements;
				::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
				buffer_memory_requirements_info.buffer = matrix_desc.buffer;
				this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

				::framework::gpu::memory_allocate_info memory_allocate_info;
				memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
				memory_allocate_info.device_mask = 0;
				memory_allocate_info.allocation_size = memory_requirements.size;
				memory_allocate_info.allocation_alignment = memory_requirements.alignment;
				memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
				memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? matrix_desc.buffer : nullptr;
				memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
				memory_allocate_info.opaque_capture_address = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &matrix_desc.device_memory));

				::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
				bind_buffer_memory_info.buffer = matrix_desc.buffer;
				bind_buffer_memory_info.memory = matrix_desc.device_memory;
				bind_buffer_memory_info.memory_offset = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

				matrix_desc.data.resize(buffer_create_info.size);
			};

		matrix_desc matrices[4]
		{
			{ M, K, matrix_layout_a == ::framework::linalg::matrix_layout::column_major },
			{ K, N, matrix_layout_b == ::framework::linalg::matrix_layout::column_major },
			{ M, N, matrix_layout_c == ::framework::linalg::matrix_layout::column_major },
			{ M, N, matrix_layout_d == ::framework::linalg::matrix_layout::column_major }
		};

		for (auto & matrix : matrices)
		{
			create_matrix(matrix);
		}

		::framework::gpu::buffer * matrix_d_k_slices_buffer;
		::framework::gpu::device_memory * matrix_d_k_slices_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = M * N * split_k_slices * 2;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &matrix_d_k_slices_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = matrix_d_k_slices_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? matrix_d_k_slices_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &matrix_d_k_slices_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = matrix_d_k_slices_buffer;
			bind_buffer_memory_info.memory = matrix_d_k_slices_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}
		::framework::gpu::buffer * device_parameters_buffer;
		::framework::gpu::device_memory * device_parameters_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = sizeof(::framework::algorithm::gemm::device_parameters);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &device_parameters_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = device_parameters_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? device_parameters_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &device_parameters_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = device_parameters_buffer;
			bind_buffer_memory_info.memory = device_parameters_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}
		::framework::gpu::buffer * dispatch_indirect_command_buffer;
		::framework::gpu::device_memory * dispatch_indirect_command_device_memory;
		if (indirect)
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = sizeof(::framework::gpu::dispatch_indirect_command) * 2;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &dispatch_indirect_command_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = dispatch_indirect_command_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? dispatch_indirect_command_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &dispatch_indirect_command_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = dispatch_indirect_command_buffer;
			bind_buffer_memory_info.memory = dispatch_indirect_command_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::descriptor_set_layout * const descriptor_set_layout = gemm.get_descriptor_set_layout();
		::framework::gpu::descriptor_set * descriptor_set;

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, &descriptor_set));

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[6];
		descriptor_buffer_infos[0].buffer = matrices[0].buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;

		descriptor_buffer_infos[1].buffer = matrices[1].buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;

		descriptor_buffer_infos[2].buffer = matrices[2].buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;

		descriptor_buffer_infos[3].buffer = matrices[3].buffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = 2;

		descriptor_buffer_infos[4].buffer = matrix_d_k_slices_buffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = 2;

		descriptor_buffer_infos[5].buffer = device_parameters_buffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[5].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[6];
		// matrix_a_buffer
		write_descriptor_sets[0].dst_set = descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// matrix_b_buffer
		write_descriptor_sets[1].dst_set = descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// matrix_c_buffer
		write_descriptor_sets[2].dst_set = descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// matrix_d_buffer
		write_descriptor_sets[3].dst_set = descriptor_set;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		// matrix_d_k_slices_buffer
		write_descriptor_sets[4].dst_set = descriptor_set;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].image_info = nullptr;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[4].texel_buffer_view = nullptr;
		write_descriptor_sets[4].acceleration_structures = nullptr;
		// device_parameters_buffer
		write_descriptor_sets[5].dst_set = descriptor_set;
		write_descriptor_sets[5].dst_binding = 5;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[5].image_info = nullptr;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
		write_descriptor_sets[5].texel_buffer_view = nullptr;
		write_descriptor_sets[5].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

		for (auto & matrix : matrices)
		{
			for (::std::uint32_t i = 0; i < matrix.rows * matrix.cols; i++)
			{
				matrix.set_data(i, ((float)(rand() & 0x3) - 1.0f) / 2.0f);
			}
		}

		::framework::linear_allocator_page_manager linear_allocator_page_manager(this->gpu_context, ::framework::linear_allocation_page::type::cpu_read);
		::framework::linear_allocator linear_allocator(linear_allocator_page_manager);

		::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();
		::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

		this->profiler->reset(command_buffer, 0);

		for (auto & matrix : matrices)
		{
			co_await graphics_command_context->update_buffer(matrix.buffer, matrix.data.data(), 0, matrix.data.size());
		}

		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = gemm_executor_0.get_device_parameters(gpu_context, M, N, K, split_k_slices, alpha, beta);
		co_await graphics_command_context->update_buffer(device_parameters_buffer, &gemm_device_parameters, 0, sizeof(::framework::algorithm::gemm::device_parameters));

		if (indirect)
		{
			::framework::gpu::dispatch_indirect_command const gemm_dispatch_indirect_commands[]
			{
				{.x = N / tile_n, .y = M / tile_m, .z = split_k_slices },
				{.x = N / 16, .y = M / 16, .z = 1 }
			};
			co_await graphics_command_context->update_buffer(dispatch_indirect_command_buffer, gemm_dispatch_indirect_commands);
		}

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		{
			if (indirect)
			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "execute_split_k_indirect");

				if (split_k)
				{
					gemm_executor_0.execute_split_k_indirect(gemm, command_buffer, descriptor_set, dispatch_indirect_command_buffer, 0);
					gemm_executor_1.execute_split_k_indirect(gemm, command_buffer, descriptor_set, dispatch_indirect_command_buffer, 0);

					::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
					buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
					buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
					buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
					buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
					buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
					buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
					buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
					buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
					buffer_memory_barrier.buffer = matrix_d_k_slices_buffer;
					buffer_memory_barrier.offset = 0;
					buffer_memory_barrier.size = ::framework::gpu::whole_size;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 0;
					dependency_info.memory_barriers = nullptr;
					dependency_info.buffer_memory_barrier_count = 1;
					dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);

					reduce_split_k_executor.execute_reduce_split_k_indirect(gemm, command_buffer, descriptor_set, dispatch_indirect_command_buffer, sizeof(::framework::gpu::dispatch_indirect_command));
				}
				else
				{
					gemm_executor_0.execute_indirect(gemm, command_buffer, descriptor_set, dispatch_indirect_command_buffer, 0);
					gemm_executor_1.execute_indirect(gemm, command_buffer, descriptor_set, dispatch_indirect_command_buffer, 0);
				}
			}
			else
			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit | ::framework::gpu::pipeline_stage_flags::transfer_bit, "gemm_split_k");

				if (split_k)
				{
					gemm_executor_0.execute_split_k(gemm, command_buffer, descriptor_set, M, N, split_k_slices);
					gemm_executor_1.execute_split_k(gemm, command_buffer, descriptor_set, M - offset_m_1, N - offset_n_1, split_k_slices);

					::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
					buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
					buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
					buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
					buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
					buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
					buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
					buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
					buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
					buffer_memory_barrier.buffer = matrix_d_k_slices_buffer;
					buffer_memory_barrier.offset = 0;
					buffer_memory_barrier.size = ::framework::gpu::whole_size;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 0;
					dependency_info.memory_barriers = nullptr;
					dependency_info.buffer_memory_barrier_count = 1;
					dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);

					reduce_split_k_executor.execute_reduce_split_k(gemm, command_buffer, descriptor_set, M, N);
				}
				else
				{
					gemm_executor_0.execute(gemm, command_buffer, descriptor_set, M, N);
					gemm_executor_1.execute(gemm, command_buffer, descriptor_set, M - offset_m_1, N - offset_n_1);
				}
			}

			::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
			buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
			buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
			buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.buffer = matrices[3].buffer;
			buffer_memory_barrier.offset = 0;
			buffer_memory_barrier.size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 1;
			dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			command_buffer->pipeline_barrier(&dependency_info);
		}

		::framework::linear_allocator::allocation read_buffer_allocation;
		if (validate)
		{
			read_buffer_allocation = co_await graphics_command_context->read_buffer(linear_allocator, matrices[3].buffer, 0, matrices[3].data.size());
		}

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		this->profiler->copy_query_pool_results(command_buffer);

		assert_framework_gpu_result(command_buffer->end_command_buffer());

		bool pass = true;
		::std::uint32_t const iterations = validate ? 1 : 100;
		float average_time = 0.0f;
		for (::std::uint32_t iteration = 0; iteration < iterations; iteration++)
		{
			co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);

			if (validate)
			{
				::std::memcpy(matrices[3].data.data(), read_buffer_allocation.mapped_buffer_data, matrices[3].data.size());

				for (::std::uint32_t i = 0; i < matrices[0].rows; ++i)
				{
					for (::std::uint32_t j = 0; j < matrices[1].cols; ++j)
					{
						float ref = 0.0f;
						for (::std::uint32_t k = 0; k < K; ++k)
						{
							ref += matrices[0].get_data(i, k) * matrices[1].get_data(k, j);
						}

						ref = alpha * ref + beta * matrices[2].get_data(i, j);

						float d_ij = matrices[3].get_data(i, j);
						if (ref != d_ij)
						{
							pass = false;
							::std::printf("error %d %d %f != %f\n", i, j, ref, d_ij);
						}
					}
				}
			}

			this->profiler->get_query_pool_results(0);

			::std::string statistics_string;
			for (auto & statistic : this->profiler->get_statistics(0))
			{
				statistics_string += ::std::format("iteration {} {} {:05.2f} ms\r\n", iteration, statistic.label, statistic.delta_timestamp * 1e-6);
				average_time += statistic.delta_timestamp * 1e-6;
			}
			::std::cout << statistics_string << ::std::endl;
		}

		co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);

		average_time /= iterations;
		::std::cout << ::std::format("average {:05.2f} ms\r\n", average_time) << ::std::endl;

		this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, 1, &descriptor_set);

		co_return pass;
	}());
}

bool ::framework::test::test_base::test_scan(bool validate)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		::std::uint32_t const problem_size = 1u << 26;

		::framework::gpu::descriptor_set_layout * shuffle_nis_samples_descriptor_set_layout;
		{
			::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
			// scan_buffer
			descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[0].binding = 0;
			descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
			descriptor_set_layout_bindings[0].hlsl_register_space = 1;
			descriptor_set_layout_bindings[0].descriptor_count = 1;
			descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

			::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
			descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
			descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
			descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &shuffle_nis_samples_descriptor_set_layout));
		}

		::framework::algorithm::scan scan(this->gpu_context, this->resource_manager, shuffle_nis_samples_descriptor_set_layout, ::framework::algorithm::scan::default_scan_executor_type_name, ::framework::algorithm::scan::default_scan_executor_path);
		::framework::algorithm::scan::executor scan_executor(scan, this->gpu_context);

		::std::vector<::std::uint32_t> problem_vector(problem_size, 1);

		::framework::gpu::buffer * scan_buffer;
		::framework::gpu::device_memory * scan_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = problem_size * sizeof(::std::uint32_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &scan_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = scan_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? scan_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &scan_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = scan_buffer;
			bind_buffer_memory_info.memory = scan_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::buffer * index_buffer;
		::framework::gpu::device_memory * index_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = sizeof(::std::uint32_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &index_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = index_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? index_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &index_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = index_buffer;
			bind_buffer_memory_info.memory = index_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::buffer * thread_block_reduction_buffer;
		::framework::gpu::device_memory * thread_block_reduction_device_memory;
		{
			::std::uint32_t block_offset_size;
			scan_executor.get_memory_requirements(problem_size, block_offset_size);

			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = block_offset_size * sizeof(::std::uint32_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &thread_block_reduction_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = thread_block_reduction_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? thread_block_reduction_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &thread_block_reduction_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = thread_block_reduction_buffer;
			bind_buffer_memory_info.memory = thread_block_reduction_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::linear_allocator_page_manager linear_allocator_page_manager(this->gpu_context, ::framework::linear_allocation_page::type::cpu_read);
		::framework::linear_allocator linear_allocator(linear_allocator_page_manager);

		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			scan.get_descriptor_set_layout(),
			shuffle_nis_samples_descriptor_set_layout
		};
		::framework::gpu::descriptor_set * descriptor_sets[2];

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		// index_buffer
		descriptor_buffer_infos[0].buffer = index_buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;
		// thread_block_reduction_buffer
		descriptor_buffer_infos[1].buffer = thread_block_reduction_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;
		// scan_buffer
		descriptor_buffer_infos[2].buffer = scan_buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		// index_buffer
		write_descriptor_sets[0].dst_set = descriptor_sets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// thread_block_reduction_buffer
		write_descriptor_sets[1].dst_set = descriptor_sets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// scan_buffer
		write_descriptor_sets[2].dst_set = descriptor_sets[1];
		write_descriptor_sets[2].dst_binding = 0;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

		::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();
		::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

		this->profiler->reset(command_buffer, 0);

		co_await graphics_command_context->update_buffer(scan_buffer, problem_vector.data(), 0, static_cast<::framework::gpu::device_size>(problem_vector.size() * sizeof(::std::uint32_t)));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "scan");

			scan_executor.execute(scan, command_buffer, descriptor_sets, problem_size);
			{
				::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
				buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
				buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
				buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
				buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
				buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
				buffer_memory_barrier.buffer = scan_buffer;
				buffer_memory_barrier.offset = 0;
				buffer_memory_barrier.size = ::framework::gpu::whole_size;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = 1;
				dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
		::framework::linear_allocator::allocation const allocation = co_await graphics_command_context->read_buffer(linear_allocator, scan_buffer, 0, static_cast<::framework::gpu::device_size>(problem_vector.size() * sizeof(::std::uint32_t)));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		this->profiler->copy_query_pool_results(command_buffer);

		assert_framework_gpu_result(command_buffer->end_command_buffer());
		co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);

		bool pass = true;
		if (validate)
		{
			::framework::gpu::mapped_memory_range mapped_memory_range;
			mapped_memory_range.memory = allocation.device_memory;
			mapped_memory_range.offset = 0;
			mapped_memory_range.size = ::framework::gpu::whole_size;
			assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

			::std::uint32_t * scan_result = reinterpret_cast<::std::uint32_t *>(allocation.mapped_buffer_data);
			::std::vector<::std::uint32_t> reference_scan(problem_size, 1);
			::std::exclusive_scan(problem_vector.begin(), problem_vector.end(), reference_scan.begin(), 0);
			if (!::std::equal(scan_result, scan_result + problem_size, reference_scan.begin()))
			{
				pass = false;
				::std::printf("error incorrect scan\n");
			}
		}

		this->profiler->get_query_pool_results(0);

		::std::string statistics_string;
		for (auto & statistic : this->profiler->get_statistics(0))
		{
			statistics_string += ::std::format("{}: {:05.2f} ms\r\n", statistic.label, statistic.delta_timestamp * 1e-6);
		}
		::std::cout << statistics_string << ::std::endl;

		this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets);

		co_return pass;
	}());
}

bool ::framework::test::test_base::test_shuffle(bool validate)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		::std::uint32_t const problem_size = 1u << 21;
		::framework::algorithm::shuffle shuffle(this->gpu_context, this->resource_manager);
		::framework::algorithm::shuffle::executor shuffle_executor(shuffle, this->gpu_context);

		::std::vector<::std::uint64_t> problem_vector(problem_size);
		::std::iota(problem_vector.begin(), problem_vector.end(), 0);

		::framework::gpu::buffer * shuffle_buffer;
		::framework::gpu::device_memory * shuffle_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = problem_size * sizeof(::std::uint64_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &shuffle_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = shuffle_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? shuffle_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &shuffle_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = shuffle_buffer;
			bind_buffer_memory_info.memory = shuffle_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::buffer * index_buffer;
		::framework::gpu::device_memory * index_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = sizeof(::std::uint32_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &index_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = index_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? index_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &index_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = index_buffer;
			bind_buffer_memory_info.memory = index_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::buffer * thread_block_reduction_buffer;
		::framework::gpu::device_memory * thread_block_reduction_device_memory;
		{
			::std::uint32_t block_offset_size;
			shuffle_executor.get_scan_executor().get_memory_requirements(problem_size, block_offset_size);

			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = block_offset_size * sizeof(::std::uint32_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &thread_block_reduction_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = thread_block_reduction_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? thread_block_reduction_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &thread_block_reduction_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = thread_block_reduction_buffer;
			bind_buffer_memory_info.memory = thread_block_reduction_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::linear_allocator_page_manager linear_allocator_page_manager(this->gpu_context, ::framework::linear_allocation_page::type::cpu_read);
		::framework::linear_allocator linear_allocator(linear_allocator_page_manager);

		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[2]
		{
			shuffle.get_descriptor_set_layout(),
			shuffle.get_scan().get_descriptor_set_layout()
		};
		::framework::gpu::descriptor_set * descriptor_sets[2];

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[5];
		// input_buffer
		descriptor_buffer_infos[0].buffer = shuffle_buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;
		// output_buffer
		descriptor_buffer_infos[1].buffer = shuffle_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;
		// scan_buffer
		descriptor_buffer_infos[2].buffer = shuffle_buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;
		// index_buffer
		descriptor_buffer_infos[3].buffer = index_buffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = 2;
		// thread_block_reduction_buffer
		descriptor_buffer_infos[4].buffer = thread_block_reduction_buffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[5];
		// input_buffer
		write_descriptor_sets[0].dst_set = descriptor_sets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// output_buffer
		write_descriptor_sets[1].dst_set = descriptor_sets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// scan_buffer
		write_descriptor_sets[2].dst_set = descriptor_sets[1];
		write_descriptor_sets[2].dst_binding = 0;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// index_buffer
		write_descriptor_sets[3].dst_set = descriptor_sets[1];
		write_descriptor_sets[3].dst_binding = 1;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		// thread_block_reduction_buffer
		write_descriptor_sets[4].dst_set = descriptor_sets[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].image_info = nullptr;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[4].texel_buffer_view = nullptr;
		write_descriptor_sets[4].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

		::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();
		::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();


		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

		co_await graphics_command_context->update_buffer(shuffle_buffer, problem_vector.data(), 0, static_cast<::framework::gpu::device_size>(problem_vector.size() * sizeof(::std::uint64_t)));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		shuffle_executor.execute(shuffle, command_buffer, descriptor_sets, problem_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		::framework::linear_allocator::allocation const allocation = co_await graphics_command_context->read_buffer(linear_allocator, shuffle_buffer, 0, static_cast<::framework::gpu::device_size>(problem_vector.size() * sizeof(::std::uint64_t)));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		assert_framework_gpu_result(command_buffer->end_command_buffer());
		co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);

		bool pass = true;
		if (validate)
		{
			::framework::gpu::mapped_memory_range mapped_memory_range;
			mapped_memory_range.memory = allocation.device_memory;
			mapped_memory_range.offset = 0;
			mapped_memory_range.size = ::framework::gpu::whole_size;
			assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

			::std::uint64_t * shuffled_array = reinterpret_cast<::std::uint64_t *>(allocation.mapped_buffer_data);
			::std::uint64_t const src_checksum = ::std::accumulate(problem_vector.begin(), problem_vector.end(), 0ull);
			::std::uint64_t const dst_checksum = ::std::accumulate(shuffled_array, shuffled_array + problem_size, 0ull);
			if (src_checksum != dst_checksum)
			{
				pass = false;
				::std::printf("error: inconsisten checksum srd(%" PRIu64 ") != dst(%" PRIu64 ")\n", src_checksum, dst_checksum);
			}
			//for (::std::uint32_t i = 0; i < problem_size; i++)
			//{
			//	auto it = ::std::find(scan_result, scan_result + problem_size, i);
			//	if (!it)
			//	{
			//		pass = false;
			//		::std::printf("error %d not found in the shuffled array", i);
			//	}
			//}
		}

		this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets);

		co_return pass;
	}());
}

bool ::framework::test::test_base::test_mlp()
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		struct texture_load_callback_t : public ::framework::resource::texture_load_callback
		{
			explicit texture_load_callback_t(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager) :
				gpu_context(gpu_context), resource_manager(resource_manager), command_buffer_manager(command_buffer_manager)
			{
			}

			::framework::coroutine::lazy_task<void> operator()(::framework::resource::texture_load_callback::texture_load_info const & texture_load_info) override
			{
				::std::uint32_t	width = this->image_width = texture_load_info.width;
				::std::uint32_t	height = this->image_height = texture_load_info.height;
				::framework::gpu::device_size row_count;
				bool const is_texture_compression_bc_format = ::framework::gpu::utility::is_texture_compression_bc_format(texture_load_info.format);
				bool const texture_compression_bc_unaligned = this->gpu_context->get_physical_device_features().texture_compression_bc_unaligned;

				auto const calculate_subresource_info = [is_texture_compression_bc_format, texture_compression_bc_unaligned](::framework::gpu::device_size & row_count, ::std::uint32_t & width, ::std::uint32_t & height)
					{
						if (is_texture_compression_bc_format)
						{
							row_count = height / 4;

							if (row_count == 0)
							{
								row_count = 1;
							}

							if (!texture_compression_bc_unaligned)
							{
								width = (width + 3) & ~3;
								height = (height + 3) & ~3;
							}
						}
						else
						{
							row_count = height;
						}
					};

				calculate_subresource_info(row_count, width, height);

				::framework::gpu::image_create_info image_create_info;
				image_create_info.flags = ::framework::gpu::image_create_flags::none;
				image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
				image_create_info.format = texture_load_info.format;
				image_create_info.extent.width = width;
				image_create_info.extent.height = height;
				image_create_info.extent.depth = 1;
				image_create_info.mip_levels = texture_load_info.mip_levels;
				image_create_info.array_layers = 1;
				image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
				image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
				image_create_info.usage = ::framework::gpu::image_usage_flags::transfer_dst_bit | ::framework::gpu::image_usage_flags::sampled_bit;
				image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
				image_create_info.queue_family_index_count = 0;
				image_create_info.queue_family_indices = nullptr;
				image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
				image_create_info.initial_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
				image_create_info.optimized_clear_value = nullptr;
				assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->device_image));

				::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
				debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
				debug_utils_object_name_info.object = this->device_image;
				debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::device_image";
				assert_framework_gpu_result(this->gpu_context->get_device()->set_object_name(&debug_utils_object_name_info));

				::framework::gpu::memory_requirements memory_requirements;

				::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
				image_memory_requirements_info.image = this->device_image;
				this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

				::framework::gpu::memory_allocate_info memory_allocate_info;
				memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
				memory_allocate_info.device_mask = 0;
				memory_allocate_info.allocation_size = memory_requirements.size;
				memory_allocate_info.allocation_alignment = memory_requirements.alignment;
				memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
				memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
				memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
				memory_allocate_info.opaque_capture_address = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->device_memory));

				::framework::gpu::bind_image_memory_info bind_image_memory_info;
				bind_image_memory_info.image = this->device_image;
				bind_image_memory_info.memory = this->device_memory;
				bind_image_memory_info.memory_offset = 0;
				assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

				::framework::gpu::image_view_create_info image_view_create_info;
				image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
				image_view_create_info.image = this->device_image;
				image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
				image_view_create_info.format = texture_load_info.format;
				image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
				image_view_create_info.subresource_range.base_mip_level = 0;
				image_view_create_info.subresource_range.level_count = texture_load_info.mip_levels;
				image_view_create_info.subresource_range.base_array_layer = 0;
				image_view_create_info.subresource_range.layer_count = 1;
				image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
				image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
				image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
				image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
				assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->image_view));

				::framework::command_context * const transfer_command_context = co_await this->command_buffer_manager->get_transfer_queue()->acquire_command_context();
				::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
				{
					::framework::gpu::command_buffer * command_buffer = transfer_command_context->get_command_buffer();

					::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
					command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
					command_buffer_begin_info.inheritance_info = nullptr;
					assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

					::framework::gpu::image_subresource_range subresource_range;
					subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
					subresource_range.base_mip_level = 0;
					subresource_range.level_count = texture_load_info.mip_levels;
					subresource_range.base_array_layer = 0;
					subresource_range.layer_count = 1;

					::framework::gpu::image_memory_barrier image_memory_barrier;
					image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::none;
					image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
					image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
					image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
					image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
					image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
					image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
					image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
					image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
					image_memory_barrier.image = this->device_image;
					image_memory_barrier.subresource_range = subresource_range;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 0;
					dependency_info.memory_barriers = nullptr;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 1;
					dependency_info.image_memory_barriers = &image_memory_barrier;

					command_buffer->pipeline_barrier(&dependency_info);

					::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment = this->gpu_context->get_physical_device_properties().limits.optimal_buffer_copy_row_pitch_alignment;
					::framework::gpu::device_size const optimal_buffer_copy_offset_alignment = this->gpu_context->get_physical_device_properties().limits.optimal_buffer_copy_offset_alignment;
					::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment_mask = optimal_buffer_copy_row_pitch_alignment - 1;
					::framework::gpu::device_size const optimal_buffer_copy_offset_alignment_mask = optimal_buffer_copy_offset_alignment - 1;
					::framework::gpu::device_size const required_size = ::std::accumulate(texture_load_info.subresource_data, texture_load_info.subresource_data + texture_load_info.subresource_count, ::framework::gpu::device_size(0),
						[&](::framework::gpu::device_size const size, ::framework::resource::texture_load_callback::subresource_data const & subresource_info)
						{
							::framework::gpu::device_size const aligned_row_pitch = (subresource_info.row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
							::std::uint32_t const mip_level = subresource_info.mip_level;
							::std::uint32_t mip_width = width >> mip_level;
							::std::uint32_t mip_height = height >> mip_level;
							calculate_subresource_info(row_count, mip_width, mip_height);
							return size + row_count * aligned_row_pitch + optimal_buffer_copy_offset_alignment_mask;
						}
					);
					::framework::linear_allocator::allocation const allocation = co_await transfer_command_context->get_linear_allocator().allocate(required_size, 1);
					::framework::gpu::device_size dst_offset = allocation.offset;
					::std::vector<::framework::gpu::buffer_image_copy> buffer_image_copy(texture_load_info.subresource_count);
					for (::std::uint32_t i = 0; i < texture_load_info.subresource_count; ++i)
					{
						dst_offset = (dst_offset + optimal_buffer_copy_offset_alignment_mask) & ~optimal_buffer_copy_offset_alignment_mask;

						::framework::gpu::device_size const aligned_row_pitch = (texture_load_info.subresource_data[i].row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
						::std::uint32_t const mip_level = texture_load_info.subresource_data[i].mip_level;
						::std::uint32_t mip_width = width >> mip_level;
						::std::uint32_t mip_height = height >> mip_level;

						calculate_subresource_info(row_count, mip_width, mip_height);

						buffer_image_copy[i].buffer_offset = dst_offset;
						buffer_image_copy[i].buffer_row_length = 0;
						buffer_image_copy[i].buffer_image_height = 0;
						buffer_image_copy[i].image_subresource.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
						buffer_image_copy[i].image_subresource.mip_level = mip_level;
						buffer_image_copy[i].image_subresource.base_array_layer = texture_load_info.subresource_data[i].array_layer;
						buffer_image_copy[i].image_subresource.layer_count = 1;
						buffer_image_copy[i].image_offset = { 0, 0, 0 };
						buffer_image_copy[i].image_extent = { mip_width, mip_height, 1u };

						::std::byte const * src_address = reinterpret_cast<::std::byte const *>(texture_load_info.subresource_data[i].data);
						for (::std::uint32_t y = 0; y < row_count; ++y)
						{
							::std::memcpy(reinterpret_cast<::std::byte *>(allocation.mapped_buffer_data) + dst_offset, src_address, texture_load_info.subresource_data[i].row_pitch);
							dst_offset += aligned_row_pitch;
							src_address += texture_load_info.subresource_data[i].row_pitch;
						}
					}
					command_buffer->copy_buffer_to_image(allocation.buffer, this->device_image, ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit, static_cast<::std::uint32_t>(buffer_image_copy.size()), buffer_image_copy.data());

					image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
					image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
					image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
					image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
					image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
					image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
					image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
					image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
					image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
					image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;

					command_buffer->pipeline_barrier(&dependency_info);
					assert_framework_gpu_result(command_buffer->end_command_buffer());

					::framework::gpu::semaphore_submit_info signal_semaphore_info;
					signal_semaphore_info.semaphore = graphics_command_context->get_semaphore();
					signal_semaphore_info.value = 0;
					signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
					signal_semaphore_info.device_index = 0;
					co_await this->command_buffer_manager->get_transfer_queue()->submit(transfer_command_context, 0, nullptr, 1, &signal_semaphore_info);
				}

				{
					::framework::gpu::command_buffer * command_buffer = graphics_command_context->get_command_buffer();

					::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
					command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
					command_buffer_begin_info.inheritance_info = nullptr;
					assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

					::framework::gpu::image_subresource_range subresource_range;
					subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
					subresource_range.base_mip_level = 0;
					subresource_range.level_count = texture_load_info.mip_levels;
					subresource_range.base_array_layer = 0;
					subresource_range.layer_count = 1;

					::framework::gpu::image_memory_barrier image_memory_barrier;
					image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
					image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
					image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
					image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
					image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
					image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
					image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
					image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
					image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
					image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
					image_memory_barrier.image = this->device_image;
					image_memory_barrier.subresource_range = subresource_range;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 0;
					dependency_info.memory_barriers = nullptr;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 1;
					dependency_info.image_memory_barriers = &image_memory_barrier;
					command_buffer->pipeline_barrier(&dependency_info);
					assert_framework_gpu_result(command_buffer->end_command_buffer());

					::framework::gpu::semaphore_submit_info wait_semaphore_info;
					wait_semaphore_info.semaphore = graphics_command_context->get_semaphore();
					wait_semaphore_info.value = 0;
					wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
					wait_semaphore_info.device_index = 0;
					co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 1, &wait_semaphore_info, 0, nullptr);
				}

				co_await this->command_buffer_manager->get_transfer_queue()->release_command_context(transfer_command_context);
				co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);
			};

			::framework::gpu_context * gpu_context;
			::framework::resource::resource_manager * resource_manager;
			::framework::command_buffer_manager * command_buffer_manager;
			::framework::gpu::image * device_image;
			::framework::gpu::device_memory * device_memory;
			::framework::gpu::image_view * image_view;
			::std::uint32_t	image_width;
			::std::uint32_t	image_height;
		} texture_load_callback(this->gpu_context, this->resource_manager, this->command_buffer_manager);

		::framework::resource::texture_load_info texture_load_info;
		texture_load_info.file_name = "texture.jpg";
		texture_load_info.texture_load_callback = &texture_load_callback;
		::framework::coroutine::sync_wait(this->resource_manager->load_texture(texture_load_info));

		::framework::gpu::component_type constexpr component_type = ::framework::gpu::component_type::float16;
		::framework::gpu::component_type constexpr pdf_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type constexpr target_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type constexpr grid_gradient_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type constexpr accumulator_type = ::framework::gpu::component_type::float16;
		char const * element_type = ::framework::gpu::utility::get_component_type_name(component_type);
		::std::size_t constexpr type_size = ::framework::gpu::utility::get_component_type_size(component_type);
		float constexpr learning_rate = 0.01f;
		::std::uint32_t const feature_count = 16u;
		::framework::nn::grid::grid_type const grid_type = ::framework::nn::grid::grid_type::hash;
		::framework::nn::grid::hash_type const hash_type = ::framework::nn::grid::hash_type::coherent_prime;
		::framework::nn::grid::interpolation_type const interpolation_type = ::framework::nn::grid::interpolation_type::linear;
		::std::uint32_t const log2_hashmap_size = 21u;
		::std::uint32_t const base_resolution = 16u;
		float const per_level_scale = 1.5f;
		bool const stochastic_interpolation = false;
		::std::uint32_t const features_per_level = 2u;
		::std::uint32_t const pos_dims = 2u;
		::std::uint32_t const batch_alignment = 256u;
		::std::uint32_t const inference_batch_alignment = 256u;
		::std::uint32_t const batch_size = 1 << 15;
		::std::uint32_t const inference_batch_size = (texture_load_callback.image_width * texture_load_callback.image_height + inference_batch_alignment - 1) / inference_batch_alignment * inference_batch_alignment;
		::framework::linalg::matrix_layout input_layout = ::framework::linalg::matrix_layout::column_major;
		::framework::linalg::matrix_layout output_layout = ::framework::linalg::matrix_layout::column_major;
		::std::uint32_t const input_size = 2;
		::std::uint32_t const encoded_input_size = feature_count; // grid encoding
		::std::uint32_t const output_size = 3;
		::std::uint32_t const padded_input_size = 2;
		::std::uint32_t const padded_output_size = 16;
		::std::uint32_t const input_stride = input_layout == ::framework::linalg::matrix_layout::column_major ? padded_input_size : batch_size;
		::std::uint32_t const hidden_width = 64;
		::std::uint32_t const hidden_layer_count = 2;
		::std::uint32_t const channel_count = 3ul;
		::std::uint32_t const channel_stride = 3ul;

		::framework::gpu::sampler * sampler;
		::framework::gpu::buffer * input_buffer;
		::framework::gpu::device_memory * input_device_memory;
		::framework::gpu::buffer * target_buffer;
		::framework::gpu::device_memory * target_device_memory;
		::framework::gpu::buffer * loss_buffer;
		::framework::gpu::device_memory * loss_device_memory;
		::framework::gpu::buffer * ldr_readback_buffer;
		::framework::gpu::device_memory * ldr_readback_device_memory;
		void * ldr_readback_mapped_data;

		::framework::gpu::buffer * encoded_input_buffer;
		::framework::gpu::device_memory * encoded_input_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = encoded_input_size * ::std::max(batch_size, inference_batch_size) * type_size;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &encoded_input_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = encoded_input_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &encoded_input_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = encoded_input_buffer;
			bind_buffer_memory_info.memory = encoded_input_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}


		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = padded_input_size * ::std::max(batch_size, inference_batch_size) * type_size;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &input_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = input_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &input_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = input_buffer;
			bind_buffer_memory_info.memory = input_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = padded_output_size * batch_size * sizeof(float);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &target_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = target_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &target_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = target_buffer;
			bind_buffer_memory_info.memory = target_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = padded_output_size * batch_size * sizeof(float);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &loss_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = loss_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &loss_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = loss_buffer;
			bind_buffer_memory_info.memory = loss_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = texture_load_callback.image_width * texture_load_callback.image_height * channel_count * sizeof(::std::uint8_t);
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &ldr_readback_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = ldr_readback_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &ldr_readback_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = ldr_readback_buffer;
			bind_buffer_memory_info.memory = ldr_readback_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

			assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(ldr_readback_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &ldr_readback_mapped_data));
		}

		::framework::gpu::device_size const batch_size_buffer_alignment = ((sizeof(::std::uint32_t) + this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1));
		::framework::gpu::buffer * batch_size_buffer;
		::framework::gpu::device_memory * batch_size_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = batch_size_buffer_alignment * 2;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &batch_size_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = batch_size_buffer;
			this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? batch_size_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &batch_size_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = batch_size_buffer;
			bind_buffer_memory_info.memory = batch_size_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::gpu::buffer * dloss_dy_buffer;
		::framework::gpu::device_memory * dloss_dy_device_memory;
		{
			::framework::gpu::buffer_create_info buffer_create_info;
			buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
			buffer_create_info.size = batch_size * feature_count * type_size;
			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			buffer_create_info.queue_family_index_count = 0;
			buffer_create_info.queue_family_indices = nullptr;
			buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
			buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
			buffer_create_info.opaque_capture_address = 0;
			assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &dloss_dy_buffer));

			::framework::gpu::memory_requirements memory_requirements;
			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = dloss_dy_buffer;
			gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? dloss_dy_buffer : nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &dloss_dy_device_memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = dloss_dy_buffer;
			bind_buffer_memory_info.memory = dloss_dy_device_memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		::framework::algorithm::xavier_uniform xavier_uniform(this->gpu_context, this->resource_manager, ::framework::gpu::component_type::float32, component_type);
		::framework::nn::grid::execution_policy encoding_execution_policy(this->gpu_context, this->resource_manager, this->command_buffer_manager, component_type, grid_gradient_type, grid_type, hash_type, interpolation_type, ::std::max(batch_size, inference_batch_size), feature_count, log2_hashmap_size, base_resolution, per_level_scale, stochastic_interpolation, features_per_level, pos_dims, padded_input_size);
		::framework::nn::grid::encoding encoding(&encoding_execution_policy, this->gpu_context, this->resource_manager, component_type, grid_gradient_type);
		::framework::nn::mlp::execution_policy neural_network_execution_policy(this->gpu_context, this->resource_manager, this->gpu_log, component_type, accumulator_type, ::framework::linalg::matrix_layout::row_major, output_layout, encoded_input_size, output_size, encoded_input_size, padded_output_size, hidden_width, hidden_layer_count, ::framework::nn::activation::relu, ::framework::nn::activation::relu, batch_alignment, inference_batch_alignment, batch_size, inference_batch_size, true);
		::framework::nn::mlp::network neural_network(neural_network_execution_policy, this->gpu_context, this->resource_manager, this->gpu_log, component_type, accumulator_type, batch_size, inference_batch_size, encoded_input_buffer, dloss_dy_buffer, this->descriptor_pool);
		::std::uint32_t const parameter_count = neural_network_execution_policy.get_parameter_count();
		::std::uint32_t const encoding_parameter_count = encoding_execution_policy.get_parameter_count();
		::framework::nn::adam optimizer(this->gpu_context, this->resource_manager, component_type, component_type, parameter_count, true, false, 0.0f, 128.0f, learning_rate, 1e-6f);
		::framework::nn::adam encoding_optimizer(this->gpu_context, this->resource_manager, component_type, grid_gradient_type, encoding_parameter_count, false, false, 0.0f, 128.0f, learning_rate, 0.0f);
		::framework::nn::relative_l2 loss(this->gpu_context, this->resource_manager, component_type, pdf_type, target_type, padded_output_size, output_size, 128.0f, true, false);

		::framework::gpu::descriptor_set_layout * evaluate_image_descriptor_set_layout;
		::framework::gpu::pipeline_layout * evaluate_image_pipeline_layout;
		::framework::gpu::pipeline * evaluate_image_pipeline;
		::framework::gpu::pipeline * generate_test_uvs_pipeline;
		::framework::gpu::shader_module * generate_test_uvs_shader_module;
		::framework::gpu::shader_module * evaluate_image_shader_module;
		::framework::gpu::descriptor_set_layout * hdr_to_ldr_descriptor_set_layout;
		::framework::gpu::pipeline_layout * hdr_to_ldr_pipeline_layout;
		::framework::gpu::pipeline * hdr_to_ldr_pipeline;
		::framework::gpu::shader_module * hdr_to_ldr_shader_module;

		{
			::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
			// image
			descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[0].binding = 0;
			descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
			descriptor_set_layout_bindings[0].hlsl_register_space = 0;
			descriptor_set_layout_bindings[0].descriptor_count = 1;
			descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
			descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
			// uvs_buffer
			descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[1].binding = 1;
			descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
			descriptor_set_layout_bindings[1].hlsl_register_space = 0;
			descriptor_set_layout_bindings[1].descriptor_count = 1;
			descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
			// image_texels_buffer
			descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[2].binding = 2;
			descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
			descriptor_set_layout_bindings[2].hlsl_register_space = 0;
			descriptor_set_layout_bindings[2].descriptor_count = 1;
			descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

			::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
			descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
			descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
			descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &evaluate_image_descriptor_set_layout));
		}

		{
			::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
			// input_buffer
			descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[0].binding = 0;
			descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
			descriptor_set_layout_bindings[0].hlsl_register_space = 0;
			descriptor_set_layout_bindings[0].descriptor_count = 1;
			descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
			descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
			// output_buffer
			descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
			descriptor_set_layout_bindings[1].binding = 1;
			descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
			descriptor_set_layout_bindings[1].hlsl_register_space = 0;
			descriptor_set_layout_bindings[1].descriptor_count = 1;
			descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

			::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
			descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
			descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
			descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &hdr_to_ldr_descriptor_set_layout));
		}

		struct hdr_to_ldr_push_constants
		{
			::std::uint32_t pixel_count;
			::std::uint32_t input_stride;
			::std::uint32_t output_stride;
		};

		{
			::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
			{
				evaluate_image_descriptor_set_layout
			};

			::framework::gpu::push_constant_range push_constant_ranges;
			push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			push_constant_ranges.binding = 0;
			push_constant_ranges.offset = 0;
			push_constant_ranges.size = sizeof(::framework::local::evaluate_image_push_constants);
			push_constant_ranges.hlsl_shader_register = 0;
			push_constant_ranges.hlsl_register_space = 0;

			::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
			pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
			pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
			pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
			pipeline_layout_create_info.push_constant_range_count = 1;
			pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &evaluate_image_pipeline_layout));
		}

		{
			::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
			{
				hdr_to_ldr_descriptor_set_layout
			};

			::framework::gpu::push_constant_range push_constant_ranges;
			push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
			push_constant_ranges.binding = 0;
			push_constant_ranges.offset = 0;
			push_constant_ranges.size = sizeof(struct hdr_to_ldr_push_constants);
			push_constant_ranges.hlsl_shader_register = 0;
			push_constant_ranges.hlsl_register_space = 0;

			::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
			pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
			pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
			pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
			pipeline_layout_create_info.push_constant_range_count = 1;
			pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &hdr_to_ldr_pipeline_layout));
		}

		{
			::framework::resource::preprocessor_define const preprocessor_defines[]
			{
				{ "ELEMENT_TYPE", element_type }
			};

			::framework::resource::glsl_source_info glsl_source_info;
			glsl_source_info.file_name = "evaluate_image.comp.glsl";
			glsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = nullptr;
			shader_stage_info.glsl_source_info = &glsl_source_info;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			evaluate_image_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::preprocessor_define const preprocessor_defines[]
			{
				{ "ELEMENT_TYPE", element_type }
			};

			::framework::resource::glsl_source_info glsl_source_info;
			glsl_source_info.file_name = "generate_test_uvs.comp.glsl";
			glsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = nullptr;
			shader_stage_info.glsl_source_info = &glsl_source_info;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			generate_test_uvs_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::preprocessor_define const preprocessor_defines[]
			{
				{ "ELEMENT_TYPE", element_type }
			};

			::framework::resource::glsl_source_info glsl_source_info;
			glsl_source_info.file_name = "hdr_to_ldr.comp.glsl";
			glsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = nullptr;
			shader_stage_info.glsl_source_info = &glsl_source_info;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			hdr_to_ldr_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
			pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
			pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
			pipeline_shader_stage_create_info.module = evaluate_image_shader_module;
			pipeline_shader_stage_create_info.name = "main";
			pipeline_shader_stage_create_info.specialization_info = nullptr;

			::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
			compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
			compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
			compute_pipeline_create_info.layout = evaluate_image_pipeline_layout;
			compute_pipeline_create_info.base_pipeline = nullptr;
			compute_pipeline_create_info.base_pipeline_index = -1;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &evaluate_image_pipeline));
		}

		{
			::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
			pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
			pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
			pipeline_shader_stage_create_info.module = generate_test_uvs_shader_module;
			pipeline_shader_stage_create_info.name = "main";
			pipeline_shader_stage_create_info.specialization_info = nullptr;

			::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
			compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
			compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
			compute_pipeline_create_info.layout = evaluate_image_pipeline_layout;
			compute_pipeline_create_info.base_pipeline = nullptr;
			compute_pipeline_create_info.base_pipeline_index = -1;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &generate_test_uvs_pipeline));
		}

		{
			::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
			pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
			pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
			pipeline_shader_stage_create_info.module = hdr_to_ldr_shader_module;
			pipeline_shader_stage_create_info.name = "main";
			pipeline_shader_stage_create_info.specialization_info = nullptr;

			::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
			compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
			compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
			compute_pipeline_create_info.layout = hdr_to_ldr_pipeline_layout;
			compute_pipeline_create_info.base_pipeline = nullptr;
			compute_pipeline_create_info.base_pipeline_index = -1;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &hdr_to_ldr_pipeline));
		}

		{
			::framework::gpu::sampler_create_info sampler_create_info;
			sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
			sampler_create_info.mag_filter = ::framework::gpu::filter::nearest;
			sampler_create_info.min_filter = ::framework::gpu::filter::nearest;
			sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::nearest;
			sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::clamp_to_edge;
			sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::clamp_to_edge;
			sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::clamp_to_edge;
			sampler_create_info.mip_lod_bias = 0.0f;
			sampler_create_info.anisotropy_enable = false;
			sampler_create_info.max_anisotropy = 0.0f;
			sampler_create_info.compare_enable = false;
			sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
			sampler_create_info.min_lod = 0.0f;
			sampler_create_info.max_lod = 0.0f;
			sampler_create_info.border_color = ::framework::gpu::border_color::int_transparent_black;
			sampler_create_info.unnormalized_coordinates = false;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_sampler(&sampler_create_info, nullptr, &sampler));
		}

		::framework::gpu::descriptor_set * network_descriptor_set;
		::framework::gpu::descriptor_set * network_initialize_parameters_descriptor_set;
		::framework::gpu::descriptor_set * loss_descriptor_set;
		::framework::gpu::descriptor_set * optimizer_descriptor_set;
		::framework::gpu::descriptor_set * evaluate_image_descriptor_set;
		::framework::gpu::descriptor_set * hdr_to_ldr_descriptor_set;
		::framework::gpu::descriptor_set * encoding_descriptor_set;
		::framework::gpu::descriptor_set * encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * encoding_optimizer_descriptor_set;
		::framework::gpu::descriptor_set * xavier_uniform_descriptor_set;
		{
			::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
			{
				neural_network_execution_policy.get_descriptor_set_layout(),
				neural_network_execution_policy.get_initialize_parameters_descriptor_set_layout(),
				loss.get_descriptor_set_layout(),
				optimizer.get_descriptor_set_layout(),
				evaluate_image_descriptor_set_layout,
				hdr_to_ldr_descriptor_set_layout,
				encoding_execution_policy.get_descriptor_set_layout(),
				encoding_execution_policy.get_initialize_dispatch_indirect_command_descriptor_set_layout(),
				xavier_uniform.get_descriptor_set_layout(),
				encoding_optimizer.get_descriptor_set_layout()
			};
			::framework::gpu::descriptor_set * descriptor_sets[static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts))];
			::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
			descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
			descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
			descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
			descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));
			network_descriptor_set = descriptor_sets[0], network_initialize_parameters_descriptor_set = descriptor_sets[1], loss_descriptor_set = descriptor_sets[2];
			optimizer_descriptor_set = descriptor_sets[3], evaluate_image_descriptor_set = descriptor_sets[4], hdr_to_ldr_descriptor_set = descriptor_sets[5];
			encoding_descriptor_set = descriptor_sets[6], encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[7], xavier_uniform_descriptor_set = descriptor_sets[8];
			encoding_optimizer_descriptor_set = descriptor_sets[9];
		}

		{
			::framework::gpu::descriptor_image_info descriptor_image_info;
			descriptor_image_info.sampler = sampler;
			descriptor_image_info.image_view = texture_load_callback.image_view;
			descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;

			::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
			descriptor_buffer_infos[0].buffer = input_buffer;
			descriptor_buffer_infos[0].offset = 0;
			descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[0].stride = 2;

			descriptor_buffer_infos[1].buffer = target_buffer;
			descriptor_buffer_infos[1].offset = 0;
			descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[1].stride = 2;

			::framework::gpu::write_descriptor_set write_descriptor_sets[3];
			// image
			write_descriptor_sets[0].dst_set = evaluate_image_descriptor_set;
			write_descriptor_sets[0].dst_binding = 0;
			write_descriptor_sets[0].dst_array_element = 0;
			write_descriptor_sets[0].descriptor_count = 1;
			write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
			write_descriptor_sets[0].image_info = &descriptor_image_info;
			write_descriptor_sets[0].buffer_info = nullptr;
			write_descriptor_sets[0].texel_buffer_view = nullptr;
			write_descriptor_sets[0].acceleration_structures = nullptr;
			// uvs_buffer
			write_descriptor_sets[1].dst_set = evaluate_image_descriptor_set;
			write_descriptor_sets[1].dst_binding = 1;
			write_descriptor_sets[1].dst_array_element = 0;
			write_descriptor_sets[1].descriptor_count = 1;
			write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			write_descriptor_sets[1].image_info = nullptr;
			write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[0];
			write_descriptor_sets[1].texel_buffer_view = nullptr;
			write_descriptor_sets[1].acceleration_structures = nullptr;
			// image_texels_buffer
			write_descriptor_sets[2].dst_set = evaluate_image_descriptor_set;
			write_descriptor_sets[2].dst_binding = 2;
			write_descriptor_sets[2].dst_array_element = 0;
			write_descriptor_sets[2].descriptor_count = 1;
			write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			write_descriptor_sets[2].image_info = nullptr;
			write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
			write_descriptor_sets[2].texel_buffer_view = nullptr;
			write_descriptor_sets[2].acceleration_structures = nullptr;
			this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
		}

		{
			::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
			descriptor_buffer_infos[0].buffer = neural_network.get_output_buffer();
			descriptor_buffer_infos[0].offset = 0;
			descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[0].stride = 2;

			descriptor_buffer_infos[1].buffer = ldr_readback_buffer;
			descriptor_buffer_infos[1].offset = 0;
			descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[1].stride = 2;

			::framework::gpu::write_descriptor_set write_descriptor_sets[2];
			// input_buffer
			write_descriptor_sets[0].dst_set = hdr_to_ldr_descriptor_set;
			write_descriptor_sets[0].dst_binding = 0;
			write_descriptor_sets[0].dst_array_element = 0;
			write_descriptor_sets[0].descriptor_count = 1;
			write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
			write_descriptor_sets[0].image_info = nullptr;
			write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
			write_descriptor_sets[0].texel_buffer_view = nullptr;
			write_descriptor_sets[0].acceleration_structures = nullptr;
			// output_buffer
			write_descriptor_sets[1].dst_set = hdr_to_ldr_descriptor_set;
			write_descriptor_sets[1].dst_binding = 1;
			write_descriptor_sets[1].dst_array_element = 0;
			write_descriptor_sets[1].descriptor_count = 1;
			write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			write_descriptor_sets[1].image_info = nullptr;
			write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
			write_descriptor_sets[1].texel_buffer_view = nullptr;
			write_descriptor_sets[1].acceleration_structures = nullptr;
			this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
		}

		neural_network.update_descriptor_sets(this->gpu_context, encoded_input_buffer, dloss_dy_buffer, batch_size_buffer, network_descriptor_set);
		neural_network.update_initialize_parameters_descriptor_sets(this->gpu_context, network_initialize_parameters_descriptor_set);
		loss.update_descriptor_sets(this->gpu_context, batch_size_buffer, batch_size_buffer_alignment, neural_network.get_output_buffer(), target_buffer, target_buffer, loss_buffer, neural_network.get_dloss_doutput_buffer(), loss_descriptor_set);
		optimizer.update_descriptor_sets(this->gpu_context, neural_network.get_full_precision_weights_matrices_buffer(), neural_network.get_weight_matrices_buffer(), neural_network.get_gradient_matrices_buffer(), optimizer_descriptor_set);
		encoding_optimizer.update_descriptor_sets(this->gpu_context, encoding.get_full_precision_parameter_buffer(), encoding.get_parameter_buffer(), encoding.get_gradient_buffer(), encoding_optimizer_descriptor_set);
		encoding.update_descriptor_sets(this->gpu_context->get_device(), batch_size_buffer, input_buffer, dloss_dy_buffer, encoded_input_buffer, encoding_descriptor_set);
		xavier_uniform.update_descriptor_sets(this->gpu_context, encoding.get_full_precision_parameter_buffer(), encoding.get_parameter_buffer(), xavier_uniform_descriptor_set);

		{
			::framework::command_context * const command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const command_buffer = command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			co_await command_context->update_buffer(batch_size_buffer, &inference_batch_size, 0, sizeof(inference_batch_size));
			co_await command_context->update_buffer(batch_size_buffer, &batch_size, batch_size_buffer_alignment, sizeof(batch_size));

			xavier_uniform.execute(command_buffer, xavier_uniform_descriptor_set, 0, encoding_parameter_count, encoding_parameter_count);
			neural_network.initialize_xavier_uniform(command_context->get_command_buffer(), network_initialize_parameters_descriptor_set);
			co_await neural_network_execution_policy.update_parameters(this->gpu_context, command_context, batch_size, inference_batch_size);

			assert_framework_gpu_result(command_context->get_command_buffer()->end_command_buffer());

			co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_compute_queue()->release_command_context(command_context);
		}

		::std::random_device random_device;
		::std::default_random_engine default_random_engine(random_device());
		::std::uniform_int_distribution<::std::uint32_t> uniform_int_distribution;

		for (::std::uint32_t i = 0; i < 10000; i++)
		{
			::std::uint32_t const epoch = i + 1;
			bool const do_validation = epoch % 1000 == 0;
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_compute_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

			this->gpu_log->reset(command_buffer, i % 3, {}, {});

			{
				::framework::local::evaluate_image_push_constants push_constants
				{
					.width = texture_load_callback.image_width,
					.height = texture_load_callback.image_height,
					.batch_size = batch_size,
					.padded_input_size = padded_input_size,
					.input_stride = input_stride,
					.output_stride = channel_stride,
					.seed = uniform_int_distribution(random_device),
					.input_layout = static_cast<::std::uint32_t>(input_layout)
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, evaluate_image_pipeline);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, evaluate_image_pipeline_layout, 0, 1, &evaluate_image_descriptor_set, 0, nullptr);
				command_buffer->push_constants(evaluate_image_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch((batch_size + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}

			{
				::std::uint32_t const batch_size_buffer_dynamic_offset = batch_size_buffer_alignment;
				encoding.forward(command_buffer, encoding_descriptor_set, batch_size_buffer_dynamic_offset, batch_size);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}

			neural_network.forward(command_buffer, network_descriptor_set, batch_size);
			loss.evaluate(command_buffer, loss_descriptor_set, padded_output_size * batch_size);
			neural_network.backward(command_buffer, network_descriptor_set, batch_size);
			{
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				::std::uint32_t const batch_size_buffer_dynamic_offset = batch_size_buffer_alignment;
				encoding.backward(command_buffer, encoding_descriptor_set, batch_size_buffer_dynamic_offset, batch_size);
			}
			optimizer.step(command_buffer, optimizer_descriptor_set);
			encoding_optimizer.step(command_buffer, encoding_optimizer_descriptor_set);

			::std::uint32_t const pixel_count = texture_load_callback.image_width * texture_load_callback.image_height;

			if (do_validation)
			{
				::framework::local::evaluate_image_push_constants push_constants
				{
					.width = texture_load_callback.image_width,
					.height = texture_load_callback.image_height,
					.batch_size = pixel_count,
					.padded_input_size = padded_input_size,
					.input_stride = input_stride,
					.output_stride = channel_stride,
					.seed = 0ull,
					.input_layout = static_cast<::std::uint32_t>(input_layout)
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, generate_test_uvs_pipeline);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, evaluate_image_pipeline_layout, 0, 1, &evaluate_image_descriptor_set, 0, nullptr);
				command_buffer->push_constants(evaluate_image_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch((pixel_count + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}

				{
					::std::uint32_t const batch_size_buffer_dynamic_offset = 0;
					encoding.forward(command_buffer, encoding_descriptor_set, batch_size_buffer_dynamic_offset, inference_batch_size);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
				}
				neural_network.inference(command_buffer, network_descriptor_set, inference_batch_size);
			}

			if (do_validation)
			{
				struct hdr_to_ldr_push_constants push_constants
				{
					.pixel_count = pixel_count,
						.input_stride = padded_output_size,
						.output_stride = channel_stride
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, hdr_to_ldr_pipeline);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, hdr_to_ldr_pipeline_layout, 0, 1, &hdr_to_ldr_descriptor_set, 0, nullptr);
				command_buffer->push_constants(hdr_to_ldr_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch((pixel_count + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}

			this->gpu_log->copy_gpu_log(command_buffer);

			assert_framework_gpu_result(command_buffer->end_command_buffer());

			co_await this->command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);

			this->gpu_log->print_gpu_log(i % 3);

			if (do_validation)
			{
				::framework::gpu::mapped_memory_range mapped_memory_range;
				mapped_memory_range.memory = ldr_readback_device_memory;
				mapped_memory_range.offset = 0;
				mapped_memory_range.size = ::framework::gpu::whole_size;
				assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

				::std::printf("MLP epoch %i complete\n", epoch);
				::std::filesystem::path const output_path = ::framework::local::tests_directory / ::std::format("mlp_{}.jpg", epoch);
				::stbi_write_jpg(output_path.string().c_str(), texture_load_callback.image_width, texture_load_callback.image_height, channel_count, ldr_readback_mapped_data, 100);
			}
		}

		::framework::gpu::descriptor_set * descriptor_sets[]
		{
			network_descriptor_set,
			network_initialize_parameters_descriptor_set
		};

		this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets);

		co_return true;
	}());
}

struct ::framework::test::test_base::image_dataset(::framework::test::test_base::load_train_dataset)(char const * file_name)
{
	struct texture_load_callback_t : public ::framework::resource::texture_load_callback
	{
		explicit texture_load_callback_t(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager) :
			gpu_context(gpu_context), resource_manager(resource_manager), command_buffer_manager(command_buffer_manager)
		{
		}

		::framework::coroutine::lazy_task<void> operator()(::framework::resource::texture_load_callback::texture_load_info const & texture_load_info) override
		{
			::std::uint32_t	width = this->image_width = texture_load_info.width;
			::std::uint32_t	height = this->image_height = texture_load_info.height;
			::framework::gpu::device_size row_count;
			bool const is_texture_compression_bc_format = ::framework::gpu::utility::is_texture_compression_bc_format(texture_load_info.format);
			bool const texture_compression_bc_unaligned = this->gpu_context->get_physical_device_features().texture_compression_bc_unaligned;

			auto const calculate_subresource_info = [is_texture_compression_bc_format, texture_compression_bc_unaligned](::framework::gpu::device_size & row_count, ::std::uint32_t & width, ::std::uint32_t & height)
			{
				if (is_texture_compression_bc_format)
				{
					row_count = height / 4;

					if (row_count == 0)
					{
						row_count = 1;
					}

					if (!texture_compression_bc_unaligned)
					{
						width = (width + 3) & ~3;
						height = (height + 3) & ~3;
					}
				}
				else
				{
					row_count = height;
				}
			};

			calculate_subresource_info(row_count, width, height);

			::framework::gpu::image_create_info image_create_info;
			image_create_info.flags = ::framework::gpu::image_create_flags::none;
			image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
			image_create_info.format = texture_load_info.format;
			image_create_info.extent.width = width;
			image_create_info.extent.height = height;
			image_create_info.extent.depth = 1;
			image_create_info.mip_levels = texture_load_info.mip_levels;
			image_create_info.array_layers = 1;
			image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
			image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
			image_create_info.usage = ::framework::gpu::image_usage_flags::transfer_dst_bit | ::framework::gpu::image_usage_flags::sampled_bit;
			image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
			image_create_info.queue_family_index_count = 0;
			image_create_info.queue_family_indices = nullptr;
			image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
			image_create_info.initial_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_create_info.optimized_clear_value = nullptr;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->image));

			::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
			debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
			debug_utils_object_name_info.object = this->image;
			debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::image";
			assert_framework_gpu_result(this->gpu_context->get_device()->set_object_name(&debug_utils_object_name_info));

			::framework::gpu::memory_requirements memory_requirements;

			::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
			image_memory_requirements_info.image = this->image;
			this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->device_memory));

			::framework::gpu::bind_image_memory_info bind_image_memory_info;
			bind_image_memory_info.image = this->image;
			bind_image_memory_info.memory = this->device_memory;
			bind_image_memory_info.memory_offset = 0;
			assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

			::framework::gpu::image_view_create_info image_view_create_info;
			image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
			image_view_create_info.image = this->image;
			image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
			image_view_create_info.format = texture_load_info.format;
			image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			image_view_create_info.subresource_range.base_mip_level = 0;
			image_view_create_info.subresource_range.level_count = texture_load_info.mip_levels;
			image_view_create_info.subresource_range.base_array_layer = 0;
			image_view_create_info.subresource_range.layer_count = 1;
			image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
			image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
			image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
			image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->image_view));

			::framework::command_context * const transfer_command_context = co_await this->command_buffer_manager->get_transfer_queue()->acquire_command_context();
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
			{
				::framework::gpu::command_buffer * command_buffer = transfer_command_context->get_command_buffer();

				::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
				command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
				command_buffer_begin_info.inheritance_info = nullptr;
				assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

				::framework::gpu::image_subresource_range subresource_range;
				subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
				subresource_range.base_mip_level = 0;
				subresource_range.level_count = texture_load_info.mip_levels;
				subresource_range.base_array_layer = 0;
				subresource_range.layer_count = 1;

				::framework::gpu::image_memory_barrier image_memory_barrier;
				image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::none;
				image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
				image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
				image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
				image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
				image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
				image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
				image_memory_barrier.image = this->image;
				image_memory_barrier.subresource_range = subresource_range;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 1;
				dependency_info.image_memory_barriers = &image_memory_barrier;

				command_buffer->pipeline_barrier(&dependency_info);

				::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment = this->gpu_context->get_physical_device_properties().limits.optimal_buffer_copy_row_pitch_alignment;
				::framework::gpu::device_size const optimal_buffer_copy_offset_alignment = this->gpu_context->get_physical_device_properties().limits.optimal_buffer_copy_offset_alignment;
				::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment_mask = optimal_buffer_copy_row_pitch_alignment - 1;
				::framework::gpu::device_size const optimal_buffer_copy_offset_alignment_mask = optimal_buffer_copy_offset_alignment - 1;
				::framework::gpu::device_size const required_size = ::std::accumulate(texture_load_info.subresource_data, texture_load_info.subresource_data + texture_load_info.subresource_count, ::framework::gpu::device_size(0),
					[&](::framework::gpu::device_size const size, ::framework::resource::texture_load_callback::subresource_data const & subresource_info)
					{
						::framework::gpu::device_size const aligned_row_pitch = (subresource_info.row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
						::std::uint32_t const mip_level = subresource_info.mip_level;
						::std::uint32_t mip_width = width >> mip_level;
						::std::uint32_t mip_height = height >> mip_level;
						calculate_subresource_info(row_count, mip_width, mip_height);
						return size + row_count * aligned_row_pitch + optimal_buffer_copy_offset_alignment_mask;
					}
				);
				::framework::linear_allocator::allocation const allocation = co_await transfer_command_context->get_linear_allocator().allocate(required_size, 1);
				::framework::gpu::device_size dst_offset = allocation.offset;
				::std::vector<::framework::gpu::buffer_image_copy> buffer_image_copy(texture_load_info.subresource_count);
				for (::std::uint32_t i = 0; i < texture_load_info.subresource_count; ++i)
				{
					dst_offset = (dst_offset + optimal_buffer_copy_offset_alignment_mask) & ~optimal_buffer_copy_offset_alignment_mask;

					::framework::gpu::device_size const aligned_row_pitch = (texture_load_info.subresource_data[i].row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
					::std::uint32_t const mip_level = texture_load_info.subresource_data[i].mip_level;
					::std::uint32_t mip_width = width >> mip_level;
					::std::uint32_t mip_height = height >> mip_level;

					calculate_subresource_info(row_count, mip_width, mip_height);

					buffer_image_copy[i].buffer_offset = dst_offset;
					buffer_image_copy[i].buffer_row_length = 0;
					buffer_image_copy[i].buffer_image_height = 0;
					buffer_image_copy[i].image_subresource.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
					buffer_image_copy[i].image_subresource.mip_level = mip_level;
					buffer_image_copy[i].image_subresource.base_array_layer = texture_load_info.subresource_data[i].array_layer;
					buffer_image_copy[i].image_subresource.layer_count = 1;
					buffer_image_copy[i].image_offset = { 0, 0, 0 };
					buffer_image_copy[i].image_extent = { mip_width, mip_height, 1u };

					::std::byte const * src_address = reinterpret_cast<::std::byte const *>(texture_load_info.subresource_data[i].data);
					for (::std::uint32_t y = 0; y < row_count; ++y)
					{
						::std::memcpy(reinterpret_cast<::std::byte *>(allocation.mapped_buffer_data) + dst_offset, src_address, texture_load_info.subresource_data[i].row_pitch);
						dst_offset += aligned_row_pitch;
						src_address += texture_load_info.subresource_data[i].row_pitch;
					}
				}
				command_buffer->copy_buffer_to_image(allocation.buffer, this->image, ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit, static_cast<::std::uint32_t>(buffer_image_copy.size()), buffer_image_copy.data());

				image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
				image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
				image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
				image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
				image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
				image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
				image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
				image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
				image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;

				command_buffer->pipeline_barrier(&dependency_info);
				assert_framework_gpu_result(command_buffer->end_command_buffer());

				::framework::gpu::semaphore_submit_info signal_semaphore_info;
				signal_semaphore_info.semaphore = graphics_command_context->get_semaphore();
				signal_semaphore_info.value = 0;
				signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				signal_semaphore_info.device_index = 0;
				co_await this->command_buffer_manager->get_transfer_queue()->submit(transfer_command_context, 0, nullptr, 1, &signal_semaphore_info);
			}

			{
				::framework::gpu::command_buffer * command_buffer = graphics_command_context->get_command_buffer();

				::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
				command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
				command_buffer_begin_info.inheritance_info = nullptr;
				assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

				::framework::gpu::image_subresource_range subresource_range;
				subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
				subresource_range.base_mip_level = 0;
				subresource_range.level_count = texture_load_info.mip_levels;
				subresource_range.base_array_layer = 0;
				subresource_range.layer_count = 1;

				::framework::gpu::image_memory_barrier image_memory_barrier;
				image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
				image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
				image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
				image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
				image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
				image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
				image_memory_barrier.src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
				image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
				image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
				image_memory_barrier.image = this->image;
				image_memory_barrier.subresource_range = subresource_range;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 1;
				dependency_info.image_memory_barriers = &image_memory_barrier;
				command_buffer->pipeline_barrier(&dependency_info);
				assert_framework_gpu_result(command_buffer->end_command_buffer());

				::framework::gpu::semaphore_submit_info wait_semaphore_info;
				wait_semaphore_info.semaphore = graphics_command_context->get_semaphore();
				wait_semaphore_info.value = 0;
				wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				wait_semaphore_info.device_index = 0;
				co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 1, &wait_semaphore_info, 0, nullptr);
			}
			co_await this->command_buffer_manager->get_transfer_queue()->release_command_context(transfer_command_context);
			co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);
		};

		::framework::gpu_context * gpu_context;
		::framework::resource::resource_manager * resource_manager;
		::framework::command_buffer_manager * command_buffer_manager;
		::std::uint32_t	image_width;
		::std::uint32_t	image_height;
		::framework::gpu::image * image;
		::framework::gpu::device_memory * device_memory;
		::framework::gpu::image_view * image_view;
	} texture_load_callback(this->gpu_context, this->resource_manager, this->command_buffer_manager);

	::framework::resource::texture_load_info texture_load_info;
	texture_load_info.file_name = file_name;
	texture_load_info.texture_load_callback = &texture_load_callback;
	::framework::coroutine::sync_wait(this->resource_manager->load_texture(texture_load_info));

	return
	{
		.width = texture_load_callback.image_width,
		.height = texture_load_callback.image_height,
		.image = texture_load_callback.image,
		.device_memory = texture_load_callback.device_memory,
		.image_view = texture_load_callback.image_view
	};
}
