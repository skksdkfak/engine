#pragma once

#include "algorithm/xavier_uniform.hpp"
#include "command_context.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "linalg.hpp"
#include "normalizing_flows/normalizing_flow.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "nn/losses/cross_entropy.hpp"
#include "nn/losses/relative_l2.hpp"
#include "nn/mlp/network.hpp"
#include "nn/optimizers/adam.hpp"
#include "nn/optimizers/sgd.hpp"
#include "nn/grid/encoding.hpp"
#include "nn/one_blob/encoding.hpp"
#include "profiler.hpp"
#include "resource/resource_manager.hpp"
#include "test/test_base.hpp"

namespace framework::test
{
	class normalizing_flow_test : public ::framework::test::test_base
	{
	public:
		normalizing_flow_test(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log);

		~normalizing_flow_test();

		bool test_normalizing_flow(bool indirect, bool validate);

	protected:
		::std::uint32_t const coupling_layer_count = 2u;
		::std::uint32_t const spatial_feature_count = 3u;
		::std::uint32_t const spatial_feature_stride = 4u;
		::std::uint32_t const hidden_width = 64u;
		::std::uint32_t const hidden_layer_count = 2u;
		float const learning_rate = 0.005f;
		::framework::normalizing_flows::transform_type const transform_type = ::framework::normalizing_flows::transform_type::piecewise_rational_quadratic;
		::framework::gpu::component_type const parameter_type = ::framework::gpu::component_type::float16;
		::framework::gpu::component_type const target_type = ::framework::gpu::component_type::float32;
		::framework::gpu::component_type const accumulator_type = ::framework::gpu::component_type::float32;
		::framework::gpu::format const sample_vertex_format = ::framework::gpu::format::r16g16_sfloat;
		char const * const type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);
		::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
		::std::uint32_t const pdf_channel_count = 1ul;
		::std::uint32_t const samples_channel_count = 1ul;
		::framework::gpu::format const sample_image_format = ::framework::gpu::format::r8_unorm;
		::std::uint32_t const samples_image_width = 1024;
		::std::uint32_t const samples_image_height = 1024;
		::std::uint32_t const pdf_image_width = 512;
		::std::uint32_t const pdf_image_height = 512;
		::std::uint32_t const batch_alignment = 256u;
		::std::uint32_t const inference_batch_alignment = 256u;
		::std::uint32_t const forward_inference_batch_size = pdf_image_width * pdf_image_height;
		::std::uint32_t const backward_inference_batch_size = pdf_image_width * pdf_image_height;
		::std::uint32_t const max_batch_size = 1u << 15;
		::std::uint32_t const max_inference_batch_size = forward_inference_batch_size;
		float const exposure = 0.5f;
		float const gamma = 2.2f;
		struct ::framework::test::test_base::image_dataset image_dataset;
		::std::unique_ptr<::framework::normalizing_flows::normalizing_flow> normalizing_flow;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline * generate_train_data_pipeline;
		::framework::gpu::pipeline * generate_forward_test_samples_pipeline;
		::framework::gpu::pipeline * generate_backward_test_samples_pipeline;
		::framework::gpu::pipeline * plot_pdf_pipeline;
		::framework::gpu::pipeline * draw_samples_pipeline;
		::framework::gpu::render_pass * draw_samples_render_pass;
		::framework::gpu::render_pass_state * draw_samples_render_pass_state;
		::framework::gpu::frame_buffer * samples_frame_buffer;
		::framework::gpu::shader_module * generate_train_data_shader_module;
		::framework::gpu::shader_module * generate_forward_test_samples_shader_module;
		::framework::gpu::shader_module * generate_backward_test_samples_shader_module;
		::framework::gpu::shader_module * plot_pdf_shader_module;
		::framework::gpu::shader_module * point_vertex_shader_module;
		::framework::gpu::shader_module * point_fragment_shader_module;
		::framework::gpu::buffer * features_buffer;
		::framework::gpu::device_memory * features_device_memory;
		::framework::gpu::buffer * target_buffer;
		::framework::gpu::device_memory * target_device_memory;
		::framework::gpu::image * samples_image;
		::framework::gpu::image_view * samples_image_view;
		::framework::gpu::device_memory * samples_device_memory;
		::framework::gpu::buffer * samples_readback_buffer;
		::framework::gpu::device_memory * samples_readback_device_memory;
		::framework::gpu::buffer * pdf_readback_buffer;
		::framework::gpu::device_memory * pdf_readback_device_memory;
		::framework::gpu::buffer * batch_size_buffer;
		::framework::gpu::device_memory * batch_size_device_memory;
		void * samples_readback_mapped_data;
		void * pdf_readback_mapped_data;
	};
}