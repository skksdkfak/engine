#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "gpu/utility.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include "test/neural_network.hpp"
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>
#include <algorithm>
#include <array>
#include <cinttypes>
#include <cstdio>
#include <format>
#include <iostream>
#include <numeric>
#include <random>

namespace framework::local
{
	namespace
	{
		::std::filesystem::path const tests_directory = "tests";

		struct evaluate_image_push_constants
		{
			::std::uint32_t width;
			::std::uint32_t height;
			::std::uint32_t batch_size;
			::std::uint32_t padded_input_size;
			::std::uint32_t input_stride;
			::std::uint32_t output_stride;
			::std::uint32_t seed;
			::std::uint32_t input_layout;
		};

		struct hdr_to_ldr_push_constants
		{
			::std::uint32_t pixel_count;
			::std::uint32_t input_stride;
			::std::uint32_t output_stride;
		};
	}
}

::framework::test::neural_network::neural_network(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log, ::std::uint32_t encoded_input_size) :
	::framework::test::test_base(gpu_context, resource_manager, command_buffer_manager, profiler, gpu_log)
{
	this->batch_size_buffer_alignment = ::framework::numeric::align_up<::framework::gpu::device_size>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	this->dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(sizeof(::framework::gpu::dispatch_indirect_command), this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);

	this->image_dataset = this->load_train_dataset("texture.jpg");

	char const * element_type = ::framework::gpu::utility::get_component_type_name(this->component_type);
	::std::size_t const type_size = ::framework::gpu::utility::get_component_type_size(this->component_type);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->padded_input_size * ::std::max(this->batch_size, this->inference_batch_size) * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->input_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->input_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->input_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->input_buffer;
		bind_buffer_memory_info.memory = this->input_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = encoded_input_size * ::std::max(this->batch_size, this->inference_batch_size) * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->encoded_input_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->encoded_input_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->encoded_input_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->encoded_input_buffer;
		bind_buffer_memory_info.memory = this->encoded_input_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->padded_output_size * this->batch_size * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->target_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->target_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->target_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->target_buffer;
		bind_buffer_memory_info.memory = this->target_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->padded_output_size * this->batch_size * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->loss_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->loss_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->loss_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->loss_buffer;
		bind_buffer_memory_info.memory = this->loss_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->image_dataset.width * this->image_dataset.height * channel_count * sizeof(::std::uint8_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->ldr_readback_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->ldr_readback_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->ldr_readback_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->ldr_readback_buffer;
		bind_buffer_memory_info.memory = this->ldr_readback_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(this->ldr_readback_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->ldr_readback_mapped_data));
	}

	if (indirect)
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::gpu::dispatch_indirect_command) * 2 + this->dispatch_indirect_command_aligned_offset; // loss + 2 grid_encoding's dispatches
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dispatch_indirect_command_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dispatch_indirect_command_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dispatch_indirect_command_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dispatch_indirect_command_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dispatch_indirect_command_buffer;
		bind_buffer_memory_info.memory = this->dispatch_indirect_command_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->batch_size_buffer_alignment * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->batch_size_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->batch_size_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->batch_size_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->batch_size_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->batch_size_buffer;
		bind_buffer_memory_info.memory = this->batch_size_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->batch_size * encoded_input_size * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dloss_dy_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dloss_dy_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dloss_dy_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dloss_dy_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dloss_dy_buffer;
		bind_buffer_memory_info.memory = this->dloss_dy_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	this->xavier_uniform = ::std::make_unique<::framework::algorithm::xavier_uniform>(this->gpu_context, this->resource_manager, ::framework::gpu::component_type::float32, this->component_type);
	this->neural_network_execution_policy = ::std::make_unique<::framework::nn::fully_fused_mlp::execution_policy>(this->gpu_context, this->resource_manager, this->gpu_log, this->component_type, this->accumulator_type, this->network_input_layout, this->output_layout, encoded_input_size, this->output_size, encoded_input_size, this->padded_output_size, this->hidden_width, this->hidden_layer_count, ::framework::nn::activation::relu, ::framework::nn::activation::exponential, this->batch_alignment, this->inference_batch_alignment, this->batch_size, this->inference_batch_size, true);
	this->network = ::std::make_unique<::framework::nn::fully_fused_mlp::network>(*this->neural_network_execution_policy, this->gpu_context, this->resource_manager, this->gpu_log, this->component_type, this->accumulator_type, this->batch_size, this->inference_batch_size, this->encoded_input_buffer, this->dloss_dy_buffer, this->descriptor_pool);
	this->optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, this->resource_manager, this->component_type, this->component_type, this->neural_network_execution_policy->get_parameter_count(), true, false, 0.0f, 128.0f, this->learning_rate, 1e-07f);
	this->loss = ::std::make_unique<::framework::nn::relative_l2>(this->gpu_context, this->resource_manager, this->component_type, this->pdf_type, this->target_type, this->padded_output_size, this->output_size, 128.0f, true, false);

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// image
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// uvs_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// image_texels_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->evaluate_image_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// this->input_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// output_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->hdr_to_ldr_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->evaluate_image_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::framework::local::evaluate_image_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->evaluate_image_pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->hdr_to_ldr_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::framework::local::hdr_to_ldr_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->hdr_to_ldr_pipeline_layout));
	}

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", element_type }
		};

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "evaluate_image.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		evaluate_image_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", element_type }
		};

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "generate_test_uvs.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		generate_test_uvs_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", element_type }
		};

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "hdr_to_ldr.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		hdr_to_ldr_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = evaluate_image_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->evaluate_image_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->evaluate_image_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = generate_test_uvs_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->evaluate_image_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &generate_test_uvs_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = hdr_to_ldr_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->hdr_to_ldr_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &hdr_to_ldr_pipeline));
	}

	{
		::framework::gpu::sampler_create_info sampler_create_info;
		sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
		sampler_create_info.mag_filter = ::framework::gpu::filter::nearest;
		sampler_create_info.min_filter = ::framework::gpu::filter::nearest;
		sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::nearest;
		sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::clamp_to_edge;
		sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::clamp_to_edge;
		sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::clamp_to_edge;
		sampler_create_info.mip_lod_bias = 0.0f;
		sampler_create_info.anisotropy_enable = false;
		sampler_create_info.max_anisotropy = 0.0f;
		sampler_create_info.compare_enable = false;
		sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
		sampler_create_info.min_lod = 0.0f;
		sampler_create_info.max_lod = 0.0f;
		sampler_create_info.border_color = ::framework::gpu::border_color::int_transparent_black;
		sampler_create_info.unnormalized_coordinates = false;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_sampler(&sampler_create_info, nullptr, &sampler));
	}

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->xavier_uniform->get_descriptor_set_layout(),
			this->neural_network_execution_policy->get_descriptor_set_layout(),
			this->loss->get_descriptor_set_layout(),
			this->optimizer->get_descriptor_set_layout(),
			this->evaluate_image_descriptor_set_layout,
			this->hdr_to_ldr_descriptor_set_layout,
			this->neural_network_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout()
		};
		::framework::gpu::descriptor_set * descriptor_sets[static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts))];
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));
		network_initialize_parameters_descriptor_set = descriptor_sets[0], this->network_descriptor_set = descriptor_sets[1], loss_descriptor_set = descriptor_sets[2];
		optimizer_descriptor_set = descriptor_sets[3], this->evaluate_image_descriptor_set = descriptor_sets[4], hdr_to_ldr_descriptor_set = descriptor_sets[5];
		this->network_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[6];
	}

	{
		::framework::gpu::descriptor_image_info descriptor_image_info;
		descriptor_image_info.sampler = sampler;
		descriptor_image_info.image_view = this->image_dataset.image_view;
		descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = this->input_buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;

		descriptor_buffer_infos[1].buffer = this->target_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		// image
		write_descriptor_sets[0].dst_set = this->evaluate_image_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
		write_descriptor_sets[0].image_info = &descriptor_image_info;
		write_descriptor_sets[0].buffer_info = nullptr;
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// uvs_buffer
		write_descriptor_sets[1].dst_set = this->evaluate_image_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// image_texels_buffer
		write_descriptor_sets[2].dst_set = this->evaluate_image_descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
	}

	{
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = this->network->get_output_buffer();
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;

		descriptor_buffer_infos[1].buffer = this->ldr_readback_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		// this->input_buffer
		write_descriptor_sets[0].dst_set = hdr_to_ldr_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// output_buffer
		write_descriptor_sets[1].dst_set = hdr_to_ldr_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
	}

	this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->network->get_full_precision_weights_matrices_buffer(), this->network->get_weight_matrices_buffer(), this->network_initialize_parameters_descriptor_set);
	this->network->update_descriptor_sets(this->gpu_context, this->encoded_input_buffer, this->dloss_dy_buffer, this->batch_size_buffer, this->network_descriptor_set);
	this->loss->update_descriptor_sets(this->gpu_context, this->batch_size_buffer, this->batch_size_buffer_alignment, this->network->get_output_buffer(), this->target_buffer, this->target_buffer, this->loss_buffer, this->network->get_dloss_doutput_buffer(), loss_descriptor_set);
	this->optimizer->update_descriptor_sets(this->gpu_context, this->network->get_full_precision_weights_matrices_buffer(), this->network->get_weight_matrices_buffer(), this->network->get_gradient_matrices_buffer(), optimizer_descriptor_set);
}

::framework::test::neural_network::~neural_network()
{
	this->gpu_context->get_device()->destroy_buffer(this->batch_size_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->dispatch_indirect_command_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->dloss_dy_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->input_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->encoded_input_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->target_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->loss_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->ldr_readback_buffer, nullptr);
	this->gpu_context->get_device()->free_memory(this->batch_size_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->dispatch_indirect_command_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->dloss_dy_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->input_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->encoded_input_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->target_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->loss_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->ldr_readback_device_memory, nullptr);
	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		this->network_descriptor_set,
		this->network_initialize_parameters_descriptor_set,
		this->network_initialize_dispatch_indirect_command_descriptor_set,
		this->loss_descriptor_set,
		this->optimizer_descriptor_set,
		this->evaluate_image_descriptor_set,
		this->hdr_to_ldr_descriptor_set
	};
	this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets);
}

bool ::framework::test::neural_network::test_fully_fused_mlp(bool indirect)
{
	return ::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<bool>
	{
		{
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

			co_await this->initialize_device_resources(graphics_command_context);

			assert_framework_gpu_result(command_buffer->end_command_buffer());

			co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);
		}

		::std::uint32_t const input_stride = input_layout == ::framework::linalg::matrix_layout::column_major ? this->padded_input_size : this->batch_size;
		
		auto record_command_buffer = [&](bool do_validation) -> ::framework::coroutine::immediate_task<::framework::command_context *>
		{
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

			this->profiler->reset(command_buffer, 0);
			this->gpu_log->reset(command_buffer, 0, {}, {});

			co_await this->forward(graphics_command_context, indirect);

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			command_buffer->fill_buffer(this->dloss_dy_buffer, 0, ::framework::gpu::whole_size, 0, {}, {}, {});

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			if (indirect)
			{
				this->network->forward_indirect(command_buffer, this->network_descriptor_set);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				this->loss->evaluate_indirect(command_buffer, loss_descriptor_set, this->dispatch_indirect_command_buffer, 0);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				this->network->backward_indirect(command_buffer, this->network_descriptor_set);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}
			else
			{
				this->network->forward(command_buffer, this->network_descriptor_set, this->batch_size);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				this->loss->evaluate(command_buffer, loss_descriptor_set, this->padded_output_size * this->batch_size);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				this->network->backward(command_buffer, this->network_descriptor_set, this->batch_size);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}
			co_await this->backward(graphics_command_context, indirect);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->optimizer->step(command_buffer, optimizer_descriptor_set);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			co_await this->optimize_step(graphics_command_context, indirect);

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			::std::uint32_t const pixel_count = this->image_dataset.width * this->image_dataset.height;

			if (do_validation)
			{
				::std::uint32_t const input_stride = input_layout == ::framework::linalg::matrix_layout::column_major ? this->padded_input_size : this->inference_batch_size;

				::framework::local::evaluate_image_push_constants push_constants
				{
					.width = this->image_dataset.width,
					.height = this->image_dataset.height,
					.batch_size = pixel_count,
					.padded_input_size = this->padded_input_size,
					.input_stride = input_stride,
					.output_stride = channel_stride,
					.seed = 0ull,
					.input_layout = static_cast<::std::uint32_t>(input_layout)
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, generate_test_uvs_pipeline);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->evaluate_image_pipeline_layout, 0, 1, &this->evaluate_image_descriptor_set, 0, nullptr);
				command_buffer->push_constants(this->evaluate_image_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch((pixel_count + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}

				co_await this->inference(graphics_command_context, indirect);

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				if (indirect)
				{
					this->network->inference_indirect(command_buffer, this->network_descriptor_set);
				}
				else
				{
					this->network->inference(command_buffer, this->network_descriptor_set, this->inference_batch_size);
				}
			}

			if (do_validation)
			{
				::framework::local::hdr_to_ldr_push_constants push_constants
				{
					.pixel_count = pixel_count,
					.input_stride = this->padded_output_size,
					.output_stride = channel_stride
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, hdr_to_ldr_pipeline);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->hdr_to_ldr_pipeline_layout, 0, 1, &hdr_to_ldr_descriptor_set, 0, nullptr);
				command_buffer->push_constants(this->hdr_to_ldr_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch((pixel_count + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}
			}

			this->profiler->copy_query_pool_results(command_buffer);
			this->gpu_log->copy_gpu_log(command_buffer);

			assert_framework_gpu_result(command_buffer->end_command_buffer());

			co_return graphics_command_context;
		};

		::framework::command_context * const training_command_context = co_await record_command_buffer(false);
		::framework::command_context * const validation_command_context = co_await record_command_buffer(true);
		::std::random_device random_device;
		::std::default_random_engine default_random_engine(random_device());
		::std::uniform_int_distribution<::std::uint32_t> uniform_int_distribution;
		::std::vector<float> average_time;

		for (::std::uint32_t i = 0; i < 10000; i++)
		{
			::std::uint32_t const epoch = i + 1;
			bool const do_validation = epoch % 1000 == 0;
			bool const print_epoch_statistics = epoch % 50 == 0;

			::framework::command_context * const generate_train_data_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();
			::framework::gpu::command_buffer * const generate_train_data_command_buffer = generate_train_data_command_context->get_command_buffer();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(generate_train_data_command_buffer->begin_command_buffer(&command_buffer_begin_info));

			{
				::framework::local::evaluate_image_push_constants push_constants
				{
					.width = this->image_dataset.width,
					.height = this->image_dataset.height,
					.batch_size = this->batch_size,
					.padded_input_size = this->padded_input_size,
					.input_stride = input_stride,
					.output_stride = channel_stride,
					.seed = uniform_int_distribution(random_device),
					.input_layout = static_cast<::std::uint32_t>(input_layout)
				};

				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					generate_train_data_command_buffer->pipeline_barrier(&dependency_info);
				}
				generate_train_data_command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->evaluate_image_pipeline);
				generate_train_data_command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->evaluate_image_pipeline_layout, 0, 1, &this->evaluate_image_descriptor_set, 0, nullptr);
				generate_train_data_command_buffer->push_constants(this->evaluate_image_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				generate_train_data_command_buffer->dispatch((this->batch_size + 128 - 1) / 128, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					generate_train_data_command_buffer->pipeline_barrier(&dependency_info);
				}
			}

			assert_framework_gpu_result(generate_train_data_command_buffer->end_command_buffer());

			co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(generate_train_data_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(do_validation ? validation_command_context : training_command_context, 0, nullptr, 0, nullptr);

			this->profiler->get_query_pool_results(0);
			this->gpu_log->print_gpu_log(0);

			auto & statistics = this->profiler->get_statistics(0);
			if (average_time.size() < statistics.size())
			{
				average_time.resize(statistics.size());
			}

			for (::std::size_t i = 0; i < statistics.size(); i++)
			{
				average_time[i] += statistics[i].delta_timestamp * 1e-3;
			}

			if (print_epoch_statistics)
			{
				::std::string statistics_string;
				for (auto & statistic : statistics)
				{
					statistics_string += ::std::format("Fully-Fused MLP epoch {} {} {:05.2f} microseconds\r\n", epoch, statistic.label, statistic.delta_timestamp * 1e-3);
				}
				::std::cout << statistics_string << ::std::endl;
			}

			if (do_validation)
			{
				::framework::gpu::mapped_memory_range mapped_memory_range;
				mapped_memory_range.memory = this->ldr_readback_device_memory;
				mapped_memory_range.offset = 0;
				mapped_memory_range.size = ::framework::gpu::whole_size;
				assert_framework_gpu_result(this->gpu_context->get_device()->invalidate_mapped_memory_ranges(1, &mapped_memory_range));

				::std::printf("Fully-Fused MLP epoch %i complete\n", epoch);

				for (::std::size_t i = 0; i < average_time.size(); i++)
				{
					::std::cout << ::std::format("average {:05.2f} microseconds\r\n", average_time[i] / epoch) << ::std::endl;
				}

				::std::filesystem::path const output_path = ::framework::local::tests_directory / ::std::format("fully_fused_mlp_{}.jpg", epoch);
				::stbi_write_jpg(output_path.string().c_str(), this->image_dataset.width, this->image_dataset.height, channel_count, this->ldr_readback_mapped_data, 100);
			}
		}

		co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(training_command_context);
		co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(validation_command_context);

		co_return true;
	}
	());
}

::framework::coroutine::immediate_task<void> (::framework::test::neural_network::inference)(::framework::command_context * const graphics_command_context, bool indirect)
{
	co_return;
}

::framework::coroutine::immediate_task<void> (::framework::test::neural_network::forward)(::framework::command_context * const graphics_command_context, bool indirect)
{
	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network::backward)(::framework::command_context * const graphics_command_context, bool indirect)
{
	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network::optimize_step)(::framework::command_context * const graphics_command_context, bool indirect)
{
	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network::initialize_device_resources)(::framework::command_context * const graphics_command_context)
{
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	co_await graphics_command_context->update_buffer(this->batch_size_buffer, &this->inference_batch_size, 0, sizeof(this->inference_batch_size));
	co_await graphics_command_context->update_buffer(this->batch_size_buffer, &this->batch_size, this->batch_size_buffer_alignment, sizeof(this->batch_size));

	if (indirect)
	{
		::framework::gpu::dispatch_indirect_command const loss_dispatch_indirect_commands[]
		{
			{.x = (this->padded_output_size * this->batch_size + 128 - 1) / 128, .y = 1, .z = 1 }
		};
		co_await graphics_command_context->update_buffer(this->dispatch_indirect_command_buffer, loss_dispatch_indirect_commands);
	}

	this->network->initialize_xavier_uniform(command_buffer, this->xavier_uniform.get(), this->network_initialize_parameters_descriptor_set);
	co_await this->neural_network_execution_policy->update_parameters(this->gpu_context, graphics_command_context, this->batch_size, this->inference_batch_size);
	this->neural_network_execution_policy->initialize_dispatch_indirect_command(this->gpu_context, command_buffer, this->network_initialize_dispatch_indirect_command_descriptor_set, true);
	this->neural_network_execution_policy->initialize_dispatch_indirect_command(this->gpu_context, command_buffer, this->network_initialize_dispatch_indirect_command_descriptor_set, false);
}

::framework::test::neural_network_with_grid_encoding::neural_network_with_grid_encoding(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log) :
	::framework::test::neural_network(gpu_context, resource_manager, command_buffer_manager, profiler, gpu_log, ::framework::test::neural_network_with_grid_encoding::feature_count)
{
	this->encoding_execution_policy = ::std::make_unique<::framework::nn::one_blob::execution_policy>(this->gpu_context, this->resource_manager, this->command_buffer_manager, this->component_type, this->one_blob_encoding_bins, this->input_size, ::framework::linalg::matrix_layout::column_major, this->network_input_layout, 0u, this->padded_input_size, 0u, this->feature_count);
	this->encoding = ::std::make_unique<::framework::nn::one_blob::encoding>(this->encoding_execution_policy.get());
	//this->encoding_execution_policy = ::std::make_unique<::framework::nn::grid::execution_policy>(this->gpu_context, this->resource_manager, this->command_buffer_manager, this->component_type, this->grid_gradient_type, this->grid_type, this->hash_type, this->interpolation_type, ::std::max(this->batch_size, this->inference_batch_size), this->feature_count, this->log2_hashmap_size, this->base_resolution, this->per_level_scale, this->stochastic_interpolation, this->features_per_level, this->pos_dims, this->padded_input_size);
	//this->encoding = ::std::make_unique<::framework::nn::grid::encoding>(this->encoding_execution_policy.get(), this->gpu_context, this->resource_manager, this->component_type, this->grid_gradient_type, this->grid_type, ::std::max(this->batch_size, this->inference_batch_size), this->feature_count, this->log2_hashmap_size, this->base_resolution, this->per_level_scale, this->stochastic_interpolation, this->features_per_level, this->pos_dims, this->padded_input_size);

	::std::uint32_t const encoding_parameter_count = this->encoding_execution_policy->get_parameter_count();
	if (encoding_parameter_count)
	{
		this->encoding_optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, this->resource_manager, this->component_type, this->grid_gradient_type, encoding_parameter_count, false, false, 0.0f, 128.0f, this->learning_rate, 0.0f);
	}

	::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
	{
		this->encoding_execution_policy->get_descriptor_set_layout(),
		this->encoding_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout(),
		this->xavier_uniform->get_descriptor_set_layout(),
		encoding_parameter_count ? this->encoding_optimizer->get_descriptor_set_layout() : nullptr
	};
	::framework::gpu::descriptor_set * descriptor_sets[static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts))];

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts)) - !bool(encoding_parameter_count);
	descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));
	this->encoding_descriptor_set = descriptor_sets[0], this->encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[1];
	this->xavier_uniform_descriptor_set = descriptor_sets[2];
	this->encoding_optimizer_descriptor_set = encoding_parameter_count ? descriptor_sets[3] : nullptr;

	this->encoding->update_descriptor_sets(this->gpu_context->get_device(), this->batch_size_buffer, this->input_buffer, this->dloss_dy_buffer, this->encoded_input_buffer, this->encoding_descriptor_set);
	if (encoding_parameter_count)
	{
		this->encoding_optimizer->update_descriptor_sets(this->gpu_context, this->encoding->get_full_precision_parameter_buffer(), this->encoding->get_parameter_buffer(), this->encoding->get_gradient_buffer(), this->encoding_optimizer_descriptor_set);
		this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->encoding->get_full_precision_parameter_buffer(), this->encoding->get_parameter_buffer(), this->xavier_uniform_descriptor_set);
	}
	if (indirect)
	{
		this->encoding_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), this->batch_size_buffer, this->dispatch_indirect_command_buffer, this->encoding_initialize_dispatch_indirect_command_descriptor_set);
		this->neural_network_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context, this->batch_size_buffer, this->network_initialize_dispatch_indirect_command_descriptor_set);
	}
}

::framework::test::neural_network_with_grid_encoding::~neural_network_with_grid_encoding()
{
	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		this->encoding_descriptor_set,
		this->encoding_initialize_dispatch_indirect_command_descriptor_set,
		this->xavier_uniform_descriptor_set,
		this->encoding_optimizer_descriptor_set
	};
	this->gpu_context->get_device()->free_descriptor_sets(this->descriptor_pool, static_cast<::std::uint32_t>(::std::size(descriptor_sets)) - !bool(this->encoding_optimizer_descriptor_set), descriptor_sets);
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network_with_grid_encoding::inference)(::framework::command_context * const graphics_command_context, bool indirect)
{
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	if (indirect)
	{
		::std::uint32_t const dynamic_offsets[] = { 0, this->dispatch_indirect_command_aligned_offset };
		this->encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->encoding->forward_indirect(command_buffer, this->encoding_descriptor_set, dynamic_offsets[0], this->dispatch_indirect_command_buffer, dynamic_offsets[1]);
	}
	else
	{
		::std::uint32_t const batch_size_buffer_dynamic_offset = 0;
		this->encoding->forward(command_buffer, this->encoding_descriptor_set, batch_size_buffer_dynamic_offset, this->inference_batch_size);
	}

	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network_with_grid_encoding::forward)(::framework::command_context * const graphics_command_context, bool indirect)
{
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "::framework::test::neural_network_with_grid_encoding::forward");

	if (indirect)
	{
		::std::uint32_t const dynamic_offsets[] = { this->batch_size_buffer_alignment, this->dispatch_indirect_command_aligned_offset };
		this->encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->encoding->forward_indirect(command_buffer, this->encoding_descriptor_set, dynamic_offsets[0], this->dispatch_indirect_command_buffer, dynamic_offsets[1]);
	}
	else
	{
		::std::uint32_t const batch_size_buffer_dynamic_offset = this->batch_size_buffer_alignment;
		this->encoding->forward(command_buffer, this->encoding_descriptor_set, batch_size_buffer_dynamic_offset, this->batch_size);
	}

	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network_with_grid_encoding::backward)(::framework::command_context * const graphics_command_context, bool indirect)
{
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	if (indirect)
	{
		this->encoding->backward_indirect(command_buffer, this->encoding_descriptor_set, this->batch_size_buffer_alignment, this->dispatch_indirect_command_buffer, this->dispatch_indirect_command_aligned_offset);
	}
	else
	{
		::std::uint32_t const batch_size_buffer_dynamic_offset = this->batch_size_buffer_alignment;
		this->encoding->backward(command_buffer, this->encoding_descriptor_set, batch_size_buffer_dynamic_offset, this->batch_size);
	}

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network_with_grid_encoding::optimize_step)(::framework::command_context * const graphics_command_context, bool indirect)
{
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	if (this->encoding_optimizer)
	{
		this->encoding_optimizer->step(command_buffer, this->encoding_optimizer_descriptor_set);
	}

	co_return;
}

::framework::coroutine::immediate_task<void>(::framework::test::neural_network_with_grid_encoding::initialize_device_resources)(::framework::command_context * const graphics_command_context)
{
	this->::framework::test::neural_network::initialize_device_resources(graphics_command_context);

	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	::std::uint32_t const encoding_parameter_count = this->encoding_execution_policy->get_parameter_count();
	if (encoding_parameter_count)
	{
		this->xavier_uniform->execute(command_buffer, this->xavier_uniform_descriptor_set, 0, encoding_parameter_count, encoding_parameter_count);
	}

	co_return;
}
