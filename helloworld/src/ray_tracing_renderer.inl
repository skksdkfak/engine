inline ::framework::graphics::scene_renderer * ::framework::graphics::ray_tracing_renderer::get_scene_renderer() const noexcept
{
	return this->scene_renderer;
}

inline ::framework::graphics::aov * (::framework::graphics::ray_tracing_renderer::get_aov)() const noexcept
{
	return this->scene_renderer->get_aov();
}
