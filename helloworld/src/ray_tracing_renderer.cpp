#define _USE_MATH_DEFINES
#include "ray_tracing_renderer.hpp"
#include "concurrency/thread_pool.h"
#include "coroutine/sync_wait.hpp"
#include "resource/resource_manager.hpp"
#include "input/input_listner.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "gpu/utility.hpp"
#include <glm/glm.hpp>
#include <array>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <execution>
#include <format>
#include <sstream>
#include <vector>

namespace framework::graphics::local
{
	namespace
	{
		constexpr ::framework::gpu::format const depth_format = ::framework::gpu::format::d16_unorm;
	}
}

::framework::graphics::ray_tracing_renderer::ray_tracing_renderer() :
	single_frame_allocator(1024u * 1024u),
	buffered_frame_count(3),
	frame_index(0),
	enable_accumulation(false),
	show_profiler_statistics(false),
	profiler_statistics_line_offset(0u),
	profiler_statistics_max_depth(0u)
{
	this->width = 1280;
	this->height = 720;
}

::framework::graphics::ray_tracing_renderer::~ray_tracing_renderer()
{
	this->destroy();
}

void ::framework::graphics::ray_tracing_renderer::initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool, typename ::framework::resource::resource_manager & resource_manager)
{
	::framework::graphics::renderer_context::initialize(api_type, static_thread_pool);

	bool const reverse_z = false;

	::std::uint32_t queue_family_property_count;
	this->gpu_context->get_physical_device()->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	this->gpu_context->get_physical_device()->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	queue_family_indices =
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	this->gpu_context->get_device()->get_queue(queue_family_indices.graphics, 0, &this->queues.graphics);
	this->gpu_context->get_device()->get_queue(queue_family_indices.compute, 0, &this->queues.compute);
	this->gpu_context->get_device()->get_queue(queue_family_indices.transfer, 0, &this->queues.transfer);

	::std::vector<::framework::gpu::command_buffer *> command_buffers(swap_chain_images.size() * 2);

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = this->queue_family_indices.graphics;
		this->gpu_context->get_device()->create_command_pool(&command_pool_create_info, nullptr, &this->graphics_queue_command_pool);
	}

	{

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = this->queue_family_indices.transfer;
		this->gpu_context->get_device()->create_command_pool(&command_pool_create_info, nullptr, &this->transfer_queue_command_pool);
	}

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->graphics_queue_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(command_buffers.size());
	this->gpu_context->get_device()->allocate_command_buffers(&command_buffer_allocate_info, command_buffers.data());

	::framework::gpu::semaphore_create_info binary_semaphore_create_info;
	binary_semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	binary_semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	binary_semaphore_create_info.initial_value = 0;

	::framework::gpu::semaphore_create_info timeline_semaphore_create_info;
	timeline_semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	timeline_semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	timeline_semaphore_create_info.initial_value = 0;

	assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&timeline_semaphore_create_info, nullptr, &this->begin_frame_semaphore));
	assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&timeline_semaphore_create_info, nullptr, &this->frame_semaphore));
	assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&binary_semaphore_create_info, nullptr, &this->ready_to_present_semaphore));

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;

	for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(swap_chain_images.size()); i++)
	{
		assert_framework_gpu_result(this->gpu_context->get_device()->create_fence(&fence_create_info, nullptr, &this->frame_resources[i].frame_fence));
		assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&binary_semaphore_create_info, nullptr, &this->frame_resources[i].present_complete_semaphore));

		this->frame_resources[i].command_buffer = command_buffers[i * 2 + 0];
		this->frame_resources[i].begin_frame_command_buffer = command_buffers[i * 2 + 1];
	}

	{
		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[3];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::color_attachment;
		descriptor_pool_sizes[0].descriptor_count = 4;

		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::depth_stencil_attachment;
		descriptor_pool_sizes[1].descriptor_count = 1;

		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_pool_sizes[2].descriptor_count = 2;

		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::host_only_bit;
		descriptor_pool_create_info.max_sets = 2;
		descriptor_pool_create_info.pool_size_count = static_cast<::std::uint32_t>(::std::size(descriptor_pool_sizes));
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->host_descriptor_pool));
	}

	{
		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[7];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_pool_sizes[0].descriptor_count = this->buffered_frame_count;

		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::acceleration_structure;
		descriptor_pool_sizes[1].descriptor_count = 1;

		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_pool_sizes[2].descriptor_count = 2;

		descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_pool_sizes[3].descriptor_count = 4096;

		descriptor_pool_sizes[4].type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_pool_sizes[4].descriptor_count = 32;

		descriptor_pool_sizes[5].type = ::framework::gpu::descriptor_type::sampler;
		descriptor_pool_sizes[5].descriptor_count = 16;

		descriptor_pool_sizes[6].type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_pool_sizes[6].descriptor_count = 256;

		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::update_after_bind_bit;
		descriptor_pool_create_info.max_sets = 128;
		descriptor_pool_create_info.pool_size_count = static_cast<::std::uint32_t>(::std::size(descriptor_pool_sizes));
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->device_descriptor_pool));
	}

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
	// color attachment
	descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].hlsl_register_space = 0;
	descriptor_set_layout_bindings[0].descriptor_count = 4;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::color_attachment;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::all;
	descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
	// depth attachment
	descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[1].binding = 1;
	descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[1].hlsl_register_space = 0;
	descriptor_set_layout_bindings[1].descriptor_count = 1;
	descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::depth_stencil_attachment;
	descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::all;
	descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::host_only_bit;
	descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->attachments_descriptor_set_layout));

	::framework::gpu::pipeline_cache_create_info pipeline_cache_create_info;
	pipeline_cache_create_info.flags = 0;
	pipeline_cache_create_info.initial_data_size = 0;
	pipeline_cache_create_info.initial_data = nullptr;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_cache(&pipeline_cache_create_info, nullptr, &this->pipeline_cache));

	::framework::profiler_create_info profiler_create_info;
	profiler_create_info.device = this->gpu_context->get_device();
	profiler_create_info.queue = this->queues.graphics;
	profiler_create_info.physical_device_features = &this->gpu_context->get_physical_device_features();
	profiler_create_info.physical_device_properties = &this->gpu_context->get_physical_device_properties();
	profiler_create_info.physical_device_memory_properties = &this->gpu_context->get_physical_device_memory_properties();
	profiler_create_info.command_buffer_manager = this->command_buffer_manager.get();
	profiler_create_info.max_statistic_count = 1024;
	profiler_create_info.frame_in_flight_count = this->buffered_frame_count;
	profiler_create_info.readback_buffer_initial_queue_family_index = this->queue_family_indices.graphics;
	this->profiler = ::std::make_unique<::framework::profiler>(profiler_create_info);

	::framework::gpu_log_create_info gpu_log_create_info;
	gpu_log_create_info.device = this->gpu_context->get_device();
	gpu_log_create_info.physical_device_features = &this->gpu_context->get_physical_device_features();
	gpu_log_create_info.physical_device_properties = &this->gpu_context->get_physical_device_properties();
	gpu_log_create_info.physical_device_memory_properties = &this->gpu_context->get_physical_device_memory_properties();
	gpu_log_create_info.gpu_log_buffer_size = (1u << 13) * 4u * this->buffered_frame_count * sizeof(::std::uint32_t) + this->buffered_frame_count;
	gpu_log_create_info.gpu_log_buffer_initial_queue_family_index = this->queue_family_indices.graphics;
	gpu_log_create_info.frame_in_flight_count = this->buffered_frame_count;
	this->gpu_log = ::std::make_unique<::framework::gpu_log>(gpu_log_create_info);

	assert_framework_gpu_result(this->gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::none, 0, ::framework::gpu::shader_stage_flags::all, this->gpu_log_device_descriptor_set_layout));
	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		assert_framework_gpu_result(this->gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::host_only_bit, 0, ::framework::gpu::shader_stage_flags::all, this->gpu_log_host_descriptor_set_layout));
	}
	else
	{
		this->gpu_log_host_descriptor_set_layout = this->gpu_log_device_descriptor_set_layout;
	}

	//if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		::framework::gpu::descriptor_set * descriptor_sets[2];
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[2]
		{
			this->attachments_descriptor_set_layout,
			this->gpu_log_host_descriptor_set_layout
		};

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->host_descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

		this->attachments_descriptor_set = descriptor_sets[0];
		this->gpu_log_host_descriptor_set = descriptor_sets[1];
	}

	{
		::framework::gpu::descriptor_set * descriptor_sets[1];
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[1]
		{
			this->gpu_log_host_descriptor_set_layout
		};

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->device_descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

		this->gpu_log_device_descriptor_set = descriptor_sets[0];
	}

	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->gpu_log->write_descriptor_set(this->gpu_log_host_descriptor_set);
		this->gpu_log_counter_host_descriptor_handle = this->gpu_log->get_gpu_log_counter_descriptor_handle(false, this->gpu_log_host_descriptor_set).host_descriptor_handle;
		this->gpu_log->copy_descriptor_set(this->gpu_log_host_descriptor_set, this->gpu_log_device_descriptor_set);
	}
	else
	{
		this->gpu_log->write_descriptor_set(this->gpu_log_device_descriptor_set);
		this->gpu_log_counter_device_descriptor_handle = this->gpu_log->get_gpu_log_counter_descriptor_handle(true, this->gpu_log_device_descriptor_set).device_descriptor_handle;
	}

	resource_manager.default_resources.gpu_log_host_descriptor_set_layout = this->gpu_log_host_descriptor_set_layout;
	resource_manager.default_resources.gpu_log_device_descriptor_set_layout = this->gpu_log_device_descriptor_set_layout;
	resource_manager.default_resources.gpu_log_host_descriptor_set = this->gpu_log_host_descriptor_set;
	resource_manager.default_resources.gpu_log_device_descriptor_set = this->gpu_log_device_descriptor_set;

	this->create_screen_resources();

	::framework::graphics::ray_tracing::scene_renderer_create_info scene_renderer_create_info;
	scene_renderer_create_info.resource_manager = &resource_manager;
	scene_renderer_create_info.thread_pool = static_thread_pool;
	scene_renderer_create_info.command_buffer_manager = this->command_buffer_manager.get();
	scene_renderer_create_info.profiler = this->profiler.get();
	scene_renderer_create_info.gpu_log = this->gpu_log.get();
	scene_renderer_create_info.gpu_context = this->gpu_context.get();
	scene_renderer_create_info.physical_device = this->gpu_context->get_physical_device();
	scene_renderer_create_info.device = this->gpu_context->get_device();
	scene_renderer_create_info.width = this->width;
	scene_renderer_create_info.height = this->height;
	scene_renderer_create_info.frame_buffer_count = static_cast<::std::uint32_t>(this->swap_chain_images.size());
	scene_renderer_create_info.output_color_format = this->swap_chain_format;
	scene_renderer_create_info.output_color_image = this->render_target_image;
	scene_renderer_create_info.output_color_image_view = this->render_target_image_view;
	scene_renderer_create_info.pipeline_cache = this->pipeline_cache;
	scene_renderer_create_info.device_descriptor_pool = this->device_descriptor_pool;
	scene_renderer_create_info.attachments_descriptor_set = this->attachments_descriptor_set;
	scene_renderer_create_info.single_frame_allocator = &this->single_frame_allocator;
	::framework::graphics::ray_tracing::scene_renderer * ray_tracing_scene_renderer = new ::framework::graphics::ray_tracing::scene_renderer(&scene_renderer_create_info);
	this->scene_renderer = ray_tracing_scene_renderer;

	::framework::graphics::scene_view_perspective_create_info scene_view_perspective_create_info;
	::framework::graphics::scene_view_perspective * scene_view_perspective = this->scene_renderer->create_scene_view_perspective(&scene_view_perspective_create_info);
	scene_view_perspective->set_eye(::glm::vec3(-840.001, 221.779, - 215.324));
	scene_view_perspective->set_center(::glm::vec3(.0f, .0f, .0f));
	scene_view_perspective->set_up(::glm::vec3(0.0f, 1.0f, 0.0f));
	scene_view_perspective->set_fov(M_PI_2);
	scene_view_perspective->set_aspect_ratio((float)this->height / (float)this->width);
	scene_view_perspective->set_near_z(0.1f);
	scene_view_perspective->set_far_z(1024.0f);
	scene_view_perspective->reverse_z(reverse_z);
	scene_view_perspective->inverse_y(true);
	scene_view_perspective->update();
	this->scene_view = scene_view_perspective;
	this->scene_renderer->set_scene_views(1, &this->scene_view);

	::framework::gpu::attachment_view_and_descriptor color_attachment_view_and_descriptor;
	color_attachment_view_and_descriptor.view = this->render_target_image_view;
	color_attachment_view_and_descriptor.descriptor_handle = this->render_target_host_descriptor_handle;
	::framework::gpu::attachment_view_and_descriptor depth_attachment_view_and_descriptor;
	depth_attachment_view_and_descriptor.view = this->ui_depth_image_view;
	depth_attachment_view_and_descriptor.descriptor_handle = this->ui_depth_host_descriptor_handle;

	{
		::framework::gpu::subpass_dependency external_dependencies[2];
		external_dependencies[0].src_subpass = ::framework::gpu::subpass_external;
		external_dependencies[0].dst_subpass = 0;
		external_dependencies[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit | ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		external_dependencies[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
		external_dependencies[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit;
		external_dependencies[0].dst_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_write_bit | ::framework::gpu::access_flags::color_attachment_read_bit;
		external_dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		external_dependencies[1].src_subpass = 0;
		external_dependencies[1].dst_subpass = ::framework::gpu::subpass_external;
		external_dependencies[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
		external_dependencies[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
		external_dependencies[1].src_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit | ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		external_dependencies[1].dst_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit | ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		external_dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		::framework::gpu::attachment_description color_attachments_description;
		color_attachments_description.flags = ::framework::gpu::attachment_description_flags::none;
		color_attachments_description.format = this->swap_chain_format;
		color_attachments_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		color_attachments_description.load_op = ::framework::gpu::attachment_load_op::load;
		color_attachments_description.store_op = ::framework::gpu::attachment_store_op::store;
		color_attachments_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		color_attachments_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		color_attachments_description.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		color_attachments_description.final_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::attachment_description depth_attachment_description;
		depth_attachment_description.flags = ::framework::gpu::attachment_description_flags::none;
		depth_attachment_description.format = ::framework::graphics::local::depth_format;
		depth_attachment_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		depth_attachment_description.load_op = ::framework::gpu::attachment_load_op::clear;
		depth_attachment_description.store_op = ::framework::gpu::attachment_store_op::store;
		depth_attachment_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		depth_attachment_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		depth_attachment_description.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		depth_attachment_description.final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		::framework::graphics::renderer_2d_create_info renderer_2d_create_info;
		renderer_2d_create_info.resource_manager = &resource_manager;
		renderer_2d_create_info.physical_device = this->gpu_context->get_physical_device();
		renderer_2d_create_info.device = this->gpu_context->get_device();
		renderer_2d_create_info.width = this->width;
		renderer_2d_create_info.height = this->height;
		renderer_2d_create_info.max_primitive_count = 8;
		renderer_2d_create_info.max_image_count = 8;
		renderer_2d_create_info.frame_buffer_count = 3;
		renderer_2d_create_info.color_attachment = color_attachment_view_and_descriptor;
		renderer_2d_create_info.color_attachments_description = color_attachments_description;
		renderer_2d_create_info.depth_attachment_description = depth_attachment_description;
		renderer_2d_create_info.depth_attachment = depth_attachment_view_and_descriptor;
		renderer_2d_create_info.external_dependency_count = static_cast<::std::uint32_t>(::std::size(external_dependencies));
		renderer_2d_create_info.external_dependencies = external_dependencies;
		renderer_2d_create_info.pipeline_cache = this->pipeline_cache;
		this->renderer_2d = new ::framework::graphics::renderer_2d(renderer_2d_create_info);
	}

	{
		::framework::gpu::subpass_dependency external_dependencies[2];
		external_dependencies[0].src_subpass = ::framework::gpu::subpass_external;
		external_dependencies[0].dst_subpass = 0;
		external_dependencies[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit;
		external_dependencies[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
		external_dependencies[0].src_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit | ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		external_dependencies[0].dst_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit | ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		external_dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		external_dependencies[1].src_subpass = 0;
		external_dependencies[1].dst_subpass = ::framework::gpu::subpass_external;
		external_dependencies[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		external_dependencies[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		external_dependencies[1].src_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		external_dependencies[1].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		external_dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		::framework::gpu::attachment_description color_attachments_description;
		color_attachments_description.flags = ::framework::gpu::attachment_description_flags::none;
		color_attachments_description.format = this->swap_chain_format;
		color_attachments_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		color_attachments_description.load_op = ::framework::gpu::attachment_load_op::load;
		color_attachments_description.store_op = ::framework::gpu::attachment_store_op::store;
		color_attachments_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		color_attachments_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		color_attachments_description.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
		color_attachments_description.final_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;

		::framework::gpu::attachment_description depth_attachment_description;
		depth_attachment_description.flags = ::framework::gpu::attachment_description_flags::none;
		depth_attachment_description.format = ::framework::graphics::local::depth_format;
		depth_attachment_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		depth_attachment_description.load_op = ::framework::gpu::attachment_load_op::load;
		depth_attachment_description.store_op = ::framework::gpu::attachment_store_op::store;
		depth_attachment_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		depth_attachment_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		depth_attachment_description.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;
		depth_attachment_description.final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		::framework::graphics::text_renderer_create_info text_renderer_create_info;
		text_renderer_create_info.resource_manager = &resource_manager;
		text_renderer_create_info.command_buffer_manager = this->command_buffer_manager.get();
		text_renderer_create_info.physical_device = this->gpu_context->get_physical_device();
		text_renderer_create_info.device = this->gpu_context->get_device();
		text_renderer_create_info.width = this->width;
		text_renderer_create_info.height = this->height;
		text_renderer_create_info.max_char_count = 1024 * 8;
		text_renderer_create_info.frame_buffer_count = 3;
		text_renderer_create_info.color_attachment = color_attachment_view_and_descriptor;
		text_renderer_create_info.depth_attachment = depth_attachment_view_and_descriptor;
		text_renderer_create_info.color_attachments_description = color_attachments_description;
		text_renderer_create_info.depth_attachment_description = depth_attachment_description;
		text_renderer_create_info.external_dependency_count = static_cast<::std::uint32_t>(::std::size(external_dependencies));
		text_renderer_create_info.external_dependencies = external_dependencies;
		text_renderer_create_info.pipeline_cache = this->pipeline_cache;
		text_renderer_create_info.profiler = this->profiler.get();
		this->text_renderer = new ::framework::graphics::text_renderer(text_renderer_create_info);
	}

	::framework::graphics::ui_create_info ui_create_info;
	ui_create_info.resource_manager = &resource_manager;
	ui_create_info.text_renderer = this->text_renderer;
	ui_create_info.renderer_2d = this->renderer_2d;
	ui_create_info.input_listner = ::framework::engine_core::input_listner;
	ui_create_info.width = this->width;
	ui_create_info.height = this->height;
	this->ui = new ::framework::graphics::ui(ui_create_info);

	this->nis_visualization_2d_primitive_index = this->renderer_2d->add_primitive();
	this->ground_truth_distribution_2d_primitive_index = this->renderer_2d->add_primitive();
	this->renderer_2d->bind_image(this->scene_renderer->get_nis_pdf_image_view(), ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit, this->nis_visualization_2d_primitive_index);
	this->renderer_2d->bind_image(this->scene_renderer->get_ground_truth_distribution_image_view(), ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit, this->ground_truth_distribution_2d_primitive_index);
}

void ::framework::graphics::ray_tracing_renderer::destroy()
{
	delete this->text_renderer;
	delete this->renderer_2d;
	delete this->ui;
	delete this->scene_renderer;
}

void ::framework::graphics::ray_tracing_renderer::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->::framework::graphics::renderer_context::resize(width, height);

	// TODO: Remove the hard assumption.
	static_cast<::framework::graphics::scene_view_perspective *>(this->scene_view)->set_aspect_ratio((float)this->height / (float)this->width);
	this->scene_view->update();

	this->gpu_context->get_device()->destroy_image_view(this->ui_depth_image_view, nullptr);
	this->gpu_context->get_device()->destroy_image(this->ui_depth_image, nullptr);
	this->gpu_context->get_device()->free_memory(this->ui_depth_device_memory, nullptr);
	this->gpu_context->get_device()->destroy_image_view(this->render_target_image_view, nullptr);
	this->gpu_context->get_device()->destroy_image(this->render_target_image, nullptr);
	this->gpu_context->get_device()->free_memory(this->render_target_device_memory, nullptr);

	this->create_screen_resources();

	::framework::graphics::ray_tracing::scene_renderer_resize_info scene_renderer_resize_info;
	scene_renderer_resize_info.width = width;
	scene_renderer_resize_info.height = height;
	scene_renderer_resize_info.output_color_image = this->render_target_image;
	scene_renderer_resize_info.output_color_image_view = this->render_target_image_view;
	this->scene_renderer->resize(&scene_renderer_resize_info);

	::framework::gpu::attachment_view_and_descriptor color_attachment_view_and_descriptor;
	color_attachment_view_and_descriptor.view = this->render_target_image_view;
	color_attachment_view_and_descriptor.descriptor_handle = this->render_target_host_descriptor_handle;

	::framework::gpu::attachment_view_and_descriptor depth_attachment_view_and_descriptor;
	depth_attachment_view_and_descriptor.view = this->ui_depth_image_view;
	depth_attachment_view_and_descriptor.descriptor_handle = this->ui_depth_host_descriptor_handle;

	::framework::graphics::text_renderer_resize_info text_renderer_resize_info;
	text_renderer_resize_info.width = width;
	text_renderer_resize_info.height = height;
	text_renderer_resize_info.color_attachment = color_attachment_view_and_descriptor;
	text_renderer_resize_info.depth_attachment = depth_attachment_view_and_descriptor;
	this->text_renderer->resize(&text_renderer_resize_info);

	::framework::graphics::renderer_2d_resize_info renderer_2d_resize_info;
	renderer_2d_resize_info.width = width;
	renderer_2d_resize_info.height = height;
	renderer_2d_resize_info.color_attachment = color_attachment_view_and_descriptor;
	renderer_2d_resize_info.depth_attachment = depth_attachment_view_and_descriptor;
	this->renderer_2d->resize(&renderer_2d_resize_info);

	this->ui->resize(width, height);
}

void ::framework::graphics::ray_tracing_renderer::update(double delta_time)
{
	::std::uint_fast8_t const frame_ring_buffer_index = this->frame_index % this->buffered_frame_count;

	assert_framework_gpu_result(this->gpu_context->get_device()->wait_for_fences(1, &this->frame_resources[frame_ring_buffer_index].frame_fence, true, UINT64_MAX));
	assert_framework_gpu_result(this->gpu_context->get_device()->reset_fences(1, &this->frame_resources[frame_ring_buffer_index].frame_fence));

	this->profiler->get_query_pool_results(frame_ring_buffer_index);

	this->text_renderer->begin_text_update();

	if (::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_left_shift))
	{
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_r))
		{
			this->scene_renderer->reset_nis();
		}
	}
	else
	{
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_r))
		{
			this->enable_accumulation = !this->enable_accumulation;
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_p))
		{
			this->show_profiler_statistics = !this->show_profiler_statistics;
		}
		
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_left))
		{
			if (this->profiler_statistics_max_depth > 0)
			{
				this->profiler_statistics_max_depth--;
			}
		}
		
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_right))
		{
			if (this->profiler_statistics_max_depth < ::std::numeric_limits<::std::uint32_t>::max())
			{
				this->profiler_statistics_max_depth++;
			}
		}
		
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_up))
		{
			if (this->profiler_statistics_line_offset > 0)
			{
				this->profiler_statistics_line_offset--;
			}
		}
		
		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_down))
		{
			if (this->profiler_statistics_line_offset < ::std::numeric_limits<::std::uint32_t>::max())
			{
				this->profiler_statistics_line_offset++;
			}
		}

		if (::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_f5))
		{
			this->scene_renderer->recompile_shaders();
			this->text_renderer->recompile_shaders();
			this->renderer_2d->recompile_shaders();
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_0))
		{
			this->scene_renderer->set_nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::none);
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_1))
		{
			this->scene_renderer->set_nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::ground_truth);
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_2))
		{
			this->scene_renderer->set_nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::nis_pdf);
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_3))
		{
			this->scene_renderer->set_nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::neural_light_field);
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_m))
		{
			this->scene_renderer->set_enable_nee(!this->scene_renderer->get_enable_nee());
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_c))
		{
			this->scene_renderer->set_enable_ncv(!this->scene_renderer->get_enable_ncv());
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_n))
		{
			this->scene_renderer->set_enable_nis(!this->scene_renderer->get_enable_nis());
		}

		if (::framework::engine_core::input_listner->is_key_first_pressed(::framework::input::key::key_t))
		{
			this->scene_renderer->set_enable_nis_training(!this->scene_renderer->get_enable_nis_training());
		}
	}

	::std::stringstream top_left_stats_stringstream;
	top_left_stats_stringstream << this->gpu_context->get_physical_device_properties().device_name << "\r\n";
	top_left_stats_stringstream << ::std::format("{}x{}\r\n", this->width, this->height);
	top_left_stats_stringstream << ::std::format("NEE {}\r\n", this->scene_renderer->get_enable_nee() ? "ON" : "OFF");
	top_left_stats_stringstream << ::std::format("NCV {}\r\n", this->scene_renderer->get_enable_ncv() ? "ON" : "OFF");
	top_left_stats_stringstream << ::std::format("NIS {}\r\n", this->scene_renderer->get_enable_nis() ? "ON" : "OFF");
	top_left_stats_stringstream << ::std::format("NIS training {}\r\n", this->scene_renderer->get_enable_nis_training() ? "ON" : "OFF");
	this->text_renderer->add_text(top_left_stats_stringstream.str(), -0.99, -0.99f, ::framework::graphics::text_renderer::text_align::align_left, ::framework::graphics::text_renderer::vertical_align::top);

	if (false)
	{
		::std::stringstream ss;
		ss << ::std::fixed << ::std::setprecision(2) << (delta_time * 1000.0f) << "ms (" << 1000.0f / delta_time << " fps)";
		this->text_renderer->add_text(ss.str(), -0.99f, -0.95f, ::framework::graphics::text_renderer::text_align::align_left, ::framework::graphics::text_renderer::vertical_align::top);
	}

	if (this->show_profiler_statistics)
	{
		::std::uint32_t line = 0u;
		::std::string statistics_string;
		for (auto & statistic : this->profiler->get_statistics(frame_ring_buffer_index))
		{
			if (statistic.depth <= this->profiler_statistics_max_depth)
			{
				if (line >= this->profiler_statistics_line_offset)
				{
					statistics_string += ::std::format("{}: {:05.2f} ms\r\n", statistic.label, statistic.delta_timestamp * 1e-6);
				}
				line++;
			}
		}
		this->text_renderer->add_text(statistics_string, 1.0, -1.0, ::framework::graphics::text_renderer::text_align::align_right, ::framework::graphics::text_renderer::vertical_align::top);
	}

	this->gpu_log->print_gpu_log(frame_ring_buffer_index);

	this->renderer_2d->begin_update();
	this->ui->update();

	float const primitive_size = 0.4f;
	::glm::vec2 const nis_visualization_2d_primitive_pos_max = ::glm::vec2(1.0f);
	::glm::vec2 const nis_visualization_2d_primitive_pos_min = ::glm::vec2(nis_visualization_2d_primitive_pos_max - primitive_size);
	::glm::vec2 const ground_truth_distribution_2d_primitive_pos_max = ::glm::vec2(1.0f, nis_visualization_2d_primitive_pos_min.y);
	::glm::vec2 const ground_truth_distribution_2d_primitive_pos_min = ::glm::vec2(ground_truth_distribution_2d_primitive_pos_max - primitive_size);

	this->renderer_2d->update_primitive(this->nis_visualization_2d_primitive_index, nis_visualization_2d_primitive_pos_min.x, nis_visualization_2d_primitive_pos_min.y, nis_visualization_2d_primitive_pos_max.x, nis_visualization_2d_primitive_pos_max.y, 0.0f, 0.0f, 1.0f, 1.0f, this->nis_visualization_2d_primitive_index);
	this->renderer_2d->update_primitive(this->ground_truth_distribution_2d_primitive_index, ground_truth_distribution_2d_primitive_pos_min.x, ground_truth_distribution_2d_primitive_pos_min.y, ground_truth_distribution_2d_primitive_pos_max.x, ground_truth_distribution_2d_primitive_pos_max.y, 0.0f, 0.0f, 1.0f, 1.0f, this->ground_truth_distribution_2d_primitive_index);
	//::std::stringstream ss;
	//ss << ::std::fixed << ::std::setprecision(2) << this->last_query_dt << "ms";
	//this->text_renderer->add_text(ss.str(), 5.0f, 45.0f, ::framework::graphics::text_renderer::text_align::align_left);

	this->text_renderer->end_text_update();
	this->renderer_2d->end_update();

	this->single_frame_allocator.clear();

	::framework::gpu::result acquire_next_image_result;
	::std::uint32_t swap_chain_image;

	::framework::gpu::acquire_next_image_info acquire_next_image_info;
	acquire_next_image_info.swap_chain = this->swap_chain;
	acquire_next_image_info.timeout = UINT64_MAX;
	acquire_next_image_info.semaphore = this->frame_resources[frame_ring_buffer_index].present_complete_semaphore;
	acquire_next_image_info.fence = nullptr;
	acquire_next_image_info.device_mask = 0x1;
	acquire_next_image_result = this->gpu_context->get_device()->acquire_next_image(&acquire_next_image_info, &swap_chain_image);

	if (acquire_next_image_result == ::framework::gpu::result::error_out_of_date)
	{
		this->resize(this->window->get_width(), this->window->get_height());
	}
	else
	{
		assert_framework_gpu_result(acquire_next_image_result);
	}

	{
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer->begin_command_buffer(&command_buffer_begin_info));
		this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
		this->profiler->reset(this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer, frame_ring_buffer_index);
		this->gpu_log->reset(this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer, frame_ring_buffer_index, this->gpu_log_counter_device_descriptor_handle, this->gpu_log_counter_host_descriptor_handle);
		assert_framework_gpu_result(this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer->end_command_buffer());

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = this->frame_semaphore;
		wait_semaphore_info.value = this->frame_index;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		wait_semaphore_info.device_index = 0;

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = this->frame_resources[frame_ring_buffer_index].begin_frame_command_buffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = this->begin_frame_semaphore;
		signal_semaphore_info.value = this->frame_index + 1;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		signal_semaphore_info.device_index = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 1;
		submit_info.wait_semaphore_infos = &wait_semaphore_info;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 1;
		submit_info.signal_semaphore_infos = &signal_semaphore_info;
		assert_framework_gpu_result(::framework::coroutine::sync_wait(this->command_buffer_manager->get_graphics_queue()->submit(1, &submit_info, nullptr)));
	}

	if (!this->enable_accumulation)
	{
		this->scene_renderer->reset();
	}

	::framework::gpu::semaphore_submit_info scene_renderer_wait_semaphore_info;
	scene_renderer_wait_semaphore_info.semaphore = this->begin_frame_semaphore;
	scene_renderer_wait_semaphore_info.value = this->frame_index + 1;
	scene_renderer_wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	scene_renderer_wait_semaphore_info.device_index = 0;
	::framework::coroutine::sync_wait(this->scene_renderer->update_command_buffer(1, &scene_renderer_wait_semaphore_info));

	this->renderer_2d->update_command_buffer(frame_ring_buffer_index);
	this->text_renderer->update_command_buffer(frame_ring_buffer_index);

	::framework::gpu::command_buffer * command_buffer = this->frame_resources[frame_ring_buffer_index].command_buffer;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = 1;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::framework::gpu::image_memory_barrier image_memory_barriers[1];
		image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
		image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::present_src_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[0].image = this->swap_chain_images[swap_chain_image];
		image_memory_barriers[0].subresource_range = subresource_range;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
		dependency_info.image_memory_barriers = image_memory_barriers;

		command_buffer->pipeline_barrier(&dependency_info);
	}

	::framework::gpu::image_copy image_copy;
	image_copy.src_subresource = { ::framework::gpu::image_aspect_flags::color_bit, 0, 0, 1 };
	image_copy.src_offset = { 0,0,0 };
	image_copy.dst_subresource = { ::framework::gpu::image_aspect_flags::color_bit, 0, 0, 1 };
	image_copy.dst_offset = { 0,0,0 };
	image_copy.extent = { this->width, this->height, 1 };
	command_buffer->copy_image(this->render_target_image, ::framework::gpu::image_layout_flags::transfer_src_optimal_bit, this->swap_chain_images[swap_chain_image], ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit, 1, &image_copy);

	{
		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = 1;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::framework::gpu::image_memory_barrier image_memory_barriers[2];
		image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
		image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
		image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;
		image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[0].image = this->render_target_image;
		image_memory_barriers[0].subresource_range = subresource_range;

		image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
		image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::present_src_bit;
		image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		image_memory_barriers[1].image = this->swap_chain_images[swap_chain_image];
		image_memory_barriers[1].subresource_range = subresource_range;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
		dependency_info.image_memory_barriers = image_memory_barriers;

		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->profiler->copy_query_pool_results(command_buffer);
	this->gpu_log->copy_gpu_log(command_buffer);
	assert_framework_gpu_result(command_buffer->end_command_buffer());

	{
		::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
		wait_semaphore_infos[0].semaphore = this->scene_renderer->get_frame_semaphore();
		wait_semaphore_infos[0].value = this->scene_renderer->get_last_frame_index();
		wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		wait_semaphore_infos[0].device_index = 0;

		wait_semaphore_infos[1].semaphore = this->frame_resources[frame_ring_buffer_index].present_complete_semaphore;
		wait_semaphore_infos[1].value = 0;
		wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		wait_semaphore_infos[1].device_index = 0;

		::framework::gpu::command_buffer_submit_info command_buffer_infos[3];
		command_buffer_infos[0].command_buffer = this->renderer_2d->get_command_buffer(frame_ring_buffer_index);
		command_buffer_infos[0].device_mask = 0;

		command_buffer_infos[1].command_buffer = this->text_renderer->get_command_buffer(frame_ring_buffer_index);
		command_buffer_infos[1].device_mask = 0;

		command_buffer_infos[2].command_buffer = this->frame_resources[frame_ring_buffer_index].command_buffer;
		command_buffer_infos[2].device_mask = 0;

		::framework::gpu::semaphore_submit_info signal_semaphore_infos[2];
		signal_semaphore_infos[0].semaphore = this->frame_semaphore;
		signal_semaphore_infos[0].value = this->frame_index + 1;
		signal_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		signal_semaphore_infos[0].device_index = 0;

		signal_semaphore_infos[1].semaphore = this->ready_to_present_semaphore;
		signal_semaphore_infos[1].value = 0;
		signal_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		signal_semaphore_infos[1].device_index = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = static_cast<::std::uint32_t>(::std::size(wait_semaphore_infos));
		submit_info.wait_semaphore_infos = wait_semaphore_infos;
		submit_info.command_buffer_info_count = static_cast<::std::uint32_t>(::std::size(command_buffer_infos));
		submit_info.command_buffer_infos = command_buffer_infos;
		submit_info.signal_semaphore_info_count = static_cast<::std::uint32_t>(::std::size(signal_semaphore_infos));
		submit_info.signal_semaphore_infos = signal_semaphore_infos;
		assert_framework_gpu_result(::framework::coroutine::sync_wait(this->command_buffer_manager->get_graphics_queue()->submit(1, &submit_info, this->frame_resources[frame_ring_buffer_index].frame_fence)));
	}

	::framework::gpu::present_info present_info;
	present_info.wait_semaphore_count = 1;
	present_info.wait_semaphores = &this->ready_to_present_semaphore;
	present_info.swap_chain_count = 1;
	present_info.swap_chains = &this->swap_chain;
	present_info.image_indices = &swap_chain_image;
	present_info.results = nullptr;
	::framework::gpu::result const result = ::framework::coroutine::sync_wait(this->command_buffer_manager->get_graphics_queue()->present(&present_info));
	if (result == ::framework::gpu::result::error_out_of_date)
	{
		this->resize(this->window->get_width(), this->window->get_height());
	}
	else
	{
		assert_framework_gpu_result(result);
	}

	++this->frame_index;
}

void ::framework::graphics::ray_tracing_renderer::create_screen_resources()
{
	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = this->swap_chain_format;
		image_create_info.extent = { this->width, this->height, 1 };
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit |
			::framework::gpu::image_usage_flags::color_attachment_bit |
			::framework::gpu::image_usage_flags::transfer_src_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit |
			::framework::gpu::image_layout_flags::general_bit |
			::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->render_target_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->render_target_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.image = this->render_target_image;
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->render_target_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->render_target_image;
		bind_image_memory_info.memory = this->render_target_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->render_target_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = this->swap_chain_format;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->render_target_image_view));
	}

	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::graphics::local::depth_format;
		image_create_info.extent = { this->width, this->height, 1 };
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->ui_depth_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->ui_depth_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.image = this->ui_depth_image;
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->ui_depth_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->ui_depth_image;
		bind_image_memory_info.memory = this->ui_depth_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->ui_depth_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::graphics::local::depth_format;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::depth_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->ui_depth_image_view));
	}

	::framework::gpu::descriptor_image_info descriptor_image_infos[2];
	descriptor_image_infos[0].sampler = nullptr;
	descriptor_image_infos[0].image_view = this->render_target_image_view;
	descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	descriptor_image_infos[1].sampler = nullptr;
	descriptor_image_infos[1].image_view = this->ui_depth_image_view;
	descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	write_descriptor_sets[0].dst_set = this->attachments_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::color_attachment;
	write_descriptor_sets[0].image_info = &descriptor_image_infos[0];
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;

	write_descriptor_sets[1].dst_set = this->attachments_descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::depth_stencil_attachment;
	write_descriptor_sets[1].image_info = &descriptor_image_infos[1];
	write_descriptor_sets[1].buffer_info = nullptr;
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	{
		::framework::gpu::descriptor_handle_info descriptor_handle_info;
		descriptor_handle_info.flags = ::framework::gpu::descriptor_handle_info_flags::none;
		descriptor_handle_info.device_descriptor_handle = false;
		descriptor_handle_info.set = this->attachments_descriptor_set;
		descriptor_handle_info.binding = 0;
		descriptor_handle_info.array_element = 0;
		this->render_target_host_descriptor_handle = this->gpu_context->get_device()->get_descriptor_handle(&descriptor_handle_info).host_descriptor_handle;
	}
	{
		::framework::gpu::descriptor_handle_info descriptor_handle_info;
		descriptor_handle_info.flags = ::framework::gpu::descriptor_handle_info_flags::none;
		descriptor_handle_info.device_descriptor_handle = false;
		descriptor_handle_info.set = this->attachments_descriptor_set;
		descriptor_handle_info.binding = 1;
		descriptor_handle_info.array_element = 0;
		this->ui_depth_host_descriptor_handle = this->gpu_context->get_device()->get_descriptor_handle(&descriptor_handle_info).host_descriptor_handle;
	}

	if (!this->gpu_context->get_physical_device_features().any_image_initial_layout)
	{
		::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
		{
			::framework::command_context * const graphics_command_context = co_await this->command_buffer_manager->get_graphics_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			::framework::gpu::image_subresource_range subresource_range;
			subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			subresource_range.base_mip_level = 0;
			subresource_range.level_count = 1;
			subresource_range.base_array_layer = 0;
			subresource_range.layer_count = 1;

			::framework::gpu::image_memory_barrier image_memory_barrier;
			image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
			image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
			image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barrier.src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
			image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
			image_memory_barrier.image = this->render_target_image;
			image_memory_barrier.subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 1;
			dependency_info.image_memory_barriers = &image_memory_barrier;
			graphics_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);

			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
			co_await this->command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);
		}());
	}
}
