#include "deferred_renderer.hpp"
#include "gpu_context.hpp"
#include <glm/glm.hpp>
#include <concurrency/thread_pool.h>
#include <resource/resource_manager.hpp>
//#include <components/MeshRendererPathTracingBVH.h>
//#include <components/Transform.h>
//#include <components/CameraController.h>
#include "graphics/deferred_shading/scene_renderer.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "coroutine/async_auto_reset_event.hpp"
#include <gpu/utility.hpp>
#include <algorithm>
#include <execution>
#include <iostream>
#include <numbers>
#include <sstream>

::framework::graphics::deferred_renderer::deferred_renderer() :
	single_frame_allocator(1024u * 1024u),
	buffered_frame_count(3),
	frame_index(0)
{
	this->width = 1280;
	this->height = 720;
}

::framework::graphics::deferred_renderer::~deferred_renderer()
{
	destroy();
}

void ::framework::graphics::deferred_renderer::initialize(::framework::gpu::api_type api_type, ::framework::coroutine::static_thread_pool * static_thread_pool, typename ::framework::resource::resource_manager & resource_manager)
{
	::framework::graphics::renderer_context::initialize(api_type, static_thread_pool);

	this->thread_pool = thread_pool;

	bool const reverse_z = false;
	
	::std::uint32_t queue_family_property_count;
	this->gpu_context->get_physical_device()->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	this->gpu_context->get_physical_device()->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	queue_family_indices =
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	this->gpu_context->get_device()->get_queue(queue_family_indices.graphics, 0, &queues.graphics);
	this->gpu_context->get_device()->get_queue(queue_family_indices.compute, 0, &queues.compute);
	this->gpu_context->get_device()->get_queue(queue_family_indices.transfer, 0, &queues.transfer);

	//m_camera = entity_manager::create<CEntity>();
	//auto cameraTransform = m_camera.assign<CTransform>();
	//auto camera = m_camera.assign<camera>();
	//camera->setZRange(0.01f, 1024.f);
	//camera->reverseZ(false);
	//camera->inverseY(apiType == GAPIType::TYPE_VK);
	//m_camera.assign<CameraController>(::glm::vec3(0.0f, 1.0f, 0.0f));

	m_lightPos = ::glm::vec4();

	::std::vector<::framework::gpu::image *> color_attachment_images(swap_chain_images.size());
	::std::vector<::framework::gpu::image_view *> color_attachment_image_views(swap_chain_images.size());
	::std::vector<::framework::gpu::command_buffer *> command_buffers(swap_chain_images.size());

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&semaphore_create_info, nullptr, &present_complete_semaphore));

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = this->queue_family_indices.graphics;
		this->gpu_context->get_device()->create_command_pool(&command_pool_create_info, nullptr, &this->graphics_queue_command_pool);
	}

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = this->queue_family_indices.transfer;
		this->gpu_context->get_device()->create_command_pool(&command_pool_create_info, nullptr, &this->transfer_queue_command_pool);
	}

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->graphics_queue_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(swap_chain_images.size());
	this->gpu_context->get_device()->allocate_command_buffers(&command_buffer_allocate_info, command_buffers.data());

	for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(swap_chain_images.size()); i++)
	{
		assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&semaphore_create_info, nullptr, &this->frame_resources[i].deferred_render_complete_semaphore));
		assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&semaphore_create_info, nullptr, &this->frame_resources[i].text_render_complete_semaphore));

		this->frame_resources[i].command_buffer = command_buffers[i];

		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = this->swap_chain_format;
		image_create_info.extent = { this->width, this->height, 1 };
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::transfer_src_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->frame_resources[i].render_target_image));

		color_attachment_images[i] = this->frame_resources[i].render_target_image;

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->frame_resources[i].render_target_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.image = this->frame_resources[i].render_target_image;
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->frame_resources[i].render_target_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->frame_resources[i].render_target_image;
		bind_image_memory_info.memory = this->frame_resources[i].render_target_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->frame_resources[i].render_target_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = this->swap_chain_format;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->frame_resources[i].render_target_image_view));

		color_attachment_image_views[i] = this->frame_resources[i].render_target_image_view;
	}

	if (!this->gpu_context->get_physical_device_features().any_image_initial_layout)
	{
		::framework::gpu::command_buffer * command_buffer;
		::framework::gpu::fence * fence;

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = this->transfer_queue_command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = 1;
		this->gpu_context->get_device()->allocate_command_buffers(&command_buffer_allocate_info, &command_buffer);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = 1;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::std::vector<::framework::gpu::image_memory_barrier> image_memory_barriers(swap_chain_images.size());

		for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(swap_chain_images.size()); i++)
		{
			image_memory_barriers[i].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[i].dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			image_memory_barriers[i].src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
			image_memory_barriers[i].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			image_memory_barriers[i].old_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[i].new_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
			image_memory_barriers[i].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[i].src_queue_family_ownership.queue_family_index = this->queue_family_indices.transfer;
			image_memory_barriers[i].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[i].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.transfer;
			image_memory_barriers[i].image = this->frame_resources[i].render_target_image;
			image_memory_barriers[i].subresource_range = subresource_range;
		}

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(image_memory_barriers.size());
		dependency_info.image_memory_barriers = image_memory_barriers.data();

		command_buffer->begin_command_buffer(&command_buffer_begin_info);
		command_buffer->pipeline_barrier(&dependency_info);
		command_buffer->end_command_buffer();

		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		this->gpu_context->get_device()->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = command_buffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		this->queues.transfer->submit(1, &submit_info, fence);

		this->gpu_context->get_device()->wait_for_fences(1, &fence, true, UINT64_MAX);
		this->gpu_context->get_device()->destroy_fence(fence, nullptr);
		this->gpu_context->get_device()->free_command_buffers(this->transfer_queue_command_pool, 1, &command_buffer);
	}

	::framework::gpu::pipeline_cache_create_info pipeline_cache_create_info;
	pipeline_cache_create_info.flags = 0;
	pipeline_cache_create_info.initial_data_size = 0;
	pipeline_cache_create_info.initial_data = nullptr;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_cache(&pipeline_cache_create_info, nullptr, &this->pipeline_cache));

	{
		::framework::gpu::attachment_description attachment_description;
		attachment_description.format = this->swap_chain_format;
		attachment_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_description.load_op = ::framework::gpu::attachment_load_op::load;
		attachment_description.store_op = ::framework::gpu::attachment_store_op::store;
		attachment_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_description.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
		attachment_description.final_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::graphics::renderer_2d_create_info renderer_2d_create_info;
		renderer_2d_create_info.resource_manager = &resource_manager;
		renderer_2d_create_info.physical_device = this->gpu_context->get_physical_device();
		renderer_2d_create_info.device = this->gpu_context->get_device();
		renderer_2d_create_info.width = width;
		renderer_2d_create_info.height = height;
		renderer_2d_create_info.max_primitive_count = 8;
		renderer_2d_create_info.max_image_count = 8;
		renderer_2d_create_info.frame_buffer_count = static_cast<::std::uint32_t>(swap_chain_images.size());
		//renderer_2d_create_info.color_attachments = color_attachment_image_views.data();
		renderer_2d_create_info.color_attachments_description = attachment_description;
		renderer_2d_create_info.pipeline_cache = this->pipeline_cache;
		this->renderer_2d = new ::framework::graphics::renderer_2d(renderer_2d_create_info);
	}

	::framework::graphics::deferred_shading::scene_renderer_create_info scene_renderer_create_info;
	scene_renderer_create_info.physical_device = this->gpu_context->get_physical_device();
	scene_renderer_create_info.device= this->gpu_context->get_device();
	scene_renderer_create_info.width = width;
	scene_renderer_create_info.height = height;
	scene_renderer_create_info.color_attachments_format = swap_chain_format;
	scene_renderer_create_info.frame_buffer_count = static_cast<::std::uint32_t>(swap_chain_images.size());
	//scene_renderer_create_info.color_attachment_images = color_attachment_images.data();
	scene_renderer_create_info.color_attachments = color_attachment_image_views.data();
	scene_renderer_create_info.pipeline_cache = this->pipeline_cache;
	scene_renderer_create_info.reverse_z_depth = reverse_z;
	scene_renderer_create_info.single_frame_allocator = &single_frame_allocator;
	scene_renderer_create_info.resource_manager = &resource_manager;
	scene_renderer_create_info.thread_pool = this->thread_pool;

	::framework::graphics::deferred_shading::scene_renderer * deferred_shading_scene_renderer = new ::framework::graphics::deferred_shading::scene_renderer(&scene_renderer_create_info);
	this->scene_renderer = deferred_shading_scene_renderer;

	this->renderer_2d->bind_image(deferred_shading_scene_renderer->get_attachments().position.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 0);
	this->renderer_2d->bind_image(deferred_shading_scene_renderer->get_attachments().normal.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 1);
	this->renderer_2d->bind_image(deferred_shading_scene_renderer->get_attachments().albedo.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 2);
	this->renderer_2d->bind_image(deferred_shading_scene_renderer->get_attachments().depth_stencil.image_view, ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit, 3);
	//this->renderer_2d->bind_image(deferred_shading_scene_renderer->image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 4);

	this->renderer_2d->begin_update();
	//this->renderer_2d->add_primitive(0.75f, -0.95f, 0.95f, -0.75f, 0.0f, 0.0f, 1.0f, 1.0f, 0);
	//this->renderer_2d->add_primitive(0.75f, -0.7f, 0.95f, -0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1);
	//this->renderer_2d->add_primitive(0.75f, -0.45f, 0.95f, -0.25f, 0.0f, 0.0f, 1.0f, 1.0f, 2);
	//this->renderer_2d->add_primitive(0.75f, -0.2f, 0.95f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 3);
	//this->renderer_2d->add_primitive(-0.99f, -0.8f, -0.66f, -0.47f, 0.0f, 0.0f, 1.0f, 1.0f, 4);
	this->renderer_2d->end_update();

	::framework::graphics::scene_view_perspective * scene_view_perspective;

	::framework::graphics::scene_view_perspective_create_info scene_view_perspective_create_info;
	scene_view_perspective = this->scene_renderer->create_scene_view_perspective(&scene_view_perspective_create_info);

	scene_view_perspective->set_eye(::glm::vec3(0.0f, 0.0f, 0.0f));
	scene_view_perspective->set_center(::glm::vec3(0.0f, 0.0f, 1.0f));
	scene_view_perspective->set_up(::glm::vec3(0.0f, 1.0f, 0.0f));
	scene_view_perspective->set_fov(::std::numbers::pi / 2);
	scene_view_perspective->set_aspect_ratio((float)height / (float)width);
	scene_view_perspective->set_near_z(0.1f);
	scene_view_perspective->set_far_z(4096.0f);
	scene_view_perspective->reverse_z(reverse_z);
	scene_view_perspective->inverse_y(api_type == ::framework::gpu::api_type::vulkan);
	scene_view_perspective->update();

	this->scene_view = scene_view_perspective;

	this->scene_renderer->set_scene_views(1, &this->scene_view);

	{
		::framework::gpu::attachment_description attachment_description;
		attachment_description.format = this->swap_chain_format;
		attachment_description.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_description.load_op = ::framework::gpu::attachment_load_op::load;
		attachment_description.store_op = ::framework::gpu::attachment_store_op::store;
		attachment_description.stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_description.stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_description.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
		attachment_description.final_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::graphics::text_renderer_create_info text_renderer_create_info;
		text_renderer_create_info.resource_manager = &resource_manager;
		text_renderer_create_info.physical_device = this->gpu_context->get_physical_device();
		text_renderer_create_info.device = this->gpu_context->get_device();
		text_renderer_create_info.width = width;
		text_renderer_create_info.height = height;
		text_renderer_create_info.max_char_count = 128;
		text_renderer_create_info.frame_buffer_count = static_cast<::std::uint32_t>(swap_chain_images.size());
		//text_renderer_create_info.color_attachments = color_attachment_image_views.data();
		text_renderer_create_info.color_attachments_description = attachment_description;
		text_renderer_create_info.pipeline_cache = this->pipeline_cache;
		this->text_renderer = new ::framework::graphics::text_renderer(text_renderer_create_info);
	}
}

void ::framework::graphics::deferred_renderer::destroy()
{
	delete this->text_renderer;
	delete this->renderer_2d;
}

void ::framework::graphics::deferred_renderer::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->renderer_context::resize(width, height);

	::framework::graphics::deferred_shading::scene_renderer_resize_info scene_renderer_resize_info;
	scene_renderer_resize_info.width = width;
	scene_renderer_resize_info.height = height;
	scene_renderer_resize_info.color_attachments = swap_chain_image_views.data();
	this->scene_renderer->resize(&scene_renderer_resize_info);

	::framework::graphics::text_renderer_resize_info text_renderer_resize_info;
	text_renderer_resize_info.width = width;
	text_renderer_resize_info.height = height;
	//text_renderer_resize_info.color_attachments = swap_chain_image_views.data();
	this->text_renderer->resize(&text_renderer_resize_info);

	::framework::graphics::renderer_2d_resize_info renderer_2d_resize_info;
	renderer_2d_resize_info.width = width;
	renderer_2d_resize_info.height = height;
	//renderer_2d_resize_info.color_attachments = swap_chain_image_views.data();
	this->renderer_2d->resize(&renderer_2d_resize_info);

	this->renderer_2d->bind_image(this->scene_renderer->get_attachments().position.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 0);
	this->renderer_2d->bind_image(this->scene_renderer->get_attachments().normal.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 1);
	this->renderer_2d->bind_image(this->scene_renderer->get_attachments().albedo.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 2);
	this->renderer_2d->bind_image(this->scene_renderer->get_attachments().depth_stencil.image_view, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit, 3);

	//m_mainCamera.component<Camera>()->setAspectRatio((float)width / (float)height);
}

void ::framework::graphics::deferred_renderer::update(double deltaT)
{
	::std::stringstream ss;

	ss << "indices this->gpu_context->get_device() memory: " << this->scene_renderer->get_indices_device_memory_usage() << " / " << this->scene_renderer->get_indices_buffer_size() << " (Bytes)";
	text_renderer->add_text(ss.str(), 5.0f, 45.0f, ::framework::graphics::text_renderer::text_align::align_left, ::framework::graphics::text_renderer::vertical_align::top);
	ss.str("");

	ss << "vertices this->gpu_context->get_device() memory: " << this->scene_renderer->get_vertices_device_memory_usage() << " / " << this->scene_renderer->get_vertices_buffer_size() << " (Bytes)";
	text_renderer->add_text(ss.str(), 5.0f, 65.0f, ::framework::graphics::text_renderer::text_align::align_left, ::framework::graphics::text_renderer::vertical_align::top);

	text_renderer->end_text_update();

	this->frame_index = this->frame_index % 3;

	this->single_frame_allocator.clear();

	//float lightMoveSpeed = 1.0f;
	//float forward = lightMoveSpeed * (
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_i) ? deltaT : 0.0f) +
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_k) ? -deltaT : 0.0f)
	//	);
	//float strafe = lightMoveSpeed * (
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_j) ? deltaT : 0.0f) +
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_l) ? -deltaT : 0.0f)
	//	);
	//float ascent = lightMoveSpeed * (
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_u) ? deltaT : 0.0f) +
	//	(this->my_input_listner->get_key_state(::framework::input::key::key_o) ? -deltaT : 0.0f)
	//	);
	//m_lightPos += ::glm::vec4(forward, ascent, ascent, 0.0f);

	::framework::gpu::result acquire_next_image_result;
	::std::uint32_t swap_chain_image;

	::framework::gpu::acquire_next_image_info acquire_next_image_info;
	acquire_next_image_info.swap_chain = swap_chain;
	acquire_next_image_info.timeout = UINT64_MAX;
	acquire_next_image_info.semaphore = present_complete_semaphore;
	acquire_next_image_info.fence = nullptr;
	acquire_next_image_info.device_mask = 0x1;
	acquire_next_image_result = this->gpu_context->get_device()->acquire_next_image(&acquire_next_image_info, &swap_chain_image);

	if (acquire_next_image_result == ::framework::gpu::result::error_out_of_date)
	{
		this->resize(this->window->get_width(), this->window->get_height());
	}
	else
	{
		assert_framework_gpu_result(acquire_next_image_result);
	}

	//todo: since renderers are using Swapchain backbuffer for rendering to, we restricted to use
	// current backbuffer on d3d12 as active rendertarget. Create separate frame_buffer for this renderers
	// and only use swap_chain backbuffer
	this->frame_index = swap_chain_image;

	this->scene_renderer->update_command_buffer(this->frame_index);
	this->text_renderer->update_command_buffer(this->frame_index);
	this->renderer_2d->update_command_buffer(this->frame_index);

	::framework::gpu::semaphore * wait_semaphores[] = { present_complete_semaphore };
	::framework::gpu::pipeline_stage_flags wait_dst_stage_masks[] = { ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit };
	//CommandBuffer * pCommandBuffers[] = { this->text_renderer->get_command_buffer(swap_chain_image), this->renderer_2d->get_command_buffer(swap_chain_image) };

	//::framework::gpu::submit_info submit_infos[2];
	//submit_infos[0].wait_semaphore_count = 1;
	//submit_infos[0].wait_semaphores = wait_semaphores;
	//submit_infos[0].wait_dst_stage_mask = wait_dst_stage_mask;
	//submit_infos[0].command_buffer_count = 1;
	//submit_infos[0].command_buffers = &m_pPrimaryCommandBuffers[swap_chain_image];
	//submit_infos[0].signal_semaphore_count = 1;
	//submit_infos[0].signal_semaphores = &pDeferredRenderCompleteSemaphore[swap_chain_image];
	//submit_infos[1].wait_semaphore_count = 1;
	//submit_infos[1].wait_semaphores = &pDeferredRenderCompleteSemaphore[swap_chain_image];
	//submit_infos[1].wait_dst_stage_mask = wait_dst_stage_mask;
	//submit_infos[1].command_buffer_count = ::std::size(pCommandBuffers);
	//submit_infos[1].command_buffers = pCommandBuffers;
	//submit_infos[1].signal_semaphore_count = 1;
	//submit_infos[1].signal_semaphores = &present_complete_semaphore[swap_chain_image];
	//CHECK_RESULT(queues.graphics->submit(::std::size(submit_infos), submit_infos, d3d12_fence[swap_chain_image]));

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::image_copy image_copy;
	image_copy.src_subresource = { ::framework::gpu::image_aspect_flags::color_bit, 0, 0, 1 };
	image_copy.src_offset = { 0,0,0 };
	image_copy.dst_subresource = { ::framework::gpu::image_aspect_flags::color_bit, 0, 0, 1 };
	image_copy.dst_offset = { 0,0,0 };
	image_copy.extent = { this->width, this->height, 1 };

	::framework::gpu::image_subresource_range subresource_range;
	subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	subresource_range.base_mip_level = 0;
	subresource_range.level_count = 1;
	subresource_range.base_array_layer = 0;
	subresource_range.layer_count = 1;

	::framework::gpu::image_memory_barrier image_memory_barriers[2];
	image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit;
	image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
	image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;
	image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
	image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
	image_memory_barriers[0].image = this->frame_resources[this->frame_index].render_target_image;
	image_memory_barriers[0].subresource_range = subresource_range;

	image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
	image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
	image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
	image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::present_src_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
	image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
	image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
	image_memory_barriers[1].image = this->swap_chain_images[this->frame_index];
	image_memory_barriers[1].subresource_range = subresource_range;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = 0;
	dependency_info.memory_barriers = nullptr;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
	dependency_info.image_memory_barriers = image_memory_barriers;

	assert_framework_gpu_result(this->frame_resources[this->frame_index].command_buffer->begin_command_buffer(&command_buffer_begin_info));
	this->frame_resources[this->frame_index].command_buffer->pipeline_barrier(&dependency_info);
	this->frame_resources[this->frame_index].command_buffer->copy_image(this->frame_resources[this->frame_index].render_target_image, ::framework::gpu::image_layout_flags::transfer_src_optimal_bit, this->swap_chain_images[this->frame_index], ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit, 1, &image_copy);

	image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit;
	image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::transfer_src_optimal_bit;
	image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
	image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;
	image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
	image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::present_src_bit;

	this->frame_resources[this->frame_index].command_buffer->pipeline_barrier(&dependency_info);
	assert_framework_gpu_result(this->frame_resources[this->frame_index].command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_infos;
	wait_semaphore_infos.semaphore = this->present_complete_semaphore;
	wait_semaphore_infos.value = 0;
	wait_semaphore_infos.stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	wait_semaphore_infos.device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_infos[4];
	command_buffer_infos[0].command_buffer = this->scene_renderer->get_command_buffer(this->frame_index);
	command_buffer_infos[0].device_mask = 0;

	command_buffer_infos[1].command_buffer = this->text_renderer->get_command_buffer(this->frame_index);
	command_buffer_infos[1].device_mask = 0;

	command_buffer_infos[2].command_buffer = this->renderer_2d->get_command_buffer(this->frame_index);
	command_buffer_infos[2].device_mask = 0;

	command_buffer_infos[3].command_buffer = this->frame_resources[this->frame_index].command_buffer;
	command_buffer_infos[3].device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_infos;
	signal_semaphore_infos.semaphore = this->frame_resources[this->frame_index].deferred_render_complete_semaphore;
	signal_semaphore_infos.value = 0;
	signal_semaphore_infos.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	signal_semaphore_infos.device_index = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 1;
	submit_info.wait_semaphore_infos = &wait_semaphore_infos;
	submit_info.command_buffer_info_count = static_cast<::std::uint32_t>(::std::size(command_buffer_infos));
	submit_info.command_buffer_infos = command_buffer_infos;
	submit_info.signal_semaphore_info_count = 1;
	submit_info.signal_semaphore_infos = &signal_semaphore_infos;
	assert_framework_gpu_result(queues.graphics->submit(1, &submit_info, this->scene_renderer->get_fence(this->frame_index)));
	assert_framework_gpu_result(queues.graphics->wait_idle());

	::framework::gpu::result result;

	::framework::gpu::present_info present_info;
	present_info.wait_semaphore_count = 1;
	present_info.wait_semaphores = &this->frame_resources[this->frame_index].deferred_render_complete_semaphore;
	present_info.swap_chain_count = 1;
	present_info.swap_chains = &swap_chain;
	present_info.image_indices = &swap_chain_image;
	present_info.results = nullptr;
	result = queues.graphics->present(&present_info);

	if (result == ::framework::gpu::result::error_out_of_date)
	{
		resize(this->window->get_width(), this->window->get_height());
	}
	else
	{
		assert_framework_gpu_result(result);
	}

	this->frame_index++;
}

::framework::graphics::scene_renderer * ::framework::graphics::deferred_renderer::get_scene_renderer()
{
	return this->scene_renderer;
}