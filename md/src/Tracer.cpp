#include "Tracer.h"
#include <gpu/utility.hpp>
#include "camera/scene_view_controller.h"
#include "graphics/deferred_shading/scene_view_perspective.hpp"
#include <resource/resource_manager.hpp>
#include <iterator>
#define _USE_MATH_DEFINES
#include <cmath>

::framework::camera::scene_view_controller * scene_view_controller;
::framework::graphics::deferred_shading::scene_view_perspective * scene_view;

::framework::Tracer::Tracer(TracerCreateInfo const * create_info) :
	physical_device(create_info->physical_device),
	device(create_info->device),
	m_bvh(create_info->pBvh),
	//m_camera(create_info->camera),
	//m_cameraTransform(create_info->cameraTransform),
	resource_manager(create_info->resource_manager),
	width(1280),
	height(720),
	m_samples(1),
	m_bounces(2)
{
}

::framework::Tracer::~Tracer()
{
}

void ::framework::Tracer::allocate()
{
	scene_view = new ::framework::graphics::deferred_shading::scene_view_perspective(nullptr, nullptr);
	scene_view->set_eye(::glm::vec3(0.0f, 0.0f, 0.0f));
	scene_view->set_center(::glm::vec3(0.0f, 0.0f, 1.0f));
	scene_view->set_up(::glm::vec3(0.0f, 1.0f, 0.0f));
	scene_view->set_fov(1.57079632679489661923);
	scene_view->set_aspect_ratio((float)height / (float)width);
	scene_view->set_near_z(0.1f);
	scene_view->set_far_z(4096.0f);
	scene_view->reverse_z(true);
	scene_view->inverse_y(true);
	scene_view->update();

	scene_view_controller = new ::framework::camera::scene_view_controller(scene_view, ::framework::engine_core::input_listner);

	physical_device->get_memory_properties(&physical_device_memory_properties);

	setupRaySampleImage();

	::framework::gpu::sampler_create_info sampler_create_info;
	sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
	sampler_create_info.min_filter = ::framework::gpu::filter::linear;
	sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
	sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.mip_lod_bias = 0.0f;
	sampler_create_info.anisotropy_enable = false;
	sampler_create_info.max_anisotropy = 1.0f;
	sampler_create_info.compare_enable = false;
	sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
	sampler_create_info.min_lod = 0.0f;
	sampler_create_info.max_lod = 0.0f;
	sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
	sampler_create_info.unnormalized_coordinates = false;
	this->device->create_sampler(&sampler_create_info, nullptr, &m_pRaySampleSampler);

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = 0;
	this->device->create_command_pool(&command_pool_create_info, nullptr, &this->primary_command_pool);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->primary_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = 1;
	this->device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPathTracingCommandBuffer);

	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(CameraBufferData) / uboAlignment) * uboAlignment + ((sizeof(CameraBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = dynamicAlignment;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		this->device->create_buffer(&buffer_create_info, nullptr, &m_pCameraUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pCameraUboBuffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pCameraUboBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		this->device->allocate_memory(&memory_allocate_info, nullptr, &m_pCameraUboDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pCameraUboBuffer;
		bind_buffer_memory_info.memory = m_pCameraUboDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		this->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}
	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(VariousBufferData) / uboAlignment) * uboAlignment + ((sizeof(VariousBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = dynamicAlignment;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		this->device->create_buffer(&buffer_create_info, nullptr, &m_pVariousUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pVariousUboBuffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pVariousUboBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		this->device->allocate_memory(&memory_allocate_info, nullptr, &m_pVariousUboDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pVariousUboBuffer;
		bind_buffer_memory_info.memory = m_pVariousUboDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		this->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}
	// PathTracing
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/PathTracing.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/PathTracing.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pPathTracingShader = this->resource_manager->load_shader_module(this->device, shader_module_info);

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->m_pPathTracingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// CameraUB
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// VariousUB
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// RadixTree
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// Spheres
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// raySampleImage
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// CompactTreeNodes
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pPathTracingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pPathTracingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pPathTracingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pPathTracingPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		this->device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pPathTracingPipeline);

		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[4];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_pool_sizes[0].descriptor_count = 2;

		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_pool_sizes[1].descriptor_count = 1;
		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_image;

		descriptor_pool_sizes[2].descriptor_count = 1;
		descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_pool_sizes[3].descriptor_count = 3;

		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
		descriptor_pool_create_info.max_sets = 1;
		descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
		this->device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &descriptor_pool);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pPathTracingDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		this->device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pPathTracingDescriptorSet);

		::framework::gpu::descriptor_image_info descriptor_image_info;
		descriptor_image_info.sampler = m_pRaySampleSampler;
		descriptor_image_info.image_view = m_pRaySampleImageView;
		descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;

		class ::framework::gpu::buffer_view * node_buffer_view[] = { m_bvh->getNodeBufferView() };

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
		descriptor_buffer_infos[0].buffer = m_pCameraUboBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = (sizeof(CameraBufferData) / 256) * 256 + ((sizeof(CameraBufferData) % 256) > 0 ? 256 : 0);

		descriptor_buffer_infos[1].buffer = m_pVariousUboBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = (sizeof(VariousBufferData) / 256) * 256 + ((sizeof(VariousBufferData) % 256) > 0 ? 256 : 0);

		descriptor_buffer_infos[2].buffer = m_bvh->getRadixTreeBuffer();
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(lbvh::CompactTreeNode);

		descriptor_buffer_infos[3].buffer = m_bvh->getPrimitiveBuffer();
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = sizeof(lbvh::CompactSphere);

		::framework::gpu::write_descriptor_set write_descriptor_sets[6];
		write_descriptor_sets[0].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];

		write_descriptor_sets[3].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];

		write_descriptor_sets[4].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		write_descriptor_sets[4].image_info = &descriptor_image_info;

		write_descriptor_sets[5].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[5].dst_binding = 5;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		write_descriptor_sets[5].texel_buffer_view = node_buffer_view;

		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	{
		class ::framework::gpu::queue * command_queue;
		this->device->get_queue(0, 0, &command_queue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::none;
		command_pool_create_info.queue_family_index = 0;

		class ::framework::gpu::command_pool * command_pool;
		this->device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * command_buffer;
		this->device->allocate_command_buffers(&command_buffer_allocate_info, &command_buffer);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		command_buffer_begin_info.inheritance_info = nullptr;

		::framework::gpu::image_memory_barrier image_memory_barrier;
		image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::none;
		image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
		image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barrier.src_queue_family_ownership.queue_family_index = this->queue_family_indices.transfer;
		image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->queue_family_indices.transfer;
		image_memory_barrier.image = m_pRaySampleImage;
		image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 1;
		dependency_info.image_memory_barriers = &image_memory_barrier;

		command_buffer->begin_command_buffer(&command_buffer_begin_info);
		//command_buffer->pipeline_barrier(&dependency_info);
		//command_buffer->dependency_info(0, 1, &dependency_info);
		command_buffer->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		this->device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = command_buffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		command_queue->submit(1, &submit_info, fence);

		this->device->wait_for_fences(1, &fence, true, UINT64_MAX);

		this->device->free_command_buffers(command_pool, 1, &command_buffer);
		this->device->destroy_command_pool(command_pool, nullptr);
		this->device->destroy_fence(fence, nullptr);
	}
}

void ::framework::Tracer::setupRaySampleImage()
{
	::framework::gpu::image_create_info image_create_info;
	image_create_info.flags = ::framework::gpu::image_create_flags::none;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = ::framework::gpu::format::b8g8r8a8_unorm;
	image_create_info.extent = { width, height, 1 };
	image_create_info.mip_levels = 1;
	image_create_info.array_layers = 1;
	image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
	image_create_info.usage = ::framework::gpu::image_usage_flags::sampled_bit | ::framework::gpu::image_usage_flags::storage_bit;
	image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
	image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	image_create_info.optimized_clear_value = nullptr;
	this->device->create_image(&image_create_info, nullptr, &m_pRaySampleImage);

	::framework::gpu::memory_requirements memory_requirements;

	::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
	image_memory_requirements_info.image = m_pRaySampleImage;
	this->device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
	memory_allocate_info.device_mask = 0;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
	memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
	memory_allocate_info.opaque_capture_address = 0;
	this->device->allocate_memory(&memory_allocate_info, nullptr, &m_pRaySampleDeviceMemory);

	::framework::gpu::bind_image_memory_info bind_image_memory_info;
	bind_image_memory_info.image = m_pRaySampleImage;
	bind_image_memory_info.memory = m_pRaySampleDeviceMemory;
	bind_image_memory_info.memory_offset = 0;
	this->device->bind_image_memory(1, &bind_image_memory_info);

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
	image_view_create_info.image = m_pRaySampleImage;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = ::framework::gpu::format::b8g8r8a8_unorm;
	image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
	this->device->create_image_view(&image_view_create_info, nullptr, &m_pRaySampleImageView);
}

void ::framework::Tracer::allocate(TracerCreateInfo const * create_info, Tracer ** ppTracer)
{
	*ppTracer = new Tracer(create_info);
	(*ppTracer)->allocate();
}

void ::framework::Tracer::deallocate(Tracer * pTracer)
{
	delete pTracer;
}

void ::framework::Tracer::trace(class ::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, class ::framework::gpu::semaphore * signal_semaphore, float deltaT)
{
	::framework::engine_core::input_listner->begin_frame_update();
	scene_view_controller->update(deltaT);

	class ::framework::gpu::queue * command_queue;
	this->device->get_queue(0, 0, &command_queue);
	{
		CameraBufferData cameraBufferData;
		cameraBufferData.viewMatrix = scene_view->get_view();
		cameraBufferData.projMatrix = scene_view->get_projection();
		cameraBufferData.viewProjMatrix = scene_view->get_view_projection();
		cameraBufferData.invViewProjMatrix = ::glm::inverse(cameraBufferData.viewProjMatrix);
		cameraBufferData.position = ::glm::vec4(scene_view->get_eye(), 0.0f);
		cameraBufferData.direction = ::glm::vec4(::glm::normalize(scene_view->get_center() - scene_view->get_eye()), 0.0f);

		void * pMappedData;
		this->device->map_memory(m_pCameraUboDeviceMemory, 0, sizeof(CameraBufferData), ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(pMappedData, &cameraBufferData, sizeof(CameraBufferData));
		this->device->unmap_memory(m_pCameraUboDeviceMemory);
	}
	{
		VariousBufferData variousBufferData;

		float t = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		float dt = 0.5f;
		variousBufferData.vScreenResolution = ::glm::vec4(width, height, 1.0f / width, 1.0f / height);
		variousBufferData.vTime = ::glm::vec4(t / 20.0, t, t * 2.0, t * 3.0);
		variousBufferData.vSinTime = ::glm::vec4(sin(t / 8.0), sin(t / 4.0), sin(t / 2.0), sin(t));
		variousBufferData.vCosTime = ::glm::vec4(cos(t / 8.0), cos(t / 4.0), cos(t / 2.0), cos(t));
		variousBufferData.vDeltaTime = ::glm::vec4(dt, 1.0 / dt, dt, 1.0 / dt);

		void * pMappedData;
		this->device->map_memory(m_pVariousUboDeviceMemory, 0, sizeof(VariousBufferData), ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(pMappedData, &variousBufferData, sizeof(VariousBufferData));
		this->device->unmap_memory(m_pVariousUboDeviceMemory);
	}

	::std::uint32_t pushConstants[3] = { 1, 3, m_bvh->getLeavesCount() };

	//PipelineBarrier pipelineBarriers[3];
	//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
	//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

	//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//buffer_memory_barriers[0].pipelineBarrierCount = 1;
	//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].buffer = m_bvh->getPrimitiveBuffer();
	//buffer_memory_barriers[0].offset = 0;
	//buffer_memory_barriers[0].size = m_bvh->getPrimitiveBuffer()->GetBufferCreateInfo()->size;

	//buffer_memory_barriers[1].pipelineBarrierCount = 1;
	//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].buffer = m_bvh->getRadixTreeBuffer();
	//buffer_memory_barriers[1].offset = 0;
	//buffer_memory_barriers[1].size = m_bvh->getRadixTreeBuffer()->GetBufferCreateInfo()->size;

	//::framework::gpu::image_memory_barrier image_memory_barrier;
	//image_memory_barrier.pipelineBarrierCount = 1;
	//image_memory_barrier.pPipelineBarriers = &pipelineBarriers[2];
	//image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//image_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//image_memory_barrier.image = m_pRaySampleImage;
	//image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

	//::framework::gpu::dependency_info dependency_info;
	//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
	//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	//dependency_info.image_memory_barrier_count = 1;
	//dependency_info.image_memory_barriers = &image_memory_barrier;

	::framework::gpu::memory_barrier memory_barrier[1];
	memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
	dependency_info.memory_barriers = memory_barrier;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	m_pPathTracingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//m_pPathTracingCommandBuffer->dependency_info(0, 1, &dependency_info);
	m_pPathTracingCommandBuffer->pipeline_barrier(&dependency_info);
	m_pPathTracingCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pPathTracingPipeline);
	m_pPathTracingCommandBuffer->bind_descriptor_pool(this->descriptor_pool);
	m_pPathTracingCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pPathTracingPipelineLayout, 0, 1, &m_pPathTracingDescriptorSet, 0, nullptr);
	//m_pPathTracingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pPathTracingDescriptorTable, 0, nullptr);
	m_pPathTracingCommandBuffer->push_constants(m_pPathTracingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), pushConstants);
	m_pPathTracingCommandBuffer->dispatch((width + 16 - 1) / 16, (height + 16 - 1) / 16, 1);
	m_pPathTracingCommandBuffer->pipeline_barrier(&dependency_info);
	m_pPathTracingCommandBuffer->end_command_buffer();

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = wait_semaphore;
	wait_semaphore_info.value = 0;
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = m_pPathTracingCommandBuffer;
	command_buffer_info.device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = signal_semaphore;
	signal_semaphore_info.value = 0;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_info.device_index = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 1;
	submit_info.wait_semaphore_infos = &wait_semaphore_info;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = 1;
	submit_info.signal_semaphore_infos = &signal_semaphore_info;
	command_queue->submit(1, &submit_info, nullptr);

	device->wait_idle();
	::framework::engine_core::input_listner->end_frame_update();
}

void ::framework::Tracer::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->width = width;
	this->height = height;

	this->device->wait_idle();

	this->device->destroy_image_view(m_pRaySampleImageView, nullptr);
	this->device->destroy_image(m_pRaySampleImage, nullptr);
	this->device->free_memory(m_pRaySampleDeviceMemory, nullptr);

	setupRaySampleImage();

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_pRaySampleSampler;
	descriptor_image_info.image_view = m_pRaySampleImageView;
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;

	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = m_pPathTracingDescriptorSet;
	write_descriptor_set.dst_binding = 6;
	write_descriptor_set.dst_array_element = 0;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_set.image_info = &descriptor_image_info;
	this->device->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);
}

class ::framework::gpu::sampler * ::framework::Tracer::GetGetRayTracedSampler() const
{
	return m_pRaySampleSampler;
}

class ::framework::gpu::image * ::framework::Tracer::GetRayTracedImage() const
{
	return m_pRaySampleImage;
}

class ::framework::gpu::image_view * ::framework::Tracer::GetRayTracedImageView() const
{
	return m_pRaySampleImageView;
}