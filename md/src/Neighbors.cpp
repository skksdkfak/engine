#include <iostream>
#include <algorithm>
#include <resource/resource_manager.hpp>
#include "gpu/utility.hpp"
#include "Neighbors.h"

::framework::Neighbors::Neighbors(NeighborsCreateInfo const * create_info) :
	physical_device(create_info->physical_device),
	device(create_info->device),
	m_bvh(create_info->pBvh),
	resource_manager(create_info->resource_manager),
	m_neighborsMax(32),
	m_currentDescriptorTable(0)
{
}

::framework::Neighbors::~Neighbors()
{
}

void ::framework::Neighbors::allocate()
{
	this->physical_device->get_memory_properties(&physical_device_memory_properties);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::glm::vec4) * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pOldVelocityBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pOldVelocityBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pOldVelocityDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pOldVelocityBuffer;
		bind_buffer_memory_info.memory = m_pOldVelocityDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::glm::vec4) * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNewVelocityBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pNewVelocityBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNewVelocityDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pNewVelocityBuffer;
		bind_buffer_memory_info.memory = m_pNewVelocityDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNeighborsCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pNeighborsCounterBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNeighborsCounterDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pNeighborsCounterBuffer;
		bind_buffer_memory_info.memory = m_pNeighborsCounterDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * 2 * m_neighborsMax * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNeighborsListBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pNeighborsListBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNeighborsListDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pNeighborsListBuffer;
		bind_buffer_memory_info.memory = m_pNeighborsListDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNeighborsHeadListBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pNeighborsHeadListBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNeighborsHeadListDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pNeighborsHeadListBuffer;
		bind_buffer_memory_info.memory = m_pNeighborsHeadListDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * 2 * m_neighborsMax * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pReadbackBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pReadbackDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pReadbackBuffer;
		bind_buffer_memory_info.memory = m_pReadbackDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pReadbackBuffer1);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pReadbackBuffer1;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pReadbackDeviceMemory1);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pReadbackBuffer1;
		bind_buffer_memory_info.memory = m_pReadbackDeviceMemory1;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_bvh->getMaxLeavesCount();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pReadbackBuffer2);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pReadbackBuffer2;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pReadbackDeviceMemory2);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pReadbackBuffer2;
		bind_buffer_memory_info.memory = m_pReadbackDeviceMemory2;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = 0;
	device->create_command_pool(&command_pool_create_info, nullptr, &this->primary_command_pool);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->primary_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = 1;
	device->allocate_command_buffers(&command_buffer_allocate_info, &m_pNeighborsCommandBuffer);
	device->allocate_command_buffers(&command_buffer_allocate_info, &m_pNeighborsCounterServicingCommandBuffer);
	device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCollideCommandBuffer);
	device->allocate_command_buffers(&command_buffer_allocate_info, &m_pIntegrateCommandBuffer);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pNeighborsCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pCollideCompleteSemaphore);

	{
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		command_buffer_begin_info.inheritance_info = nullptr;

		//PipelineBarrier pipelineBarriers[3];
		//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
		//buffer_memory_barriers[0].pipelineBarrierCount = 1;
		//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].buffer = m_pNeighborsHeadListBuffer;
		//buffer_memory_barriers[0].offset = 0;
		//buffer_memory_barriers[0].size = m_pNeighborsHeadListBuffer->GetBufferCreateInfo()->size;

		//buffer_memory_barriers[1].pipelineBarrierCount = 1;
		//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].buffer = m_pNeighborsListBuffer;
		//buffer_memory_barriers[1].offset = 0;
		//buffer_memory_barriers[1].size = m_pNeighborsListBuffer->GetBufferCreateInfo()->size;

		//buffer_memory_barriers[2].pipelineBarrierCount = 1;
		//buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		//buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[2].buffer = m_pNeighborsCounterBuffer;
		//buffer_memory_barriers[2].offset = 0;
		//buffer_memory_barriers[2].size = m_pNeighborsCounterBuffer->GetBufferCreateInfo()->size;

		//::framework::gpu::dependency_info dependency_info;
		//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::global_memory_barrier;
		//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		//dependency_info.image_memory_barrier_count = 0;
		//dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barriers[0].src_queue_family_ownership;
		buffer_memory_barriers[0].dst_queue_family_ownership;
		buffer_memory_barriers[0].buffer = nullptr;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		m_pNeighborsCounterServicingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
		//m_pNeighborsCounterServicingCommandBuffer->dependency_info(0, 1, &dependency_info);
		//m_pNeighborsCounterServicingCommandBuffer->pipeline_barrier(&dependency_info);
		m_pNeighborsCounterServicingCommandBuffer->fill_buffer(m_pNeighborsHeadListBuffer, 0, sizeof(::std::uint32_t) * m_bvh->getMaxLeavesCount(), UINT32_MAX, 0, 0, 0);
		m_pNeighborsCounterServicingCommandBuffer->fill_buffer(m_pNeighborsListBuffer, 0, sizeof(::std::uint32_t) * m_neighborsMax * 2 * m_bvh->getMaxLeavesCount(), UINT32_MAX, 0, 0, 0);
		m_pNeighborsCounterServicingCommandBuffer->fill_buffer(m_pNeighborsCounterBuffer, 0, sizeof(::std::uint32_t), 0, 0, 0, 0);

		//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		//m_pNeighborsCounterServicingCommandBuffer->dependency_info(0, 1, &dependency_info);
		m_pNeighborsCounterServicingCommandBuffer->end_command_buffer();
	}

	// NeighborsList
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/neighbor.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/neighbor.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pNeighborsShader = this->resource_manager->load_shader_module(this->device, shader_module_info);

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[5];
		// RadixTree
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// Spheres
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// Counter
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// NeighborsList
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// NeighborsHeadList
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pNeighborsDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pNeighborsDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pNeighborsPipelineLayout);

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pNeighborsShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pNeighborsPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pNeighborsPipeline);

		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[3];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_pool_sizes[0].descriptor_count = 6 + 100;
		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_pool_sizes[1].descriptor_count = 1 + 100;
		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_pool_sizes[2].descriptor_count = 9 + 100;

		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
		descriptor_pool_create_info.max_sets = 5 + 10;
		descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
		device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &descriptor_pool);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pNeighborsDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pNeighborsDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[5];
		descriptor_buffer_infos[0].buffer = m_bvh->getRadixTreeBuffer();
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactTreeNode);

		descriptor_buffer_infos[1].buffer = m_bvh->getPrimitiveBuffer();
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(lbvh::CompactSphere);

		descriptor_buffer_infos[2].buffer = m_pNeighborsCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

		descriptor_buffer_infos[3].buffer = m_pNeighborsListBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t) * 2;

		descriptor_buffer_infos[4].buffer = m_pNeighborsHeadListBuffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[5];
		write_descriptor_sets[0].dst_set = m_pNeighborsDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pNeighborsDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pNeighborsDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];

		write_descriptor_sets[3].dst_set = m_pNeighborsDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];

		write_descriptor_sets[4].dst_set = m_pNeighborsDescriptorSet;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// CollideSystem
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/collide.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/collide.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pCollideShader = this->resource_manager->load_shader_module(this->device, shader_module_info);

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// Spheres
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// NewVelocity
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// OldVelocity
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// NeighborsList
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// NeighborsHeadList
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// Counter
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pCollideDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCollideDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pCollidePipelineLayout);

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pCollideShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCollidePipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pCollidePipeline);

		::framework::gpu::descriptor_set_layout * pCollideDescriptorSetLayouts[2] = { m_pCollideDescriptorSetLayout, m_pCollideDescriptorSetLayout };

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(pCollideDescriptorSetLayouts));
		descriptor_set_allocate_info.set_layouts = pCollideDescriptorSetLayouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, m_pCollideDescriptorSets);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[6];
		descriptor_buffer_infos[0].buffer = m_bvh->getPrimitiveBuffer();
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactSphere);

		descriptor_buffer_infos[1].buffer = m_pNewVelocityBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::glm::vec4);

		descriptor_buffer_infos[2].buffer = m_pOldVelocityBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::glm::vec4);

		descriptor_buffer_infos[3].buffer = m_pNeighborsListBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t) * 2;

		descriptor_buffer_infos[4].buffer = m_pNeighborsHeadListBuffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t);

		descriptor_buffer_infos[5].buffer = m_pNeighborsCounterBuffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[5].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[9];
		// Spheres 
		write_descriptor_sets[0].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// NewVelocity
		write_descriptor_sets[1].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		// OldVelocity
		write_descriptor_sets[2].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		// NeighborsList
		write_descriptor_sets[3].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		// NeighborsHeadList
		write_descriptor_sets[4].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		// NewVelocity
		write_descriptor_sets[5].dst_set = m_pCollideDescriptorSets[1];
		write_descriptor_sets[5].dst_binding = 1;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[2];
		// OldVelocity
		write_descriptor_sets[6].dst_set = m_pCollideDescriptorSets[1];
		write_descriptor_sets[6].dst_binding = 2;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[1];
		// Counter
		write_descriptor_sets[7].dst_set = m_pCollideDescriptorSets[0];
		write_descriptor_sets[7].dst_binding = 5;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[5];
		// Counter
		write_descriptor_sets[8].dst_set = m_pCollideDescriptorSets[1];
		write_descriptor_sets[8].dst_binding = 5;
		write_descriptor_sets[8].dst_array_element = 0;
		write_descriptor_sets[8].descriptor_count = 1;
		write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[5];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		::framework::gpu::copy_descriptor_set copyDescriptorSets[3];
		// Spheres 
		copyDescriptorSets[0].src_set = m_pCollideDescriptorSets[0];
		copyDescriptorSets[0].src_binding = 0;
		copyDescriptorSets[0].src_array_element = 0;
		copyDescriptorSets[0].dst_set = m_pCollideDescriptorSets[1];
		copyDescriptorSets[0].dst_binding = 0;
		copyDescriptorSets[0].dst_array_element = 0;
		copyDescriptorSets[0].descriptor_count = 1;
		// NeighborsList
		copyDescriptorSets[1].src_set = m_pCollideDescriptorSets[0];
		copyDescriptorSets[1].src_binding = 3;
		copyDescriptorSets[1].src_array_element = 0;
		copyDescriptorSets[1].dst_set = m_pCollideDescriptorSets[1];
		copyDescriptorSets[1].dst_binding = 3;
		copyDescriptorSets[1].dst_array_element = 0;
		copyDescriptorSets[1].descriptor_count = 1;
		// NeighborsHeadList
		copyDescriptorSets[2].src_set = m_pCollideDescriptorSets[0];
		copyDescriptorSets[2].src_binding = 4;
		copyDescriptorSets[2].src_array_element = 0;
		copyDescriptorSets[2].dst_set = m_pCollideDescriptorSets[1];
		copyDescriptorSets[2].dst_binding = 4;
		copyDescriptorSets[2].dst_array_element = 0;
		copyDescriptorSets[2].descriptor_count = 1;
		device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
	}

	// IntegrateSystem
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/integrate.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/integrate.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pIntegrateShader = this->resource_manager->load_shader_module(this->device, shader_module_info);

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// Spheres
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// NewVelocity
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// OldVelocity
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pIntegrateDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pIntegrateDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pIntegratePipelineLayout);

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pIntegrateShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pIntegratePipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pIntegratePipeline);

		::framework::gpu::descriptor_set_layout * pIntegrateDescriptorSetLayouts[2] = { m_pIntegrateDescriptorSetLayout, m_pIntegrateDescriptorSetLayout };

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(pIntegrateDescriptorSetLayouts));;
		descriptor_set_allocate_info.set_layouts = pIntegrateDescriptorSetLayouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, m_pIntegrateDescriptorSets);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_bvh->getPrimitiveBuffer();
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactSphere);

		descriptor_buffer_infos[1].buffer = m_pNewVelocityBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::glm::vec4);

		descriptor_buffer_infos[2].buffer = m_pOldVelocityBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::glm::vec4);

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		// Spheres
		write_descriptor_sets[0].dst_set = m_pIntegrateDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// NewVelocity
		write_descriptor_sets[1].dst_set = m_pIntegrateDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		// NewVelocity
		write_descriptor_sets[2].dst_set = m_pIntegrateDescriptorSets[1];
		write_descriptor_sets[2].dst_binding = 1;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		::framework::gpu::copy_descriptor_set copyDescriptorSets[1];
		copyDescriptorSets[0].src_set = m_pIntegrateDescriptorSets[0];
		copyDescriptorSets[0].src_binding = 0;
		copyDescriptorSets[0].src_array_element = 0;
		copyDescriptorSets[0].dst_set = m_pIntegrateDescriptorSets[1];
		copyDescriptorSets[0].dst_binding = 0;
		copyDescriptorSets[0].dst_array_element = 0;
		copyDescriptorSets[0].descriptor_count = 1;
		device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
	}
}

void ::framework::Neighbors::allocate(NeighborsCreateInfo const * create_info, Neighbors ** ppNeighbors)
{
	*ppNeighbors = new Neighbors(create_info);
	(*ppNeighbors)->allocate();
}

void ::framework::Neighbors::deallocate(Neighbors * ppNeighbors)
{
	delete ppNeighbors;
}

static ::std::uint32_t neighbors_count = 0;

void ::framework::Neighbors::execute(::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore)
{
	::framework::gpu::queue * command_queue;
	device->get_queue(0, 0, &command_queue);
	::std::uint32_t pushConstants[1] = { m_bvh->getLeavesCount() };

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = 0;
	buffer_copy.dst_offset = 0;
	buffer_copy.size = sizeof(::std::uint32_t);

	//PipelineBarrier pipelineBarriers[2];
	//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//buffer_memory_barriers[0].pipelineBarrierCount = 1;
	//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].buffer = m_bvh->getRadixTreeBuffer();
	//buffer_memory_barriers[0].offset = 0;
	//buffer_memory_barriers[0].size = m_bvh->getRadixTreeBuffer()->GetBufferCreateInfo()->size;

	//buffer_memory_barriers[1].pipelineBarrierCount = 1;
	//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].buffer = m_bvh->getPrimitiveBuffer();
	//buffer_memory_barriers[1].offset = 0;
	//buffer_memory_barriers[1].size = m_bvh->getPrimitiveBuffer()->GetBufferCreateInfo()->size;

	//::framework::gpu::dependency_info dependency_info;
	//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
	//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	//dependency_info.image_memory_barrier_count = 0;
	//dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
	buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
	buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
	buffer_memory_barriers[0].src_queue_family_ownership;
	buffer_memory_barriers[0].dst_queue_family_ownership;
	buffer_memory_barriers[0].buffer = nullptr;
	buffer_memory_barriers[0].offset = 0;
	buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
	dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	m_pNeighborsCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//m_pNeighborsCommandBuffer->dependency_info(0, 1, &dependency_info);
	//m_pNeighborsCommandBuffer->pipeline_barrier(&dependency_info);
	m_pNeighborsCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pNeighborsPipeline);
	m_pNeighborsCommandBuffer->bind_descriptor_pool(this->descriptor_pool);
	//m_pNeighborsCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pNeighborsDescriptorTable, 0, nullptr);
	m_pNeighborsCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pNeighborsPipelineLayout, 0, 1, &m_pNeighborsDescriptorSet, 0, nullptr);
	m_pNeighborsCommandBuffer->push_constants(m_pNeighborsPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), pushConstants);
	m_pNeighborsCommandBuffer->dispatch((m_bvh->getLeavesCount() + 128 - 1) / 128, 1, 1);
	m_pNeighborsCommandBuffer->copy_buffer(m_pNeighborsCounterBuffer, m_pReadbackBuffer, 1, &buffer_copy);
	m_pNeighborsCommandBuffer->end_command_buffer();

	::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
	wait_semaphore_infos[0].semaphore = wait_semaphore;
	wait_semaphore_infos[0].value = 0;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = m_pNeighborsCompleteSemaphore;
	wait_semaphore_infos[1].value = 0;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[1].device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_infos[2];
	command_buffer_infos[0].command_buffer = m_pNeighborsCounterServicingCommandBuffer;
	command_buffer_infos[0].device_mask = 0;

	command_buffer_infos[1].command_buffer = m_pNeighborsCommandBuffer;
	command_buffer_infos[1].device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_infos[2];
	signal_semaphore_infos[0].semaphore = m_pNeighborsCompleteSemaphore;
	signal_semaphore_infos[0].value = 0;
	signal_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_infos[0].device_index = 0;

	signal_semaphore_infos[1].semaphore = signal_semaphore;
	signal_semaphore_infos[1].value = 0;
	signal_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_infos[1].device_index = 0;

	::framework::gpu::submit_info submit_infos[2];
	submit_infos[0].flags = ::framework::gpu::submit_flags::none;
	submit_infos[0].wait_semaphore_info_count = 1;
	submit_infos[0].wait_semaphore_infos = &wait_semaphore_infos[0];
	submit_infos[0].command_buffer_info_count = 1;
	submit_infos[0].command_buffer_infos = &command_buffer_infos[0];
	submit_infos[0].signal_semaphore_info_count = 1;
	submit_infos[0].signal_semaphore_infos = &signal_semaphore_infos[0];

	submit_infos[1].flags = ::framework::gpu::submit_flags::none;
	submit_infos[1].wait_semaphore_info_count = 1;
	submit_infos[1].wait_semaphore_infos = &wait_semaphore_infos[1];
	submit_infos[1].command_buffer_info_count = 1;
	submit_infos[1].command_buffer_infos = &command_buffer_infos[1];
	submit_infos[1].signal_semaphore_info_count = 1;
	submit_infos[1].signal_semaphore_infos = &signal_semaphore_infos[1];
	command_queue->submit(static_cast<::std::uint32_t>(::std::size(submit_infos)), submit_infos, nullptr);

	device->wait_idle();

	::std::vector<::std::uint32_t> neighborsReadback(m_bvh->getLeavesCount());
	void * pMappedData;
	device->map_memory(m_pReadbackDeviceMemory, 0, sizeof(::std::uint32_t), ::framework::gpu::memory_map_flags::none, &pMappedData);
	::std::memcpy(neighborsReadback.data(), pMappedData, sizeof(::std::uint32_t));
	device->unmap_memory(m_pReadbackDeviceMemory);
	::std::cout << "neighborsCount: " << neighborsReadback[0] << ::std::endl;
	neighbors_count = neighborsReadback[0];
	//::std::uint32_t neighborsCount = 0;
	//::std::uint32_t maxNeighbors = 0;
	//::std::uint32_t maxNeighborsCount = 0;
	//::std::uint32_t median = 0;
	//::std::uint32_t naighborsGreaterThenNCount = 0;
	//::std::uint32_t static threshold = 9;
	//threshold++;
	//::std::for_each(neighborsReadback.begin(), neighborsReadback.end(), [&](::std::uint32_t & n)
	//{
	//	if (n)
	//	{
	//		if (n > maxNeighbors)
	//		{
	//			maxNeighbors = n;
	//			maxNeighborsCount = 0;
	//		}
	//		else if (n == maxNeighbors)
	//		{
	//			maxNeighborsCount++;
	//		}
	//		neighborsCount += n;
	//		if (n > threshold)
	//			naighborsGreaterThenNCount += n;
	//		//::std::cout << n << ", ";
	//	}
	//});

	//::std::sort(neighborsReadback.begin(), neighborsReadback.end(), ::std::greater<::std::uint32_t>());
	//::std::size_t n = neighborsReadback.size() / 2;
	//::std::nth_element(neighborsReadback.begin(), neighborsReadback.begin() + n, neighborsReadback.end());
	//median = neighborsReadback[n];

	//::std::cout << "neighborsCount: " << neighborsCount << ::std::endl;
	//::std::cout << "maxNeighbors: " << maxNeighbors << ::std::endl;
	//::std::cout << "maxNeighborsCount: " << maxNeighborsCount << ::std::endl;
	//::std::cout << "median: " << median << ::std::endl;
	//::std::cout << "naighborsGreaterThen " << threshold << " : " << naighborsGreaterThenNCount << ::std::endl;
}

void ::framework::Neighbors::integrate(::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore)
{
	static auto tStart = ::std::chrono::high_resolution_clock::now();
	auto tEnd = ::std::chrono::high_resolution_clock::now();
	auto tDiff = ::std::chrono::duration<double, ::std::milli>(tEnd - tStart).count();
	tStart = ::std::chrono::high_resolution_clock::now();
	double deltaTime = tDiff / 1000.0;

	::std::cout << deltaTime << ::std::endl;

	if (deltaTime > 1.0f)
		deltaTime = 0.1f;
	else if (deltaTime < 0.001f)
		deltaTime = 0.1f;

	deltaTime = 0.1;

	::framework::gpu::queue * command_queue;
	device->get_queue(0, 0, &command_queue);
	struct
	{
		::std::uint32_t num_primitives;
		float deltaTime;
	} pushConstants{ m_bvh->getLeavesCount(), deltaTime };

	//PipelineBarrier pipelineBarriers[2];
	//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//buffer_memory_barriers[0].pipelineBarrierCount = 1;
	//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].buffer = m_bvh->getRadixTreeBuffer();
	//buffer_memory_barriers[0].offset = 0;
	//buffer_memory_barriers[0].size = m_bvh->getRadixTreeBuffer()->GetBufferCreateInfo()->size;

	//buffer_memory_barriers[1].pipelineBarrierCount = 1;
	//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].buffer = m_bvh->getPrimitiveBuffer();
	//buffer_memory_barriers[1].offset = 0;
	//buffer_memory_barriers[1].size = m_bvh->getPrimitiveBuffer()->GetBufferCreateInfo()->size;

	//::framework::gpu::dependency_info dependency_info;
	//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
	//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	//dependency_info.image_memory_barrier_count = 0;
	//dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
	buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
	buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
	buffer_memory_barriers[0].src_queue_family_ownership;
	buffer_memory_barriers[0].dst_queue_family_ownership;
	buffer_memory_barriers[0].buffer = nullptr;
	buffer_memory_barriers[0].offset = 0;
	buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
	dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::buffer_copy buffer_copy[3];
	buffer_copy[0].src_offset = 0;
	buffer_copy[0].dst_offset = 0;
	buffer_copy[0].size = sizeof(::std::uint32_t) * 2 * neighbors_count;

	buffer_copy[1].src_offset = 0;
	buffer_copy[1].dst_offset = 0;
	buffer_copy[1].size = sizeof(::std::uint32_t) * m_bvh->getLeavesCount();

	buffer_copy[2].src_offset = 0;
	buffer_copy[2].dst_offset = 0;
	buffer_copy[2].size = sizeof(::std::uint32_t);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	m_pCollideCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//m_pCollideCommandBuffer->dependency_info(0, 1, &dependency_info);
	//m_pCollideCommandBuffer->pipeline_barrier(&dependency_info);
	m_pCollideCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pCollidePipeline);
	m_pCollideCommandBuffer->bind_descriptor_pool(this->descriptor_pool);
	//m_pCollideCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCollideDescriptorTables[m_currentDescriptorTable], 0, nullptr);
	m_pCollideCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pCollidePipelineLayout, 0, 1, &m_pCollideDescriptorSets[m_currentDescriptorTable], 0, nullptr);
	m_pCollideCommandBuffer->push_constants(m_pCollidePipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
	m_pCollideCommandBuffer->dispatch((m_bvh->getLeavesCount() + 128 - 1) / 128, 1, 1);
	//m_pCollideCommandBuffer->dependency_info(::framework::gpu::dependency_flags::legacy_bit, 1, &dependency_info);
	//m_pCollideCommandBuffer->copy_buffer(m_pNeighborsListBuffer, m_pReadbackBuffer, 1, &buffer_copy[0]);
	//m_pCollideCommandBuffer->copy_buffer(m_pNeighborsHeadListBuffer, m_pReadbackBuffer2, 1, &buffer_copy[1]);
	//m_pCollideCommandBuffer->copy_buffer(m_pNeighborsCounterBuffer, m_pReadbackBuffer1, 1, &buffer_copy[2]);
	//m_pCollideCommandBuffer->dependency_info(::framework::gpu::dependency_flags::legacy_bit, 1, &dependency_info);
	m_pCollideCommandBuffer->end_command_buffer();

	m_pIntegrateCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//m_pIntegrateCommandBuffer->dependency_info(0, 1, &dependency_info);
	//m_pIntegrateCommandBuffer->pipeline_barrier(&dependency_info);
	m_pIntegrateCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pIntegratePipeline);
	m_pIntegrateCommandBuffer->bind_descriptor_pool(this->descriptor_pool);
	//m_pIntegrateCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pIntegrateDescriptorTables[m_currentDescriptorTable], 0, nullptr);
	m_pIntegrateCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pIntegratePipelineLayout, 0, 1, &m_pIntegrateDescriptorSets[m_currentDescriptorTable], 0, nullptr);
	m_pIntegrateCommandBuffer->push_constants(m_pIntegratePipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
	m_pIntegrateCommandBuffer->dispatch((m_bvh->getLeavesCount() + 128 - 1) / 128, 1, 1);
	m_pIntegrateCommandBuffer->end_command_buffer();

	::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
	wait_semaphore_infos[0].semaphore = wait_semaphore;
	wait_semaphore_infos[0].value = 0;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = m_pCollideCompleteSemaphore;
	wait_semaphore_infos[1].value = 0;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[1].device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_infos[2];
	command_buffer_infos[0].command_buffer = m_pCollideCommandBuffer;
	command_buffer_infos[0].device_mask = 0;

	command_buffer_infos[1].command_buffer = m_pIntegrateCommandBuffer;
	command_buffer_infos[1].device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_infos[2];
	signal_semaphore_infos[0].semaphore = m_pCollideCompleteSemaphore;
	signal_semaphore_infos[0].value = 0;
	signal_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_infos[0].device_index = 0;

	signal_semaphore_infos[1].semaphore = signal_semaphore;
	signal_semaphore_infos[1].value = 0;
	signal_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_infos[1].device_index = 0;

	::framework::gpu::submit_info submit_infos[2];
	submit_infos[0].flags = ::framework::gpu::submit_flags::none;
	submit_infos[0].wait_semaphore_info_count = 1;
	submit_infos[0].wait_semaphore_infos = &wait_semaphore_infos[0];
	submit_infos[0].command_buffer_info_count = 1;
	submit_infos[0].command_buffer_infos = &command_buffer_infos[0];
	submit_infos[0].signal_semaphore_info_count = 1;
	submit_infos[0].signal_semaphore_infos = &signal_semaphore_infos[0];

	submit_infos[1].flags = ::framework::gpu::submit_flags::none;
	submit_infos[1].wait_semaphore_info_count = 1;
	submit_infos[1].wait_semaphore_infos = &wait_semaphore_infos[1];
	submit_infos[1].command_buffer_info_count = 1;
	submit_infos[1].command_buffer_infos = &command_buffer_infos[1];
	submit_infos[1].signal_semaphore_info_count = 1;
	submit_infos[1].signal_semaphore_infos = &signal_semaphore_infos[1];
	command_queue->submit(static_cast<::std::uint32_t>(::std::size(submit_infos)), submit_infos, nullptr);

	device->wait_idle();
	m_currentDescriptorTable = !m_currentDescriptorTable;

	//struct list_node
	//{
	//	::std::uint32_t next;
	//	::std::uint32_t value;
	//};
	//::std::vector<list_node> neighborsReadback(neighbors_count);
	//::std::vector<::std::uint32_t> heads(m_bvh->getLeavesCount());
	//{
	//	void * pMappedData;
	//	device->map_memory(m_pReadbackDeviceMemory, 0, sizeof(list_node) * neighbors_count, ::framework::gpu::memory_map_flags::none, &pMappedData);
	//	::std::memcpy(neighborsReadback.data(), pMappedData, sizeof(list_node) * neighbors_count);
	//	device->unmap_memory(m_pReadbackDeviceMemory);
	//}
	//{
	//	void * pMappedData;
	//	device->map_memory(m_pReadbackDeviceMemory2, 0, sizeof(::std::uint32_t) * m_bvh->getLeavesCount(), ::framework::gpu::memory_map_flags::none, &pMappedData);
	//	::std::memcpy(heads.data(), pMappedData, sizeof(::std::uint32_t) * m_bvh->getLeavesCount());
	//	device->unmap_memory(m_pReadbackDeviceMemory2);
	//}

	//::std::uint32_t neighbors_counter = 0;
	//for (size_t i = 0; i < heads.size(); i++)
	//{
	//	::std::uint32_t node = heads[i];
	//	while (node != -1)
	//	{
	//		node = neighborsReadback[node].next;
	//		neighbors_counter++;
	//	}
	//}

	//::std::uint32_t gpu_neighbors_counter;
	//{
	//	void * pMappedData;
	//	device->map_memory(m_pReadbackDeviceMemory1, 0, sizeof(::std::uint32_t), ::framework::gpu::memory_map_flags::none, &pMappedData);
	//	::std::memcpy(&gpu_neighbors_counter, pMappedData, sizeof(::std::uint32_t));
	//	device->unmap_memory(m_pReadbackDeviceMemory1);
	//	::std::cout << "post neighborsCount (gpu): " << gpu_neighbors_counter << ::std::endl;
	//}
	//::std::cout << "post neighborsCount (cpu): " << neighbors_counter << ::std::endl;
}