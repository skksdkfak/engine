#include "Renderer.h"
#include "gpu/utility.hpp"
#include <resource/resource_manager.hpp>
//#include <Camera.h>
//#include <components/CameraController.h>
//#include <CameraControllerSystem.h>
#include <iostream>
#include <iterator>

::framework::renderer_context::renderer_context() :
	width(1280),
	height(720)
{
}

::framework::renderer_context::~renderer_context()
{
}

void ::framework::renderer_context::initialize()
{
	//m_mainCamera = entity_manager::create<CEntity>();
	//auto cameraTransform = m_mainCamera.assign<CTransform>();
	//cameraTransform.get()->setTranslation(::glm::vec3(0.0f, 0.0f, 0.0f));
	//auto camera = m_mainCamera.assign<camera>();
	//camera->setZRange(0.01f, 1024.f);
	//camera->reverseZ(false);
	//m_mainCamera.assign<CameraController>(::glm::vec3(0.0f, 1.0f, 0.0f));

	::framework::platform::window_create_info window_create_info;
	window_create_info.x = 0;
	window_create_info.y = 0;
	window_create_info.width = this->width;
	window_create_info.height = this->height;
	window_create_info.border_width = 0;
	window_create_info.full_screen = false;
	::framework::engine_core::connection->create_window(&window_create_info, &this->window);

	int device_type_index;
	::std::cout << "Choose graphics API type:\n0 - Vulkan;\n1 - DirectX 12;\n";
	::std::cin >> device_type_index;
	::framework::gpu::api_type api_type;
	::std::vector<::framework::resource::shader_language> frontend_shader_language_preferences;
	::framework::resource::resource_manager_create_info resource_manager_create_info;
	switch (device_type_index)
	{
	case 0:
	default:
		api_type = ::framework::gpu::api_type::vulkan;
		frontend_shader_language_preferences =
		{
			::framework::resource::shader_language::glsl,
			::framework::resource::shader_language::hlsl
		};
		resource_manager_create_info.shader_language = ::framework::resource::shader_language::spirv_1_5;
		resource_manager_create_info.shader_environment = ::framework::resource::shader_environment::vulkan_1_3;
		::std::cout << "Vulkan api has been chosen" << ::std::endl << "Glslang shader compiler is using" << ::std::endl;
		break;
	case 1:
		api_type = ::framework::gpu::api_type::d3d12;
		frontend_shader_language_preferences =
		{
			::framework::resource::shader_language::glsl,
			::framework::resource::shader_language::hlsl
		};
		resource_manager_create_info.shader_language = ::framework::resource::shader_language::dxil;
		resource_manager_create_info.shader_environment = ::framework::resource::shader_environment::d3d12;
		::std::cout << "DirectX 12 api has been chosen" << ::std::endl << "Dxc shader compiler is using" << ::std::endl;
		break;
	}
	resource_manager_create_info.frontend_shader_language_preference_count = static_cast<::std::uint32_t>(frontend_shader_language_preferences.size());
	resource_manager_create_info.frontend_shader_language_preferences = frontend_shader_language_preferences.data();
	this->resource_manager = ::std::make_unique<::framework::resource::resource_manager>(resource_manager_create_info);

	resource_manager->add_search_directory("../");
	resource_manager->add_search_directory("../data/kernels/");
	resource_manager->add_search_directory("../data/models/");
	resource_manager->add_search_directory("../data/scenes/");
	resource_manager->add_search_directory("../data/sounds/");
	resource_manager->add_search_directory("../data/textures/");
	resource_manager->add_search_directory("../data/levels/");

	//camera->inverseY(deviceType == gapi_type::vulkan);

	::std::vector<const char *> enabled_layer_names =
	{
#ifdef _DEBUG
		::framework::gpu::layer_standard_validation_layer_name
#endif
	};

	::std::vector<const char *> enabled_extension_names =
	{
		::framework::gpu::surface_extension_name,
#if defined(_WIN32)
		::framework::gpu::win32_surface_extension_name,
#elif defined(__linux__)
		::framework::gpu::xcb_surface_extension_name,
#endif
#ifdef _DEBUG
		::framework::gpu::debug_utils_extension_name,
		//::framework::gpu::validation_features_extension_name
#endif
	};

	::framework::gpu::application_info application_info;
	application_info.application_name = "MyTestGame";
	application_info.application_version = 1;
	application_info.engine_name = "Engine";
	application_info.engine_version = 1;
	application_info.api_version = 1;

	::framework::gpu::instance_create_info instance_create_info;
	instance_create_info.application_info = &application_info;
	instance_create_info.enabled_layer_count = static_cast<::std::uint32_t>(enabled_layer_names.size());
	instance_create_info.enabled_layer_names = enabled_layer_names.data();
	instance_create_info.enabled_extension_count = static_cast<::std::uint32_t>(enabled_extension_names.size());
	instance_create_info.enabled_extension_names = enabled_extension_names.data();
	::framework::gpu::instance::create_instance(api_type, &instance_create_info, nullptr, &this->instance);

	::framework::gpu::surface_create_info surface_create_info;
	surface_create_info.window = this->window;
	instance->create_surface(&surface_create_info, &surface);

	instance->enumerate_physical_devices(&this->physical_device_count, nullptr);
	assert(this->physical_device_count > 0);

	this->physical_devices = new ::framework::gpu::physical_device * [this->physical_device_count];
	instance->enumerate_physical_devices(&this->physical_device_count, this->physical_devices);

	::std::uint32_t physical_device_index;
	if (auto found_physical_device = ::std::find_if(this->physical_devices, this->physical_devices + this->physical_device_count,
		[](::framework::gpu::physical_device * physical_device)
		{
			::framework::gpu::physical_device_properties properties;
			physical_device->get_properties(&properties);
			return properties.device_type == ::framework::gpu::physical_device_type::discrete_gpu;
		}); found_physical_device != this->physical_devices + this->physical_device_count)
	{
		physical_device_index = ::std::distance(this->physical_devices, found_physical_device);
	}
	else
	{
		physical_device_index = 0;
	}
	this->physical_device = this->physical_devices[physical_device_index];

	::std::uint32_t queue_family_property_count;
	this->physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	this->queue_family_properties.resize(queue_family_property_count);
	this->physical_device->get_queue_family_properties(&queue_family_property_count, this->queue_family_properties.data());

	// Iterate over each queue to learn whether it supports presenting:
	// Find a queue with present support
	// Will be used to present the swap chain images to the windowing system
	::std::vector<::framework::gpu::bool32_t> supportsPresent(queue_family_property_count);
	for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
	{
		this->physical_device->get_surface_support(i, surface, &supportsPresent[i]);
	}

	this->physical_device->get_memory_properties(&physical_device_memory_properties);

	const float defaultQueuePriority(0.0f);
	::std::vector<::framework::gpu::device_queue_create_info> device_queue_create_infos;

	this->queue_family_indices.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit);
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.graphics;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->queue_family_indices.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit);
	if (this->queue_family_indices.compute != this->queue_family_indices.graphics)
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.compute;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->queue_family_indices.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit);
	if ((this->queue_family_indices.transfer != this->queue_family_indices.graphics) && (this->queue_family_indices.transfer != this->queue_family_indices.compute))
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.transfer;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->physical_device->get_features(&this->physical_device_features);

	::framework::gpu::device_create_info device_create_info;
	device_create_info.device_idx = 0;
	device_create_info.queue_create_info_count = static_cast<::std::uint32_t>(device_queue_create_infos.size());
	device_create_info.queue_create_infos = device_queue_create_infos.data();
	device_create_info.enabled_features = &this->physical_device_features;

	assert_framework_gpu_result(this->physical_device->create_device(&device_create_info, nullptr, &this->device));

	device->get_queue(queue_family_indices.graphics, 0, &queue);

	::framework::gpu::surface_capabilities surface_capabilities;
	this->physical_device->get_surface_capabilities(surface, &surface_capabilities);

	this->width = surface_capabilities.current_extent.width;
	this->height = surface_capabilities.current_extent.height;

	createSwapChain();

	// DoublyLinkedList
	//if (false)
	//{
	//	::std::uint32_t const doublyLinkedListSize = 16777216;
	//	class Buffer * pDoublyLinkedListBuffer;
	//	class DeviceMemory * pDoublyLinkedListDeviceMemory;
	//	class Buffer * pDoublyLinkedListStagingBuffer;
	//	class DeviceMemory * pDoublyLinkedListStagingDeviceMemory;
	//	class Buffer * pReadbackBuffer;
	//	class DeviceMemory * pReadbackDeviceMemory;
	//	class DescriptorSetLayout * pDoublyLinkedListDescriptorSetLayout;
	//	class PipelineLayout * pDoublyLinkedListPipelineLayout;
	//	class Pipeline * pDoublyLinkedListPipeline;
	//	class DescriptorPool * pDoublyLinkedListDescriptorPool;
	//	class DescriptorSet * pDoublyLinkedListDescriptorSet;
	//	class DescriptorTable * pDoublyLinkedListDescriptorTable;
	//	class CommandPool * pDoublyLinkedListCommandPool;
	//	class CommandBuffer * pDoublyLinkedListCommandBuffer;

	//	struct DoublyLinkedListNode
	//	{
	//		::std::uint32_t prev;
	//		::std::uint32_t next;
	//		::std::uint32_t val;
	//	};

	//	{
	//		::framework::gpu::buffer_create_info buffer_create_info;
	//		buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
	//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//		buffer_create_info.queue_family_index_count = 0;
	//		buffer_create_info.queue_family_indices = nullptr;
	//		device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListStagingBuffer);

	//		::framework::gpu::memory_requirements memory_requirements;
	//		device->get_buffer_memory_requirements(pDoublyLinkedListStagingBuffer, &memory_requirements);

	//		::framework::gpu::memory_allocate_info memory_allocate_info;
	//		memory_allocate_info.allocation_size = memory_requirements.size;
	//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
	//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//		device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListStagingDeviceMemory);
	//		device->bind_buffer_memory(pDoublyLinkedListStagingBuffer, pDoublyLinkedListStagingDeviceMemory, 0);
	//	}
	//	{
	//		::framework::gpu::buffer_create_info buffer_create_info;
	//		buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
	//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//		buffer_create_info.queue_family_index_count = 0;
	//		buffer_create_info.queue_family_indices = nullptr;
	//		device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListBuffer);

	//		::framework::gpu::memory_requirements memory_requirements;
	//		device->get_buffer_memory_requirements(pDoublyLinkedListBuffer, &memory_requirements);

	//		::framework::gpu::memory_allocate_info memory_allocate_info;
	//		memory_allocate_info.allocation_size = memory_requirements.size;
	//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//		device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListDeviceMemory);
	//		device->bind_buffer_memory(pDoublyLinkedListBuffer, pDoublyLinkedListDeviceMemory, 0);
	//	}
	//	{
	//		::framework::gpu::buffer_create_info buffer_create_info;
	//		buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
	//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//		buffer_create_info.queue_family_index_count = 0;
	//		buffer_create_info.queue_family_indices = nullptr;
	//		device->create_buffer(&buffer_create_info, nullptr, &pReadbackBuffer);

	//		::framework::gpu::memory_requirements memory_requirements;
	//		device->get_buffer_memory_requirements(pReadbackBuffer, &memory_requirements);

	//		::framework::gpu::memory_allocate_info memory_allocate_info;
	//		memory_allocate_info.allocation_size = memory_requirements.size;
	//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
	//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//		device->allocate_memory(&memory_allocate_info, nullptr, &pReadbackDeviceMemory);
	//		device->bind_buffer_memory(pReadbackBuffer, pReadbackDeviceMemory, 0);
	//	}

	//	class ShaderModule const * pDoublyLinkedListShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "doubly_linked_list.comp");

	//	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	//	pipeline_shader_stage_create_info.module = pDoublyLinkedListShaderModule;
	//	pipeline_shader_stage_create_info.name = "main";
	//	pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

	//	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
	//	// DoublyLinkedList
	//	descriptor_set_layout_bindings[0].binding = 0;
	//	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	//	descriptor_set_layout_bindings[0].descriptor_count = 1;
	//	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	//	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;

	//	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	//	descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	//	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	//	device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &pDoublyLinkedListDescriptorSetLayout);

	//	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	//	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	//	pipeline_layout_create_info.descriptor_set_layouts = &pDoublyLinkedListDescriptorSetLayout;
	//	pipeline_layout_create_info.push_constant_range_count = 0;
	//	pipeline_layout_create_info.push_constant_ranges = nullptr;
	//	device->create_pipeline_layout(&pipeline_layout_create_info, &pDoublyLinkedListPipelineLayout);

	//	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	//	compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	//	compute_pipeline_create_info.pipeline_layout = pDoublyLinkedListPipelineLayout;
	//	device->create_compute_pipelines(&compute_pipeline_create_info, 1, &pDoublyLinkedListPipeline);

	//	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	//	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_buffer;
	//	descriptor_pool_sizes[0].descriptor_count = 1;
	//	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	//	descriptor_pool_create_info.max_sets = 1;
	//	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	//	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	//	device->create_descriptor_pool(&descriptor_pool_create_info, &pDoublyLinkedListDescriptorPool);

	//	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	//	descriptor_set_allocate_info.descriptor_set_count = 1;
	//	descriptor_set_allocate_info.descriptor_pool = pDoublyLinkedListDescriptorPool;
	//	descriptor_set_allocate_info.set_layouts = &pDoublyLinkedListDescriptorSetLayout;
	//	device->allocate_descriptor_sets(&descriptor_set_allocate_info, &pDoublyLinkedListDescriptorSet);

	//	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
	//	descriptor_buffer_infos[0].buffer = pDoublyLinkedListBuffer;
	//	descriptor_buffer_infos[0].offset = 0;
	//	descriptor_buffer_infos[0].range = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
	//	descriptor_buffer_infos[0].stride = sizeof(DoublyLinkedListNode);

	//	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	//	write_descriptor_sets[0].dst_set = pDoublyLinkedListDescriptorSet;
	//	write_descriptor_sets[0].dst_binding = 0;
	//	write_descriptor_sets[0].dst_array_element = 0;
	//	write_descriptor_sets[0].descriptor_count = 1;
	//	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	//	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	//	device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

	//	DescriptorTableCreateInfo descriptorTableCreateInfo;
	//	descriptorTableCreateInfo.pipeline_layout = pDoublyLinkedListPipelineLayout;
	//	device->CreateDescriptorTable(&descriptorTableCreateInfo, &pDoublyLinkedListDescriptorTable);
	//	device->UpdateDescriptorTable(pDoublyLinkedListDescriptorTable, 0, 1, &pDoublyLinkedListDescriptorSet);


	//	::framework::gpu::command_pool_create_info command_pool_create_info;
	//	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	//	command_pool_create_info.queue_family_index = 0;
	//	device->create_command_pool(&command_pool_create_info, &pDoublyLinkedListCommandPool);

	//	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	//	command_buffer_allocate_info.command_pool = pDoublyLinkedListCommandPool;
	//	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	//	command_buffer_allocate_info.command_buffer_count = 1;
	//	device->allocate_command_buffers(&command_buffer_allocate_info, &pDoublyLinkedListCommandBuffer);

	//	{
	//		::framework::gpu::buffer_copy buffer_copy{};
	//		buffer_copy.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;

	//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	//		buffer_memory_barriers[0].src_queue_family_index = 0;
	//		buffer_memory_barriers[0].dst_queue_family_index = 0;
	//		buffer_memory_barriers[0].buffer = pDoublyLinkedListBuffer;
	//		buffer_memory_barriers[0].offset = 0;
	//		buffer_memory_barriers[0].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
	//		buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	//		buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//		buffer_memory_barriers[1].src_queue_family_index = 0;
	//		buffer_memory_barriers[1].dst_queue_family_index = 0;
	//		buffer_memory_barriers[1].buffer = pDoublyLinkedListBuffer;
	//		buffer_memory_barriers[1].offset = 0;
	//		buffer_memory_barriers[1].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;

	//		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//		command_buffer_begin_info.flags = 0;
	//		command_buffer_begin_info.inheritance_info = nullptr;
	//		pDoublyLinkedListCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//		pDoublyLinkedListCommandBuffer->bind_pipeline(pDoublyLinkedListPipeline);
	//		pDoublyLinkedListCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, pDoublyLinkedListDescriptorTable, 0, nullptr);
	//		pDoublyLinkedListCommandBuffer->dispatch((doublyLinkedListSize + 128 - 1) / 128, 1, 1);
	//		pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_bind_point::compute, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
	//		pDoublyLinkedListCommandBuffer->copy_buffer(pDoublyLinkedListBuffer, pReadbackBuffer, 1, &buffer_copy);
	//		pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_bind_point::compute, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
	//		pDoublyLinkedListCommandBuffer->end_command_buffer();
	//	}

	//	device->CreateSemaphore(&m_pNeighborsCompleteSemaphore);

	//	if (false)
	//	{
	//		{
	//			::std::vector<DoublyLinkedListNode> doublyLinkedList(doublyLinkedListSize);
	//			for (::std::uint32_t i = 0; i < doublyLinkedListSize; i++)
	//			{
	//				doublyLinkedList[i].prev = i == 0 ? doublyLinkedListSize - 1 : i - 1;
	//				doublyLinkedList[i].next = i == doublyLinkedListSize - 1 ? 0 : i + 1;
	//			}
	//			void * data;
	//			device->map_memory(pDoublyLinkedListStagingDeviceMemory, 0, sizeof(DoublyLinkedListNode) * doublyLinkedListSize, 0, &data);
	//			::std::memcpy(data, doublyLinkedList.data(), sizeof(DoublyLinkedListNode) * doublyLinkedListSize);
	//			device->unmap_memory(pDoublyLinkedListStagingDeviceMemory);

	//			::framework::gpu::command_pool_create_info command_pool_create_info;
	//			command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	//			command_pool_create_info.queue_family_index = 0;
	//			class CommandPool * command_pool;
	//			device->create_command_pool(&command_pool_create_info, &command_pool);

	//			::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	//			command_buffer_allocate_info.command_pool = command_pool;
	//			command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	//			command_buffer_allocate_info.command_buffer_count = 1;
	//			class CommandBuffer * command_buffer;
	//			device->allocate_command_buffers(&command_buffer_allocate_info, &command_buffer);

	//			::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	//			command_buffer->begin_command_buffer(&command_buffer_begin_info);
	//			::framework::gpu::buffer_copy buffer_copy{};
	//			buffer_copy.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//			command_buffer->copy_buffer(pDoublyLinkedListStagingBuffer, pDoublyLinkedListBuffer, 1, &buffer_copy);
	//			command_buffer->end_command_buffer();

	//			::framework::gpu::submit_info submit_info{};
	//			submit_info.command_buffer_count = 1;
	//			submit_info.command_buffers = &command_buffer;

	//			class ::framework::gpu::fence * class ::framework::gpu::fence;
	//			::framework::gpu::fence_create_info fence_create_info;
	//			fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	//			device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

	//			queue->submit(1, &submit_info, class ::framework::gpu::fence);

	//			device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);

	//			device->free_command_buffers(command_pool, 1, &command_buffer);
	//			device->destroy_command_pool(command_pool);
	//			device->destroy_fence(class ::framework::gpu::fence);
	//		}

	//		::framework::gpu::submit_info submit_infos[1];
	//		submit_infos[0].command_buffer_count = 1;
	//		submit_infos[0].command_buffers = &pDoublyLinkedListCommandBuffer;
	//		submit_infos[0].wait_semaphore_count = 0;
	//		submit_infos[0].wait_semaphores = nullptr;
	//		submit_infos[0].wait_dst_stage_mask = nullptr;
	//		submit_infos[0].signal_semaphore_count = 0;
	//		submit_infos[0].signal_semaphores = nullptr;
	//		queue->submit(::std::size(submit_infos), submit_infos, nullptr);

	//		queue->wait_idle();

	//		::std::vector<DoublyLinkedListNode> doublyLinkedList(doublyLinkedListSize);
	//		void * pMappedData;
	//		device->map_memory(pReadbackDeviceMemory, 0, sizeof(DoublyLinkedListNode) * doublyLinkedListSize, 0, &pMappedData);
	//		::std::memcpy(doublyLinkedList.data(), pMappedData, sizeof(DoublyLinkedListNode) * doublyLinkedListSize);
	//		device->unmap_memory(pReadbackDeviceMemory);
	//		::std::uint32_t curr = 0;
	//		::std::uint32_t prev = doublyLinkedListSize - 1;
	//		::std::cout << "start>";
	//		while (curr != doublyLinkedListSize - 1)
	//		{
	//			//::std::cout << curr << ">";
	//			if (doublyLinkedList[curr].prev != prev)
	//			{
	//				::std::cerr << "assertion failed at " << curr << " !" << ::std::endl;
	//			}
	//			//assert(doublyLinkedList[curr].prev == prev);
	//			prev = curr;
	//			curr = doublyLinkedList[curr].next;
	//		}
	//		::std::cout << "end!" << ::std::endl;
	//	}
	//	device->destroy_buffer(pDoublyLinkedListStagingBuffer);
	//	device->free_memory(pDoublyLinkedListStagingDeviceMemory);
	//}

	::framework::LBVHCreateInfo lbvhCreateInfo;
	lbvhCreateInfo.physical_device = physical_device;
	lbvhCreateInfo.device = device;
	lbvhCreateInfo.maxPrimitives = 50000u;
	lbvhCreateInfo.resource_manager = resource_manager.get();
	::framework::lbvh::allocate(&lbvhCreateInfo, &m_bvh);

	::framework::NeighborsCreateInfo neighborsCreateInfo;
	neighborsCreateInfo.physical_device = physical_device;
	neighborsCreateInfo.device = device;
	neighborsCreateInfo.pBvh = m_bvh;
	neighborsCreateInfo.resource_manager = resource_manager.get();
	::framework::Neighbors::allocate(&neighborsCreateInfo, &m_neighbors);

	::framework::TracerCreateInfo tracerCreateInfo;
	tracerCreateInfo.physical_device = physical_device;
	tracerCreateInfo.device = device;
	tracerCreateInfo.pBvh = m_bvh;
	tracerCreateInfo.resource_manager = resource_manager.get();
	//tracerCreateInfo.camera = m_mainCamera.component<camera>().get();
	//tracerCreateInfo.cameraTransform = cameraTransform.get();
	::framework::Tracer::allocate(&tracerCreateInfo, &m_tracer);

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_pool_sizes[0].descriptor_count = 1;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
	descriptor_pool_create_info.max_sets = 1;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &descriptor_pool);

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
	descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
	descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pFullScreenQuadDescriptorSetLayout);

	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.descriptor_set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
	pipeline_layout_create_info.push_constant_range_count = 0;
	pipeline_layout_create_info.push_constant_ranges = nullptr;
	device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pFullScreenQuadPipelineLayout);

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pFullScreenQuadDescriptorSet);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_tracer->GetGetRayTracedSampler();
	descriptor_image_info.image_view = m_tracer->GetRayTracedImageView();
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = m_pFullScreenQuadDescriptorSet;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	write_descriptor_sets[0].image_info = &descriptor_image_info;
	device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

	{
		::framework::gpu::attachment_description attachment_descriptions[1];
		// Color attachment
		attachment_descriptions[0].format = ::framework::gpu::format::b8g8r8a8_unorm;
		attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::present_src_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::present_src_bit;

		::framework::gpu::attachment_reference color_reference[1];
		color_reference[0].attachment = 0;
		color_reference[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::subpass_dependency subpass_dependencies[2];
		subpass_dependencies[0].src_subpass = ::framework::gpu::subpass_external;
		subpass_dependencies[0].dst_subpass = 0;
		subpass_dependencies[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		subpass_dependencies[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		subpass_dependencies[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		subpass_dependencies[0].dst_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		subpass_dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		subpass_dependencies[1].src_subpass = 0;
		subpass_dependencies[1].dst_subpass = ::framework::gpu::subpass_external;
		subpass_dependencies[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		subpass_dependencies[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
		subpass_dependencies[1].src_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		subpass_dependencies[1].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		subpass_dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		::framework::gpu::subpass_description subpass_descriptions[1];
		subpass_descriptions[0].flags = ::framework::gpu::subpass_description_flags::none;
		subpass_descriptions[0].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
		subpass_descriptions[0].input_attachment_count = 0;
		subpass_descriptions[0].input_attachments = nullptr;
		subpass_descriptions[0].color_attachment_count = ::std::size(color_reference);
		subpass_descriptions[0].color_attachments = color_reference;
		subpass_descriptions[0].resolve_attachments = nullptr;
		subpass_descriptions[0].depth_stencil_attachment = nullptr;
		subpass_descriptions[0].preserve_attachment_count = 0;
		subpass_descriptions[0].preserve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
		render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
		render_pass_create_info.attachments = attachment_descriptions;
		render_pass_create_info.subpass_count = ::std::size(subpass_descriptions);
		render_pass_create_info.subpasses = subpass_descriptions;
		render_pass_create_info.dependency_count = ::std::size(subpass_dependencies);
		render_pass_create_info.dependencies = subpass_dependencies;
		device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass);

		::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
		render_pass_state_create_info.render_pass = this->render_pass;
		render_pass_state_create_info.max_clear_value_count = ::std::size(attachment_descriptions);
		assert_framework_gpu_result(this->device->create_render_pass_state(&render_pass_state_create_info, nullptr, &this->render_pass_state));
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/FullScreenQuad.vert";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/FullScreenQuad.vert.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pFullScreenQuadVertexShaderModule = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/FullScreenQuad.frag";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/FullScreenQuad.frag.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pFullScreenQuadFragmentShaderModule = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
	pipeline_shader_stage_create_infos[0].module = m_pFullScreenQuadVertexShaderModule;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	pipeline_shader_stage_create_infos[1].module = m_pFullScreenQuadFragmentShaderModule;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	::framework::gpu::viewport viewport{ 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
	::framework::gpu::rect_2d scissor{ { 0, 0 },{ width, height } };
	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::dynamic_state dynamicStates[] =
	{
		::framework::gpu::dynamic_state::viewport,
		::framework::gpu::dynamic_state::scissor
	};
	::framework::gpu::pipeline_dynamic_state_create_info pipelineDynamicStateCreateInfo;
	pipelineDynamicStateCreateInfo.dynamic_states = dynamicStates;
	pipelineDynamicStateCreateInfo.dynamic_state_count = static_cast<::std::uint32_t>(::std::size(dynamicStates));

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
	pipeline_color_blend_attachment_state.blend_enable = false;
	pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.color_write_mask =
		::framework::gpu::color_component_flags::r_bit |
		::framework::gpu::color_component_flags::g_bit |
		::framework::gpu::color_component_flags::b_bit |
		::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo;
	pipelineDepthStencilStateCreateInfo.depth_test_enable = false;
	pipelineDepthStencilStateCreateInfo.depth_write_enable = false;
	pipelineDepthStencilStateCreateInfo.depth_compare_op = ::framework::gpu::compare_op::never;
	pipelineDepthStencilStateCreateInfo.depth_bounds_test_enable = false;
	pipelineDepthStencilStateCreateInfo.stencil_test_enable = false;
	pipelineDepthStencilStateCreateInfo.front = {};
	pipelineDepthStencilStateCreateInfo.back = {};
	pipelineDepthStencilStateCreateInfo.back.compare_op = ::framework::gpu::compare_op::always;
	pipelineDepthStencilStateCreateInfo.min_depth_bounds = 0.0f;
	pipelineDepthStencilStateCreateInfo.max_depth_bounds = 0.0f;

	::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
	vertex_input_state.vertex_binding_description_count = 0;
	vertex_input_state.vertex_binding_descriptions = nullptr;
	vertex_input_state.vertex_attribute_description_count = 0;
	vertex_input_state.vertex_attribute_descriptions = nullptr;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::pipeline_multisample_state_create_info pipeline_multisample_state_create_info;
	pipeline_multisample_state_create_info.flags = ::framework::gpu::pipeline_multisample_state_create_flags::none;
	pipeline_multisample_state_create_info.rasterization_samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	pipeline_multisample_state_create_info.sample_shading_enable = false;
	pipeline_multisample_state_create_info.min_sample_shading = 0.0f;
	pipeline_multisample_state_create_info.sample_mask = nullptr;
	pipeline_multisample_state_create_info.alpha_to_coverage_enable = false;
	pipeline_multisample_state_create_info.alpha_to_one_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = &pipeline_multisample_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = &pipelineDynamicStateCreateInfo;
	graphics_pipeline_create_info.layout = m_pFullScreenQuadPipelineLayout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 0;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	device->create_graphics_pipelines(nullptr, 1, &graphics_pipeline_create_info, nullptr, &m_pFullScreenQuadGraphicsPipeline);

	setupDepthStencil();
	setupFrameBuffer();

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = queue_family_indices.graphics;
	device->create_command_pool(&command_pool_create_info, nullptr, &this->command_pool);

	createCommandBuffers();

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::none/*::framework::gpu::fence_create_flags::signaled_bit*/;
	device->create_fence(&fence_create_info, nullptr, &fence[0]);
	device->create_fence(&fence_create_info, nullptr, &fence[1]);
	device->create_fence(&fence_create_info, nullptr, &fence[2]);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	device->create_semaphore(&semaphore_create_info, nullptr, &this->render_complete_semaphores[0]);
	device->create_semaphore(&semaphore_create_info, nullptr, &this->render_complete_semaphores[1]);
	device->create_semaphore(&semaphore_create_info, nullptr, &this->render_complete_semaphores[2]);
	device->create_semaphore(&semaphore_create_info, nullptr, &present_complete_semaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pPathTracingCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pNeighborsCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pIntegrateCompleteSemaphore);
}

void ::framework::renderer_context::update(float deltaT)
{
	::framework::gpu::pipeline_stage_flags waitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	class ::framework::gpu::semaphore * pBvhBuildCompletionSemaphore = m_bvh->getBuildCompletionSemaphore();
	m_bvh->update();
	m_tracer->trace(pBvhBuildCompletionSemaphore, waitDstStageMask, m_pPathTracingCompleteSemaphore, deltaT);
	m_neighbors->execute(m_pPathTracingCompleteSemaphore, waitDstStageMask, m_pNeighborsCompleteSemaphore);
	m_neighbors->integrate(m_pNeighborsCompleteSemaphore, waitDstStageMask, m_pIntegrateCompleteSemaphore);

	::framework::gpu::acquire_next_image_info acquire_next_image_info;
	acquire_next_image_info.swap_chain = swap_chain;
	acquire_next_image_info.fence = nullptr;
	acquire_next_image_info.semaphore = present_complete_semaphore;
	acquire_next_image_info.timeout = UINT64_MAX;
	acquire_next_image_info.device_mask = 0x1;
	device->acquire_next_image(&acquire_next_image_info, &this->swap_chain_image);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };

	::framework::gpu::clear_value clear_values[1];
	clear_values[0].color.float32[0] = 0.0f;
	clear_values[0].color.float32[1] = 0.6f;
	clear_values[0].color.float32[2] = 0.1f;
	clear_values[0].color.float32[3] = 1.0f;

	::framework::gpu::render_pass_state_info render_pass_state_info;
	render_pass_state_info.frame_buffer = this->frame_buffers[this->swap_chain_image];
	render_pass_state_info.render_area.offset.x = 0;
	render_pass_state_info.render_area.offset.y = 0;
	render_pass_state_info.render_area.extent.width = this->width;
	render_pass_state_info.render_area.extent.height = this->height;
	render_pass_state_info.clear_value_count = static_cast<::std::uint32_t>(::std::size(clear_values));
	render_pass_state_info.clear_values = clear_values;
	this->render_pass_state->set_state(render_pass_state_info);

	::framework::gpu::render_pass_begin_info render_pass_begin_info;
	render_pass_begin_info.render_pass_state = this->render_pass_state;
	//::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	//renderPassBeginInfo.frame_buffer = this->frame_buffers[this->swap_chain_image];
	//renderPassBeginInfo.render_pass = m_pFullScreenQuadRenderPass;
	//renderPassBeginInfo.render_area = render_area;
	//renderPassBeginInfo.clear_value_count = ::std::size(clear_values);
	//renderPassBeginInfo.clear_values = clear_values;

	//PipelineBarrier pipelineBarrier;
	//pipelineBarrier.src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarrier.dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//pipelineBarrier.src_access_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarrier.dst_access_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;

	//::framework::gpu::image_memory_barrier image_memory_barrier;
	//image_memory_barrier.pipelineBarrierCount = 1;
	//image_memory_barrier.pPipelineBarriers = &pipelineBarrier;
	//image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//image_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//image_memory_barrier.image = m_tracer->GetRayTracedImage();
	//image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

	//::framework::gpu::dependency_info dependency_info;
	//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	//dependency_info.buffer_memory_barrier_count = 0;
	//dependency_info.buffer_memory_barriers = nullptr;
	//dependency_info.image_memory_barrier_count = 1;
	//dependency_info.image_memory_barriers = &image_memory_barrier;

	::framework::gpu::viewport viewport;
	viewport.x = 0;
	viewport.y = 0;
	viewport.width = (float)width;
	viewport.height = (float)height;
	viewport.min_depth = (float)0.0f;
	viewport.max_depth = (float)1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = width;
	scissor.extent.height = height;

	this->command_buffers[this->swap_chain_image]->begin_command_buffer(&command_buffer_begin_info);
	//this->command_buffers[this->swap_chain_image]->dependency_info(0, 1, &dependency_info);
	this->command_buffers[this->swap_chain_image]->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, m_pFullScreenQuadGraphicsPipeline);
	this->command_buffers[this->swap_chain_image]->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::inline_);
	this->command_buffers[this->swap_chain_image]->set_viewport(0, 1, &viewport);
	this->command_buffers[this->swap_chain_image]->set_scissor(0, 1, &scissor);
	//this->command_buffers[this->swap_chain_image]->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pFullScreenQuadDescriptorTable, 0, nullptr);
	this->command_buffers[this->swap_chain_image]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->m_pFullScreenQuadPipelineLayout, 0, 1, &m_pFullScreenQuadDescriptorSet, 0, nullptr);
	this->command_buffers[this->swap_chain_image]->draw(3, 1, 0, 0);
	this->command_buffers[this->swap_chain_image]->end_render_pass();
	this->command_buffers[this->swap_chain_image]->end_command_buffer();

	::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
	wait_semaphore_infos[0].semaphore = present_complete_semaphore;
	wait_semaphore_infos[0].value = 0;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = m_pIntegrateCompleteSemaphore;
	wait_semaphore_infos[1].value = 0;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	wait_semaphore_infos[1].device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = this->command_buffers[this->swap_chain_image];
	command_buffer_info.device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->render_complete_semaphores[this->swap_chain_image];
	signal_semaphore_info.value = 0;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_info.device_index = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 2;
	submit_info.wait_semaphore_infos = wait_semaphore_infos;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = 1;
	submit_info.signal_semaphore_infos = &signal_semaphore_info;
	queue->submit(1, &submit_info, fence[this->swap_chain_image]);

	device->wait_for_fences(1, &fence[this->swap_chain_image], true, UINT64_MAX);
	device->reset_fences(1, &fence[this->swap_chain_image]);

	::framework::gpu::present_info presentInfo;
	presentInfo.wait_semaphore_count = 1;
	presentInfo.wait_semaphores = &this->render_complete_semaphores[this->swap_chain_image];
	presentInfo.swap_chain_count = 1;
	presentInfo.swap_chains = &swap_chain;
	presentInfo.image_indices = &this->swap_chain_image;
	presentInfo.results = nullptr;
	queue->present(&presentInfo);
}

void ::framework::renderer_context::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->width = width;
	this->height = height;

	device->wait_idle();

	createSwapChain();

	device->destroy_image_view(this->depth_stencil_image_view, nullptr);
	device->destroy_image(this->depth_stencil_image, nullptr);
	device->free_memory(this->depth_stencil_device_memory, nullptr);

	setupDepthStencil();

	::std::uint32_t swap_chain_image_count;
	device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);
	for (::std::uint32_t i = 0; i < swap_chain_image_count; ++i)
	{
		device->destroy_frame_buffer(this->frame_buffers[i], nullptr);
	}
	setupFrameBuffer();
	destroyCommandBuffers();
	createCommandBuffers();

	m_tracer->resize(width, height);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_tracer->GetGetRayTracedSampler();
	descriptor_image_info.image_view = m_tracer->GetRayTracedImageView();
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;

	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = m_pFullScreenQuadDescriptorSet;
	write_descriptor_set.dst_binding = 0;
	write_descriptor_set.dst_array_element = 0;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	write_descriptor_set.image_info = &descriptor_image_info;
	device->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);

	//m_mainCamera.component<camera>()->setAspectRatio((float)width / (float)height);
}

::std::uint32_t(::framework::renderer_context::get_width)()
{
	return width;
}

::std::uint32_t(::framework::renderer_context::get_height)()
{
	return height;
}

class ::framework::gpu::device * ::framework::renderer_context::get_device()
{
	return device;
}

void ::framework::renderer_context::createSwapChain()
{
	class ::framework::gpu::swap_chain * old_swap_chain = swap_chain;

	::framework::gpu::swap_chain_create_info swap_chain_create_info;
	swap_chain_create_info.flags = ::framework::gpu::swap_chain_create_flags::none;
	swap_chain_create_info.surface = surface;
	swap_chain_create_info.min_image_count = 3;
	swap_chain_create_info.image_format = ::framework::gpu::format::b8g8r8a8_unorm;
	swap_chain_create_info.image_color_space = ::framework::gpu::color_space::srgb_nonlinear;
	swap_chain_create_info.image_extent = { width, height };
	swap_chain_create_info.image_array_layers = 1;
	swap_chain_create_info.image_usage = ::framework::gpu::image_usage_flags::color_attachment_bit;
	swap_chain_create_info.image_sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	swap_chain_create_info.queue_family_index_count = 0;
	swap_chain_create_info.queue_family_indices = nullptr;
	swap_chain_create_info.pre_transform = ::framework::gpu::surface_transform_flags::identity_bit;
	swap_chain_create_info.composite_alpha = ::framework::gpu::composite_alpha_flags::opaque_bit;
	swap_chain_create_info.present_mode = ::framework::gpu::present_mode::immediate;
	swap_chain_create_info.clipped = false;
	swap_chain_create_info.old_swap_chain = old_swap_chain;
	device->create_swap_chain(&swap_chain_create_info, nullptr, &swap_chain);

	if (old_swap_chain)
	{
		for (::std::size_t i = 0; i < swap_chain_images.size(); i++)
		{
			device->destroy_image_view(this->image_views[i], nullptr);
		}
		device->destroy_swap_chain(old_swap_chain, nullptr);
	}

	::std::uint32_t swap_chain_image_count;
	device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);

	swap_chain_images.resize(swap_chain_image_count);
	device->get_swap_chain_images(swap_chain, &swap_chain_image_count, swap_chain_images.data());
}

void ::framework::renderer_context::setupDepthStencil()
{
	//::framework::gpu::image_create_info image_create_info;
	//image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	//image_create_info.format = ::framework::gpu::format::d32_sfloat;
	//image_create_info.extent = { width, height, 1 };
	//image_create_info.mip_levels = 1;
	//image_create_info.array_layers = 1;
	//image_create_info.samples = sample_count_1_bit;
	//image_create_info.tiling = optimal;
	//image_create_info.usage = depth_stencil_attachment_bit | transfer_src_bit;
	//image_create_info.sharing_mode = SHARING_MODE_EXCLUSIVE;
	//image_create_info.queue_family_index_count = 0;
	//image_create_info.queue_family_indices = nullptr;
	//image_create_info.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	//device->create_image(&image_create_info, nullptr, &this->depth_stencil_image);

	//::framework::gpu::memory_requirements memory_requirements;
	//device->get_image_memory_requirements(this->depth_stencil_image, &memory_requirements);

	//::framework::gpu::memory_allocate_info memory_allocate_info;
	//memory_allocate_info.allocation_size = memory_requirements.size;
	//memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	//memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//device->allocate_memory(&memory_allocate_info, nullptr, &this->depth_stencil_device_memory);
	//device->bind_image_memory(this->depth_stencil_image, this->depth_stencil_device_memory, 0);

	//::framework::gpu::image_view_create_info image_view_create_info;
	//image_view_create_info.image = this->depth_stencil_image;
	//image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	//image_view_create_info.format = this->depth_stencil_image->GetImageCreateInfo()->format;
	//image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::depth_bit;
	//image_view_create_info.subresource_range.base_mip_level = 0;
	//image_view_create_info.subresource_range.level_count = 1;
	//image_view_create_info.subresource_range.base_array_layer = 0;
	//image_view_create_info.subresource_range.layer_count = 1;
	//device->create_image_view(&image_view_create_info, nullptr, &this->depth_stencil_image_view);
}

void ::framework::renderer_context::setupFrameBuffer()
{
	::std::uint32_t swap_chain_image_count;
	device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);

	this->image_views.resize(swap_chain_image_count);
	this->frame_buffers.resize(swap_chain_image_count);
	for (::std::uint32_t i = 0; i < swap_chain_image_count; i++)
	{
		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.image = swap_chain_images[i];
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::b8g8r8a8_unorm;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		device->create_image_view(&image_view_create_info, nullptr, &this->image_views[i]);

		class ::framework::gpu::attachment_view_and_descriptor attachment_view_and_descriptors[1];
		attachment_view_and_descriptors[0] = { this->image_views[i] };

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
		frame_buffer_create_info.render_pass = this->render_pass;
		frame_buffer_create_info.attachment_count = ::std::size(attachment_view_and_descriptors);
		frame_buffer_create_info.attachments = attachment_view_and_descriptors;
		frame_buffer_create_info.width = width;
		frame_buffer_create_info.height = height;
		frame_buffer_create_info.layers = 1;
		device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffers[i]);
	}
}

void ::framework::renderer_context::createCommandBuffers()
{
	::std::uint32_t swap_chain_image_count;
	device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);
	this->command_buffers.resize(swap_chain_image_count);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = swap_chain_image_count;
	device->allocate_command_buffers(&command_buffer_allocate_info, this->command_buffers.data());
}

void ::framework::renderer_context::destroyCommandBuffers()
{
	device->free_command_buffers(this->command_pool, this->command_buffers.size(), this->command_buffers.data());
}