#pragma once

#include "concurrency/thread_pool.h"
#include "algorithm/radix_sort.h"
#include "algorithm/hybrid_radix_sort.hpp"
#include <glm/glm.hpp>

namespace framework
{
	struct LBVHCreateInfo
	{
		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		::std::uint32_t maxPrimitives;
		typename ::framework::resource::resource_manager * resource_manager;
	};

	class lbvh
	{
	public:
		struct Bound
		{
			::glm::vec3 min;
			::glm::float32 pad0;
			::glm::vec3 max;
			::glm::float32 pad1;
		};

		struct CompactTreeNode
		{
			::glm::uvec4 field0; // parent, left, right, triangleID
			::glm::vec4 field1; // boundMin.xyz, cost
			::glm::vec4 field2; // boundMax.xyz, area
		};

		struct CompactSphere
		{
			::glm::vec4	field0; // pos.xyz, r
			::glm::vec4	field1; // color.rgba
		};

		struct TransformBufferData
		{
			::glm::mat4	mMVPx;
			::glm::mat4	mMVPy;
			::glm::mat4	mMVPz;
			::glm::mat4	mObject2World;
			::glm::mat4	mWorld2Object;
			::glm::mat4	mModelView;
			::glm::mat4	mModelViewProjection;
			::glm::mat4	mTransposeModelView;
			::glm::mat4	mInverseTransposeModelView;
		};

		static void allocate(LBVHCreateInfo const * create_info, lbvh ** ppLBVH);

		static void deallocate(lbvh * pLBVH);

		void performTriangleListFillPass();

		void buildBVH();

		void update();

		::std::uint32_t getMaxLeavesCount() const;

		::std::uint32_t getLeavesCount() const;

		class ::framework::gpu::buffer * getRadixTreeBuffer();

		class ::framework::gpu::buffer * getPrimitiveBuffer();

		class ::framework::gpu::semaphore * getBuildCompletionSemaphore();

		class ::framework::gpu::buffer * getNodeBuffer()
		{
			return this->m_pNodesBuffer;
		}

		class ::framework::gpu::buffer_view * getNodeBufferView()
		{
			return this->m_pNodesBufferView;
		}

	private:
		struct thread_data
		{
			struct CommandBufferData
			{
				bool is_command_buffer_open;
				::std::uint32_t drawCallCount;
			};

			class ::framework::gpu::command_pool * command_pool;
			::std::vector<class ::framework::gpu::command_buffer *> commandBuffers;
			::std::vector<CommandBufferData> commandBufferData;
		};
		lbvh(LBVHCreateInfo const * create_info);
		~lbvh();

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;

		void allocate();

		void deallocate();

		void printBVH(::std::uint32_t root, CompactTreeNode * nodes);

		bool check_bound(CompactTreeNode * p, CompactTreeNode * l, CompactTreeNode * r);

		bool check_sanity(::std::uint32_t n, CompactTreeNode * nodes);

		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		class ::framework::gpu::shader_module * m_pTriangleListFillVertexShaderModule;
		class ::framework::gpu::shader_module * m_pTriangleListFillGeometryShaderModule;
		class ::framework::gpu::shader_module * m_pTriangleListFillFragmentShaderModule;
		class ::framework::gpu::shader_module * m_pGlobalBoundComputingShader;
		class ::framework::gpu::shader_module * m_pReductionGlobalBoundComputingShader;
		class ::framework::gpu::shader_module * m_pGenerateMortonCodesShader;
		class ::framework::gpu::shader_module * m_pConstructRadixTreeShader;
		class ::framework::gpu::shader_module * m_pCalculateNodeBoundingBoxesShader;
		class ::framework::gpu::shader_module * m_pCreateNodesShader;
		class ::framework::gpu::buffer * m_pRadixTreeBuffer;
		class ::framework::gpu::device_memory * m_pRadixTreeDeviceMemory;
		class ::framework::gpu::buffer * m_pRadixTreeReadbackBuffer;
		class ::framework::gpu::device_memory * m_pRadixTreeReadbackDeviceMemory;
		class ::framework::gpu::buffer * m_pCounterBuffer;
		class ::framework::gpu::device_memory * m_pCounterDeviceMemory;
		class ::framework::gpu::buffer * m_pBoundsBuffer;
		class ::framework::gpu::device_memory * m_pBoundsDeviceMemory;
		class ::framework::gpu::buffer * m_pPrimitivesBuffer;
		class ::framework::gpu::device_memory * m_pPrimitivesDeviceMemory;
		class ::framework::gpu::buffer * m_pPrimitivesReadbackBuffer;
		class ::framework::gpu::device_memory * m_pPrimitivesReadbackDeviceMemory;
		class ::framework::gpu::buffer * m_pPrimitivesCounterBuffer;
		class ::framework::gpu::device_memory * m_pPrimitivesCounterDeviceMemory;
		class ::framework::gpu::buffer * m_pPrimitiveIndicesBuffer;
		class ::framework::gpu::device_memory * m_pPrimitiveIndicesDeviceMemory;
		class ::framework::gpu::buffer * m_pPrimitiveIndicesUploadBuffer;
		class ::framework::gpu::device_memory * m_pPrimitiveIndicesUploadDeviceMemory;
		class ::framework::gpu::buffer * m_pMortonCodesBuffer;
		class ::framework::gpu::device_memory * m_pMortonCodesDeviceMemory;
		class ::framework::gpu::buffer * m_pReadbackBuffer;
		class ::framework::gpu::device_memory * m_pReadbackDeviceMemory;
		class ::framework::gpu::buffer * m_pTriangleListFillUboBuffer;
		class ::framework::gpu::device_memory * m_pTriangleListFillUboDeviceMemory;
		class ::framework::gpu::buffer * m_pNodesBuffer;
		class ::framework::gpu::device_memory * m_pNodesDeviceMemory;
		class ::framework::gpu::buffer_view * m_pNodesBufferView;
		void * m_pTriangleListFillUboMappedData;
		class ::framework::gpu::command_pool * primary_command_pool;
		class ::framework::gpu::command_buffer * m_pPrimaryCommandBuffer;
		class ::framework::gpu::command_buffer * m_pPrimitiveCounterServicingCommandBuffer;
		class ::framework::gpu::command_buffer * m_pGlobalBoundComputingCommandBuffer;
		class ::framework::gpu::command_buffer * m_pGenerateMortonCodesCommandBuffer;
		class ::framework::gpu::command_buffer * m_pConstructRadixTreeCommandBuffer;
		class ::framework::gpu::command_buffer * m_pCalculateNodeBoundingBoxesCommandBuffer;
		class ::framework::gpu::command_buffer * m_pCreateNodesCommandBuffer;
		class ::framework::gpu::semaphore * m_pTriangleListFillCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pGlobalBoundComputingCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pGenerateMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pSortMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pConstructRadixTreeCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		class ::framework::gpu::semaphore * m_pCreateNodesCompleteSemaphore;
		class ::framework::gpu::frame_buffer * frame_buffer;
		class ::framework::gpu::render_pass * render_pass;
		class ::framework::gpu::pipeline_layout * m_pTriangleListFillPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pReductionGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pGenerateMortonCodesPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pConstructRadixTreePipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pCalculateNodeBoundingBoxesPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pCreateNodesPipelineLayout;
		class ::framework::gpu::pipeline * m_pTriangleListFillPipeline;
		class ::framework::gpu::pipeline * m_pGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline * m_pReductionGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline * m_pGenerateMortonCodesPipeline;
		class ::framework::gpu::pipeline * m_pConstructRadixTreePipeline;
		class ::framework::gpu::pipeline * m_pCalculateNodeBoundingBoxesPipeline;
		class ::framework::gpu::pipeline * m_pCreateNodesPipeline;
		class ::framework::gpu::descriptor_pool * descriptor_pool;
		class ::framework::gpu::descriptor_set_layout * m_pTriangleListFillDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pReductionGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pGenerateMortonCodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pConstructRadixTreeDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pCreateNodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set * m_pTriangleListFillDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pReductionGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pGenerateMortonCodesDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pConstructRadixTreeDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pCalculateNodeBoundingBoxesDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pCreateNodesDescriptorSet;
		::framework::algorithm::lsd_radix_sort::Data * m_pRadixSortData;
		::framework::algorithm::hybrid_radix_sort * pRadixSortData;
		::std::uint32_t thread_count;
		::std::vector<thread_data> thread_data;
		::framework::concurrency::job_system thread_pool;
		class ::framework::gpu::fence * m_pPrimitiveCounterServicingCompleteFence;
		const ::std::uint64_t m_maxUBOs;
		const ::std::uint64_t m_maxPrimitives;
		::std::uint32_t m_numTriangles;
		typename ::framework::resource::resource_manager * resource_manager;
	};
}