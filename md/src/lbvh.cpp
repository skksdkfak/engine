#include "lbvh.hpp"
//#include "components/Mesh.h"
#include "resource/resource_manager.hpp"
#include "gpu/core.hpp"
#include "gpu/utility.hpp"
//#include "RadixSort.h"
//#include "EntityManager.h"
//#include "components/Transform.h"
//#include "components/MeshRendererPathTracingBVH.h"
//#include "Readbackbuffer.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#include <glm/gtc/matrix_transform.hpp>
#include <forward_list>
#include <iostream>
#include <algorithm>
#include <future>
#include <algorithm>

::std::uint32_t width = 2048, height = 2048;

class BVHNodeQueue // as a doubly-linked list
{
private:
	typedef struct Node {
		struct Node * prev;
		struct Node * next;
		void * v;
	} Node;

	Node * head;
	Node * tail;
public:

	BVHNodeQueue() {
		head = NULL;
		tail = NULL;
	}

	void push(void * v) {
		Node * n = new Node();
		n->v = v;
		if (head == NULL) {
			n->prev = NULL;
			n->next = NULL;
			head = n;
			tail = n;
		}
		else {
			n->prev = NULL;
			n->next = head;
			if (head != NULL) {
				head->prev = n;
			}
			head = n;
		}
	}

	void pop() {
		if (head != NULL) {
			if (head == tail) {
				delete head;
				head = NULL;
				tail = NULL;
			}
			else {
				Node * subtail = tail->prev;
				delete tail;
				subtail->next = NULL;
				tail = subtail;
			}
		}
	}

	void * last()
	{
		if (tail != NULL) {
			return tail->v;
		}
		else {
			return NULL;
		}
	}

	bool empty()
	{
		return head == NULL;
	}
};

bool ::framework::lbvh::check_sanity(::std::uint32_t n, ::framework::lbvh::CompactTreeNode * nodes)
{
	if (nodes[n].field0.w != 0xFFFFFFFF) {
		return true;
	}
	else {
		return (
			nodes[nodes[n].field0.y].field0.x == n &&
			nodes[nodes[n].field0.z].field0.x == n
			);
	}
}

bool ::framework::lbvh::check_bound(::framework::lbvh::CompactTreeNode * p, ::framework::lbvh::CompactTreeNode * l, ::framework::lbvh::CompactTreeNode * r)
{
	return (
		p->field1.x == ::std::min(l->field1.x, r->field1.x) &&
		p->field2.x == ::std::max(l->field2.x, r->field2.x) &&
		p->field1.y == ::std::min(l->field1.y, r->field1.y) &&
		p->field2.y == ::std::max(l->field2.y, r->field2.y) &&
		p->field1.z == ::std::min(l->field1.z, r->field1.z) &&
		p->field2.z == ::std::max(l->field2.z, r->field2.z)
		);
}

void ::framework::lbvh::printBVH(::std::uint32_t root, ::framework::lbvh::CompactTreeNode * nodes)
{
	if (0)
	{
		::std::uint32_t stack[64];
		stack[0] = 0xFFFFFFFF;
		int ptr = 1;
		::std::uint32_t idx = 0;
		int test = 0;
		while (ptr != 0)
		{
			auto & node = nodes[idx];
			if (idx < 2)
			{
				idx = node.field0.y;
				stack[ptr++] = node.field0.z;
				continue;
			}
			else
			{
				::std::uint32_t primitiveID = node.field0.w;
				test |= (1 << primitiveID);
				printf("%d\n", primitiveID);
			}
			idx = stack[--ptr];
		}

		printf("%d\n", test);
	}

	int level = 1;
	BVHNodeQueue * q = new BVHNodeQueue();
	q->push((void *)&root);

	BVHNodeQueue * qt = new BVHNodeQueue();

	while (!q->empty())
	{
		//printf("\n######### Level %d ##########\n", level++);
		while (!q->empty())
		{
			::std::uint32_t n = *(::std::uint32_t *)(q->last());
			q->pop();
			//printf("(%d %d) %d", nodes[n].field0.y, nodes[n].field0.z, n);

			if (!check_sanity(n, nodes))
			{
				printf(" !SanityError! ");
			}

			if (nodes[n].field0.w == 0xFFFFFFFF)
			{
				auto parent = &nodes[n];
				auto left = &nodes[nodes[n].field0.y];
				auto right = &nodes[nodes[n].field0.z];
				if (!check_bound(parent, left, right))
				{
					printf(" !BoundError!");
				}
				//printf("\n");
				qt->push((void *)&nodes[n].field0.y);
				qt->push((void *)&nodes[n].field0.z);
			}
			else
			{
				//printf(" ((A:%.0lf C:%.0lf) PrimitiveID: %d)\n", nodes[n].field2.w, nodes[n].field1.w, nodes[n].field0.w);
			}
		}
		//printf("\n");

		BVHNodeQueue * t = q;
		q = qt;
		qt = t;
	}

	//printf("\n");

	delete q;
	delete qt;
}

int sumArithmeticSequence(int numberOfElements, int firstElement, int lastElement)
{
	return numberOfElements * (firstElement + lastElement) / 2;
}

::framework::lbvh::lbvh(::framework::LBVHCreateInfo const * create_info) :
	physical_device(create_info->physical_device),
	device(create_info->device),
	m_maxPrimitives(create_info->maxPrimitives),
	resource_manager(create_info->resource_manager),
	m_pRadixSortData(nullptr),
	m_maxUBOs(1024),
	m_numTriangles()
{
}

::framework::lbvh::~lbvh()
{
}

void ::framework::lbvh::allocate()
{
	::framework::gpu::physical_device_memory_properties memoryProperties;
	this->physical_device->get_memory_properties(&memoryProperties);
	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		device->create_command_pool(&command_pool_create_info, nullptr, &this->primary_command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = this->primary_command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = 1;
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPrimaryCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPrimitiveCounterServicingCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pGlobalBoundComputingCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pGenerateMortonCodesCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pConstructRadixTreeCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCalculateNodeBoundingBoxesCommandBuffer);
		device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCreateNodesCommandBuffer);
	}

	thread_count = ::std::thread::hardware_concurrency();
	thread_pool.set_thread_count(thread_count);
	thread_data.resize(thread_count);

	for (::std::uint32_t i = 0; i < thread_count; i++)
	{
		struct ::framework::lbvh::thread_data * thread = &thread_data[i];

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		device->create_command_pool(&command_pool_create_info, nullptr, &thread->command_pool);

		// One secondary command class Buffer per object that is updated by this thread
		thread->commandBufferData.resize(1);
		thread->commandBuffers.resize(1);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = thread->command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::secondary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(thread->commandBufferData.size());
		device->allocate_command_buffers(&command_buffer_allocate_info, thread->commandBuffers.data());
	}

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	device->create_fence(&fence_create_info, nullptr, &m_pPrimitiveCounterServicingCompleteFence);

	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(TransformBufferData) / uboAlignment) * uboAlignment + ((sizeof(TransformBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = dynamicAlignment * m_maxUBOs;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriangleListFillUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pTriangleListFillUboBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pTriangleListFillUboBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriangleListFillUboDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pTriangleListFillUboBuffer;
		bind_buffer_memory_info.memory = m_pTriangleListFillUboDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);

		device->map_memory(m_pTriangleListFillUboDeviceMemory, 0, dynamicAlignment * m_maxUBOs, ::framework::gpu::memory_map_flags::none, &m_pTriangleListFillUboMappedData);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::lbvh::CompactTreeNode) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pRadixTreeBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pRadixTreeBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pRadixTreeBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pRadixTreeDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pRadixTreeBuffer;
		bind_buffer_memory_info.memory = m_pRadixTreeDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::lbvh::CompactTreeNode) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pRadixTreeReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pRadixTreeReadbackBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pRadixTreeReadbackBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pRadixTreeReadbackDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pRadixTreeReadbackBuffer;
		bind_buffer_memory_info.memory = m_pRadixTreeReadbackDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pCounterBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pCounterDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pCounterBuffer;
		bind_buffer_memory_info.memory = m_pCounterDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::lbvh::Bound) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pBoundsBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pBoundsBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pBoundsDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pBoundsBuffer;
		bind_buffer_memory_info.memory = m_pBoundsDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(CompactSphere) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pPrimitivesBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pPrimitivesBuffer;
		bind_buffer_memory_info.memory = m_pPrimitivesDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pPrimitivesCounterBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesCounterDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pPrimitivesCounterBuffer;
		bind_buffer_memory_info.memory = m_pPrimitivesCounterDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitiveIndicesBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pPrimitiveIndicesBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitiveIndicesDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pPrimitiveIndicesBuffer;
		bind_buffer_memory_info.memory = m_pPrimitiveIndicesDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pMortonCodesBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pMortonCodesBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pMortonCodesDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pMortonCodesBuffer;
		bind_buffer_memory_info.memory = m_pMortonCodesDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * 4 * ((m_maxPrimitives - 1) * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNodesBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pNodesBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNodesDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pNodesBuffer;
		bind_buffer_memory_info.memory = m_pNodesDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.flags = ::framework::gpu::buffer_view_create_flags::none;
		buffer_view_create_info.buffer = m_pNodesBuffer;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = sizeof(::std::uint32_t) * 4 * ((m_maxPrimitives - 1) * 4);
		device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pNodesBufferView);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pReadbackBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pReadbackBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pReadbackDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pReadbackBuffer;
		bind_buffer_memory_info.memory = m_pReadbackDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(CompactSphere) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pPrimitivesReadbackBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pPrimitivesReadbackBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesReadbackDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pPrimitivesReadbackBuffer;
		bind_buffer_memory_info.memory = m_pPrimitivesReadbackDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitiveIndicesUploadBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = m_pPrimitiveIndicesUploadBuffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_pPrimitiveIndicesUploadBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitiveIndicesUploadDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = m_pPrimitiveIndicesUploadBuffer;
		bind_buffer_memory_info.memory = m_pPrimitiveIndicesUploadDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pTriangleListFillCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pGlobalBoundComputingCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pGenerateMortonCodesCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pSortMortonCodesCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pConstructRadixTreeCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pCalculateNodeBoundingBoxesCompleteSemaphore);
	device->create_semaphore(&semaphore_create_info, nullptr, &m_pCreateNodesCompleteSemaphore);

	/*{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/TriangleListFillPathTracing.vert";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/TriangleListFillPathTracing.vert";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pTriangleListFillVertexShaderModule = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/TriangleListFillPathTracing.geom";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/TriangleListFillPathTracing.geom";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::geometry_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pTriangleListFillGeometryShaderModule = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/TriangleListFillPathTracing.frag";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/TriangleListFillPathTracing.frag";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pTriangleListFillFragmentShaderModule = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}*/
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/GlobalBoundComputing.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/GlobalBoundComputing.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pGlobalBoundComputingShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/ReductionGlobalBoundComputing.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/ReductionGlobalBoundComputing.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pReductionGlobalBoundComputingShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/GenerateMortonCodes.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/GenerateMortonCodes.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pGenerateMortonCodesShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/ConstructRadixTree.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/ConstructRadixTree.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pConstructRadixTreeShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/CalculateNodeBoundingBoxes.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/CalculateNodeBoundingBoxes.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pCalculateNodeBoundingBoxesShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/CreateNodes.comp.glsl";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/CreateNodes.comp.glsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->m_pCreateNodesShader = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[8];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
	descriptor_pool_sizes[0].descriptor_count = 2;

	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_pool_sizes[1].descriptor_count = 1;

	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::sampled_buffer;
	descriptor_pool_sizes[2].descriptor_count = 15;

	descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[3].descriptor_count = 20;

	descriptor_pool_sizes[4].type = ::framework::gpu::descriptor_type::storage_image;
	descriptor_pool_sizes[4].descriptor_count = 1;

	descriptor_pool_sizes[5].type = ::framework::gpu::descriptor_type::storage_texel_buffer;
	descriptor_pool_sizes[5].descriptor_count = 10;

	descriptor_pool_sizes[6].type = ::framework::gpu::descriptor_type::storage_texel_buffer;
	descriptor_pool_sizes[6].descriptor_count = 10;

	descriptor_pool_sizes[7].type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_pool_sizes[7].descriptor_count = 1;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
	descriptor_pool_create_info.max_sets = 11;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &descriptor_pool);

	// Triangle list fill
	if (0)
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
		pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
		pipeline_shader_stage_create_infos[0].module = m_pTriangleListFillVertexShaderModule;
		pipeline_shader_stage_create_infos[0].name = "main";
		pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

		pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::geometry_bit;
		pipeline_shader_stage_create_infos[1].module = m_pTriangleListFillGeometryShaderModule;
		pipeline_shader_stage_create_infos[1].name = "main";
		pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

		pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
		pipeline_shader_stage_create_infos[2].module = m_pTriangleListFillFragmentShaderModule;
		pipeline_shader_stage_create_infos[2].name = "main";
		pipeline_shader_stage_create_infos[2].specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::vertex_bit | ::framework::gpu::shader_stage_flags::geometry_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit | ::framework::gpu::shader_stage_flags::geometry_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit | ::framework::gpu::shader_stage_flags::geometry_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pTriangleListFillDescriptorSetLayout);

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pTriangleListFillDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pTriangleListFillPipelineLayout);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pTriangleListFillDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pTriangleListFillDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pTriangleListFillUboBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = (sizeof(TransformBufferData) / 256) * 256 + ((sizeof(TransformBufferData) % 256) > 0 ? 256 : 0);
		descriptor_buffer_infos[0].stride = (sizeof(TransformBufferData) / 256) * 256 + ((sizeof(TransformBufferData) % 256) > 0 ? 256 : 0);

		descriptor_buffer_infos[1].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(CompactSphere);

		descriptor_buffer_infos[2].buffer = m_pPrimitivesCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		::framework::gpu::subpass_description subpass_description;
		subpass_description.flags = ::framework::gpu::subpass_description_flags::none;
		subpass_description.pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
		subpass_description.input_attachment_count = 0;
		subpass_description.input_attachments = nullptr;
		subpass_description.color_attachment_count = 0;
		subpass_description.color_attachments = nullptr;
		subpass_description.resolve_attachments = nullptr;
		subpass_description.depth_stencil_attachment = nullptr;
		subpass_description.preserve_attachment_count = 0;
		subpass_description.preserve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.attachment_count = 0;
		render_pass_create_info.attachments = nullptr;
		render_pass_create_info.subpass_count = 1;
		render_pass_create_info.subpasses = &subpass_description;
		render_pass_create_info.dependency_count = 0;
		render_pass_create_info.dependencies = nullptr;
		device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass);

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.width = width;
		frame_buffer_create_info.height = height;
		frame_buffer_create_info.attachment_count = 0;
		frame_buffer_create_info.attachments = nullptr;
		frame_buffer_create_info.render_pass = this->render_pass;
		device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffer);

		::framework::gpu::viewport viewport;
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)width;
		viewport.height = (float)height;
		viewport.min_depth = 0.0f;
		viewport.max_depth = 1.0f;
		::framework::gpu::rect_2d scissor;
		scissor.offset = { 0, 0 };
		scissor.extent = { width, height };
		::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
		pipeline_viewport_state_create_info.viewport_count = 1;
		pipeline_viewport_state_create_info.scissors = &scissor;
		pipeline_viewport_state_create_info.scissor_count = 1;
		pipeline_viewport_state_create_info.viewports = &viewport;

		::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
		pipeline_rasterization_state_create_info.depth_clamp_enable = false;
		pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
		pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
		pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
		pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
		pipeline_rasterization_state_create_info.depth_bias_enable = false;
		pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
		pipeline_rasterization_state_create_info.line_width = 1.0f;

		::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
		pipeline_color_blend_state_create_info.logic_op_enable = false;
		pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
		pipeline_color_blend_state_create_info.attachment_count = 0;
		pipeline_color_blend_state_create_info.attachments = nullptr;
		pipeline_color_blend_state_create_info.blend_constants[0] = 0;
		pipeline_color_blend_state_create_info.blend_constants[1] = 0;
		pipeline_color_blend_state_create_info.blend_constants[2] = 0;
		pipeline_color_blend_state_create_info.blend_constants[3] = 0;

		::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo;
		pipelineDepthStencilStateCreateInfo.depth_test_enable = false;
		pipelineDepthStencilStateCreateInfo.depth_write_enable = false;
		pipelineDepthStencilStateCreateInfo.depth_compare_op = ::framework::gpu::compare_op::always;
		pipelineDepthStencilStateCreateInfo.depth_bounds_test_enable = false;
		pipelineDepthStencilStateCreateInfo.stencil_test_enable = false;
		pipelineDepthStencilStateCreateInfo.front = {};
		pipelineDepthStencilStateCreateInfo.back = {};
		pipelineDepthStencilStateCreateInfo.back.compare_op = ::framework::gpu::compare_op::always;
		pipelineDepthStencilStateCreateInfo.min_depth_bounds = 0.0f;
		pipelineDepthStencilStateCreateInfo.max_depth_bounds = 0.0f;

		::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[2];
		vertex_input_binding_descriptions[0].binding = 0;
		vertex_input_binding_descriptions[0].stride = sizeof(float) * 3;
		vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		vertex_input_binding_descriptions[1].binding = 4;
		vertex_input_binding_descriptions[1].stride = sizeof(float) * 2;
		vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		//vertex_input_binding_descriptions[1].binding = 1;
		//vertex_input_binding_descriptions[1].stride = sizeof(float) * 3;
		//vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		//vertex_input_binding_descriptions[2].binding = 2;
		//vertex_input_binding_descriptions[2].stride = sizeof(float) * 3;
		//vertex_input_binding_descriptions[2].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		//vertex_input_binding_descriptions[3].binding = 3;
		//vertex_input_binding_descriptions[3].stride = sizeof(float) * 3;
		//vertex_input_binding_descriptions[3].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		//vertex_input_binding_descriptions[4].binding = 4;
		//vertex_input_binding_descriptions[4].stride = sizeof(float) * 2;
		//vertex_input_binding_descriptions[4].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[2];
		// Location 0 : Position
		vertex_input_attribute_descriptions[0].location = 0;
		vertex_input_attribute_descriptions[0].binding = 0;
		vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32_sfloat;
		vertex_input_attribute_descriptions[0].offset = 0;
		// Location 4 : Texture coordinates
		vertex_input_attribute_descriptions[1].location = 4;
		vertex_input_attribute_descriptions[1].binding = 4;
		vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32_sfloat;
		vertex_input_attribute_descriptions[1].offset = 0;
		//// Location 1 : Vertex normal
		//vertex_input_attribute_descriptions[1].location = 1;
		//vertex_input_attribute_descriptions[1].binding = 1;
		//vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32_sfloat;
		//vertex_input_attribute_descriptions[1].offset = 0;
		//// Location 2 : Vertex binormal
		//vertex_input_attribute_descriptions[2].location = 2;
		//vertex_input_attribute_descriptions[2].binding = 2;
		//vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32g32b32_sfloat;
		//vertex_input_attribute_descriptions[2].offset = 0;
		//// Location 3 : Vertex tangent
		//vertex_input_attribute_descriptions[3].location = 3;
		//vertex_input_attribute_descriptions[3].binding = 3;
		//vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32g32b32_sfloat;
		//vertex_input_attribute_descriptions[3].offset = 0;
		//// Location 4 : Texture coordinates
		//vertex_input_attribute_descriptions[4].location = 4;
		//vertex_input_attribute_descriptions[4].binding = 4;
		//vertex_input_attribute_descriptions[4].format = ::framework::gpu::format::r32g32_sfloat;
		//vertex_input_attribute_descriptions[4].offset = 0;
		::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
		vertex_input_state.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
		vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
		vertex_input_state.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
		vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

		::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
		pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
		pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

		::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info{};
		graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
		graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
		graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
		graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
		graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
		graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
		graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
		graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
		graphics_pipeline_create_info.layout = m_pTriangleListFillPipelineLayout;
		graphics_pipeline_create_info.render_pass = this->render_pass;
		device->create_graphics_pipelines(nullptr, 1, &graphics_pipeline_create_info, nullptr, &m_pTriangleListFillPipeline);
	}

	// GlobalBoundComputing
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pGlobalBoundComputingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pGlobalBoundComputingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pGlobalBoundComputingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pGlobalBoundComputingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pGlobalBoundComputingPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pGlobalBoundComputingPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pGlobalBoundComputingDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pGlobalBoundComputingDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(CompactSphere);

		descriptor_buffer_infos[1].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::framework::lbvh::Bound);

		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = m_pGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// ReductionGlobalBoundComputing
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pReductionGlobalBoundComputingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pReductionGlobalBoundComputingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pReductionGlobalBoundComputingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pReductionGlobalBoundComputingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pReductionGlobalBoundComputingPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pReductionGlobalBoundComputingPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pReductionGlobalBoundComputingDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pReductionGlobalBoundComputingDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(::framework::lbvh::Bound);

		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
		write_descriptor_sets[0].dst_set = m_pReductionGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// GenerateMortonCodes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pGenerateMortonCodesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pGenerateMortonCodesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pGenerateMortonCodesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pGenerateMortonCodesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pGenerateMortonCodesPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pGenerateMortonCodesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pGenerateMortonCodesDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pGenerateMortonCodesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
		descriptor_buffer_infos[0].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(::framework::lbvh::Bound);

		descriptor_buffer_infos[1].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(CompactSphere);

		descriptor_buffer_infos[2].buffer = m_pMortonCodesBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

		descriptor_buffer_infos[3].buffer = m_pPrimitiveIndicesBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
		write_descriptor_sets[0].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];

		write_descriptor_sets[3].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// ConstructRadixTree
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pConstructRadixTreeShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pConstructRadixTreeDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pConstructRadixTreeDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pConstructRadixTreePipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pConstructRadixTreePipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pConstructRadixTreePipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pConstructRadixTreeDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pConstructRadixTreeDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pMortonCodesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);

		descriptor_buffer_infos[1].buffer = m_pPrimitiveIndicesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);

		descriptor_buffer_infos[2].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::framework::lbvh::CompactTreeNode);

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// CalculateNodeBoundingBoxes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pCalculateNodeBoundingBoxesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pCalculateNodeBoundingBoxesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pCalculateNodeBoundingBoxesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCalculateNodeBoundingBoxesPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pCalculateNodeBoundingBoxesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pCalculateNodeBoundingBoxesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(CompactSphere);

		descriptor_buffer_infos[1].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::framework::lbvh::CompactTreeNode);

		descriptor_buffer_infos[2].buffer = m_pCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	// CreateNodes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = m_pCreateNodesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &m_pCreateNodesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCreateNodesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pCreateNodesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCreateNodesPipelineLayout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &m_pCreateNodesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &m_pCreateNodesDescriptorSetLayout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pCreateNodesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactTreeNode);

		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = m_pCreateNodesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pCreateNodesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		write_descriptor_sets[1].texel_buffer_view = &m_pNodesBufferView;
		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	}

	//::framework::algorithm::lsd_radix_sort::allocate(this->physical_device, device, m_maxPrimitives, 32, &m_pRadixSortData);
	//::framework::algorithm::lsd_radix_sort::configure(m_pRadixSortData, m_pMortonCodesBuffer, m_pPrimitiveIndicesBuffer);

	::framework::algorithm::hybrid_radix_sort_create_info hybrid_radix_sort_create_info;
	hybrid_radix_sort_create_info.maxSize = m_maxPrimitives;
	hybrid_radix_sort_create_info.maxSortBits = 32;
	hybrid_radix_sort_create_info.physical_device = this->physical_device;
	hybrid_radix_sort_create_info.device = device;
	hybrid_radix_sort_create_info.pInoutKeysBuffer = m_pMortonCodesBuffer;
	hybrid_radix_sort_create_info.pInoutValuesBuffer = m_pPrimitiveIndicesBuffer;
	hybrid_radix_sort_create_info.resource_manager = this->resource_manager;
	::framework::algorithm::hybrid_radix_sort::allocate(hybrid_radix_sort_create_info, pRadixSortData);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = 0;
	buffer_copy.dst_offset = 0;
	buffer_copy.size = sizeof(::std::uint32_t);

	//PipelineBarrier pipelineBarriers[3];
	//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
	//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;

	//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

	//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
	//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
	//buffer_memory_barriers[0].pipelineBarrierCount = 1;
	//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[0].buffer = m_pPrimitivesCounterBuffer;
	//buffer_memory_barriers[0].offset = 0;
	//buffer_memory_barriers[0].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	//buffer_memory_barriers[1].pipelineBarrierCount = 1;
	//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[1].buffer = m_pPrimitivesCounterBuffer;
	//buffer_memory_barriers[1].offset = 0;
	//buffer_memory_barriers[1].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	//buffer_memory_barriers[2].pipelineBarrierCount = 1;
	//buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
	//buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barriers[2].buffer = m_pPrimitivesCounterBuffer;
	//buffer_memory_barriers[2].offset = 0;
	//buffer_memory_barriers[2].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	//::framework::gpu::dependency_info memoryBarriers[3];
	//memoryBarriers[0].flags = 0;
	//memoryBarriers[0].buffer_memory_barrier_count = 1;
	//memoryBarriers[0].buffer_memory_barriers = &buffer_memory_barriers[0];
	//memoryBarriers[0].image_memory_barrier_count = 0;
	//memoryBarriers[0].image_memory_barriers = nullptr;

	//memoryBarriers[1].flags = 0;
	//memoryBarriers[1].buffer_memory_barrier_count = 1;
	//memoryBarriers[1].buffer_memory_barriers = &buffer_memory_barriers[1];
	//memoryBarriers[1].image_memory_barrier_count = 0;
	//memoryBarriers[1].image_memory_barriers = nullptr;

	//memoryBarriers[2].flags = 0;
	//memoryBarriers[2].buffer_memory_barrier_count = 1;
	//memoryBarriers[2].buffer_memory_barriers = &buffer_memory_barriers[2];
	//memoryBarriers[2].image_memory_barrier_count = 0;
	//memoryBarriers[2].image_memory_barriers = nullptr;

	::framework::gpu::memory_barrier memory_barrier[1];
	memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
	dependency_info.memory_barriers = memory_barrier;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	m_pPrimitiveCounterServicingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	m_pPrimitiveCounterServicingCommandBuffer->pipeline_barrier(&dependency_info);
	//m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &memoryBarriers[0]);
	m_pPrimitiveCounterServicingCommandBuffer->pipeline_barrier(&dependency_info);
	m_pPrimitiveCounterServicingCommandBuffer->copy_buffer(m_pPrimitivesCounterBuffer, m_pReadbackBuffer, 1, &buffer_copy);
	//m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &memoryBarriers[1]);
	m_pPrimitiveCounterServicingCommandBuffer->pipeline_barrier(&dependency_info);
	m_pPrimitiveCounterServicingCommandBuffer->fill_buffer(m_pPrimitivesCounterBuffer, 0, sizeof(::std::uint32_t), 0, 0, 0, 0);
	//m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &memoryBarriers[2]);
	m_pPrimitiveCounterServicingCommandBuffer->pipeline_barrier(&dependency_info);
	m_pPrimitiveCounterServicingCommandBuffer->end_command_buffer();
}

void ::framework::lbvh::deallocate()
{
}

void ::framework::lbvh::allocate(LBVHCreateInfo const * create_info, lbvh ** ppLBVH)
{
	*ppLBVH = new lbvh(create_info);
	(*ppLBVH)->allocate();
}

void ::framework::lbvh::deallocate(lbvh * pLBVH)
{
	pLBVH->deallocate();
	delete pLBVH;
}

void ::framework::lbvh::update()
{
	performTriangleListFillPass();
	buildBVH();
}

::std::uint32_t(::framework::lbvh::getMaxLeavesCount)() const
{
	return m_maxPrimitives;
}

::std::uint32_t(::framework::lbvh::getLeavesCount)() const
{
	return m_numTriangles;
}

class ::framework::gpu::buffer * ::framework::lbvh::getRadixTreeBuffer()
{
	return m_pRadixTreeBuffer;
}

class ::framework::gpu::buffer * ::framework::lbvh::getPrimitiveBuffer()
{
	return m_pPrimitivesBuffer;
}

class ::framework::gpu::semaphore * ::framework::lbvh::getBuildCompletionSemaphore()
{
	return m_pCreateNodesCompleteSemaphore;
}

void ::framework::lbvh::performTriangleListFillPass()
{
	if (this->m_numTriangles)
	{
		return;
	}

	this->m_numTriangles = this->m_maxPrimitives;

	class ::framework::gpu::queue * command_queue;
	device->get_queue(0, 0, &command_queue);


	::std::uint32_t * primitive_indices_mapped_data;
	device->map_memory(m_pPrimitiveIndicesUploadDeviceMemory, 0, sizeof(::std::uint32_t) * this->m_numTriangles, ::framework::gpu::memory_map_flags::none, ((void **)&primitive_indices_mapped_data));

	for (::std::uint32_t i = 0; i < this->m_numTriangles; i++)
	{
		primitive_indices_mapped_data[i] = i;
	}

	device->unmap_memory(m_pPrimitiveIndicesUploadDeviceMemory);

	CompactSphere * primitives_mapped_data;
	device->map_memory(m_pPrimitivesReadbackDeviceMemory, 0, sizeof(CompactSphere) * this->m_numTriangles, ::framework::gpu::memory_map_flags::none, ((void **)&primitives_mapped_data));

	for (::std::uint32_t i = 0; i < this->m_numTriangles; i++)
	{
		float const sphere_radius = 0.01f;
		float const box_size = 8.0;

		float p = i * sphere_radius * 2;
		primitives_mapped_data[i] =
		{
			{(rand() / double(RAND_MAX) - 0.5) * box_size, (rand() / double(RAND_MAX) - 0.5) * box_size, (rand() / double(RAND_MAX) - 0.5) * box_size, sphere_radius },
			{1.0f, 0.0f, 0.0f, 1.0f }
		};

		/*float x = ((i / 4) % 2) * sphere_radius * 2;
		float y = ((i / 2) % 2) * sphere_radius * 2;
		float z = ((i / 1) % 2) * sphere_radius * 2;
		primitives_mapped_data[i] =
		{
			{x, y, z, sphere_radius + 0.0001f },
			{1.0f, 0.0f, 0.0f, 1.0f }
		};*/
	}

	device->unmap_memory(m_pPrimitivesReadbackDeviceMemory);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::buffer_copy buffer_copy[2];
	buffer_copy[0].src_offset = 0;
	buffer_copy[0].dst_offset = 0;
	buffer_copy[0].size = sizeof(CompactSphere) * this->m_numTriangles;

	buffer_copy[1].src_offset = 0;
	buffer_copy[1].dst_offset = 0;
	buffer_copy[1].size = sizeof(::std::uint32_t) * this->m_numTriangles;

	::framework::gpu::memory_barrier memory_barrier[1];
	memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
	dependency_info.memory_barriers = memory_barrier;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	m_pCalculateNodeBoundingBoxesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
	m_pCalculateNodeBoundingBoxesCommandBuffer->copy_buffer(m_pPrimitivesReadbackBuffer, m_pPrimitivesBuffer, 1, &buffer_copy[0]);
	m_pCalculateNodeBoundingBoxesCommandBuffer->copy_buffer(m_pPrimitiveIndicesUploadBuffer, m_pPrimitiveIndicesBuffer, 1, &buffer_copy[1]);
	m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
	m_pCalculateNodeBoundingBoxesCommandBuffer->end_command_buffer();

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = m_pCalculateNodeBoundingBoxesCommandBuffer;
	command_buffer_info.device_mask = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 0;
	submit_info.wait_semaphore_infos = nullptr;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = 0;
	submit_info.signal_semaphore_infos = nullptr;
	command_queue->submit(1, &submit_info, nullptr);

	device->wait_idle();

	////Orthograhic projection
	//::glm::mat4 Ortho = ::glm::ortho(0.0f, 128.0f, 0.0f, 128.0f, -64.0f, 64.0f);
	////Create an modelview-orthographic projection matrix see from +X axis
	//::glm::mat4 mvpX = Ortho * ::glm::lookAt(::glm::vec3(0, 0, 0), ::glm::vec3(1, 0, 0), ::glm::vec3(0, 1, 0));
	////Create an modelview-orthographic projection matrix see from +Y axis
	//::glm::mat4 mvpY = Ortho * ::glm::lookAt(::glm::vec3(0, 0, 0), ::glm::vec3(0, 1, 0), ::glm::vec3(0, 0, -1));
	////Create an modelview-orthographic projection matrix see from +Z axis
	//::glm::mat4 mvpZ = Ortho * ::glm::lookAt(::glm::vec3(0, 0, 0), ::glm::vec3(0, 0, 1), ::glm::vec3(0, 1, 0));

	//::std::uint32_t threadIndex = 0;
	//::std::uint32_t entityIndex = 0;
	//::std::mutex commandBuffersMutex;
	//const ::std::size_t uboAlignment = 256;
	//const ::std::size_t dynamicAlignment = (sizeof(TransformBufferData) / uboAlignment) * uboAlignment + ((sizeof(TransformBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

	//::framework::gpu::command_buffer_inheritance_info inheritanceInfo;
	//inheritanceInfo.frame_buffer = class ::framework::gpu::frame_buffer;
	//inheritanceInfo.subpass = 0;
	//inheritanceInfo.render_pass = class ::framework::gpu::render_pass;

	//auto entities = entity_manager::entities_with_components<CMeshRendererPathTracingBVH, CTransform>();
	//component_handle<CMeshRendererPathTracingBVH> mesh_renderer;
	//component_handle<CTransform> transform;
	//for (auto entity : entities)
	//{
	//	entity.unpack<CMeshRendererPathTracingBVH, CTransform>(mesh_renderer, transform);
	//	thread_pool.threads[threadIndex]->addJob([=, &inheritanceInfo, &commandBuffersMutex, &thread_data = thread_data]
	//	{
	//		TransformBufferData transformBufferData;
	//		transformBufferData.mMVPx = mvpX;
	//		transformBufferData.mMVPy = mvpY;
	//		transformBufferData.mMVPz = mvpZ;
	//		transformBufferData.mObject2World = transform->getWorldMatrix();
	//		transformBufferData.mWorld2Object = ::glm::inverse(transformBufferData.mObject2World);
	//		//transformBufferData.mModelView = transformBufferData.mObject2World * m_camera->getViewMatrix();
	//		//transformBufferData.mModelViewProjection = transformBufferData.mObject2World * m_camera->getViewProjectionMatrix();
	//		//transformBufferData.mTransposeModelView = ::glm::transpose(transformBufferData.mModelView);
	//		//transformBufferData.mInverseTransposeModelView = ::glm::inverse(transformBufferData.mTransposeModelView);

	//		const ::std::uint32_t dynamic_offset = entityIndex * static_cast<::std::uint32_t>(dynamicAlignment);
	//		::std::memcpy(static_cast<char *>(m_pTriangleListFillUboMappedData) + dynamic_offset, &transformBufferData, sizeof(transformBufferData));

	//		auto & thread = thread_data[threadIndex];

	//		::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
	//		for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
	//			drawCallCountMap.insert(::std::make_pair(thread.commandBufferData[i].drawCallCount, i));

	//		auto & commandBufferIndex = drawCallCountMap.begin()->second;
	//		auto & commandBufferData = thread.commandBufferData[commandBufferIndex];
	//		auto & class ::framework::gpu::command_buffer = thread.commandBuffers[commandBufferIndex];
	//		if (!commandBufferData.is_command_buffer_open)
	//		{
	//			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit;
	//			command_buffer_begin_info.inheritance_info = &inheritanceInfo;

	//			class ::framework::gpu::command_buffer->begin_command_buffer(&command_buffer_begin_info);
	//			class ::framework::gpu::command_buffer->bind_pipeline(m_pTriangleListFillPipeline);

	//			commandBufferData.is_command_buffer_open = true;
	//		}

	//		class ::framework::gpu::command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pTriangleListFillDescriptorTable, 1, &dynamic_offset);
	//		mesh_renderer->Draw(class ::framework::gpu::command_buffer);

	//		commandBufferData.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
	//	});
	//	threadIndex = (threadIndex + 1) % thread_count;
	//	entityIndex++;
	//}

	//::std::vector<class ::framework::gpu::command_buffer *> commandBuffers;
	//for (::std::uint32_t t = 0; t < thread_count; t++)
	//{
	//	thread_pool.threads[t]->addJob([=, &commandBuffers, &commandBuffersMutex]
	//	{
	//		auto & thread = thread_data[t];
	//		for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
	//		{
	//			if (thread.commandBufferData[i].is_command_buffer_open)
	//			{
	//				thread.commandBuffers[i]->end_command_buffer();
	//				thread.commandBufferData[i].is_command_buffer_open = false;
	//				thread.commandBufferData[i].drawCallCount = 0;
	//				::std::lock_guard<::std::mutex> lock(commandBuffersMutex);
	//				commandBuffers.push_back(thread.commandBuffers[i]);
	//			}
	//		}
	//	});
	//}

	//thread_pool.wait();

	//::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	//command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;

	//::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };
	//::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	//renderPassBeginInfo.frame_buffer = class ::framework::gpu::frame_buffer;
	//renderPassBeginInfo.render_pass = class ::framework::gpu::render_pass;
	//renderPassBeginInfo.render_area = render_area;
	//renderPassBeginInfo.clear_value_count = 0;
	//renderPassBeginInfo.clear_values = nullptr;

	//PipelineBarrier pipelineBarrier;
	//pipelineBarrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//pipelineBarrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
	//pipelineBarrier.src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//pipelineBarrier.dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

	//::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
	//buffer_memory_barrier.pipelineBarrierCount = 1;
	//buffer_memory_barrier.pPipelineBarriers = &pipelineBarrier;
	//buffer_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	//buffer_memory_barrier.buffer = m_pPrimitivesCounterBuffer;
	//buffer_memory_barrier.offset = 0;
	//buffer_memory_barrier.size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	//::framework::gpu::dependency_info dependency_info;
	//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	//dependency_info.buffer_memory_barrier_count = 1;
	//dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
	//dependency_info.image_memory_barrier_count = 0;
	//dependency_info.image_memory_barriers = nullptr;

	//m_pPrimaryCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//m_pPrimaryCommandBuffer->dependency_info(0, 1, &dependency_info);
	//m_pPrimaryCommandBuffer->begin_render_pass(&renderPassBeginInfo, ::framework::gpu::subpass_contents::secondary_command_buffers);
	//m_pPrimaryCommandBuffer->execute_commands(commandBuffers.size(), commandBuffers.data());
	//m_pPrimaryCommandBuffer->end_render_pass();
	//m_pPrimaryCommandBuffer->end_command_buffer();

	//class ::framework::gpu::queue * command_queue;
	//device->get_queue(0, 0, &command_queue);

	//::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	//::framework::gpu::submit_info submit_infos[2];
	//submit_infos[0].command_buffer_count = 1;
	//submit_infos[0].command_buffers = &m_pPrimaryCommandBuffer;
	//submit_infos[0].wait_semaphore_count = 0;
	//submit_infos[0].wait_semaphores = nullptr;
	//submit_infos[0].wait_dst_stage_mask = nullptr;
	//submit_infos[0].signal_semaphore_count = 1;
	//submit_infos[0].signal_semaphores = &m_pTriangleListFillCompleteSemaphore;

	//submit_infos[1].command_buffer_count = 1;
	//submit_infos[1].command_buffers = &m_pPrimitiveCounterServicingCommandBuffer;
	//submit_infos[1].wait_semaphore_count = 1;
	//submit_infos[1].wait_semaphores = &m_pTriangleListFillCompleteSemaphore;
	//submit_infos[1].wait_dst_stage_mask = &pWaitDstStageMask;
	//submit_infos[1].signal_semaphore_count = 0;
	//submit_infos[1].signal_semaphores = nullptr;

	//command_queue->submit(::std::size(submit_infos), submit_infos, m_pPrimitiveCounterServicingCompleteFence);

	//device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
	//device->reset_fences(1, &m_pPrimitiveCounterServicingCompleteFence);

	//void * pMappedData;
	//device->map_memory(m_pReadbackDeviceMemory, 0, sizeof(::std::uint32_t), 0, &pMappedData);
	//::std::memcpy(&m_numTriangles, pMappedData, sizeof(::std::uint32_t));
	//device->unmap_memory(m_pReadbackDeviceMemory);

	///*ReadbackBuffer readbackBuffer;
	//readbackBuffer.initialise(device, m_pPrimitivesBuffer->GetBufferCreateInfo()->size);
	//{
	//	::std::vector<CompactSphere> compactSpheres(m_numTriangles);

	//	::framework::gpu::buffer_copy buffer_copy{};
	//	buffer_copy.size = sizeof(CompactSphere) * m_numTriangles;

	//	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	//	command_buffer_begin_info.inheritance_info = nullptr;

	//	m_pGlobalBoundComputingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//	m_pGlobalBoundComputingCommandBuffer->copy_buffer(m_pPrimitivesBuffer, m_pPrimitivesReadbackBuffer, 1, &buffer_copy);
	//	m_pGlobalBoundComputingCommandBuffer->end_command_buffer();

	//	::framework::gpu::submit_info submit_info;
	//	submit_info.command_buffer_count = 1;
	//	submit_info.command_buffers = &m_pGlobalBoundComputingCommandBuffer;
	//	submit_info.wait_semaphore_count = 0;
	//	submit_info.wait_semaphores = nullptr;
	//	submit_info.wait_dst_stage_mask = nullptr;
	//	submit_info.signal_semaphore_count = 0;
	//	submit_info.signal_semaphores = nullptr;
	//	command_queue->submit(1, &submit_info, m_pPrimitiveCounterServicingCompleteFence);

	//	device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);

	//	void * pMappedData;
	//	device->map_memory(m_pPrimitivesReadbackDeviceMemory, sizeof(CompactSphere) * m_numTriangles, 0, &pMappedData);
	//	::std::memcpy(compactSpheres.data(), pMappedData, sizeof(CompactSphere) * m_numTriangles);
	//	device->unmap_memory(m_pPrimitivesReadbackDeviceMemory);
	//	::std::for_each(compactSpheres.begin(), compactSpheres.end(), [](CompactSphere &n) { ::std::cout << n.field0.x << ", " << n.field0.y  << ", " << n.field0.z << ::std::endl; });
	//	system("pause");
	//}*/

	//::std::cout << "primitivesCounter: " << m_numTriangles << ::std::endl;
}

void ::framework::lbvh::buildBVH()
{
	if (!m_numTriangles)
	{
		return;
	}

	static bool refit_only = false;

	class ::framework::gpu::queue * command_queue;
	device->get_queue(0, 0, &command_queue);

	if (!refit_only)
	{
		// GlobalBoundComputing
		{
			::std::uint32_t remaining = m_numTriangles;
			::std::forward_list<::std::uint32_t> push_constant;
			push_constant.push_front(remaining);

			//PipelineBarrier pipelineBarriers[2];
			//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
			//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;

			//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
			//buffer_memory_barriers[0].pipelineBarrierCount = 1;
			//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
			//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].buffer = m_pPrimitivesBuffer;
			//buffer_memory_barriers[0].offset = 0;
			//buffer_memory_barriers[0].size = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;

			//buffer_memory_barriers[1].pipelineBarrierCount = 1;
			//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
			//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].buffer = m_pBoundsBuffer;
			//buffer_memory_barriers[1].offset = 0;
			//buffer_memory_barriers[1].size = m_pBoundsBuffer->GetBufferCreateInfo()->size;

			//::framework::gpu::dependency_info dependency_info;
			//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
			//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
			//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			//dependency_info.image_memory_barrier_count = 0;
			//dependency_info.image_memory_barriers = nullptr;

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;

			::framework::gpu::memory_barrier memory_barrier[1];
			memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
			dependency_info.memory_barriers = memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			m_pGlobalBoundComputingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
			//m_pGlobalBoundComputingCommandBuffer->dependency_info(0, 1, &dependency_info);
			m_pGlobalBoundComputingCommandBuffer->pipeline_barrier(&dependency_info);
			m_pGlobalBoundComputingCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pGlobalBoundComputingPipeline);
			//m_pGlobalBoundComputingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pGlobalBoundComputingDescriptorTable, 0, nullptr);
			m_pGlobalBoundComputingCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pGlobalBoundComputingPipelineLayout, 0, 1, &m_pGlobalBoundComputingDescriptorSet, 0, nullptr);
			m_pGlobalBoundComputingCommandBuffer->push_constants(m_pGlobalBoundComputingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constant.front()), &push_constant.front());
			m_pGlobalBoundComputingCommandBuffer->dispatch((remaining + (64 * 64) - 1) / (64 * 64), 1, 1);

			//dependency_info.buffer_memory_barrier_count = 1;
			//dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

			remaining = (remaining + (64 * 64) - 1) / (64 * 64);
			push_constant.push_front(remaining);
			while (remaining > 1)
			{
				//m_pGlobalBoundComputingCommandBuffer->dependency_info(0, 1, &dependency_info);
				m_pGlobalBoundComputingCommandBuffer->pipeline_barrier(&dependency_info);
				m_pGlobalBoundComputingCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pReductionGlobalBoundComputingPipeline);
				//m_pGlobalBoundComputingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pReductionGlobalBoundComputingDescriptorTable, 0, nullptr);
				m_pGlobalBoundComputingCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pReductionGlobalBoundComputingPipelineLayout, 0, 1, &m_pReductionGlobalBoundComputingDescriptorSet, 0, nullptr);
				m_pGlobalBoundComputingCommandBuffer->push_constants(m_pReductionGlobalBoundComputingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constant.front()), &push_constant.front());
				m_pGlobalBoundComputingCommandBuffer->dispatch((remaining + (64 * 64) - 1) / (64 * 64), 1, 1);

				remaining = (remaining + (64 * 64) - 1) / (64 * 64);
				push_constant.push_front(remaining);
			}
			m_pGlobalBoundComputingCommandBuffer->end_command_buffer();

			::framework::gpu::command_buffer_submit_info command_buffer_info;
			command_buffer_info.command_buffer = m_pGlobalBoundComputingCommandBuffer;
			command_buffer_info.device_mask = 0;

			::framework::gpu::semaphore_submit_info signal_semaphore_info;
			signal_semaphore_info.semaphore = m_pGlobalBoundComputingCompleteSemaphore;
			signal_semaphore_info.value = 0;
			signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			signal_semaphore_info.device_index = 0;

			::framework::gpu::submit_info submit_info;
			submit_info.flags = ::framework::gpu::submit_flags::none;
			submit_info.wait_semaphore_info_count = 0;
			submit_info.wait_semaphore_infos = nullptr;
			submit_info.command_buffer_info_count = 1;
			submit_info.command_buffer_infos = &command_buffer_info;
			submit_info.signal_semaphore_info_count = 1;
			submit_info.signal_semaphore_infos = &signal_semaphore_info;
			command_queue->submit(1, &submit_info, nullptr);
		}

		// GenerateMortonCodes
		{
			::std::uint32_t pushConstant = m_numTriangles;

			//PipelineBarrier pipelineBarriers[3];
			//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
			//buffer_memory_barriers[0].pipelineBarrierCount = 1;
			//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
			//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].buffer = m_pBoundsBuffer;
			//buffer_memory_barriers[0].offset = 0;
			//buffer_memory_barriers[0].size = m_pBoundsBuffer->GetBufferCreateInfo()->size;

			//buffer_memory_barriers[1].pipelineBarrierCount = 1;
			//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
			//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].buffer = m_pMortonCodesBuffer;
			//buffer_memory_barriers[1].offset = 0;
			//buffer_memory_barriers[1].size = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;

			//buffer_memory_barriers[2].pipelineBarrierCount = 1;
			//buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
			//buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[2].buffer = m_pPrimitiveIndicesBuffer;
			//buffer_memory_barriers[2].offset = 0;
			//buffer_memory_barriers[2].size = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;

			//::framework::gpu::dependency_info dependency_info;
			//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
			//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
			//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			//dependency_info.image_memory_barrier_count = 0;
			//dependency_info.image_memory_barriers = nullptr;

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;

			::framework::gpu::memory_barrier memory_barrier[1];
			memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
			dependency_info.memory_barriers = memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			m_pGenerateMortonCodesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
			//m_pGenerateMortonCodesCommandBuffer->dependency_info(0, 1, &dependency_info);
			m_pGenerateMortonCodesCommandBuffer->pipeline_barrier(&dependency_info);
			m_pGenerateMortonCodesCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pGenerateMortonCodesPipeline);
			//m_pGenerateMortonCodesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pGenerateMortonCodesDescriptorTable, 0, nullptr);
			m_pGenerateMortonCodesCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pGenerateMortonCodesPipelineLayout, 0, 1, &m_pGenerateMortonCodesDescriptorSet, 0, nullptr);
			m_pGenerateMortonCodesCommandBuffer->push_constants(m_pGenerateMortonCodesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
			m_pGenerateMortonCodesCommandBuffer->dispatch((m_numTriangles + 64 - 1) / 64, 1, 1);
			m_pGenerateMortonCodesCommandBuffer->end_command_buffer();

			::framework::gpu::semaphore_submit_info wait_semaphore_info;
			wait_semaphore_info.semaphore = m_pGlobalBoundComputingCompleteSemaphore;
			wait_semaphore_info.value = 0;
			wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			wait_semaphore_info.device_index = 0;

			::framework::gpu::command_buffer_submit_info command_buffer_info;
			command_buffer_info.command_buffer = m_pGenerateMortonCodesCommandBuffer;
			command_buffer_info.device_mask = 0;

			::framework::gpu::semaphore_submit_info signal_semaphore_info;
			signal_semaphore_info.semaphore = m_pGenerateMortonCodesCompleteSemaphore;
			signal_semaphore_info.value = 0;
			signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			signal_semaphore_info.device_index = 0;

			::framework::gpu::submit_info submit_info;
			submit_info.flags = ::framework::gpu::submit_flags::none;
			submit_info.wait_semaphore_info_count = 1;
			submit_info.wait_semaphore_infos = &wait_semaphore_info;
			submit_info.command_buffer_info_count = 1;
			submit_info.command_buffer_infos = &command_buffer_info;
			submit_info.signal_semaphore_info_count = 1;
			submit_info.signal_semaphore_infos = &signal_semaphore_info;
			command_queue->submit(1, &submit_info, nullptr);
		}

		// Sort morton codes --------------------------------
		{
			::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//::framework::algorithm::lsd_radix_sort::setSize(m_pRadixSortData, m_numTriangles);
			//::framework::algorithm::lsd_radix_sort::execute(m_pRadixSortData, command_queue, m_pGenerateMortonCodesCompleteSemaphore, pWaitDstStageMask, m_pSortMortonCodesCompleteSemaphore, nullptr);

			pRadixSortData->setSize(m_numTriangles);
			pRadixSortData->execute(command_queue, m_pGenerateMortonCodesCompleteSemaphore, pWaitDstStageMask, m_pSortMortonCodesCompleteSemaphore, nullptr);
		}
		// Sort morton codes --------------------------------

		// Construct radix tree --------------------------------
		{
			::std::uint32_t pushConstant = m_numTriangles;

			//PipelineBarrier pipelineBarriers[3];
			//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

			//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
			//buffer_memory_barriers[0].pipelineBarrierCount = 1;
			//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
			//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[0].buffer = m_pMortonCodesBuffer;
			//buffer_memory_barriers[0].offset = 0;
			//buffer_memory_barriers[0].size = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;

			//buffer_memory_barriers[1].pipelineBarrierCount = 1;
			//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
			//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[1].buffer = m_pPrimitiveIndicesBuffer;
			//buffer_memory_barriers[1].offset = 0;
			//buffer_memory_barriers[1].size = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;

			//buffer_memory_barriers[2].pipelineBarrierCount = 1;
			//buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
			//buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
			//buffer_memory_barriers[2].buffer = m_pRadixTreeBuffer;
			//buffer_memory_barriers[2].offset = 0;
			//buffer_memory_barriers[2].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

			//::framework::gpu::dependency_info dependency_info;
			//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
			//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
			//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			//dependency_info.image_memory_barrier_count = 0;
			//dependency_info.image_memory_barriers = nullptr;

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;

			m_pConstructRadixTreeCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
			//m_pConstructRadixTreeCommandBuffer->dependency_info(0, 1, &dependency_info);
			m_pConstructRadixTreeCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pConstructRadixTreePipeline);
			//m_pConstructRadixTreeCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pConstructRadixTreeDescriptorTable, 0, nullptr);
			m_pConstructRadixTreeCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pConstructRadixTreePipelineLayout, 0, 1, &m_pConstructRadixTreeDescriptorSet, 0, nullptr);
			m_pConstructRadixTreeCommandBuffer->push_constants(m_pConstructRadixTreePipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
			m_pConstructRadixTreeCommandBuffer->dispatch(((m_numTriangles - 1) + 256 - 1) / 256, 1, 1);
			m_pConstructRadixTreeCommandBuffer->end_command_buffer();

			::framework::gpu::semaphore_submit_info wait_semaphore_info;
			wait_semaphore_info.semaphore = m_pSortMortonCodesCompleteSemaphore;
			wait_semaphore_info.value = 0;
			wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			wait_semaphore_info.device_index = 0;

			::framework::gpu::command_buffer_submit_info command_buffer_info;
			command_buffer_info.command_buffer = m_pConstructRadixTreeCommandBuffer;
			command_buffer_info.device_mask = 0;

			::framework::gpu::semaphore_submit_info signal_semaphore_info;
			signal_semaphore_info.semaphore = m_pConstructRadixTreeCompleteSemaphore;
			signal_semaphore_info.value = 0;
			signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			signal_semaphore_info.device_index = 0;

			::framework::gpu::submit_info submit_info;
			submit_info.flags = ::framework::gpu::submit_flags::none;
			submit_info.wait_semaphore_info_count = 1;
			submit_info.wait_semaphore_infos = &wait_semaphore_info;
			submit_info.command_buffer_info_count = 1;
			submit_info.command_buffer_infos = &command_buffer_info;
			submit_info.signal_semaphore_info_count = 1;
			submit_info.signal_semaphore_infos = &signal_semaphore_info;
			command_queue->submit(1, &submit_info, nullptr);
		}
		// Construct radix tree --------------------------------

	}

	// CalculateNodeBoundingBoxes --------------------------------
	{
		::std::uint32_t pushConstant = m_numTriangles;

		//PipelineBarrier pipelineBarriers[3];
		//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		//pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
		//buffer_memory_barriers[0].pipelineBarrierCount = 1;
		//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].buffer = m_pCounterBuffer;
		//buffer_memory_barriers[0].offset = 0;
		//buffer_memory_barriers[0].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		//buffer_memory_barriers[1].pipelineBarrierCount = 1;
		//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].buffer = m_pCounterBuffer;
		//buffer_memory_barriers[1].offset = 0;
		//buffer_memory_barriers[1].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		//buffer_memory_barriers[2].pipelineBarrierCount = 1;
		//buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		//buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[2].buffer = m_pRadixTreeBuffer;
		//buffer_memory_barriers[2].offset = 0;
		//buffer_memory_barriers[2].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		//::framework::gpu::dependency_info dependency_info;
		//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		//dependency_info.buffer_memory_barrier_count = 1;
		//dependency_info.buffer_memory_barriers = &buffer_memory_barriers[0];
		//dependency_info.image_memory_barrier_count = 0;
		//dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		::framework::gpu::memory_barrier memory_barrier[1];
		memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
		dependency_info.memory_barriers = memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		m_pCalculateNodeBoundingBoxesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
		//m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(0, 1, &dependency_info);
		m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
		m_pCalculateNodeBoundingBoxesCommandBuffer->fill_buffer(m_pCounterBuffer, 0, sizeof(::std::uint32_t) * (m_numTriangles - 1), 0xFFFFFFFF, 0, 0, 0);

		//dependency_info.buffer_memory_barrier_count = 2;
		//dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

		//m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(0, 1, &dependency_info);
		m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
		m_pCalculateNodeBoundingBoxesCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pCalculateNodeBoundingBoxesPipeline);
		//m_pCalculateNodeBoundingBoxesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCalculateNodeBoundingBoxesDescriptorTable, 0, nullptr);
		m_pCalculateNodeBoundingBoxesCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pCalculateNodeBoundingBoxesPipelineLayout, 0, 1, &m_pCalculateNodeBoundingBoxesDescriptorSet, 0, nullptr);
		m_pCalculateNodeBoundingBoxesCommandBuffer->push_constants(m_pCalculateNodeBoundingBoxesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
		m_pCalculateNodeBoundingBoxesCommandBuffer->dispatch((m_numTriangles + 128 - 1) / 128, 1, 1);
		m_pCalculateNodeBoundingBoxesCommandBuffer->end_command_buffer();

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = m_pConstructRadixTreeCompleteSemaphore;
		wait_semaphore_info.value = 0;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		wait_semaphore_info.device_index = 0;

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = m_pCalculateNodeBoundingBoxesCommandBuffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		signal_semaphore_info.value = 0;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		signal_semaphore_info.device_index = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = refit_only ? 0 : 1;
		submit_info.wait_semaphore_infos = &wait_semaphore_info;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 1;
		submit_info.signal_semaphore_infos = &signal_semaphore_info;
		command_queue->submit(1, &submit_info, nullptr);

	}
	// CalculateNodeBoundingBoxes --------------------------------

	// CreateNodes --------------------------------
	{
		::std::uint32_t pushConstant = m_numTriangles;

		//PipelineBarrier pipelineBarriers[2];
		//pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		//pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;

		//pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		//pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		//pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

		//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
		//buffer_memory_barriers[0].pipelineBarrierCount = 1;
		//buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		//buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[0].buffer = m_pRadixTreeBuffer;
		//buffer_memory_barriers[0].offset = 0;
		//buffer_memory_barriers[0].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		//buffer_memory_barriers[1].pipelineBarrierCount = 1;
		//buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		//buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		//buffer_memory_barriers[1].buffer = m_pNodesBuffer;
		//buffer_memory_barriers[1].offset = 0;
		//buffer_memory_barriers[1].size = m_pNodesBuffer->GetBufferCreateInfo()->size;

		//::framework::gpu::dependency_info dependency_info;
		//dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		//dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		//dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		//dependency_info.image_memory_barrier_count = 0;
		//dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::memory_barrier memory_barrier[1];
		memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
		dependency_info.memory_barriers = memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pCreateNodesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
		//m_pCreateNodesCommandBuffer->dependency_info(0, 1, &dependency_info);
		m_pCreateNodesCommandBuffer->pipeline_barrier(&dependency_info);
		m_pCreateNodesCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pCreateNodesPipeline);
		//m_pCreateNodesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCreateNodesDescriptorTable, 0, nullptr);
		m_pCreateNodesCommandBuffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pCreateNodesPipelineLayout, 0, 1, &m_pCreateNodesDescriptorSet, 0, nullptr);
		m_pCreateNodesCommandBuffer->push_constants(m_pCreateNodesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
		m_pCreateNodesCommandBuffer->dispatch((m_numTriangles - 1 + 64 - 1) / 64, 1, 1);
		m_pCreateNodesCommandBuffer->end_command_buffer();

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		wait_semaphore_info.value = 0;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		wait_semaphore_info.device_index = 0;

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = m_pCreateNodesCommandBuffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = m_pCreateNodesCompleteSemaphore;
		signal_semaphore_info.value = 0;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		signal_semaphore_info.device_index = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 1;
		submit_info.wait_semaphore_infos = &wait_semaphore_info;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 1;
		submit_info.signal_semaphore_infos = &signal_semaphore_info;
		command_queue->submit(1, &submit_info, nullptr);
	}
	// CreateNodes --------------------------------

	device->wait_idle();

	if (false)
	{
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(CompactTreeNode) * (m_numTriangles * 2 - 1);

		::framework::gpu::memory_barrier memory_barrier[1];
		memory_barrier[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = static_cast<::std::uint32_t>(::std::size(memory_barrier));
		dependency_info.memory_barriers = memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		m_pCalculateNodeBoundingBoxesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
		m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
		//m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::compute_shader_bit, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
		m_pCalculateNodeBoundingBoxesCommandBuffer->copy_buffer(m_pRadixTreeBuffer, m_pRadixTreeReadbackBuffer, 1, &buffer_copy);
		m_pCalculateNodeBoundingBoxesCommandBuffer->pipeline_barrier(&dependency_info);
		//m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
		m_pCalculateNodeBoundingBoxesCommandBuffer->end_command_buffer();

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = m_pCalculateNodeBoundingBoxesCommandBuffer;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		command_queue->submit(1, &submit_info, nullptr);

		device->wait_idle();

		::std::vector<CompactTreeNode> tree(m_numTriangles * 2 - 1);

		void * pMappedData;
		device->map_memory(m_pRadixTreeReadbackDeviceMemory, 0, sizeof(CompactTreeNode) * (m_numTriangles * 2 - 1), ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(tree.data(), pMappedData, sizeof(CompactTreeNode) * (m_numTriangles * 2 - 1));
		device->unmap_memory(m_pRadixTreeReadbackDeviceMemory);

		float sah = 0;
		float nodeTraversalCost = 1.2f;
		float triangleIntersectionCost = 1.0f;

		int root = 0;

		float totalSA = tree[root].field2.w;
		float sumInternalSA = totalSA;
		float sumLeavesSA = 0.0;
		//::std::ofstream file;
		//file.open("dumpTree.txt");

		//file << m_numTriangles << ::std::endl;
		//for (unsigned int i = 0; i < m_numTriangles * 2 - 1; ++i)
		//{
		//	file << "i: " << i << " Data: " << tree[i].field0.x << " Left: " << tree[i].field0.y <<
		//		" Right: " << tree[i].field0.z << " Parent: " << tree[i].field0.w << " ";
		//	file << "BBoxMin: " << tree[i].field1.x << " " << tree[i].field1.y << " " <<
		//		tree[i].field1.z;
		//	file << " BBoxMax: " << tree[i].field2.x << " " << tree[i].field2.y << " " <<
		//		tree[i].field2.z << ::std::endl;
		//}
		//file.close();

		// Internal nodes
		for (unsigned int i = 0; i < m_numTriangles - 1; ++i)
		{
			if (i != root)
			{
				sumInternalSA += tree[i].field2.w;
				//file << tree[i].field0.y << ", ";
			}
		}

		// Leaves
		for (unsigned int i = m_numTriangles - 1; i < 2 * m_numTriangles - 1; ++i)
		{
			sumLeavesSA += tree[i].field2.w;
		}

		sah = (nodeTraversalCost * sumInternalSA + triangleIntersectionCost * sumLeavesSA) / totalSA;
		printf("LBVH SAH: %f\n", sah);

		printBVH(0, tree.data());
	}

	//refit_only = true;
}