#pragma once

#include "gpu/core.hpp"
#include "lbvh.hpp"

namespace framework
{
	struct NeighborsCreateInfo
	{
		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		::framework::lbvh * pBvh;
		typename ::framework::resource::resource_manager * resource_manager;
	};

	class Neighbors
	{
	public:
		static void allocate(NeighborsCreateInfo const * create_info, Neighbors ** ppNeighbors);

		static void deallocate(Neighbors * ppNeighbors);

		void execute(::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore);

		void integrate(::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore);

	private:
		Neighbors(NeighborsCreateInfo const * create_info);
		~Neighbors();

		void allocate();

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;

		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		::framework::lbvh * m_bvh;
		class ::framework::gpu::buffer * m_pOldVelocityBuffer;
		class ::framework::gpu::device_memory * m_pOldVelocityDeviceMemory;
		class ::framework::gpu::buffer * m_pNewVelocityBuffer;
		class ::framework::gpu::device_memory * m_pNewVelocityDeviceMemory;
		class ::framework::gpu::buffer * m_pNeighborsCounterBuffer;
		class ::framework::gpu::device_memory * m_pNeighborsCounterDeviceMemory;
		class ::framework::gpu::buffer * m_pNeighborsListBuffer;
		class ::framework::gpu::device_memory * m_pNeighborsListDeviceMemory;
		class ::framework::gpu::buffer * m_pNeighborsHeadListBuffer;
		class ::framework::gpu::device_memory * m_pNeighborsHeadListDeviceMemory;
		class ::framework::gpu::buffer * m_pReadbackBuffer;
		class ::framework::gpu::device_memory * m_pReadbackDeviceMemory;
		class ::framework::gpu::buffer * m_pReadbackBuffer1;
		class ::framework::gpu::device_memory * m_pReadbackDeviceMemory1;
		class ::framework::gpu::buffer * m_pReadbackBuffer2;
		class ::framework::gpu::device_memory * m_pReadbackDeviceMemory2;
		class ::framework::gpu::descriptor_pool * descriptor_pool;
		class ::framework::gpu::command_pool * primary_command_pool;
		class ::framework::gpu::command_buffer * m_pNeighborsCommandBuffer;
		class ::framework::gpu::command_buffer * m_pNeighborsCounterServicingCommandBuffer;
		class ::framework::gpu::shader_module * m_pNeighborsShader;
		class ::framework::gpu::descriptor_set_layout * m_pNeighborsDescriptorSetLayout;
		class ::framework::gpu::pipeline_layout * m_pNeighborsPipelineLayout;
		class ::framework::gpu::pipeline * m_pNeighborsPipeline;
		class ::framework::gpu::descriptor_set * m_pNeighborsDescriptorSet;
		class ::framework::gpu::semaphore * m_pNeighborsCompleteSemaphore;
		class ::framework::gpu::command_buffer * m_pCollideCommandBuffer;
		class ::framework::gpu::shader_module * m_pCollideShader;
		class ::framework::gpu::descriptor_set_layout * m_pCollideDescriptorSetLayout;
		class ::framework::gpu::pipeline_layout * m_pCollidePipelineLayout;
		class ::framework::gpu::pipeline * m_pCollidePipeline;
		class ::framework::gpu::descriptor_set * m_pCollideDescriptorSets[2];
		class ::framework::gpu::semaphore * m_pCollideCompleteSemaphore;
		class ::framework::gpu::command_buffer * m_pIntegrateCommandBuffer;
		class ::framework::gpu::shader_module * m_pIntegrateShader;
		class ::framework::gpu::descriptor_set_layout * m_pIntegrateDescriptorSetLayout;
		class ::framework::gpu::pipeline_layout * m_pIntegratePipelineLayout;
		class ::framework::gpu::pipeline * m_pIntegratePipeline;
		class ::framework::gpu::descriptor_set * m_pIntegrateDescriptorSets[2];
		bool m_currentDescriptorTable;
		::framework::gpu::physical_device_memory_properties	physical_device_memory_properties;
		::std::uint32_t const m_neighborsMax;
		typename ::framework::resource::resource_manager * resource_manager;
	};
}