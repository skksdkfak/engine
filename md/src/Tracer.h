#pragma once

#include "gpu/core.hpp"
//#include <components/Transform.h>
//#include <Camera.h>
#include "lbvh.hpp"
#include <cstdint>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace framework
{
	struct TracerCreateInfo
	{
		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		lbvh * pBvh;
		//camera * camera;
		//CTransform * cameraTransform;
		typename ::framework::resource::resource_manager * resource_manager;
	};

	class Tracer
	{
	public:
		static void allocate(TracerCreateInfo const * create_info, Tracer ** ppTracer);

		static void deallocate(Tracer * pTracer);

		void trace(class ::framework::gpu::semaphore * wait_semaphore, const ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, class ::framework::gpu::semaphore * signal_semaphore, float deltaT);

		void resize(::std::uint32_t width, ::std::uint32_t height);

		class ::framework::gpu::sampler * GetGetRayTracedSampler() const;

		class ::framework::gpu::image * GetRayTracedImage() const;

		class ::framework::gpu::image_view * GetRayTracedImageView() const;

	private:
		struct CameraBufferData
		{
			::glm::mat4	viewMatrix;
			::glm::mat4	projMatrix;
			::glm::mat4	viewProjMatrix;
			::glm::mat4	invViewProjMatrix;
			::glm::vec4	position;
			::glm::vec4	direction;
			float		nearClipDistance;
			float		farClipDistance;
			float		nearFarClipDistance;
		};

		struct VariousBufferData
		{
			::glm::vec4	vTime;
			::glm::vec4	vSinTime;
			::glm::vec4	vCosTime;
			::glm::vec4	vDeltaTime;
			::glm::vec4	vScreenResolution;
		};

		Tracer(TracerCreateInfo const * create_info);
		~Tracer();

		void allocate();

		void setupRaySampleImage();

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;

		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		class ::framework::lbvh * m_bvh;
		class ::framework::gpu::descriptor_pool * descriptor_pool;
		//camera * m_camera;
		//CTransform * m_cameraTransform;
		class ::framework::gpu::command_pool * primary_command_pool;
		class ::framework::gpu::command_buffer * m_pPathTracingCommandBuffer;
		class ::framework::gpu::shader_module * m_pPathTracingShader;
		class ::framework::gpu::descriptor_set_layout * m_pPathTracingDescriptorSetLayout;
		class ::framework::gpu::pipeline_layout * m_pPathTracingPipelineLayout;
		class ::framework::gpu::pipeline * m_pPathTracingPipeline;
		class ::framework::gpu::descriptor_set * m_pPathTracingDescriptorSet;
		class ::framework::gpu::sampler * m_pRaySampleSampler;
		class ::framework::gpu::image * m_pRaySampleImage;
		class ::framework::gpu::device_memory * m_pRaySampleDeviceMemory;
		class ::framework::gpu::image_view * m_pRaySampleImageView;
		class ::framework::gpu::buffer * m_pCameraUboBuffer;
		class ::framework::gpu::device_memory * m_pCameraUboDeviceMemory;
		class ::framework::gpu::buffer * m_pVariousUboBuffer;
		class ::framework::gpu::device_memory * m_pVariousUboDeviceMemory;
		::framework::gpu::physical_device_memory_properties	physical_device_memory_properties;
		typename ::framework::resource::resource_manager * resource_manager;
		::std::uint32_t m_samples;
		::std::uint32_t m_bounces;
		::std::uint32_t width;
		::std::uint32_t height;
	};
}