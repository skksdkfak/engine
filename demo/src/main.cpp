#include <glm/glm.hpp>
#include <stdio.h>
#include <iostream>
#include <engine_core.h>
#include <resource/resource_manager.hpp>
#include <Transform.h>
#include <TransformUpdateSystem.h>
#include <Camera.h>
#include <CameraController.h>
#include <CameraControllerSystem.h>
#include <concurrency/thread_pool.h>
#include <PathTracingATRBVH.h>
#include <MeshRendererPathTracingBVH.h>
#include <Mesh.h>

using namespace framework;

#define NUM_OBJECTS 1024
#define COMMAND_BUFFERS_PER_THREAD 1
#define USE_SECONDARY_COMMAND_BUFFERS true

class my_application : public ::framework::application
{
public:
	my_application() :
		width(1280),
		height(720),
		swap_chain_image(0)
	{
	}

	virtual void startup(void) override;
	virtual void cleanup(void) override {};

	virtual void update(float deltaT) override;

private:
	class ::framework::platform::window * window;
	::framework::gpu::instance*					instance;
	::std::uint32_t					physical_device_count;
	::std::uint32_t					m_physicalDeviceIndex;
	::framework::gpu::physical_device**			physical_devices;
	::std::vector<::framework::gpu::queue_family_properties>	queue_family_properties;
	::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
	struct
	{
		::std::uint32_t graphics;
		::std::uint32_t compute;
		::std::uint32_t transfer;
	} queue_family_indices;
	class ::framework::gpu::device*						device;
	class ::framework::gpu::surface*					surface;
	::std::uint32_t					width, height;
	::std::uint32_t					swap_chain_image;
	class ::framework::gpu::swap_chain*					swap_chain;
	::std::vector<class ::framework::gpu::image*>			swap_chain_images;
	class ::framework::gpu::queue*						queue;
	struct thread_data
	{
		struct CommandBufferData
		{
			bool			is_command_buffer_open;
			::std::uint32_t		drawCallCount;
		};

		class ::framework::gpu::command_pool*					command_pool;
		::std::vector<class ::framework::gpu::command_buffer*>		commandBuffers;
		::std::vector<CommandBufferData>	commandBufferData;
	};
	::std::uint32_t					thread_count;
	::std::vector<thread_data>		thread_data[3];
	thread_pool					thread_pool[3];
	class ::framework::gpu::command_pool*				primary_command_pool;
	class ::framework::gpu::command_pool*				m_pSecondaryCommandPool[3];
	class ::framework::gpu::command_buffer*				m_pPrimaryCommandBuffers[3];
	//class CommandBuffer*				m_pSecondaryCommandBuffers[3][NUM_OBJECTS];
	class ::framework::gpu::image_view**					this->image_views;
	class ::framework::gpu::frame_buffer**				frame_buffers;
	class ::framework::gpu::image*						this->depth_stencil_image;
	class ::framework::gpu::device_memory*				this->depth_stencil_device_memory;
	class ::framework::gpu::image_view*					this->depth_stencil_image_view;
	class ::framework::gpu::render_pass*				render_pass;
	class ::framework::gpu::buffer*						ssbo;
	class ::framework::gpu::device_memory*				m_pSsboDeviceMemory;
	class ::framework::gpu::buffer_view*					m_pSSBOView;
	const class ::framework::gpu::image*				image;
	const class ::framework::gpu::shader_module*			pShaderModuleVertex;
	const class ::framework::gpu::shader_module*			pShaderModuleFragment;
	class ::framework::gpu::pipeline_layout*				pipeline_layout;
	class ::framework::gpu::pipeline *					m_pGraphicsPipeline;
	class ::framework::gpu::fence*						d3d12_fence[3];
	class ::framework::gpu::semaphore*					this->render_complete_semaphores[3];
	class ::framework::gpu::semaphore*					present_complete_semaphore;
	class ::framework::gpu::semaphore*					m_pPathTracingCompleteSemaphore;
	class ::framework::gpu::descriptor_set_layout*		m_pDescriptorSetLayout;
	class ::framework::gpu::descriptor_pool*				descriptor_pool;
	class ::framework::gpu::descriptor_set*				m_pDescriptorSet;
	class DescriptorTable*			m_pDescriptorTable;
	class ::framework::gpu::device_memory*				m_pUboDeviceMemory;
	class ::framework::gpu::buffer*						m_ubo;
	void * mappedData;
	class ::framework::gpu::buffer * unsortedData, *unsortedValues, *unsortedDataUpload, *unsortedDataReadback;
	class ::framework::gpu::device_memory * unsortedDataMemory, *unsortedValuesMemory, *unsortedDataUploadMemory, *unsortedDataReadbackMemory;
	struct UboData
	{
		::glm::mat4 projectionMatrix;
		::glm::mat4 modelMatrix;
		::glm::mat4 viewMatrix;
	};
	// For Path tracing
	CPathTracingATRBVH*			pTracer;
	const class ::framework::gpu::shader_module*			m_pFullScreenQuadVertexShaderModule;
	const class ::framework::gpu::shader_module*			m_pFullScreenQuadFragmentShaderModule;
	class ::framework::gpu::pipeline *					m_pFullScreenQuadGraphicsPipeline;
	class ::framework::gpu::render_pass*					m_pFullScreenQuadRenderPass;
	class ::framework::gpu::descriptor_set_layout*		m_pFullScreenQuadDescriptorSetLayout;
	class ::framework::gpu::pipeline_layout*				m_pFullScreenQuadPipelineLayout;
	class ::framework::gpu::descriptor_set*				m_pFullScreenQuadDescriptorSet;
	class DescriptorTable*			m_pFullScreenQuadDescriptorTable;
	CEntity						m_gameObject;
	//::std::shared_ptr<CEntity>	m_gameObject1;
	CEntity						m_mainCamera;
	//CEntity						spotLight;
	//::std::shared_ptr<DirectionalLight>	dirLight;
	//::std::shared_ptr<PointLight>		pointLight;
	::std::uint32_t					m_fps;
	float						delta_time;

};

CREATE_APPLICATION(my_application)

void my_application::startup(void)
{
	::framework::platform::window_create_info windowCreateInfo;
	windowCreateInfo.x = 0;
	windowCreateInfo.y = 0;
	windowCreateInfo.width = width;
	windowCreateInfo.height = height;
	windowCreateInfo.border_width = 0;
	windowCreateInfo.full_screen = false;
	::framework::platform::connection::create_window(&window, &windowCreateInfo);

	//auto& readFile = [](::std::ifstream& is, ::std::size_t& size) ->char*
	//{
	//	size = is.tellg();
	//	is.seekg(0, ::std::ios::beg);
	//	char * data = new char[size];
	//	is.read(data, size);
	//	is.close();
	//	return data;
	//};

	//::std::ifstream is1("1.txt", ::std::ios::in | ::std::ios::ate);
	//::std::ifstream is2("2.txt", ::std::ios::in | ::std::ios::ate);
	//::std::size_t data1_size, data2_size;
	//auto data1 = readFile(is1, data1_size);
	//auto data2 = readFile(is2, data2_size);
	//if (data1_size != data2_size)
	//{
	//	::std::cerr << "error";
	//}
	//for (::std::size_t i = 0; i < data1_size; i++)
	//{
	//	if (data1[i] != data2[i])
	//	{
	//		::std::cerr << "error";
	//	}
	//}

	::framework::resource::g_resource_manager.AddDirectory("../data/shaders/");
	::framework::resource::g_resource_manager.AddDirectory("../data/models/");
	::framework::resource::g_resource_manager.AddDirectory("../data/scenes/");
	::framework::resource::g_resource_manager.AddDirectory("../data/sounds/");
	::framework::resource::g_resource_manager.AddDirectory("../data/textures/");
	::framework::resource::g_resource_manager.AddDirectory("../data/levels/");

	int deviceTypeIdx;
	::std::cout << "Choose class Device type:\n0 - Vulkan class Device;\n1 - DirectX 12 class Device;\n";
	::std::cin >> deviceTypeIdx;
	::framework::gapi_type deviceType;
	switch (deviceTypeIdx)
	{
	case 0:
		deviceType = vulkan;
		::std::cout << "Vulkan class Device was chosen\n";
		break;
	case 1:
		deviceType = d3d12;
		::std::cout << "DirectX 12 class Device was chosen\n";
		break;
	default:
		deviceType = vulkan;
		::std::cout << "Index out of range\n";
		::std::cout << "Vulkan class Device was chosen by default\n";
		break;
	}

	::std::vector<const char*> ppEnabledLayerNames =
	{
#ifdef _DEBUG
		LAYER_STANDARD_VALIDATION_LAYER_NAME
#endif
	};

	::std::vector<const char*> ppEnabledExtensionNames =
	{
#ifdef _DEBUG
		EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
	};

	ApplicationInfo application_info;
	application_info.pApplicationName = "MyTestGame";
	application_info.applicationVersion = 1;
	application_info.pEngineName = "HorhyEngine";
	application_info.engineVersion = 1;
	application_info.apiVersion = 1;

	::framework::gpu::instance_create_info instance_create_info;
	instance_create_info.application_info = &application_info;
	instance_create_info.enabled_layer_count = static_cast<::std::uint32_t>(ppEnabledLayerNames.size());
	instance_create_info.enabled_layer_names = ppEnabledLayerNames.data();
	instance_create_info.enabled_extension_count = static_cast<::std::uint32_t>(ppEnabledExtensionNames.size());
	instance_create_info.enabled_extension_names = ppEnabledExtensionNames.data();
	InstanceManager::create_instance(deviceType, &instance_create_info, &instance);

	instance->enumerate_physical_devices(&physical_device_count, nullptr);
	assert(physical_device_count > 0);
	m_physicalDeviceIndex = 0;
	physical_devices = new ::framework::gpu::physical_device*[physical_device_count];
	instance->enumerate_physical_devices(&physical_device_count, physical_devices);

	::std::uint32_t queue_family_property_count;
    physical_devices[m_physicalDeviceIndex]->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);
	queue_family_properties.resize(queue_family_property_count);
    physical_devices[m_physicalDeviceIndex]->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

    physical_devices[m_physicalDeviceIndex]->get_memory_properties(&physical_device_memory_properties);

	::std::vector<::framework::gpu::device_queue_create_info> queueCreateInfos{};
	const float defaultQueuePriority(0.0f);
	queue_family_indices.graphics = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit);
	::framework::gpu::device_queue_create_info device_queue_create_info;
	device_queue_create_info.queue_family_index = queue_family_indices.graphics;
	device_queue_create_info.queue_count = 1;
	device_queue_create_info.queue_priorities = &defaultQueuePriority;
	queueCreateInfos.push_back(device_queue_create_info);

	queue_family_indices.compute = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit);
	if (queue_family_indices.compute != queue_family_indices.graphics)
	{
		// If compute family index differs, we need an additional class Queue create info for the compute class Queue
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.queue_family_index = queue_family_indices.compute;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		queueCreateInfos.push_back(device_queue_create_info);
	}

	queue_family_indices.transfer = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit);
	if ((queue_family_indices.transfer != queue_family_indices.graphics) && (queue_family_indices.transfer != queue_family_indices.compute))
	{
		// If compute family index differs, we need an additional class Queue create info for the compute class Queue
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.queue_family_index = queue_family_indices.transfer;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		queueCreateInfos.push_back(device_queue_create_info);
	}

	float deviceQueuesPriority = 0.0f;
	::framework::gpu::device_queue_create_info deviceQueueCreateInfos[2];
	deviceQueueCreateInfos[0].queue_family_index = queue_family_indices.graphics;
	deviceQueueCreateInfos[0].queue_count = 1;
	deviceQueueCreateInfos[0].queue_priorities = &deviceQueuesPriority;
	//deviceQueueCreateInfos[1].queue_flags = ::framework::gpu::queue_flags::compute_bit;
	//deviceQueueCreateInfos[1].queue_count = 1;
	//deviceQueueCreateInfos[1].queue_priorities = &deviceQueuesPriority;
	deviceQueueCreateInfos[1].queue_family_index = queue_family_indices.transfer;
	deviceQueueCreateInfos[1].queue_count = 1;
	deviceQueueCreateInfos[1].queue_priorities = &deviceQueuesPriority;

	::framework::gpu::device_create_info deviceCreateInfo;
	deviceCreateInfo.deviceIdx = 0;
	deviceCreateInfo.queue_create_info_count = ::std::size(deviceQueueCreateInfos);
	deviceCreateInfo.queue_create_infos = deviceQueueCreateInfos;

    physical_devices[m_physicalDeviceIndex]->create_device(&deviceCreateInfo, &device);

    device->get_queue(queue_family_indices.graphics, 0, &queue);

	// SYSTEMS SETUP
	SystemManager::add<CTransformUpdateSystem, 0>();
	SystemManager::add<CameraControllerSystem, 1>();
	//SystemManager::add<DeferredShading, 1>(m_camera);
	//SystemManager::add<CPathTracingMaterialSystem, 1>();
	//auto mat_sys = SystemManager::system<IMaterialSystem>();
	//auto mat_sys1 = SystemManager::system<CPathTracingMaterialSystem>();
	//SystemManager::add<CPathTracingATRBVH, 2>(m_camera, 1, 4, 1 << 19, 9, 2);
	//SystemManager::add<CPathTracingBVH, 1>(m_camera, 1000000);
	//SystemManager::add<EngineTuning, 3>();

	m_mainCamera = entity_manager::create<CEntity>();
	auto cameraTransform = m_mainCamera.assign<CTransform>();
	auto camera = m_mainCamera.assign<camera>();
	camera->setZRange(0.01f, 1024.f);
	camera->reverseZ(false);
	camera->inverseY(deviceType == gapi_type::vulkan);
	m_mainCamera.assign<CameraController>(::glm::vec3(0.0f, 1.0f, 0.0f));

	m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "house_2_from_obj.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "sponza.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "head.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "buddha.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "CornellBox-Water.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "suzanne.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "cs_office.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "hairball.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "Scene_1_from_obj.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "minecraft.obj");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "sportsCar.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "teapot.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "House_4.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "lost_empire.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "Chess_Set.fbx");
	//auto obj1 = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "buddha.fbx");
	//auto obj2 = ::framework::resource::g_resource_manager.load_fbx_scene(device, nullptr, nullptr, "lost_empire.fbx");

	//spotLight = EntityManager::create<CEntity>();
	//auto transform = spotLight.assign<CTransform>();
	//transform->SetPosition(Math::Vector3(20, 0, 0));
	//spotLight.assign<SpotLight>();

	//spotLight = SpotLight::Create<SpotLight>();
	//dirLight = DirectionalLight::Create<DirectionalLight>();
	//pointLight = PointLight::Create<PointLight>();

	PathTracingATRBVHCreateInfo pathTracingATRBVHCreateInfo;
	pathTracingATRBVHCreateInfo.camera = camera.get();
	pathTracingATRBVHCreateInfo.cameraTransform = cameraTransform.get();
	pathTracingATRBVHCreateInfo.samples = 1;
	pathTracingATRBVHCreateInfo.bounces = 2;
	pathTracingATRBVHCreateInfo.maxPrimitives = 1 << 21;
	pathTracingATRBVHCreateInfo.treeletSize = 9;
	pathTracingATRBVHCreateInfo.iterations = 2;
	CPathTracingATRBVH::allocate(&pathTracingATRBVHCreateInfo, &pTracer);
	pTracer->configure(device);

	::framework::gpu::surface_create_info surface_create_info;
	surface_create_info.window = window;
	instance->create_surface(&surface_create_info, &surface);

	::framework::gpu::surface_capabilities surfaceCapabilities;
    physical_devices[0]->get_physical_device_surface_capabilities(surface, &surfaceCapabilities);
	width = surfaceCapabilities.current_extent.width;
	height = surfaceCapabilities.current_extent.height;

	::framework::gpu::swap_chain_create_info swap_chain_create_info;
	swap_chain_create_info.surface = surface;
	swap_chain_create_info.min_image_count = 3;
	swap_chain_create_info.image_format = ::framework::gpu::format::B8G8R8A8_UNORM;
	swap_chain_create_info.image_extent = { width, height };
	swap_chain_create_info.image_usage = ::framework::gpu::image_usage_flags::color_attachment_bit;
	swap_chain_create_info.image_sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	swap_chain_create_info.queue_family_index_count = 0;
	swap_chain_create_info.queue_family_indices = nullptr;
	swap_chain_create_info.old_swap_chain = nullptr;
    device->create_swap_chain(&swap_chain_create_info, &swap_chain);

	::std::uint32_t swap_chain_image_count;
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);

	swap_chain_images.resize(swap_chain_image_count);
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, swap_chain_images.data());

	::framework::gpu::attachment_description attachment_descriptions[2];
	// Color attachment
	attachment_descriptions[0].format = swap_chain_images[0]->GetImageCreateInfo()->format;
	attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::present_src_bit;

	// Depth attachment
	attachment_descriptions[1].format = ::framework::gpu::format::d32_sfloat;
	attachment_descriptions[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[1].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[1].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[1].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[1].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::attachment_reference color_reference[1];
	color_reference[0].attachment = 0;
	color_reference[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference depthReference[1];
	depthReference[0].attachment = 1;
	depthReference[0].layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	// Setup subpass dependencies
	// These will add the implicit attachment layout transitionss specified by the attachment descriptions
	// The actual usage layout is preserved through the layout specified in the attachment reference
	// Each subpass dependency will introduce a memory and execution_barrier dependency between the source and dest subpass described by
	// src_stage_mask, dst_stage_mask, src_access_mask, dst_access_mask (and dependency_flags is set)
	// Note: ::framework::gpu::subpass_external is a special constant that refers to all commands executed outside of the actual class RenderPass)
	::framework::gpu::subpass_dependency dependencies[2];

	// First dependency at the start of the class RenderPass
	// Does the transition from final to initial layout
	dependencies[0].src_subpass = ::framework::gpu::subpass_external;									// Producer of the dependency
	dependencies[0].dst_subpass = 0;													// Consumer is our single subpass that will wait for the execution_barrier depdendency
	dependencies[0].src_stage_mask = PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dst_stage_mask = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].src_access_mask = ACCESS_MEMORY_READ_BIT;
	dependencies[0].dst_access_mask = ACCESS_COLOR_ATTACHMENT_READ_BIT | ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependency_flags = DEPENDENCY_BY_REGION_BIT;

	// Second dependency at the end the class RenderPass
	// Does the transition from the initial to the final layout
	dependencies[1].src_subpass = 0;													// Producer of the dependency is our single subpass
	dependencies[1].dst_subpass = ::framework::gpu::subpass_external;									// Consumer are all commands outside of the class RenderPass
	dependencies[1].src_stage_mask = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dst_stage_mask = PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].src_access_mask = ACCESS_COLOR_ATTACHMENT_READ_BIT | ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dst_access_mask = ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependency_flags = DEPENDENCY_BY_REGION_BIT;

	::framework::gpu::subpass_description subpassDescriptions[1];
	subpassDescriptions[0].color_attachment_count = ::std::size(color_reference);
	subpassDescriptions[0].color_attachments = color_reference;
	subpassDescriptions[0].depth_stencil_attachment = depthReference;
	subpassDescriptions[0].input_attachment_count = 0;
	subpassDescriptions[0].input_attachments = nullptr;
	subpassDescriptions[0].resolve_attachments = nullptr;

	::framework::gpu::render_pass_create_info render_pass_create_info;
	render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
	render_pass_create_info.attachments = attachment_descriptions;
	render_pass_create_info.subpass_count = ::std::size(subpassDescriptions);
	render_pass_create_info.subpasses = subpassDescriptions;
	render_pass_create_info.dependency_count = ::std::size(dependencies);
	render_pass_create_info.dependencies = dependencies;
    device->create_render_pass(&render_pass_create_info, &this->render_pass);

	::framework::gpu::image_create_info image_create_info;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = ::framework::gpu::format::d32_sfloat;
	image_create_info.extent = { width, height, 1 };
	image_create_info.mip_levels = 1;
	image_create_info.array_layers = 1;
	image_create_info.samples = sample_count_1_bit;
	image_create_info.tiling = optimal;
	image_create_info.usage = depth_stencil_attachment_bit | transfer_src_bit;
	image_create_info.sharing_mode = SHARING_MODE_EXCLUSIVE;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
    device->create_image(&image_create_info, nullptr, &this->depth_stencil_image);

	::framework::gpu::memory_requirements memory_requirements;
    device->get_image_memory_requirements(this->depth_stencil_image, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	device->allocate_memory(&memory_allocate_info, nullptr, &this->depth_stencil_device_memory);
    device->bind_image_memory(this->depth_stencil_image, this->depth_stencil_device_memory, 0);

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.image = this->depth_stencil_image;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = this->depth_stencil_image->GetImageCreateInfo()->format;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::depth_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
    device->create_image_view(&image_view_create_info, nullptr, &this->depth_stencil_image_view);

	this->image_views = new class ::framework::gpu::image_view*[swap_chain_image_count];
	this->frame_buffers = new class ::framework::gpu::frame_buffer*[swap_chain_image_count];
	for (::std::uint32_t i = 0; i < swap_chain_image_count; ++i)
	{
		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.image = swap_chain_images[i];
		image_view_create_info.view_type = two_dimensional;
		image_view_create_info.format = swap_chain_images[i]->GetImageCreateInfo()->format;
		image_view_create_info.subresource_range.aspect_mask = color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
        device->create_image_view(&image_view_create_info, nullptr, &this->image_views[i]);

		class ::framework::gpu::image_view const * attachments[2];
		attachments[0] = this->image_views[i];
		attachments[1] = this->depth_stencil_image_view;

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.width = width;
		frame_buffer_create_info.height = height;
		frame_buffer_create_info.attachment_count = ::std::size(attachments);
		frame_buffer_create_info.attachments = attachments;
		frame_buffer_create_info.render_pass = this->render_pass;
        device->create_frame_buffer(&frame_buffer_create_info, &this->frame_buffers[i]);
	}

	{
		::std::size_t uboAlignment = 256;
		::std::size_t dynamicAlignment = (sizeof(UboData) / uboAlignment) * uboAlignment + ((sizeof(UboData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = static_cast<::std::uint32_t>(dynamicAlignment) * NUM_OBJECTS;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_ubo);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_ubo, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pUboDeviceMemory);
		device->bind_buffer_memory(m_ubo, m_pUboDeviceMemory, 0);

        device->map_memory(m_pUboDeviceMemory, 0, dynamicAlignment * NUM_OBJECTS, 0, &mappedData);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(float) * 4 * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &ssbo);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(ssbo, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pSsboDeviceMemory);
		device->bind_buffer_memory(ssbo, m_pSsboDeviceMemory, 0);

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.buffer = ssbo;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = sizeof(float) * 4 * 2;
        device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pSSBOView);
	}

	class ::framework::gpu::buffer * pSSBOUploadBuffer;
	class ::framework::gpu::device_memory * pSSBOUploadDeviceMemory;
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(float) * 4 * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &pSSBOUploadBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(pSSBOUploadBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &pSSBOUploadDeviceMemory);
		device->bind_buffer_memory(pSSBOUploadBuffer, pSSBOUploadDeviceMemory, 0);
	}

	{
		float color[2][4] = { { 1.0f, 0.1f, 0.7f, 1.0f }, { 0.1f, 0.89f, 0.31f, 1.0f } };

		void * pMappedData;
        device->map_memory(pSSBOUploadDeviceMemory, 0, sizeof(float) * 4 * 2, 0, &pMappedData);
		::std::memcpy(pMappedData, color, sizeof(float) * 4 * 2);
        device->unmap_memory(pSSBOUploadDeviceMemory);

		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer_begin_info.flags = 0;
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(float) * 4 * 2;
        copyCmd->copy_buffer(pSSBOUploadBuffer, ssbo, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * class ::framework::gpu::fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

		::framework::gpu::submit_info submit_info;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

        device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
        device->destroy_fence(class ::framework::gpu::fence);
	}

	//{
	//	m_pImage = ::framework::resource::g_resource_manager.LoadTexture(device, L"1.dds", true);
	//}

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::vertex_bit;
	descriptor_set_layout_bindings[1].binding = 1;
	descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[1].descriptor_count = 1;
	descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	//descriptor_set_layout_bindings[2].binding = { 2, 0 };
	//descriptor_set_layout_bindings[2].descriptor_count = 1;
	//descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	//descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
    device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pDescriptorSetLayout);

	::framework::gpu::push_constant_range push_constant_ranges;
	push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	push_constant_ranges.binding = 0;
	push_constant_ranges.offset = 0;
	push_constant_ranges.size = sizeof(::glm::vec4);
	push_constant_ranges.hlsl_shader_register = 0;
	push_constant_ranges.hlsl_register_space = 0;
	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.descriptor_set_layouts = &m_pDescriptorSetLayout;
	pipeline_layout_create_info.push_constant_range_count = 1;
	pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
    device->create_pipeline_layout(&pipeline_layout_create_info, &pipeline_layout);

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[4];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_pool_sizes[0].descriptor_count = 1;
	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_pool_sizes[1].descriptor_count = 1;
	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_pool_sizes[2].descriptor_count = 1;
	descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[3].descriptor_count = 1;
	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.max_sets = 2;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
    device->create_descriptor_pool(&descriptor_pool_create_info, &descriptor_pool);

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
	descriptor_set_allocate_info.set_layouts = &m_pDescriptorSetLayout;
    device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pDescriptorSet);

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
	descriptor_buffer_infos[0].buffer = m_ubo;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = m_ubo->GetBufferCreateInfo()->size;
	descriptor_buffer_infos[0].stride = (sizeof(UboData) / 256) * 256 + ((sizeof(UboData) % 256) > 0 ? 256 : 0);
	//descriptor_buffer_infos[1].buffer = ssbo;
	//descriptor_buffer_infos[1].offset = 0;
	//descriptor_buffer_infos[1].range = sizeof(float) * 4;
	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	write_descriptor_sets[0].dst_set = m_pDescriptorSet;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[1].dst_set = m_pDescriptorSet;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	write_descriptor_sets[1].texel_buffer_view = &m_pSSBOView;
    device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

	DescriptorTableCreateInfo descriptorTableCreateInfo;
	descriptorTableCreateInfo.pipeline_layout = pipeline_layout;
	device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pDescriptorTable);

	device->UpdateDescriptorTable(m_pDescriptorTable, 0, 1, &m_pDescriptorSet);

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	switch (deviceType)
	{
	case gapi_type::vulkan:
	{
		pShaderModuleVertex = ::framework::resource::g_resource_manager.load_shader_module(device, "1.vert.spv");
		//class ShaderModule * pShaderModuleGeometry = ::framework::resource::g_resource_manager.load_shader_module(device, "1.geom.spv", true);
		pShaderModuleFragment = ::framework::resource::g_resource_manager.load_shader_module(device, "1.frag.spv");

		pipeline_shader_stage_create_infos[0].module = pShaderModuleVertex;
		pipeline_shader_stage_create_infos[0].name = "main";
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		//pipeline_shader_stage_create_infos[1].module = pShaderModuleGeometry;
		//pipeline_shader_stage_create_infos[1].name = "main";
		//pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::geometry_bit;

		pipeline_shader_stage_create_infos[1].module = pShaderModuleFragment;
		pipeline_shader_stage_create_infos[1].name = "main";
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	}
	break;
	case gapi_type::d3d12:
	{
		pShaderModuleVertex = ::framework::resource::g_resource_manager.load_shader_module(device, "shaders.hlsl");
		pShaderModuleFragment = ::framework::resource::g_resource_manager.load_shader_module(device, "shaders.hlsl");

		pipeline_shader_stage_create_infos[0].module = pShaderModuleVertex;
		pipeline_shader_stage_create_infos[0].name = "VSMain";
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		pipeline_shader_stage_create_infos[1].module = pShaderModuleFragment;
		pipeline_shader_stage_create_infos[1].name = "PSMain";
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	}
	break;
	}

	::framework::gpu::viewport viewport{ 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
	::framework::gpu::rect_2d scissor{ { 0, 0 },{ width, height } };
	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info{};
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::back_bit;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state{};
	pipeline_color_blend_attachment_state.color_write_mask =
		::framework::gpu::color_component_flags::r_bit |
		::framework::gpu::color_component_flags::g_bit |
		::framework::gpu::color_component_flags::b_bit |
		::framework::gpu::color_component_flags::a_bit;
	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info{};
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo{};
	pipelineDepthStencilStateCreateInfo.depth_test_enable = true;
	pipelineDepthStencilStateCreateInfo.depth_write_enable = true;
	pipelineDepthStencilStateCreateInfo.depth_compare_op = m_mainCamera.component<camera>()->getClearDepth() == 0.0f ? ::framework::gpu::compare_op::greater : ::framework::gpu::compare_op::less;
	pipelineDepthStencilStateCreateInfo.depth_bounds_test_enable = false;
	pipelineDepthStencilStateCreateInfo.stencil_test_enable = false;
	pipelineDepthStencilStateCreateInfo.back.fail_op = ::framework::gpu::stencil_op::keep;
	pipelineDepthStencilStateCreateInfo.back.pass_op = ::framework::gpu::stencil_op::keep;
	pipelineDepthStencilStateCreateInfo.back.compare_op = ::framework::gpu::compare_op::always;
	pipelineDepthStencilStateCreateInfo.front = pipelineDepthStencilStateCreateInfo.back;

	::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[5];
	vertex_input_binding_descriptions[0].binding = 0;
	vertex_input_binding_descriptions[0].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[1].binding = 1;
	vertex_input_binding_descriptions[1].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[2].binding = 2;
	vertex_input_binding_descriptions[2].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[2].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[3].binding = 3;
	vertex_input_binding_descriptions[3].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[3].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[4].binding = 4;
	vertex_input_binding_descriptions[4].stride = sizeof(float) * 2;
	vertex_input_binding_descriptions[4].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[5];
	// Location 0 : Position
	vertex_input_attribute_descriptions[0].location = 0;
	vertex_input_attribute_descriptions[0].binding = 0;
	vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[0].offset = 0;
	// Location 1 : Vertex normal
	vertex_input_attribute_descriptions[1].location = 1;
	vertex_input_attribute_descriptions[1].binding = 1;
	vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[1].offset = 0;
	// Location 2 : Vertex binormal
	vertex_input_attribute_descriptions[2].location = 2;
	vertex_input_attribute_descriptions[2].binding = 2;
	vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[2].offset = 0;
	// Location 3 : Vertex tangent
	vertex_input_attribute_descriptions[3].location = 3;
	vertex_input_attribute_descriptions[3].binding = 3;
	vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[3].offset = 0;
	// Location 4 : Texture coordinates
	vertex_input_attribute_descriptions[4].location = 4;
	vertex_input_attribute_descriptions[4].binding = 4;
	vertex_input_attribute_descriptions[4].format = ::framework::gpu::format::r32g32_sfloat;
	vertex_input_attribute_descriptions[4].offset = 0;
	::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
	vertex_input_state.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
	vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
	vertex_input_state.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
	vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info{};
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
	graphics_pipeline_create_info.layout = pipeline_layout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
    device->create_graphics_pipelines(&graphics_pipeline_create_info, 1, &m_pGraphicsPipeline);

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.graphics;
        device->create_command_pool(&command_pool_create_info, &this->primary_command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = this->primary_command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = swap_chain_image_count;
        device->allocate_command_buffers(&command_buffer_allocate_info, m_pPrimaryCommandBuffers);
	}

	// FullScreenQuad
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pFullScreenQuadDescriptorSetLayout);

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pFullScreenQuadPipelineLayout);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pFullScreenQuadDescriptorSet);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pFullScreenQuadPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pFullScreenQuadDescriptorTable);

		::framework::gpu::descriptor_image_info descriptor_image_info;
		descriptor_image_info.sampler = pTracer->GetGetRayTracedSampler();
		descriptor_image_info.image_view = pTracer->GetRayTracedImageView();
		descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
		write_descriptor_sets[0].dst_set = m_pFullScreenQuadDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
		write_descriptor_sets[0].image_info = &descriptor_image_info;
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		device->UpdateDescriptorTable(m_pFullScreenQuadDescriptorTable, 0, 1, &m_pFullScreenQuadDescriptorSet);

		::framework::gpu::attachment_description attachment_descriptions[2];
		// Color attachment
		attachment_descriptions[0].format = swap_chain_images[0]->GetImageCreateInfo()->format;
		attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::present_src_bit;

		// Depth attachment
		attachment_descriptions[1].format = ::framework::gpu::format::d32_sfloat;
		attachment_descriptions[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[1].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[1].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[1].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[1].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		::framework::gpu::attachment_reference color_reference[1];
		color_reference[0].attachment = 0;
		color_reference[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::attachment_reference depthReference[1];
		depthReference[0].attachment = 1;
		depthReference[0].layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		// Setup subpass dependencies
		// These will add the implicit attachment layout transitionss specified by the attachment descriptions
		// The actual usage layout is preserved through the layout specified in the attachment reference
		// Each subpass dependency will introduce a memory and execution_barrier dependency between the source and dest subpass described by
		// src_stage_mask, dst_stage_mask, src_access_mask, dst_access_mask (and dependency_flags is set)
		// Note: ::framework::gpu::subpass_external is a special constant that refers to all commands executed outside of the actual class RenderPass)
		::framework::gpu::subpass_dependency dependencies[2];
		// First dependency at the start of the class RenderPass
		// Does the transition from final to initial layout
		dependencies[0].src_subpass = ::framework::gpu::subpass_external;									// Producer of the dependency
		dependencies[0].dst_subpass = 0;													// Consumer is our single subpass that will wait for the execution_barrier depdendency
		dependencies[0].src_stage_mask = PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[0].dst_stage_mask = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].src_access_mask = ACCESS_MEMORY_READ_BIT;
		dependencies[0].dst_access_mask = ACCESS_COLOR_ATTACHMENT_READ_BIT | ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[0].dependency_flags = DEPENDENCY_BY_REGION_BIT;

		// Second dependency at the end the class RenderPass
		// Does the transition from the initial to the final layout
		dependencies[1].src_subpass = 0;													// Producer of the dependency is our single subpass
		dependencies[1].dst_subpass = ::framework::gpu::subpass_external;									// Consumer are all commands outside of the class RenderPass
		dependencies[1].src_stage_mask = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[1].dst_stage_mask = PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[1].src_access_mask = ACCESS_COLOR_ATTACHMENT_READ_BIT | ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[1].dst_access_mask = ACCESS_MEMORY_READ_BIT;
		dependencies[1].dependency_flags = DEPENDENCY_BY_REGION_BIT;

		::framework::gpu::subpass_description subpassDescriptions[1];
		subpassDescriptions[0].color_attachment_count = ::std::size(color_reference);
		subpassDescriptions[0].color_attachments = color_reference;
		subpassDescriptions[0].depth_stencil_attachment = depthReference;
		subpassDescriptions[0].input_attachment_count = 0;
		subpassDescriptions[0].input_attachments = nullptr;
		subpassDescriptions[0].resolve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
		render_pass_create_info.attachments = attachment_descriptions;
		render_pass_create_info.subpass_count = ::std::size(subpassDescriptions);
		render_pass_create_info.subpasses = subpassDescriptions;
		render_pass_create_info.dependency_count = ::std::size(dependencies);
		render_pass_create_info.dependencies = dependencies;
        device->create_render_pass(&render_pass_create_info, &m_pFullScreenQuadRenderPass);

		m_pFullScreenQuadVertexShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "FullScreenQuad.vert.sdr");
		m_pFullScreenQuadFragmentShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "FullScreenQuad.frag.sdr");

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
		pipeline_shader_stage_create_infos[0].module = m_pFullScreenQuadVertexShaderModule;
		pipeline_shader_stage_create_infos[0].name = "main";
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
		pipeline_shader_stage_create_infos[1].module = m_pFullScreenQuadFragmentShaderModule;
		pipeline_shader_stage_create_infos[1].name = "main";
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::gpu::viewport viewport{ 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
		::framework::gpu::rect_2d scissor{ { 0, 0 },{ width, height } };
		::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
		pipeline_viewport_state_create_info.viewport_count = 1;
		pipeline_viewport_state_create_info.scissors = &scissor;
		pipeline_viewport_state_create_info.scissor_count = 1;
		pipeline_viewport_state_create_info.viewports = &viewport;

		::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
		pipeline_rasterization_state_create_info.depth_clamp_enable = false;
		pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
		pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
		pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::front_bit;
		pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
		pipeline_rasterization_state_create_info.depth_bias_enable = false;
		pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
		pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
		pipeline_rasterization_state_create_info.line_width = 1.0f;

		::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
		pipeline_color_blend_attachment_state.blend_enable = false;
		pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::zero;
		pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
		pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
		pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
		pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
		pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
		pipeline_color_blend_attachment_state.color_write_mask =
			::framework::gpu::color_component_flags::r_bit |
			::framework::gpu::color_component_flags::g_bit |
			::framework::gpu::color_component_flags::b_bit |
			::framework::gpu::color_component_flags::a_bit;

		::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
		pipeline_color_blend_state_create_info.logic_op_enable = false;
		pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
		pipeline_color_blend_state_create_info.attachment_count = 1;
		pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
		pipeline_color_blend_state_create_info.blend_constants[0] = 0;
		pipeline_color_blend_state_create_info.blend_constants[1] = 0;
		pipeline_color_blend_state_create_info.blend_constants[2] = 0;
		pipeline_color_blend_state_create_info.blend_constants[3] = 0;

		::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo;
		pipelineDepthStencilStateCreateInfo.depth_test_enable = false;
		pipelineDepthStencilStateCreateInfo.depth_write_enable = false;
		pipelineDepthStencilStateCreateInfo.depth_compare_op = ::framework::gpu::compare_op::never;
		pipelineDepthStencilStateCreateInfo.depth_bounds_test_enable = false;
		pipelineDepthStencilStateCreateInfo.stencil_test_enable = false;
		pipelineDepthStencilStateCreateInfo.front = {};
		pipelineDepthStencilStateCreateInfo.back = {};
		pipelineDepthStencilStateCreateInfo.back.compare_op = ::framework::gpu::compare_op::always;
		pipelineDepthStencilStateCreateInfo.min_depth_bounds = 0.0f;
		pipelineDepthStencilStateCreateInfo.max_depth_bounds = 0.0f;

		::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
		vertex_input_state.vertex_binding_description_count = 0;
		vertex_input_state.vertex_binding_descriptions = nullptr;
		vertex_input_state.vertex_attribute_description_count = 0;
		vertex_input_state.vertex_attribute_descriptions = nullptr;

		::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
		pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
		pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

		::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info{};
		graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
		graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
		graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
		graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
		graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
		graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
		graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
		graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
		graphics_pipeline_create_info.layout = m_pFullScreenQuadPipelineLayout;
		graphics_pipeline_create_info.render_pass = m_pFullScreenQuadRenderPass;
        device->create_graphics_pipelines(&graphics_pipeline_create_info, 1, &m_pFullScreenQuadGraphicsPipeline);
	}

	const ::std::uint32_t problemSize = 1 << 10;
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedData);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(unsortedData, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataMemory);
		device->bind_buffer_memory(unsortedData, unsortedDataMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedValues);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(unsortedValues, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedValuesMemory);
		device->bind_buffer_memory(unsortedValues, unsortedValuesMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedDataUpload);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(unsortedDataUpload, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataUploadMemory);
		device->bind_buffer_memory(unsortedDataUpload, unsortedDataUploadMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedDataReadback);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(unsortedDataReadback, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataReadbackMemory);
		device->bind_buffer_memory(unsortedDataReadback, unsortedDataReadbackMemory, 0);
	}

	// Upload data to gpu
	{
		::std::vector<::std::uint32_t> data(problemSize);
		for (::std::size_t i = 0; i < data.size(); i++)
			data[i] = problemSize - 1 - static_cast<::std::uint32_t>(i);

		void * pMappedData;
        device->map_memory(unsortedDataUploadMemory, 0, sizeof(::std::uint32_t) * problemSize, 0, &pMappedData);
		::std::memcpy(pMappedData, data.data(), sizeof(::std::uint32_t) * problemSize);
        device->unmap_memory(unsortedDataUploadMemory);

		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = COMMAND_BUFFER_LEVEL_PRIMARY;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer_begin_info.flags = COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
        copyCmd->copy_buffer(unsortedDataUpload, unsortedData, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * class ::framework::gpu::fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

		::framework::gpu::submit_info submit_info;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

        device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
        device->destroy_fence(class ::framework::gpu::fence);
	}

	// Upload values to gpu
	{
		::std::vector<::std::uint32_t> values(problemSize);
		for (::std::size_t i = 0; i < values.size(); i++)
		{
			values[i] = i;
			//values[i] = 16 - static_cast<::std::uint32_t>(i) / 16;
		}

		void * pMappedData;
        device->map_memory(unsortedDataUploadMemory, 0, sizeof(::std::uint32_t) * problemSize, 0, &pMappedData);
		::std::memcpy(pMappedData, values.data(), sizeof(::std::uint32_t) * problemSize);
        device->unmap_memory(unsortedDataUploadMemory);

		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = COMMAND_BUFFER_LEVEL_PRIMARY;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
        copyCmd->copy_buffer(unsortedDataUpload, unsortedValues, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * class ::framework::gpu::fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

		::framework::gpu::submit_info submit_info;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

        device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
        device->destroy_fence(class ::framework::gpu::fence);
	}

    device->destroy_buffer(unsortedDataUpload);
    device->free_memory(unsortedDataUploadMemory);

	//class Queue * computeQueue;
	//class CommandPool * command_pool;
	//class CommandBuffer * computeCmd;

	//{
	//	device->get_queue(::framework::gpu::queue_flags::graphics_bit, 0, &computeQueue);

	//	::framework::gpu::command_pool_create_info command_pool_create_info;
	//	command_pool_create_info.flags = reset_command_buffer_bit;
	//	device->create_command_pool(&command_pool_create_info, &command_pool);

	//	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	//	command_buffer_allocate_info.command_buffer_count = 1;
	//	command_buffer_allocate_info.command_pool = command_pool;
	//	command_buffer_allocate_info.level = primary;
	//	device->allocate_command_buffers(&command_buffer_allocate_info, &computeCmd);
	//}

	//SubmitBatch * transferSubmitInfo;
	//device->CreateSubmitInfo(1, &transferSubmitInfo);
	//transferSubmitInfo->SetCommandBuffers(0, 1, &computeCmd);

	//class ::framework::gpu::fence * class ::framework::gpu::fence;
	//::framework::gpu::fence_create_info fence_create_info;
	//fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	//device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

	/*CRadixSort::Data * pRadixSortData;
	CRadixSort::allocate(device, problemSize, 32, &pRadixSortData);
	CRadixSort::configure(pRadixSortData, unsortedData, unsortedValues, 4);
	CRadixSort::setSize(pRadixSortData, problemSize);
	CRadixSort::execute(pRadixSortData, queue, 0, nullptr, nullptr, 0, nullptr, nullptr);*/

	/*auto m_pRadixSortData = CRadixSort::Allocate(device, problemSize, 32);
	CRadixSort::Execute(m_pRadixSortData, commandQueue, &unsortedData, &unsortedValues, problemSize, 32);
	CRadixSort::Deallocate(m_pRadixSortData);

	computeQueue->SubmitCommandBuffers(transferSubmitInfo, class ::framework::gpu::fence);

	device->wait_for_fences(1, &class ::framework::gpu::fence);

	delete transferSubmitInfo;
	delete class ::framework::gpu::fence;*/

	// Readback from gpu
	/*{
		class Queue * transferQueue;
		device->get_queue(queue_family_indices.graphics, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.graphics;
		class CommandPool * command_pool;
		device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class CommandBuffer * copyCmd;

		device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		copyCmd->begin_command_buffer(&command_buffer_begin_info);

		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
		copyCmd->copy_buffer(unsortedValues, unsortedDataReadback, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * class ::framework::gpu::fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

		::framework::gpu::submit_info submit_info;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

		device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
		device->destroy_fence(class ::framework::gpu::fence);

		::std::vector<::std::uint32_t> data(problemSize);

		void * pMappedData;
		device->map_memory(unsortedDataReadbackMemory, 0, sizeof(::std::uint32_t) * problemSize, 0, &pMappedData);
		::std::memcpy(data.data(), pMappedData, sizeof(::std::uint32_t) * problemSize);
		device->unmap_memory(unsortedDataReadbackMemory);

		for (::std::size_t i = 0; i < data.size(); i++)
			::std::cout << data[i] << ", ";

		device->destroy_buffer(unsortedDataReadback);
	}*/

	//auto entities = EntityManager::entities_with_components<CMesh>();
	//component_handle<CMesh> mesh;
	//for (auto entity : entities)
	//{
	//	entity.unpack<CMesh>(mesh);
	//}

	//for (::std::uint32_t i = 0; i < class ::framework::gpu::swap_chain->GetImageCount(); ++i)
	//{
	//	::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	//	command_buffer_begin_info.flags = simultaneous_use_bit;
	//	m_pPrimaryCommandBuffers[i]->begin_command_buffer(&command_buffer_begin_info);
	//	{
	//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
	//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
	//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
	//		buffer_memory_barriers[0].srcQueueFamily = QUEUE_FAMILY_IGNORED;
	//		buffer_memory_barriers[0].dstQueueFamily = QUEUE_FAMILY_IGNORED;
	//		buffer_memory_barriers[0].buffer = ssbo;
	//		buffer_memory_barriers[0].offset = 0;
	//		buffer_memory_barriers[0].size = ssbo->GetBufferCreateInfo()->ElementSize * ssbo->GetBufferCreateInfo()->NumElements;
	//		m_pPrimaryCommandBuffers[i]->dependency_info(vertex_shader_bit, vertex_shader_bit, 0, 0, nullptr, _countof(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);

	//		::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };
	//		::framework::gpu::clear_value clearValues[2];
	//		clearValues[0].color.float32[0] = 1.0f;
	//		clearValues[0].color.float32[1] = 0.6f;
	//		clearValues[0].color.float32[2] = 0.1f;
	//		clearValues[0].color.float32[3] = 1.0f;
	//		clearValues[1].depth_stencil = { 1.0f, 0 };
	//		::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	//		renderPassBeginInfo.frame_buffer = frame_buffers[i];
	//		renderPassBeginInfo.render_pass = class RenderPass;
	//		renderPassBeginInfo.render_area = render_area;
	//		renderPassBeginInfo.clear_value_count = _countof(clearValues);
	//		renderPassBeginInfo.clear_values = clearValues;
	//		m_pPrimaryCommandBuffers[i]->begin_render_pass(&renderPassBeginInfo, secondary_command_buffers);
	//		{
	//			::framework::gpu::command_buffer_inheritance_info commandBufferInheritanceInfo;
	//			commandBufferInheritanceInfo.frame_buffer = frame_buffers[i];
	//			commandBufferInheritanceInfo.subpass = 0;
	//			commandBufferInheritanceInfo.render_pass = class RenderPass;

	//			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//			command_buffer_begin_info.flags = simultaneous_use_bit | render_pass_continue_bit;
	//			command_buffer_begin_info.inheritance_info = &commandBufferInheritanceInfo;
	//			m_pSecondaryCommandBuffers[i]->begin_command_buffer(&command_buffer_begin_info);

	//			m_pSecondaryCommandBuffers[i]->bind_pipeline(graphicsPipeline);

	//			::std::uint32_t dynamic_offset = 0;

	//			m_pSecondaryCommandBuffers[i]->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pDescriptorTable, 1, &dynamic_offset);

	//			m_pSecondaryCommandBuffers[i]->bind_index_buffer(mesh->indices_buffer, 0, ::framework::gpu::index_type::uint32);

	//			::std::vector<class Buffer*> vertexBuffers = { mesh->m_VBOs[0] };
	//			::std::vector<::framework::gpu::device_size> vertex_offsets = { 0 };
	//			m_pSecondaryCommandBuffers[i]->bind_vertex_buffers(0, 1, vertexBuffers.data(), vertex_offsets.data());

	//			m_pSecondaryCommandBuffers[i]->draw_indexed(mesh->m_subMeshes[0].triangle_count * 3, 1, 0, 0, 0);

	//			m_pSecondaryCommandBuffers[i]->end_command_buffer();

	//			m_pPrimaryCommandBuffers[i]->execute_commands(1, &m_pSecondaryCommandBuffers[i]);
	//		}
	//		m_pPrimaryCommandBuffers[i]->end_render_pass();
	//	}
	//	m_pPrimaryCommandBuffers[i]->end_command_buffer();
	//}

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = 0/*::framework::gpu::fence_create_flags::signaled_bit*/;
	device->create_fence(&fence_create_info, nullptr, &d3d12_fence[0]);
	device->create_fence(&fence_create_info, nullptr, &d3d12_fence[1]);
	device->create_fence(&fence_create_info, nullptr, &d3d12_fence[2]);
	device->create_semaphore(&this->render_complete_semaphores[0]);
	device->create_semaphore(&this->render_complete_semaphores[1]);
	device->create_semaphore(&this->render_complete_semaphores[2]);
	device->create_semaphore(&present_complete_semaphore);
	device->create_semaphore(&m_pPathTracingCompleteSemaphore);

	thread_count = ::std::thread::hardware_concurrency();

	for (::std::uint32_t f = 0; f < 3; f++)
	{
		thread_pool[f].set_thread_count(thread_count);
		thread_data[f].resize(thread_count);

		for (::std::uint32_t i = 0; i < thread_count; i++)
		{
			thread_data * thread = &thread_data[f][i];

			::framework::gpu::command_pool_create_info command_pool_create_info;
			command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
			command_pool_create_info.queue_family_index = queue_family_indices.graphics;
            device->create_command_pool(&command_pool_create_info, &thread->command_pool);

			thread->commandBufferData.resize(COMMAND_BUFFERS_PER_THREAD);
			thread->commandBuffers.resize(COMMAND_BUFFERS_PER_THREAD);

			::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
			command_buffer_allocate_info.command_pool = thread->command_pool;
#if USE_SECONDARY_COMMAND_BUFFERS
			command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::secondary;
#else
			command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
#endif
			command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(thread->commandBuffers.size());
            device->allocate_command_buffers(&command_buffer_allocate_info, thread->commandBuffers.data());
		}
	}
}

void my_application::update(float deltaT)
{
	delta_time += deltaT;
	m_fps++;
	if (delta_time >= 1.0f)
	{
		::std::cout << m_fps << ::std::endl;
		delta_time = 0;
		m_fps = 0;
	}

	const ::std::size_t uboAlignment = 256;
	const ::std::size_t dynamicAlignment = (sizeof(UboData) / uboAlignment) * uboAlignment + ((sizeof(UboData) % uboAlignment) > 0 ? uboAlignment : 0);

	::framework::gpu::acquire_next_image_info acquireNextImageInfo{};
	acquireNextImageInfo.swap_chain = swap_chain;
	acquireNextImageInfo.fence = nullptr;
	acquireNextImageInfo.semaphore = present_complete_semaphore;
	acquireNextImageInfo.timeout = UINT64_MAX;
	device->acquire_next_image(&acquireNextImageInfo, &swap_chain_image);

	pTracer->update(1, &m_pPathTracingCompleteSemaphore);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::simultaneous_use_bit | ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };
	::framework::gpu::clear_value clearValues[2];
	clearValues[0].color.float32[0] = 1.0f;
	clearValues[0].color.float32[1] = 0.6f;
	clearValues[0].color.float32[2] = 0.1f;
	clearValues[0].color.float32[3] = 1.0f;
	clearValues[1].depth_stencil = { 1.0f, 0 };
	::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	renderPassBeginInfo.frame_buffer = this->frame_buffers[swap_chain_image];
	renderPassBeginInfo.render_pass = m_pFullScreenQuadRenderPass;
	renderPassBeginInfo.render_area = render_area;
	renderPassBeginInfo.clear_value_count = ::std::size(clearValues);
	renderPassBeginInfo.clear_values = clearValues;

	::framework::gpu::image_memory_barrier image_memory_barrier;
	image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	image_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
	image_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	image_memory_barrier.image = pTracer->GetRayTracedImage();
	image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

	m_pPrimaryCommandBuffers[swap_chain_image]->begin_command_buffer(&command_buffer_begin_info);
    m_pPrimaryCommandBuffers[swap_chain_image]->pipeline_barrier(::framework::gpu::pipeline_stage_flags::compute_shader_bit, ::framework::gpu::pipeline_stage_flags::fragment_shader_bit, 0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
    m_pPrimaryCommandBuffers[swap_chain_image]->bind_pipeline(m_pFullScreenQuadGraphicsPipeline);
    m_pPrimaryCommandBuffers[swap_chain_image]->begin_render_pass(&renderPassBeginInfo, ::framework::gpu::subpass_contents::inline_);
	m_pPrimaryCommandBuffers[swap_chain_image]->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pFullScreenQuadDescriptorTable, 0, nullptr);
    m_pPrimaryCommandBuffers[swap_chain_image]->draw(3, 1, 0, 0);
    m_pPrimaryCommandBuffers[swap_chain_image]->end_render_pass();
	m_pPrimaryCommandBuffers[swap_chain_image]->end_command_buffer();

	/*::framework::gpu::command_buffer_inheritance_info inheritance_info;
	inheritance_info.frame_buffer = this->frame_buffers[swap_chain_image];
	inheritance_info.subpass = 0;
	inheritance_info.render_pass = render_pass;

	::std::uint32_t threadIndex(0);
	::std::uint32_t entityIndex(0);
	auto entities = EntityManager::entities_with_components<CMeshRendererPathTracingBVH, CTransform>();
	component_handle<CMeshRendererPathTracingBVH> mesh_renderer;
	component_handle<CTransform> transform;
	for (auto entity : entities)
	{
		entity.unpack<CMeshRendererPathTracingBVH, CTransform>(mesh_renderer, transform);
		thread_pool[swap_chain_image].threads[threadIndex]->addJob([=, &inheritance_info, &thread_data = thread_data]
#if USE_SECONDARY_COMMAND_BUFFERS
		{
			ubo_data uboData;
			uboData.projectionMatrix = m_mainCamera.component<Camera>()->getProjectionMatrix();
			uboData.modelMatrix = transform->getWorldMatrix();
			uboData.viewMatrix = m_mainCamera.component<Camera>()->getViewMatrix();

			const ::std::uint32_t dynamic_offset = entityIndex * static_cast<::std::uint32_t>(dynamicAlignment);
			::std::memcpy(static_cast<char*>(mapped_data) + dynamic_offset, &uboData, sizeof(uboData));

			auto& thread = thread_data[swap_chain_image][threadIndex];

			::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
			for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
				drawCallCountMap.insert(::std::make_pair(thread.commandBufferData[i].drawCallCount, i));

			auto& commandBufferIndex = drawCallCountMap.begin()->second;
			auto& commandBufferData = thread.commandBufferData[commandBufferIndex];
			auto& class CommandBuffer = thread.commandBuffers[commandBufferIndex];
			if (!commandBufferData.is_command_buffer_open)
			{
				::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
				command_buffer_begin_info.flags = one_time_submit_bit | render_pass_continue_bit;
				command_buffer_begin_info.inheritance_info = &inheritance_info;
				class CommandBuffer->begin_command_buffer(&command_buffer_begin_info);
				class CommandBuffer->bind_pipeline(m_pGraphicsPipeline);

				commandBufferData.is_command_buffer_open = true;
			}

			class CommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pDescriptorTable, 1, &dynamic_offset);
			float color[4] = { 0.2f, 0.91f, 0.12f, 1.0f };
			class CommandBuffer->push_constants(pipeline_layout, ::framework::gpu::shader_stage_flags::fragment_bit, 0, sizeof(float) * 4, color);

			mesh_renderer->Draw(class CommandBuffer);

			commandBufferData.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
		}
#else
		{
			struct {
				Matrix4 projectionMatrix;
				Matrix4 modelMatrix;
				Matrix4 viewMatrix;
			}uboData;
			uboData.projectionMatrix = m_camera.GetProjMatrix();
			uboData.modelMatrix = transform->GetWorldMatrix();
			uboData.viewMatrix = m_camera.GetViewMatrix();

			const ::std::uint32_t dynamic_offset = entityIndex * static_cast<::std::uint32_t>(dynamicAlignment);
			::std::memcpy(static_cast<char*>(mapped_data) + dynamic_offset, &uboData, sizeof(uboData));

			auto& thread = thread_data[swap_chain_image][threadIndex];

			::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
			for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
				drawCallCountMap.insert(::std::make_pair(thread.commandBufferData[i].drawCallCount, i));

			auto& commandBufferIndex = drawCallCountMap.begin()->second;
			auto& commandBufferData = thread.commandBufferData[commandBufferIndex];
			auto& class CommandBuffer = thread.commandBuffers[commandBufferIndex];
			if (!commandBufferData.is_command_buffer_open)
			{
				::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
				command_buffer_begin_info.flags = one_time_submit_bit;
				::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };
				::framework::gpu::clear_value clearValues[2];
				clearValues[0].color.float32[0] = 0.3f;
				clearValues[0].color.float32[1] = 0.6f;
				clearValues[0].color.float32[2] = 0.1f;
				clearValues[0].color.float32[3] = 1.0f;
				clearValues[1].depth_stencil = { 1.0f, 0 };
				::framework::gpu::render_pass_begin_info renderPassBeginInfo;
				renderPassBeginInfo.frame_buffer = this->frame_buffers[swap_chain_image];
				renderPassBeginInfo.render_pass = render_pass;
				renderPassBeginInfo.render_area = render_area;
				renderPassBeginInfo.clear_value_count = _countof(clearValues);
				renderPassBeginInfo.clear_values = clearValues;

				class CommandBuffer->begin_command_buffer(&command_buffer_begin_info);
				class CommandBuffer->bind_pipeline(m_pGraphicsPipeline);
				class CommandBuffer->BeginRenderPass(&renderPassBeginInfo, secondary_command_buffers);

				commandBufferData.is_command_buffer_open = true;
			}

			class CommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pDescriptorTable, 1, &dynamic_offset);

			mesh_renderer->draw(class CommandBuffer);

			commandBufferData.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
		}
#endif
		);
		threadIndex = (threadIndex + 1) % thread_count;
		entityIndex++;
	}

	::std::vector<class CommandBuffer*> commandBuffers;
	::std::mutex commandBuffersMutex;
	for (::std::uint32_t t = 0; t < thread_count; t++)
	{
		thread_pool[swap_chain_image].threads[t]->addJob([=, &commandBuffers, &commandBuffersMutex]
		{
			auto& thread = thread_data[swap_chain_image][t];
			for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
			{
				if (thread.commandBufferData[i].is_command_buffer_open)
				{
#if !USE_SECONDARY_COMMAND_BUFFERS
					thread.commandBuffers[i]->EndRenderPass();
#endif
					thread.commandBuffers[i]->end_command_buffer();
					thread.commandBufferData[i].is_command_buffer_open = false;
					thread.commandBufferData[i].drawCallCount = 0;
					::std::lock_guard<::std::mutex> lock(commandBuffersMutex);
					commandBuffers.push_back(thread.commandBuffers[i]);
				}
			}
		});
	}

	device->reset_fences(1, &d3d12_fence[swap_chain_image]);

	thread_pool[swap_chain_image].wait();

#if USE_SECONDARY_COMMAND_BUFFERS
	::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	command_buffer_begin_info.flags = one_time_submit_bit;
	m_pPrimaryCommandBuffers[swap_chain_image]->begin_command_buffer(&command_buffer_begin_info);
	{
		::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };
		::framework::gpu::clear_value clearValues[2];
		clearValues[0].color.float32[0] = 0.3f;
		clearValues[0].color.float32[1] = 0.6f;
		clearValues[0].color.float32[2] = 0.1f;
		clearValues[0].color.float32[3] = 1.0f;
		clearValues[1].depth_stencil = { m_mainCamera.component<Camera>()->getClearDepth(), 0 };
		::framework::gpu::render_pass_begin_info renderPassBeginInfo;
		renderPassBeginInfo.frame_buffer = this->frame_buffers[swap_chain_image];
		renderPassBeginInfo.render_pass = render_pass;
		renderPassBeginInfo.render_area = render_area;
		renderPassBeginInfo.clear_value_count = ::std::size(clearValues);
		renderPassBeginInfo.clear_values = clearValues;
		m_pPrimaryCommandBuffers[swap_chain_image]->begin_render_pass(&renderPassBeginInfo, secondary_command_buffers);
		{
			m_pPrimaryCommandBuffers[swap_chain_image]->execute_commands(static_cast<::std::uint32_t>(commandBuffers.size()), commandBuffers.data());
		}
		m_pPrimaryCommandBuffers[swap_chain_image]->end_render_pass();
	}
	m_pPrimaryCommandBuffers[swap_chain_image]->end_command_buffer();
#endif
*/
	class ::framework::gpu::semaphore * wait_semaphores[] = { present_complete_semaphore, m_pPathTracingCompleteSemaphore };
	::framework::gpu::pipeline_stage_flags pWaitDstStageMask[] = { ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit, ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit };

	::framework::gpu::submit_info submit_info;
	submit_info.wait_semaphore_count = 2;
	submit_info.wait_semaphores = wait_semaphores;
	submit_info.wait_dst_stage_mask = pWaitDstStageMask;
#if USE_SECONDARY_COMMAND_BUFFERS
	submit_info.command_buffer_count = 1;
	submit_info.command_buffers = &m_pPrimaryCommandBuffers[swap_chain_image];
#else
	submit_info.command_buffer_count = commandBuffers.size();
	submit_info.command_buffers = commandBuffers.data();
#endif
	submit_info.signal_semaphore_count = 1;
	submit_info.signal_semaphores = &this->render_complete_semaphores[swap_chain_image];
	queue->submit(1, &submit_info, d3d12_fence[swap_chain_image]);

    device->wait_for_fences(1, &d3d12_fence[swap_chain_image], true, UINT64_MAX);
    device->reset_fences(1, &d3d12_fence[swap_chain_image]);

	::framework::gpu::present_info presentInfo;
	presentInfo.wait_semaphore_count = 1;
	presentInfo.wait_semaphores = &this->render_complete_semaphores[swap_chain_image];
	presentInfo.swap_chain_count = 1;
	presentInfo.swap_chains = &swap_chain;
	presentInfo.image_indices = &swap_chain_image;
	presentInfo.results = nullptr;
	queue->present(&presentInfo);
}