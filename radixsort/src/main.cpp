#include "MyRadixSort.h"
#include <algorithm/hybrid_radix_sort.hpp>
#include <algorithm/radix_sort.h>
#include <engine_core.h>
#include <resource/resource_manager.hpp>
#include <gpu/utility.hpp>
#include <glm/glm.hpp>
#include <stdio.h>
#include <iostream>
#include <numeric>
#include <chrono>
#include <random>
#include <array>

using namespace framework;

#define NUM_OBJECTS 1024
#define COMMAND_BUFFERS_PER_THREAD 1
#define USE_SECONDARY_COMMAND_BUFFERS true

class my_application : public ::framework::application
{
public:
	virtual void startup(void) override;
	virtual void cleanup(void) override {};

	virtual void update(float deltaT) override;

private:
	::framework::platform::window * window;
	::framework::gpu::instance * instance;
	::std::uint32_t physical_device_count;
	::framework::gpu::physical_device ** physical_devices;
	::framework::gpu::physical_device * physical_device;
	::framework::gpu::physical_device_features physical_device_features;
	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties;
	::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
	struct
	{
		::std::uint32_t graphics;
		::std::uint32_t compute;
		::std::uint32_t transfer;
	} queue_family_indices;
	class ::framework::gpu::queue * queue;
	::framework::gpu::device * device;
	::framework::gpu::surface * surface;
	::std::uint32_t width, height;
	::std::uint32_t swap_chain_image;
	::framework::gpu::swap_chain * swap_chain;
	::framework::gpu::format swap_chain_format;
	::framework::gpu::color_space swap_chain_color_space;
	::std::vector<::framework::gpu::image *> swap_chain_images;
	::std::vector<::framework::gpu::image_view *> swap_chain_image_views;
	void * mappedData;
	class ::framework::gpu::buffer * unsortedData, * unsortedValues, * unsortedDataUpload, * unsortedDataReadback;
	class ::framework::gpu::device_memory * unsortedDataMemory, * unsortedValuesMemory, * unsortedDataUploadMemory, * unsortedDataReadbackMemory;

};

CREATE_APPLICATION(my_application)

void Comparator(
	::std::uint32_t & keyA,
	::std::uint32_t & keyB,
	::std::uint32_t dir
)
{
	::std::uint32_t t;

	if ((keyA > keyB) == dir)
	{
		t = keyA;
		keyA = keyB;
		keyB = t;
	}
}


void bitonicSortShared()
{
	::std::uint32_t dir = 1;
	::std::uint32_t const arrayLength = 8;
	::std::uint32_t s_key[arrayLength] = { 7, 6, 5, 4, 3, 2, 1, 0 };
	::std::uint32_t dstKey[arrayLength];

	for (::std::uint32_t threadIdx = 0; threadIdx < arrayLength / 2; ++threadIdx)
	{
		for (::std::uint32_t size = 2; size < arrayLength; size <<= 1u)
		{
			//Bitonic merge
			::std::uint32_t ddd = dir ^ ((threadIdx & (size / 2)) != 0);

			for (::std::uint32_t stride = size / 2; stride > 0; stride >>= 1u)
			{
				::std::uint32_t pos = 2 * threadIdx - (threadIdx & (stride - 1));
				Comparator(s_key[pos], s_key[pos + stride], ddd);
			}
		}

		//ddd == dir for the last bitonic merge step
		{
			for (::std::uint32_t stride = arrayLength / 2; stride > 0; stride >>= 1u)
			{
				::std::uint32_t pos = 2 * threadIdx - (threadIdx & (stride - 1));
				Comparator(s_key[pos], s_key[pos + stride], dir);
			}
		}
	}

	::std::cout << "done!" << ::std::endl;
}

constexpr int COUNT = 15;
alignas(32) float g_values[COUNT]; // addr of &g_value is aligned of 32 in this case

::std::uint32_t comparisions = 0;
::std::uint32_t const arrayLength = 8;
::std::uint32_t digits[arrayLength] = { 1, 6, 5, 1, 3, 2, 1, 0 };

void bitonicSort()
{
	for (int p = 0; p < 3; p++)
	{
		for (int q = 0; q <= p; q++)
		{
			int d = 1 << (p - q);

			for (int i = 0; i < 8; i++) {
				bool up = ((i >> p) & 2) == 0;

				comparisions++;
				if ((i & d) == 0 && (digits[i] > digits[i | d]) == up) {
					::std::uint32_t t = digits[i];
					digits[i] = digits[i | d];
					digits[i | d] = t;
				}
			}
		}
	}
}

void my_application::startup(void)
{
	//char * chr = "Hello"; // string literal, located in .text segment - const
	//chr[1] = 'z'; // this will generate error, couse write to text segment

	::std::cout << sizeof(g_values) << ", " << &g_values << ::std::endl;
	bitonicSort();
	bitonicSortShared();
	//int a<::> = < % 1, 2, 3, 4, 5, 6 % >;
	/*::glm::uvec4 b(1, 2, 4, 15);*/
//	::std::uint16_t test[9][32][2] = {};
//    for (int i = 0; i < 9 * 32 * 2; ++i) {
//        ((::std::uint16_t *)test)[i] = i;
//    }
//    ::std::cout << ((::std::uint16_t *)test)[1 + (5 * 2) + (1 * 32 * 2)] << ::std::endl;
	if (0)
	{
#define WG_SIZE 256
#define BITS_PER_PASS 8
#define NUM_BUCKET (1 << BITS_PER_PASS)
#define KPT 18 // number of keys per worker_thread
#define KPB (WG_SIZE * KPT) // number of keys per block
#define LST 9216
#define MBT 3000
		::std::random_device rd;
		::std::mt19937 gen(rd());
		::std::uniform_int_distribution<> dis(0, LST * 2);

		::std::vector<int> ldsBucketHistogram(256);
		::std::generate(ldsBucketHistogram.begin(), ldsBucketHistogram.end(), [&, n = 0]() mutable { return dis(gen); });
		::std::uint32_t totalSum = 0;
		::std::uint32_t bucketSum = 0;
		::std::uint32_t idx, bucketSize;
		::std::uint32_t sum = 0;
		::std::uint32_t mergeSum = 0;
		::std::uint32_t count = 0;
		for (idx = 0; idx < ldsBucketHistogram.size(); ++idx)
		{
			bucketSize = ldsBucketHistogram[idx];
			bucketSum += bucketSize;
			mergeSum += bucketSize;
			if (mergeSum > MBT)
			{
				if (count > 0 && (mergeSum -= bucketSize) > 0)
				{
					totalSum += mergeSum;
					::std::cout << "merged: " << mergeSum << ::std::endl;
				}

				if (bucketSize > MBT)
				{
					if (bucketSize > LST)
					{
						const ::std::uint32_t quotient = bucketSize / KPB;
						const ::std::uint32_t remainder = bucketSize % KPB;

						for (::std::uint32_t i = 0; i < quotient; ++i)
						{
							totalSum += KPB;
							::std::cout << "KPB: " << KPB << ::std::endl;
						}
						if (remainder > 0)
						{
							totalSum += remainder;
							::std::cout << "remainder: " << remainder << ::std::endl;
						}
					}
					else
					{
						totalSum += bucketSize;
						::std::cout << "bucketSize: " << bucketSize << ::std::endl;
					}

					mergeSum = 0;
					count = 0;
				}
				else
				{
					mergeSum = bucketSize;
					count = 1;
				}
			}
			else
			{
				++count;
			}
			sum += bucketSize;
		}

		if (count > 1 && mergeSum > 0 && mergeSum <= MBT)
		{
			totalSum += mergeSum;
			::std::cout << "bucketSize: " << mergeSum << ::std::endl;
		}

		if (totalSum != bucketSum)
		{
			::std::cerr << "SANITY CHECK FAILED! REMAIN:" << bucketSum - totalSum << ::std::endl;
		}
	}

	::framework::resource::g_resource_manager.add_search_directory("../data/kernels/");

	int deviceTypeIdx;
	::std::cout << "Choose class Device type:\n0 - Vulkan class Device;\n1 - DirectX 12 class Device;\n";
	::std::cin >> deviceTypeIdx;
	gapi_type deviceType;
	switch (deviceTypeIdx)
	{
	case 0:
		deviceType = gapi_type::vulkan;
		::std::cout << "Vulkan class Device was chosen\n";
		break;
	case 1:
		deviceType = gapi_type::d3d12;
		::std::cout << "DirectX 12 class Device was chosen\n";
		break;
	default:
		deviceType = gapi_type::vulkan;
		::std::cout << "Index out of range\n";
		::std::cout << "Vulkan class Device was chosen by default\n";
		break;
	}

	::std::vector<const char *> enabled_layer_names =
	{
#ifdef _DEBUG
		LAYER_STANDARD_VALIDATION_LAYER_NAME
#endif
	};

	::std::vector<const char *> enabled_extension_names =
	{
		SURFACE_EXTENSION_NAME,
#if defined(_WIN32)
		WIN32_SURFACE_EXTENSION_NAME,
#elif defined(__linux__)
		XCB_SURFACE_EXTENSION_NAME,
#endif
#ifdef _DEBUG
		DEBUG_UTILS_EXTENSION_NAME,
		VALIDATION_FEATURES_EXTENSION_NAME
#endif
	};

	::framework::platform::window_create_info window_create_info;
	window_create_info.x = 0;
	window_create_info.y = 0;
	window_create_info.width = this->width;
	window_create_info.height = this->height;
	window_create_info.border_width = 0;
	window_create_info.full_screen = false;
	::framework::engine_core::connection->create_window(&window_create_info, &this->window);

	::framework::gpu::application_info application_info;
	application_info.application_name = "MyTestGame";
	application_info.application_version = 1;
	application_info.engine_name = "Engine";
	application_info.engine_version = 1;
	application_info.api_version = 1;

	::framework::gpu::instance_create_info instance_create_info;
	instance_create_info.application_info = &application_info;
	instance_create_info.enabled_layer_count = static_cast<::std::uint32_t>(enabled_layer_names.size());
	instance_create_info.enabled_layer_names = enabled_layer_names.data();
	instance_create_info.enabled_extension_count = static_cast<::std::uint32_t>(enabled_extension_names.size());
	instance_create_info.enabled_extension_names = enabled_extension_names.data();
	::framework::InstanceManager::create_instance(deviceType, &instance_create_info, nullptr, &instance);

	::framework::gpu::surface_create_info surface_create_info;
	surface_create_info.window = this->window;
	instance->create_surface(&surface_create_info, &surface);

	instance->enumerate_physical_devices(&this->physical_device_count, nullptr);
	assert(this->physical_device_count > 0);

	this->physical_devices = new ::framework::gpu::physical_device * [this->physical_device_count];
	instance->enumerate_physical_devices(&this->physical_device_count, this->physical_devices);

	this->physical_device = this->physical_devices[0];

	::std::uint32_t queue_family_property_count;
	this->physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	this->queue_family_properties.resize(queue_family_property_count);
	this->physical_device->get_queue_family_properties(&queue_family_property_count, this->queue_family_properties.data());

	// Iterate over each queue to learn whether it supports presenting:
	// Find a queue with present support
	// Will be used to present the swap chain images to the windowing system
	::std::vector<::framework::gpu::bool32_t> supportsPresent(queue_family_property_count);
	for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
	{
		this->physical_device->get_surface_support(i, surface, &supportsPresent[i]);
	}

	this->physical_device->get_memory_properties(&physical_device_memory_properties);

	const float defaultQueuePriority(0.0f);
	::std::vector<::framework::gpu::device_queue_create_info> device_queue_create_infos;

	this->queue_family_indices.graphics = ::framework::gpu::utility::get_queue_family_index(this->static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit);
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.graphics;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->queue_family_indices.compute = ::framework::gpu::utility::get_queue_family_index(this->static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit);
	if (this->queue_family_indices.compute != this->queue_family_indices.graphics)
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.compute;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->queue_family_indices.transfer = ::framework::gpu::utility::get_queue_family_index(this->static_cast<::std::uint32_t>(queue_family_properties.size()), this->queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit);
	if ((this->queue_family_indices.transfer != this->queue_family_indices.graphics) && (this->queue_family_indices.transfer != this->queue_family_indices.compute))
	{
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.flags = ::framework::gpu::device_queue_create_flags::none;
		device_queue_create_info.queue_family_index = this->queue_family_indices.transfer;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		device_queue_create_infos.push_back(device_queue_create_info);
	}

	this->physical_device->get_features(&this->physical_device_features);

	::framework::gpu::device_create_info device_create_info;
	device_create_info.device_idx = 0;
	device_create_info.queue_create_info_count = static_cast<::std::uint32_t>(device_queue_create_infos.size());
	device_create_info.queue_create_infos = device_queue_create_infos.data();
	device_create_info.enabled_features = &this->physical_device_features;

	assert_framework_gpu_result(this->physical_device->create_device(&device_create_info, nullptr, &this->device));

	device->get_queue(queue_family_indices.graphics, 0, &queue);

	// Doubly Linked List Microbenchmarking
//	if (false)
//	{
//		::std::uint32_t const doublyLinkedListSize = 67108864;
//		::framework::gpu::buffer * pDoublyLinkedListBuffer;
//		::framework::gpu::device_memory * pDoublyLinkedListDeviceMemory;
//		::framework::gpu::buffer * pDoublyLinkedListStagingBuffer;
//		::framework::gpu::device_memory * pDoublyLinkedListStagingDeviceMemory;
//		::framework::gpu::buffer * pReadbackBuffer;
//		::framework::gpu::device_memory * pReadbackDeviceMemory;
//		::framework::gpu::descriptor_set_layout * pDoublyLinkedListDescriptorSetLayout;
//		::framework::gpu::pipeline_layout * pDoublyLinkedListPipelineLayout;
//		::framework::gpu::pipeline * pDoublyLinkedListPipeline;
//		::framework::gpu::descriptor_pool * pDoublyLinkedListDescriptorPool;
//		::framework::gpu::descriptor_set * pDoublyLinkedListDescriptorSet;
//		::framework::gpu::command_pool * pDoublyLinkedListCommandPool;
//		::framework::gpu::command_buffer * pDoublyLinkedListCommandBuffer;
//		::framework::gpu::fence * fence;
//		::framework::gpu::fence_create_info fence_create_info;
//		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
//		device->create_fence(&fence_create_info, nullptr, &fence);
//
//		struct DoublyLinkedListNode
//		{
//			::std::uint32_t prev;
//		};
//
//		{
//			::framework::gpu::buffer_create_info buffer_create_info;
//			buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
//			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
//			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//			buffer_create_info.queue_family_index_count = 0;
//			buffer_create_info.queue_family_indices = nullptr;
//			device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListStagingBuffer);
//
//			::framework::gpu::memory_requirements memory_requirements;
//			device->get_buffer_memory_requirements(pDoublyLinkedListStagingBuffer, &memory_requirements);
//
//			::framework::gpu::memory_allocate_info memory_allocate_info;
//			memory_allocate_info.allocation_size = memory_requirements.size;
//			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
//			memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//			device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListStagingDeviceMemory);
//			device->bind_buffer_memory(pDoublyLinkedListStagingBuffer, pDoublyLinkedListStagingDeviceMemory, 0);
//		}
//		{
//			::framework::gpu::buffer_create_info buffer_create_info;
//			buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
//			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
//			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//			buffer_create_info.queue_family_index_count = 0;
//			buffer_create_info.queue_family_indices = nullptr;
//			device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListBuffer);
//
//			::framework::gpu::memory_requirements memory_requirements;
//			device->get_buffer_memory_requirements(pDoublyLinkedListBuffer, &memory_requirements);
//
//			::framework::gpu::memory_allocate_info memory_allocate_info;
//			memory_allocate_info.allocation_size = memory_requirements.size;
//			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
//			memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//			device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListDeviceMemory);
//			device->bind_buffer_memory(pDoublyLinkedListBuffer, pDoublyLinkedListDeviceMemory, 0);
//		}
//		{
//			::framework::gpu::buffer_create_info buffer_create_info;
//			buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
//			buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
//			buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//			buffer_create_info.queue_family_index_count = 0;
//			buffer_create_info.queue_family_indices = nullptr;
//			device->create_buffer(&buffer_create_info, nullptr, &pReadbackBuffer);
//
//			::framework::gpu::memory_requirements memory_requirements;
//			device->get_buffer_memory_requirements(pReadbackBuffer, &memory_requirements);
//
//			::framework::gpu::memory_allocate_info memory_allocate_info;
//			memory_allocate_info.allocation_size = memory_requirements.size;
//			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
//			memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//			device->allocate_memory(&memory_allocate_info, nullptr, &pReadbackDeviceMemory);
//			device->bind_buffer_memory(pReadbackBuffer, pReadbackDeviceMemory, 0);
//		}
//
//		class ::framework::gpu::shader_module const * pDoublyLinkedListShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "merge_buckets.comp.sdr");
//
//		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
//		pipeline_shader_stage_create_info.module = pDoublyLinkedListShaderModule;
//		pipeline_shader_stage_create_info.name = "main";
//		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
//		// DoublyLinkedList
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &pDoublyLinkedListDescriptorSetLayout);
//
//		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
//		pipeline_layout_create_info.descriptor_set_layout_count = 1;
//		pipeline_layout_create_info.descriptor_set_layouts = &pDoublyLinkedListDescriptorSetLayout;
//		pipeline_layout_create_info.push_constant_range_count = 0;
//		pipeline_layout_create_info.push_constant_ranges = nullptr;
//        device->create_pipeline_layout(&pipeline_layout_create_info, &pDoublyLinkedListPipelineLayout);
//
//		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
//		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
//		compute_pipeline_create_info.layout = pDoublyLinkedListPipelineLayout;
//        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &pDoublyLinkedListPipeline);
//
//		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
//		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_pool_sizes[0].descriptor_count = 1;
//		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
//		descriptor_pool_create_info.max_sets = 1;
//		descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
//		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
//        device->create_descriptor_pool(&descriptor_pool_create_info, &pDoublyLinkedListDescriptorPool);
//
//		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
//		descriptor_set_allocate_info.descriptor_set_count = 1;
//		descriptor_set_allocate_info.descriptor_pool = pDoublyLinkedListDescriptorPool;
//		descriptor_set_allocate_info.set_layouts = &pDoublyLinkedListDescriptorSetLayout;
//        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &pDoublyLinkedListDescriptorSet);
//
//		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
//		descriptor_buffer_infos[0].buffer = pDoublyLinkedListBuffer;
//		descriptor_buffer_infos[0].offset = 0;
//		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
//		descriptor_buffer_infos[0].stride = sizeof(DoublyLinkedListNode);
//
//		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
//		write_descriptor_sets[0].dst_set = pDoublyLinkedListDescriptorSet;
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
//
//		::framework::gpu::command_pool_create_info command_pool_create_info;
//		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
//		command_pool_create_info.queue_family_index = 0;
//        device->create_command_pool(&command_pool_create_info, &pDoublyLinkedListCommandPool);
//
//		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
//		command_buffer_allocate_info.command_pool = pDoublyLinkedListCommandPool;
//		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
//		command_buffer_allocate_info.command_buffer_count = 1;
//        device->allocate_command_buffers(&command_buffer_allocate_info, &pDoublyLinkedListCommandBuffer);
//
//		{
//			::framework::gpu::buffer_copy buffer_copy{};
//			buffer_copy.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
//
//			//TODO
////            ::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
////            buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
////            buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
////            buffer_memory_barriers[0].src_queue_family_index = 0;
////            buffer_memory_barriers[0].dst_queue_family_index = 0;
////            buffer_memory_barriers[0].buffer = pDoublyLinkedListBuffer;
////            buffer_memory_barriers[0].offset = 0;
////            buffer_memory_barriers[0].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
////            buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
////            buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
////            buffer_memory_barriers[1].src_queue_family_index = 0;
////            buffer_memory_barriers[1].dst_queue_family_index = 0;
////            buffer_memory_barriers[1].buffer = pDoublyLinkedListBuffer;
////            buffer_memory_barriers[1].offset = 0;
////            buffer_memory_barriers[1].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
//
//			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
//			command_buffer_begin_info.flags = 0;
//			command_buffer_begin_info.inheritance_info = nullptr;
//			pDoublyLinkedListCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
//            pDoublyLinkedListCommandBuffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, pDoublyLinkedListPipeline);
//			pDoublyLinkedListCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, pDoublyLinkedListDescriptorTable, 0, nullptr);
//            pDoublyLinkedListCommandBuffer->dispatch((doublyLinkedListSize + 256 - 1) / 256, 1, 1);
//			//            pDoublyLinkedListCommandBuffer->dispatch((doublyLinkedListSize + (256 * 256) - 1) / (256 * 256), 1, 1);
//			//            pDoublyLinkedListCommandBuffer->dispatch(1, 1, 1);
//			//            pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_bind_point::compute, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
//			//            pDoublyLinkedListCommandBuffer->copy_buffer(pDoublyLinkedListBuffer, pReadbackBuffer, 1, &buffer_copy);
//			//            pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_bind_point::compute, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
//			pDoublyLinkedListCommandBuffer->end_command_buffer();
//		}
//
//		if (true)
//		{
//			auto tStart = ::std::chrono::high_resolution_clock::now();
//
//			::framework::gpu::submit_info submit_infos[1];
//			submit_infos[0].command_buffer_count = 1;
//			submit_infos[0].command_buffers = &pDoublyLinkedListCommandBuffer;
//			submit_infos[0].wait_semaphore_count = 0;
//			submit_infos[0].wait_semaphores = nullptr;
//			submit_infos[0].wait_dst_stage_mask = nullptr;
//			submit_infos[0].signal_semaphore_count = 0;
//			submit_infos[0].signal_semaphores = nullptr;
//			queue->submit(::std::size(submit_infos), submit_infos, fence);
//
//            device->wait_for_fences(1, &fence, true, UINT_FAST64_MAX);
//
//			auto tEnd = ::std::chrono::high_resolution_clock::now();
//			auto tDiff = ::std::chrono::duration<double, ::std::milli>(tEnd - tStart).count();
//			::std::cout << tDiff << ::std::endl;
//			::std::cout << "done!" << ::std::endl;
//
//			//            ::std::vector<::std::uint32_t> doublyLinkedList(doublyLinkedListSize);
//			//            void * pMappedData;
//			//            device->map_memory(pReadbackDeviceMemory, 0, sizeof(DoublyLinkedListNode) * doublyLinkedListSize, 0, &pMappedData);
//			//            ::std::memcpy(doublyLinkedList.data(), pMappedData, sizeof(DoublyLinkedListNode) * doublyLinkedListSize);
//			//            device->unmap_memory(pReadbackDeviceMemory);
//			//            ::std::cout << "end!" << ::std::endl;
//		}
//        device->destroy_buffer(pDoublyLinkedListStagingBuffer);
//        device->free_memory(pDoublyLinkedListStagingDeviceMemory);
//	}

	//    if (false)
	//    {
	//        ::std::uint32_t const doublyLinkedListSize = 16777216;
	//        class Buffer * pDoublyLinkedListBuffer;
	//        class DeviceMemory * pDoublyLinkedListDeviceMemory;
	//        class Buffer * pDoublyLinkedListStagingBuffer;
	//        class DeviceMemory * pDoublyLinkedListStagingDeviceMemory;
	//        class Buffer * pReadbackBuffer;
	//        class DeviceMemory * pReadbackDeviceMemory;
	//        class DescriptorSetLayout * pDoublyLinkedListDescriptorSetLayout;
	//        class PipelineLayout * pDoublyLinkedListPipelineLayout;
	//        class Pipeline * pDoublyLinkedListPipeline;
	//        class DescriptorPool * pDoublyLinkedListDescriptorPool;
	//        class DescriptorSet * pDoublyLinkedListDescriptorSet;
	//        class DescriptorTable * pDoublyLinkedListDescriptorTable;
	//        class CommandPool * pDoublyLinkedListCommandPool;
	//        class CommandBuffer * pDoublyLinkedListCommandBuffer;
	//
	//        struct DoublyLinkedListNode
	//        {
	//            ::std::uint32_t prev;
	//            ::std::uint32_t next;
	//            ::std::uint32_t val;
	//        };
	//
	//        {
	//            ::framework::gpu::buffer_create_info buffer_create_info;
	//            buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//            buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
	//            buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//            buffer_create_info.queue_family_index_count = 0;
	//            buffer_create_info.queue_family_indices = nullptr;
	//            device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListStagingBuffer);
	//
	//            ::framework::gpu::memory_requirements memory_requirements;
	//            device->get_buffer_memory_requirements(pDoublyLinkedListStagingBuffer, &memory_requirements);
	//
	//            ::framework::gpu::memory_allocate_info memory_allocate_info;
	//            memory_allocate_info.allocation_size = memory_requirements.size;
	//            memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//            memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
	//            device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListStagingDeviceMemory);
	//            device->bind_buffer_memory(pDoublyLinkedListStagingBuffer, pDoublyLinkedListStagingDeviceMemory, 0);
	//        }
	//        {
	//            ::framework::gpu::buffer_create_info buffer_create_info;
	//            buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//            buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
	//            buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//            buffer_create_info.queue_family_index_count = 0;
	//            buffer_create_info.queue_family_indices = nullptr;
	//            device->create_buffer(&buffer_create_info, nullptr, &pDoublyLinkedListBuffer);
	//
	//            ::framework::gpu::memory_requirements memory_requirements;
	//            device->get_buffer_memory_requirements(pDoublyLinkedListBuffer, &memory_requirements);
	//
	//            ::framework::gpu::memory_allocate_info memory_allocate_info;
	//            memory_allocate_info.allocation_size = memory_requirements.size;
	//            memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//            memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	//			memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//            device->allocate_memory(&memory_allocate_info, nullptr, &pDoublyLinkedListDeviceMemory);
	//            device->bind_buffer_memory(pDoublyLinkedListBuffer, pDoublyLinkedListDeviceMemory, 0);
	//        }
	//        {
	//            ::framework::gpu::buffer_create_info buffer_create_info;
	//            buffer_create_info.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//            buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
	//            buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//            buffer_create_info.queue_family_index_count = 0;
	//            buffer_create_info.queue_family_indices = nullptr;
	//            device->create_buffer(&buffer_create_info, nullptr, &pReadbackBuffer);
	//
	//            ::framework::gpu::memory_requirements memory_requirements;
	//            device->get_buffer_memory_requirements(pReadbackBuffer, &memory_requirements);
	//
	//            ::framework::gpu::memory_allocate_info memory_allocate_info;
	//            memory_allocate_info.allocation_size = memory_requirements.size;
	//            memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	//            memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
	//			memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	//            device->allocate_memory(&memory_allocate_info, nullptr, &pReadbackDeviceMemory);
	//            device->bind_buffer_memory(pReadbackBuffer, pReadbackDeviceMemory, 0);
	//        }
	//
	//        class ShaderModule const * pDoublyLinkedListShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "doubly_linked_list.comp.sdr");
	//
	//        ::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	//        pipeline_shader_stage_create_info.module = pDoublyLinkedListShaderModule;
	//        pipeline_shader_stage_create_info.name = "main";
	//        pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
	//
	//        ::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
	//        // DoublyLinkedList
	//        descriptor_set_layout_bindings[0].binding = 0;
	//        descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	//        descriptor_set_layout_bindings[0].descriptor_count = 1;
	//        descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	//        descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
	//
	//        ::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	//        descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	//        descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	//        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &pDoublyLinkedListDescriptorSetLayout);
	//
	//        ::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	//        pipeline_layout_create_info.descriptor_set_layout_count = 1;
	//        pipeline_layout_create_info.descriptor_set_layouts = &pDoublyLinkedListDescriptorSetLayout;
	//        pipeline_layout_create_info.push_constant_range_count = 0;
	//        pipeline_layout_create_info.push_constant_ranges = nullptr;
	//        device->create_pipeline_layout(&pipeline_layout_create_info, &pDoublyLinkedListPipelineLayout);
	//
	//        ::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	//        compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	//        compute_pipeline_create_info.pipeline_layout = pDoublyLinkedListPipelineLayout;
	//        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &pDoublyLinkedListPipeline);
	//
	//        ::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	//        descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_buffer;
	//        descriptor_pool_sizes[0].descriptor_count = 1;
	//        ::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	//        descriptor_pool_create_info.max_sets = 1;
	//        descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	//        descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	//        device->create_descriptor_pool(&descriptor_pool_create_info, &pDoublyLinkedListDescriptorPool);
	//
	//        ::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	//        descriptor_set_allocate_info.descriptor_set_count = 1;
	//        descriptor_set_allocate_info.descriptor_pool = pDoublyLinkedListDescriptorPool;
	//        descriptor_set_allocate_info.set_layouts = &pDoublyLinkedListDescriptorSetLayout;
	//        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &pDoublyLinkedListDescriptorSet);
	//
	//        ::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
	//        descriptor_buffer_infos[0].buffer = pDoublyLinkedListBuffer;
	//        descriptor_buffer_infos[0].offset = 0;
	//        descriptor_buffer_infos[0].range = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
	//        descriptor_buffer_infos[0].stride = sizeof(DoublyLinkedListNode);
	//
	//        ::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	//        write_descriptor_sets[0].dst_set = pDoublyLinkedListDescriptorSet;
	//        write_descriptor_sets[0].dst_binding = 0;
	//        write_descriptor_sets[0].dst_array_element = 0;
	//        write_descriptor_sets[0].descriptor_count = 1;
	//        write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	//        write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	//        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);
	//
	//        DescriptorTableCreateInfo descriptorTableCreateInfo;
	//        descriptorTableCreateInfo.pipeline_layout = pDoublyLinkedListPipelineLayout;
	//        device->CreateDescriptorTable(&descriptorTableCreateInfo, &pDoublyLinkedListDescriptorTable);
	//        device->UpdateDescriptorTable(pDoublyLinkedListDescriptorTable, 0, 1, &pDoublyLinkedListDescriptorSet);
	//
	//
	//        ::framework::gpu::command_pool_create_info command_pool_create_info;
	//        command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	//        command_pool_create_info.queue_family_index = 0;
	//        device->create_command_pool(&command_pool_create_info, &pDoublyLinkedListCommandPool);
	//
	//        ::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	//        command_buffer_allocate_info.command_pool = pDoublyLinkedListCommandPool;
	//        command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	//        command_buffer_allocate_info.command_buffer_count = 1;
	//        device->allocate_command_buffers(&command_buffer_allocate_info, &pDoublyLinkedListCommandBuffer);
	//
	//        {
	//            ::framework::gpu::buffer_copy buffer_copy{};
	//            buffer_copy.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//
	//            ::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//            buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//            buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	//            buffer_memory_barriers[0].src_queue_family_index = 0;
	//            buffer_memory_barriers[0].dst_queue_family_index = 0;
	//            buffer_memory_barriers[0].buffer = pDoublyLinkedListBuffer;
	//            buffer_memory_barriers[0].offset = 0;
	//            buffer_memory_barriers[0].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
	//            buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	//            buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	//            buffer_memory_barriers[1].src_queue_family_index = 0;
	//            buffer_memory_barriers[1].dst_queue_family_index = 0;
	//            buffer_memory_barriers[1].buffer = pDoublyLinkedListBuffer;
	//            buffer_memory_barriers[1].offset = 0;
	//            buffer_memory_barriers[1].size = pDoublyLinkedListBuffer->GetBufferCreateInfo()->size;
	//
	//            ::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//            command_buffer_begin_info.flags = 0;
	//            command_buffer_begin_info.inheritance_info = nullptr;
	//            pDoublyLinkedListCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
	//            pDoublyLinkedListCommandBuffer->bind_pipeline(pDoublyLinkedListPipeline);
	//            pDoublyLinkedListCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, pDoublyLinkedListDescriptorTable, 0, nullptr);
	//            pDoublyLinkedListCommandBuffer->dispatch((doublyLinkedListSize + 128 - 1) / 128, 1, 1);
	//            pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_bind_point::compute, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
	//            pDoublyLinkedListCommandBuffer->copy_buffer(pDoublyLinkedListBuffer, pReadbackBuffer, 1, &buffer_copy);
	//            pDoublyLinkedListCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_bind_point::compute, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
	//            pDoublyLinkedListCommandBuffer->end_command_buffer();
	//        }
	//
	//        if (true)
	//        {
	//            {
	//                ::std::vector<DoublyLinkedListNode> doublyLinkedList(doublyLinkedListSize);
	//                for (::std::uint32_t i = 0; i < doublyLinkedListSize; i++)
	//                {
	//                    doublyLinkedList[i].prev = i == 0 ? doublyLinkedListSize - 1 : i - 1;
	//                    doublyLinkedList[i].next = i == doublyLinkedListSize - 1 ? 0 : i + 1;
	//                }
	//                void * data;
	//                device->map_memory(pDoublyLinkedListStagingDeviceMemory, 0, sizeof(DoublyLinkedListNode) * doublyLinkedListSize, 0, &data);
	//                ::std::memcpy(data, doublyLinkedList.data(), sizeof(DoublyLinkedListNode) * doublyLinkedListSize);
	//                device->unmap_memory(pDoublyLinkedListStagingDeviceMemory);
	//
	//                ::framework::gpu::command_pool_create_info command_pool_create_info;
	//                command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	//                command_pool_create_info.queue_family_index = 0;
	//                class CommandPool * command_pool;
	//                device->create_command_pool(&command_pool_create_info, &command_pool);
	//
	//                ::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	//                command_buffer_allocate_info.command_pool = command_pool;
	//                command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	//                command_buffer_allocate_info.command_buffer_count = 1;
	//                class CommandBuffer * command_buffer;
	//                device->allocate_command_buffers(&command_buffer_allocate_info, &command_buffer);
	//
	//                ::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
	//                command_buffer->begin_command_buffer(&command_buffer_begin_info);
	//                ::framework::gpu::buffer_copy buffer_copy{};
	//                buffer_copy.size = sizeof(DoublyLinkedListNode) * doublyLinkedListSize;
	//                command_buffer->copy_buffer(pDoublyLinkedListStagingBuffer, pDoublyLinkedListBuffer, 1, &buffer_copy);
	//                command_buffer->end_command_buffer();
	//
	//                ::framework::gpu::submit_info submit_info{};
	//                submit_info.command_buffer_count = 1;
	//                submit_info.command_buffers = &command_buffer;
	//
	//                class ::framework::gpu::fence * class ::framework::gpu::fence;
	//                ::framework::gpu::fence_create_info fence_create_info;
	//                fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	//                device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);
	//
	//                queue->submit(1, &submit_info, class ::framework::gpu::fence);
	//
	//                device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
	//
	//                device->free_command_buffers(command_pool, 1, &command_buffer);
	//                device->destroy_command_pool(command_pool);
	//                device->destroy_fence(class ::framework::gpu::fence);
	//            }
	//
	//            ::framework::gpu::submit_info submit_infos[1];
	//            submit_infos[0].command_buffer_count = 1;
	//            submit_infos[0].command_buffers = &pDoublyLinkedListCommandBuffer;
	//            submit_infos[0].wait_semaphore_count = 0;
	//            submit_infos[0].wait_semaphores = nullptr;
	//            submit_infos[0].wait_dst_stage_mask = nullptr;
	//            submit_infos[0].signal_semaphore_count = 0;
	//            submit_infos[0].signal_semaphores = nullptr;
	//            queue->submit(::std::size(submit_infos), submit_infos, nullptr);
	//            auto tStart = ::std::chrono::high_resolution_clock::now();
	//
	//            queue->wait_idle();
	//
	//            auto tEnd = ::std::chrono::high_resolution_clock::now();
	//            auto tDiff = ::std::chrono::duration<double, ::std::milli>(tEnd - tStart).count();
	//            ::std::cout << tDiff << ::std::endl;
	//
	//            ::std::vector<DoublyLinkedListNode> doublyLinkedList(doublyLinkedListSize);
	//            void * pMappedData;
	//            device->map_memory(pReadbackDeviceMemory, 0, sizeof(DoublyLinkedListNode) * doublyLinkedListSize, 0, &pMappedData);
	//            ::std::memcpy(doublyLinkedList.data(), pMappedData, sizeof(DoublyLinkedListNode) * doublyLinkedListSize);
	//            device->unmap_memory(pReadbackDeviceMemory);
	//            ::std::uint32_t curr = 0;
	//            ::std::uint32_t prev = doublyLinkedListSize - 1;
	//            ::std::cout << "start>";
	//            while (curr != doublyLinkedListSize - 1)
	//            {
	//                //::std::cout << curr << ">";
	//                if (doublyLinkedList[curr].prev != prev)
	//                {
	//                    ::std::cerr << "assertion failed at " << curr << " !" << ::std::endl;
	//                }
	//                //assert(doublyLinkedList[curr].prev == prev);
	//                prev = curr;
	//                curr = doublyLinkedList[curr].next;
	//            }
	//            ::std::cout << "end!" << ::std::endl;
	//        }
	//        device->destroy_buffer(pDoublyLinkedListStagingBuffer);
	//        device->free_memory(pDoublyLinkedListStagingDeviceMemory);
	//    }

	const ::std::uint32_t problemSize = 50000000;
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedData);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = unsortedData;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = unsortedData;
		bind_buffer_memory_info.memory = unsortedDataMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedValues);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = unsortedValues;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedValuesMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = unsortedValues;
		bind_buffer_memory_info.memory = unsortedValuesMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedDataUpload);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = unsortedDataUpload;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataUploadMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = unsortedDataUpload;
		bind_buffer_memory_info.memory = unsortedDataUploadMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * problemSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		device->create_buffer(&buffer_create_info, nullptr, &unsortedDataReadback);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = unsortedDataReadback;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		device->allocate_memory(&memory_allocate_info, nullptr, &unsortedDataReadbackMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = unsortedDataReadback;
		bind_buffer_memory_info.memory = unsortedDataReadbackMemory;
		bind_buffer_memory_info.memory_offset = 0;
		device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	// Upload data to gpu
	::std::vector<::std::uint32_t> data(problemSize);
	{
		::std::random_device rd;
		::std::mt19937 gen(rd());
		::std::uniform_int_distribution<> dis(0);
		::std::uniform_int_distribution<> dis255(0, 255);
		for (::std::size_t i = 0; i < data.size(); i++)
		{
			::std::uint8_t radix = 0;
			::std::uint8_t radix1 = dis(gen);
			/*if ((radix == 1 || radix == 2) && i > 256 * 228)
			{
				radix += 2;
			}*/
			//			data[i] = (255 - radix) << 24 | radix << 16 | radix<< 8 | radix;
			//			if (i > 6663)
			//                data[i] = (255 - radix) << 24 | radix << 16 | (255 - radix) << 8 | radix;
			//            else
			//                data[i] = 6663 - i;
			//			if (i < 150)
			//                data[i] = 0 << 24;
			//            else if (i < 305)
			//                data[i] = 1 << 24;
			//            else if (i < 800)
			//                data[i] = 2 << 24;
			//            else
			//                data[i] = 100 << 24;
			data[i] = dis(gen);
			//data[i] = 0x1fffffff;
//			data[i] = problemSize - static_cast<::std::uint32_t>(i);
		}

		void * pMappedData;
        device->map_memory(unsortedDataUploadMemory, 0, sizeof(::std::uint32_t) * problemSize, ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(pMappedData, data.data(), sizeof(::std::uint32_t) * problemSize);
        device->unmap_memory(unsortedDataUploadMemory);

		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		class ::framework::gpu::command_pool * command_pool;

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
        device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		copyCmd->begin_command_buffer(&command_buffer_begin_info);

		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
        copyCmd->copy_buffer(unsortedDataUpload, unsortedData, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = copyCmd;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);
        device->destroy_fence(fence);
	}

	// Upload values to gpu
	if (1)
	{
		::std::vector<::std::uint32_t> values(problemSize);
		for (::std::size_t i = 0; i < values.size(); i++)
		{
			values[i] = data[i];
			//values[i] = i;
			//values[i] = 16 - static_cast<::std::uint32_t>(i) / 16;
		}

		void * pMappedData;
        device->map_memory(unsortedDataUploadMemory, 0, sizeof(::std::uint32_t) * problemSize, ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(pMappedData, values.data(), sizeof(::std::uint32_t) * problemSize);
        device->unmap_memory(unsortedDataUploadMemory);

		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;

		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
        copyCmd->copy_buffer(unsortedDataUpload, unsortedValues, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = copyCmd;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);
        device->destroy_fence(fence);
	}

    device->destroy_buffer(unsortedDataUpload);
    device->free_memory(unsortedDataUploadMemory);

	class ::framework::gpu::fence * fence;
	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	device->create_fence(&fence_create_info, nullptr, &fence);

	//::framework::algorithm::lsd_radix_sort::Data * m_pRadixSortData;
	//::framework::algorithm::lsd_radix_sort::allocate(this->physical_device, device, problemSize, 32, &m_pRadixSortData);
	//::framework::algorithm::lsd_radix_sort::configure(m_pRadixSortData, unsortedData, unsortedValues);

	::framework::algorithm::hybrid_radix_sort * pRadixSortData;
	::framework::algorithm::hybrid_radix_sort_create_info hybridRadixSortCreateInfo;
	hybridRadixSortCreateInfo.maxSize = problemSize;
	hybridRadixSortCreateInfo.maxSortBits = 32;
	hybridRadixSortCreateInfo.physical_device = this->physical_device;
	hybridRadixSortCreateInfo.device = device;
	hybridRadixSortCreateInfo.pInoutKeysBuffer = unsortedData;
	hybridRadixSortCreateInfo.pInoutValuesBuffer = unsortedValues;
	::framework::algorithm::hybrid_radix_sort::allocate(hybridRadixSortCreateInfo, pRadixSortData);

	double tAccum = 0;
	double tMin = ::std::numeric_limits<double>::max();
	::std::uint32_t const nPasses = 1;
	for (::std::uint32_t i = 0; i < nPasses; i++)
	{
		pRadixSortData->setSize(problemSize);
		//::framework::algorithm::lsd_radix_sort::setSize(m_pRadixSortData, problemSize);

		auto tStart = ::std::chrono::high_resolution_clock::now();

		pRadixSortData->execute(queue, nullptr, ::framework::gpu::pipeline_stage_flags::none, nullptr, fence);
		//::framework::algorithm::lsd_radix_sort::execute(m_pRadixSortData, queue, nullptr, ::framework::gpu::pipeline_stage_flags::none, nullptr, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);

		auto tEnd = ::std::chrono::high_resolution_clock::now();
		auto tDiff = ::std::chrono::duration<double, ::std::milli>(tEnd - tStart).count();
		::std::cout << tDiff << ::std::endl;
		tAccum += tDiff;
		tMin = ::std::min(tMin, tDiff);

        device->reset_fences(1, &fence);
	}
	printf("Average: %f, Min: %f\n", tAccum / static_cast<double>(nPasses), tMin);

	// Readback from gpu
	{
		class ::framework::gpu::queue * transferQueue;
        device->get_queue(queue_family_indices.graphics, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.graphics;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;

        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		copyCmd->begin_command_buffer(&command_buffer_begin_info);

		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * problemSize;
        copyCmd->copy_buffer(unsortedValues, unsortedDataReadback, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = copyCmd;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);
        device->destroy_fence(fence);

		::std::vector<::std::uint32_t> data(problemSize);

		void * pMappedData;
        device->map_memory(unsortedDataReadbackMemory, 0, sizeof(::std::uint32_t) * problemSize, ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(data.data(), pMappedData, sizeof(::std::uint32_t) * problemSize);
        device->unmap_memory(unsortedDataReadbackMemory);

		bool const validate = 1;
		if (validate)
		{
			for (::std::size_t i = 1; i < data.size(); i++)
			{
				if (data[i] < data[i - 1])
				{
					::std::cerr << "sort failed" << ::std::endl;
					break;
				}
			}
		}

		bool const print = 0;
		if (print)
		{
			for (::std::size_t i = 0; i < data.size(); i++)
			{
				::std::cout << data[i] << ", ";
			}
		}

        device->destroy_buffer(unsortedDataReadback);
	}

	::std::cout << "Done!\n";
}

void my_application::update(float deltaT)
{
}