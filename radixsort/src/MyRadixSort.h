#pragma once

#include <InstanceManager.h>

namespace framework
{
	struct MyRadixSortUB
	{
		::std::uint32_t m_n;
		::std::uint32_t m_nAligned;
		::std::uint32_t m_nWGs;
		::std::uint32_t m_nBlocksPerWG;
	};

	class CMyRadixSort
	{
	public:
		struct Data
		{
			::std::uint64_t				m_maxSize;
			::std::uint32_t				m_maxSortBits;
			::std::uint64_t				size;
			::std::uint32_t				m_sortBits;
			class ::framework::gpu::device*					device;
			class ::framework::gpu::command_pool*			this->command_pool;
			class ::framework::gpu::command_buffer*			this->command_buffer;
			class ::framework::gpu::fence*					d3d12_fence;
			class ::framework::gpu::shader_module const *		m_pStreamCountShader;
			class ::framework::gpu::shader_module const *		m_pPrefixScanShader;
			class ::framework::gpu::shader_module const *		m_pSortAndScatterShader;
			class ::framework::gpu::pipeline *				m_pStreamCountPso;
			class ::framework::gpu::pipeline *				m_pPrefixScanPso;
			class ::framework::gpu::pipeline *				m_pSortAndScatterPso;
			class ::framework::gpu::pipeline_layout*			m_pStreamCountPipelineLayout;
			class ::framework::gpu::pipeline_layout*			m_pPrefixScanPipelineLayout;
			class ::framework::gpu::pipeline_layout*			m_pSortAndScatterPipelineLayout;
			class ::framework::gpu::descriptor_set_layout*	m_pStreamCountDescriptorSetLayout;
			class ::framework::gpu::descriptor_set_layout*	m_pPrefixScanDescriptorSetLayout;
			class ::framework::gpu::descriptor_set_layout*	m_pSortAndScatterDescriptorSetLayout;
			class ::framework::gpu::descriptor_pool*			descriptor_pool;
			class ::framework::gpu::descriptor_set*			m_pStreamCountDescriptorSets[2];
			class ::framework::gpu::descriptor_set*			m_pPrefixScanDescriptorSet;
			class ::framework::gpu::descriptor_set*			m_pSortAndScatterDescriptorSets[2];
			class DescriptorTable*		m_pStreamCountDescriptorTables[2];
			class DescriptorTable*		m_pPrefixScanDescriptorTable;
			class DescriptorTable*		m_pSortAndScatterDescriptorTables[2];
			class ::framework::gpu::buffer*					m_pWorkBuffer0;
			class ::framework::gpu::device_memory*			m_pWorkBuffer0Memory;
			class ::framework::gpu::buffer*					m_pWorkBuffer1;
			class ::framework::gpu::device_memory*			m_pWorkBuffer1Memory;
			class ::framework::gpu::buffer*					m_pWorkBuffer2;
			class ::framework::gpu::device_memory*			m_pWorkBuffer2Memory;
			class ::framework::gpu::buffer*					m_pWorkBuffer2Debug;
			class ::framework::gpu::device_memory*			m_pWorkBuffer2DebugMemory;
			class ::framework::gpu::buffer*					m_pConstBuffer;
			class ::framework::gpu::device_memory*			m_pConstBufferMemory;
			class ::framework::gpu::buffer *				m_pDispathIndirectCommandsBuffer;
			class ::framework::gpu::device_memory *			m_pDispathIndirectCommandsBufferDeviceMemory;
		};

		static void allocate(class ::framework::gpu::device * device, ::std::uint64_t maxSize, ::std::uint32_t maxSortBits, Data ** data);

		static void deallocate(Data * pData);

		static void configure(Data * data, class ::framework::gpu::buffer * pInoutKeysBuffer, class ::framework::gpu::buffer * pInoutValuesBuffer, ::std::uint32_t sortBits);

		static void setSize(Data * data, ::std::uint64_t size);

		static void execute(Data * data, class ::framework::gpu::queue * command_queue, ::std::uint32_t wait_semaphore_count, class ::framework::gpu::semaphore * const * wait_semaphores, ::framework::gpu::pipeline_stage_flags const * pWaitDstStageMask, ::std::uint32_t signal_semaphore_count, class ::framework::gpu::semaphore * const * signal_semaphores, class ::framework::gpu::fence * fence);

	private:
	    enum : ::std::uint32_t
        {
            WG_SIZE = 256,
            ELEMENTS_PER_WORK_GROUP = 1024,
            ELEMENTS_PER_WORK_ITEM = ELEMENTS_PER_WORK_GROUP / WG_SIZE,
            BITS_PER_PASS = 4,
            //NUM_WGS = 20 * 6, // cypress
            //NUM_WGS = 24 * 6, // cayman
            NUM_WGS = 1024, // nv
        };
	};
}