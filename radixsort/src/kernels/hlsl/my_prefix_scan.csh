#include "globals.hlsl"



typedef uint u32;

#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x
#define GROUP_LDS_BARRIER GroupMemoryBarrierWithGroupSync()
#define GROUP_MEM_FENCE GroupMemoryBarrier()
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define AtomInc(x) InterlockedAdd(x, 1)
#define AtomInc1(x, out) InterlockedAdd(x, 1, out)
#define AtomAdd(x, inc) InterlockedAdd(x, inc)

#define make_uint4 uint4
#define make_uint2 uint2

uint4 SELECT_UINT4(uint4 b, uint4 a, uint4 condition) { return  make_uint4(((condition).x) ? a.x : b.x, ((condition).y) ? a.y : b.y, ((condition).z) ? a.z : b.z, ((condition).w) ? a.w : b.w); }


#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define CHECK_BOUNDARY 1

//#define NV_GPU 1

//	Cypress
#define nPerWI 16
//	Cayman
//#define nPerWI 20

#define GET_GROUP_SIZE WG_SIZE

cbuffer SortCB : register(b0)
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

cbuffer PushConstants : register(b1)
{
	uint m_startBit;
	uint m_currentBitset;
};

RWStructuredBuffer<u32> wHistogram1 : register(u0);

groupshared u32 ldsSortData[WG_SIZE*ELEMENTS_PER_WORK_ITEM + 16];

uint prefixScanVectorEx(inout uint4 data)
{
	u32 sum = 0;
	u32 tmp = data.x;
	data.x = sum;
	sum += tmp;
	tmp = data.y;
	data.y = sum;
	sum += tmp;
	tmp = data.z;
	data.z = sum;
	sum += tmp;
	tmp = data.w;
	data.w = sum;
	sum += tmp;
	return sum;
}

u32 localPrefixSum(u32 pData, uint lIdx, inout uint totalSum, int wgSize /*64 or 128*/)
{
	{	//	Set data
		ldsSortData[lIdx] = 0;
		ldsSortData[lIdx + wgSize] = pData;
	}

	GROUP_LDS_BARRIER;

	{	//	Prefix sum
		int idx = 2 * lIdx + (wgSize + 1);
#if defined(USE_2LEVEL_REDUCE)
		if (lIdx < 64)
		{
			u32 u0, u1, u2;
			u0 = ldsSortData[idx - 3];
			u1 = ldsSortData[idx - 2];
			u2 = ldsSortData[idx - 1];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 12];
			u1 = ldsSortData[idx - 8];
			u2 = ldsSortData[idx - 4];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 48];
			u1 = ldsSortData[idx - 32];
			u2 = ldsSortData[idx - 16];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[idx] += ldsSortData[idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[idx - 1] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
		}
#else
		if (lIdx < 64)
		{
			ldsSortData[idx] += ldsSortData[idx - 1];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 4];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 8];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 16];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 32];
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[idx] += ldsSortData[idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[idx - 1] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
		}
#endif
	}

	GROUP_LDS_BARRIER;

	totalSum = ldsSortData[wgSize * 2 - 1];
	u32 addValue = ldsSortData[lIdx + wgSize - 1];
	return addValue;
}

//__attribute__((reqd_work_group_size(128,1,1)))
uint4 localPrefixSum128V(uint4 pData, uint lIdx, inout uint totalSum)
{
	u32 s4 = prefixScanVectorEx(pData);
	u32 rank = localPrefixSum(s4, lIdx, totalSum, 128);
	return pData + make_uint4(rank, rank, rank, rank);
}

//__attribute__((reqd_work_group_size(64,1,1)))
uint4 localPrefixSum64V(uint4 pData, uint lIdx, inout uint totalSum)
{
	u32 s4 = prefixScanVectorEx(pData);
	u32 rank = localPrefixSum(s4, lIdx, totalSum, 64);
	return pData + make_uint4(rank, rank, rank, rank);
}

#define nPerLane (nPerWI/4)

//	NUM_BUCKET*nWGs < 128*nPerWI
[numthreads(128, 1, 1)]
void main(DEFAULT_ARGS)
{
	u32 lIdx = GET_LOCAL_IDX;
	u32 wgIdx = GET_GROUP_IDX;
	const int nWGs = m_nWGs;

	u32 data[nPerWI];
	for (int i = 0; i<nPerWI; i++)
	{
		data[i] = 0;
		if ((nPerWI*lIdx + i) < NUM_BUCKET*nWGs)
			data[i] = wHistogram1[nPerWI*lIdx + i];
	}

	uint4 myData = make_uint4(0, 0, 0, 0);

	for (int i = 0; i<nPerLane; i++)
	{
		myData.x += data[nPerLane * 0 + i];
		myData.y += data[nPerLane * 1 + i];
		myData.z += data[nPerLane * 2 + i];
		myData.w += data[nPerLane * 3 + i];
	}

	uint totalSum;
	uint4 scanned = localPrefixSum128V(myData, lIdx, totalSum);

	//	for(int j=0; j<4; j++) //	somehow it introduces a lot of branches
	{	int j = 0;
	u32 sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		u32 tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 1;
	u32 sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		u32 tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 2;
	u32 sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		u32 tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 3;
	u32 sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		u32 tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}

	for (int i = 0; i<nPerLane; i++)
	{
		data[nPerLane * 0 + i] += scanned.x;
		data[nPerLane * 1 + i] += scanned.y;
		data[nPerLane * 2 + i] += scanned.z;
		data[nPerLane * 3 + i] += scanned.w;
	}

	for (int i = 0; i<nPerWI; i++)
	{
		wHistogram1[nPerWI*lIdx + i] = data[i];
	}
}