#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

struct CompactSphere
{
	vec4 field0; // pos.xyz, r
};

layout (std430, binding = 1) buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 2) restrict buffer Counter
{
    uint trianglesCounter;
};

layout (location = 0) in vec3 inWorldPos[];
//layout (location = 1) in vec3 inNormal[];
//layout (location = 2) in vec3 inBinormal[];
//layout (location = 3) in vec3 inTangent[];
//layout (location = 4) in vec2 inUV[];

struct Bound
{
	vec3 min;
	float pad0;
	vec3 max;
	float pad1;
};

vec3 ClosestPtPointTriangle(vec3 p, vec3 a, vec3 b, vec3 c)
{
	// Check if P in vertex region outside A 
	vec3 ab = b - a;
	vec3 ac = c - a;
	vec3 ap = p - a;
	float d1 = dot(ab, ap);
	float d2 = dot(ac, ap);
	if (d1 <= 0.0f && d2 <= 0.0f)
		return a; // barycentric coordinates (1,0,0)
	// Check if P in vertex region outside B 
	vec3 bp = p - b;
	float d3 = dot(ab, bp);
	float d4 = dot(ac, bp);
	if (d3 >= 0.0f && d4 <= d3)
		return b; // barycentric coordinates (0,1,0)
	// Check if P in edge region of AB, if so return projection of P onto AB 
	float vc = d1*d4 - d3*d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
	{
		float v = d1 / (d1 -d3);
		return a + v * ab; // barycentric coordinates (1-v,v,0)
	}

	// Check if P in vertex region outside C 
	vec3 cp = p - c;
	float d5 = dot(ab, cp);
	float d6 = dot(ac, cp);
	if (d6 >= 0.0f && d5 <= d6)
		return c; // barycentric coordinates (0,0,1)
	// Check if P in edge region of AC, if so return projection of P onto AC 
	float vb = d5*d2 - d1*d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
	{
		float w = d2 / (d2 -d6);
		return a + w * ac; // barycentric coordinates (1-w,0,w)
	}
	// Check if P in edge region of BC, if so return projection of P onto BC 
	float va = d3*d6 - d5*d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
	{
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + w * (c - b); // barycentric coordinates (0,1-w,w)
	}
	// P inside face region. Compute Q through its barycentric coordinates (u,v,w) 
	float denom = 1.0f / (va + vb + vc);
	float v = vb *denom;
	float w = vc *denom;
	return a + ab * v + ac * w; // = u*a + v*b + w*c, u = va *denom = 1.0f-v-w
}

bool TestSphereTriangle(CompactSphere s, vec3 a, vec3 b, vec3 c, out vec3 p)
{
	p = ClosestPtPointTriangle(s.field0.xyz, a, b, c);
	// Sphere and triangle intersect if the (squared) distance from sphere 
	// center to point p is less than the (squared) sphere radius 
	vec3 v = p - s.field0.xyz;
	return dot(v, v) <= s.field0.w * s.field0.w;
}

void main(void)
{
	const float size = 0.025f;
	Bound bound;
	bound.min = vec3(FLT_MAX);
	bound.max = vec3(FLT_MIN);
	#pragma unroll 3
	for (uint vIdx = 0; vIdx < 3; vIdx++)
	{
		bound.min = min(bound.min, inWorldPos[vIdx]);
		bound.max = max(bound.max, inWorldPos[vIdx]);
	}

	for (float x = bound.min.x - size; x < bound.max.x + size; x += size)
	{
		for (float y = bound.min.y - size; y < bound.max.y + size; y += size)
		{
			for (float z = bound.min.z - size; z < bound.max.z + size; z += size)
			{
				vec3 p;
				const CompactSphere sphere = { vec4(x, y, z, size) };
				if (TestSphereTriangle(sphere, inWorldPos[0], inWorldPos[1], inWorldPos[2], p))
				{
					const uint primitiveID = atomicAdd(trianglesCounter, 1);	
					spheres[primitiveID] = sphere;
				}
			}
		}
	}
}