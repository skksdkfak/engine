//#include "resource/resource_manager.hpp"
//#include "InstanceManager.h"
//#include "gpu/utility.hpp"
//#include "MyRadixSort.h"
//
//using namespace framework;
//
//void CMyRadixSort::allocate(class Device * device, ::std::uint64_t maxSize, ::std::uint32_t maxSortBits, Data ** data)
//{
//	Data * data = new Data;
//	data->m_maxSize = maxSize;
//	data->m_maxSortBits = maxSortBits;
//	data->device = device;
//	data->m_pStreamCountShader = ::framework::resource::g_resource_manager.load_shader_module(device, "my_stream_count.sdr");
//	data->m_pPrefixScanShader = ::framework::resource::g_resource_manager.load_shader_module(device, "my_prefix_scan.sdr");
//	data->m_pSortAndScatterShader = ::framework::resource::g_resource_manager.load_shader_module(device, "my_sort_and_scatter.sdr");
//
//	::framework::gpu::physical_device_memory_properties memoryProperties;
//	device->GetPhysicalDevice()->get_memory_properties(&memoryProperties);
//
//	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
//	pipeline_shader_stage_create_infos[0].module = data->m_pStreamCountShader;
//	pipeline_shader_stage_create_infos[0].name = "main";
//	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//	pipeline_shader_stage_create_infos[1].module = data->m_pPrefixScanShader;
//	pipeline_shader_stage_create_infos[1].name = "main";
//	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//	pipeline_shader_stage_create_infos[2].module = data->m_pSortAndScatterShader;
//	pipeline_shader_stage_create_infos[2].name = "main";
//	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//
//	// StreamCount class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
//		// UBO consts
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// srcKeys
//		descriptor_set_layout_bindings[1].binding = 1;
//		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[1].descriptor_count = 1;
//		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// histogram
//		descriptor_set_layout_bindings[2].binding = 2;
//		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[2].descriptor_count = 1;
//		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pStreamCountDescriptorSetLayout);
//	}
//
//	// PrefixScan class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
//		// UBO consts
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// histogram
//		descriptor_set_layout_bindings[1].binding = 1;
//		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[1].descriptor_count = 1;
//		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pPrefixScanDescriptorSetLayout);
//	}
//
//	// SortAndScatter class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
//		// UBO consts
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// histogram
//		descriptor_set_layout_bindings[1].binding = 1;
//		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[1].descriptor_count = 1;
//		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// srcKeys
//		descriptor_set_layout_bindings[2].binding = 2;
//		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[2].descriptor_count = 1;
//		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// srcValues
//		descriptor_set_layout_bindings[3].binding = 3;
//		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
//		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[3].descriptor_count = 1;
//		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// dstKeys
//		descriptor_set_layout_bindings[4].binding = 4;
//		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[4].descriptor_count = 1;
//		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// dstValues
//		descriptor_set_layout_bindings[5].binding = 5;
//		descriptor_set_layout_bindings[5].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[5].descriptor_count = 1;
//		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pSortAndScatterDescriptorSetLayout);
//	}
//
//	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[3];
//	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
//	descriptor_pool_sizes[0].descriptor_count = 5;
//	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_buffer;
//	descriptor_pool_sizes[1].descriptor_count = 8;
//	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_buffer;
//	descriptor_pool_sizes[2].descriptor_count = 7;
//	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
//	descriptor_pool_create_info.max_sets = 5;
//	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
//	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
//	device->create_descriptor_pool(&descriptor_pool_create_info, &data->descriptor_pool);
//
//	class DescriptorSetLayout * set_layouts[] =
//	{
//		data->m_pStreamCountDescriptorSetLayout,
//		data->m_pPrefixScanDescriptorSetLayout,
//		data->m_pSortAndScatterDescriptorSetLayout
//	};
//	class DescriptorSet * descriptor_sets[3];
//
//	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
//	descriptor_set_allocate_info.descriptor_pool = data->descriptor_pool;
//	descriptor_set_allocate_info.descriptor_set_count = ::std::size(set_layouts);
//	descriptor_set_allocate_info.set_layouts = set_layouts;
//	device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);
//
//	data->m_pStreamCountDescriptorSets[0] = descriptor_sets[0];
//	data->m_pPrefixScanDescriptorSet = descriptor_sets[1];
//	data->m_pSortAndScatterDescriptorSets[0] = descriptor_sets[2];
//
//	set_layouts[0] = data->m_pStreamCountDescriptorSetLayout;
//	set_layouts[1] = data->m_pSortAndScatterDescriptorSetLayout;
//
//	descriptor_set_allocate_info.descriptor_set_count = 2;
//	device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);
//
//	data->m_pStreamCountDescriptorSets[1] = descriptor_sets[0];
//	data->m_pSortAndScatterDescriptorSets[1] = descriptor_sets[1];
//
//	::framework::gpu::push_constant_range push_constant_ranges;
//	push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//	push_constant_ranges.binding = 0;
//	push_constant_ranges.offset = 0;
//	push_constant_ranges.size = sizeof(::std::uint32_t);
//	push_constant_ranges.hlsl_shader_register = 1;
//	push_constant_ranges.hlsl_register_space = 0;
//	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
//	pipeline_layout_create_info.descriptor_set_layout_count = 1;
//	pipeline_layout_create_info.push_constant_range_count = 1;
//	pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
//
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pStreamCountDescriptorSetLayout;
//	device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pStreamCountPipelineLayout);
//
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pPrefixScanDescriptorSetLayout;
//	device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pPrefixScanPipelineLayout);
//
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pSortAndScatterDescriptorSetLayout;
//	device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pSortAndScatterPipelineLayout);
//
//	class Pipeline * pPipelines[3];
//
//	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[3];
//	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
//	compute_pipeline_create_infos[0].pipeline_layout = data->m_pStreamCountPipelineLayout;
//	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
//	compute_pipeline_create_infos[1].pipeline_layout = data->m_pPrefixScanPipelineLayout;
//	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
//	compute_pipeline_create_infos[2].pipeline_layout = data->m_pSortAndScatterPipelineLayout;
//	device->create_compute_pipelines(compute_pipeline_create_infos, 3, pPipelines);
//
//	data->m_pStreamCountPso = pPipelines[0];
//	data->m_pPrefixScanPso = pPipelines[1];
//	data->m_pSortAndScatterPso = pPipelines[2];
//
//	{
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = sizeof(::std::uint32_t) * maxSize;
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer0);
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer1);
//
//		{
//			::framework::gpu::memory_requirements memory_requirements;
//			device->get_buffer_memory_requirements(data->m_pWorkBuffer0, &memory_requirements);
//
//			::framework::gpu::memory_allocate_info memory_allocate_info;
//			memory_allocate_info.allocation_size = memory_requirements.size;
//			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//			device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer0Memory);
//			device->bind_buffer_memory(data->m_pWorkBuffer0, data->m_pWorkBuffer0Memory, 0);
//		}
//
//		{
//			::framework::gpu::memory_requirements memory_requirements;
//			device->get_buffer_memory_requirements(data->m_pWorkBuffer1, &memory_requirements);
//
//			::framework::gpu::memory_allocate_info memory_allocate_info;
//			memory_allocate_info.allocation_size = memory_requirements.size;
//			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//			device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer1Memory);
//			device->bind_buffer_memory(data->m_pWorkBuffer1, data->m_pWorkBuffer1Memory, 0);
//		}
//	}
//
//	{
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = sizeof(::std::uint32_t) * (NUM_WGS * WG_SIZE * (1 << BITS_PER_PASS));
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer2);
//
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer2Debug);
//
//		::framework::gpu::memory_requirements memory_requirements;
//		device->get_buffer_memory_requirements(data->m_pWorkBuffer2, &memory_requirements);
//
//		::framework::gpu::memory_allocate_info memory_allocate_info;
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//		device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer2Memory);
//		device->bind_buffer_memory(data->m_pWorkBuffer2, data->m_pWorkBuffer2Memory, 0);
//
//		device->get_buffer_memory_requirements(data->m_pWorkBuffer2Debug, &memory_requirements);
//
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
//		device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer2DebugMemory);
//		device->bind_buffer_memory(data->m_pWorkBuffer2Debug, data->m_pWorkBuffer2DebugMemory, 0);
//	}
//
//	{
//		::std::size_t uboAlignment = 256;
//		::std::size_t dynamicAlignment = (sizeof(MyRadixSortUB) / uboAlignment) * uboAlignment + ((sizeof(MyRadixSortUB) % uboAlignment) > 0 ? uboAlignment : 0);
//
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = dynamicAlignment * (data->m_maxSortBits / BITS_PER_PASS);
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pConstBuffer);
//
//		::framework::gpu::memory_requirements memory_requirements;
//		device->get_buffer_memory_requirements(data->m_pConstBuffer, &memory_requirements);
//
//		::framework::gpu::memory_allocate_info memory_allocate_info;
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//		device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pConstBufferMemory);
//		device->bind_buffer_memory(data->m_pConstBuffer, data->m_pConstBufferMemory, 0);
//	}
//
//	{
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = 3 * sizeof(::framework::gpu::dispatch_indirect_command);
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//		device->create_buffer(&buffer_create_info, nullptr, &data->m_pDispathIndirectCommandsBuffer);
//
//		::framework::gpu::memory_requirements memory_requirements;
//		device->get_buffer_memory_requirements(data->m_pDispathIndirectCommandsBuffer, &memory_requirements);
//
//		::framework::gpu::memory_allocate_info memory_allocate_info;
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//		device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pDispathIndirectCommandsBufferDeviceMemory);
//		device->bind_buffer_memory(data->m_pDispathIndirectCommandsBuffer, data->m_pDispathIndirectCommandsBufferDeviceMemory, 0);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pStreamCountPipelineLayout;
//		device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pStreamCountDescriptorTables[0]);
//		device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pStreamCountDescriptorTables[1]);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pPrefixScanPipelineLayout;
//		device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pPrefixScanDescriptorTable);
//
//		device->UpdateDescriptorTable(data->m_pPrefixScanDescriptorTable, 0, 1, &data->m_pPrefixScanDescriptorSet);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pSortAndScatterPipelineLayout;
//		device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pSortAndScatterDescriptorTables[0]);
//		device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pSortAndScatterDescriptorTables[1]);
//	}
//
//	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
//	descriptor_buffer_infos[0].buffer = data->m_pConstBuffer;
//	descriptor_buffer_infos[0].offset = 0;
//	descriptor_buffer_infos[0].range = (sizeof(MyRadixSortUB) / 256) * 256 + ((sizeof(MyRadixSortUB) % 256) > 0 ? 256 : 0);
//	descriptor_buffer_infos[0].stride = (sizeof(MyRadixSortUB) / 256) * 256 + ((sizeof(MyRadixSortUB) % 256) > 0 ? 256 : 0);
//	descriptor_buffer_infos[1].buffer = data->m_pWorkBuffer0;
//	descriptor_buffer_infos[1].offset = 0;
//	descriptor_buffer_infos[1].range = data->m_pWorkBuffer0->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);
//	descriptor_buffer_infos[2].buffer = data->m_pWorkBuffer1;
//	descriptor_buffer_infos[2].offset = 0;
//	descriptor_buffer_infos[2].range = data->m_pWorkBuffer1->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);
//	descriptor_buffer_infos[3].buffer = data->m_pWorkBuffer2;
//	descriptor_buffer_infos[3].offset = 0;
//	descriptor_buffer_infos[3].range = data->m_pWorkBuffer2->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);
//
//	// Setup StreamCountDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
//		write_descriptor_sets[0].dst_set = data->m_pStreamCountDescriptorSets[0];
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		write_descriptor_sets[1].dst_set = data->m_pStreamCountDescriptorSets[1];
//		write_descriptor_sets[1].dst_binding = 1;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
//		write_descriptor_sets[2].dst_set = data->m_pStreamCountDescriptorSets[0];
//		write_descriptor_sets[2].dst_binding = 2;
//		write_descriptor_sets[2].dst_array_element = 0;
//		write_descriptor_sets[2].descriptor_count = 1;
//		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[3];
//
//		::framework::gpu::copy_descriptor_set copyDescriptorSets[2];
//		copyDescriptorSets[0].src_set = data->m_pStreamCountDescriptorSets[0];
//		copyDescriptorSets[0].src_binding = 0;
//		copyDescriptorSets[0].src_array_element = 0;
//		copyDescriptorSets[0].dst_set = data->m_pStreamCountDescriptorSets[1];
//		copyDescriptorSets[0].dst_binding = 0;
//		copyDescriptorSets[0].dst_array_element = 0;
//		copyDescriptorSets[0].descriptor_count = 1;
//		copyDescriptorSets[1].src_set = data->m_pStreamCountDescriptorSets[0];
//		copyDescriptorSets[1].src_binding = 2;
//		copyDescriptorSets[1].src_array_element = 0;
//		copyDescriptorSets[1].dst_set = data->m_pStreamCountDescriptorSets[1];
//		copyDescriptorSets[1].dst_binding = 2;
//		copyDescriptorSets[1].dst_array_element = 0;
//		copyDescriptorSets[1].descriptor_count = 1;
//
//		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
//		device->UpdateDescriptorTable(data->m_pStreamCountDescriptorTables[1], 0, 1, &data->m_pStreamCountDescriptorSets[1]);
//	}
//
//	// Setup PrefixScanDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
//		write_descriptor_sets[0].dst_set = data->m_pPrefixScanDescriptorSet;
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		write_descriptor_sets[1].dst_set = data->m_pPrefixScanDescriptorSet;
//		write_descriptor_sets[1].dst_binding = 1;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[3];
//
//		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//	}
//
//	// Setup SortAndScatterDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[6];
//		write_descriptor_sets[0].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		write_descriptor_sets[1].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[1].dst_binding = 1;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[3];
//		write_descriptor_sets[2].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[2].dst_binding = 4;
//		write_descriptor_sets[2].dst_array_element = 0;
//		write_descriptor_sets[2].descriptor_count = 1;
//		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
//		write_descriptor_sets[3].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[3].dst_binding = 5;
//		write_descriptor_sets[3].dst_array_element = 0;
//		write_descriptor_sets[3].descriptor_count = 1;
//		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[2];
//		write_descriptor_sets[4].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		write_descriptor_sets[4].dst_binding = 2;
//		write_descriptor_sets[4].dst_array_element = 0;
//		write_descriptor_sets[4].descriptor_count = 1;
//		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[1];
//		write_descriptor_sets[5].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		write_descriptor_sets[5].dst_binding = 3;
//		write_descriptor_sets[5].dst_array_element = 0;
//		write_descriptor_sets[5].descriptor_count = 1;
//		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[2];
//
//		::framework::gpu::copy_descriptor_set copyDescriptorSets[2];
//		copyDescriptorSets[0].src_set = data->m_pSortAndScatterDescriptorSets[0];
//		copyDescriptorSets[0].src_binding = 0;
//		copyDescriptorSets[0].src_array_element = 0;
//		copyDescriptorSets[0].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		copyDescriptorSets[0].dst_binding = 0;
//		copyDescriptorSets[0].dst_array_element = 0;
//		copyDescriptorSets[0].descriptor_count = 1;
//		copyDescriptorSets[1].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		copyDescriptorSets[1].dst_binding = 1;
//		copyDescriptorSets[1].dst_array_element = 0;
//		copyDescriptorSets[1].src_set = data->m_pSortAndScatterDescriptorSets[0];
//		copyDescriptorSets[1].src_binding = 1;
//		copyDescriptorSets[1].src_array_element = 0;
//		copyDescriptorSets[1].descriptor_count = 1;
//
//		device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
//	}
//
//	{
//		::framework::gpu::command_pool_create_info command_pool_create_info;
//		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
//		command_pool_create_info.queue_family_index = 0;
//		data->device->create_command_pool(&command_pool_create_info, &data->command_pool);
//
//		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
//		command_buffer_allocate_info.command_pool = data->command_pool;
//		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
//		command_buffer_allocate_info.command_buffer_count = 1;
//		data->device->allocate_command_buffers(&command_buffer_allocate_info, &data->command_buffer);
//	}
//
//	::framework::gpu::fence_create_info fence_create_info;
//	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
//	data->device->create_fence(&fence_create_info, nullptr, &data->d3d12_fence);
//
//	*data = data;
//}
//
//void CMyRadixSort::deallocate(Data * pData)
//{
//	pData->device->free_command_buffers(pData->command_pool, 1, &pData->command_buffer);
//	pData->device->destroy_command_pool(pData->command_pool);
//	pData->device->destroy_pipeline(pData->m_pStreamCountPso);
//	pData->device->destroy_pipeline(pData->m_pPrefixScanPso);
//	pData->device->destroy_pipeline(pData->m_pSortAndScatterPso);
//	pData->device->destroy_descriptor_set_layout(pData->m_pStreamCountDescriptorSetLayout);
//	pData->device->destroy_descriptor_set_layout(pData->m_pPrefixScanDescriptorSetLayout);
//	pData->device->destroy_descriptor_set_layout(pData->m_pSortAndScatterDescriptorSetLayout);
//	pData->device->DestroyDescriptorTable(pData->m_pStreamCountDescriptorTables[0]);
//	pData->device->DestroyDescriptorTable(pData->m_pStreamCountDescriptorTables[1]);
//	pData->device->DestroyDescriptorTable(pData->m_pPrefixScanDescriptorTable);
//	pData->device->DestroyDescriptorTable(pData->m_pSortAndScatterDescriptorTables[0]);
//	pData->device->DestroyDescriptorTable(pData->m_pSortAndScatterDescriptorTables[1]);
//	pData->device->destroy_descriptor_pool(pData->descriptor_pool);
//	pData->device->destroy_buffer(pData->m_pWorkBuffer0);
//	pData->device->free_memory(pData->m_pWorkBuffer0Memory);
//	pData->device->destroy_buffer(pData->m_pWorkBuffer1);
//	pData->device->free_memory(pData->m_pWorkBuffer1Memory);
//	pData->device->destroy_buffer(pData->m_pWorkBuffer2);
//	pData->device->free_memory(pData->m_pWorkBuffer2Memory);
//	pData->device->destroy_buffer(pData->m_pConstBuffer);
//	pData->device->free_memory(pData->m_pConstBufferMemory);
//	pData->device->destroy_buffer(pData->m_pDispathIndirectCommandsBuffer);
//	pData->device->free_memory(pData->m_pDispathIndirectCommandsBufferDeviceMemory);
//
//	delete pData;
//}
//
//void CMyRadixSort::configure(Data * data, class Buffer * pInoutKeysBuffer, class Buffer * pInoutValuesBuffer, ::std::uint32_t sortBits)
//{
//	class Buffer * srcKeys = pInoutKeysBuffer;
//	class Buffer * srcValues = pInoutValuesBuffer;
//	class Buffer * dstKeys = data->m_pWorkBuffer0;
//	class Buffer * dstValues = data->m_pWorkBuffer1;
//	class Buffer * histogramBuffer = data->m_pWorkBuffer2;
//
//	{
//		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
//		descriptor_buffer_infos[0].buffer = srcKeys;
//		descriptor_buffer_infos[0].offset = 0;
//		descriptor_buffer_infos[0].range = srcKeys->GetBufferCreateInfo()->size;
//		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
//
//		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
//		write_descriptor_sets[0].dst_set = data->m_pStreamCountDescriptorSets[0];
//		write_descriptor_sets[0].dst_binding = 1;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//
//		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		data->device->UpdateDescriptorTable(data->m_pStreamCountDescriptorTables[0], 0, 1, &data->m_pStreamCountDescriptorSets[0]);
//	}
//	{
//		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
//		descriptor_buffer_infos[0].buffer = srcKeys;
//		descriptor_buffer_infos[0].offset = 0;
//		descriptor_buffer_infos[0].range = srcKeys->GetBufferCreateInfo()->size;
//		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
//		descriptor_buffer_infos[1].buffer = srcValues;
//		descriptor_buffer_infos[1].offset = 0;
//		descriptor_buffer_infos[1].range = srcValues->GetBufferCreateInfo()->size;
//		descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);
//
//		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
//		write_descriptor_sets[0].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[0].dst_binding = 2;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		write_descriptor_sets[1].dst_set = data->m_pSortAndScatterDescriptorSets[0];
//		write_descriptor_sets[1].dst_binding = 3;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
//		write_descriptor_sets[2].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		write_descriptor_sets[2].dst_binding = 4;
//		write_descriptor_sets[2].dst_array_element = 0;
//		write_descriptor_sets[2].descriptor_count = 1;
//		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[0];
//		write_descriptor_sets[3].dst_set = data->m_pSortAndScatterDescriptorSets[1];
//		write_descriptor_sets[3].dst_binding = 5;
//		write_descriptor_sets[3].dst_array_element = 0;
//		write_descriptor_sets[3].descriptor_count = 1;
//		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[1];
//
//		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		data->device->UpdateDescriptorTable(data->m_pSortAndScatterDescriptorTables[0], 0, 1, &data->m_pSortAndScatterDescriptorSets[0]);
//		data->device->UpdateDescriptorTable(data->m_pSortAndScatterDescriptorTables[1], 0, 1, &data->m_pSortAndScatterDescriptorSets[1]);
//	}
//
//	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
//	command_buffer_begin_info.flags = 0;
//	command_buffer_begin_info.inheritance_info = nullptr;
//	data->command_buffer->begin_command_buffer(&command_buffer_begin_info);
//	::std::uint32_t currentSet = 0, lastSet = 1;
//	for (int ib = 0; ib < sortBits; ib += BITS_PER_PASS)
//	{
//		::std::uint32_t const pushConstants[1] = { ib };
//
//		{// stream count
//			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
//			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].buffer = histogramBuffer;
//			buffer_memory_barriers[0].offset = 0;
//			buffer_memory_barriers[0].size = histogramBuffer->GetBufferCreateInfo()->size;
//
//			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[1].buffer = srcKeys;
//			buffer_memory_barriers[1].offset = 0;
//			buffer_memory_barriers[1].size = srcKeys->GetBufferCreateInfo()->size;
//
//			data->command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//			data->command_buffer->bind_pipeline(data->m_pStreamCountPso);
//			data->command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pStreamCountDescriptorTables[currentSet], 0, nullptr);
//			data->command_buffer->push_constants(data->m_pStreamCountPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
//			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, 0);
//		}
//		{//	prefix scan group histogram
//			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
//			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].buffer = histogramBuffer;
//			buffer_memory_barriers[0].offset = 0;
//			buffer_memory_barriers[0].size = histogramBuffer->GetBufferCreateInfo()->size;
//
//			data->command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//			data->command_buffer->bind_pipeline(data->m_pPrefixScanPso);
//			data->command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pPrefixScanDescriptorTable, 0, nullptr);
//			data->command_buffer->push_constants(data->m_pPrefixScanPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
//			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command));
//		}
//		{//	local sort and distribute
//			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[4];
//			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[0].buffer = histogramBuffer;
//			buffer_memory_barriers[0].offset = 0;
//			buffer_memory_barriers[0].size = histogramBuffer->GetBufferCreateInfo()->size;
//
//			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[1].buffer = srcValues;
//			buffer_memory_barriers[1].offset = 0;
//			buffer_memory_barriers[1].size = srcValues->GetBufferCreateInfo()->size;
//
//			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[2].buffer = dstKeys;
//			buffer_memory_barriers[2].offset = 0;
//			buffer_memory_barriers[2].size = dstKeys->GetBufferCreateInfo()->size;
//
//			buffer_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//			buffer_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//			buffer_memory_barriers[3].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[3].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//			buffer_memory_barriers[3].buffer = dstValues;
//			buffer_memory_barriers[3].offset = 0;
//			buffer_memory_barriers[3].size = dstValues->GetBufferCreateInfo()->size;
//
//			data->command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//			data->command_buffer->bind_pipeline(data->m_pSortAndScatterPso);
//			data->command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pSortAndScatterDescriptorTables[currentSet], 0, nullptr);
//			data->command_buffer->push_constants(data->m_pSortAndScatterPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
//			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, 2 * sizeof(::framework::gpu::dispatch_indirect_command));
//		}
//		::std::swap(currentSet, lastSet);
//		::std::swap(srcKeys, dstKeys);
//		::std::swap(srcValues, dstValues);
//	}
//	data->command_buffer->end_command_buffer();
//}
//
//void CMyRadixSort::setSize(Data * data, ::std::uint64_t size)
//{
//	int nAligned = size;
//	{
//		int tmp;
//		if ((tmp = nAligned % ELEMENTS_PER_WORK_GROUP) != 0)
//			nAligned += nAligned > -1 ? (ELEMENTS_PER_WORK_GROUP - tmp) : -tmp;
//	}
//
//	::std::uint32_t const nBlocks = (size + ELEMENTS_PER_WORK_ITEM * WG_SIZE - 1) / (ELEMENTS_PER_WORK_ITEM * WG_SIZE);
//	::std::uint32_t const nWGs = ::std::min(nBlocks, static_cast<::std::uint32_t>(NUM_WGS));
//
//	MyRadixSortUB MyRadixSortUB;
//	MyRadixSortUB.m_n = size;
//	MyRadixSortUB.m_nAligned = nAligned;
//	MyRadixSortUB.m_nWGs = NUM_WGS;
//	MyRadixSortUB.m_nBlocksPerWG = nBlocks < NUM_WGS ? 1 : (nBlocks + MyRadixSortUB.m_nWGs - 1) / MyRadixSortUB.m_nWGs;
//
//	::std::size_t const uboAlignment = 256;
//	::std::size_t const dynamicAlignment = (sizeof(MyRadixSortUB) / uboAlignment) * uboAlignment + ((sizeof(MyRadixSortUB) % uboAlignment) > 0 ? uboAlignment : 0);
//
//	void * mapped_data;
//	data->device->map_memory(data->m_pConstBufferMemory, 0, dynamicAlignment, 0, &mapped_data);
//	::std::memcpy(static_cast<char*>(mapped_data), &MyRadixSortUB, sizeof(MyRadixSortUB));
//	data->device->unmap_memory(data->m_pConstBufferMemory);
//
//	::framework::gpu::dispatch_indirect_command dispatchIndirectCommand;
//	void * mappedData1;
//	data->device->map_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory, 0, sizeof(::framework::gpu::dispatch_indirect_command) * 3, 0, &mappedData1);
//	dispatchIndirectCommand = { NUM_WGS, 1, 1 };
//	::std::memcpy(static_cast<char*>(mappedData1), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
//	dispatchIndirectCommand = { 1, 1, 1 };
//	::std::memcpy(static_cast<char*>(mappedData1) + sizeof(::framework::gpu::dispatch_indirect_command), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
//	dispatchIndirectCommand = { nWGs, 1, 1 };
//	::std::memcpy(static_cast<char*>(mappedData1) + sizeof(::framework::gpu::dispatch_indirect_command) * 2, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
//	data->device->unmap_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory);
//}
//
//void CMyRadixSort::execute(Data * data, class Queue * command_queue, ::std::uint32_t wait_semaphore_count, class Semaphore * const * wait_semaphores, ::framework::gpu::pipeline_stage_flags const * wait_dst_stage_mask, ::std::uint32_t signal_semaphore_count, class Semaphore * const * signal_semaphores, class ::framework::gpu::fence * fence)
//{
//	::framework::gpu::submit_info submit_info;
//	submit_info.command_buffer_count = 1;
//	submit_info.command_buffers = &data->command_buffer;
//	submit_info.wait_semaphore_count = wait_semaphore_count;
//	submit_info.wait_semaphores = wait_semaphores;
//	submit_info.wait_dst_stage_mask = wait_dst_stage_mask;
//	submit_info.signal_semaphore_count = signal_semaphore_count;
//	submit_info.signal_semaphores = signal_semaphores;
//	command_queue->submit(1, &submit_info, fence);
//
//	// Readback from gpu
//	if (false)
//	{
//		command_queue->wait_idle();
//
//		class Queue * transferQueue;
//		data->device->get_queue(0, 0, &transferQueue);
//
//		::framework::gpu::command_pool_create_info command_pool_create_info;
//		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
//		command_pool_create_info.queue_family_index = 0;
//		class CommandPool * command_pool;
//		data->device->create_command_pool(&command_pool_create_info, &command_pool);
//
//		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
//		command_buffer_allocate_info.command_pool = command_pool;
//		command_buffer_allocate_info.level = primary;
//		command_buffer_allocate_info.command_buffer_count = 1;
//		class CommandBuffer * copyCmd;
//
//		data->device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
//		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
//		copyCmd->begin_command_buffer(&command_buffer_begin_info);
//
//		::framework::gpu::buffer_copy buffer_copy{};
//		buffer_copy.size = data->m_pWorkBuffer2Debug->GetBufferCreateInfo()->size;
//		copyCmd->copy_buffer(data->m_pWorkBuffer2, data->m_pWorkBuffer2Debug, 1, &buffer_copy);
//		copyCmd->end_command_buffer();
//
//		class ::framework::gpu::fence * fence;
//		::framework::gpu::fence_create_info fence_create_info;
//		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
//		data->device->create_fence(&fence_create_info, nullptr, &fence);
//
//		::framework::gpu::submit_info submit_info;
//		submit_info.wait_semaphore_count = 0;
//		submit_info.wait_semaphores = nullptr;
//		submit_info.wait_dst_stage_mask = nullptr;
//		submit_info.command_buffer_count = 1;
//		submit_info.command_buffers = &copyCmd;
//		submit_info.signal_semaphore_count = 0;
//		submit_info.signal_semaphores = nullptr;
//		transferQueue->submit(1, &submit_info, fence);
//
//		data->device->wait_for_fences(1, &fence, true, UINT64_MAX);
//		data->device->destroy_fence(fence);
//
//		::std::vector<::std::uint32_t> dataVec(NUM_WGS * (1 << BITS_PER_PASS));
//
//		void * pMappedData;
//		data->device->map_memory(data->m_pWorkBuffer2DebugMemory, 0, buffer_copy.size, 0, &pMappedData);
//		::std::memcpy(dataVec.data(), pMappedData, buffer_copy.size);
//		data->device->unmap_memory(data->m_pWorkBuffer2DebugMemory);
//
//		for (::std::size_t i = 0; i < 128; i++)
//			::std::cout << /*i << "= " <<*/ dataVec[i] << (((i + 1) % 128) ? ", " : "\n");
//		system("pause");
//	}
//}