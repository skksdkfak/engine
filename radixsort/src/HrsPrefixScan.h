//#pragma once
//
//#include <gpu/core.h>
//#include <InstanceManager.h>
//
//namespace framework
//{
//	struct HrsPrefixScanCreateInfo
//	{
//		::std::uint64_t		maxSize;
//		::std::uint32_t		lst;
//		::std::uint32_t		invocationSize;
//		class ::framework::gpu::device *		device;
//		class ::framework::gpu::buffer *		pSrcDataBuffer;
//		class ::framework::gpu::device_memory *	pSrcDataDeviceMemory;
//		class ::framework::gpu::buffer *		pHrsDataBuffer;
//		class ::framework::gpu::device_memory *	pHrsDataDeviceMemory;
//		class ::framework::gpu::buffer *		pBlockAssignmentsBuffer[2];
//		class ::framework::gpu::device_memory *	pBlockAssignmentsDeviceMemory[2];
//		class ::framework::gpu::buffer *		pDispathIndirectCommandsBuffer;
//		class ::framework::gpu::device_memory *	pDispathIndirectCommandsBufferDeviceMemory;
//	};
//
//	struct HrsPrefixScanUB
//	{
//		::std::uint32_t m_n;
//		::std::uint32_t m_nBlocksPerWG;
//		::std::uint32_t m_lst;
//		::std::uint32_t m_invocationIndex;
//	};
//
//	class HrsPrefixScan
//	{
//	public:
//		struct Data
//		{
//			::std::uint64_t				m_maxSize;
//			::std::uint64_t				size;
//			::std::uint32_t				m_lst;
//			::std::uint32_t				m_invocationSize;
//			class ::framework::gpu::device*					device;
//			class ::framework::gpu::command_pool*			this->command_pool;
//			class ::framework::gpu::command_buffer*			this->command_buffer;
//			class ::framework::gpu::fence*					d3d12_fence;
//			class ::framework::gpu::shader_module const *	m_pTopLevelScanShader;
//			class ::framework::gpu::shader_module const *	m_pLocalScanShader;
//			class ::framework::gpu::shader_module const *	m_pAddOffsetShader;
//			class ::framework::gpu::pipeline *				m_pTopLevelScanPso;
//			class ::framework::gpu::pipeline *				m_pLocalScanPso;
//			class ::framework::gpu::pipeline *				m_pAddOffsetPso;
//			class ::framework::gpu::pipeline_layout*			m_pTopLevelScanPipelineLayout;
//			class ::framework::gpu::pipeline_layout*			m_pLocalScanPipelineLayout;
//			class ::framework::gpu::pipeline_layout*			m_pAddOffsetPipelineLayout;
//			class ::framework::gpu::descriptor_set_layout*	m_pTopLevelScanDescriptorSetLayout;
//			class ::framework::gpu::descriptor_set_layout*	m_pLocalScanDescriptorSetLayout;
//			class ::framework::gpu::descriptor_set_layout*	m_pAddOffsetDescriptorSetLayout;
//			class ::framework::gpu::descriptor_pool*			descriptor_pool;
//			class ::framework::gpu::descriptor_set*			m_pTopLevelScanDescriptorSet;
//			class ::framework::gpu::descriptor_set*			m_pLocalScanDescriptorSet;
//			class ::framework::gpu::descriptor_set*			m_pAddOffsetDescriptorSet[2];
//			class ::framework::gpu::buffer *				m_pSrcDataBuffer;
//			class ::framework::gpu::device_memory *			m_pSrcDataDeviceMemory;
//			class ::framework::gpu::buffer *				m_pHrsDataBuffer;
//			class ::framework::gpu::device_memory *			m_pHrsDataDeviceMemory;
//			class ::framework::gpu::buffer *				m_pBlockAssignmentsBuffer[2];
//			class ::framework::gpu::device_memory *			m_pBlockAssignmentsDeviceMemory[2];
//			class ::framework::gpu::buffer*					m_pWorkBuffer;
//			class ::framework::gpu::device_memory*			m_pWorkBufferMemory;
//			class ::framework::gpu::buffer*					m_pWorkBuffer2Debug;
//			class ::framework::gpu::device_memory*			m_pWorkBuffer2DebugMemory;
//			class ::framework::gpu::buffer *				m_pDispathIndirectCommandsBuffer;
//			class ::framework::gpu::device_memory *			m_pDispathIndirectCommandsBufferDeviceMemory;
//			HrsPrefixScanUB			m_prefixScanUB;
//		};
//
//		static void allocate(HrsPrefixScanCreateInfo const * pHrsPrefixScanCreateInfo, Data *& data);
//
//		static void deallocate(Data * pData);
//
//		static void configure(Data * data, class ::framework::gpu::buffer * pDataBuffer);
//
//		static void setSize(Data * data, ::std::uint64_t size);
//
//		static void execute(Data * data, class ::framework::gpu::queue * command_queue, ::std::uint32_t wait_semaphore_count, class ::framework::gpu::semaphore * const * wait_semaphores, ::framework::gpu::pipeline_stage_flags const * pWaitDstStageMask, ::std::uint32_t signal_semaphore_count, class ::framework::gpu::semaphore * const * signal_semaphores, class ::framework::gpu::fence * fence);
//
//		static void recordCommands(Data * data, class ::framework::gpu::command_buffer * command_buffer, ::std::uint32_t setIndex, ::std::uint32_t invocationIndex);
//
//	private:
//	    enum : ::std::uint32_t
//        {
//		    WG_SIZE = 64,
//		    NUM_WGS = 1024
//        };
//	};
//}