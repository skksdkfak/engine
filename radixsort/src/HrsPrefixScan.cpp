//#include "resource/resource_manager.hpp"
//#include "InstanceManager.h"
//#include "gpu/utility.hpp"
//#include "HrsPrefixScan.h"
//
//using namespace framework;
//
//void HrsPrefixScan::allocate(HrsPrefixScanCreateInfo const * pHrsPrefixScanCreateInfo, Data *& data)
//{
//	data = new Data;
//
//	data->m_maxSize = pHrsPrefixScanCreateInfo->maxSize;
//	data->m_lst = pHrsPrefixScanCreateInfo->lst;
//	data->m_invocationSize = pHrsPrefixScanCreateInfo->invocationSize;
//	data->device = pHrsPrefixScanCreateInfo->device;
//	data->m_pSrcDataBuffer = pHrsPrefixScanCreateInfo->pSrcDataBuffer;
//	data->m_pSrcDataDeviceMemory = pHrsPrefixScanCreateInfo->pSrcDataDeviceMemory;
//	data->m_pHrsDataBuffer = pHrsPrefixScanCreateInfo->pHrsDataBuffer;
//	data->m_pHrsDataDeviceMemory = pHrsPrefixScanCreateInfo->pHrsDataDeviceMemory;
//	data->m_pBlockAssignmentsBuffer[0] = pHrsPrefixScanCreateInfo->pBlockAssignmentsBuffer[0];
//	data->m_pBlockAssignmentsDeviceMemory[0] = pHrsPrefixScanCreateInfo->pBlockAssignmentsDeviceMemory[0];
//	data->m_pBlockAssignmentsBuffer[1] = pHrsPrefixScanCreateInfo->pBlockAssignmentsBuffer[1];
//	data->m_pBlockAssignmentsDeviceMemory[1] = pHrsPrefixScanCreateInfo->pBlockAssignmentsDeviceMemory[1];
//	data->m_pLocalScanShader = ::framework::resource::resource_manager.load_shader_module(data->device, "hrslocalscan.comp.sdr");
//	data->m_pTopLevelScanShader = ::framework::resource::resource_manager.load_shader_module(data->device, "hrstoplevelscan.comp.sdr");
//	data->m_pAddOffsetShader = ::framework::resource::resource_manager.load_shader_module(data->device, "hrsaddoffset.comp.sdr");
//
//	::framework::gpu::physical_device_memory_properties memoryProperties;
//	data->device->GetPhysicalDevice()->get_memory_properties(&memoryProperties);
//
//	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
//	pipeline_shader_stage_create_infos[0].module = data->m_pLocalScanShader;
//	pipeline_shader_stage_create_infos[0].name = "main";
//	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//	pipeline_shader_stage_create_infos[1].module = data->m_pTopLevelScanShader;
//	pipeline_shader_stage_create_infos[1].name = "main";
//	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//	pipeline_shader_stage_create_infos[2].module = data->m_pAddOffsetShader;
//	pipeline_shader_stage_create_infos[2].name = "main";
//	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
//
//	// LocalScan class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
//		// BucketHistogram
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HrsBucketHistogram
//		descriptor_set_layout_bindings[1].binding = 1;
//		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[1].descriptor_count = 1;
//		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HrsBlockHistogram
//		descriptor_set_layout_bindings[2].binding = 2;
//		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[2].descriptor_count = 1;
//		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pLocalScanDescriptorSetLayout);
//	}
//
//	// TopLevelScan class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
//		// histogram
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pTopLevelScanDescriptorSetLayout);
//	}
//
//	// AddOffset class DescriptorSetLayout
//	{
//		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[7];
//		// BucketHistogram
//		descriptor_set_layout_bindings[0].binding = 0;
//		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
//		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[0].descriptor_count = 1;
//		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HrsBlockHistogram
//		descriptor_set_layout_bindings[1].binding = 1;
//		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[1].descriptor_count = 1;
//		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HrsBucketHistogram
//		descriptor_set_layout_bindings[2].binding = 2;
//		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[2].descriptor_count = 1;
//		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// BlockAssignments
//		descriptor_set_layout_bindings[3].binding = 3;
//		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[3].descriptor_count = 1;
//		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HrsPrefixScanDispatchIndirectCommand
//		descriptor_set_layout_bindings[4].binding = 4;
//		descriptor_set_layout_bindings[4].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[4].descriptor_count = 1;
//		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// PrefixScanDispatchIndirectCommand
//		descriptor_set_layout_bindings[5].binding = 5;
//		descriptor_set_layout_bindings[5].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[5].descriptor_count = 1;
//		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//		// HybridRadixSortDispatchIndirectCommand
//		descriptor_set_layout_bindings[6].binding = 6;
//		descriptor_set_layout_bindings[6].hlsl_shader_register = 1;
//		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
//		descriptor_set_layout_bindings[6].descriptor_count = 1;
//		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//
//		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
//		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
//		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
//		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &data->m_pAddOffsetDescriptorSetLayout);
//	}
//
//	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[2];
//	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::sampled_buffer;
//	descriptor_pool_sizes[0].descriptor_count = 1;
//	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::storage_buffer;
//	descriptor_pool_sizes[1].descriptor_count = 4;
//	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
//	descriptor_pool_create_info.max_sets = 4;
//	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
//	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
//	data->device->create_descriptor_pool(&descriptor_pool_create_info, &data->descriptor_pool);
//
//	class DescriptorSetLayout * set_layouts[] =
//	{
//		data->m_pLocalScanDescriptorSetLayout,
//		data->m_pTopLevelScanDescriptorSetLayout,
//		data->m_pAddOffsetDescriptorSetLayout,
//		data->m_pAddOffsetDescriptorSetLayout
//	};
//	class DescriptorSet * descriptor_sets[4];
//	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
//	descriptor_set_allocate_info.descriptor_pool = data->descriptor_pool;
//	descriptor_set_allocate_info.descriptor_set_count = ::std::size(set_layouts);
//	descriptor_set_allocate_info.set_layouts = set_layouts;
//	data->device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);
//
//	data->m_pLocalScanDescriptorSet = descriptor_sets[0];
//	data->m_pTopLevelScanDescriptorSet = descriptor_sets[1];
//	data->m_pAddOffsetDescriptorSet[0] = descriptor_sets[2];
//	data->m_pAddOffsetDescriptorSet[1] = descriptor_sets[3];
//
//	::framework::gpu::push_constant_range push_constant_ranges;
//	push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
//	push_constant_ranges.binding = 0;
//	push_constant_ranges.offset = 0;
//	push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
//	push_constant_ranges.hlsl_shader_register = 1;
//	push_constant_ranges.hlsl_register_space = 0;
//	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
//	pipeline_layout_create_info.descriptor_set_layout_count = 1;
//	pipeline_layout_create_info.push_constant_range_count = 1;
//	pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
//
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pLocalScanDescriptorSetLayout;
//	data->device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pLocalScanPipelineLayout);
//
//	pipeline_layout_create_info.push_constant_range_count = 0;
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pTopLevelScanDescriptorSetLayout;
//	data->device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pTopLevelScanPipelineLayout);
//
//	pipeline_layout_create_info.push_constant_range_count = 1;
//	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pAddOffsetDescriptorSetLayout;
//	data->device->create_pipeline_layout(&pipeline_layout_create_info, &data->m_pAddOffsetPipelineLayout);
//
//	class Pipeline * pPipelines[3];
//	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[3];
//	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
//	compute_pipeline_create_infos[0].pipeline_layout = data->m_pLocalScanPipelineLayout;
//	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
//	compute_pipeline_create_infos[1].pipeline_layout = data->m_pTopLevelScanPipelineLayout;
//	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
//	compute_pipeline_create_infos[2].pipeline_layout = data->m_pAddOffsetPipelineLayout;
//	data->device->create_compute_pipelines(compute_pipeline_create_infos, 3, pPipelines);
//
//	data->m_pLocalScanPso = pPipelines[0];
//	data->m_pTopLevelScanPso = pPipelines[1];
//	data->m_pAddOffsetPso = pPipelines[2];
//
//	{
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = sizeof(::std::uint32_t) * 3 * NUM_WGS;
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer);
//
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
//		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer2Debug);
//
//		::framework::gpu::memory_requirements memory_requirements;
//		data->device->get_buffer_memory_requirements(data->m_pWorkBuffer, &memory_requirements);
//
//		::framework::gpu::memory_allocate_info memory_allocate_info;
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBufferMemory);
//		data->device->bind_buffer_memory(data->m_pWorkBuffer, data->m_pWorkBufferMemory, 0);
//
//		data->device->get_buffer_memory_requirements(data->m_pWorkBuffer2Debug, &memory_requirements);
//
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
//		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer2DebugMemory);
//		data->device->bind_buffer_memory(data->m_pWorkBuffer2Debug, data->m_pWorkBuffer2DebugMemory, 0);
//	}
//
//	{
//		::framework::gpu::buffer_create_info buffer_create_info;
//		buffer_create_info.size = sizeof(::framework::gpu::dispatch_indirect_command) * data->m_invocationSize;
//		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit;
//		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
//		buffer_create_info.queue_family_index_count = 0;
//		buffer_create_info.queue_family_indices = nullptr;
//		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pDispathIndirectCommandsBuffer);
//
//		::framework::gpu::memory_requirements memory_requirements;
//		data->device->get_buffer_memory_requirements(data->m_pDispathIndirectCommandsBuffer, &memory_requirements);
//
//		::framework::gpu::memory_allocate_info memory_allocate_info;
//		memory_allocate_info.allocation_size = memory_requirements.size;
//		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
//		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
//		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
//		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pDispathIndirectCommandsBufferDeviceMemory);
//		data->device->bind_buffer_memory(data->m_pDispathIndirectCommandsBuffer, data->m_pDispathIndirectCommandsBufferDeviceMemory, 0);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pLocalScanPipelineLayout;
//		data->device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pLocalScanDescriptorTable);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pTopLevelScanPipelineLayout;
//		data->device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pTopLevelScanDescriptorTable);
//	}
//
//	{
//		DescriptorTableCreateInfo descriptorTableCreateInfo;
//		descriptorTableCreateInfo.pipeline_layout = data->m_pAddOffsetPipelineLayout;
//		data->device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pAddOffsetDescriptorTable[0]);
//		data->device->CreateDescriptorTable(&descriptorTableCreateInfo, &data->m_pAddOffsetDescriptorTable[1]);
//	}
//
//	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[5];
//	descriptor_buffer_infos[0].buffer = data->m_pSrcDataBuffer;
//	descriptor_buffer_infos[0].offset = 0;
//	descriptor_buffer_infos[0].range = data->m_pSrcDataBuffer->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
//	descriptor_buffer_infos[1].buffer = data->m_pHrsDataBuffer;
//	descriptor_buffer_infos[1].offset = 0;
//	descriptor_buffer_infos[1].range = data->m_pHrsDataBuffer->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t) * 3;
//	descriptor_buffer_infos[2].buffer = data->m_pWorkBuffer;
//	descriptor_buffer_infos[2].offset = 0;
//	descriptor_buffer_infos[2].range = data->m_pWorkBuffer->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t) * 3;
//	descriptor_buffer_infos[3].buffer = data->m_pBlockAssignmentsBuffer[0];
//	descriptor_buffer_infos[3].offset = 0;
//	descriptor_buffer_infos[3].range = data->m_pBlockAssignmentsBuffer[0]->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t) * 4;
//	descriptor_buffer_infos[4].buffer = data->m_pBlockAssignmentsBuffer[1];
//	descriptor_buffer_infos[4].offset = 0;
//	descriptor_buffer_infos[4].range = data->m_pBlockAssignmentsBuffer[1]->GetBufferCreateInfo()->size;
//	descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t) * 4;
//
//	// Setup LocalScanDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
//		// BucketHistogram
//		write_descriptor_sets[0].dst_set = data->m_pLocalScanDescriptorSet;
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		// HrsBucketHistogram
//		write_descriptor_sets[1].dst_set = data->m_pLocalScanDescriptorSet;
//		write_descriptor_sets[1].dst_binding = 1;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
//		// HrsBlockHistogram
//		write_descriptor_sets[2].dst_set = data->m_pLocalScanDescriptorSet;
//		write_descriptor_sets[2].dst_binding = 2;
//		write_descriptor_sets[2].dst_array_element = 0;
//		write_descriptor_sets[2].descriptor_count = 1;
//		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
//
//		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		data->device->UpdateDescriptorTable(data->m_pLocalScanDescriptorTable, 0, 1, &data->m_pLocalScanDescriptorSet);
//	}
//
//	// Setup TopLevelScanDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
//		// HrsBlockHistogram
//		write_descriptor_sets[0].dst_set = data->m_pTopLevelScanDescriptorSet;
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[2];
//
//		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		data->device->UpdateDescriptorTable(data->m_pTopLevelScanDescriptorTable, 0, 1, &data->m_pTopLevelScanDescriptorSet);
//	}
//
//	// Setup AddOffsetDescriptorSet
//	{
//		::framework::gpu::write_descriptor_set write_descriptor_sets[5];
//		// BucketHistogram
//		write_descriptor_sets[0].dst_set = data->m_pAddOffsetDescriptorSet[0];
//		write_descriptor_sets[0].dst_binding = 0;
//		write_descriptor_sets[0].dst_array_element = 0;
//		write_descriptor_sets[0].descriptor_count = 1;
//		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
//		// HrsBlockHistogram
//		write_descriptor_sets[1].dst_set = data->m_pAddOffsetDescriptorSet[0];
//		write_descriptor_sets[1].dst_binding = 1;
//		write_descriptor_sets[1].dst_array_element = 0;
//		write_descriptor_sets[1].descriptor_count = 1;
//		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
//		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[2];
//		// HrsBucketHistogram
//		write_descriptor_sets[2].dst_set = data->m_pAddOffsetDescriptorSet[0];
//		write_descriptor_sets[2].dst_binding = 2;
//		write_descriptor_sets[2].dst_array_element = 0;
//		write_descriptor_sets[2].descriptor_count = 1;
//		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
//		// BlockAssignments
//		write_descriptor_sets[3].dst_set = data->m_pAddOffsetDescriptorSet[0];
//		write_descriptor_sets[3].dst_binding = 3;
//		write_descriptor_sets[3].dst_array_element = 0;
//		write_descriptor_sets[3].descriptor_count = 1;
//		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
//		// BlockAssignments
//		write_descriptor_sets[4].dst_set = data->m_pAddOffsetDescriptorSet[1];
//		write_descriptor_sets[4].dst_binding = 3;
//		write_descriptor_sets[4].dst_array_element = 0;
//		write_descriptor_sets[4].descriptor_count = 1;
//		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
//		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
//
//		::framework::gpu::copy_descriptor_set copyDescriptorSets[3];
//		copyDescriptorSets[0].src_set = data->m_pAddOffsetDescriptorSet[0];
//		copyDescriptorSets[0].src_binding = 0;
//		copyDescriptorSets[0].src_array_element = 0;
//		copyDescriptorSets[0].dst_set = data->m_pAddOffsetDescriptorSet[1];
//		copyDescriptorSets[0].dst_binding = 0;
//		copyDescriptorSets[0].dst_array_element = 0;
//		copyDescriptorSets[0].descriptor_count = 1;
//		copyDescriptorSets[1].src_set = data->m_pAddOffsetDescriptorSet[0];
//		copyDescriptorSets[1].src_binding = 1;
//		copyDescriptorSets[1].src_array_element = 0;
//		copyDescriptorSets[1].dst_set = data->m_pAddOffsetDescriptorSet[1];
//		copyDescriptorSets[1].dst_binding = 1;
//		copyDescriptorSets[1].dst_array_element = 0;
//		copyDescriptorSets[1].descriptor_count = 1;
//		copyDescriptorSets[2].src_set = data->m_pAddOffsetDescriptorSet[0];
//		copyDescriptorSets[2].src_binding = 2;
//		copyDescriptorSets[2].src_array_element = 0;
//		copyDescriptorSets[2].dst_set = data->m_pAddOffsetDescriptorSet[1];
//		copyDescriptorSets[2].dst_binding = 2;
//		copyDescriptorSets[2].dst_array_element = 0;
//		copyDescriptorSets[2].descriptor_count = 1;
//
//		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
//		data->device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
//		data->device->UpdateDescriptorTable(data->m_pAddOffsetDescriptorTable[0], 0, 1, &data->m_pAddOffsetDescriptorSet[0]);
//		data->device->UpdateDescriptorTable(data->m_pAddOffsetDescriptorTable[1], 0, 1, &data->m_pAddOffsetDescriptorSet[1]);
//	}
//
//	{
//		::framework::gpu::command_pool_create_info command_pool_create_info;
//		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
//		command_pool_create_info.queue_family_index = 0;
//		data->device->create_command_pool(&command_pool_create_info, &data->command_pool);
//
//		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
//		command_buffer_allocate_info.command_pool = data->command_pool;
//		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
//		command_buffer_allocate_info.command_buffer_count = 1;
//		data->device->allocate_command_buffers(&command_buffer_allocate_info, &data->command_buffer);
//	}
//
//	::framework::gpu::fence_create_info fence_create_info;
//	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
//	data->device->create_fence(&fence_create_info, nullptr, &data->d3d12_fence);
//
//	*data = data;
//}
//
//void HrsPrefixScan::deallocate(Data * pData)
//{
//	pData->device->free_command_buffers(pData->command_pool, 1, &pData->command_buffer);
//	pData->device->destroy_command_pool(pData->command_pool);
//	pData->device->destroy_pipeline(pData->m_pTopLevelScanPso);
//	pData->device->destroy_pipeline(pData->m_pLocalScanPso);
//	pData->device->destroy_pipeline(pData->m_pAddOffsetPso);
//	pData->device->destroy_descriptor_set_layout(pData->m_pTopLevelScanDescriptorSetLayout);
//	pData->device->destroy_descriptor_set_layout(pData->m_pLocalScanDescriptorSetLayout);
//	pData->device->destroy_descriptor_set_layout(pData->m_pAddOffsetDescriptorSetLayout);
//	pData->device->DestroyDescriptorTable(pData->m_pTopLevelScanDescriptorTable);
//	pData->device->DestroyDescriptorTable(pData->m_pLocalScanDescriptorTable);
//	pData->device->DestroyDescriptorTable(pData->m_pAddOffsetDescriptorTable[0]);
//	pData->device->DestroyDescriptorTable(pData->m_pAddOffsetDescriptorTable[1]);
//	pData->device->destroy_descriptor_pool(pData->descriptor_pool);
//	pData->device->destroy_buffer(pData->m_pWorkBuffer);
//	pData->device->free_memory(pData->m_pWorkBufferMemory);
//	pData->device->destroy_buffer(pData->m_pDispathIndirectCommandsBuffer);
//	pData->device->free_memory(pData->m_pDispathIndirectCommandsBufferDeviceMemory);
//
//	delete pData;
//}
//
//void HrsPrefixScan::configure(Data * data, class Buffer * pDataBuffer)
//{
//	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
//	command_buffer_begin_info.flags = 0;
//	command_buffer_begin_info.inheritance_info = nullptr;
//	data->command_buffer->begin_command_buffer(&command_buffer_begin_info);
//	recordCommands(data, data->command_buffer, 0, 0);
//	data->command_buffer->end_command_buffer();
//}
//
//void HrsPrefixScan::setSize(Data * data, ::std::uint64_t size)
//{
//	::std::uint32_t const nBlocks = (size + WG_SIZE - 1) / WG_SIZE;
//	::std::uint32_t const nWGs = ::std::min(nBlocks, static_cast<::std::uint32_t>(NUM_WGS));
//
//	data->m_prefixScanUB.m_n = size;
//	data->m_prefixScanUB.m_nBlocksPerWG = nBlocks < NUM_WGS ? 1 : (nBlocks + nWGs - 1) / nWGs;
//	data->m_prefixScanUB.m_lst = data->m_lst;
//	data->m_prefixScanUB.m_invocationIndex = 0;
//
//	::framework::gpu::dispatch_indirect_command dispatchIndirectCommand;
//	void * mapped_data;
//	data->device->map_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory, 0, sizeof(::framework::gpu::dispatch_indirect_command), 0, &mapped_data);
//	dispatchIndirectCommand = { nWGs, 1, 1 };
//	::std::memcpy(static_cast<char*>(mapped_data), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
//	data->device->unmap_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory);
//}
//
//void HrsPrefixScan::execute(Data * data, class Queue * command_queue, ::std::uint32_t wait_semaphore_count, class Semaphore * const * wait_semaphores, ::framework::gpu::pipeline_stage_flags const * wait_dst_stage_mask, ::std::uint32_t signal_semaphore_count, class Semaphore * const * signal_semaphores, class ::framework::gpu::fence * fence)
//{
//	::framework::gpu::submit_info submit_info;
//	submit_info.command_buffer_count = 1;
//	submit_info.command_buffers = &data->command_buffer;
//	submit_info.wait_semaphore_count = wait_semaphore_count;
//	submit_info.wait_semaphores = wait_semaphores;
//	submit_info.wait_dst_stage_mask = wait_dst_stage_mask;
//	submit_info.signal_semaphore_count = signal_semaphore_count;
//	submit_info.signal_semaphores = signal_semaphores;
//	command_queue->submit(1, &submit_info, fence);
//
//	// Readback from gpu
//	if (false)
//	{
//		command_queue->wait_idle();
//
//		class Queue * transferQueue;
//		data->device->get_queue(0, 0, &transferQueue);
//
//		::framework::gpu::command_pool_create_info command_pool_create_info;
//		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
//		command_pool_create_info.queue_family_index = 0;
//		class CommandPool * command_pool;
//		data->device->create_command_pool(&command_pool_create_info, &command_pool);
//
//		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
//		command_buffer_allocate_info.command_pool = command_pool;
//		command_buffer_allocate_info.level = primary;
//		command_buffer_allocate_info.command_buffer_count = 1;
//		class CommandBuffer * copyCmd;
//
//		data->device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
//		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
//		copyCmd->begin_command_buffer(&command_buffer_begin_info);
//
//		::framework::gpu::buffer_copy buffer_copy{};
//		buffer_copy.size = data->m_pWorkBuffer2Debug->GetBufferCreateInfo()->size;
//		copyCmd->copy_buffer(data->m_pWorkBuffer, data->m_pWorkBuffer2Debug, 1, &buffer_copy);
//		copyCmd->end_command_buffer();
//
//		class ::framework::gpu::fence * fence;
//		::framework::gpu::fence_create_info fence_create_info;
//		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
//		data->device->create_fence(&fence_create_info, nullptr, &fence);
//
//		::framework::gpu::submit_info submit_info;
//		submit_info.wait_semaphore_count = 0;
//		submit_info.wait_semaphores = nullptr;
//		submit_info.wait_dst_stage_mask = nullptr;
//		submit_info.command_buffer_count = 1;
//		submit_info.command_buffers = &copyCmd;
//		submit_info.signal_semaphore_count = 0;
//		submit_info.signal_semaphores = nullptr;
//		transferQueue->submit(1, &submit_info, fence);
//
//		data->device->wait_for_fences(1, &fence, true, UINT64_MAX);
//		data->device->destroy_fence(fence);
//
//		::std::vector<::std::uint32_t> dataVec(NUM_WGS);
//
//		void * pMappedData;
//		data->device->map_memory(data->m_pWorkBuffer2DebugMemory, 0, buffer_copy.size, 0, &pMappedData);
//		::std::memcpy(dataVec.data(), pMappedData, buffer_copy.size);
//		data->device->unmap_memory(data->m_pWorkBuffer2DebugMemory);
//
//		for (::std::size_t i = 0; i < NUM_WGS; i++)
//			::std::cout << /*i << "= " <<*/ dataVec[i] << ", ";
//			//::std::cout << /*i << "= " <<*/ dataVec[i] << (((i + 1) % WG_SIZE) ? ", " : "\n");
//		system("pause");
//	}
//}
//
//void HrsPrefixScan::recordCommands(Data * data, class CommandBuffer * command_buffer, ::std::uint32_t setIndex, ::std::uint32_t invocationIndex)
//{
//	data->m_prefixScanUB.m_invocationIndex = invocationIndex;
//
//	{//	prefix scan group histogram
//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].buffer = data->m_pWorkBuffer;
//		buffer_memory_barriers[0].offset = 0;
//		buffer_memory_barriers[0].size = data->m_pWorkBuffer->GetBufferCreateInfo()->size;
//
//		command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//		command_buffer->bind_pipeline(data->m_pLocalScanPso);
//		command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pLocalScanDescriptorTable, 0, nullptr);
//		command_buffer->push_constants(data->m_pLocalScanPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(data->m_prefixScanUB), &data->m_prefixScanUB);
//		command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * invocationIndex);
//	}
//
//	{// top level scan count
//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].buffer = data->m_pWorkBuffer;
//		buffer_memory_barriers[0].offset = 0;
//		buffer_memory_barriers[0].size = data->m_pWorkBuffer->GetBufferCreateInfo()->size;
//
//		command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//		command_buffer->bind_pipeline(data->m_pTopLevelScanPso);
//		command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pTopLevelScanDescriptorTable, 0, nullptr);
//		command_buffer->dispatch(1, 1, 1);
//	}
//
//	{//	local sort and distribute
//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
//		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[0].buffer = data->m_pWorkBuffer;
//		buffer_memory_barriers[0].offset = 0;
//		buffer_memory_barriers[0].size = data->m_pWorkBuffer->GetBufferCreateInfo()->size;
//
//		buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
//		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
//		buffer_memory_barriers[1].buffer = data->m_pSrcDataBuffer;
//		buffer_memory_barriers[1].offset = 0;
//		buffer_memory_barriers[1].size = data->m_pSrcDataBuffer->GetBufferCreateInfo()->size;
//
//		command_buffer->dependency_info(compute_shader_bit, compute_shader_bit, 0, 0, nullptr, ::std::size(buffer_memory_barriers), buffer_memory_barriers, 0, nullptr);
//		command_buffer->bind_pipeline(data->m_pAddOffsetPso);
//		command_buffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, data->m_pAddOffsetDescriptorTable[setIndex], 0, nullptr);
//		command_buffer->push_constants(data->m_pAddOffsetPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(data->m_prefixScanUB), &data->m_prefixScanUB);
//		command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * invocationIndex);
//	}
//}