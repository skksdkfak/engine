#pragma once

#include "concurrency/thread_pool.h"
#include "RadixSort.h"
#include <glm/glm.hpp>

namespace framework
{
	struct LBVHCreateInfo
	{
		class ::framework::gpu::device * device;
		::std::uint32_t maxPrimitives;
		::std::uint32_t treeletSize;
		::std::uint32_t iterations;
	};

	class lbvh
	{
	public:
		struct Bound
		{
			::glm::vec3 min;
			::glm::float32 pad0;
			::glm::vec3 max;
			::glm::float32 pad1;
		};

		struct CompactTreeNode
		{
			::glm::uvec4 field0; // parent, left, right, triangleID
			::glm::vec4 field1; // boundMin.xyz, cost
			::glm::vec4 field2; // boundMax.xyz, area
		};

		struct CompactTriangle
		{
			::glm::vec4	field0; // vert0.xyz, t0.x
			::glm::vec4	field1; // vert1.xyz, t1.y
			::glm::vec4	field2; // vert2.xyz, materialID
			::glm::vec4	field3; // t1.xy, t2.xy
			::glm::mat3x4	tbn;
		};

		struct TransformBufferData
		{
			::glm::mat4	mObject2World;
			::glm::mat4	mWorld2Object;
			::glm::mat4	mModelView;
			::glm::mat4	mModelViewProjection;
			::glm::mat4	mTransposeModelView;
			::glm::mat4	mInverseTransposeModelView;
		};

		static void allocate(LBVHCreateInfo const * create_info, lbvh ** ppLBVH);

		static void deallocate(lbvh * pLBVH);

		void performTriangleListFillPass();

		void buildBVH();

		void update();

		::std::uint32_t getLeavesCount() const;

		class ::framework::gpu::buffer * getRadixTreeBuffer();
		class ::framework::gpu::buffer_view * getRadixTreeBufferView();

		class ::framework::gpu::buffer * getNodeBuffer();
		class ::framework::gpu::buffer_view * getNodeBufferView();

		class ::framework::gpu::buffer * getTriWoopBuffer();
		class ::framework::gpu::buffer_view * getTriWoopBufferView();

		class ::framework::gpu::buffer * getPrimitiveBuffer();
		class ::framework::gpu::buffer_view * getPrimitiveBufferView();

		class ::framework::gpu::buffer * getTriIndexBuffer();
		class ::framework::gpu::buffer_view * getTriIndexBufferView();

		class ::framework::gpu::semaphore * getBuildCompletionSemaphore();

	private:
		struct thread_data
		{
			struct CommandBufferData
			{
				bool			is_command_buffer_open;
				::std::uint32_t		drawCallCount;
			};

			class ::framework::gpu::command_pool*					command_pool;
			::std::vector<class ::framework::gpu::command_buffer*>		commandBuffers;
			::std::vector<CommandBufferData>	commandBufferData;
		};
		lbvh(LBVHCreateInfo const * create_info);
		~lbvh();

		void allocate();

		void deallocate();

		void printBVH(::std::uint32_t root, CompactTreeNode * nodes);

		bool check_bound(CompactTreeNode * p, CompactTreeNode * l, CompactTreeNode * r);

		bool check_sanity(::std::uint32_t n, CompactTreeNode * nodes);

		class ::framework::gpu::device*						device;
		const class ::framework::gpu::shader_module*			m_pTriangleListFillVertexShaderModule;
		const class ::framework::gpu::shader_module*			m_pTriangleListFillGeometryShaderModule;
		const class ::framework::gpu::shader_module*			m_pGlobalBoundComputingShader;
		const class ::framework::gpu::shader_module*			m_pReductionGlobalBoundComputingShader;
		const class ::framework::gpu::shader_module*			m_pGenerateMortonCodesShader;
		const class ::framework::gpu::shader_module*			m_pConstructRadixTreeShader;
		const class ::framework::gpu::shader_module*			m_pCalculateNodeBoundingBoxesShader;
		const class ::framework::gpu::shader_module*			m_pAgglomerativeTreeletOptimizerShader;
		const class ::framework::gpu::shader_module*			m_pAgglomerativeSmallTreeletOptimizerShader;
		const class ::framework::gpu::shader_module*			m_pCreateNodesShader;
		const class ::framework::gpu::shader_module*			m_pCreateWoopifyTrianglesShader;
		class ::framework::gpu::buffer*						m_pRadixTreeBuffer;
		class ::framework::gpu::device_memory*				m_pRadixTreeDeviceMemory;
		class ::framework::gpu::buffer*						m_pRadixTreeReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pRadixTreeReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pScheduleBuffer;
		class ::framework::gpu::device_memory*				m_pScheduleDeviceMemory;
		class ::framework::gpu::buffer*						m_pDistanceMatrixBuffer;
		class ::framework::gpu::device_memory*				m_pDistanceMatrixDeviceMemory;
		class ::framework::gpu::buffer*						m_pSubtreeTrianglesCountBuffer;
		class ::framework::gpu::device_memory*				m_pSubtreeTrianglesCountDeviceMemory;
		class ::framework::gpu::buffer*						m_pCounterBuffer;
		class ::framework::gpu::device_memory*				m_pCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pBoundsBuffer;
		class ::framework::gpu::device_memory*				m_pBoundsDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesCounterBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitiveIndicesBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitiveIndicesDeviceMemory;
		class ::framework::gpu::buffer*						m_pMortonCodesBuffer;
		class ::framework::gpu::device_memory*				m_pMortonCodesDeviceMemory;
		class ::framework::gpu::buffer*						m_pNodesBuffer;
		class ::framework::gpu::device_memory*				m_pNodesDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pNodesBufferView;
		class ::framework::gpu::buffer*						m_pTriWoopBuffer;
		class ::framework::gpu::device_memory*				m_pTriWoopDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pTriWoopBufferView;
		class ::framework::gpu::buffer*						m_pTriIndexBuffer;
		class ::framework::gpu::device_memory*				m_pTriIndexDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pTriIndexBufferView;
		class ::framework::gpu::buffer*						m_pNodesUploadBuffer;
		class ::framework::gpu::device_memory*				m_pNodesUploadDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriWoopUploadBuffer;
		class ::framework::gpu::device_memory*				m_pTriWoopUploadDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriIndexUploadBuffer;
		class ::framework::gpu::device_memory*				m_pTriIndexUploadDeviceMemory;
		//StructuredBuffer			m_primitiveBuffer;
		//StructuredBuffer			m_rays;
		//StructuredBuffer			m_hits;
		//TypedBuffer rayOrigin(DXGI_FORMAT_R32G32B32A32_FLOAT);
		//TypedBuffer rayDirection(DXGI_FORMAT_R32G32B32A32_FLOAT);
		//TypedBuffer hitIndex(DXGI_FORMAT_R32_SINT);
		//TypedBuffer hitDistance(DXGI_FORMAT_R32_FLOAT);
		//TypedBuffer hitBarycentricUV(DXGI_FORMAT_R32G32_FLOAT);
		//TypedBuffer					m_materialIndices;
		//TypedBuffer					m_nodes;
		//TypedBuffer					m_triWoop;
		//TypedBuffer					m_triIndex;
		//TypedBuffer					m_colorMask;
		//TypedBuffer					m_accumulatedColor;
		class ::framework::gpu::buffer*						m_pReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pUnsortedDataReadback;
		class ::framework::gpu::device_memory*				m_pUnsortedDataReadbackMemory;
		class ::framework::gpu::buffer*						m_pBoundsReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pBoundsReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriangleListFillUboBuffer;
		class ::framework::gpu::device_memory*				m_pTriangleListFillUboDeviceMemory;
		void*						m_pTriangleListFillUboMappedData;
		class ::framework::gpu::buffer*						m_pDebugCounterBuffer;
		class ::framework::gpu::device_memory*				m_pDebugCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pDebugBuffer;
		class ::framework::gpu::device_memory*				m_pDebugDeviceMemory;
		class ::framework::gpu::buffer*						m_pDebugReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pDebugReadbackDeviceMemory;
		class ::framework::gpu::command_pool*				this->primary_command_pool;
		class ::framework::gpu::command_buffer *				m_pPrimaryCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pPrimitiveCounterServicingCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pGlobalBoundComputingCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pGenerateMortonCodesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pConstructRadixTreeCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCalculateNodeBoundingBoxesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pAgglomerativeTreeletOptimizerCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCreateNodesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCreateWoopifyTrianglesCommandBuffer;
		class ::framework::gpu::semaphore*					m_pTriangleListFillCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pGlobalBoundComputingCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pGenerateMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pSortMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pConstructRadixTreeCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pAgglomerativeTreeletOptimizerCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCreateNodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCreateWoopifyTrianglesCompleteSemaphore;
		class ::framework::gpu::frame_buffer*					m_pFramebuffer;
		class ::framework::gpu::render_pass*					this->render_pass;
		class ::framework::gpu::pipeline_layout*				m_pTriangleListFillPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pReductionGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pGenerateMortonCodesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pConstructRadixTreePipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCalculateNodeBoundingBoxesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pAgglomerativeTreeletOptimizerPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pAgglomerativeSmallTreeletOptimizerPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCreateNodesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCreateWoopifyTrianglesPipelineLayout;
		class ::framework::gpu::pipeline *					m_pTriangleListFillPipeline;
		class ::framework::gpu::pipeline *					m_pGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline *					m_pReductionGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline *					m_pGenerateMortonCodesPipeline;
		class ::framework::gpu::pipeline *					m_pConstructRadixTreePipeline;
		class ::framework::gpu::pipeline *					m_pCalculateNodeBoundingBoxesPipeline;
		class ::framework::gpu::pipeline *					m_pAgglomerativeTreeletOptimizerPipeline;
		class ::framework::gpu::pipeline *					m_pAgglomerativeSmallTreeletOptimizerPipeline;
		class ::framework::gpu::pipeline *					m_pCreateNodesPipeline;
		class ::framework::gpu::pipeline *					m_pCreateWoopifyTrianglesPipeline;
		class ::framework::gpu::descriptor_pool*				descriptor_pool;
		class ::framework::gpu::descriptor_set_layout*		m_pTriangleListFillDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pReductionGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pGenerateMortonCodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pConstructRadixTreeDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pAgglomerativeTreeletOptimizerDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pAgglomerativeSmallTreeletOptimizerDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCreateNodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCreateWoopifyTrianglesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set*				m_pTriangleListFillDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pReductionGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pGenerateMortonCodesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pConstructRadixTreeDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCalculateNodeBoundingBoxesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pAgglomerativeTreeletOptimizerDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCreateNodesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCreateWoopifyTrianglesDescriptorSet;
		class DescriptorTable*			m_pTriangleListFillDescriptorTable;
		class DescriptorTable*			m_pGlobalBoundComputingDescriptorTable;
		class DescriptorTable*			m_pReductionGlobalBoundComputingDescriptorTable;
		class DescriptorTable*			m_pGenerateMortonCodesDescriptorTable;
		class DescriptorTable*			m_pConstructRadixTreeDescriptorTable;
		class DescriptorTable*			m_pCalculateNodeBoundingBoxesDescriptorTable;
		class DescriptorTable*			m_pAgglomerativeTreeletOptimizerDescriptorTable;
		class DescriptorTable*			m_pAgglomerativeSmallTreeletOptimizerDescriptorTable;
		class DescriptorTable*			m_pCreateNodesDescriptorTable;
		class DescriptorTable*			m_pCreateWoopifyTrianglesDescriptorTable;
		lsd_radix_sort::Data*			m_pRadixSortData;
		::std::uint32_t					thread_count;
		::std::vector<thread_data>		thread_data;
		thread_pool					thread_pool;
		class ::framework::gpu::fence*						m_pPrimitiveCounterServicingCompleteFence;
		const ::std::uint64_t				m_maxUBOs;
		const ::std::uint64_t				m_maxPrimitives;
		::std::uint32_t					m_treeletSize;
		::std::uint32_t					m_iterations;
		::std::uint32_t					m_scheduleSize;
		::std::uint32_t					m_numTriangles;
	};
}