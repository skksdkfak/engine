#include "lbvh.hpp"
#include "Renderer.h"
#include <glm/glm.hpp>
#include <stdio.h>
#include <iostream>
#include <engine_core.h>
#include <resource/resource_manager.hpp>
#include <components/Transform.h>
#include <TransformUpdateSystem.h>
#include <Camera.h>
#include <components/CameraController.h>
#include <CameraControllerSystem.h>
#include <concurrency/thread_pool.h>
#include <components/MeshRendererPathTracingBVH.h>
#if defined(_WIN32)
#include <platform/win32/connection.hpp>
#elif defined(VK_USE_PLATFORM_XCB_KHR)
#include <platform/linux/xcb/::framework::platform::linux_::xcb::connection.h>
#endif

using namespace framework;

class my_application : public ::framework::application
{
public:
	virtual void startup(void) override;
	virtual void cleanup(void) override;

	virtual bool is_done(void);

	virtual void update(float deltaT) override;

private:
	renderer_context					m_renderer;
	CEntity						m_gameObject;
	::std::uint32_t					m_fps;
	float						delta_time;
	::std::uint32_t					width, height;

};

CREATE_APPLICATION(my_application)

void my_application::startup(void)
{
	::framework::resource::g_resource_manager.AddDirectory("../data/shaders/glsl/spv/");
	::framework::resource::g_resource_manager.AddDirectory("../data/shaders/");
	::framework::resource::g_resource_manager.AddDirectory("../data/models/");
	::framework::resource::g_resource_manager.AddDirectory("../data/scenes/");
	::framework::resource::g_resource_manager.AddDirectory("../data/sounds/");
	::framework::resource::g_resource_manager.AddDirectory("../data/textures/");
	::framework::resource::g_resource_manager.AddDirectory("../data/levels/");

	// SYSTEMS SETUP
	SystemManager::add<CTransformUpdateSystem, 0>();
	SystemManager::add<CameraControllerSystem, 1>();

	m_renderer.initialize();

	{
		//Device * device = m_renderer.getDevice();
		//Buffer * buffer;
		//DeviceMemory * device_memory;

		//::framework::gpu::buffer_create_info buffer_create_info;
		//buffer_create_info.size = sizeof(float) * numberOfWarps * numberOfElements;
		//buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		//buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		//buffer_create_info.queue_family_index_count = 0;
		//buffer_create_info.queue_family_indices = nullptr;
		//device->create_buffer(&buffer_create_info, nullptr, &buffer);

		//::framework::gpu::memory_requirements memory_requirements;
		//device->get_buffer_memory_requirements(buffer, &memory_requirements);

		//::framework::gpu::memory_allocate_info memory_allocate_info;
		//memory_allocate_info.allocation_size = memory_requirements.size;
		//memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		//memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		//memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		//device->allocate_memory(&memory_allocate_info, nullptr, &device_memory);
		//device->bind_buffer_memory(buffer, device_memory, 0);
	}

	m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.get_device(), nullptr, nullptr, "house_2_from_obj.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "Duty Exoskeleton.obj");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "sponza.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "head.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "buddha.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "CornellBox-Water.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "suzanne.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "cs_office.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "hairball.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "Scene_1_from_obj.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "minecraft.obj");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "sportsCar.fbx");
	//m_gameObject1 = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "teapot.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "House_4.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "lost_empire.fbx");
	//m_gameObject = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "Chess_Set.fbx");
	//auto obj1 = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "buddha.fbx");
	//auto obj2 = ::framework::resource::g_resource_manager.load_fbx_scene(m_renderer.getDevice(), nullptr, nullptr, "lost_empire.fbx");
#if defined(_WIN32)
    width = ::framework::platform::win32::connection::width;
	height = ::framework::platform::win32::connection::height;
#elif defined(VK_USE_PLATFORM_XCB_KHR)
    width = 1280;
	height = 1024;
#endif
}

void my_application::cleanup(void)
{
}

bool my_application::is_done(void)
{
	return !::framework::input::input_listner::getKeyState(eKeycode::KEY_Escape);
}

void my_application::update(float deltaT)
{
//	if (::framework::platform::win32::connection::width != width || ::framework::platform::win32::connection::height != height) {
//		width = ::framework::platform::win32::connection::width;
//		height = ::framework::platform::win32::connection::height;
//		m_renderer.resize(width, height);
//	}
	/*if (::framework::input::input_listner::getKeyState(eKeycode::KEY_Up))
	{
		m_renderer.resize(m_renderer.get_width(), m_renderer.get_height() - 5);
	}
	else if (::framework::input::input_listner::getKeyState(eKeycode::KEY_Down))
	{
		m_renderer.resize(m_renderer.get_width(), m_renderer.get_height() + 5);
	}

	if (::framework::input::input_listner::getKeyState(eKeycode::KEY_Left))
	{
		m_renderer.resize(m_renderer.get_width() - 5, m_renderer.get_height());
	}
	else if (::framework::input::input_listner::getKeyState(eKeycode::KEY_Right))
	{
		m_renderer.resize(m_renderer.get_width() + 5, m_renderer.get_height());
	}*/

	delta_time += deltaT;
	m_fps++;
	if (delta_time >= 1.0f)
	{
		::std::cout << m_fps << ::std::endl;
		delta_time = 0;
		m_fps = 0;
	}

	m_renderer.update();
}