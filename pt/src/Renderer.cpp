#include <iostream>
#include <iterator>
#include <resource/resource_manager.hpp>
#include <Camera.h>
#include <components/CameraController.h>
#include <CameraControllerSystem.h>
#include "Renderer.h"

#define COMMAND_BUFFERS_PER_THREAD 1
#define USE_SECONDARY_COMMAND_BUFFERS true

using namespace framework;

renderer_context::renderer_context() :
	width(1280),
	height(720)
{
}

renderer_context::~renderer_context()
{
}

void renderer_context::initialize()
{
	m_mainCamera = entity_manager::create<CEntity>();
	auto cameraTransform = m_mainCamera.assign<CTransform>();
	auto camera = m_mainCamera.assign<camera>();
	camera->setZRange(0.01f, 1024.f);
	camera->reverseZ(false);
	m_mainCamera.assign<CameraController>(::glm::vec3(0.0f, 1.0f, 0.0f));

	::framework::platform::window_create_info windowCreateInfo;
	windowCreateInfo.x = 0;
	windowCreateInfo.y = 0;
	windowCreateInfo.width = width;
	windowCreateInfo.height = height;
	windowCreateInfo.border_width = 0;
	windowCreateInfo.full_screen = false;
	::framework::platform::connection::create_window(&window, &windowCreateInfo);

	int deviceTypeIdx;
	::std::cout << "Choose class Device type:\n0 - Vulkan class Device;\n1 - DirectX 12 class Device;\n";
	::std::cin >> deviceTypeIdx;
	gapi_type deviceType;
	switch (deviceTypeIdx)
	{
	case 0:
		deviceType = vulkan;
		::std::cout << "Vulkan class Device was chosen\n";
		break;
	case 1:
		deviceType = d3d12;
		::std::cout << "DirectX 12 class Device was chosen\n";
		break;
	default:
		deviceType = vulkan;
		::std::cout << "Index out of range\n";
		::std::cout << "Vulkan class Device was chosen by default\n";
		break;
	}
	camera->inverseY(deviceType == gapi_type::vulkan);

	::std::vector<const char*> ppEnabledLayerNames =
	{
#ifdef _DEBUG
		LAYER_STANDARD_VALIDATION_LAYER_NAME
#endif
	};

	::std::vector<const char*> ppEnabledExtensionNames =
	{
#ifdef _DEBUG
		EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
	};

	ApplicationInfo application_info;
	application_info.pApplicationName = "MyTestGame";
	application_info.applicationVersion = 1;
	application_info.pEngineName = "HorhyEngine";
	application_info.engineVersion = 1;
	application_info.apiVersion = 1;

	::framework::gpu::instance_create_info instance_create_info;
	instance_create_info.application_info = &application_info;
	instance_create_info.enabled_layer_count = static_cast<::std::uint32_t>(ppEnabledLayerNames.size());
	instance_create_info.enabled_layer_names = ppEnabledLayerNames.data();
	instance_create_info.enabled_extension_count = static_cast<::std::uint32_t>(ppEnabledExtensionNames.size());
	instance_create_info.enabled_extension_names = ppEnabledExtensionNames.data();
	InstanceManager::create_instance(deviceType, &instance_create_info, &instance);

	instance->enumerate_physical_devices(&physical_device_count, nullptr);
	assert(physical_device_count > 0);
	m_physicalDeviceIndex = 0;
	physical_devices.resize(physical_device_count);
	instance->enumerate_physical_devices(&physical_device_count, physical_devices.data());

	::std::uint32_t queue_family_property_count;
    physical_devices[m_physicalDeviceIndex]->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);
	queue_family_properties.resize(queue_family_property_count);
    physical_devices[m_physicalDeviceIndex]->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

    physical_devices[m_physicalDeviceIndex]->get_memory_properties(&physical_device_memory_properties);

	::std::vector<::framework::gpu::device_queue_create_info> queueCreateInfos{};
	const float defaultQueuePriority(0.0f);
	queue_family_indices.graphics = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit);
	::framework::gpu::device_queue_create_info device_queue_create_info;
	device_queue_create_info.queue_family_index = queue_family_indices.graphics;
	device_queue_create_info.queue_count = 1;
	device_queue_create_info.queue_priorities = &defaultQueuePriority;
	queueCreateInfos.push_back(device_queue_create_info);

	queue_family_indices.compute = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit);
	if (queue_family_indices.compute != queue_family_indices.graphics)
	{
		// If compute family index differs, we need an additional class Queue create info for the compute class Queue
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.queue_family_index = queue_family_indices.compute;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		queueCreateInfos.push_back(device_queue_create_info);
	}

	queue_family_indices.transfer = Utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit);
	if ((queue_family_indices.transfer != queue_family_indices.graphics) && (queue_family_indices.transfer != queue_family_indices.compute))
	{
		// If compute family index differs, we need an additional class Queue create info for the compute class Queue
		::framework::gpu::device_queue_create_info device_queue_create_info;
		device_queue_create_info.queue_family_index = queue_family_indices.transfer;
		device_queue_create_info.queue_count = 1;
		device_queue_create_info.queue_priorities = &defaultQueuePriority;
		queueCreateInfos.push_back(device_queue_create_info);
	}

	float deviceQueuesPriority = 0.0f;
	::framework::gpu::device_queue_create_info deviceQueueCreateInfos[2];
	deviceQueueCreateInfos[0].queue_family_index = queue_family_indices.graphics;
	deviceQueueCreateInfos[0].queue_count = 1;
	deviceQueueCreateInfos[0].queue_priorities = &deviceQueuesPriority;
	//deviceQueueCreateInfos[1].queue_flags = ::framework::gpu::queue_flags::compute_bit;
	//deviceQueueCreateInfos[1].queue_count = 1;
	//deviceQueueCreateInfos[1].queue_priorities = &deviceQueuesPriority;
	deviceQueueCreateInfos[1].queue_family_index = queue_family_indices.transfer;
	deviceQueueCreateInfos[1].queue_count = 1;
	deviceQueueCreateInfos[1].queue_priorities = &deviceQueuesPriority;

	::framework::gpu::device_create_info deviceCreateInfo;
	deviceCreateInfo.deviceIdx = 0;
	deviceCreateInfo.queue_create_info_count = ::std::size(deviceQueueCreateInfos);
	deviceCreateInfo.queue_create_infos = deviceQueueCreateInfos;

    physical_devices[m_physicalDeviceIndex]->create_device(&deviceCreateInfo, &device);

    device->get_queue(queue_family_indices.graphics, 0, &queue);

	::framework::gpu::surface_create_info surface_create_info;
	surface_create_info.window = window;
	instance->create_surface(&surface_create_info, &surface);

	::framework::gpu::surface_capabilities surfaceCapabilities;
    physical_devices[0]->get_physical_device_surface_capabilities(surface, &surfaceCapabilities);
	width = surfaceCapabilities.current_extent.width;
	height = surfaceCapabilities.current_extent.height;

	createSwapChain();

	LBVHCreateInfo lbvhCreateInfo;
	lbvhCreateInfo.device = device;
	lbvhCreateInfo.maxPrimitives = 1 << 21;
	lbvhCreateInfo.treeletSize = 9;
	lbvhCreateInfo.iterations = 2;
	lbvh::allocate(&lbvhCreateInfo, &m_bvh);

	TracerCreateInfo tracerCreateInfo;
	tracerCreateInfo.device = device;
	tracerCreateInfo.pBvh = m_bvh;
	tracerCreateInfo.camera = m_mainCamera.component<camera>().get();
	tracerCreateInfo.cameraTransform = cameraTransform.get();
	Tracer::allocate(&tracerCreateInfo, &m_tracer);

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_pool_sizes[0].descriptor_count = 1;
	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.max_sets = 1;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
    device->create_descriptor_pool(&descriptor_pool_create_info, &descriptor_pool);

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
    device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pFullScreenQuadDescriptorSetLayout);

	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.descriptor_set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
	pipeline_layout_create_info.push_constant_range_count = 0;
	pipeline_layout_create_info.push_constant_ranges = nullptr;
    device->create_pipeline_layout(&pipeline_layout_create_info, &m_pFullScreenQuadPipelineLayout);

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
	descriptor_set_allocate_info.set_layouts = &m_pFullScreenQuadDescriptorSetLayout;
    device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pFullScreenQuadDescriptorSet);

	DescriptorTableCreateInfo descriptorTableCreateInfo;
	descriptorTableCreateInfo.pipeline_layout = m_pFullScreenQuadPipelineLayout;
	device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pFullScreenQuadDescriptorTable);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_tracer->GetGetRayTracedSampler();
	descriptor_image_info.image_view = m_tracer->GetRayTracedImageView();
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = m_pFullScreenQuadDescriptorSet;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	write_descriptor_sets[0].image_info = &descriptor_image_info;
    device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

	device->UpdateDescriptorTable(m_pFullScreenQuadDescriptorTable, 0, 1, &m_pFullScreenQuadDescriptorSet);

	{
		::framework::gpu::attachment_description attachment_descriptions[2];
		// Color attachment
		attachment_descriptions[0].format = swap_chain_images[0]->GetImageCreateInfo()->format;
		attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::present_src_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::present_src_bit;

		// Depth attachment
		attachment_descriptions[1].format = ::framework::gpu::format::d32_sfloat;
		attachment_descriptions[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[1].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[1].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[1].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[1].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		::framework::gpu::attachment_reference color_reference[1];
		color_reference[0].attachment = 0;
		color_reference[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::attachment_reference depthReference[1];
		depthReference[0].attachment = 1;
		depthReference[0].layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		PipelineBarrier pipelineBarriers[2];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;

		AttachmentMemoryBarrier attachmentMemoryBarriers[2];
		attachmentMemoryBarriers[0].pipelineBarrierCount = 1;
		attachmentMemoryBarriers[0].pPipelineBarriers = &pipelineBarriers[0];
		attachmentMemoryBarriers[0].attachment = 0;

		attachmentMemoryBarriers[1].pipelineBarrierCount = 1;
		attachmentMemoryBarriers[1].pPipelineBarriers = &pipelineBarriers[1];
		attachmentMemoryBarriers[1].attachment = 0;

		// Setup subpass dependencies
		// These will add the implicit attachment layout transitionss specified by the attachment descriptions
		// The actual usage layout is preserved through the layout specified in the attachment reference
		// Each subpass dependency will introduce a memory and execution_barrier dependency between the source and dest subpass described by
		// src_stage_mask, dst_stage_mask, src_access_mask, dst_access_mask (and dependency_flags is set)
		// Note: ::framework::gpu::subpass_external is a special constant that refers to all commands executed outside of the actual class RenderPass)
		::framework::gpu::subpass_dependency dependencies[2];
		// First dependency at the start of the class RenderPass
		// Does the transition from final to initial layout
		dependencies[0].attachmentMemoryBarrierCount = 1;
		dependencies[0].pAttachmentMemoryBarriers = &attachmentMemoryBarriers[0];
		dependencies[0].src_subpass = ::framework::gpu::subpass_external;									// Producer of the dependency
		dependencies[0].dst_subpass = 0;													// Consumer is our single subpass that will wait for the execution_barrier depdendency
		dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		// Second dependency at the end the class RenderPass
		// Does the transition from the initial to the final layout
		dependencies[1].attachmentMemoryBarrierCount = 1;
		dependencies[1].pAttachmentMemoryBarriers = &attachmentMemoryBarriers[1];
		dependencies[1].src_subpass = 0;													// Producer of the dependency is our single subpass
		dependencies[1].dst_subpass = ::framework::gpu::subpass_external;									// Consumer are all commands outside of the class RenderPass
		dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		::framework::gpu::subpass_description subpassDescriptions[1];
		subpassDescriptions[0].color_attachment_count = ::std::size(color_reference);
		subpassDescriptions[0].color_attachments = color_reference;
		subpassDescriptions[0].depth_stencil_attachment = depthReference;
		subpassDescriptions[0].input_attachment_count = 0;
		subpassDescriptions[0].input_attachments = nullptr;
		subpassDescriptions[0].resolve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
		render_pass_create_info.attachments = attachment_descriptions;
		render_pass_create_info.subpass_count = ::std::size(subpassDescriptions);
		render_pass_create_info.subpasses = subpassDescriptions;
		render_pass_create_info.dependency_count = ::std::size(dependencies);
		render_pass_create_info.dependencies = dependencies;
        device->create_render_pass(&render_pass_create_info, &m_pFullScreenQuadRenderPass);
	}

	m_pFullScreenQuadVertexShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "FullScreenQuad.vert.sdr");
	m_pFullScreenQuadFragmentShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "FullScreenQuad.frag.sdr");

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].module = m_pFullScreenQuadVertexShaderModule;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
	pipeline_shader_stage_create_infos[1].module = m_pFullScreenQuadFragmentShaderModule;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;

	::framework::gpu::viewport viewport{ 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
	::framework::gpu::rect_2d scissor{ { 0, 0 },{ width, height } };
	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::front_bit;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::dynamic_state dynamicStates[] =
	{
		::framework::gpu::dynamic_state::viewport,
		::framework::gpu::dynamic_state::scissor
	};
	::framework::gpu::pipeline_dynamic_state_create_info pipelineDynamicStateCreateInfo;
	pipelineDynamicStateCreateInfo.dynamic_states = dynamicStates;
	pipelineDynamicStateCreateInfo.dynamic_state_count = static_cast<::std::uint32_t>(::std::size(dynamicStates));

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
	pipeline_color_blend_attachment_state.blend_enable = false;
	pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.color_write_mask =
		::framework::gpu::color_component_flags::r_bit |
		::framework::gpu::color_component_flags::g_bit |
		::framework::gpu::color_component_flags::b_bit |
		::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo;
	pipelineDepthStencilStateCreateInfo.depth_test_enable = false;
	pipelineDepthStencilStateCreateInfo.depth_write_enable = false;
	pipelineDepthStencilStateCreateInfo.depth_compare_op = ::framework::gpu::compare_op::never;
	pipelineDepthStencilStateCreateInfo.depth_bounds_test_enable = false;
	pipelineDepthStencilStateCreateInfo.stencil_test_enable = false;
	pipelineDepthStencilStateCreateInfo.front = {};
	pipelineDepthStencilStateCreateInfo.back = {};
	pipelineDepthStencilStateCreateInfo.back.compare_op = ::framework::gpu::compare_op::always;
	pipelineDepthStencilStateCreateInfo.min_depth_bounds = 0.0f;
	pipelineDepthStencilStateCreateInfo.max_depth_bounds = 0.0f;

	::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
	vertex_input_state.vertex_binding_description_count = 0;
	vertex_input_state.vertex_binding_descriptions = nullptr;
	vertex_input_state.vertex_attribute_description_count = 0;
	vertex_input_state.vertex_attribute_descriptions = nullptr;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info{};
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = &pipelineDynamicStateCreateInfo;
	graphics_pipeline_create_info.layout = m_pFullScreenQuadPipelineLayout;
	graphics_pipeline_create_info.render_pass = m_pFullScreenQuadRenderPass;
    device->create_graphics_pipelines(&graphics_pipeline_create_info, 1, &m_pFullScreenQuadGraphicsPipeline);

	{
		::framework::gpu::attachment_description attachment_descriptions[2];
		// Color attachment
		attachment_descriptions[0].format = swap_chain_images[0]->GetImageCreateInfo()->format;
		attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::present_src_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::present_src_bit;

		// Depth attachment
		attachment_descriptions[1].format = ::framework::gpu::format::d32_sfloat;
		attachment_descriptions[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		attachment_descriptions[1].load_op = ::framework::gpu::attachment_load_op::clear;
		attachment_descriptions[1].store_op = ::framework::gpu::attachment_store_op::store;
		attachment_descriptions[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
		attachment_descriptions[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
		attachment_descriptions[1].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		attachment_descriptions[1].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		::framework::gpu::attachment_reference color_reference[1];
		color_reference[0].attachment = 0;
		color_reference[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

		::framework::gpu::attachment_reference depthReference[1];
		depthReference[0].attachment = 1;
		depthReference[0].layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

		PipelineBarrier pipelineBarriers[2];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;

		AttachmentMemoryBarrier attachmentMemoryBarriers[2];
		attachmentMemoryBarriers[0].pipelineBarrierCount = 1;
		attachmentMemoryBarriers[0].pPipelineBarriers = &pipelineBarriers[0];
		attachmentMemoryBarriers[0].attachment = 0;

		attachmentMemoryBarriers[1].pipelineBarrierCount = 1;
		attachmentMemoryBarriers[1].pPipelineBarriers = &pipelineBarriers[1];
		attachmentMemoryBarriers[1].attachment = 0;

		// Setup subpass dependencies
		// These will add the implicit attachment layout transitionss specified by the attachment descriptions
		// The actual usage layout is preserved through the layout specified in the attachment reference
		// Each subpass dependency will introduce a memory and execution_barrier dependency between the source and dest subpass described by
		// src_stage_mask, dst_stage_mask, src_access_mask, dst_access_mask (and dependency_flags is set)
		// Note: ::framework::gpu::subpass_external is a special constant that refers to all commands executed outside of the actual class RenderPass)
		::framework::gpu::subpass_dependency dependencies[2];
		// First dependency at the start of the class RenderPass
		// Does the transition from final to initial layout
		dependencies[0].attachmentMemoryBarrierCount = 1;
		dependencies[0].pAttachmentMemoryBarriers = &attachmentMemoryBarriers[0];
		dependencies[0].src_subpass = ::framework::gpu::subpass_external;									// Producer of the dependency
		dependencies[0].dst_subpass = 0;													// Consumer is our single subpass that will wait for the execution_barrier depdendency
		dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		// Second dependency at the end the class RenderPass
		// Does the transition from the initial to the final layout
		dependencies[1].attachmentMemoryBarrierCount = 1;
		dependencies[1].pAttachmentMemoryBarriers = &attachmentMemoryBarriers[1];
		dependencies[1].src_subpass = 0;													// Producer of the dependency is our single subpass
		dependencies[1].dst_subpass = ::framework::gpu::subpass_external;									// Consumer are all commands outside of the class RenderPass
		dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

		::framework::gpu::subpass_description subpassDescriptions[1];
		subpassDescriptions[0].color_attachment_count = ::std::size(color_reference);
		subpassDescriptions[0].color_attachments = color_reference;
		subpassDescriptions[0].depth_stencil_attachment = depthReference;
		subpassDescriptions[0].input_attachment_count = 0;
		subpassDescriptions[0].input_attachments = nullptr;
		subpassDescriptions[0].resolve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
		render_pass_create_info.attachments = attachment_descriptions;
		render_pass_create_info.subpass_count = ::std::size(subpassDescriptions);
		render_pass_create_info.subpasses = subpassDescriptions;
		render_pass_create_info.dependency_count = ::std::size(dependencies);
		render_pass_create_info.dependencies = dependencies;
        device->create_render_pass(&render_pass_create_info, &this->render_pass);
	}

	setupDepthStencil();
	setupFrameBuffer();

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = queue_family_indices.graphics;
    device->create_command_pool(&command_pool_create_info, &this->command_pool);

	createCommandBuffers();

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = 0/*::framework::gpu::fence_create_flags::signaled_bit*/;
	device->create_fence(&fence_create_info, nullptr, &fence[0]);
	device->create_fence(&fence_create_info, nullptr, &fence[1]);
	device->create_fence(&fence_create_info, nullptr, &fence[2]);
	device->create_semaphore(&this->render_complete_semaphores[0]);
	device->create_semaphore(&this->render_complete_semaphores[1]);
	device->create_semaphore(&this->render_complete_semaphores[2]);
	device->create_semaphore(&present_complete_semaphore);
	device->create_semaphore(&m_pPathTracingCompleteSemaphore);
}

void renderer_context::update()
{
	::framework::gpu::pipeline_stage_flags waitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	class ::framework::gpu::semaphore * pBvhBuildCompletionSemaphore = m_bvh->getBuildCompletionSemaphore();
	m_bvh->update();
	m_tracer->trace(1, &pBvhBuildCompletionSemaphore, &waitDstStageMask, 1, &m_pPathTracingCompleteSemaphore);

	::framework::gpu::acquire_next_image_info acquireNextImageInfo{};
	acquireNextImageInfo.swap_chain = swap_chain;
	acquireNextImageInfo.fence = nullptr;
	acquireNextImageInfo.semaphore = present_complete_semaphore;
	acquireNextImageInfo.timeout = UINT64_MAX;
	device->acquire_next_image(&acquireNextImageInfo, &this->swap_chain_image);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::simultaneous_use_bit | ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	
	::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ width, height } };

	::framework::gpu::clear_value clearValues[2];
	clearValues[0].color.float32[0] = 1.0f;
	clearValues[0].color.float32[1] = 0.6f;
	clearValues[0].color.float32[2] = 0.1f;
	clearValues[0].color.float32[3] = 1.0f;
	clearValues[1].depth_stencil = { 1.0f, 0 };

	::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	renderPassBeginInfo.frame_buffer = this->frame_buffers[this->swap_chain_image];
	renderPassBeginInfo.render_pass = m_pFullScreenQuadRenderPass;
	renderPassBeginInfo.render_area = render_area;
	renderPassBeginInfo.clear_value_count = ::std::size(clearValues);
	renderPassBeginInfo.clear_values = clearValues;

	PipelineBarrier pipelineBarrier;
	pipelineBarrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
	pipelineBarrier.src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	pipelineBarrier.dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;

	::framework::gpu::image_memory_barrier image_memory_barrier;
	image_memory_barrier.pipelineBarrierCount = 1;
	image_memory_barrier.pPipelineBarriers = &pipelineBarrier;
	image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	image_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
	image_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
	image_memory_barrier.image = m_tracer->GetRayTracedImage();
	image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

	::framework::gpu::dependency_info dependency_info;
	dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = 1;
	dependency_info.image_memory_barriers = &image_memory_barrier;

	::framework::gpu::viewport viewport;
	viewport.x = 0;
	viewport.y = 0;
	viewport.width = (float)width;
	viewport.height = (float)height;
	viewport.min_depth = (float)0.0f;
	viewport.max_depth = (float)1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = width;
	scissor.extent.height = height;

	this->command_buffers[this->swap_chain_image]->begin_command_buffer(&command_buffer_begin_info);
    this->command_buffers[this->swap_chain_image]->pipeline_barrier(0, 1, &dependency_info);
    this->command_buffers[this->swap_chain_image]->bind_pipeline(m_pFullScreenQuadGraphicsPipeline);
    this->command_buffers[this->swap_chain_image]->begin_render_pass(&renderPassBeginInfo, ::framework::gpu::subpass_contents::inline_);
    this->command_buffers[this->swap_chain_image]->set_viewport(0, 1, &viewport);
    this->command_buffers[this->swap_chain_image]->set_scissor(0, 1, &scissor);
	this->command_buffers[this->swap_chain_image]->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pFullScreenQuadDescriptorTable, 0, nullptr);
    this->command_buffers[this->swap_chain_image]->draw(3, 1, 0, 0);
    this->command_buffers[this->swap_chain_image]->end_render_pass();
	this->command_buffers[this->swap_chain_image]->end_command_buffer();

	class ::framework::gpu::semaphore * wait_semaphores[] = { present_complete_semaphore, m_pPathTracingCompleteSemaphore };
	::framework::gpu::pipeline_stage_flags pWaitDstStageMask[] = { ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit, ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit };

	::framework::gpu::submit_info submit_info;
	submit_info.wait_semaphore_count = 2;
	submit_info.wait_semaphores = wait_semaphores;
	submit_info.wait_dst_stage_mask = pWaitDstStageMask;
#if USE_SECONDARY_COMMAND_BUFFERS
	submit_info.command_buffer_count = 1;
	submit_info.command_buffers = &this->command_buffers[this->swap_chain_image];
#else
	submit_info.command_buffer_count = commandBuffers.size();
	submit_info.command_buffers = commandBuffers.data();
#endif
	submit_info.signal_semaphore_count = 1;
	submit_info.signal_semaphores = &this->render_complete_semaphores[this->swap_chain_image];
	queue->submit(1, &submit_info, fence[this->swap_chain_image]);

    device->wait_for_fences(1, &fence[this->swap_chain_image], true, UINT64_MAX);
    device->reset_fences(1, &fence[this->swap_chain_image]);

	::framework::gpu::present_info presentInfo;
	presentInfo.wait_semaphore_count = 1;
	presentInfo.wait_semaphores = &this->render_complete_semaphores[this->swap_chain_image];
	presentInfo.swap_chain_count = 1;
	presentInfo.swap_chains = &swap_chain;
	presentInfo.image_indices = &this->swap_chain_image;
	presentInfo.results = nullptr;
	queue->present(&presentInfo);
}

void renderer_context::resize(::std::uint32_t width, ::std::uint32_t height)
{
	width = width;
	height = height;

	device->wait_idle();

	createSwapChain();

    device->destroy_image_view(this->depth_stencil_image_view);
    device->destroy_image(this->depth_stencil_image);
    device->free_memory(this->depth_stencil_device_memory);

	setupDepthStencil();

	::std::uint32_t swap_chain_image_count;
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);
	for (::std::uint32_t i = 0; i < swap_chain_image_count; ++i)
	{
        device->destroy_frame_buffer(this->frame_buffers[i]);
	}
	setupFrameBuffer();
	destroyCommandBuffers();
	createCommandBuffers();

	m_tracer->resize(width, height);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_tracer->GetGetRayTracedSampler();
	descriptor_image_info.image_view = m_tracer->GetRayTracedImageView();
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = m_pFullScreenQuadDescriptorSet;
	write_descriptor_set.dst_binding = 0;
	write_descriptor_set.dst_array_element = 0;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::combined_image_sampler;
	write_descriptor_set.image_info = &descriptor_image_info;
    device->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);
	device->UpdateDescriptorTable(m_pFullScreenQuadDescriptorTable, 0, 1, &m_pFullScreenQuadDescriptorSet);

	m_mainCamera.component<camera>()->setAspectRatio((float)width / (float)height);
}

::std::uint32_t renderer_context::get_width()
{
	return width;
}

::std::uint32_t renderer_context::get_height()
{
	return height;
}

class ::framework::gpu::device * renderer_context::get_device()
{
	return device;
}

void renderer_context::createSwapChain()
{
	class ::framework::gpu::swap_chain * old_swap_chain = swap_chain;
	::framework::gpu::swap_chain_create_info swap_chain_create_info;
	swap_chain_create_info.surface = surface;
	swap_chain_create_info.min_image_count = 3;
	swap_chain_create_info.image_format = ::framework::gpu::format::B8G8R8A8_UNORM;
	swap_chain_create_info.image_extent = { width, height };
	swap_chain_create_info.image_usage = ::framework::gpu::image_usage_flags::color_attachment_bit;
	swap_chain_create_info.image_sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	swap_chain_create_info.queue_family_index_count = 0;
	swap_chain_create_info.queue_family_indices = nullptr;
	swap_chain_create_info.old_swap_chain = old_swap_chain;
    device->create_swap_chain(&swap_chain_create_info, &swap_chain);
	if (old_swap_chain)
	{
		for (::std::size_t i = 0; i < swap_chain_images.size(); i++)
		{
            device->destroy_image_view(this->image_views[i]);
		}
        device->destroy_swap_chain(old_swap_chain);
	}

	::std::uint32_t swap_chain_image_count;
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);

	swap_chain_images.resize(swap_chain_image_count);
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, swap_chain_images.data());
}

void renderer_context::setupDepthStencil()
{
	::framework::gpu::image_create_info image_create_info;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = ::framework::gpu::format::d32_sfloat;
	image_create_info.extent = { width, height, 1 };
	image_create_info.mip_levels = 1;
	image_create_info.array_layers = 1;
	image_create_info.samples = sample_count_1_bit;
	image_create_info.tiling = optimal;
	image_create_info.usage = depth_stencil_attachment_bit | transfer_src_bit;
	image_create_info.sharing_mode = SHARING_MODE_EXCLUSIVE;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
    device->create_image(&image_create_info, nullptr, &this->depth_stencil_image);

	::framework::gpu::memory_requirements memory_requirements;
    device->get_image_memory_requirements(this->depth_stencil_image, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	device->allocate_memory(&memory_allocate_info, nullptr, &this->depth_stencil_device_memory);
    device->bind_image_memory(this->depth_stencil_image, this->depth_stencil_device_memory, 0);

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.image = this->depth_stencil_image;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = this->depth_stencil_image->GetImageCreateInfo()->format;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::depth_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
    device->create_image_view(&image_view_create_info, nullptr, &this->depth_stencil_image_view);
}

void renderer_context::setupFrameBuffer()
{
	::std::uint32_t swap_chain_image_count;
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);

	this->image_views.resize(swap_chain_image_count);
	this->frame_buffers.resize(swap_chain_image_count);
	for (::std::uint32_t i = 0; i < swap_chain_image_count; i++)
	{
		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.image = swap_chain_images[i];
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = swap_chain_images[i]->GetImageCreateInfo()->format;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
        device->create_image_view(&image_view_create_info, nullptr, &this->image_views[i]);

		class ::framework::gpu::image_view const * attachments[2];
		attachments[0] = this->image_views[i];
		attachments[1] = this->depth_stencil_image_view;

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.width = width;
		frame_buffer_create_info.height = height;
		frame_buffer_create_info.attachment_count = ::std::size(attachments);
		frame_buffer_create_info.attachments = attachments;
		frame_buffer_create_info.render_pass = this->render_pass;
        device->create_frame_buffer(&frame_buffer_create_info, &this->frame_buffers[i]);
	}
}

void renderer_context::createCommandBuffers()
{
	::std::uint32_t swap_chain_image_count;
    device->get_swap_chain_images(swap_chain, &swap_chain_image_count, nullptr);
	this->command_buffers.resize(swap_chain_image_count);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.command_buffer_count = swap_chain_image_count;
    device->allocate_command_buffers(&command_buffer_allocate_info, this->command_buffers.data());
}

void renderer_context::destroyCommandBuffers()
{
    device->free_command_buffers(this->command_pool, this->command_buffers.size(), this->command_buffers.data());
}