#include "Tracer.h"
#include "resource/resource_manager.hpp"
#include <iterator>

using namespace framework;

Tracer::Tracer(TracerCreateInfo const * create_info) :
	device(create_info->device),
	m_bvh(create_info->pBvh),
	m_camera(create_info->camera),
	m_cameraTransform(create_info->cameraTransform),
	width(1280),
	height(720),
	m_samples(1),
	m_bounces(2)
{
}

Tracer::~Tracer()
{
}

void Tracer::allocate()
{
	device->get_physical_device()->GetMemoryProperties(&physical_device_memory_properties);

	setupRaySampleImage();

	::framework::gpu::sampler_create_info sampler_create_info;
	sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
	sampler_create_info.min_filter = ::framework::gpu::filter::linear;
	sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
	sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::clamp_to_border;
	sampler_create_info.mip_lod_bias = 0.0f;
	sampler_create_info.anisotropy_enable = false;
	sampler_create_info.max_anisotropy = 1.0f;
	sampler_create_info.compare_enable = false;
	sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
	sampler_create_info.min_lod = 0.0f;
	sampler_create_info.max_lod = 0.0f;
	sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
	sampler_create_info.unnormalized_coordinates = false;
    device->create_sampler(&sampler_create_info, nullptr, &m_pRaySampleSampler);

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = 0;
    device->create_command_pool(&command_pool_create_info, &this->primary_command_pool);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->primary_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.command_buffer_count = 1;
    device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPathTracingCommandBuffer);

	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(CameraBufferData) / uboAlignment) * uboAlignment + ((sizeof(CameraBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = dynamicAlignment;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pCameraUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pCameraUboBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pCameraUboDeviceMemory);
		device->bind_buffer_memory(m_pCameraUboBuffer, m_pCameraUboDeviceMemory, 0);
	}
	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(VariousBufferData) / uboAlignment) * uboAlignment + ((sizeof(VariousBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = dynamicAlignment;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pVariousUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pVariousUboBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pVariousUboDeviceMemory);
		device->bind_buffer_memory(m_pVariousUboBuffer, m_pVariousUboDeviceMemory, 0);
	}
	// PathTracing
	{
		m_pPathTracingShader = ::framework::resource::g_resource_manager.load_shader_module(device, "PathTracing.comp.sdr");

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pPathTracingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[7];
		// CameraUB
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// VariousUB
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// Nodes
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// triWoops
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// triIndices
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// primitives
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		// raySampleImage
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pPathTracingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pPathTracingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pPathTracingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pPathTracingPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pPathTracingPipeline);

		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[4];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_pool_sizes[0].descriptor_count = 2;
		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_pool_sizes[1].descriptor_count = 1;
		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_pool_sizes[2].descriptor_count = 1;
		descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		descriptor_pool_sizes[3].descriptor_count = 3;
		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.max_sets = 1;
		descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
        device->create_descriptor_pool(&descriptor_pool_create_info, &descriptor_pool);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pPathTracingDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pPathTracingDescriptorSet);

		::framework::gpu::descriptor_image_info descriptor_image_info;
		descriptor_image_info.sampler = m_pRaySampleSampler;
		descriptor_image_info.image_view = m_pRaySampleImageView;
		descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pCameraUboBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pCameraUboBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = (sizeof(CameraBufferData) / 256) * 256 + ((sizeof(CameraBufferData) % 256) > 0 ? 256 : 0);
		descriptor_buffer_infos[1].buffer = m_pVariousUboBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pVariousUboBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = (sizeof(VariousBufferData) / 256) * 256 + ((sizeof(VariousBufferData) % 256) > 0 ? 256 : 0);
		descriptor_buffer_infos[2].buffer = m_bvh->getPrimitiveBuffer();
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_bvh->getPrimitiveBuffer()->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(lbvh::CompactTriangle);

		class ::framework::gpu::buffer_view * pNodeBufferView[] = { m_bvh->getNodeBufferView() };
		class ::framework::gpu::buffer_view * pTriWoopBufferView[] = { m_bvh->getTriWoopBufferView() };
		class ::framework::gpu::buffer_view * pTriIndexBufferView[] = { m_bvh->getTriIndexBufferView() };
		::framework::gpu::write_descriptor_set write_descriptor_sets[7];
		write_descriptor_sets[0].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		write_descriptor_sets[2].texel_buffer_view = pNodeBufferView;
		write_descriptor_sets[3].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		write_descriptor_sets[3].texel_buffer_view = pTriWoopBufferView;
		write_descriptor_sets[4].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
		write_descriptor_sets[4].texel_buffer_view = pTriIndexBufferView;
		write_descriptor_sets[5].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[5].dst_binding = 5;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[6].dst_set = m_pPathTracingDescriptorSet;
		write_descriptor_sets[6].dst_binding = 6;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		write_descriptor_sets[6].image_info = &descriptor_image_info;
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pPathTracingPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pPathTracingDescriptorTable);
		device->UpdateDescriptorTable(m_pPathTracingDescriptorTable, 0, 1, &m_pPathTracingDescriptorSet);
	}

	{
		class Queue * command_queue;
        device->get_queue(0, 0, &command_queue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = 0;
		command_pool_create_info.queue_family_index = 0;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * command_buffer;
        device->allocate_command_buffers(&command_buffer_allocate_info, &command_buffer);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer->begin_command_buffer(&command_buffer_begin_info);
		//::framework::gpu::image_memory_barrier image_memory_barrier;
		//image_memory_barrier.src_access_mask = 0;
		//image_memory_barrier.dst_access_mask = 0;
		//image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		//image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
		//image_memory_barrier.src_queue_family_index = 0;
		//image_memory_barrier.dst_queue_family_index = 0;
		//image_memory_barrier.image = m_pRaySampleImage;
		//image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };
		//command_buffer->dependency_info(::framework::gpu::pipeline_stage_flags::all_commands_bit, ::framework::gpu::pipeline_stage_flags::all_commands_bit, 0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
		command_buffer->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info{};
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::submit_info submit_info{};
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &command_buffer;
		command_queue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);

        device->free_command_buffers(command_pool, 1, &command_buffer);
        device->destroy_command_pool(command_pool);
        device->destroy_fence(fence);
	}
}

void Tracer::setupRaySampleImage()
{
	::framework::gpu::image_create_info image_create_info;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = ::framework::gpu::format::B8G8R8A8_UNORM;
	image_create_info.extent = { width, height, 1 };
	image_create_info.mip_levels = 1;
	image_create_info.array_layers = 1;
	image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
	image_create_info.usage = ::framework::gpu::image_usage_flags::sampled_bit | ::framework::gpu::image_usage_flags::storage_bit;
	image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
    device->create_image(&image_create_info, nullptr, &m_pRaySampleImage);

	::framework::gpu::memory_requirements memory_requirements;
    device->get_image_memory_requirements(m_pRaySampleImage, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type(&physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	memory_allocate_info.memory_dedicated_allocate_info = nullptr;
	device->allocate_memory(&memory_allocate_info, nullptr, &m_pRaySampleDeviceMemory);
    device->bind_image_memory(m_pRaySampleImage, m_pRaySampleDeviceMemory, 0);

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.image = m_pRaySampleImage;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = ::framework::gpu::format::B8G8R8A8_UNORM;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
    device->create_image_view(&image_view_create_info, nullptr, &m_pRaySampleImageView);
}

void Tracer::allocate(TracerCreateInfo const * create_info, Tracer ** ppTracer)
{
	*ppTracer = new Tracer(create_info);
	(*ppTracer)->allocate();
}

void Tracer::deallocate(Tracer * pTracer)
{
	delete pTracer;
}

void Tracer::trace(::std::uint32_t wait_semaphore_count, class ::framework::gpu::semaphore * const * wait_semaphores, const ::framework::gpu::pipeline_stage_flags * pWaitDstStageMask, ::std::uint32_t signal_semaphore_count, class ::framework::gpu::semaphore * const * signal_semaphores)
{
	class Queue * command_queue;
    device->get_queue(0, 0, &command_queue);
	{
		CameraBufferData cameraBufferData;
		cameraBufferData.viewMatrix = m_camera->getViewMatrix();
		cameraBufferData.projMatrix = m_camera->getProjectionMatrix();
		cameraBufferData.viewProjMatrix = m_camera->getViewProjectionMatrix();
		cameraBufferData.invViewProjMatrix = ::glm::inverse(cameraBufferData.viewProjMatrix);
		cameraBufferData.position = ::glm::vec4(m_cameraTransform->getTranslation(), 0.0f);
		cameraBufferData.direction = ::glm::vec4(m_cameraTransform->getForwardVector(), 0.0f);

		void * pMappedData;
        device->map_memory(m_pCameraUboDeviceMemory, 0, sizeof(CameraBufferData), 0, &pMappedData);
		::std::memcpy(pMappedData, &cameraBufferData, sizeof(CameraBufferData));
        device->unmap_memory(m_pCameraUboDeviceMemory);
	}
	{
		VariousBufferData variousBufferData;

		auto width = width;
		auto height = height;

		float t = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		float dt = 0.5f;
		variousBufferData.vScreenResolution = ::glm::vec4(width, height, 1.0f / width, 1.0f / height);
		variousBufferData.vTime = ::glm::vec4(t / 20.0, t, t * 2.0, t * 3.0);
		variousBufferData.vSinTime = ::glm::vec4(sin(t / 8.0), sin(t / 4.0), sin(t / 2.0), sin(t));
		variousBufferData.vCosTime = ::glm::vec4(cos(t / 8.0), cos(t / 4.0), cos(t / 2.0), cos(t));
		variousBufferData.vDeltaTime = ::glm::vec4(dt, 1.0 / dt, dt, 1.0 / dt);

		void * pMappedData;
        device->map_memory(m_pVariousUboDeviceMemory, 0, sizeof(VariousBufferData), 0, &pMappedData);
		::std::memcpy(pMappedData, &variousBufferData, sizeof(VariousBufferData));
        device->unmap_memory(m_pVariousUboDeviceMemory);
	}

	::std::uint32_t pushConstant[3] = { m_samples, m_bounces, m_bvh->getLeavesCount() };

	PipelineBarrier pipelineBarriers[3];
	pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	buffer_memory_barriers[0].pipelineBarrierCount = 1;
	buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	buffer_memory_barriers[0].src_queue_family_index = 0;
	buffer_memory_barriers[0].dst_queue_family_index = 0;
	buffer_memory_barriers[0].buffer = m_bvh->getPrimitiveBuffer();
	buffer_memory_barriers[0].offset = 0;
	buffer_memory_barriers[0].size = m_bvh->getPrimitiveBuffer()->GetBufferCreateInfo()->size;

	buffer_memory_barriers[1].pipelineBarrierCount = 1;
	buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	buffer_memory_barriers[1].src_queue_family_index = 0;
	buffer_memory_barriers[1].dst_queue_family_index = 0;
	buffer_memory_barriers[1].buffer = m_bvh->getRadixTreeBuffer();
	buffer_memory_barriers[1].offset = 0;
	buffer_memory_barriers[1].size = m_bvh->getRadixTreeBuffer()->GetBufferCreateInfo()->size;

	//::framework::gpu::image_memory_barrier image_memory_barrier;
	//image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
	//image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	//image_memory_barrier.src_queue_family_index = 0;
	//image_memory_barrier.dst_queue_family_index = 0;
	//image_memory_barrier.image = m_pRaySampleImage;
	//image_memory_barrier.subresource_range = { ::framework::gpu::image_aspect_flags::color_bit, 0, 1, 0, 1 };

	::framework::gpu::dependency_info dependency_info;
	dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
	dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	m_pPathTracingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
    m_pPathTracingCommandBuffer->pipeline_barrier(0, 1, &dependency_info);
	//m_pPathTracingCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::compute_shader_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit, 0, 0, nullptr, 2, &buffer_memory_barriers[0], 0, nullptr);
	//m_pPathTracingCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::fragment_shader_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit, 0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
    m_pPathTracingCommandBuffer->bind_pipeline(m_pPathTracingPipeline);
	m_pPathTracingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pPathTracingDescriptorTable, 0, nullptr);
    m_pPathTracingCommandBuffer->push_constants(m_pPathTracingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), pushConstant);
    m_pPathTracingCommandBuffer->dispatch((width + 16 - 1) / 16, (height + 16 - 1) / 16, 1);
	m_pPathTracingCommandBuffer->end_command_buffer();

	::framework::gpu::submit_info submit_info;
	submit_info.command_buffer_count = 1;
	submit_info.command_buffers = &m_pPathTracingCommandBuffer;
	submit_info.wait_semaphore_count = wait_semaphore_count;
	submit_info.wait_semaphores = wait_semaphores;
	submit_info.wait_dst_stage_mask = pWaitDstStageMask;
	submit_info.signal_semaphore_count = signal_semaphore_count;
	submit_info.signal_semaphores = signal_semaphores;
	command_queue->submit(1, &submit_info, nullptr);
}

void Tracer::resize(::std::uint32_t width, ::std::uint32_t height)
{
	width = width;
	height = height;

	device->wait_idle();

    device->destroy_image_view(m_pRaySampleImageView);
    device->destroy_image(m_pRaySampleImage);
    device->free_memory(m_pRaySampleDeviceMemory);

	setupRaySampleImage();

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = m_pRaySampleSampler;
	descriptor_image_info.image_view = m_pRaySampleImageView;
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = m_pPathTracingDescriptorSet;
	write_descriptor_set.dst_binding = 6;
	write_descriptor_set.dst_array_element = 0;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_set.image_info = &descriptor_image_info;
    device->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);
	device->UpdateDescriptorTable(m_pPathTracingDescriptorTable, 0, 1, &m_pPathTracingDescriptorSet);
}

class ::framework::gpu::sampler * Tracer::GetGetRayTracedSampler() const
{
	return m_pRaySampleSampler;
}

class ::framework::gpu::image * Tracer::GetRayTracedImage() const
{
	return m_pRaySampleImage;
}

class ::framework::gpu::image_view * Tracer::GetRayTracedImageView() const
{
	return m_pRaySampleImageView;
}