#include "components/Mesh.h"
#include "resource/resource_manager.hpp"
#include "InstanceManager.h"
#include "gpu/utility.hpp"
#include "RadixSort.h"
#include "EntityManager.h"
#include "components/Transform.h"
#include "components/MeshRendererPathTracingBVH.h"
#include "lbvh.hpp"
#include <algorithm>
#include <future>
#include <iostream>

using namespace framework;

#define MAX_TREELET_SIZE_DIST_SHARED_MEM 20

class BVHNodeQueue // as a doubly-linked list
{
private:
	typedef struct Node {
		struct Node * prev;
		struct Node * next;
		void * v;
	} Node;

	Node * head;
	Node * tail;
public:

	BVHNodeQueue() {
		head = NULL;
		tail = NULL;
	}

	void push(void * v) {
		Node * n = new Node();
		n->v = v;
		if (head == NULL) {
			n->prev = NULL;
			n->next = NULL;
			head = n;
			tail = n;
		}
		else {
			n->prev = NULL;
			n->next = head;
			if (head != NULL) {
				head->prev = n;
			}
			head = n;
		}
	}

	void pop() {
		if (head != NULL) {
			if (head == tail) {
				delete head;
				head = NULL;
				tail = NULL;
			}
			else {
				Node * subtail = tail->prev;
				delete tail;
				subtail->next = NULL;
				tail = subtail;
			}
		}
	}

	void * last()
	{
		if (tail != NULL) {
			return tail->v;
		}
		else {
			return NULL;
		}
	}

	bool empty()
	{
		return head == NULL;
	}
};

bool lbvh::check_sanity(::std::uint32_t n, lbvh::CompactTreeNode * nodes)
{
	if (nodes[n].field0.w != 0xFFFFFFFF) {
		return true;
	}
	else {
		return (
			nodes[nodes[n].field0.y].field0.x == n &&
			nodes[nodes[n].field0.z].field0.x == n
			);
	}
}

bool lbvh::check_bound(lbvh::CompactTreeNode * p, lbvh::CompactTreeNode * l, lbvh::CompactTreeNode * r)
{
	return (
		p->field1.x == ::std::min(l->field1.x, r->field1.x) &&
		p->field2.x == ::std::max(l->field2.x, r->field2.x) &&
		p->field1.y == ::std::min(l->field1.y, r->field1.y) &&
		p->field2.y == ::std::max(l->field2.y, r->field2.y) &&
		p->field1.z == ::std::min(l->field1.z, r->field1.z) &&
		p->field2.z == ::std::max(l->field2.z, r->field2.z)
		);
}

void lbvh::printBVH(::std::uint32_t root, lbvh::CompactTreeNode * nodes)
{
	int level = 1;
	BVHNodeQueue * q = new BVHNodeQueue();
	q->push((void *)&root);

	BVHNodeQueue * qt = new BVHNodeQueue();

	while (!q->empty())
	{
		//printf("\n######### Level %d ##########\n", level++);
		while (!q->empty())
		{
			::std::uint32_t n = *(::std::uint32_t*)(q->last());
			q->pop();
			//printf("(%d %d) %d", nodes[n].field1, nodes[n].max, n);

			if (!check_sanity(n, nodes))
			{
				printf(" !SanityError! ");
			}

			if (nodes[n].field0.w == 0xFFFFFFFF)
			{
				auto parent = &nodes[n];
				auto left = &nodes[nodes[n].field0.y];
				auto right = &nodes[nodes[n].field0.z];
				if (!check_bound(parent, left, right))
				{
					printf(" !BoundError!");
				}
				//printf("\n");
				qt->push((void *)&nodes[n].field0.y);
				qt->push((void *)&nodes[n].field0.z);
			}
			else
			{
				//printf(" ((A:%.0lf C:%.0lf) PrimitiveID: %d)\n", nodes[n].field2.w, nodes[n].field1.w, nodes[n].field0.w);
			}
		}
		//printf("\n");

		BVHNodeQueue * t = q;
		q = qt;
		qt = t;
	}

	printf("\n");

	delete q;
	delete qt;
}

int sumArithmeticSequence(int numberOfElements, int firstElement, int lastElement)
{
	return numberOfElements * (firstElement + lastElement) / 2;
}

class AgglomerativeScheduler
{
public:

	/// <summary> Default constructor. </summary>
	///
	/// <remarks> Leonardo, 04/04/2015. </remarks>
	AgglomerativeScheduler() {}

	/// <summary> Destructor. </summary>
	///
	/// <remarks> Leonardo, 04/04/2015. </remarks>
	~AgglomerativeScheduler() {}

	/// <summary> Generates a schedule for an upper triangular matrix.
	///
	/// <remarks> Leonardo, 04/04/2015. </remarks>
	///
	/// <param name="treeletSize"> Treelet size. </param>
	/// <param name="warpSize">    Warp size. </param>
	/// <param name="schedule">    [out] The generated schedule. The outer vector represents round 
	///                            and the inner one represents subsets. </param>
	void GenerateScheduleUpper(int treeletSize, int warpSize, ::std::vector<int>& schedule) const
	{
		int numberOfElements = sumArithmeticSequence(treeletSize - 1, 1, treeletSize - 1);
		int elementsPerWarp = 2 * warpSize;
		int numberOfIterations = (numberOfElements + elementsPerWarp - 1) / elementsPerWarp;
		int scheduleSize = numberOfIterations * warpSize;

		schedule.clear();
		schedule.resize(scheduleSize, 0);

		int count = 0;
		float multiplier = 0.0f;
		for (int i = 0; i < treeletSize; ++i)
		{
			for (int j = i + 1; j < treeletSize; ++j)
			{
				int index = count + static_cast<int>(multiplier)* warpSize;
				schedule[index] = (schedule[index] << 16) | (i << 8) | j;

				++count;
				if (count == warpSize)
				{
					multiplier += 0.5f;
					count = 0;
				}
			}
		}
	}

	/// <summary> Generates a schedule for an upper triangular matrix.
	///
	/// <remarks> Leonardo, 04/04/2015. </remarks>
	///
	/// <param name="treeletSize">  Treelet size. </param>
	/// <param name="warpSize">     Warp size. </param>
	/// <param name="schedule">     [out] The generated schedule. The outer vector represents 
	///                             round and the inner one represents subsets. </param>
	/// <param name="scheduleSize"> Number of elements contained in the schedule. </param>
	void GenerateScheduleLower(int treeletSize, int warpSize, ::std::vector<::std::uint32_t>& schedule, ::std::uint32_t& scheduleSize) const
	{
		int numberOfElements = sumArithmeticSequence(treeletSize - 1, 1, treeletSize - 1);
		int elementsPerWarp = 2 * warpSize;
		int numberOfIterations = (numberOfElements + elementsPerWarp - 1) / elementsPerWarp;
		scheduleSize = numberOfIterations * warpSize;

		schedule.clear();
		schedule.resize(scheduleSize, 0);

		int count = 0;
		float multiplier = 0.0f;
		for (int i = 0; i < treeletSize; ++i)
		{
			for (::std::uint32_t j = 0; j < i; ++j)
			{
				int index = count + static_cast<int>(multiplier) * warpSize;
				schedule[index] = (schedule[index] << 16) | (i << 8) | j;

				++count;
				if (count == warpSize)
				{
					multiplier += 0.5f;
					count = 0;
				}
			}
		}

		// If multiplier was not integer, shift the last 'warpSize' elements over to the left
		if (static_cast<int>(multiplier + 0.5f) > static_cast<int>(multiplier))
		{
			for (int i = count; i < 32; ++i)
			{
				int index = i + static_cast<int>(multiplier) * warpSize;
				schedule[index] = (schedule[index] << 16);
			}
		}
	}
};

struct MaterialBuffer
{
	::glm::vec4			vAmbientColor;
	::glm::vec4			vDiffuseColor;
	::glm::vec4			vSpecularColor;
	::glm::vec4			vGlossinessColor;
	::glm::vec4			vEmissivityColor;
	::std::uint32_t			albedoMapIndex;
	::std::uint32_t			normalMapIndex;
	::std::uint32_t			specularMapIndex;
};

lbvh::lbvh(LBVHCreateInfo const * create_info) :
	device(create_info->device),
	m_maxPrimitives(create_info->maxPrimitives),
	m_treeletSize(create_info->treeletSize),
	m_iterations(create_info->iterations),
	m_pRadixSortData(nullptr),
	m_maxUBOs(1024),
	m_numTriangles()
{
}

lbvh::~lbvh()
{
}

void lbvh::allocate()
{
	::framework::gpu::physical_device_memory_properties memoryProperties;
	device->get_physical_device()->GetMemoryProperties(&memoryProperties);
	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
        device->create_command_pool(&command_pool_create_info, &this->primary_command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = this->primary_command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPrimaryCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pPrimitiveCounterServicingCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pGlobalBoundComputingCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pGenerateMortonCodesCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pConstructRadixTreeCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCalculateNodeBoundingBoxesCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pAgglomerativeTreeletOptimizerCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCreateNodesCommandBuffer);
        device->allocate_command_buffers(&command_buffer_allocate_info, &m_pCreateWoopifyTrianglesCommandBuffer);
	}

	thread_count = ::std::thread::hardware_concurrency();
	thread_pool.set_thread_count(thread_count);
	thread_data.resize(thread_count);

	for (::std::uint32_t i = 0; i < thread_count; i++)
	{
		thread_data * thread = &thread_data[i];

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
        device->create_command_pool(&command_pool_create_info, &thread->command_pool);

		// One secondary command class Buffer per object that is updated by this thread
		thread->commandBufferData.resize(1);
		thread->commandBuffers.resize(1);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = thread->command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::secondary;
		command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(thread->commandBufferData.size());
        device->allocate_command_buffers(&command_buffer_allocate_info, thread->commandBuffers.data());
	}

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	device->create_fence(&fence_create_info, nullptr, &m_pPrimitiveCounterServicingCompleteFence);

	const int warpSize = 32;

	// Allocate schedule
	AgglomerativeScheduler scheduler;
	::std::vector<::std::uint32_t> schedule;
	scheduler.GenerateScheduleLower(m_treeletSize, warpSize, schedule, m_scheduleSize);

	// Allocate distance matrix
	::std::uint32_t numberOfElements = sumArithmeticSequence(m_treeletSize - 1, 1, m_treeletSize - 1);
	//if (m_treeletSize > MAX_TREELET_SIZE_DIST_SHARED_MEM)
	{
		::std::uint32_t numberOfWarps = (m_maxPrimitives + warpSize - 1) / warpSize;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(float) * numberOfWarps * numberOfElements;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pDistanceMatrixBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pDistanceMatrixBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pDistanceMatrixDeviceMemory);
		device->bind_buffer_memory(m_pDistanceMatrixBuffer, m_pDistanceMatrixDeviceMemory, 0);
	}

	{
		const ::std::size_t uboAlignment = 256;
		const ::std::size_t dynamicAlignment = (sizeof(TransformBufferData) / uboAlignment) * uboAlignment + ((sizeof(TransformBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = dynamicAlignment * m_maxUBOs;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriangleListFillUboBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pTriangleListFillUboBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits,
                                                                                                ::framework::gpu::memory_property_flags::host_visible_write_bit |
                                                                                                ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriangleListFillUboDeviceMemory);
		device->bind_buffer_memory(m_pTriangleListFillUboBuffer, m_pTriangleListFillUboDeviceMemory, 0);

        device->map_memory(m_pTriangleListFillUboDeviceMemory, 0,
                           dynamicAlignment * m_maxUBOs, 0, &m_pTriangleListFillUboMappedData);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(lbvh::CompactTreeNode) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pRadixTreeBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pRadixTreeBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pRadixTreeDeviceMemory);
		device->bind_buffer_memory(m_pRadixTreeBuffer, m_pRadixTreeDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(lbvh::CompactTreeNode) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pRadixTreeReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pRadixTreeReadbackBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pRadixTreeReadbackDeviceMemory);
		device->bind_buffer_memory(m_pRadixTreeReadbackBuffer, m_pRadixTreeReadbackDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pCounterBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pCounterDeviceMemory);
		device->bind_buffer_memory(m_pCounterBuffer, m_pCounterDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(lbvh::Bound) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pBoundsBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pBoundsBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pBoundsDeviceMemory);
		device->bind_buffer_memory(m_pBoundsBuffer, m_pBoundsDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(CompactTriangle) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pPrimitivesBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesDeviceMemory);
		device->bind_buffer_memory(m_pPrimitivesBuffer, m_pPrimitivesDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pPrimitivesCounterBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesCounterDeviceMemory);
		device->bind_buffer_memory(m_pPrimitivesCounterBuffer, m_pPrimitivesCounterDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitiveIndicesBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pPrimitiveIndicesBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitiveIndicesDeviceMemory);
		device->bind_buffer_memory(m_pPrimitiveIndicesBuffer, m_pPrimitiveIndicesDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pMortonCodesBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pMortonCodesBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pMortonCodesDeviceMemory);
		device->bind_buffer_memory(m_pMortonCodesBuffer, m_pMortonCodesDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_scheduleSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pScheduleBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pScheduleBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pScheduleDeviceMemory);
		device->bind_buffer_memory(m_pScheduleBuffer, m_pScheduleDeviceMemory, 0);
	}
	{
		class ::framework::gpu::buffer * m_pStagingScheduleBuffer;
		class ::framework::gpu::device_memory * m_pStagingScheduleDeviceMemory;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_scheduleSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pStagingScheduleBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pStagingScheduleBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pStagingScheduleDeviceMemory);
		device->bind_buffer_memory(m_pStagingScheduleBuffer, m_pStagingScheduleDeviceMemory, 0);

		void * pMappedData;
        device->map_memory(m_pStagingScheduleDeviceMemory, 0, sizeof(::std::uint32_t) * m_scheduleSize, 0, &pMappedData);
		::std::memcpy(pMappedData, schedule.data(), sizeof(::std::uint32_t) * m_scheduleSize);
        device->unmap_memory(m_pStagingScheduleDeviceMemory);

		class Queue * transferQueue;
        device->get_queue(0, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;
        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::simultaneous_use_bit;
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * m_scheduleSize;
        copyCmd->copy_buffer(m_pStagingScheduleBuffer, m_pScheduleBuffer, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		transferQueue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);
        device->destroy_fence(fence);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pSubtreeTrianglesCountBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pSubtreeTrianglesCountBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pSubtreeTrianglesCountDeviceMemory);
		device->bind_buffer_memory(m_pSubtreeTrianglesCountBuffer, m_pSubtreeTrianglesCountDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * 4 * ((m_maxPrimitives - 1) * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNodesBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pNodesBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNodesDeviceMemory);
		device->bind_buffer_memory(m_pNodesBuffer, m_pNodesDeviceMemory, 0);

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.buffer = m_pNodesBuffer;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = sizeof(::std::uint32_t) * 4 * ((m_maxPrimitives - 1) * 4);
        device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pNodesBufferView);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * 4 * (m_maxPrimitives * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriWoopBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pTriWoopBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriWoopDeviceMemory);
		device->bind_buffer_memory(m_pTriWoopBuffer, m_pTriWoopDeviceMemory, 0);

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.buffer = m_pTriWoopBuffer;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = (sizeof(::std::uint32_t) * 4) * (m_maxPrimitives * 4);
        device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pTriWoopBufferView);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriIndexBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pTriIndexBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriIndexDeviceMemory);
		device->bind_buffer_memory(m_pTriIndexBuffer, m_pTriIndexDeviceMemory, 0);

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.buffer = m_pTriIndexBuffer;
		buffer_view_create_info.format = ::framework::gpu::format::R32_SINT;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = sizeof(::std::uint32_t) * (m_maxPrimitives * 4);
        device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pTriIndexBufferView);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pReadbackBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pReadbackDeviceMemory);
		device->bind_buffer_memory(m_pReadbackBuffer, m_pReadbackDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(lbvh::Bound);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pBoundsReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pBoundsReadbackBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pBoundsReadbackDeviceMemory);
		device->bind_buffer_memory(m_pBoundsReadbackBuffer, m_pBoundsReadbackDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(CompactTriangle) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pPrimitivesReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pPrimitivesReadbackBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pPrimitivesReadbackDeviceMemory);
		device->bind_buffer_memory(m_pPrimitivesReadbackBuffer, m_pPrimitivesReadbackDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * m_maxPrimitives;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pUnsortedDataReadback);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pUnsortedDataReadback, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pUnsortedDataReadbackMemory);
		device->bind_buffer_memory(m_pUnsortedDataReadback, m_pUnsortedDataReadbackMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * 4 * ((m_maxPrimitives - 1) * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pNodesUploadBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pNodesUploadBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pNodesUploadDeviceMemory);
		device->bind_buffer_memory(m_pNodesUploadBuffer, m_pNodesUploadDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * 4 * (m_maxPrimitives * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriWoopUploadBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pTriWoopUploadBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriWoopUploadDeviceMemory);
		device->bind_buffer_memory(m_pTriWoopUploadBuffer, m_pTriWoopUploadDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives * 4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pTriIndexUploadBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pTriIndexUploadBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pTriIndexUploadDeviceMemory);
		device->bind_buffer_memory(m_pTriIndexUploadBuffer, m_pTriIndexUploadDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pDebugCounterBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pDebugCounterBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pDebugCounterDeviceMemory);
		device->bind_buffer_memory(m_pDebugCounterBuffer, m_pDebugCounterDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pDebugBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pDebugBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pDebugDeviceMemory);
		device->bind_buffer_memory(m_pDebugBuffer, m_pDebugDeviceMemory, 0);
	}
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.size = sizeof(::std::uint32_t) * (m_maxPrimitives * 2 - 1);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		device->create_buffer(&buffer_create_info, nullptr, &m_pDebugReadbackBuffer);

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(m_pDebugReadbackBuffer, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		memory_allocate_info.memory_dedicated_allocate_info = nullptr;
		device->allocate_memory(&memory_allocate_info, nullptr, &m_pDebugReadbackDeviceMemory);
		device->bind_buffer_memory(m_pDebugReadbackBuffer, m_pDebugReadbackDeviceMemory, 0);
	}

	device->create_semaphore(&m_pTriangleListFillCompleteSemaphore);
	device->create_semaphore(&m_pGlobalBoundComputingCompleteSemaphore);
	device->create_semaphore(&m_pGenerateMortonCodesCompleteSemaphore);
	device->create_semaphore(&m_pSortMortonCodesCompleteSemaphore);
	device->create_semaphore(&m_pConstructRadixTreeCompleteSemaphore);
	device->create_semaphore(&m_pCalculateNodeBoundingBoxesCompleteSemaphore);
	device->create_semaphore(&m_pAgglomerativeTreeletOptimizerCompleteSemaphore);
	device->create_semaphore(&m_pCreateNodesCompleteSemaphore);
	device->create_semaphore(&m_pCreateWoopifyTrianglesCompleteSemaphore);

	// Load Shaders
	m_pTriangleListFillVertexShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "TriangleListFillPathTracing.vert.sdr");
	m_pTriangleListFillGeometryShaderModule = ::framework::resource::g_resource_manager.load_shader_module(device, "TriangleListFillPathTracing.geom.sdr");
	m_pGlobalBoundComputingShader = ::framework::resource::g_resource_manager.load_shader_module(device, "GlobalBoundComputing.comp.sdr");
	m_pReductionGlobalBoundComputingShader = ::framework::resource::g_resource_manager.load_shader_module(device, "ReductionGlobalBoundComputing.comp.sdr");
	m_pGenerateMortonCodesShader = ::framework::resource::g_resource_manager.load_shader_module(device, "GenerateMortonCodes.comp.sdr");
	m_pConstructRadixTreeShader = ::framework::resource::g_resource_manager.load_shader_module(device, "ConstructRadixTree.comp.sdr");
	m_pCalculateNodeBoundingBoxesShader = ::framework::resource::g_resource_manager.load_shader_module(device, "CalculateNodeBoundingBoxes.comp.sdr");
	m_pAgglomerativeTreeletOptimizerShader = ::framework::resource::g_resource_manager.load_shader_module(device, "AgglomerativeTreeletOptimizer.comp.sdr");
	m_pAgglomerativeSmallTreeletOptimizerShader = ::framework::resource::g_resource_manager.load_shader_module(device, "AgglomerativeSmallTreeletOptimizer.comp.sdr");
	m_pCreateNodesShader = ::framework::resource::g_resource_manager.load_shader_module(device, "CreateNodes.comp.sdr");
	m_pCreateWoopifyTrianglesShader = ::framework::resource::g_resource_manager.load_shader_module(device, "CreateWoopifyTriangles.comp.sdr");

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[7];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
	descriptor_pool_sizes[0].descriptor_count = 2;
	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_pool_sizes[1].descriptor_count = 1;
	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::sampled_buffer;
	descriptor_pool_sizes[2].descriptor_count = 15;
	descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[3].descriptor_count = 20;
	descriptor_pool_sizes[4].type = ::framework::gpu::descriptor_type::storage_image;
	descriptor_pool_sizes[4].descriptor_count = 1;
	descriptor_pool_sizes[5].type = ::framework::gpu::descriptor_type::sampled_texel_buffer;
	descriptor_pool_sizes[5].descriptor_count = 10;
	descriptor_pool_sizes[6].type = ::framework::gpu::descriptor_type::storage_texel_buffer;
	descriptor_pool_sizes[6].descriptor_count = 10;
	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.max_sets = 11;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
    device->create_descriptor_pool(&descriptor_pool_create_info, &descriptor_pool);

	// Triangle list fill
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
		pipeline_shader_stage_create_infos[0].module = m_pTriangleListFillVertexShaderModule;
		pipeline_shader_stage_create_infos[0].name = "main";
		pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		pipeline_shader_stage_create_infos[1].module = m_pTriangleListFillGeometryShaderModule;
		pipeline_shader_stage_create_infos[1].name = "main";
		pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::geometry_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::vertex_bit;

		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::geometry_bit;

		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::geometry_bit;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pTriangleListFillDescriptorSetLayout);

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pTriangleListFillDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pTriangleListFillPipelineLayout);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pTriangleListFillDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pTriangleListFillDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pTriangleListFillUboBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = (sizeof(TransformBufferData) / 256) * 256 + ((sizeof(TransformBufferData) % 256) > 0 ? 256 : 0);
		descriptor_buffer_infos[0].stride = (sizeof(TransformBufferData) / 256) * 256 + ((sizeof(TransformBufferData) % 256) > 0 ? 256 : 0);
		descriptor_buffer_infos[1].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(CompactTriangle);

		descriptor_buffer_infos[2].buffer = m_pPrimitivesCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = m_pTriangleListFillDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];

        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pTriangleListFillPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pTriangleListFillDescriptorTable);

		device->UpdateDescriptorTable(m_pTriangleListFillDescriptorTable, 0, 1, &m_pTriangleListFillDescriptorSet);

		::framework::gpu::subpass_description subpass_description;
		subpass_description.color_attachment_count = 0;
		subpass_description.color_attachments = nullptr;
		subpass_description.depth_stencil_attachment = nullptr;
		subpass_description.input_attachment_count = 0;
		subpass_description.input_attachments = nullptr;
		subpass_description.resolve_attachments = nullptr;

		::framework::gpu::render_pass_create_info render_pass_create_info;
		render_pass_create_info.attachment_count = 0;
		render_pass_create_info.attachments = nullptr;
		render_pass_create_info.subpass_count = 1;
		render_pass_create_info.subpasses = &subpass_description;
		render_pass_create_info.dependency_count = 0;
		render_pass_create_info.dependencies = nullptr;
        device->create_render_pass(&render_pass_create_info, &this->render_pass);

		::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
		frame_buffer_create_info.width = 0;
		frame_buffer_create_info.height = 0;
		frame_buffer_create_info.attachment_count = 0;
		frame_buffer_create_info.attachments = nullptr;
		frame_buffer_create_info.render_pass = this->render_pass;
        device->create_frame_buffer(&frame_buffer_create_info, &m_pFramebuffer);

		::std::uint32_t width(0), height(0);
		::framework::gpu::viewport viewport{ 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
		::framework::gpu::rect_2d scissor{ { 0, 0 },{ width, height } };
		::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
		pipeline_viewport_state_create_info.viewport_count = 1;
		pipeline_viewport_state_create_info.scissors = &scissor;
		pipeline_viewport_state_create_info.scissor_count = 1;
		pipeline_viewport_state_create_info.viewports = &viewport;

		::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info{};
		::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info{};
		::framework::gpu::pipeline_depth_stencil_state_create_info pipelineDepthStencilStateCreateInfo{};

		::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[5];
		vertex_input_binding_descriptions[0].binding = 0;
		vertex_input_binding_descriptions[0].stride = sizeof(float) * 3;
		vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		vertex_input_binding_descriptions[1].binding = 1;
		vertex_input_binding_descriptions[1].stride = sizeof(float) * 3;
		vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		vertex_input_binding_descriptions[2].binding = 2;
		vertex_input_binding_descriptions[2].stride = sizeof(float) * 3;
		vertex_input_binding_descriptions[2].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		vertex_input_binding_descriptions[3].binding = 3;
		vertex_input_binding_descriptions[3].stride = sizeof(float) * 3;
		vertex_input_binding_descriptions[3].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		vertex_input_binding_descriptions[4].binding = 4;
		vertex_input_binding_descriptions[4].stride = sizeof(float) * 2;
		vertex_input_binding_descriptions[4].input_rate = ::framework::gpu::vertex_input_rate::vertex;
		::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[5];
		// Location 0 : Position
		vertex_input_attribute_descriptions[0].location = 0;
		vertex_input_attribute_descriptions[0].binding = 0;
		vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32_sfloat;
		vertex_input_attribute_descriptions[0].offset = 0;
		// Location 1 : Vertex normal
		vertex_input_attribute_descriptions[1].location = 1;
		vertex_input_attribute_descriptions[1].binding = 1;
		vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32_sfloat;
		vertex_input_attribute_descriptions[1].offset = 0;
		// Location 2 : Vertex binormal
		vertex_input_attribute_descriptions[2].location = 2;
		vertex_input_attribute_descriptions[2].binding = 2;
		vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32g32b32_sfloat;
		vertex_input_attribute_descriptions[2].offset = 0;
		// Location 3 : Vertex tangent
		vertex_input_attribute_descriptions[3].location = 3;
		vertex_input_attribute_descriptions[3].binding = 3;
		vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32g32b32_sfloat;
		vertex_input_attribute_descriptions[3].offset = 0;
		// Location 4 : Texture coordinates
		vertex_input_attribute_descriptions[4].location = 4;
		vertex_input_attribute_descriptions[4].binding = 4;
		vertex_input_attribute_descriptions[4].format = ::framework::gpu::format::r32g32_sfloat;
		vertex_input_attribute_descriptions[4].offset = 0;
		::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
		vertex_input_state.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
		vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
		vertex_input_state.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
		vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

		::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
		pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
		pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

		::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info{};
		graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
		graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
		graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
		graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
		graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
		graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
		graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
		graphics_pipeline_create_info.depth_stencil_state = &pipelineDepthStencilStateCreateInfo;
		graphics_pipeline_create_info.layout = m_pTriangleListFillPipelineLayout;
		graphics_pipeline_create_info.render_pass = this->render_pass;
        device->create_graphics_pipelines(&graphics_pipeline_create_info, 1, &m_pTriangleListFillPipeline);
	}
	// GlobalBoundComputing
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pGlobalBoundComputingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pGlobalBoundComputingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pGlobalBoundComputingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pGlobalBoundComputingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pGlobalBoundComputingPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pGlobalBoundComputingPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pGlobalBoundComputingDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pGlobalBoundComputingDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(CompactTriangle);
		descriptor_buffer_infos[1].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pBoundsBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(lbvh::Bound);
		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = m_pGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pGlobalBoundComputingPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pGlobalBoundComputingDescriptorTable);

		device->UpdateDescriptorTable(m_pGlobalBoundComputingDescriptorTable, 0, 1, &m_pGlobalBoundComputingDescriptorSet);
	}
	// ReductionGlobalBoundComputing
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pReductionGlobalBoundComputingShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pReductionGlobalBoundComputingDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pReductionGlobalBoundComputingDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pReductionGlobalBoundComputingPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pReductionGlobalBoundComputingPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pReductionGlobalBoundComputingPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pReductionGlobalBoundComputingDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pReductionGlobalBoundComputingDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pBoundsBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::Bound);
		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
		write_descriptor_sets[0].dst_set = m_pReductionGlobalBoundComputingDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pReductionGlobalBoundComputingPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pReductionGlobalBoundComputingDescriptorTable);

		device->UpdateDescriptorTable(m_pReductionGlobalBoundComputingDescriptorTable, 0, 1, &m_pReductionGlobalBoundComputingDescriptorSet);
	}
	// GenerateMortonCodes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pGenerateMortonCodesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pGenerateMortonCodesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pGenerateMortonCodesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pGenerateMortonCodesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pGenerateMortonCodesPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pGenerateMortonCodesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pGenerateMortonCodesDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pGenerateMortonCodesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
		descriptor_buffer_infos[0].buffer = m_pBoundsBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pBoundsBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::Bound);
		descriptor_buffer_infos[1].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(CompactTriangle);
		descriptor_buffer_infos[2].buffer = m_pMortonCodesBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[3].buffer = m_pPrimitiveIndicesBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);
		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
		write_descriptor_sets[0].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[3].dst_set = m_pGenerateMortonCodesDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pGenerateMortonCodesPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pGenerateMortonCodesDescriptorTable);

		device->UpdateDescriptorTable(m_pGenerateMortonCodesDescriptorTable, 0, 1, &m_pGenerateMortonCodesDescriptorSet);
	}
	// ConstructRadixTree
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pConstructRadixTreeShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pConstructRadixTreeDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pConstructRadixTreeDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pConstructRadixTreePipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pConstructRadixTreePipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pConstructRadixTreePipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pConstructRadixTreeDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pConstructRadixTreeDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pMortonCodesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[1].buffer = m_pPrimitiveIndicesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[2].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(lbvh::CompactTreeNode);
		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pConstructRadixTreeDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pConstructRadixTreePipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pConstructRadixTreeDescriptorTable);

		device->UpdateDescriptorTable(m_pConstructRadixTreeDescriptorTable, 0, 1, &m_pConstructRadixTreeDescriptorSet);
	}
	// CalculateNodeBoundingBoxes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pCalculateNodeBoundingBoxesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pCalculateNodeBoundingBoxesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pCalculateNodeBoundingBoxesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCalculateNodeBoundingBoxesPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pCalculateNodeBoundingBoxesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pCalculateNodeBoundingBoxesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
		descriptor_buffer_infos[0].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(CompactTriangle);
		descriptor_buffer_infos[1].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(lbvh::CompactTreeNode);
		descriptor_buffer_infos[2].buffer = m_pCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pCounterBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);
		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pCalculateNodeBoundingBoxesDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pCalculateNodeBoundingBoxesPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pCalculateNodeBoundingBoxesDescriptorTable);

		device->UpdateDescriptorTable(m_pCalculateNodeBoundingBoxesDescriptorTable, 0, 1, &m_pCalculateNodeBoundingBoxesDescriptorSet);
	}
	// AgglomerativeTreeletOptimizer
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pAgglomerativeTreeletOptimizerShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[5];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pAgglomerativeTreeletOptimizerDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pAgglomerativeTreeletOptimizerDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pAgglomerativeTreeletOptimizerPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pAgglomerativeTreeletOptimizerPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pAgglomerativeTreeletOptimizerPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pAgglomerativeTreeletOptimizerDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pAgglomerativeTreeletOptimizerDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[5];
		descriptor_buffer_infos[0].buffer = m_pScheduleBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pScheduleBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[1].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(lbvh::CompactTreeNode);
		descriptor_buffer_infos[2].buffer = m_pCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pCounterBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[3].buffer = m_pSubtreeTrianglesCountBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = m_pSubtreeTrianglesCountBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[4].buffer = m_pDistanceMatrixBuffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = m_pDistanceMatrixBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[4].stride = sizeof(float);
		::framework::gpu::write_descriptor_set write_descriptor_sets[5];
		write_descriptor_sets[0].dst_set = m_pAgglomerativeTreeletOptimizerDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pAgglomerativeTreeletOptimizerDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pAgglomerativeTreeletOptimizerDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[3].dst_set = m_pAgglomerativeTreeletOptimizerDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[4].dst_set = m_pAgglomerativeTreeletOptimizerDescriptorSet;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pAgglomerativeTreeletOptimizerPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pAgglomerativeTreeletOptimizerDescriptorTable);

		device->UpdateDescriptorTable(m_pAgglomerativeTreeletOptimizerDescriptorTable, 0, 1, &m_pAgglomerativeTreeletOptimizerDescriptorSet);
	}
	// AgglomerativeSmallTreeletOptimizer
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pAgglomerativeSmallTreeletOptimizerShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pAgglomerativeSmallTreeletOptimizerDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 3;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pAgglomerativeSmallTreeletOptimizerDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pAgglomerativeSmallTreeletOptimizerPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pAgglomerativeSmallTreeletOptimizerPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pAgglomerativeSmallTreeletOptimizerPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pAgglomerativeSmallTreeletOptimizerDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pAgglomerativeSmallTreeletOptimizerDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[6];
		descriptor_buffer_infos[0].buffer = m_pScheduleBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pScheduleBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[1].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(lbvh::CompactTreeNode);
		descriptor_buffer_infos[2].buffer = m_pCounterBuffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = m_pCounterBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[3].buffer = m_pSubtreeTrianglesCountBuffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = m_pSubtreeTrianglesCountBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[4].buffer = m_pDebugBuffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = m_pDebugBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[5].buffer = m_pDebugCounterBuffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = m_pDebugCounterBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[5].stride = sizeof(::std::uint32_t);
		::framework::gpu::write_descriptor_set write_descriptor_sets[6];
		write_descriptor_sets[0].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[3].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[4].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[5].dst_set = m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		write_descriptor_sets[5].dst_binding = 5;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pAgglomerativeSmallTreeletOptimizerPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pAgglomerativeSmallTreeletOptimizerDescriptorTable);

		device->UpdateDescriptorTable(m_pAgglomerativeSmallTreeletOptimizerDescriptorTable, 0, 1, &m_pAgglomerativeSmallTreeletOptimizerDescriptorSet);
	}
	// CreateNodes
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pCreateNodesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pCreateNodesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCreateNodesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pCreateNodesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCreateNodesPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pCreateNodesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pCreateNodesDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pCreateNodesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactTreeNode);
		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = m_pCreateNodesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pCreateNodesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		write_descriptor_sets[1].texel_buffer_view = &m_pNodesBufferView;
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pCreateNodesPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pCreateNodesDescriptorTable);

		device->UpdateDescriptorTable(m_pCreateNodesDescriptorTable, 0, 1, &m_pCreateNodesDescriptorSet);
	}
	// CreateWoopifyTriangles
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.module = m_pCreateWoopifyTrianglesShader;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        device->create_descriptor_set_layout(&descriptor_set_layout_create_info, &m_pCreateWoopifyTrianglesDescriptorSetLayout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &m_pCreateWoopifyTrianglesDescriptorSetLayout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
        device->create_pipeline_layout(&pipeline_layout_create_info, &m_pCreateWoopifyTrianglesPipelineLayout);

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = m_pCreateWoopifyTrianglesPipelineLayout;
        device->create_compute_pipelines(&compute_pipeline_create_info, 1, &m_pCreateWoopifyTrianglesPipeline);

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.set_layouts = &m_pCreateWoopifyTrianglesDescriptorSetLayout;
        device->allocate_descriptor_sets(&descriptor_set_allocate_info, &m_pCreateWoopifyTrianglesDescriptorSet);

		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = m_pRadixTreeBuffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[0].stride = sizeof(lbvh::CompactTreeNode);
		descriptor_buffer_infos[1].buffer = m_pPrimitivesBuffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;
		descriptor_buffer_infos[1].stride = sizeof(CompactTriangle);
		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
		write_descriptor_sets[0].dst_set = m_pCreateWoopifyTrianglesDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = m_pCreateWoopifyTrianglesDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = m_pCreateWoopifyTrianglesDescriptorSet;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		write_descriptor_sets[2].texel_buffer_view = &m_pTriWoopBufferView;
		write_descriptor_sets[3].dst_set = m_pCreateWoopifyTrianglesDescriptorSet;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_texel_buffer;
		write_descriptor_sets[3].texel_buffer_view = &m_pTriIndexBufferView;
        device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, NULL);

		DescriptorTableCreateInfo descriptorTableCreateInfo;
		descriptorTableCreateInfo.pipeline_layout = m_pCreateWoopifyTrianglesPipelineLayout;
		device->CreateDescriptorTable(&descriptorTableCreateInfo, &m_pCreateWoopifyTrianglesDescriptorTable);

		device->UpdateDescriptorTable(m_pCreateWoopifyTrianglesDescriptorTable, 0, 1, &m_pCreateWoopifyTrianglesDescriptorSet);
	}

	lsd_radix_sort::allocate(device, m_maxPrimitives, 32, &m_pRadixSortData);
	lsd_radix_sort::configure(m_pRadixSortData, m_pMortonCodesBuffer, m_pPrimitiveIndicesBuffer);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = 0;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = 0;
	buffer_copy.dst_offset = 0;
	buffer_copy.size = sizeof(::std::uint32_t);

	PipelineBarrier pipelineBarriers[3];
	pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
	pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
	pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;

	pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
	pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

	pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
	pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
	pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

	::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
	buffer_memory_barriers[0].pipelineBarrierCount = 1;
	buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
	buffer_memory_barriers[0].src_queue_family_index = 0;
	buffer_memory_barriers[0].dst_queue_family_index = 0;
	buffer_memory_barriers[0].buffer = m_pPrimitivesCounterBuffer;
	buffer_memory_barriers[0].offset = 0;
	buffer_memory_barriers[0].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	buffer_memory_barriers[1].pipelineBarrierCount = 1;
	buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
	buffer_memory_barriers[1].src_queue_family_index = 0;
	buffer_memory_barriers[1].dst_queue_family_index = 0;
	buffer_memory_barriers[1].buffer = m_pPrimitivesCounterBuffer;
	buffer_memory_barriers[1].offset = 0;
	buffer_memory_barriers[1].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	buffer_memory_barriers[2].pipelineBarrierCount = 1;
	buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
	buffer_memory_barriers[2].src_queue_family_index = 0;
	buffer_memory_barriers[2].dst_queue_family_index = 0;
	buffer_memory_barriers[2].buffer = m_pPrimitivesCounterBuffer;
	buffer_memory_barriers[2].offset = 0;
	buffer_memory_barriers[2].size = m_pPrimitivesCounterBuffer->GetBufferCreateInfo()->size;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	dependency_info.memory_barrier_count = 0;
	dependency_info.memory_barriers = nullptr;
	dependency_info.buffer_memory_barrier_count = 1;
	dependency_info.buffer_memory_barriers = &buffer_memory_barriers[0];
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	m_pPrimitiveCounterServicingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
    m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &dependency_info);
    m_pPrimitiveCounterServicingCommandBuffer->copy_buffer(m_pPrimitivesCounterBuffer, m_pReadbackBuffer, 1, &buffer_copy);

	dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];
    m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &dependency_info);
    m_pPrimitiveCounterServicingCommandBuffer->fill_buffer(m_pPrimitivesCounterBuffer, 0, sizeof(::std::uint32_t), 0);

	dependency_info.buffer_memory_barriers = &buffer_memory_barriers[2];
    m_pPrimitiveCounterServicingCommandBuffer->dependency_info(0, 1, &dependency_info);
	m_pPrimitiveCounterServicingCommandBuffer->end_command_buffer();
}

void lbvh::deallocate()
{
}

void lbvh::allocate(LBVHCreateInfo const * create_info, lbvh ** ppLBVH)
{
	*ppLBVH = new lbvh(create_info);
	(*ppLBVH)->allocate();
}

void lbvh::deallocate(lbvh * pLBVH)
{
	pLBVH->deallocate();
	delete pLBVH;
}

void lbvh::update()
{
	performTriangleListFillPass();
	buildBVH();
}

::std::uint32_t lbvh::getLeavesCount() const
{
	return m_numTriangles;
}

class ::framework::gpu::buffer * lbvh::getRadixTreeBuffer()
{
	return m_pRadixTreeBuffer;
}

class ::framework::gpu::buffer_view * lbvh::getRadixTreeBufferView()
{
	return nullptr;
}

class ::framework::gpu::buffer * lbvh::getNodeBuffer()
{
	return m_pNodesBuffer;
}

class ::framework::gpu::buffer_view * lbvh::getNodeBufferView()
{
	return m_pNodesBufferView;
}

class ::framework::gpu::buffer * lbvh::getTriWoopBuffer()
{
	return m_pTriWoopBuffer;
}

class ::framework::gpu::buffer_view * lbvh::getTriWoopBufferView()
{
	return m_pTriWoopBufferView;
}

class ::framework::gpu::buffer * lbvh::getPrimitiveBuffer()
{
	return m_pPrimitivesBuffer;
}

class ::framework::gpu::buffer_view * lbvh::getPrimitiveBufferView()
{
	return nullptr;
}

class ::framework::gpu::buffer * lbvh::getTriIndexBuffer()
{
	return m_pTriIndexBuffer;
}

class ::framework::gpu::buffer_view * lbvh::getTriIndexBufferView()
{
	return m_pTriIndexBufferView;
}

class ::framework::gpu::semaphore * lbvh::getBuildCompletionSemaphore()
{
	return m_pCreateWoopifyTrianglesCompleteSemaphore;
}

void lbvh::performTriangleListFillPass()
{
	::std::uint32_t threadIndex = 0;
	::std::uint32_t entityIndex = 0;
	::std::mutex commandBuffersMutex;
	const ::std::size_t uboAlignment = 256;
	const ::std::size_t dynamicAlignment = (sizeof(TransformBufferData) / uboAlignment) * uboAlignment + ((sizeof(TransformBufferData) % uboAlignment) > 0 ? uboAlignment : 0);

	::framework::gpu::command_buffer_inheritance_info inheritanceInfo;
	inheritanceInfo.frame_buffer = m_pFramebuffer;
	inheritanceInfo.subpass = 0;
	inheritanceInfo.render_pass = this->render_pass;

	auto entities = entity_manager::entities_with_components<CMeshRendererPathTracingBVH, CTransform>();
	component_handle<CMeshRendererPathTracingBVH> mesh_renderer;
	component_handle<CTransform> transform;
	for (auto entity : entities)
	{
		entity.unpack<CMeshRendererPathTracingBVH, CTransform>(mesh_renderer, transform);
		thread_pool.threads[threadIndex]->addJob([=, &inheritanceInfo, &commandBuffersMutex, &thread_data = thread_data]
		{
			TransformBufferData transformBufferData;
			transformBufferData.mObject2World = transform->getWorldMatrix();
			transformBufferData.mWorld2Object = ::glm::inverse(transformBufferData.mObject2World);
			//transformBufferData.mModelView = transformBufferData.mObject2World * m_camera->getViewMatrix();
			//transformBufferData.mModelViewProjection = transformBufferData.mObject2World * m_camera->getViewProjectionMatrix();
			//transformBufferData.mTransposeModelView = ::glm::transpose(transformBufferData.mModelView);
			//transformBufferData.mInverseTransposeModelView = ::glm::inverse(transformBufferData.mTransposeModelView);

			const ::std::uint32_t dynamic_offset = entityIndex * static_cast<::std::uint32_t>(dynamicAlignment);
			::std::memcpy(static_cast<char*>(m_pTriangleListFillUboMappedData) + dynamic_offset, &transformBufferData, sizeof(transformBufferData));

			auto& thread = thread_data[threadIndex];

			::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
			for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
				drawCallCountMap.insert(::std::make_pair(thread.commandBufferData[i].drawCallCount, i));

			auto& commandBufferIndex = drawCallCountMap.begin()->second;
			auto& commandBufferData = thread.commandBufferData[commandBufferIndex];
			auto& commandBuffer = thread.commandBuffers[commandBufferIndex];
			if (!commandBufferData.is_command_buffer_open)
			{
				::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
				command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit;
				command_buffer_begin_info.inheritance_info = &inheritanceInfo;

				commandBuffer->begin_command_buffer(&command_buffer_begin_info);
                commandBuffer->bind_pipeline(m_pTriangleListFillPipeline);

				commandBufferData.is_command_buffer_open = true;
			}

			commandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::graphics, m_pTriangleListFillDescriptorTable, 1, &dynamic_offset);
			mesh_renderer->Draw(commandBuffer);

			commandBufferData.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
		});
		threadIndex = (threadIndex + 1) % thread_count;
		entityIndex++;
	}

	::std::vector<class ::framework::gpu::command_buffer*> commandBuffers;
	for (::std::uint32_t t = 0; t < thread_count; t++)
	{
		thread_pool.threads[t]->addJob([=, &commandBuffers, &commandBuffersMutex]
		{
			auto& thread = thread_data[t];
			for (::std::uint32_t i = 0; i < thread.commandBufferData.size(); i++)
			{
				if (thread.commandBufferData[i].is_command_buffer_open)
				{
					thread.commandBuffers[i]->end_command_buffer();
					thread.commandBufferData[i].is_command_buffer_open = false;
					thread.commandBufferData[i].drawCallCount = 0;
					::std::lock_guard<::std::mutex> lock(commandBuffersMutex);
					commandBuffers.push_back(thread.commandBuffers[i]);
				}
			}
		});
	}

	thread_pool.wait();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::rect_2d render_area{ ::framework::gpu::offset_2d{ 0, 0 }, ::framework::gpu::extent_2d{ 0, 0 } };

	::framework::gpu::render_pass_begin_info renderPassBeginInfo;
	renderPassBeginInfo.frame_buffer = m_pFramebuffer;
	renderPassBeginInfo.render_pass = this->render_pass;
	renderPassBeginInfo.render_area = render_area;
	renderPassBeginInfo.clear_value_count = 0;
	renderPassBeginInfo.clear_values = nullptr;

	PipelineBarrier pipelineBarrier;
	pipelineBarrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	pipelineBarrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
	pipelineBarrier.src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	pipelineBarrier.dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

	::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
	buffer_memory_barrier.pipelineBarrierCount = 1;
	buffer_memory_barrier.pPipelineBarriers = &pipelineBarrier;
	buffer_memory_barrier.src_queue_family_index = 0;
	buffer_memory_barrier.dst_queue_family_index = 0;
	buffer_memory_barrier.buffer = m_pPrimitivesBuffer;
	buffer_memory_barrier.offset = 0;
	buffer_memory_barrier.size = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
	dependency_info.buffer_memory_barrier_count = 1;
	dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;

	m_pPrimaryCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
    m_pPrimaryCommandBuffer->dependency_info(0, 1, &dependency_info);
    m_pPrimaryCommandBuffer->begin_render_pass(&renderPassBeginInfo, ::framework::gpu::subpass_contents::secondary_command_buffers);
    m_pPrimaryCommandBuffer->execute_commands(commandBuffers.size(), commandBuffers.data());
    m_pPrimaryCommandBuffer->end_render_pass();
	m_pPrimaryCommandBuffer->end_command_buffer();

	class Queue * command_queue;
    device->get_queue(0, 0, &command_queue);

	::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	::framework::gpu::submit_info submit_infos[2];
	submit_infos[0].command_buffer_count = 1;
	submit_infos[0].command_buffers = &m_pPrimaryCommandBuffer;
	submit_infos[0].wait_semaphore_count = 0;
	submit_infos[0].wait_semaphores = nullptr;
	submit_infos[0].wait_dst_stage_mask = nullptr;
	submit_infos[0].signal_semaphore_count = 1;
	submit_infos[0].signal_semaphores = &m_pTriangleListFillCompleteSemaphore;

	submit_infos[1].command_buffer_count = 1;
	submit_infos[1].command_buffers = &m_pPrimitiveCounterServicingCommandBuffer;
	submit_infos[1].wait_semaphore_count = 1;
	submit_infos[1].wait_semaphores = &m_pTriangleListFillCompleteSemaphore;
	submit_infos[1].wait_dst_stage_mask = &pWaitDstStageMask;
	submit_infos[1].signal_semaphore_count = 0;
	submit_infos[1].signal_semaphores = nullptr;

	command_queue->submit(::std::size(submit_infos), submit_infos, m_pPrimitiveCounterServicingCompleteFence);

    device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
    device->reset_fences(1, &m_pPrimitiveCounterServicingCompleteFence);

	void * pMappedData;
    device->map_memory(m_pReadbackDeviceMemory, 0, sizeof(::std::uint32_t), 0, &pMappedData);
	::std::memcpy(&m_numTriangles, pMappedData, sizeof(::std::uint32_t));
    device->unmap_memory(m_pReadbackDeviceMemory);

	::std::cout << "primitivesCounter: " << m_numTriangles << ::std::endl;
}

void lbvh::buildBVH()
{
	class Queue * command_queue;
    device->get_queue(0, 0, &command_queue);

	::std::vector<CompactTriangle> triangles;
	// GlobalBoundComputing
	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t remaining = m_numTriangles;
		::std::forward_list<::std::uint32_t> pushConstant;
		pushConstant.push_front(remaining);

		PipelineBarrier pipelineBarriers[2];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::geometry_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pPrimitivesBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pPrimitivesBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pBoundsBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pBoundsBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pGlobalBoundComputingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pGlobalBoundComputingCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pGlobalBoundComputingCommandBuffer->bind_pipeline(m_pGlobalBoundComputingPipeline);
		m_pGlobalBoundComputingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pGlobalBoundComputingDescriptorTable, 0, nullptr);
        m_pGlobalBoundComputingCommandBuffer->push_constants(m_pGlobalBoundComputingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant.front()), &pushConstant.front());
        m_pGlobalBoundComputingCommandBuffer->dispatch((remaining + (64 * 64) - 1) / (64 * 64), 1, 1);

		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

		remaining = (remaining + (64 * 64) - 1) / (64 * 64);
		pushConstant.push_front(remaining);
		while (remaining > 1)
		{
            m_pGlobalBoundComputingCommandBuffer->dependency_info(0, 1, &dependency_info);
            m_pGlobalBoundComputingCommandBuffer->bind_pipeline(m_pReductionGlobalBoundComputingPipeline);
			m_pGlobalBoundComputingCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pReductionGlobalBoundComputingDescriptorTable, 0, nullptr);
            m_pGlobalBoundComputingCommandBuffer->push_constants(m_pReductionGlobalBoundComputingPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant.front()), &pushConstant.front());
            m_pGlobalBoundComputingCommandBuffer->dispatch((remaining + (64 * 64) - 1) / (64 * 64), 1, 1);

			remaining = (remaining + (64 * 64) - 1) / (64 * 64);
			pushConstant.push_front(remaining);
		}
		m_pGlobalBoundComputingCommandBuffer->end_command_buffer();

		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pGlobalBoundComputingCommandBuffer;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pGlobalBoundComputingCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);
	}//);

	if (false)
	{
        device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
        device->reset_fences(1, &m_pPrimitiveCounterServicingCompleteFence);

		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(Bound);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pGlobalBoundComputingCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pGlobalBoundComputingCommandBuffer->copy_buffer(m_pBoundsBuffer, m_pBoundsReadbackBuffer, 1, &buffer_copy);
		buffer_copy.size = sizeof(CompactTriangle) * m_numTriangles;
        m_pGlobalBoundComputingCommandBuffer->copy_buffer(m_pPrimitivesBuffer, m_pPrimitivesReadbackBuffer, 1, &buffer_copy);
		m_pGlobalBoundComputingCommandBuffer->end_command_buffer();

		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pGlobalBoundComputingCommandBuffer;
		submit_info.wait_semaphore_count = 0;
		submit_info.wait_semaphores = nullptr;
		submit_info.wait_dst_stage_mask = nullptr;
		submit_info.signal_semaphore_count = 0;
		submit_info.signal_semaphores = nullptr;
		command_queue->submit(1, &submit_info, m_pPrimitiveCounterServicingCompleteFence);

        device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);

		void * pMappedData;
		lbvh::Bound bound;
        device->map_memory(m_pBoundsReadbackDeviceMemory, 0, sizeof(lbvh::Bound), 0, &pMappedData);
		::std::memcpy(&bound, pMappedData, sizeof(lbvh::Bound));
        device->unmap_memory(m_pBoundsReadbackDeviceMemory);

		triangles.resize(m_numTriangles);
        device->map_memory(m_pPrimitivesReadbackDeviceMemory, 0,
                           sizeof(CompactTriangle) * m_numTriangles, 0, &pMappedData);
		::std::memcpy(triangles.data(), pMappedData, sizeof(CompactTriangle) * m_numTriangles);
        device->unmap_memory(m_pPrimitivesReadbackDeviceMemory);

		lbvh::Bound bound1;
		bound1.min = ::glm::vec3(FLT_MAX);
		bound1.max = ::glm::vec3(FLT_MIN);
		for (::std::uint32_t i = 0; i < m_numTriangles; i++)
		{
			::glm::vec3 pos[3];
			pos[0] = triangles[i].field0;
			pos[1] = triangles[i].field1;
			pos[2] = triangles[i].field2;
			//::std::cout << pos[0].x << pos[0].y, pos[0].z;
			//::std::cout << pos[1].x << pos[1].y, pos[1].z;
			//::std::cout << pos[2].x << pos[2].y, pos[2].z;
			for (::std::size_t j = 0; j < 3; j++)
			{
				bound1.min = ::glm::min(bound1.min, ::glm::vec3(pos[j]));
				bound1.max = ::glm::max(bound1.max, ::glm::vec3(pos[j]));
			}
		}
		::std::cout << "BBoxMin: " << bound.min.x << " " << bound.min.y << " " << bound.min.z << ::std::endl;
		::std::cout << "BBoxMax: " << bound.max.x << " " << bound.max.y << " " << bound.max.z << ::std::endl;

		::std::cout << "BBox1Min: " << bound1.min.x << " " << bound1.min.y << " " << bound1.min.z << ::std::endl;
		::std::cout << "BBox1Max: " << bound1.max.x << " " << bound1.max.y << " " << bound1.max.z << ::std::endl;
	}

	// GenerateMortonCodes
	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstant = m_numTriangles;

		PipelineBarrier pipelineBarriers[3];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pBoundsBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pBoundsBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pMortonCodesBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[2].pipelineBarrierCount = 1;
		buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].buffer = m_pPrimitiveIndicesBuffer;
		buffer_memory_barriers[2].offset = 0;
		buffer_memory_barriers[2].size = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pGenerateMortonCodesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pGenerateMortonCodesCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pGenerateMortonCodesCommandBuffer->bind_pipeline(m_pGenerateMortonCodesPipeline);
		m_pGenerateMortonCodesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pGenerateMortonCodesDescriptorTable, 0, nullptr);
        m_pGenerateMortonCodesCommandBuffer->push_constants(m_pGenerateMortonCodesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
        m_pGenerateMortonCodesCommandBuffer->dispatch((m_numTriangles + 64 - 1) / 64, 1, 1);

		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;

        m_pGenerateMortonCodesCommandBuffer->dependency_info(0, 1, &dependency_info);
		m_pGenerateMortonCodesCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pGenerateMortonCodesCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pGlobalBoundComputingCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pGenerateMortonCodesCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);
	}//);

	// Sort morton codes --------------------------------
	//::std::async(::std::launch::async, [=]
	{
		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		lsd_radix_sort::setSize(m_pRadixSortData, m_numTriangles);
		lsd_radix_sort::execute(m_pRadixSortData, command_queue, 1, &m_pGenerateMortonCodesCompleteSemaphore, &pWaitDstStageMask, 1, &m_pSortMortonCodesCompleteSemaphore, nullptr);
	}//);

	// Readback from gpu
	if (true)
	{
		class Queue * transferQueue;
        device->get_queue(0, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		class ::framework::gpu::command_pool * command_pool;
        device->create_command_pool(&command_pool_create_info, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = COMMAND_BUFFER_LEVEL_PRIMARY;
		command_buffer_allocate_info.command_buffer_count = 1;
		class ::framework::gpu::command_buffer * copyCmd;

        device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * m_numTriangles;

		PipelineBarrier pipelineBarrier;
		pipelineBarrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarrier.src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarrier.dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.pipelineBarrierCount = 1;
		buffer_memory_barrier.pPipelineBarriers = &pipelineBarrier;
		buffer_memory_barrier.src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barrier.dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barrier.buffer = m_pMortonCodesBuffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		copyCmd->begin_command_buffer(&command_buffer_begin_info);
        copyCmd->dependency_info(0, 1, &dependency_info);
        copyCmd->copy_buffer(m_pMortonCodesBuffer, m_pUnsortedDataReadback, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		class ::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;

		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &copyCmd;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pSortMortonCodesCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pSortMortonCodesCompleteSemaphore;
		command_queue->submit(1, &submit_info, fence);

        device->wait_for_fences(1, &fence, true, UINT64_MAX);
        device->destroy_fence(fence);

		::std::vector<::std::uint32_t> data(m_numTriangles);

		void * pMappedData;
        device->map_memory(m_pUnsortedDataReadbackMemory, 0, sizeof(::std::uint32_t) * m_numTriangles, 0, &pMappedData);
		::std::memcpy(data.data(), pMappedData, sizeof(::std::uint32_t) * m_numTriangles);
        device->unmap_memory(m_pUnsortedDataReadbackMemory);

		//::std::ofstream myfile;
		//myfile.open("lbvh.txt");
		for (::std::size_t i = 1; i < data.size(); i++)
		{
			if (data[i] < data[i - 1])
			{
				::std::cerr << "error";
			}
			//::std::cout << data[i] << ", ";
			//myfile << data[i] << ",";
		}
		//myfile.close();
		::std::cout << " Done!";
	}
	// Sort morton codes --------------------------------

	// Construct radix tree --------------------------------
	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstant = m_numTriangles;

		PipelineBarrier pipelineBarriers[3];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pMortonCodesBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pMortonCodesBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pPrimitiveIndicesBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pPrimitiveIndicesBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[2].pipelineBarrierCount = 1;
		buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].buffer = m_pRadixTreeBuffer;
		buffer_memory_barriers[2].offset = 0;
		buffer_memory_barriers[2].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pConstructRadixTreeCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pConstructRadixTreeCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pConstructRadixTreeCommandBuffer->bind_pipeline(m_pConstructRadixTreePipeline);
		m_pConstructRadixTreeCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pConstructRadixTreeDescriptorTable, 0, nullptr);
        m_pConstructRadixTreeCommandBuffer->push_constants(m_pConstructRadixTreePipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
        m_pConstructRadixTreeCommandBuffer->dispatch(((m_numTriangles - 1) + 256 - 1) / 256, 1, 1);
		m_pConstructRadixTreeCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pConstructRadixTreeCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pSortMortonCodesCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pConstructRadixTreeCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);

		/*if (false)
		{
			device->WaitForFences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
			device->ResetFences(1, &m_pPrimitiveCounterServicingCompleteFence);

			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barriers[0].src_queue_family_index = 0;
			buffer_memory_barriers[0].dst_queue_family_index = 0;
			buffer_memory_barriers[0].buffer = m_pRadixTreeBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].src_queue_family_index = 0;
			buffer_memory_barriers[1].dst_queue_family_index = 0;
			buffer_memory_barriers[1].buffer = m_pRadixTreeBuffer;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			::framework::gpu::buffer_copy buffer_copy{};
			buffer_copy.size = sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1);

			m_pCalculateNodeBoundingBoxesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
			m_pCalculateNodeBoundingBoxesCommandBuffer->PipelineBarrier(::framework::gpu::pipeline_stage_flags::compute_shader_bit, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
			m_pCalculateNodeBoundingBoxesCommandBuffer->copy_buffer(m_pRadixTreeBuffer, m_pRadixTreeReadbackBuffer, 1, &buffer_copy);
			m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
			m_pCalculateNodeBoundingBoxesCommandBuffer->end_command_buffer();

			::framework::gpu::submit_info submit_info;
			submit_info.command_buffer_count = 1;
			submit_info.command_buffers = &m_pCalculateNodeBoundingBoxesCommandBuffer;
			submit_info.wait_semaphore_count = 0;
			submit_info.wait_semaphores = nullptr;
			submit_info.signal_semaphore_count = 0;
			submit_info.signal_semaphores = nullptr;
			command_queue->submit(1, &submit_info, m_pPrimitiveCounterServicingCompleteFence);

			device->wait_for_fences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
			device->reset_fences(1, &m_pPrimitiveCounterServicingCompleteFence);

			::std::vector<lbvh::CompactTreeNode> tree(m_numTriangles * 2 - 1);

			void * pMappedData;
			device->map_memory(m_pRadixTreeReadbackDeviceMemory, 0, sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1), 0, &pMappedData);
			::std::memcpy(tree.data(), pMappedData, sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1));
			device->unmap_memory(m_pRadixTreeReadbackDeviceMemory);

			float sah = 0;
			float nodeTraversalCost = 1.2f;
			float triangleIntersectionCost = 1.0f;

			int root = 0;

			float totalSA = tree[root].field2.w;
			float sumInternalSA = totalSA;
			float sumLeavesSA = 0.0;
			//::std::ofstream file;
			//file.open("dumpTree.txt");

			//file << m_numTriangles << ::std::endl;
			//for (unsigned int i = 0; i < m_numTriangles * 2 - 1; ++i)
			//{
			//	file << "i: " << i << " Data: " << tree[i].field0.x << " Left: " << tree[i].field0.y <<
			//		" Right: " << tree[i].field0.z << " Parent: " << tree[i].field0.w << " ";
			//	file << "BBoxMin: " << tree[i].field1.x << " " << tree[i].field1.y << " " <<
			//		tree[i].field1.z;
			//	file << " BBoxMax: " << tree[i].field2.x << " " << tree[i].field2.y << " " <<
			//		tree[i].field2.z << ::std::endl;
			//}
			//file.close();

			// Internal nodes
			for (unsigned int i = 0; i < m_numTriangles - 1; ++i)
			{
				if (i != root)
				{
					sumInternalSA += tree[i].field2.w;
					//file << tree[i].field0.y << ", ";
				}
			}

			// Leaves
			for (unsigned int i = m_numTriangles - 1; i < 2 * m_numTriangles - 1; ++i)
			{
				sumLeavesSA += tree[i].field2.w;
			}

			sah = (nodeTraversalCost * sumInternalSA + triangleIntersectionCost * sumLeavesSA) / totalSA;
			printf("lbvh SAH: %f\n", sah);

			//printBVH(0, tree.data());
		}*/
	}//);
	// Construct radix tree --------------------------------

	// CalculateNodeBoundingBoxes --------------------------------
	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstant = m_numTriangles;

		PipelineBarrier pipelineBarriers[3];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pCounterBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pCounterBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[2].pipelineBarrierCount = 1;
		buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		buffer_memory_barriers[2].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[2].buffer = m_pRadixTreeBuffer;
		buffer_memory_barriers[2].offset = 0;
		buffer_memory_barriers[2].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barriers[0];
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pCalculateNodeBoundingBoxesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pCalculateNodeBoundingBoxesCommandBuffer->fill_buffer(m_pCounterBuffer, 0, sizeof(::std::uint32_t) *
                                                                                     (m_numTriangles - 1), 0xFFFFFFFF);

		dependency_info.buffer_memory_barrier_count = 2;
		dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

        m_pCalculateNodeBoundingBoxesCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pCalculateNodeBoundingBoxesCommandBuffer->bind_pipeline(m_pCalculateNodeBoundingBoxesPipeline);
		m_pCalculateNodeBoundingBoxesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCalculateNodeBoundingBoxesDescriptorTable, 0, nullptr);
        m_pCalculateNodeBoundingBoxesCommandBuffer->push_constants(m_pCalculateNodeBoundingBoxesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
        m_pCalculateNodeBoundingBoxesCommandBuffer->dispatch((m_numTriangles + 128 - 1) / 128, 1, 1);
		m_pCalculateNodeBoundingBoxesCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pCalculateNodeBoundingBoxesCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pConstructRadixTreeCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);

	}//);
	// CalculateNodeBoundingBoxes --------------------------------

	// Optimize --------------------------------
	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstants[3]{ m_numTriangles, m_treeletSize, m_scheduleSize };

		PipelineBarrier pipelineBarriers[5];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[3].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[4].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[4].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[4].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[4].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[5];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = 0;
		buffer_memory_barriers[0].dst_queue_family_index = 0;
		buffer_memory_barriers[0].buffer = m_pCounterBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = 0;
		buffer_memory_barriers[1].dst_queue_family_index = 0;
		buffer_memory_barriers[1].buffer = m_pCounterBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pCounterBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[2].pipelineBarrierCount = 1;
		buffer_memory_barriers[2].pPipelineBarriers = &pipelineBarriers[2];
		buffer_memory_barriers[2].src_queue_family_index = 0;
		buffer_memory_barriers[2].dst_queue_family_index = 0;
		buffer_memory_barriers[2].buffer = m_pRadixTreeBuffer;
		buffer_memory_barriers[2].offset = 0;
		buffer_memory_barriers[2].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[3].pipelineBarrierCount = 1;
		buffer_memory_barriers[3].pPipelineBarriers = &pipelineBarriers[3];
		buffer_memory_barriers[3].src_queue_family_index = 0;
		buffer_memory_barriers[3].dst_queue_family_index = 0;
		buffer_memory_barriers[3].buffer = m_pSubtreeTrianglesCountBuffer;
		buffer_memory_barriers[3].offset = 0;
		buffer_memory_barriers[3].size = m_pSubtreeTrianglesCountBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[4].pipelineBarrierCount = 1;
		buffer_memory_barriers[4].pPipelineBarriers = &pipelineBarriers[4];
		buffer_memory_barriers[4].src_queue_family_index = 0;
		buffer_memory_barriers[4].dst_queue_family_index = 0;
		buffer_memory_barriers[4].buffer = m_pDistanceMatrixBuffer;
		buffer_memory_barriers[4].offset = 0;
		buffer_memory_barriers[4].size = m_pDistanceMatrixBuffer->GetBufferCreateInfo()->size;
		
		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barriers[0];
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pAgglomerativeTreeletOptimizerCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
		for (::std::uint32_t i = 0; i < /*m_iterations*/0; i++)
		{
			if (m_treeletSize > MAX_TREELET_SIZE_DIST_SHARED_MEM)
			{
                m_pAgglomerativeTreeletOptimizerCommandBuffer->dependency_info(0, 1, &dependency_info);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->fill_buffer(m_pCounterBuffer, 0,
                                                                           sizeof(::std::uint32_t) *
                                                                           (m_numTriangles - 1), 0);

				dependency_info.buffer_memory_barrier_count = 4;
				dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

                m_pAgglomerativeTreeletOptimizerCommandBuffer->dependency_info(0, 1, &dependency_info);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->bind_pipeline(m_pAgglomerativeTreeletOptimizerPipeline);
				m_pAgglomerativeTreeletOptimizerCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pAgglomerativeTreeletOptimizerDescriptorTable, 0, nullptr);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->push_constants(m_pAgglomerativeTreeletOptimizerPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), pushConstants);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->dispatch((m_numTriangles + 128 - 1) / 128, 1, 1);
			}
			else
			{
                m_pAgglomerativeTreeletOptimizerCommandBuffer->dependency_info(0, 1, &dependency_info);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->fill_buffer(m_pCounterBuffer, 0,
                                                                           sizeof(::std::uint32_t) *
                                                                           (m_numTriangles - 1), 0);

				dependency_info.buffer_memory_barrier_count = 4;
				dependency_info.buffer_memory_barriers = &buffer_memory_barriers[1];

                m_pAgglomerativeTreeletOptimizerCommandBuffer->dependency_info(0, 1, &dependency_info);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->bind_pipeline(m_pAgglomerativeSmallTreeletOptimizerPipeline);
				m_pAgglomerativeTreeletOptimizerCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pAgglomerativeSmallTreeletOptimizerDescriptorTable, 0, nullptr);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->push_constants(m_pAgglomerativeSmallTreeletOptimizerPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), pushConstants);
                m_pAgglomerativeTreeletOptimizerCommandBuffer->dispatch((m_numTriangles + 128 - 1) / 128, 1, 1);
			}

			pushConstants[1] *= 2;
		}
		m_pAgglomerativeTreeletOptimizerCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pAgglomerativeTreeletOptimizerCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pAgglomerativeTreeletOptimizerCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);

		/*if (false)
		{
			device->WaitForFences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
			device->ResetFences(1, &m_pPrimitiveCounterServicingCompleteFence);

			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barriers[0].src_queue_family_index = 0;
			buffer_memory_barriers[0].dst_queue_family_index = 0;
			buffer_memory_barriers[0].buffer = m_pRadixTreeBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].src_queue_family_index = 0;
			buffer_memory_barriers[1].dst_queue_family_index = 0;
			buffer_memory_barriers[1].buffer = m_pRadixTreeBuffer;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;
			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].src_queue_family_index = 0;
			buffer_memory_barriers[2].dst_queue_family_index = 0;
			buffer_memory_barriers[2].buffer = m_pDebugBuffer;
			buffer_memory_barriers[2].offset = 0;
			buffer_memory_barriers[2].size = m_pDebugBuffer->GetBufferCreateInfo()->size;

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			::framework::gpu::buffer_copy buffer_copy{};
			buffer_copy.size = sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1);

			m_pAgglomerativeTreeletOptimizerCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->PipelineBarrier(::framework::gpu::pipeline_stage_flags::compute_shader_bit, ::framework::gpu::pipeline_stage_flags::transfer_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[0], 0, nullptr);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->CopyBuffer(m_pRadixTreeBuffer, m_pRadixTreeReadbackBuffer, 1, &buffer_copy);
			buffer_copy.size = sizeof(::std::uint32_t) * (m_numTriangles * 2 - 1);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->CopyBuffer(m_pDebugBuffer, m_pDebugReadbackBuffer, 1, &buffer_copy);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->dependency_info(::framework::gpu::pipeline_stage_flags::transfer_bit, ::framework::gpu::pipeline_stage_flags::compute_shader_bit, 0, 0, nullptr, 1, &buffer_memory_barriers[1], 0, nullptr);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->fill_buffer(m_pDebugCounterBuffer, 0, sizeof(::std::uint32_t) * (m_numTriangles - 1), 0);
			m_pAgglomerativeTreeletOptimizerCommandBuffer->end_command_buffer();

			::framework::gpu::submit_info submit_info;
			submit_info.command_buffer_count = 1;
			submit_info.command_buffers = &m_pAgglomerativeTreeletOptimizerCommandBuffer;
			submit_info.wait_semaphore_count = 0;
			submit_info.wait_semaphores = nullptr;
			submit_info.signal_semaphore_count = 0;
			submit_info.signal_semaphores = nullptr;
			command_queue->submit(1, &submit_info, m_pPrimitiveCounterServicingCompleteFence);

			device->WaitForFences(1, &m_pPrimitiveCounterServicingCompleteFence, true, UINT64_MAX);
			device->reset_fences(1, &m_pPrimitiveCounterServicingCompleteFence);

			::std::vector<::std::uint32_t> debugInfo(m_numTriangles * 2 - 1);
			{
				void * pMappedData;
				device->map_memory(m_pDebugReadbackDeviceMemory, 0, sizeof(::std::uint32_t) * (m_numTriangles * 2 - 1), 0, &pMappedData);
				::std::memcpy(debugInfo.data(), pMappedData, sizeof(::std::uint32_t) * (m_numTriangles * 2 - 1));
				device->UnmapMemory(m_pDebugReadbackDeviceMemory);
			}

			::std::vector<lbvh::CompactTreeNode> tree(m_numTriangles * 2 - 1);
			{
				void * pMappedData;
				device->map_memory(m_pRadixTreeReadbackDeviceMemory, 0, sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1), 0, &pMappedData);
				::std::memcpy(tree.data(), pMappedData, sizeof(lbvh::CompactTreeNode) * (m_numTriangles * 2 - 1));
				device->UnmapMemory(m_pRadixTreeReadbackDeviceMemory);
			}

			::std::for_each(debugInfo.begin(), debugInfo.end(), [](::std::uint32_t &n) { if (n > 1) ::std::cout << n << ", "; });

			float sah = 0;
			float nodeTraversalCost = 1.2f;
			float triangleIntersectionCost = 1.0f;

			int root = 0;

			float totalSA = tree[root].field2.w;
			float sumInternalSA = totalSA;
			float sumLeavesSA = 0.0;
			//::std::ofstream file;
			//file.open("dumpTree.txt");

			//file << m_numTriangles << ::std::endl;
			//for (unsigned int i = 0; i < m_numTriangles * 2 - 1; ++i)
			//{
			//	file << "i: " << i << " Data: " << tree[i].field0.x << " Left: " << tree[i].field0.y <<
			//		" Right: " << tree[i].field0.z << " Parent: " << tree[i].field0.w << " ";
			//	file << "BBoxMin: " << tree[i].field1.x << " " << tree[i].field1.y << " " <<
			//		tree[i].field1.z;
			//	file << " BBoxMax: " << tree[i].field2.x << " " << tree[i].field2.y << " " <<
			//		tree[i].field2.z << ::std::endl;
			//}
			//file.close();

			// Internal nodes
			for (unsigned int i = 0; i < m_numTriangles - 1; ++i)
			{
				if (i != root)
				{
					sumInternalSA += tree[i].field2.w;
					//file << tree[i].field0.y << ", ";
				}
			}

			// Leaves
			for (unsigned int i = m_numTriangles - 1; i < 2 * m_numTriangles - 1; ++i)
			{
				sumLeavesSA += tree[i].field2.w;
			}

			sah = (nodeTraversalCost * sumInternalSA + triangleIntersectionCost * sumLeavesSA) / totalSA;
			printf("ATRBVH SAH: %f\n", sah);

			printBVH(0, tree.data());

			if (false)
			{

				struct StackEntry
				{
					signed int index; // My index
					signed int nodeDataIndex; // Originally stored index

					StackEntry(signed int n = 0, signed int i = 0) : index(n), nodeDataIndex(i) {}
				};

				// Construct data.
				::std::vector<::glm::ivec4> nodeData(4);
				::std::vector<::glm::ivec4> triWoopData;
				::std::vector<signed int> triIndexData;
				::std::vector<StackEntry> stack;
				stack.push_back(StackEntry(0, 0));
				::glm::vec4 m_woop[3];

				int counter = 0;
				while (stack.size())
				{
					++counter;
					StackEntry entry = stack.back();
					stack.pop_back();
					signed int index = entry.index;
					int cidx[2];

					// Process children.
					signed int childIndices[2];
					childIndices[0] = tree[index].field0.y;
					childIndices[1] = tree[index].field0.z;

					for (int k = 0; k < 2; ++k)
					{
						int childIndex = childIndices[k];

						// Inner node => push to stack.
						if (childIndex < (m_numTriangles - 1))
						{
							cidx[k] = nodeData.size();
							stack.emplace_back(StackEntry(childIndex, nodeData.size()));
							nodeData.resize(nodeData.size() + 4);
						}
						else
						{
							// Leaf => append triangles.
							cidx[k] = ~triWoopData.size();

							// Loop over triangles in a leaf. BVHTree only has one triangle per leaf
							int dataIndex = tree[childIndex].field0.w;
							//woopifyTri(indexBuffer, vertexBuffer, dataIndex);
							{
								const ::glm::vec3 v0(triangles[dataIndex].field0);
								const ::glm::vec3 v1(triangles[dataIndex].field1);
								const ::glm::vec3 v2(triangles[dataIndex].field2);

								::glm::vec3 col0(v0 - v2);
								::glm::vec3 col1(v1 - v2);
								::glm::vec3 col2(::glm::cross(v0 - v2, v1 - v2));
								::glm::vec3 col3(v2);
								::glm::mat4 mtx;
								mtx[0] = ::glm::vec4(col0.x, col1.x, col2.x, col3.x);
								mtx[1] = ::glm::vec4(col0.y, col1.y, col2.y, col3.y);
								mtx[2] = ::glm::vec4(col0.z, col1.z, col2.z, col3.z);
								mtx[3] = ::glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
								mtx = ::glm::inverse(mtx);

								m_woop[0] = ::glm::vec4(mtx[2].x, mtx[2].y, mtx[2].z, -mtx[2].w);
								m_woop[1] = ::glm::vec4(mtx[0].x, mtx[0].y, mtx[0].z, mtx[0].w);
								m_woop[2] = ::glm::vec4(mtx[1].x, mtx[1].y, mtx[1].z, mtx[1].w);
							}

							triWoopData.push_back(*(::glm::ivec4*)&m_woop[0]);
							triWoopData.push_back(*(::glm::ivec4*)&m_woop[1]);
							triWoopData.push_back(*(::glm::ivec4*)&m_woop[2]);
							triIndexData.push_back(dataIndex);
							triIndexData.push_back(0);
							triIndexData.push_back(0);

							// Terminator.
							triWoopData.push_back(::glm::ivec4(0x80000000, 0x80000000, 0x80000000, 0x80000000));
							triIndexData.push_back(0);
						}
					}

					::glm::vec3 leftMin = tree[childIndices[0]].field1;
					::glm::vec3 leftMax = tree[childIndices[0]].field2;
					::glm::vec3 rightMin = tree[childIndices[1]].field1;
					::glm::vec3 rightMax = tree[childIndices[1]].field2;

					// Write entry.
					::glm::ivec4 * dst = &nodeData[entry.nodeDataIndex];
					dst[0] = ::glm::ivec4(*(::std::uint32_t*)&(leftMin.x), *(::std::uint32_t*)&(leftMax.x), *(::std::uint32_t*)&(leftMin.y), *(::std::uint32_t*)&(leftMax.y));
					dst[1] = ::glm::ivec4(*(::std::uint32_t*)&(rightMin.x), *(::std::uint32_t*)&(rightMax.x), *(::std::uint32_t*)&(rightMin.y), *(::std::uint32_t*)&(rightMax.y));
					dst[2] = ::glm::ivec4(*(::std::uint32_t*)&(leftMin.z), *(::std::uint32_t*)&(leftMax.z), *(::std::uint32_t*)&(rightMin.z), *(::std::uint32_t*)&(rightMax.z));
					//dst[3] = XMINT4(cidx[0] < m_numTriangles - 1 ? cidx[0] * 4 : ~((cidx[0] - (m_numTriangles - 1)) * 4), cidx[1] < m_numTriangles - 1 ? cidx[1] * 4 : ~((cidx[1] - (m_numTriangles - 1)) * 4), 0, 0);
					dst[3] = ::glm::ivec4(cidx[0], cidx[1], 0, 0);
				}

				//nodeData.resize((m_numTriangles - 1) * 4);
				//for (int i = 0; i < m_numTriangles - 1; i++)
				//{
				//	signed int cidx[2];
				//	int childIndices[2];
				//	childIndices[0] = tree[i].field0.y;
				//	childIndices[1] = tree[i].field0.z;

				//	for (int k = 0; k < 2; ++k)
				//	{
				//		int childIndex = childIndices[k];

				//		if (childIndex < (m_numTriangles - 1))
				//		{
				//			cidx[k] = childIndex * 4;
				//		}
				//		else
				//		{
				//			cidx[k] = ~((childIndex - (m_numTriangles - 1)) * 4);
				//		}
				//	}

				//	::glm::vec3 leftMin = tree[cidx[0]].field1;
				//	::glm::vec3 leftMax = tree[cidx[0]].field2;
				//	::glm::vec3 rightMin = tree[cidx[1]].field1;
				//	::glm::vec3 rightMax = tree[cidx[1]].field2;
				//	//Bound left = tree[childIndices[0]].bound;
				//	//Bound right = tree[childIndices[1]].bound;
				//	//left.min = XMFLOAT3(-1000, -1000, -1000);
				//	//left.max = XMFLOAT3(1000, 1000, 1000);
				//	//right.min = XMFLOAT3(-1000, -1000, -1000);
				//	//right.max = XMFLOAT3(1000, 1000, 1000);

				//	// Write entry.
				//	::glm::ivec4 * dst = &nodeData[i * 4];
				//	dst[0] = ::glm::ivec4(*(::std::uint32_t*)&(leftMin.x), *(::std::uint32_t*)&(leftMax.x), *(::std::uint32_t*)&(leftMin.y), *(::std::uint32_t*)&(leftMax.y));
				//	dst[1] = ::glm::ivec4(*(::std::uint32_t*)&(rightMin.x), *(::std::uint32_t*)&(rightMax.x), *(::std::uint32_t*)&(rightMin.y), *(::std::uint32_t*)&(rightMax.y));
				//	dst[2] = ::glm::ivec4(*(::std::uint32_t*)&(leftMin.z), *(::std::uint32_t*)&(leftMax.z), *(::std::uint32_t*)&(rightMin.z), *(::std::uint32_t*)&(rightMax.z));
				//	//dst[3] = XMINT4(cidx[0] < m_numTriangles - 1 ? cidx[0] * 4 : ~((cidx[0] - (m_numTriangles - 1)) * 4), cidx[1] < m_numTriangles - 1 ? cidx[1] * 4 : ~((cidx[1] - (m_numTriangles - 1)) * 4), 0, 0);
				//	dst[3] = ::glm::ivec4(cidx[0], cidx[1], 0, 0);
				//}

				//triWoopData.resize(m_numTriangles * 4);
				//triIndexData.resize(m_numTriangles * 4);
				//for (int i = 0; i < m_numTriangles; i++)
				//{
				//	int dataIndex = tree[i + m_numTriangles - 1].field0.w;
				//	const ::glm::vec3 v0(triangles[dataIndex].field0);
				//	const ::glm::vec3 v1(triangles[dataIndex].field1);
				//	const ::glm::vec3 v2(triangles[dataIndex].field2);

				//	::glm::vec3 col0(v0 - v2);
				//	::glm::vec3 col1(v1 - v2);
				//	::glm::vec3 col2(::glm::cross(v0 - v2, v1 - v2));
				//	::glm::vec3 col3(v2);
				//	::glm::mat4 mtx;
				//	mtx[0] = ::glm::vec4(col0.x, col1.x, col2.x, col3.x);
				//	mtx[1] = ::glm::vec4(col0.y, col1.y, col2.y, col3.y);
				//	mtx[2] = ::glm::vec4(col0.z, col1.z, col2.z, col3.z);
				//	mtx[3] = ::glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
				//	mtx = ::glm::inverse(mtx);

				//	m_woop[0] = ::glm::vec4(mtx[2].x, mtx[2].y, mtx[2].z, -mtx[2].w);
				//	m_woop[1] = ::glm::vec4(mtx[0].x, mtx[0].y, mtx[0].z, mtx[0].w);
				//	m_woop[2] = ::glm::vec4(mtx[1].x, mtx[1].y, mtx[1].z, mtx[1].w);

				//	if (m_woop[0].x == 0.0f)
				//		m_woop[0].x = 0.0f;

				//	triWoopData[i * 4 + 0] = *(::glm::ivec4*)&m_woop[0];
				//	triWoopData[i * 4 + 1] = *(::glm::ivec4*)&m_woop[1];
				//	triWoopData[i * 4 + 2] = *(::glm::ivec4*)&m_woop[2];
				//	triIndexData[i * 4 + 0] = dataIndex;
				//	triIndexData[i * 4 + 1] = 0;
				//	triIndexData[i * 4 + 2] = 0;

				//	// Terminator.
				//	triWoopData[i * 4 + 3] = ::glm::ivec4(0x80000000, 0x80000000, 0x80000000, 0x80000000);
				//	triIndexData[i * 4 + 3] = 0;
				//}
				{
					void * pMappedData;
					device->map_memory(m_pNodesUploadDeviceMemory, 0, sizeof(::glm::ivec4) * nodeData.size(), 0, &pMappedData);
					::std::memcpy(pMappedData, nodeData.data(), sizeof(::glm::ivec4) * nodeData.size());
					device->UnmapMemory(m_pNodesUploadDeviceMemory);

					class Queue * transferQueue;
					device->get_queue(0, 0, &transferQueue);

					::framework::gpu::command_pool_create_info command_pool_create_info;
					command_pool_create_info.flags = reset_command_buffer_bit;
					command_pool_create_info.queue_family_index = 0;
					class CommandPool * command_pool;
					device->CreateCommandPool(&command_pool_create_info, &command_pool);

					::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
					command_buffer_allocate_info.command_pool = command_pool;
					command_buffer_allocate_info.level = primary;
					command_buffer_allocate_info.command_buffer_count = 1;
					class CommandBuffer * copyCmd;
					device->AllocateCommandBuffers(&command_buffer_allocate_info, &copyCmd);

					::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
					command_buffer_begin_info.flags = simultaneous_use_bit;
					copyCmd->begin_command_buffer(&command_buffer_begin_info);
					::framework::gpu::buffer_copy buffer_copy{};
					buffer_copy.size = sizeof(::glm::ivec4) * nodeData.size();
					copyCmd->CopyBuffer(m_pNodesUploadBuffer, m_pNodesBuffer, 1, &buffer_copy);
					copyCmd->end_command_buffer();

					class ::framework::gpu::fence * class ::framework::gpu::fence;
					::framework::gpu::fence_create_info fence_create_info;
					fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
					device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

					::framework::gpu::submit_info submit_info;
					submit_info.wait_semaphore_count = 0;
					submit_info.wait_semaphores = nullptr;
					submit_info.wait_dst_stage_mask = nullptr;
					submit_info.command_buffer_count = 1;
					submit_info.command_buffers = &copyCmd;
					submit_info.signal_semaphore_count = 0;
					submit_info.signal_semaphores = nullptr;
					transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

					device->WaitForFences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
					device->DestroyFence(class ::framework::gpu::fence);
				}
				{
					void * pMappedData;
					device->map_memory(m_pTriWoopUploadDeviceMemory, 0, sizeof(::glm::ivec4) * triWoopData.size(), 0, &pMappedData);
					::std::memcpy(pMappedData, triWoopData.data(), sizeof(::glm::ivec4) * triWoopData.size());
					device->UnmapMemory(m_pTriWoopUploadDeviceMemory);

					class Queue * transferQueue;
					device->get_queue(0, 0, &transferQueue);

					::framework::gpu::command_pool_create_info command_pool_create_info;
					command_pool_create_info.flags = reset_command_buffer_bit;
					command_pool_create_info.queue_family_index = 0;
					class CommandPool * command_pool;
					device->CreateCommandPool(&command_pool_create_info, &command_pool);

					::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
					command_buffer_allocate_info.command_pool = command_pool;
					command_buffer_allocate_info.level = primary;
					command_buffer_allocate_info.command_buffer_count = 1;
					class CommandBuffer * copyCmd;
					device->AllocateCommandBuffers(&command_buffer_allocate_info, &copyCmd);

					::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
					command_buffer_begin_info.flags = simultaneous_use_bit;
					copyCmd->begin_command_buffer(&command_buffer_begin_info);
					::framework::gpu::buffer_copy buffer_copy{};
					buffer_copy.size = sizeof(::glm::ivec4) * triWoopData.size();
					copyCmd->CopyBuffer(m_pTriWoopUploadBuffer, m_pTriWoopBuffer, 1, &buffer_copy);
					copyCmd->end_command_buffer();

					class ::framework::gpu::fence * class ::framework::gpu::fence;
					::framework::gpu::fence_create_info fence_create_info;
					fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
					device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

					::framework::gpu::submit_info submit_info;
					submit_info.wait_semaphore_count = 0;
					submit_info.wait_semaphores = nullptr;
					submit_info.wait_dst_stage_mask = nullptr;
					submit_info.command_buffer_count = 1;
					submit_info.command_buffers = &copyCmd;
					submit_info.signal_semaphore_count = 0;
					submit_info.signal_semaphores = nullptr;
					transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

					device->WaitForFences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
					device->DestroyFence(class ::framework::gpu::fence);
				}
				{
					void * pMappedData;
					device->map_memory(m_pTriIndexUploadDeviceMemory, 0, sizeof(int) * triIndexData.size(), 0, &pMappedData);
					::std::memcpy(pMappedData, triIndexData.data(), sizeof(int) * triIndexData.size());
					device->unmap_memory(m_pTriIndexUploadDeviceMemory);

					class Queue * transferQueue;
					device->get_queue(0, 0, &transferQueue);

					::framework::gpu::command_pool_create_info command_pool_create_info;
					command_pool_create_info.flags = reset_command_buffer_bit;
					command_pool_create_info.queue_family_index = 0;
					class CommandPool * command_pool;
					device->create_command_pool(&command_pool_create_info, &command_pool);

					::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
					command_buffer_allocate_info.command_pool = command_pool;
					command_buffer_allocate_info.level = primary;
					command_buffer_allocate_info.command_buffer_count = 1;
					class CommandBuffer * copyCmd;
					device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

					::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
					command_buffer_begin_info.flags = simultaneous_use_bit;
					copyCmd->begin_command_buffer(&command_buffer_begin_info);
					::framework::gpu::buffer_copy buffer_copy{};
					buffer_copy.size = sizeof(int) * triIndexData.size();
					copyCmd->copy_buffer(m_pTriIndexUploadBuffer, m_pTriIndexBuffer, 1, &buffer_copy);
					copyCmd->end_command_buffer();

					class ::framework::gpu::fence * class ::framework::gpu::fence;
					::framework::gpu::fence_create_info fence_create_info;
					fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
					device->create_fence(&fence_create_info, nullptr, &class ::framework::gpu::fence);

					::framework::gpu::submit_info submit_info;
					submit_info.wait_semaphore_count = 0;
					submit_info.wait_semaphores = nullptr;
					submit_info.wait_dst_stage_mask = nullptr;
					submit_info.command_buffer_count = 1;
					submit_info.command_buffers = &copyCmd;
					submit_info.signal_semaphore_count = 0;
					submit_info.signal_semaphores = nullptr;
					transferQueue->submit(1, &submit_info, class ::framework::gpu::fence);

					device->wait_for_fences(1, &class ::framework::gpu::fence, true, UINT64_MAX);
					device->destroy_fence(class ::framework::gpu::fence);
				}
			}
		}*/
	}//);
	// Optimize --------------------------------

	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstant = m_numTriangles;

		PipelineBarrier pipelineBarriers[2];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pRadixTreeBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pRadixTreeBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pNodesBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pNodesBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pCreateNodesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pCreateNodesCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pCreateNodesCommandBuffer->bind_pipeline(m_pCreateNodesPipeline);
		m_pCreateNodesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCreateNodesDescriptorTable, 0, nullptr);
        m_pCreateNodesCommandBuffer->push_constants(m_pCreateNodesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
        m_pCreateNodesCommandBuffer->dispatch((m_numTriangles - 1 + 64 - 1) / 64, 1, 1);
		m_pCreateNodesCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pCreateNodesCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pAgglomerativeTreeletOptimizerCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pCreateNodesCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);
	}//);

	//::std::async(::std::launch::async, [=]
	{
		::std::uint32_t pushConstant = m_numTriangles;

		PipelineBarrier pipelineBarriers[2];
		pipelineBarriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		pipelineBarriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

		pipelineBarriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		pipelineBarriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		pipelineBarriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
		buffer_memory_barriers[0].pipelineBarrierCount = 1;
		buffer_memory_barriers[0].pPipelineBarriers = &pipelineBarriers[0];
		buffer_memory_barriers[0].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[0].buffer = m_pTriWoopBuffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = m_pTriWoopBuffer->GetBufferCreateInfo()->size;

		buffer_memory_barriers[1].pipelineBarrierCount = 1;
		buffer_memory_barriers[1].pPipelineBarriers = &pipelineBarriers[1];
		buffer_memory_barriers[1].src_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].dst_queue_family_index = QUEUE_FAMILY_IGNORED;
		buffer_memory_barriers[1].buffer = m_pTriIndexBuffer;
		buffer_memory_barriers[1].offset = 0;
		buffer_memory_barriers[1].size = m_pTriIndexBuffer->GetBufferCreateInfo()->size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.flags = ::framework::gpu::pipeline_barrier_flags::none;
		dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;

		m_pCreateWoopifyTrianglesCommandBuffer->begin_command_buffer(&command_buffer_begin_info);
        m_pCreateWoopifyTrianglesCommandBuffer->dependency_info(0, 1, &dependency_info);
        m_pCreateWoopifyTrianglesCommandBuffer->bind_pipeline(m_pCreateWoopifyTrianglesPipeline);
		m_pCreateWoopifyTrianglesCommandBuffer->BindDescriptorTable(::framework::gpu::pipeline_bind_point::compute, m_pCreateWoopifyTrianglesDescriptorTable, 0, nullptr);
        m_pCreateWoopifyTrianglesCommandBuffer->push_constants(m_pCreateWoopifyTrianglesPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstant), &pushConstant);
        m_pCreateWoopifyTrianglesCommandBuffer->dispatch((m_numTriangles + 64 - 1) / 64, 1, 1);
		m_pCreateWoopifyTrianglesCommandBuffer->end_command_buffer();

		::framework::gpu::pipeline_stage_flags pWaitDstStageMask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		::framework::gpu::submit_info submit_info;
		submit_info.command_buffer_count = 1;
		submit_info.command_buffers = &m_pCreateWoopifyTrianglesCommandBuffer;
		submit_info.wait_semaphore_count = 1;
		submit_info.wait_semaphores = &m_pCreateNodesCompleteSemaphore;
		submit_info.wait_dst_stage_mask = &pWaitDstStageMask;
		submit_info.signal_semaphore_count = 1;
		submit_info.signal_semaphores = &m_pCreateWoopifyTrianglesCompleteSemaphore;
		command_queue->submit(1, &submit_info, nullptr);
	}//);
}