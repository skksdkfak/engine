#pragma once

#include <engine_core.h>
#include "lbvh.hpp"
#include "Tracer.h"
#include <cstdint>
#include <vector>

namespace framework
{
	class renderer_context
	{
	public:
		renderer_context();
		~renderer_context();

		void initialize();

		void update();

		void resize(::std::uint32_t width, ::std::uint32_t height);

		::std::uint32_t get_width();

		::std::uint32_t get_height();

		class ::framework::gpu::device * get_device();

	private:
		struct UboData
		{
			::glm::mat4 projectionMatrix;
			::glm::mat4 modelMatrix;
			::glm::mat4 viewMatrix;
		};

		struct thread_data
		{
			struct CommandBufferData
			{
				bool			is_command_buffer_open;
				::std::uint32_t		drawCallCount;
			};

			class ::framework::gpu::command_pool*					command_pool;
			::std::vector<class ::framework::gpu::command_buffer*>		commandBuffers;
			::std::vector<CommandBufferData>	commandBufferData;
		};

		void createSwapChain();

		void setupDepthStencil();

		void setupFrameBuffer();

		void createCommandBuffers();

		void destroyCommandBuffers();

		class ::framework::platform::window *							window;
		::framework::gpu::instance*							instance;
		::std::uint32_t							physical_device_count;
		::std::uint32_t							m_physicalDeviceIndex;
		::std::vector<::framework::gpu::physical_device*>		physical_devices;
		::std::vector<::framework::gpu::queue_family_properties>	queue_family_properties;
		::framework::gpu::physical_device_memory_properties		physical_device_memory_properties;
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		Tracer *							m_tracer;
		lbvh *								m_bvh;
		class ::framework::gpu::device*								device;
		class ::framework::gpu::surface*							surface;
		class ::framework::gpu::swap_chain*							swap_chain;
		::std::vector<class ::framework::gpu::image*>					swap_chain_images;
		::std::uint32_t							swap_chain_image;
		class Queue*								queue;
		class ::framework::gpu::command_pool *						command_pool;
		::std::vector<class ::framework::gpu::command_buffer*>			this->command_buffers;
		::std::vector<class ::framework::gpu::image_view*>				this->image_views;
		::std::vector<class ::framework::gpu::frame_buffer*>			frame_buffers;
		class ::framework::gpu::image *								this->depth_stencil_image;
		class ::framework::gpu::device_memory *						this->depth_stencil_device_memory;
		class ::framework::gpu::image_view *							this->depth_stencil_image_view;
		class ::framework::gpu::render_pass *						render_pass;
		const class ::framework::gpu::image *						image;
		class ::framework::gpu::fence *								fence[3];
		class ::framework::gpu::semaphore *							this->render_complete_semaphores[3];
		class ::framework::gpu::semaphore *							present_complete_semaphore;
		class ::framework::gpu::semaphore *							m_pPathTracingCompleteSemaphore;
		class ::framework::gpu::descriptor_pool *					descriptor_pool;
		const class ::framework::gpu::shader_module *				m_pFullScreenQuadVertexShaderModule;
		const class ::framework::gpu::shader_module *				m_pFullScreenQuadFragmentShaderModule;
		class ::framework::gpu::pipeline *							m_pFullScreenQuadGraphicsPipeline;
		class ::framework::gpu::render_pass *						m_pFullScreenQuadRenderPass;
		class ::framework::gpu::descriptor_set_layout *				m_pFullScreenQuadDescriptorSetLayout;
		class ::framework::gpu::pipeline_layout *					m_pFullScreenQuadPipelineLayout;
		class ::framework::gpu::descriptor_set *						m_pFullScreenQuadDescriptorSet;
		class DescriptorTable *					m_pFullScreenQuadDescriptorTable;
		::std::uint32_t							width, height;
		CEntity								m_mainCamera;
	};
}