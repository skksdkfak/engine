set openexr_install_dir=C:\Program Files (x86)\OpenEXR
set zlib_install_dir=D:\source\openexr\build\zlib-install

cmake -Bbuild/vc17win64 -H. -DOpenEXR_DIR="%openexr_install_dir%\lib\cmake\OpenEXR" -DZLIB_ROOT="%zlib_install_dir%" -DImath_DIR="%openexr_install_dir%\lib\cmake\Imath"