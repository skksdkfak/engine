#include "globals.hlsl"

Texture2D		g_SlopeDiffuseTexture		: register(CUSTOM0_TEX_BP);
Texture2D		g_SandDiffuseTexture		: register(CUSTOM1_TEX_BP);
Texture2D		g_RockDiffuseTexture		: register(CUSTOM2_TEX_BP);
Texture2D		g_GrassDiffuseTexture		: register(CUSTOM3_TEX_BP);
Texture2D		g_RockMicroBumpTexture		: register(CUSTOM4_TEX_BP);
Texture2D		g_SandMicroBumpTexture		: register(CUSTOM6_TEX_BP);

SamplerState	g_samLinear				: register(COLOR_SAM_BP);
SamplerState	g_samAnisotropicWrap	: register(CUSTOM0_SAM_BP);

struct PS_INPUT
{
	float4 vPosition				: SV_Position;
	centroid float2 texCoord		: TEXCOORD0;
	centroid float3 vNormal			: NORMAL;
	centroid float3 vPositionWS		: TEXCOORD1;
	centroid float4 layerdef		: TEXCOORD2;
	centroid float4 depthmap_scaler	: TEXCOORD3;
	uint RTIndex					: SV_RenderTargetArrayIndex;
};

struct PS_OUTPUT
{
	float4 vPositionWS : SV_TARGET0;
	float4 color : SV_TARGET1;
	float4 vNormal : SV_TARGET2;
};

PS_OUTPUT main(PS_INPUT input)
{
	PS_OUTPUT output;

	float4 color;
	//float3 pixel_to_light_vector = normalize(g_LightPosition - input.positionWS);
	//float3 pixel_to_eye_vector = normalize(g_CameraPosition - input.positionWS);
	float3 microbump_normal;

	float3x3 normal_rotation_matrix;

	// culling halfspace if needed
	//clip(g_HalfSpaceCullSign*(input.positionWS.y - g_HalfSpaceCullPosition));

	// fetching default microbump normal
	microbump_normal = normalize(2 * g_SandMicroBumpTexture.Sample(g_samAnisotropicWrap, input.texCoord).rbg - float3 (1.0, 1.0, 1.0));
	microbump_normal = normalize(lerp(microbump_normal, 2 * g_RockMicroBumpTexture.Sample(g_samAnisotropicWrap, input.texCoord).rbg - float3 (1.0, 1.0, 1.0), input.layerdef.w));

	//calculating base normal rotation matrix
	normal_rotation_matrix[1] = input.vNormal;
	normal_rotation_matrix[2] = normalize(cross(float3(-1.0, 0.0, 0.0), normal_rotation_matrix[1]));
	normal_rotation_matrix[0] = normalize(cross(normal_rotation_matrix[2], normal_rotation_matrix[1]));
	microbump_normal = mul(microbump_normal, normal_rotation_matrix);

	// getting diffuse color
	color = g_SlopeDiffuseTexture.Sample(g_samAnisotropicWrap, input.texCoord);
	color = lerp(color, g_SandDiffuseTexture.Sample(g_samAnisotropicWrap, input.texCoord), input.layerdef.g*input.layerdef.g);
	color = lerp(color, g_RockDiffuseTexture.Sample(g_samAnisotropicWrap, input.texCoord), input.layerdef.w*input.layerdef.w);
	color = lerp(color, g_GrassDiffuseTexture.Sample(g_samAnisotropicWrap, input.texCoord), input.layerdef.b);

	//// adding per-vertex lighting defined by displacement of vertex 
	//color *= 0.5 + 0.5*min(1.0, max(0.0, input.depthmap_scaler.b / 3.0f + 0.5f));

	//// calculating pixel position in light view space
	//float4 positionLS = mul(float4(input.positionWS, 1), g_LightModelViewProjectionMatrix);
	//positionLS.xyz /= positionLS.w;
	//positionLS.x = (positionLS.x + 1)*0.5;
	//positionLS.y = (1 - positionLS.y)*0.5;
	
	//// fetching shadowmap and shading
	//float dsf = 0.75f / 4096.0f;
	//float shadow_factor = 1;
	////float shadow_factor = 0.2*g_DepthTexture.SampleCmp(SamplerDepthAnisotropic, positionLS.xy, positionLS.z* 0.995f).r;
	////shadow_factor += 0.2*g_DepthTexture.SampleCmp(SamplerDepthAnisotropic, positionLS.xy + float2(dsf, dsf), positionLS.z* 0.995f).r;
	////shadow_factor += 0.2*g_DepthTexture.SampleCmp(SamplerDepthAnisotropic, positionLS.xy + float2(-dsf, dsf), positionLS.z* 0.995f).r;
	////shadow_factor += 0.2*g_DepthTexture.SampleCmp(SamplerDepthAnisotropic, positionLS.xy + float2(dsf, -dsf), positionLS.z* 0.995f).r;
	////shadow_factor += 0.2*g_DepthTexture.SampleCmp(SamplerDepthAnisotropic, positionLS.xy + float2(-dsf, -dsf), positionLS.z* 0.995f).r;
	//color.rgb *= max(0, dot(pixel_to_light_vector, microbump_normal))*shadow_factor + 0.2;
	
	//// adding light from the sky
	//color.rgb += (0.0 + 0.2*max(0, (dot(float3(0, 1, 0), microbump_normal))))*float3(0.2, 0.2, 0.3);

	//// making all a bit brighter, simultaneously pretending the wet surface is darker than normal;
	//color.rgb *= 0.5 + 0.8*max(0, min(1, input.positionWS.y*0.5 + 0.5));

	//// applying refraction caustics
	//color.rgb *= (1.0 + max(0, 0.4 + 0.6*dot(pixel_to_light_vector, microbump_normal))*input.depthmap_scaler.a*(0.4 + 0.6*shadow_factor));

	// applying fog
	//color.rgb = lerp(CalculateFogColor(pixel_to_light_vector, pixel_to_eye_vector).rgb, color.rgb, min(1, exp(-length(g_CameraPosition - input.positionWS)*g_FogDensity)));
	//color.a = length(g_CameraPosition - input.positionWS);

	output.vPositionWS = float4(input.vPositionWS, 1);
	output.color = float4(color.rgb, 0.03f);
	output.vNormal = float4(microbump_normal, 0.126f);

	return output;
}