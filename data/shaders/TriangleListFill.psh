#include "globals.hlsl"
#include "globalIllum.shi"
#include "Model.hlsl"
#include "PathTracing.hlsl"

Texture2D			g_colorMap		: register(COLOR_TEX_BP);
Texture2D			g_nmhTexture	: register(NORMAL_TEX_BP);
Texture2D			g_specularMap	: register(SPECULAR_TEX_BP);

SamplerState		g_samLinear		: register(COLOR_SAM_BP);

RWStructuredBuffer<uint4>			voxelPosList	: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint4>			voxelKdList		: register(CUSTOM1_UAV_BP);
RWTexture3D<uint>					nodeGrid		: register(CUSTOM2_UAV_BP);
RWStructuredBuffer<Triangle>		triangles		: register(CUSTOM3_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	ModelParams modelCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		uint voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

#ifndef USE_VERTEX_BUFFER
cbuffer StartVertex : register(b4)
{
	uint baseVertex;
};
#endif

struct GS_Output
{
	float4 vPosition	: SV_POSITION0;
	float3 vWorldPos	: POSITION0;
	float3 vNormal		: NORMAL0;
	float3 vBinormal	: BINORMAL0;
	float3 vTangent		: TANGENT0;
	float2 texCoord		: TEXCOORD0;
	float3 vWorldPosArray[3]: POSITION1;
	float3 vNormalArray[3]	: NORMAL1;
	float3 vBinormalArray[3]: BINORMAL1;
	float3 vTangentArray[3]	: TANGENT1;
	float2 texCoordArray[3]	: TEXCOORD1;
	uint primID : SV_PrimitiveID;
};

void main(GS_Output input)
{
	float4 diffuseColor = modelCustomUB.hasDiffuseTexture ?
		g_colorMap.Sample(g_samLinear, input.texCoord) :
		modelCustomUB.vDiffuseColor;

	float specularLevel = modelCustomUB.hasSpecularTexture ?
		g_specularMap.Sample(g_samLinear, input.texCoord).r :
		modelCustomUB.specularLevel;

	float3 normal;
	float4x4 transmat = float4x4(input.vTangent.x, input.vBinormal.x, input.vNormal.x, 1.0f,
		input.vTangent.y, input.vBinormal.y, input.vNormal.y, 1.0f,
		input.vTangent.z, input.vBinormal.z, input.vNormal.z, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f);
	if (modelCustomUB.hasNormalTexture)
	{
		float4 vNormalHeight = g_nmhTexture.Sample(g_samLinear, input.texCoord);
		float3 vNormalTS = normalize(vNormalHeight * 2.0f - 1.0f);
		normal = normalize(mul(transmat, vNormalTS));
	}
	else
	{
		normal = input.vNormal;
	}

	float3 offset = (input.vWorldPos.xyz - voxelCustomUB.voxelizePosition.xyz);
	// get position in the voxel-grid
	int3 voxelPos = offset / voxelCustomUB.voxelSize + (voxelCustomUB.voxelizeSize / voxelCustomUB.voxelSize) / 2;

	//if ((voxelPos.x > -1) && (voxelPos.x < voxelCustomUB.voxelizeDimension) &&
	//	(voxelPos.y > -1) && (voxelPos.y < voxelCustomUB.voxelizeDimension) &&
	//	(voxelPos.z > -1) && (voxelPos.z < voxelCustomUB.voxelizeDimension))
	{
		uint primitiveID = baseVertex + input.primID;
		voxelPosList[voxelPosList.IncrementCounter()] = uint4(voxelPos, primitiveID);
		voxelKdList[voxelKdList.IncrementCounter()] = uint4(diffuseColor.rgb * 255, 0);
		nodeGrid[voxelPos] = 0;
		[unroll]
		for (uint i = 0; i < 3; i++)
		{
			triangles[primitiveID].position[i] = input.vWorldPosArray[i];
			triangles[primitiveID].normal[i] = input.vNormalArray[i];
			triangles[primitiveID].binormal[i] = input.vBinormalArray[i];
			triangles[primitiveID].tangent[i] = input.vTangentArray[i];
			triangles[primitiveID].texCoord[i] = input.texCoordArray[i];
		}
		triangles[primitiveID].tbn = transmat;
		triangles[primitiveID].materialID = modelCustomUB.materialID;
		//nodeGrid[voxelPos] = input.primID;
	}
}