#include "globals.hlsl"

GLOBAL_CAMERA_UB(cameraUB);

cbuffer cbPerCubeRender : register(CUSTOM1_UB_BP)
{
	matrix g_mViewProjPSM[6];
	vector g_vLightPos;
};

struct GS_INPUT
{
	float4 vPosition				: SV_Position;
	centroid float2 texCoord		: TEXCOORD0;
	centroid float3 vNormal			: NORMAL;
	centroid float3 vPositionWS		: TEXCOORD1;
	centroid float4 layerdef		: TEXCOORD2;
	centroid float4 depthmap_scaler	: TEXCOORD3;
	uint RTIndex					: RTARRAYINDEX;
};

struct PS_INPUT
{
	float4 vPosition				: SV_Position;
	centroid float2 texCoord		: TEXCOORD0;
	centroid float3 vNormal			: NORMAL;
	centroid float3 vPositionWS		: TEXCOORD1;
	centroid float4 layerdef		: TEXCOORD2;
	centroid float4 depthmap_scaler	: TEXCOORD3;
	uint RTIndex					: SV_RenderTargetArrayIndex;
};

[maxvertexcount(3)]
void main(triangle GS_INPUT input[3], inout TriangleStream<PS_INPUT> CubeMapStream)
{
	PS_INPUT output;
	for (int v = 0; v < 3; v++)
	{
		output.vPosition = mul(float4(input[v].vPositionWS, 1.0f), g_mViewProjPSM[input[v].RTIndex]);
		output.vPositionWS = input[v].vPositionWS;
		output.vNormal = input[v].vNormal;
		output.layerdef = input[v].layerdef;
		output.depthmap_scaler = input[v].depthmap_scaler;
		output.texCoord = input[v].texCoord;
		output.RTIndex = input[v].RTIndex;
		CubeMapStream.Append(output);
	}
}