#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint> octreeIdx : register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint> octreeKd : register(CUSTOM1_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

[numthreads(8, 8, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.y * 1024 + dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numThread)
		return;

	octreeIdx[octreeCustomUB.allocOffset + thxId] = 0;
	octreeKd[octreeCustomUB.allocOffset + thxId] = 0;
	
}