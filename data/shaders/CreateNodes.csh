struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

StructuredBuffer<CompactTreeNode>	radixTreeNodes			: register(t0);
RWBuffer<float4>					nodes					: register(u0);

cbuffer CustomUB : register(b0)
{
	uint numTriangles;
};

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint threadIndex = dispatchThreadID.x;
	const uint offset = numTriangles - 1;
	if (threadIndex >= offset)
		return;

	int2 cidx = { radixTreeNodes[threadIndex].field0.y, radixTreeNodes[threadIndex].field0.z };
	float3 leftMin = radixTreeNodes[cidx[0]].field1.xyz;
	float3 leftMax = radixTreeNodes[cidx[0]].field2.xyz;
	float3 rightMin = radixTreeNodes[cidx[1]].field1.xyz;
	float3 rightMax = radixTreeNodes[cidx[1]].field2.xyz;

	const int nodeIndex = threadIndex * 4;
	nodes[nodeIndex + 0] = float4(leftMin.x, leftMax.x, leftMin.y, leftMax.y);
	nodes[nodeIndex + 1] = float4(rightMin.x, rightMax.x, rightMin.y, rightMax.y);
	nodes[nodeIndex + 2] = float4(leftMin.z, leftMax.z, rightMin.z, rightMax.z);
	nodes[nodeIndex + 3] = float4(cidx[0] < offset ? asfloat(cidx[0] * 4) : asfloat(~((cidx[0] - offset) * 4)), cidx[1] < offset ? asfloat(cidx[1] * 4) : asfloat(~((cidx[1] - offset) * 4)), 0, 0);
}