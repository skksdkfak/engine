#include "globals.hlsl"
#include "PathTracing.hlsl"

//Texture2D							colorMap			: register(COLOR_TEX_BP);
//Texture2D							normalMap			: register(NORMAL_TEX_BP);
//Texture2D							specularMap			: register(SPECULAR_TEX_BP);
//TextureCube							skyMap				: register(CUSTOM0_TEX_BP);
//
//SamplerState						sTrilinear			: register(COLOR_SAM_BP);

Buffer<int>					hitIndex			: register(t0);
Buffer<float>				hitDistance			: register(t1);
Buffer<float2>				hitBarycentricUV	: register(t2);
StructuredBuffer<Vertex>	vertices			: register(t3);
StructuredBuffer<Triangle>	triangles			: register(t4);
RWBuffer<float4>			rayOrigin			: register(u0);
RWBuffer<float4>			rayDirection		: register(u1);
RWBuffer<float3>			colorMask			: register(u2);
RWBuffer<float3>			accumulatedColor	: register(u3);


GLOBAL_VARIOUS_UB(variousUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint samples;
		uint bounceCount;
		uint materialID;
	}customUP;
};

float random(float2 p)
{
	return frac(cos(dot(p, float2(23.14069263277926, 2.665144142690225)))*123456.);
}

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint screenWidth = variousUB.vScreenResolution.x;
	const uint screenHeight = variousUB.vScreenResolution.y;
	if (dispatchThreadID.x >= screenWidth * screenHeight)
		return;

	//if (triangles[hit.primitiveID].materialID != customUP.materialID)
	//	return;

	const int primitiveID = hitIndex.Load(dispatchThreadID.x);
	if (primitiveID < 0)
		return;

	const uint x = dispatchThreadID.x % screenWidth;
	const uint y = dispatchThreadID.x / screenWidth;

	//float2 texCoord = vertices[hit.primitiveID * 3].texCoord * (1.0f - hit.barycentricUV.x - hit.barycentricUV.y);
	//texCoord += vertices[hit.primitiveID * 3 + 1].texCoord * hit.barycentricUV.x;
	//texCoord += vertices[hit.primitiveID * 3 + 2].texCoord * hit.barycentricUV.y;
	//texCoord.y = 1.0f - texCoord.y;

	float3 emit = { 0.0f, 0.0f, 0.0f };
	float4 diffuse = { 0.9f, 0.3f, 0.0f, 1.0f };
	//float4 diffuse = colorMap.SampleLevel(sTrilinear, texCoord, 0);
	//if (diffuse.a > 0.999f)
	{
		float seed = random(uint2(x, y) + variousUB.vCosTime.w);

		float3 unitVector;
		float e = 1.0f;
		float f = 2 * PI * random(uint2(x, y) + variousUB.vCosTime.w);
		float cosTheta = pow(1.0f - random(uint2(x, y) + variousUB.vCosTime.w + 1.0f), 1.0f / (e + 1.0f));
		float sinTheta = sqrt(1.0f - cosTheta*cosTheta);
		unitVector.x = sinTheta * cos(f);
		unitVector.y = sinTheta * sin(f);
		unitVector.z = cosTheta;

		float4 origin = rayOrigin.Load(dispatchThreadID.x);
		float4 direction = rayDirection.Load(dispatchThreadID.x);
		origin.xyz += direction.xyz * hitDistance.Load(dispatchThreadID.x);
		direction.xyz = normalize(mul(triangles[primitiveID].tbn, unitVector));
		origin.xyz += direction.xyz * 0.001f;

		rayOrigin[dispatchThreadID.x] = origin;
		rayDirection[dispatchThreadID.x] = direction;

		accumulatedColor[dispatchThreadID.x] += colorMask.Load(dispatchThreadID.x) * emit;
		colorMask[dispatchThreadID.x] *= diffuse.rgb;
	}
}