#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

struct CompactTriangle
{
	vec4 field0; // vert0.xyz, t0.x
	vec4 field1; // vert1.xyz, t1.y
	vec4 field2; // vert2.xyz, materialID
	vec4 field3; // t1.xy, t2.xy
	mat3x3 tbn;
};

layout (std430, binding = 1) buffer Triangles
{
    CompactTriangle triangles[];
};

layout (std430, binding = 2) restrict buffer Counter
{
    uint trianglesCounter;
};

layout (location = 0) in vec3 inWorldPos[];
layout (location = 1) in vec3 inNormal[];
layout (location = 2) in vec3 inBinormal[];
layout (location = 3) in vec3 inTangent[];
layout (location = 4) in vec2 inUV[];

void main(void)
{
	const uint primitiveID = atomicAdd(trianglesCounter, 1);
	
	vec3 normal = vec3(0);
	vec3 binormal = vec3(0);
	vec3 tangent = vec3(0);
	#pragma unroll 3
	for (uint i = 0; i < 3; i++)
	{
		normal += inNormal[i];
		binormal += inBinormal[i];
		tangent += inTangent[i];
	}

	CompactTriangle tri;
	tri.field0 = vec4(inWorldPos[0], inUV[0].x);
	tri.field1 = vec4(inWorldPos[1], inUV[0].y);
	tri.field2 = vec4(inWorldPos[2], uintBitsToFloat(0));
	tri.field3 = vec4(inUV[1], inUV[2]);
	tri.tbn = mat3x3(
		tangent.x, binormal.x, normal.x,
		tangent.y, binormal.y, normal.y,
		tangent.z, binormal.z, normal.z);

	triangles[primitiveID] = tri;
}