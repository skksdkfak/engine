#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint>	octreeIdx		: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint>	octreeKd		: register(CUSTOM1_UAV_BP);
RWStructuredBuffer<uint4>	voxelPosList	: register(CUSTOM2_UAV_BP);
RWStructuredBuffer<uint4>	voxelKdList		: register(CUSTOM3_UAV_BP);
RWTexture3D<uint>			nodeGrid		: register(CUSTOM4_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		uint voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

[numthreads(8, 8, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.y * 1024 + dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numVoxelFrag)
		return;

	uint4 posAndTriangle = voxelPosList[thxId];
	uint4 colorAndNode = voxelKdList[thxId];

	uint prev;
	InterlockedExchange(nodeGrid[posAndTriangle.xyz], thxId, prev);

	voxelKdList[thxId] = uint4(colorAndNode.rgb, prev);
	
	//uint4 loc = voxelPosList[thxId];
	//int childIdx = 0;
	//uint node;
	//bool bFlag = true;

	//uint voxelDim = voxelCustomUB.voxelizeDimension;
	//uint3 umin = uint3(0, 0, 0);

	//node = octreeIdx[childIdx];

	//for (int i = 0; i < octreeCustomUB.level; ++i)
	//{
	//	voxelDim /= 2;
	//	if ((node & 0x80000000) == 0)
	//	{
	//		bFlag = false;
	//		break;
	//	}
	//	childIdx = int(node & 0x7FFFFFFF);  //mask out flag bit to get child idx

	//	if ((loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
	//		(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
	//		(loc.z >= umin.z && loc.z < umin.z + voxelDim)
	//		)
	//	{

	//	}
	//	else if (
	//		(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
	//		(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
	//		(loc.z >= umin.z && loc.z < umin.z + voxelDim)
	//		)
	//	{
	//		childIdx += 1;
	//		umin.x = umin.x + voxelDim;
	//	}
	//	else if (
	//		(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
	//		(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
	//		(loc.z >= umin.z + voxelDim && loc.z < umin.z + 2 * voxelDim)
	//		)
	//	{
	//		childIdx += 2;
	//		umin.z += voxelDim;
	//	}
	//	else if (
	//		(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
	//		(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
	//		(loc.z >= umin.z + voxelDim && loc.z < umin.z + 2 * voxelDim)
	//		)
	//	{
	//		childIdx += 3;
	//		umin.x += voxelDim;
	//		umin.z += voxelDim;
	//	}
	//	else if (
	//		(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
	//		(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
	//		(loc.z >= umin.z && loc.z < umin.z + voxelDim)
	//		)
	//	{
	//		childIdx += 4;
	//		umin.y += voxelDim;

	//	}
	//	else if (
	//		(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
	//		(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
	//		(loc.z >= umin.z && loc.z < umin.z + voxelDim)
	//		)
	//	{
	//		childIdx += 5;
	//		umin.x += voxelDim;
	//		umin.y += voxelDim;
	//	}
	//	else if (
	//		(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
	//		(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
	//		(loc.z >= umin.z + voxelDim && loc.z < umin.z + voxelDim * 2)
	//		)
	//	{
	//		childIdx += 6;
	//		umin.z += voxelDim;
	//		umin.y += voxelDim;
	//	}
	//	else if (
	//		(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
	//		(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
	//		(loc.z >= umin.z + voxelDim && loc.z < umin.z + voxelDim * 2)
	//		)
	//	{
	//		childIdx += 7;
	//		umin += voxelDim;
	//	}
	//	else
	//	{
	//		bFlag = false;
	//		break;
	//	}
	//	node = octreeIdx[childIdx];
	//}

	//uint leafIdx = octreeKd[childIdx];
	//if (leafIdx == 0)
	//{
	//	octreeKd[childIdx] = thxId;
	//}
	//else
	//{
	//	uint4 curVal = voxelKdList[thxId];
	//	voxelKdList[thxId] = uint4(curVal.rgb, leafIdx);
	//	octreeKd[childIdx] = thxId;
	//}
}