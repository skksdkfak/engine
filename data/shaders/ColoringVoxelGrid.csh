#include "globals.hlsl"

//Texture3D<float4> g_VoxelGridColorInputTexture: register(COLOR_TEX_BP);

RWTexture3D<float4> g_VoxelGridShadedOutputTexture	: register(CUSTOM0_UAV_BP);
RWTexture3D<float4> g_VoxelGridColorInputTexture	: register(CUSTOM1_UAV_BP);

[numthreads(8, 8, 8)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	float4 voxelColor = g_VoxelGridColorInputTexture[dispatchThreadID.xyz];
	//if (voxelColor.w == 0) // It's so slow
	//	return;

	g_VoxelGridShadedOutputTexture[dispatchThreadID.xyz] += float4(0, 0, 0, voxelColor.a);
	g_VoxelGridShadedOutputTexture[dispatchThreadID.xyz] *= float4(voxelColor.rgb, 1.0f);
}