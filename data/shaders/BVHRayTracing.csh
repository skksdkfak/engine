#include "globals.hlsl"
#include "PathTracing.hlsl"

#define NV_SHADER_EXTN_SLOT u7
#include "nvHLSLExtns.h"

Buffer<float4>				rayOrigin			: register(t0);
Buffer<float4>				rayDirection		: register(t1);
StructuredBuffer<TreeNode>	radixTreeNodes		: register(t2);
StructuredBuffer<Vertex>	vertices			: register(t3);
//StructuredBuffer<float4>				nodes				: register(t2);
//StructuredBuffer<float4>				triWoops			: register(t3);
//Buffer<int>					triIndices			: register(t4);
//RWStructuredBuffer<Hit>	hits			: register(u0);
RWBuffer<int>				hitIndex			: register(u0);
RWBuffer<float>				hitDistance			: register(u1);
RWBuffer<float2>			hitBarycentricUV	: register(u2);

GLOBAL_VARIOUS_UB(variousUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint samples;
		uint bounces;
		uint materialID;
		uint primitiveCount;
	}customUP;
};

float copysignf(float x, float y)
{
	return x * y < 0 ? -1.0f : 1.0f;
}

float max4(float a, float b, float c, float d)
{
	return max(max(max(a, b), c), d);
}

float min4(float a, float b, float c, float d)
{
	return min(min(min(a, b), c), d);
}

void swap_int(inout int a, inout int b)
{
	int t = a;
	a = b;
	b = t;
}

bool IntersectTriangle(in Ray r, in float3 v1, in float3 v2, in float3 v3, inout float2 barycentricUV, inout float distance)
{
	const float3 e1 = v2 - v1;
	const float3 e2 = v3 - v1;
	const float3 d = r.origin - v1;
	const float3 s1 = cross(r.direction, e2);
	const float3 s2 = cross(d, e1);
	const float invd = 1.0f / (dot(s1, e1));
	const float t = dot(e2, s2) * invd;
	if (t > 0.f && t < distance)
	{
		const float b1 = dot(d, s1) * invd;
		if (b1 >= 0.0f)
		{
			const float b2 = dot(r.direction, s2) * invd;
			if (b2 >= 0.0f && b1 + b2 <= 1.0f)
			{
				barycentricUV = float2(b1, b2);
				distance = t;
				return true;
			}
		}
	}

	return false;
}

bool RayBoxIntersection(float3 ray_pos, in float3 inv_dir, in Bound box, inout float mint, inout float maxt)
{
	const float3 n = (box.min - ray_pos) * inv_dir;
	const float3 f = (box.max - ray_pos) * inv_dir;

	const float3 tmin = min(f, n);
	const float3 tmax = max(f, n);

	mint = max(tmin.x, max(tmin.y, tmin.z));
	maxt = min(tmax.x, min(tmax.y, tmax.z));

	return (mint <= maxt) && (maxt > 0.f);
}

#define INTERSECT_STACK_SIZE (16)
bool intersectBVH(in Ray r, inout Hit hit, in uint leafs)
{
	bool intersected = false;
	int stack[INTERSECT_STACK_SIZE];
	int ptr = 0;
	stack[ptr++] = -1;
	int idx = 0;
	const float3 invdir = 1.0f / r.direction;
	hit.distance = INFINITY;

	int triangleID;
	int leftNode;
	int rightNode;
	Bound lbox;
	Bound rbox;
	float leftMin;
	float rightMin;
	float leftMax;
	float rightMax;

	while (idx > -1)
	{
		TreeNode node = radixTreeNodes[idx];
		if (idx < leafs)
		{
			leftNode = node.left;
			rightNode = node.right;

			lbox = radixTreeNodes[leftNode].bound;
			rbox = radixTreeNodes[rightNode].bound;

			bool traverseLeftChild = RayBoxIntersection(r.origin, invdir, lbox, leftMin, leftMax);
			bool traverseRightChild = RayBoxIntersection(r.origin, invdir, rbox, rightMin, rightMax);
			traverseLeftChild = traverseLeftChild && (hit.distance >= leftMin);
			traverseRightChild = traverseRightChild && (hit.distance >= rightMin);

			if (traverseLeftChild && traverseRightChild)
			{
				int deferred;
				if (leftMin > rightMin)
				{
					idx = rightNode;
					deferred = leftNode;
				}
				else
				{
					idx = leftNode;
					deferred = rightNode;
				}
				stack[ptr++] = deferred;
				continue;
			}
			else if (traverseLeftChild)
			{
				idx = leftNode;
				continue;
			}
			else if (traverseRightChild)
			{
				idx = rightNode;
				continue;
			}
		}
		else
		{
			triangleID = node.triangleID;
			if (IntersectTriangle(r,
				vertices[triangleID * 3].position,
				vertices[triangleID * 3 + 1].position,
				vertices[triangleID * 3 + 2].position,
				hit.barycentricUV,
				hit.distance))
			{
				hit.primitiveID = triangleID;
				intersected = true;
			}
		}
		idx = stack[--ptr];
	}

	return intersected;
}

//#define EntrypointSentinel 0x76543210
//#define STACK_SIZE 16

//void trace(in float4 origin, 
//	in float4 direction, 
//	inout int hitIndex, 
//	inout float hitDistance, 
//	inout float2 hitBarycentricUV)
//{
//	const bool anyHit = false;
//
//	// Traversal stack in CUDA thread-local memory.
//	int traversalStack[STACK_SIZE];
//
//	// Live state during traversal, stored in registers.
//	int     rayidx;                 // Ray index.
//	float   origx, origy, origz;    // Ray origin.
//	float   dirx, diry, dirz;       // Ray direction.
//	float   tmin;                   // t-value from which the ray starts. Usually 0.
//	float   idirx, idiry, idirz;    // 1 / dir
//	float   oodx, oody, oodz;       // orig / dir
//
//	int		stackPtr;               // Current position in traversal stack.
//	int     leafAddr;               // First postponed leaf, non-negative if none.
//	int     nodeAddr;               // Non-negative: current internal node, negative: second postponed leaf.
//	int     hitIdx;			        // Triangle index of the closest intersection, -1 if none.
//	float   hitT;                   // t-value of the closest intersection.
//	float2	uv;
//
//	// Initialize.
//	{
//		// Fetch ray.
//		origx = origin.x, origy = origin.y, origz = origin.z;
//		dirx = direction.x, diry = direction.y, dirz = direction.z;
//		tmin = origin.w;
//
//		float ooeps = exp2(-80.0f); // Avoid div by zero.
//		idirx = 1.0f / (abs(dirx) > ooeps ? dirx : copysignf(ooeps, dirx));
//		idiry = 1.0f / (abs(diry) > ooeps ? diry : copysignf(ooeps, diry));
//		idirz = 1.0f / (abs(dirz) > ooeps ? dirz : copysignf(ooeps, dirz));
//		oodx = origx * idirx, oody = origy * idiry, oodz = origz * idirz;
//
//		// Setup traversal.
//		traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
//		stackPtr = 0;
//		leafAddr = 0;   // No postponed leaf.
//		nodeAddr = 0;   // Start from the root.
//		hitIdx = -1;  // No triangle intersected so far.
//		hitT = direction.w; // tmax
//	}
//
//	// Traversal loop.
//	while (nodeAddr != EntrypointSentinel)
//	{
//		// Traverse internal nodes until all SIMD lanes have found a leaf.
//		bool searchingLeaf = true;
//		while (nodeAddr >= 0 && nodeAddr != EntrypointSentinel)
//		{
//			// Fetch AABBs of the two child nodes.
//			float4 n0xy = nodes[nodeAddr]; // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
//			float4 n1xy = nodes[nodeAddr + 1]; // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
//			float4 nz = nodes[nodeAddr + 2]; // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)
//
//			// Intersect the ray against the child nodes.
//			float c0lox = n0xy.x * idirx - oodx;
//			float c0hix = n0xy.y * idirx - oodx;
//			float c0loy = n0xy.z * idiry - oody;
//			float c0hiy = n0xy.w * idiry - oody;
//			float c0loz = nz.x   * idirz - oodz;
//			float c0hiz = nz.y   * idirz - oodz;
//			float c1loz = nz.z   * idirz - oodz;
//			float c1hiz = nz.w   * idirz - oodz;
//			float c0min = max4(tmin, min(c0lox, c0hix), min(c0loy, c0hiy), min(c0loz, c0hiz));
//			float c0max = min4(hitT, max(c0lox, c0hix), max(c0loy, c0hiy), max(c0loz, c0hiz));
//			float c1lox = n1xy.x * idirx - oodx;
//			float c1hix = n1xy.y * idirx - oodx;
//			float c1loy = n1xy.z * idiry - oody;
//			float c1hiy = n1xy.w * idiry - oody;
//			float c1min = max4(tmin, min(c1lox, c1hix), min(c1loy, c1hiy), min(c1loz, c1hiz));
//			float c1max = min4(hitT, max(c1lox, c1hix), max(c1loy, c1hiy), max(c1loz, c1hiz));
//
//			bool traverseChild0 = (c0max >= c0min);
//			bool traverseChild1 = (c1max >= c1min);
//
//			// Neither child was intersected => pop stack.
//			if (!traverseChild0 && !traverseChild1)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//			// Otherwise => fetch child pointers.
//			else
//			{
//				int2 cnodes = asint(nodes[nodeAddr + 3].xy);
//				nodeAddr = (traverseChild0) ? cnodes.x : cnodes.y;
//
//				// Both children were intersected => push the farther one.
//				if (traverseChild0 && traverseChild1)
//				{
//					if (c1min < c0min)
//						swap_int(nodeAddr, cnodes.y);
//					stackPtr++;
//					traversalStack[stackPtr] = cnodes.y;
//				}
//			}
//
//			// First leaf => postpone and continue traversal.
//			if (nodeAddr < 0 && leafAddr >= 0)
//			{
//				searchingLeaf = false;
//				leafAddr = nodeAddr;
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//
//			// All SIMD lanes have found a leaf => process them.
//			if (!NvAny(searchingLeaf))
//				break;
//		}
//
//		// Process postponed leaf nodes.
//		while (leafAddr < 0)
//		{
//			// Intersect the ray against each triangle using Sven Woop's algorithm.
//			for (int triAddr = ~leafAddr;; triAddr += 3)
//			{
//				// Read first 16 bytes of the triangle.
//				// End marker (negative zero) => all triangles processed.
//				float4 v00 = triWoops[triAddr];
//				if (asint(v00.x) == 0x80000000)
//					break;
//
//				// Compute and check intersection t-value.
//				float Oz = v00.w - origx*v00.x - origy*v00.y - origz*v00.z;
//				float invDz = 1.0f / (dirx*v00.x + diry*v00.y + dirz*v00.z);
//				float t = Oz * invDz;
//
//				if (t > tmin && t < hitT)
//				{
//					// Compute and check barycentric u.
//					float4 v11 = triWoops[triAddr + 1];
//					float Ox = v11.w + origx*v11.x + origy*v11.y + origz*v11.z;
//					float Dx = dirx*v11.x + diry*v11.y + dirz*v11.z;
//					float u = Ox + t*Dx;
//
//					if (u >= 0.0f && u <= 1.0f)
//					{
//						// Compute and check barycentric v.
//						float4 v22 = triWoops[triAddr + 2];
//						float Oy = v22.w + origx*v22.x + origy*v22.y + origz*v22.z;
//						float Dy = dirx*v22.x + diry*v22.y + dirz*v22.z;
//						float v = Oy + t*Dy;
//
//						if (v >= 0.0f && u + v <= 1.0f)
//						{
//							// Record intersection.
//							// Closest intersection not required => terminate.
//							hitT = t;
//							hitIdx = triAddr;
//							uv.x = u;
//							uv.y = v;
//							if (anyHit)
//							{
//								nodeAddr = EntrypointSentinel;
//								break;
//							}
//						}
//					}
//				}
//			} // triangle
//
//			// Another leaf was postponed => process it as well.
//			leafAddr = nodeAddr;
//			if (nodeAddr < 0)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//		} // leaf
//	} // traversal
//
//	if (hitIdx != -1)
//	{
//		hitIdx = triIndices.Load(hitIdx);
//		hitBarycentricUV = uv;
//		hitDistance = hitT;
//	}
//	hitIndex = hitIdx;
//}
//
//float2 RayBox(const Ray ray, const Bound box)
//{
//	float3 t0 = (box.min - ray.origin) / ray.direction;
//	float3 t1 = (box.max - ray.origin) / ray.direction;
//
//	float3 tmin = min(t0, t1);
//	float3 tmax = max(t0, t1);
//
//	float tNear = max(max(tmin.x, tmin.y), tmin.z);
//	float tFar = min(min(tmax.x, tmax.y), tmax.z);
//
//	return float2(tNear, tFar);
//}
//
//bool hit_bbox(in Ray ray, in Bound bound)
//{
//	float3 r = 1.0f / ray.direction;
//	float3 t0 = (bound.min - ray.origin) * r;
//	float3 t1 = (bound.max - ray.origin) * r;
//
//	float4 n, f;
//	n.xyz = min(t0, t1);
//	n.w = max(max(n.x, n.y), n.z);
//	f.xyz = max(t0, t1);
//	f.w = min(min(f.x, f.y), f.z);
//	return (f.w >= 0.) && (f.w >= n.w);
//}
//
//float3 RayTriangle(const float3 v0, const float3 v1, const float3 v2, const Ray ray)
//{
//	//  const float EPSILON = 0.000001f; // breaks FairyForest
//	const float EPSILON = 0.000001f; // works better
//	const float3 miss = float3(FLT_MAX, FLT_MAX, FLT_MAX);
//
//	float3 edge1 = v1 - v0;
//	float3 edge2 = v2 - v0;
//	float3 pvec = cross(ray.direction, edge2);
//	float det = dot(edge1, pvec);
//
//	float3 tvec = ray.origin - v0;
//	float u = dot(tvec, pvec);
//
//	float3 qvec = cross(tvec, edge1);
//	float v = dot(ray.direction, qvec);
//
//	// TODO: clear this
//	if (det > EPSILON)
//	{
//		if (u < 0.0 || u > det)
//			return miss;
//		if (v < 0.0 || u + v > det)
//			return miss;
//	}
//	else if (det < -EPSILON)
//	{
//		if (u > 0.0 || u < det)
//			return miss;
//		if (v > 0.0 || u + v < det)
//			return miss;
//	}
//	else
//		return miss;
//
//	float inv_det = 1.f / det;
//	float t = dot(edge2, qvec) * inv_det;
//	u *= inv_det;
//	v *= inv_det;
//	if (t > 0.000000001f && t < 100000.0f)
//		return float3(u, v, t);
//
//	return miss;
//}
//
//void swap_float2(inout float2 a, inout float2 b)
//{
//	float2 t = a;
//	a = b;
//	b = t;
//}
//
//void swap_uint(inout uint a, inout uint b)
//{
//	uint t = a;
//	a = b;
//	b = t;
//}
//
//#define INTERSECT_STACK_SIZE (32)
//
//bool intersectBVH(in uint cur, in Ray r, inout float2 barycentric, inout float t, inout uint id)
//{
//	// Use static allocation because malloc() can't be called in parallel
//	// Use stack to traverse BVH to save space (cost is O(height))
//	uint stack[INTERSECT_STACK_SIZE];
//	int topIndex = INTERSECT_STACK_SIZE;
//	stack[--topIndex] = cur;
//	bool intersected = false;
//	t = INFINITY;
//
//	// Do while stack is not empty
//	while (topIndex != INTERSECT_STACK_SIZE)
//	{
//		TreeNode n = radixTreeNodes[stack[topIndex++]];
//		if (n.leaf)
//		{
//			float3 res = RayTriangle(vertices[n.triangleID * 3].position, vertices[n.triangleID * 3 + 1].position, vertices[n.triangleID * 3 + 2].position, r);
//			if (res.z < t && res.z > 0.000000001f && res.z < 100000.0f)
//			{
//				barycentric = res.xy;
//				t = res.z;
//				id = n.triangleID;
//				intersected = true;
//			}
//		}
//		else
//		{
//			//uint child0 = n.left;
//			//uint child1 = n.right;
//			//TreeNode left = radixTreeNodes[child0];
//			//TreeNode right = radixTreeNodes[child1];
//			//float2 tspan0 = RayBox(r, left.bound);
//			//float2 tspan1 = RayBox(r, right.bound);
//			//bool intersect0 = (tspan0.x <= tspan0.y) && (tspan0.y >= 0.000000001f) && (tspan0.x <= t);
//			//bool intersect1 = (tspan1.x <= tspan1.y) && (tspan1.y >= 0.000000001f) && (tspan1.x <= t);
//
//			//if (intersect0 && intersect1)
//			//	if (tspan0.x > tspan1.x)
//			//	{
//			//		swap_float2(tspan0, tspan1);
//			//		swap_uint(child0, child1);
//			//	}
//
//			//if (intersect0)
//			//	stack[--topIndex] = child0;
//			//if (intersect1)
//			//	stack[--topIndex] = child1;
//
//
//			if (hit_bbox(r, n.bound))
//			{
//				stack[--topIndex] = n.right;
//				stack[--topIndex] = n.left;
//
//				if (topIndex < 0)
//				{
//					// Intersect stack not big enough. Increase INTERSECT_STACK_SIZE!
//					return false;
//				}
//			}
//		}
//	}
//
//	return intersected;
//}
//bool intersectBVH(in Ray r, inout Hit hit)
//{
//	const bool anyHit = false;
//
//	// Traversal stack in CUDA thread-local memory.
//	int traversalStack[STACK_SIZE];
//
//	// Live state during traversal, stored in registers.
//	int     rayidx;                 // Ray index.
//	float   origx, origy, origz;    // Ray origin.
//	float   dirx, diry, dirz;       // Ray direction.
//	float   tmin;                   // t-value from which the ray starts. Usually 0.
//	float   idirx, idiry, idirz;    // 1 / dir
//	float   oodx, oody, oodz;       // orig / dir
//
//	int		stackPtr;               // Current position in traversal stack.
//	int     leafAddr;               // First postponed leaf, non-negative if none.
//	int     nodeAddr;               // Non-negative: current internal node, negative: second postponed leaf.
//	int     hitIndex;               // Triangle index of the closest intersection, -1 if none.
//	float   hitT;                   // t-value of the closest intersection.
//
//									// Initialize.
//	{
//		// Fetch ray.
//		origx = r.origin.x, origy = r.origin.y, origz = r.origin.z;
//		dirx = r.direction.x, diry = r.direction.y, dirz = r.direction.z;
//		tmin = 0.0f;
//
//		float ooeps = exp2(-80.0f); // Avoid div by zero.
//		idirx = 1.0f / (abs(r.direction.x) > ooeps ? r.direction.x : copysignf(ooeps, r.direction.x));
//		idiry = 1.0f / (abs(r.direction.y) > ooeps ? r.direction.y : copysignf(ooeps, r.direction.y));
//		idirz = 1.0f / (abs(r.direction.z) > ooeps ? r.direction.z : copysignf(ooeps, r.direction.z));
//		oodx = origx * idirx, oody = origy * idiry, oodz = origz * idirz;
//
//		// Setup traversal.
//		traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
//		stackPtr = 0;
//		leafAddr = 0;   // No postponed leaf.
//		nodeAddr = 0;   // Start from the root.
//		hitIndex = -1;  // No triangle intersected so far.
//		hitT = INFINITY; // tmax
//	}
//
//	// Traversal loop.
//	while (nodeAddr != EntrypointSentinel)
//	{
//		// Traverse internal nodes until all SIMD lanes have found a leaf.
//		bool searchingLeaf = true;
//		while (nodeAddr >= 0 && nodeAddr != EntrypointSentinel)
//		{
//			// Fetch AABBs of the two child nodes.
//			float4 n0xy = nodes[nodeAddr]; // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
//			float4 n1xy = nodes[nodeAddr + 1]; // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
//			float4 nz = nodes[nodeAddr + 2]; // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)
//
//											 // Intersect the ray against the child nodes.
//			float c0lox = n0xy.x * idirx - oodx;
//			float c0hix = n0xy.y * idirx - oodx;
//			float c0loy = n0xy.z * idiry - oody;
//			float c0hiy = n0xy.w * idiry - oody;
//			float c0loz = nz.x   * idirz - oodz;
//			float c0hiz = nz.y   * idirz - oodz;
//			float c1loz = nz.z   * idirz - oodz;
//			float c1hiz = nz.w   * idirz - oodz;
//			float c0min = max4(tmin, min(c0lox, c0hix), min(c0loy, c0hiy), min(c0loz, c0hiz));
//			float c0max = min4(hitT, max(c0lox, c0hix), max(c0loy, c0hiy), max(c0loz, c0hiz));
//			float c1lox = n1xy.x * idirx - oodx;
//			float c1hix = n1xy.y * idirx - oodx;
//			float c1loy = n1xy.z * idiry - oody;
//			float c1hiy = n1xy.w * idiry - oody;
//			float c1min = max4(tmin, min(c1lox, c1hix), min(c1loy, c1hiy), min(c1loz, c1hiz));
//			float c1max = min4(hitT, max(c1lox, c1hix), max(c1loy, c1hiy), max(c1loz, c1hiz));
//
//			bool traverseChild0 = (c0max >= c0min);
//			bool traverseChild1 = (c1max >= c1min);
//
//			// Neither child was intersected => pop stack.
//			if (!traverseChild0 && !traverseChild1)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//			// Otherwise => fetch child pointers.
//			else
//			{
//				int2 cnodes = asint(nodes[nodeAddr + 3].xy);
//				nodeAddr = (traverseChild0) ? cnodes.x : cnodes.y;
//
//				// Both children were intersected => push the farther one.
//				if (traverseChild0 && traverseChild1)
//				{
//					if (c1min < c0min)
//						swap_int(nodeAddr, cnodes.y);
//					stackPtr++;
//					traversalStack[stackPtr] = cnodes.y;
//				}
//			}
//
//			// First leaf => postpone and continue traversal.
//			if (nodeAddr < 0 && leafAddr >= 0)
//			{
//				searchingLeaf = false;
//				leafAddr = nodeAddr;
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//
//			// All SIMD lanes have found a leaf => process them.
//			if (!NvAny(searchingLeaf))
//				break;
//		}
//
//		// Process postponed leaf nodes.
//		while (leafAddr < 0)
//		{
//			// Intersect the ray against each triangle using Sven Woop's algorithm.
//			for (int triAddr = ~leafAddr;; triAddr += 3)
//			{
//				// Read first 16 bytes of the triangle.
//				// End marker (negative zero) => all triangles processed.
//				float4 v00 = triWoops[triAddr];
//				if (asint(v00.x) == 0x80000000)
//					break;
//
//				// Compute and check intersection t-value.
//				float Oz = v00.w - origx*v00.x - origy*v00.y - origz*v00.z;
//				float invDz = 1.0f / (dirx*v00.x + diry*v00.y + dirz*v00.z);
//				float t = Oz * invDz;
//
//				if (t > tmin && t < hitT)
//				{
//					// Compute and check barycentric u.
//					float4 v11 = triWoops[triAddr + 1];
//					float Ox = v11.w + origx*v11.x + origy*v11.y + origz*v11.z;
//					float Dx = dirx*v11.x + diry*v11.y + dirz*v11.z;
//					float u = Ox + t*Dx;
//
//					if (u >= 0.0f && u <= 1.0f)
//					{
//						// Compute and check barycentric v.
//						float4 v22 = triWoops[triAddr + 2];
//						float Oy = v22.w + origx*v22.x + origy*v22.y + origz*v22.z;
//						float Dy = dirx*v22.x + diry*v22.y + dirz*v22.z;
//						float v = Oy + t*Dy;
//
//						if (v >= 0.0f && u + v <= 1.0f)
//						{
//							// Record intersection.
//							// Closest intersection not required => terminate.
//							hitT = t;
//							hitIndex = triAddr;
//							hit.primitiveID = triIndices[hitIndex];
//							hit.distance = hitT;
//							hit.barycentricUV = float2(u, v);
//							if (anyHit)
//							{
//								nodeAddr = EntrypointSentinel;
//								break;
//							}
//						}
//					}
//				}
//			} // triangle
//
//			  // Another leaf was postponed => process it as well.
//			leafAddr = nodeAddr;
//			if (nodeAddr < 0)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//		} // leaf
//	} // traversal
//
//	return (hitIndex != -1);
//}

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint screenWidth = variousUB.vScreenResolution.x;
	const uint screenHeight = variousUB.vScreenResolution.y;
	if (dispatchThreadID.x >= screenWidth * screenHeight)
		return;

	if (hitIndex.Load(dispatchThreadID.x) >= 0)
	{
		Ray ray;
		ray.origin = rayOrigin.Load(dispatchThreadID.x).xyz;
		ray.direction = rayDirection.Load(dispatchThreadID.x).xyz;
		ray.tmin = 0;
		ray.tmax = INFINITY;

		float3 color;

		Hit hit;
		if (intersectBVH(ray, hit, customUP.primitiveCount))
		{
			hitDistance[dispatchThreadID.x] = hit.distance;
			hitBarycentricUV[dispatchThreadID.x] = hit.barycentricUV;
			hitIndex[dispatchThreadID.x] = hit.primitiveID;
		}
		else
		{
			hitIndex[dispatchThreadID.x] = -1;
		}
	}
	//trace(rayOrigin.Load(dispatchThreadID.x), rayDirection.Load(dispatchThreadID.x), 
	//	hitIndex[dispatchThreadID.x], hitDistance[dispatchThreadID.x], hitBarycentricUV[dispatchThreadID.x]);
}