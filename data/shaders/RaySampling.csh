#include "globals.hlsl"
#include "PathTracing.hlsl"

//#define NV_SHADER_EXTN_SLOT u7
//
//#include "nvHLSLExtns.h"

Buffer<float3>			colorMask			: register(t0);
Buffer<float3>			accumulatedColor	: register(t1);
RWBuffer<int>			hitIndex			: register(u0);
RWTexture2D<float4>		raySampleTexture	: register(u1);
//RWStructuredBuffer<TreeNode>	radixTreeNodes		: register(u1);
//RWStructuredBuffer<Vertex>		vertices			: register(u2);
//RWStructuredBuffer<Triangle>	triangles			: register(u3);

//GLOBAL_CAMERA_UB(cameraUB);
GLOBAL_VARIOUS_UB(variousUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint samples;
		uint bounceCount;
		uint materialID;
	}customUP;
};

//float random(float2 p)
//{
//	return frac(cos(dot(p, float2(23.14069263277926, 2.665144142690225)))*123456.);
//}
//
//float2 RayBox(const Ray ray, const Bound box)
//{
//	float3 t0 = (box.min - ray.origin) / ray.direction;
//	float3 t1 = (box.max - ray.origin) / ray.direction;
//
//	float3 tmin = min(t0, t1);
//	float3 tmax = max(t0, t1);
//
//	float tNear = max(max(tmin.x, tmin.y), tmin.z);
//	float tFar = min(min(tmax.x, tmax.y), tmax.z);
//
//	return float2(tNear, tFar);
//}
//
//bool hit_bbox(in Ray ray, in Bound bound)
//{
//	float3 r = 1.0f / ray.direction;
//	float3 t0 = (bound.min - ray.origin) * r;
//	float3 t1 = (bound.max - ray.origin) * r;
//
//	float4 n, f;
//	n.xyz = min(t0, t1);
//	n.w = max(max(n.x, n.y), n.z);
//	f.xyz = max(t0, t1);
//	f.w = min(min(f.x, f.y), f.z);
//	return (f.w >= 0.) && (f.w >= n.w);
//}
//
//float3 RayTriangle(const float3 v0, const float3 v1, const float3 v2, const Ray ray)
//{
//	//  const float EPSILON = 0.000001f; // breaks FairyForest
//	const float EPSILON = 0.000001f; // works better
//	const float3 miss = float3(FLT_MAX, FLT_MAX, FLT_MAX);
//
//	float3 edge1 = v1 - v0;
//	float3 edge2 = v2 - v0;
//	float3 pvec = cross(ray.direction, edge2);
//	float det = dot(edge1, pvec);
//
//	float3 tvec = ray.origin - v0;
//	float u = dot(tvec, pvec);
//
//	float3 qvec = cross(tvec, edge1);
//	float v = dot(ray.direction, qvec);
//
//	// TODO: clear this
//	if (det > EPSILON)
//	{
//		if (u < 0.0 || u > det)
//			return miss;
//		if (v < 0.0 || u + v > det)
//			return miss;
//	}
//	else if (det < -EPSILON)
//	{
//		if (u > 0.0 || u < det)
//			return miss;
//		if (v > 0.0 || u + v < det)
//			return miss;
//	}
//	else
//		return miss;
//
//	float inv_det = 1.f / det;
//	float t = dot(edge2, qvec) * inv_det;
//	u *= inv_det;
//	v *= inv_det;
//	if (t > 0.000000001f && t < 100000.0f)
//		return float3(u, v, t);
//
//	return miss;
//}
//
//float copysignf(float x, float y)
//{
//	return x * y < 0 ? -1.0f : 1.0f;
//}
//
//float max4(float a, float b, float c, float d)
//{
//	return max(max(max(a, b), c), d);
//}
//
//float min4(float a, float b, float c, float d)
//{
//	return min(min(min(a, b), c), d);
//}
//
//void swap_int(inout int a, inout int b)
//{
//	int t = a;
//	a = b;
//	b = t;
//}
//
//#define fermi_speculative_while_while
//
//#ifdef kepler_dynamic_fetch
//#define STACK_SIZE 64
//bool intersectBVH(in Ray r, inout float2 barycentric, inout float t, inout uint id)
//{
//	const int EntrypointSentinel = 0x76543210;
//	int traversalStack[STACK_SIZE];
//	traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
//
//	// Live state during traversal, stored in registers.
//	float   origx, origy, origz;            // Ray origin.
//	int		stackPtr;                       // Current position in traversal stack.
//	int     leafAddr;                       // First postponed leaf, non-negative if none.
//	int     leafAddr2;                      // Second postponed leaf, non-negative if none.
//	int     nodeAddr = EntrypointSentinel;  // Non-negative: current internal node, negative: second postponed leaf.
//	int     hitIndex;                       // Triangle index of the closest intersection, -1 if none.
//	float   hitT;                           // t-value of the closest intersection.
//	float   tmin;
//	int     rayidx;
//	float   oodx;
//	float   oody;
//	float   oodz;
//	float   dirx;
//	float   diry;
//	float   dirz;
//	float   idirx;
//	float   idiry;
//	float   idirz;
//
//	// Fetch ray.
//	float3 o = r.origin;
//	float3 d = r.direction;
//	origx = o.x;
//	origy = o.y;
//	origz = o.z;
//	tmin = 0.0001f;
//	dirx = d.x;
//	diry = d.y;
//	dirz = d.z;
//	hitT = 100000.0f;
//	float ooeps = exp2(-80.0f); // Avoid div by zero.
//	idirx = 1.0f / (abs(d.x) > ooeps ? d.x : copysignf(ooeps, d.x));
//	idiry = 1.0f / (abs(d.y) > ooeps ? d.y : copysignf(ooeps, d.y));
//	idirz = 1.0f / (abs(d.z) > ooeps ? d.z : copysignf(ooeps, d.z));
//	oodx = origx * idirx;
//	oody = origy * idiry;
//	oodz = origz * idirz;
//
//	// Setup traversal.
//	stackPtr = 0;
//	leafAddr = 0;   // No postponed leaf.
//	leafAddr2 = 0;   // No postponed leaf.
//	nodeAddr = 0;   // Start from the root.
//	hitIndex = -1;  // No triangle intersected so far.
//
//	// Traversal loop.
//	while (nodeAddr != EntrypointSentinel)
//	{
//		// Traverse internal nodes until all SIMD lanes have found a leaf.
//		while (nodeAddr >= 0 && nodeAddr != EntrypointSentinel)
//			//while (unsigned int(nodeAddr) < unsigned int(EntrypointSentinel))   // functionally equivalent, but faster
//		{
//			// Fetch AABBs of the two child nodes.
//			const float4 n0xy = tex1Dfetch(t_nodesA, nodeAddr + 0); // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
//			const float4 n1xy = tex1Dfetch(t_nodesA, nodeAddr + 1); // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
//			const float4 nz = tex1Dfetch(t_nodesA, nodeAddr + 2); // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)
//			float4 tmp = tex1Dfetch(t_nodesA, nodeAddr + 3); // child_index0, child_index1
//			int2  cnodes = *(int2*)&tmp;
//
//			// Intersect the ray against the child nodes.
//
//			const float c0lox = n0xy.x * idirx - oodx;
//			const float c0hix = n0xy.y * idirx - oodx;
//			const float c0loy = n0xy.z * idiry - oody;
//			const float c0hiy = n0xy.w * idiry - oody;
//			const float c0loz = nz.x   * idirz - oodz;
//			const float c0hiz = nz.y   * idirz - oodz;
//			const float c1loz = nz.z   * idirz - oodz;
//			const float c1hiz = nz.w   * idirz - oodz;
//			const float c0min = max4(tmin, fminf(c0lox, c0hix), fminf(c0loy, c0hiy), fminf(c0loz, c0hiz));
//			const float c0max = min4(hitT, fmaxf(c0lox, c0hix), fmaxf(c0loy, c0hiy), fmaxf(c0loz, c0hiz));
//			//const float c0min = spanBeginKepler(c0lox, c0hix, c0loy, c0hiy, c0loz, c0hiz, tmin);
//			//const float c0max = spanEndKepler  (c0lox, c0hix, c0loy, c0hiy, c0loz, c0hiz, hitT);
//			const float c1lox = n1xy.x * idirx - oodx;
//			const float c1hix = n1xy.y * idirx - oodx;
//			const float c1loy = n1xy.z * idiry - oody;
//			const float c1hiy = n1xy.w * idiry - oody;
//			//const float c1min = spanBeginKepler(c1lox, c1hix, c1loy, c1hiy, c1loz, c1hiz, tmin);
//			//const float c1max = spanEndKepler  (c1lox, c1hix, c1loy, c1hiy, c1loz, c1hiz, hitT);
//			const float c1min = max4(tmin, fminf(c1lox, c1hix), fminf(c1loy, c1hiy), fminf(c1loz, c1hiz));
//			const float c1max = min4(hitT, fmaxf(c1lox, c1hix), fmaxf(c1loy, c1hiy), fmaxf(c1loz, c1hiz));
//
//			bool swp = (c1min < c0min);
//
//			bool traverseChild0 = (c0max >= c0min);
//			bool traverseChild1 = (c1max >= c1min);
//
//			// Neither child was intersected => pop stack.
//
//			if (!traverseChild0 && !traverseChild1)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//
//			// Otherwise => fetch child pointers.
//
//			else
//			{
//				nodeAddr = (traverseChild0) ? cnodes.x : cnodes.y;
//
//				// Both children were intersected => push the farther one.
//
//				if (traverseChild0 && traverseChild1)
//				{
//					if (swp)
//						swap(nodeAddr, cnodes.y);
//					stackPtr++;
//					traversalStack[stackPtr] = cnodes.y;
//				}
//			}
//
//			// First leaf => postpone and continue traversal.
//
//			if (nodeAddr < 0 && leafAddr >= 0)     // Postpone max 1
//												   //              if (nodeAddr < 0 && leafAddr2 >= 0)     // Postpone max 2
//			{
//				//leafAddr2= leafAddr;          // postpone 2
//				leafAddr = nodeAddr;
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//
//			// All SIMD lanes have found a leaf? => process them.
//
//			// NOTE: inline PTX implementation of "if(!__any(leafAddr >= 0)) break;".
//			// tried everything with CUDA 4.2 but always got several redundant instructions.
//
//			unsigned int mask;
//			asm("{\n"
//			"   .reg .pred p;               \n"
//				"setp.ge.s32        p, %1, 0;   \n"
//				"vote.ballot.b32    %0,p;       \n"
//				"}"
//				: "=r"(mask)
//				: "r"(leafAddr));
//			if (!mask)
//				break;
//
//			//if(!__any(leafAddr >= 0))
//			//    break;
//		}
//
//		// Process postponed leaf nodes.
//
//		while (leafAddr < 0)
//		{
//			for (int triAddr = ~leafAddr;; triAddr += 3)
//			{
//				// Tris in TEX (good to fetch as a single batch)
//				const float4 v00 = tex1Dfetch(t_trisA, triAddr + 0);
//				const float4 v11 = tex1Dfetch(t_trisA, triAddr + 1);
//				const float4 v22 = tex1Dfetch(t_trisA, triAddr + 2);
//
//				// End marker (negative zero) => all triangles processed.
//				if (__float_as_int(v00.x) == 0x80000000)
//					break;
//
//				float Oz = v00.w - origx*v00.x - origy*v00.y - origz*v00.z;
//				float invDz = 1.0f / (dirx*v00.x + diry*v00.y + dirz*v00.z);
//				float t = Oz * invDz;
//
//				if (t > tmin && t < hitT)
//				{
//					// Compute and check barycentric u.
//
//					float Ox = v11.w + origx*v11.x + origy*v11.y + origz*v11.z;
//					float Dx = dirx*v11.x + diry*v11.y + dirz*v11.z;
//					float u = Ox + t*Dx;
//
//					if (u >= 0.0f)
//					{
//						// Compute and check barycentric v.
//
//						float Oy = v22.w + origx*v22.x + origy*v22.y + origz*v22.z;
//						float Dy = dirx*v22.x + diry*v22.y + dirz*v22.z;
//						float v = Oy + t*Dy;
//
//						if (v >= 0.0f && u + v <= 1.0f)
//						{
//							// Record intersection.
//							// Closest intersection not required => terminate.
//
//							hitT = t;
//							hitIndex = triAddr;
//							if (anyHit)
//							{
//								nodeAddr = EntrypointSentinel;
//								break;
//							}
//						}
//					}
//				}
//			} // triangle
//
//			  // Another leaf was postponed => process it as well.
//
//			  //              if(leafAddr2<0) { leafAddr = leafAddr2; leafAddr2=0; } else     // postpone2
//			{
//				leafAddr = nodeAddr;
//				if (nodeAddr < 0)
//				{
//					nodeAddr = traversalStack[stackPtr];
//					stackPtr--;
//				}
//			}
//		} // leaf
//
//		  // DYNAMIC FETCH
//
//		if (__popc(__ballot(true)) < DYNAMIC_FETCH_THRESHOLD)
//			break;
//
//	} // traversal
//
//}
//#endif
//
//#ifdef fermi_speculative_while_while
//
//RWStructuredBuffer<float4>		nodesA				: register(u4);
//RWStructuredBuffer<float4>		trisA				: register(u5);
//RWStructuredBuffer<int>			triIndices			: register(u6);
//
//#define EntrypointSentinel 0x76543210
//#define STACK_SIZE 16
//bool intersectBVH(in Ray r, inout Hit hit)
//{
//	const bool anyHit = false;
//
//	// Traversal stack in CUDA thread-local memory.
//	int traversalStack[STACK_SIZE];
//
//	// Live state during traversal, stored in registers.
//	int     rayidx;                 // Ray index.
//	float   origx, origy, origz;    // Ray origin.
//	float   dirx, diry, dirz;       // Ray direction.
//	float   tmin;                   // t-value from which the ray starts. Usually 0.
//	float   idirx, idiry, idirz;    // 1 / dir
//	float   oodx, oody, oodz;       // orig / dir
//
//	int		stackPtr;               // Current position in traversal stack.
//	int     leafAddr;               // First postponed leaf, non-negative if none.
//	int     nodeAddr;               // Non-negative: current internal node, negative: second postponed leaf.
//	int     hitIndex;               // Triangle index of the closest intersection, -1 if none.
//	float   hitT;                   // t-value of the closest intersection.
//
//	// Initialize.
//	{
//		// Fetch ray.
//		origx = r.origin.x, origy = r.origin.y, origz = r.origin.z;
//		dirx = r.direction.x, diry = r.direction.y, dirz = r.direction.z;
//		tmin = 0.0f;
//
//		float ooeps = exp2(-80.0f); // Avoid div by zero.
//		idirx = 1.0f / (abs(r.direction.x) > ooeps ? r.direction.x : copysignf(ooeps, r.direction.x));
//		idiry = 1.0f / (abs(r.direction.y) > ooeps ? r.direction.y : copysignf(ooeps, r.direction.y));
//		idirz = 1.0f / (abs(r.direction.z) > ooeps ? r.direction.z : copysignf(ooeps, r.direction.z));
//		oodx = origx * idirx, oody = origy * idiry, oodz = origz * idirz;
//
//		// Setup traversal.
//		traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
//		stackPtr = 0;
//		leafAddr = 0;   // No postponed leaf.
//		nodeAddr = 0;   // Start from the root.
//		hitIndex = -1;  // No triangle intersected so far.
//		hitT = INFINITY; // tmax
//	}
//
//	// Traversal loop.
//	while (nodeAddr != EntrypointSentinel)
//	{
//		// Traverse internal nodes until all SIMD lanes have found a leaf.
//		bool searchingLeaf = true;
//		while (nodeAddr >= 0 && nodeAddr != EntrypointSentinel)
//		{
//			// Fetch AABBs of the two child nodes.
//			float4 n0xy	= nodesA[nodeAddr]; // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
//			float4 n1xy	= nodesA[nodeAddr + 1]; // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
//			float4 nz	= nodesA[nodeAddr + 2]; // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)
//
//			// Intersect the ray against the child nodes.
//			float c0lox = n0xy.x * idirx - oodx;
//			float c0hix = n0xy.y * idirx - oodx;
//			float c0loy = n0xy.z * idiry - oody;
//			float c0hiy = n0xy.w * idiry - oody;
//			float c0loz = nz.x   * idirz - oodz;
//			float c0hiz = nz.y   * idirz - oodz;
//			float c1loz = nz.z   * idirz - oodz;
//			float c1hiz = nz.w   * idirz - oodz;
//			float c0min = max4(tmin, min(c0lox, c0hix), min(c0loy, c0hiy), min(c0loz, c0hiz));
//			float c0max = min4(hitT, max(c0lox, c0hix), max(c0loy, c0hiy), max(c0loz, c0hiz));
//			float c1lox = n1xy.x * idirx - oodx;
//			float c1hix = n1xy.y * idirx - oodx;
//			float c1loy = n1xy.z * idiry - oody;
//			float c1hiy = n1xy.w * idiry - oody;
//			float c1min = max4(tmin, min(c1lox, c1hix), min(c1loy, c1hiy), min(c1loz, c1hiz));
//			float c1max = min4(hitT, max(c1lox, c1hix), max(c1loy, c1hiy), max(c1loz, c1hiz));
//
//			bool traverseChild0 = (c0max >= c0min);
//			bool traverseChild1 = (c1max >= c1min);
//
//			// Neither child was intersected => pop stack.
//			if (!traverseChild0 && !traverseChild1)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//			// Otherwise => fetch child pointers.
//			else
//			{
//				int2 cnodes = asint(nodesA[nodeAddr + 3].xy);
//				nodeAddr = (traverseChild0) ? cnodes.x : cnodes.y;
//
//				// Both children were intersected => push the farther one.
//				if (traverseChild0 && traverseChild1)
//				{
//					if (c1min < c0min)
//						swap_int(nodeAddr, cnodes.y);
//					stackPtr++;
//					traversalStack[stackPtr] = cnodes.y;
//				}
//			}
//
//			// First leaf => postpone and continue traversal.
//			if (nodeAddr < 0 && leafAddr >= 0)
//			{
//				searchingLeaf = false;
//				leafAddr = nodeAddr;
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//
//			// All SIMD lanes have found a leaf => process them.
//			if (!NvAny(searchingLeaf))
//				break;
//		}
//
//		// Process postponed leaf nodes.
//		while (leafAddr < 0)
//		{
//			// Intersect the ray against each triangle using Sven Woop's algorithm.
//			for (int triAddr = ~leafAddr;; triAddr += 3)
//			{
//				// Read first 16 bytes of the triangle.
//				// End marker (negative zero) => all triangles processed.
//				float4 v00 = trisA[triAddr];
//				if (asint(v00.x) == 0x80000000)
//					break;
//
//				// Compute and check intersection t-value.
//				float Oz = v00.w - origx*v00.x - origy*v00.y - origz*v00.z;
//				float invDz = 1.0f / (dirx*v00.x + diry*v00.y + dirz*v00.z);
//				float t = Oz * invDz;
//
//				if (t > tmin && t < hitT)
//				{
//					// Compute and check barycentric u.
//					float4 v11 = trisA[triAddr + 1];
//					float Ox = v11.w + origx*v11.x + origy*v11.y + origz*v11.z;
//					float Dx = dirx*v11.x + diry*v11.y + dirz*v11.z;
//					float u = Ox + t*Dx;
//
//					if (u >= 0.0f && u <= 1.0f)
//					{
//						// Compute and check barycentric v.
//						float4 v22 = trisA[triAddr + 2];
//						float Oy = v22.w + origx*v22.x + origy*v22.y + origz*v22.z;
//						float Dy = dirx*v22.x + diry*v22.y + dirz*v22.z;
//						float v = Oy + t*Dy;
//
//						if (v >= 0.0f && u + v <= 1.0f)
//						{
//							// Record intersection.
//							// Closest intersection not required => terminate.
//							hitT = t;
//							hitIndex = triAddr;
//							hit.primitiveID = triIndices[hitIndex];
//							hit.distance = hitT;
//							hit.barycentricUV = float2(u, v);
//							if (anyHit)
//							{
//								nodeAddr = EntrypointSentinel;
//								break;
//							}
//						}
//					}
//				}
//			} // triangle
//
//			// Another leaf was postponed => process it as well.
//			leafAddr = nodeAddr;
//			if (nodeAddr < 0)
//			{
//				nodeAddr = traversalStack[stackPtr];
//				stackPtr--;
//			}
//		} // leaf
//	} // traversal
//
//	return (hitIndex != -1);
//}
//#else
////#define INTERSECT_STACK_SIZE (32)
////bool intersectBVH(in Ray r, inout float2 barycentric, inout float t, inout uint id)
////{
////	// Use static allocation because malloc() can't be called in parallel
////	// Use stack to traverse BVH to save space (cost is O(height))
////	int stack[INTERSECT_STACK_SIZE];
////	int topIndex = INTERSECT_STACK_SIZE;
////	stack[--topIndex] = 0;
////	bool intersected = false;
////	t = INFINITY;
////
////	// Do while stack is not empty
////	while (topIndex != INTERSECT_STACK_SIZE)
////	{
////		TreeNode n = radixTreeNodes[stack[topIndex++]];
////		if (n.leaf)
////		{
////			float3 res = RayTriangle(vertices[n.triangleID * 3].position, vertices[n.triangleID * 3 + 1].position, vertices[n.triangleID * 3 + 2].position, r);
////			if (res.z < t/* && res.z > 0.00001f && res.z < 100000.0f*/)
////			{
////				barycentric = res.xy;
////				t = res.z;
////				id = n.triangleID;
////				intersected = true;
////			}
////		}
////		else
////		{
////			int child0 = n.left;
////			int child1 = n.right;
////			TreeNode left = radixTreeNodes[child0];
////			TreeNode right = radixTreeNodes[child1];
////			float2 tspan0 = RayBox(r, left.bound);
////			float2 tspan1 = RayBox(r, right.bound);
////			bool intersect0 = (tspan0.x <= tspan0.y) && (tspan0.y >= 0.000000001f) && (tspan0.x <= t);
////			bool intersect1 = (tspan1.x <= tspan1.y) && (tspan1.y >= 0.000000001f) && (tspan1.x <= t);
////
////			if (intersect0 && intersect1)
////			{
////				if (tspan0.x > tspan1.x)
////				{
////					swap_float2(tspan0, tspan1);
////					swap_uint(child0, child1);
////				}
////			}
////			else if (intersect0)
////				stack[--topIndex] = child0;
////			else if (intersect1)
////				stack[--topIndex] = child1;
////
////			//float2 tspan0 = RayBox(r, n.bound);
////			//bool intersect0 = (tspan0.x <= tspan0.y) && (tspan0.y >= 0.00001f) && (tspan0.x <= 100000.0f);
////
////			////if (hit_bbox(r, n.bound))
////			//if (intersect0)
////			//{
////			//	stack[--topIndex] = n.right;
////			//	stack[--topIndex] = n.left;
////
////			//	if (topIndex < 0)
////			//	{
////			//		// Intersect stack not big enough. Increase INTERSECT_STACK_SIZE!
////			//		return false;
////			//	}
////			//}
////		}
////	}
////
////	return intersected;
////}
//
//float IntersectBoxF(in Ray r, in float3 invdir, in Bound box, in float maxt)
//{
//	const float3 f = (box.max - r.origin) * invdir;
//	const float3 n = (box.min - r.origin) * invdir;
//
//	const float3 tmax = max(f, n);
//	const float3 tmin = min(f, n);
//
//	const float t1 = min(min(tmax.x, min(tmax.y, tmax.z)), maxt);
//	const float t0 = max(max(tmin.x, max(tmin.y, tmin.z)), 0.f);
//
//	return (t1 >= t0) ? (t0 > 0.f ? t0 : t1) : -1.f;
//}
//
////bool IntersectTriangle(in Ray r, in float3 v1, in float3 v2, in float3 v3, inout float2 barycentricUV, inout float distance)
////{
////	const float3 e1 = v2 - v1;
////	const float3 e2 = v3 - v1;
////	const float3 s1 = cross(r.direction, e2);
////	const float  invd = 1.0f / (dot(s1, e1));
////	const float3 d = r.origin - v1;
////	const float  b1 = dot(d, s1) * invd;
////	const float3 s2 = cross(d, e1);
////	const float  b2 = dot(r.direction, s2) * invd;
////	const float temp = dot(e2, s2) * invd;
////
////	if (b1 < 0.f || b1 > 1.f || b2 < 0.f || b1 + b2 > 1.f || temp < 0.f || temp > distance)
////	{
////		return false;
////	}
////	else
////	{
////		barycentricUV = float2(b1, b2);
////		distance = temp;
////		return true;
////	}
////}
//
//bool IntersectTriangle(in Ray r, in float3 v1, in float3 v2, in float3 v3, inout float2 barycentricUV, inout float distance)
//{
//	const float3 e1 = v2 - v1;
//	const float3 e2 = v3 - v1;
//	const float3 d = r.origin - v1;
//	const float3 s1 = cross(r.direction, e2);
//	const float3 s2 = cross(d, e1);
//	const float invd = 1.0f / (dot(s1, e1));
//	const float t = dot(e2, s2) * invd;
//	if (t > 0.f && t < distance)
//	{
//		const float b1 = dot(d, s1) * invd;
//		if (b1 >= 0.0f)
//		{
//			const float b2 = dot(r.direction, s2) * invd;
//			if (b2 >= 0.0f && b1 + b2 <= 1.0f)
//			{
//				barycentricUV = float2(b1, b2);
//				distance = t;
//				return true;
//			}
//		}
//	}
//
//	return false;
//}
//
//bool RayBoxIntersection(float3 ray_pos, in float3 inv_dir, in Bound box, inout float mint, inout float maxt)
//{
//	const float3 n = (box.min - ray_pos) * inv_dir;
//	const float3 f = (box.max - ray_pos) * inv_dir;
//
//	const float3 tmin = min(f, n);
//	const float3 tmax = max(f, n);
//
//	mint = max(tmin.x, max(tmin.y, tmin.z));
//	maxt = min(tmax.x, min(tmax.y, tmax.z));
//
//	return (mint <= maxt) && (maxt > 0.f);
//}
//
//bool RayBoxIntersection1(float3 ray_pos, float3 inv_dir, in Bound box, inout float tmin, inout float tmax)
//{
//	float lo = inv_dir.x * (box.min.x - ray_pos.x);
//	float hi = inv_dir.x * (box.max.x - ray_pos.x);
//	tmin = min(lo, hi);
//	tmax = max(lo, hi);
//
//	lo = inv_dir.y * (box.min.y - ray_pos.y);
//	hi = inv_dir.y * (box.max.y - ray_pos.y);
//	tmin = max(tmin, min(lo, hi));
//	tmax = min(tmax, max(lo, hi));
//
//	lo = inv_dir.z * (box.min.z - ray_pos.z);
//	hi = inv_dir.z * (box.max.z - ray_pos.z);
//	tmin = max(tmin, min(lo, hi));
//	tmax = min(tmax, max(lo, hi));
//
//	return (tmin <= tmax) && (tmax > 0.f);
//}
//
//#define INTERSECT_STACK_SIZE (16)
//bool intersectBVH(in Ray r, inout Hit hit)
//{
//	bool intersected = false;
//	int stack[INTERSECT_STACK_SIZE];
//	int ptr = 0;
//	stack[ptr++] = -1;
//	int idx = 0;
//	const float3 invdir = 1.0f / r.direction;
//	hit.distance = INFINITY;
//
//	int triangleID;
//	int leftNode;
//	int rightNode;
//	Bound lbox;
//	Bound rbox;
//	float leftMin;
//	float rightMin;
//	float leftMax;
//	float rightMax;
//
//	while (idx > -1)
//	{
//		if (radixTreeNodes[idx].leaf)
//		{
//			triangleID = radixTreeNodes[idx].triangleID;
//			if (IntersectTriangle(r,
//				vertices[triangleID * 3].position,
//				vertices[triangleID * 3 + 1].position,
//				vertices[triangleID * 3 + 2].position,
//				hit.barycentricUV,
//				hit.distance))
//			{
//				hit.primitiveID = triangleID;
//				intersected = true;
//			}
//		}
//		else
//		{
//			leftNode = radixTreeNodes[idx].left;
//			rightNode = radixTreeNodes[idx].right;
//
//			lbox = radixTreeNodes[leftNode].bound;
//			rbox = radixTreeNodes[rightNode].bound;
//
//			bool traverseLeftChild = RayBoxIntersection(r.origin, invdir, lbox, leftMin, leftMax);
//			bool traverseRightChild = RayBoxIntersection(r.origin, invdir, rbox, rightMin, rightMax);
//			traverseLeftChild = traverseLeftChild && (hit.distance >= leftMin);
//			traverseRightChild = traverseRightChild && (hit.distance >= rightMin);
//
//			if (traverseLeftChild && traverseRightChild)
//			{
//				int deferred;
//				if (leftMin > rightMin)
//				{
//					idx = rightNode;
//					deferred = leftNode;
//				}
//				else
//				{
//					idx = leftNode;
//					deferred = rightNode;
//				}
//				stack[ptr++] = deferred;
//				continue;
//			}
//			else if (traverseLeftChild)
//			{
//				idx = leftNode;
//				continue;
//			}
//			else if (traverseRightChild)
//			{
//				idx = rightNode;
//				continue;
//			}
//		}
//		idx = stack[--ptr];
//	}
//
//	return intersected;
//}
//
////#define INTERSECT_STACK_SIZE (16)
////bool intersectBVH(in Ray r, inout Hit hit)
////{
////	bool intersected = false;
////	int stack[INTERSECT_STACK_SIZE];
////	int ptr = 0;
////	stack[ptr++] = -1;
////	int idx = 0;
////	// ooeps is very small number, used instead of raydir xyz component when that component is near zero
////	float ooeps = exp2(-80.0f); // Avoid div by zero, returns 1/2^80, an extremely small number
////	float idirx = 1.0f / (abs(r.direction.x) > ooeps ? r.direction.x : copysignf(ooeps, r.direction.x)); // inverse ray direction
////	float idiry = 1.0f / (abs(r.direction.y) > ooeps ? r.direction.y : copysignf(ooeps, r.direction.y)); // inverse ray direction
////	float idirz = 1.0f / (abs(r.direction.z) > ooeps ? r.direction.z : copysignf(ooeps, r.direction.z)); // inverse ray direction
////	float oodx = r.origin.x * idirx;  // ray origin / ray direction
////	float oody = r.origin.y * idiry;  // ray origin / ray direction
////	float oodz = r.origin.z * idirz;  // ray origin / ray direction
////
////	hit.distance = INFINITY;
////
////	uint triangleID;
////	uint leftNode;
////	uint rightNode;
////	Bound lbox;
////	Bound rbox;
////	float leftMin = 0.f;
////	float rightMin = 0.f;
////	float leftMax = 0.f;
////	float rightMax = 0.f;
////	float c0lox;
////	float c0hix;
////	float c0loy;
////	float c0hiy;
////	float c0loz;
////	float c0hiz;
////	float c1loz;
////	float c1hiz;
////	float c0min;
////	float c0max;
////	float c1lox;
////	float c1hix;
////	float c1loy;
////	float c1hiy;
////	float c1min;
////	float c1max;
////	bool traverseLeftChild;
////	bool traverseRightChild;
////
////	while (idx > -1)
////	{
////		if (radixTreeNodes[idx].leaf)
////		{
////			triangleID = radixTreeNodes[idx].triangleID;
////			if (IntersectTriangle(r,
////				vertices[triangleID * 3].position,
////				vertices[triangleID * 3 + 1].position,
////				vertices[triangleID * 3 + 2].position,
////				hit.barycentricUV,
////				hit.distance))
////			{
////				hit.primitiveID = triangleID;
////				intersected = true;
////			}
////		}
////		else
////		{
////			leftNode = radixTreeNodes[idx].left;
////			rightNode = radixTreeNodes[idx].right;
////
////			lbox = radixTreeNodes[leftNode].bound;
////			rbox = radixTreeNodes[rightNode].bound;
////
////			c0lox = lbox.min.x * idirx - oodx; // n0xy.x = c0.lo.x, child 0 minbound x
////			c0hix = lbox.max.x * idirx - oodx; // n0xy.y = c0.hi.x, child 0 maxbound x
////			c0loy = lbox.min.y * idiry - oody; // n0xy.z = c0.lo.y, child 0 minbound y
////			c0hiy = lbox.max.y * idiry - oody; // n0xy.w = c0.hi.y, child 0 maxbound y
////			c0loz = lbox.min.z * idirz - oodz; // nz.x   = c0.lo.z, child 0 minbound z
////			c0hiz = lbox.max.z * idirz - oodz; // nz.y   = c0.hi.z, child 0 maxbound z
////			c0min = max4(min(c0lox, c0hix), min(c0loy, c0hiy), min(c0loz, c0hiz), 0.f);
////			c0max = min4(max(c0lox, c0hix), max(c0loy, c0hiy), max(c0loz, c0hiz), hit.distance);
////			c1lox = rbox.min.x * idirx - oodx; // n1xy.x = c1.lo.x, child 1 minbound x
////			c1hix = rbox.max.x * idirx - oodx; // n1xy.y = c1.hi.x, child 1 maxbound x
////			c1loy = rbox.min.y * idiry - oody; // n1xy.z = c1.lo.y, child 1 minbound y
////			c1hiy = rbox.max.y * idiry - oody; // n1xy.w = c1.hi.y, child 1 maxbound y
////			c1loz = rbox.min.z * idirz - oodz; // nz.z   = c1.lo.z, child 1 minbound z
////			c1hiz = rbox.max.z * idirz - oodz; // nz.w   = c1.hi.z, child 1 maxbound z
////			c1min = max4(min(c1lox, c1hix), min(c1loy, c1hiy), min(c1loz, c1hiz), 0.f);
////			c1max = min4(max(c1lox, c1hix), max(c1loy, c1hiy), max(c1loz, c1hiz), hit.distance);
////
////			traverseLeftChild = (c0max >= c0min); // && (c0min >= tmin) && (c0min <= ray_tmax);
////			traverseRightChild = (c1max >= c1min); // && (c1min >= tmin) && (c1min <= ray_tmax);
////
////			if (traverseLeftChild && traverseRightChild)
////			{
////				int deferred;
////				if (c0min > c1min)
////				{
////					idx = rightNode;
////					deferred = leftNode;
////				}
////				else
////				{
////					idx = leftNode;
////					deferred = rightNode;
////				}
////				stack[ptr++] = deferred;
////				continue;
////			}
////			else if (traverseLeftChild)
////			{
////				idx = leftNode;
////				continue;
////			}
////			else if (traverseRightChild)
////			{
////				idx = rightNode;
////				continue;
////			}
////		}
////		idx = stack[--ptr];
////	}
////
////	return intersected;
////}
//
//#endif

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint screenWidth = variousUB.vScreenResolution.x;
	const uint screenHeight = variousUB.vScreenResolution.y;
	if (dispatchThreadID.x >= screenWidth * screenHeight)
		return;

	float3 color = accumulatedColor.Load(dispatchThreadID.x);
	if (hitIndex.Load(dispatchThreadID.x) == -1)
	{
		float3 emit = float3(1.0f, 1.0f, 1.0f);
		color += colorMask.Load(dispatchThreadID.x) * emit;
		hitIndex[dispatchThreadID.x] = -2;
	}

	raySampleTexture[uint2(dispatchThreadID.x % screenWidth, dispatchThreadID.x / screenWidth)] += float4(color, 0.0f);
	//raySampleTexture[dispatchThreadID.xy] *= 1.0f / customUP.samples;

	//int2 xy = int2(dispatchThreadID.x % screenWidth, dispatchThreadID.x / screenWidth);

	//float2 uv = { float(xy.x) / screenWidth, float(xy.y) / screenHeight };
	//uv = 2.0 * uv - 1.0;
	//uv.y = -uv.y;
	//float4 rayDirection = mul(cameraUB.invViewProjMatrix, float4(uv, 0.0f, 1.0f));
	//rayDirection /= rayDirection.w;

	//Ray ray;
	//ray.origin = cameraUB.position.xyz;
	//ray.direction = normalize(rayDirection.xyz - cameraUB.position.xyz);
	//ray.tmin = 0.0f;
	//ray.tmax = INFINITY;

	//Hit hit;
	//if (intersectBVH(ray, hit))
	//	raySampleTexture[xy] = float4(1.0f, 1.0f, 1.0f, 0);
	//else
	//	raySampleTexture[xy] = float4(0.1f, 0.1f, 0.98f, 0);

	//if (intersectBVH(ray, hit))
	//{
	//	float3 unitVector;
	//	float e = 1.0f;
	//	float f = 2 * PI * random(xy + variousUB.vCosTime.w);
	//	float cosTheta = pow(1.0f - random(xy + variousUB.vCosTime.w + 1), 1.0f / (e + 1.0f));
	//	float sinTheta = sqrt(1.0f - cosTheta * cosTheta);
	//	unitVector.x = sinTheta * cos(f);
	//	unitVector.y = sinTheta * sin(f);
	//	unitVector.z = cosTheta;

	//	ray.origin += ray.direction * hit.distance;
	//	ray.direction = normalize(mul(triangles[hit.primitiveID].tbn, unitVector));
	//	ray.origin += ray.direction * 0.001f;

	//	if (intersectBVH(ray, hit))
	//		ray.color = float3(0, 0, 0);
	//	else
	//		ray.color = float3(1, 1, 1) * float3(0.9f, 0.9f, 1.0f);

	//}
	//else
	//	ray.color = float4(0.9f, 0.9f, 1.f, 1.f);
}