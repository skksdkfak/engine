﻿#include "globals.hlsl"
#include "PathTracing.hlsl"

StructuredBuffer<TreeNode>		radixTreeNodes		: register(t0);
StructuredBuffer<Vertex>		vertices			: register(t1);
RWBuffer<int4>					triWoop				: register(u0);
RWBuffer<int>					triIndex			: register(u1);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

// okay, so this one seems excessive
float nvMatrixCollapse(float2x2 inpMatrix, int row, int column)
{
	float result = 0;
	int i;
	int m = 0;
	for (i = 0; i < 1; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 1; j += 1) {
			if (n == column) n += 1;
			result = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

float2x2 nvMatrixCollapse(float3x3 inpMatrix, int row, int column)
{
	float2x2 result = (float2x2)0;
	int i;
	int m = 0;
	for (i = 0; i < 2; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 2; j += 1) {
			if (n == column) n += 1;
			result[i][j] = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

float3x3 nvMatrixCollapse(float4x4 inpMatrix, int row, int column)
{
	float3x3 result = (float3x3)0;
	int i;
	int m = 0;
	for (i = 0; i < 3; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 3; j += 1) {
			if (n == column) n += 1;
			result[i][j] = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

// Returns a matrix which is the Adjoint of inpMatrix.
float4x4 nvAdjoint(float4x4 inpMatrix)
{
	float4x4 result = (float4x4)0;
	result[0][0] = determinant(nvMatrixCollapse(inpMatrix, 0, 0));
	result[0][1] = -determinant(nvMatrixCollapse(inpMatrix, 0, 1));
	result[0][2] = determinant(nvMatrixCollapse(inpMatrix, 0, 2));
	result[1][0] = -determinant(nvMatrixCollapse(inpMatrix, 1, 0));
	result[1][1] = determinant(nvMatrixCollapse(inpMatrix, 1, 1));
	result[1][2] = -determinant(nvMatrixCollapse(inpMatrix, 1, 2));
	result[2][0] = determinant(nvMatrixCollapse(inpMatrix, 2, 0));
	result[2][1] = -determinant(nvMatrixCollapse(inpMatrix, 2, 1));
	result[2][2] = determinant(nvMatrixCollapse(inpMatrix, 2, 2));
	// Cofactor of 4th column
	result[0][3] = -determinant(nvMatrixCollapse(inpMatrix, 0, 3));
	result[1][3] = determinant(nvMatrixCollapse(inpMatrix, 1, 3));
	result[2][3] = -determinant(nvMatrixCollapse(inpMatrix, 2, 3));
	// Cofactor of 4th row
	result[3][0] = -determinant(nvMatrixCollapse(inpMatrix, 3, 0));
	result[3][1] = determinant(nvMatrixCollapse(inpMatrix, 3, 1));
	result[3][2] = -determinant(nvMatrixCollapse(inpMatrix, 3, 2));
	result[3][3] = determinant(nvMatrixCollapse(inpMatrix, 3, 3));
	// Adjoint is TRANSPOSE of matrix containing cofactors
	return transpose(result);
}

// Returns a matrix which is the Adjoint of inpMatrix.
float3x3 nvAdjoint(float3x3 inpMatrix)
{
	float3x3 result = (float3x3)0;
	// Cofactor of top-left 3�3 matrix
	result[0][0] = determinant(nvMatrixCollapse(inpMatrix, 0, 0));
	result[0][1] = -determinant(nvMatrixCollapse(inpMatrix, 0, 1));
	result[0][2] = determinant(nvMatrixCollapse(inpMatrix, 0, 2));
	result[1][0] = -determinant(nvMatrixCollapse(inpMatrix, 1, 0));
	result[1][1] = determinant(nvMatrixCollapse(inpMatrix, 1, 1));
	result[1][2] = -determinant(nvMatrixCollapse(inpMatrix, 1, 2));
	result[2][0] = determinant(nvMatrixCollapse(inpMatrix, 2, 0));
	result[2][1] = -determinant(nvMatrixCollapse(inpMatrix, 2, 1));
	result[2][2] = determinant(nvMatrixCollapse(inpMatrix, 2, 2));
	// Adjoint is TRANSPOSE of matrix containing cofactors
	return transpose(result);
}

// Returns a matrix which is the Inverse of inpMatrix.
float4x4 nvInverse(float4x4 inpMatrix)
{
	float4x4 outMatrix = (float4x4)0;
	float det = determinant(inpMatrix);
	if (det != 0.0)
		outMatrix = (1.0 / det) * nvAdjoint(inpMatrix);
	return outMatrix;
}

// Returns a matrix which is the Inverse of inpMatrix.
float3x3 nvInverse(float3x3 inpMatrix)
{
	float3x3 outMatrix = (float3x3)0;
	float det = determinant(inpMatrix);
	if (det != 0.0)
		outMatrix = (1.0 / det) * nvAdjoint(inpMatrix);
	return outMatrix;
}

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const int threadIndex = dispatchThreadID.x;
	const int numTriangles = bvhUB.numTriangles;
	if (threadIndex >= numTriangles)
		return;

	const int triangleID = radixTreeNodes[threadIndex + numTriangles - 1].triangleID;
	const float3 v0 = vertices[triangleID * 3].position;
	const float3 v1 = vertices[triangleID * 3 + 1].position;
	const float3 v2 = vertices[triangleID * 3 + 2].position;

	float3 col0 = v0 - v2;
	float3 col1 = v1 - v2;
	float3 col2 = cross(v0 - v2, v1 - v2);
	float3 col3 = v2;
	float4x4 mtx;
	mtx[0] = float4(col0.x, col1.x, col2.x, col3.x);
	mtx[1] = float4(col0.y, col1.y, col2.y, col3.y);
	mtx[2] = float4(col0.z, col1.z, col2.z, col3.z);
	mtx[3] = float4(0.0f, 0.0f, 0.0f, 1.0f);
	mtx = nvInverse(mtx);

	if (mtx[2][0] == 0.0f)
		mtx[2][0] = 0.0f;  // avoid degenerate coordinates

	const int dataIndex = threadIndex * 4;
	triWoop[dataIndex + 0] = int4(asint(mtx[2][0]), asint(mtx[2][1]), asint(mtx[2][2]), asint(-mtx[2][3]));
	triWoop[dataIndex + 1] = asint(mtx[0]);
	triWoop[dataIndex + 2] = asint(mtx[1]);
	triWoop[dataIndex + 3] = 0x80000000; // Terminator

	triIndex[dataIndex + 0] = triangleID;
	triIndex[dataIndex + 1] = 0;
	triIndex[dataIndex + 2] = 0;
	triIndex[dataIndex + 3] = 0;
}