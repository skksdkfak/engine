#include "globals.hlsl"

#define WG_SIZE 128
#define BITS_PER_PASS 4
#define GET_GROUP_SIZE WG_SIZE
#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x

RWStructuredBuffer<uint> dst : register(u0);
RWStructuredBuffer<uint> src : register(u1);
RWStructuredBuffer<uint> sumBuffer : register(u2);

groupshared uint ldsData[2048];

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	int m_numElems;
	int m_numBlocks;
	int m_numScanBlocks;
};

uint ScanExclusive(uint n, int lIdx, int lSize)
{
	uint blocksum;
	int offset = 1;
	for (int nActive = n >> 1; nActive>0; nActive >>= 1, offset <<= 1)
	{
		GroupMemoryBarrierWithGroupSync();
		for (int iIdx = lIdx; iIdx<nActive; iIdx += lSize)
		{
			int ai = offset*(2 * iIdx + 1) - 1;
			int bi = offset*(2 * iIdx + 2) - 1;
			ldsData[bi] += ldsData[ai];
		}
	}

	GroupMemoryBarrierWithGroupSync();

	if (lIdx == 0)
	{
		blocksum = ldsData[n - 1];
		ldsData[n - 1] = 0;
	}

	GroupMemoryBarrierWithGroupSync();

	offset >>= 1;
	for (int nActive = 1; nActive<n; nActive <<= 1, offset >>= 1)
	{
		GroupMemoryBarrierWithGroupSync();
		for (int iIdx = lIdx; iIdx<nActive; iIdx += lSize)
		{
			int ai = offset*(2 * iIdx + 1) - 1;
			int bi = offset*(2 * iIdx + 2) - 1;
			uint temp = ldsData[ai];
			ldsData[ai] = ldsData[bi];
			ldsData[bi] += temp;
		}
	}
	GroupMemoryBarrierWithGroupSync();

	return blocksum;
}

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID)
{
	int gIdx = GET_GLOBAL_IDX;
	int lIdx = GET_LOCAL_IDX;

	ldsData[2 * lIdx] = (2 * gIdx < m_numElems) ? src[2 * gIdx] : 0;
	ldsData[2 * lIdx + 1] = (2 * gIdx + 1 < m_numElems) ? src[2 * gIdx + 1] : 0;

	uint sum = ScanExclusive(WG_SIZE * 2, GET_LOCAL_IDX, GET_GROUP_SIZE);

	if (lIdx == 0) sumBuffer[GET_GROUP_IDX] = sum;

	if ((2 * gIdx) < m_numElems)
	{
		dst[2 * gIdx] = ldsData[2 * lIdx];
	}
	if ((2 * gIdx + 1) < m_numElems)
	{
		dst[2 * gIdx + 1] = ldsData[2 * lIdx + 1];
	}
}