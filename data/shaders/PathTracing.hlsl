#define _EPSILON_ 0.000001f
#define INFINITY 1e10
#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F
#define Ci 1.2
#define Ct 1.0

struct Bound
{
	float3 min;
	float pad0;
	float3 max;
	float pad1;
};

struct Sphere
{
	Bound bound;
	float rad;
	float3 p;
	int index;
	uint morton_code[2];
};

struct TreeNode
{
	int triangleID;
	int left;
	int right;
	int parent;
	Bound bound;
	float cost;
	float area;
};

struct Triangle
{
	float4 vertices[3];
	//float4 normal;
	//float4 binormal;
	//float4 tangent;
	//float2 texCoord[3];
	//float3x3 tbn;
	//uint materialID;
};

struct Ray
{
	float3 origin;
	float tmin;
	float3 direction;
	float tmax;
};

struct Hit
{
	uint primitiveID;
	float distance;
	float2 barycentricUV;
};

float TriangleIntersect(Ray ray, float3 vertexA, float3 vertexB, float3 vertexC, out float3 ret)
{
	float3 edge1, edge2, tvec, pvec, qvec;
	float det;
	float result = -1.0f;
	ret = float3(0.0f, 0.0f, 0.0f);
	qvec = float3(0.0f, 0.0f, 0.0f);

	edge1 = vertexB - vertexA;
	edge2 = vertexC - vertexA;

	pvec = cross(ray.direction, edge2);
	det = dot(edge1, pvec);

	if (det > _EPSILON_)
	{
		result = 1.0;
		tvec = ray.origin - vertexA;
		ret.x = dot(tvec, pvec);
		if (ret.x < 0.0 || ret.x > det)
			result = -1.0;
		if (result > 0.0)
		{
			qvec = cross(tvec, edge1);
			ret.y = dot(ray.direction, qvec);
			if ((ret.y < 0.0) || ((ret.x + ret.y) > det))
				result = -1.0;
		}
	}

	if (det < -_EPSILON_)
	{
		result = 1.0;
		tvec = ray.origin - vertexA;
		ret.x = dot(tvec, pvec);
		if (ret.x > 0.0 || ret.x < det)
			result = -1.0;
		if (result > 0.0)
		{
			qvec = cross(tvec, edge1);
			ret.y = dot(ray.direction, qvec);
			if ((ret.y > 0.0) || ((ret.x + ret.y) < det))
				result = -1.0;
		}
	}

	if (result > 0.0)
	{
		ret.z = dot(edge2, qvec);
		ret /= det;
	}

	if (ret.z < _EPSILON_)
		result = -1.0;

	return result;
}
//-----------------------------------------------
// return value:
//   result > 0.0: intersection found
//   result < 0.0: no intersection found
//
//   ret.x --> barycentric u
//   ret.y --> barycentric v
//   ret.z --> Hitpoint t (ray)
//-----------------------------------------------

void merge_bounds(in Bound b1, in Bound b2, inout Bound b3)
{
	b3.min = min(b1.min, b2.min);
	b3.max = max(b1.max, b2.max);
}

void calculateLeafBoundingBox(float3 vertex1, float3 vertex2, float3 vertex3, inout Bound bound)
{
	bound.min = min(vertex1, min(vertex2, vertex3));
	bound.max = max(vertex1, max(vertex2, vertex3));
}