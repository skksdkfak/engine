#include "globals.hlsl"
#include "Model.hlsl"

#ifndef BLEND_SHAPE_DEFORMER_COUNT
#define BLEND_SHAPE_DEFORMER_COUNT 16
#endif

struct Influence
{
	vector position;
	vector normal;
};

StructuredBuffer<Influence> g_influenceBuffer[BLEND_SHAPE_DEFORMER_COUNT] : register(t3);

GLOBAL_CAMERA_UB(cameraUB);
GLOBAL_TRANSFORM_UB(transformUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	Material materialCustomUB;
};

#ifdef USE_VERTEX_BUFFER

struct VS_INPUT
{
	float3 vInPositionOS		: POSITION0;
	float3 vInNormalOS			: NORMAL0;
	float3 vInBinormalOS		: BINORMAL0;
	float3 vInTangentOS			: TANGENT0;
#ifdef HAS_TEXCOORD
	float2 vInTexCoord			: TEXCOORD0;
#endif
#ifdef SKIN_MESH
	float4 vBlendIndices1		: BLENDINDICES0;
	float4 vBlendWeights1		: BLENDWEIGHT0;
	float4 vBlendIndices2		: BLENDINDICES1;
	float4 vBlendWeights2		: BLENDWEIGHT1;
#endif
#ifdef MORPH_MESH
	float3 vInBlendPositionOS0	: POSITION1;
	float3 vInBlendNormalOS0	: NORMAL1;
#endif
};

#else

struct VS_INPUT
{
	float3 vInPositionOS;
	float3 vInNormalOS;
	float3 vInBinormalOS;
	float3 vInTangentOS;
#ifdef HAS_TEXCOORD
	float2 vInTexCoord;
#endif
#ifdef SKIN_MESH
	float4 vBlendIndices1;
	float4 vBlendWeights1;
	float4 vBlendIndices2;
	float4 vBlendWeights2;
#endif
#ifdef MORPH_MESH
	float3 vInBlendPositionOS0;
	float3 vInBlendNormalOS0;
#endif
};

StructuredBuffer<float3> vInPositionOS	: register(t12);
StructuredBuffer<float3> vInNormalOS	: register(t13);
StructuredBuffer<float3> vInBinormalOS	: register(t14);
StructuredBuffer<float3> vInTangentOS	: register(t15);
#ifdef HAS_TEXCOORD
StructuredBuffer<float2> vInTexCoord	: register(t16);
#endif
#ifdef SKIN_MESH
StructuredBuffer<float4> vBlendIndices1	: register(t17);
StructuredBuffer<float4> vBlendWeights1	: register(t18);
StructuredBuffer<float4> vBlendIndices2	: register(t19);
StructuredBuffer<float4> vBlendWeights2	: register(t20);
#endif
#ifdef MORPH_MESH
StructuredBuffer<float3> vInBlendPositionOS0 : register(t21);
StructuredBuffer<float3> vInBlendNormalOS0 : register(t22);
#endif

StructuredBuffer<uint> indexBuffer : register(t23);

#endif

struct VS_OUTPUT
{
	float4 vPosition    : SV_POSITION;
	float4 vWorldPos	: POSITION;
	float3 vNormal		: NORMAL;
	float3 vBinormal	: BINORMAL;
	float3 vTangent		: TANGENT;
	float2 texCoord		: TEXCOORD0;
	uint RTIndex : RTARRAYINDEX;
};

#ifdef USE_VERTEX_BUFFER
VS_OUTPUT main(VS_INPUT i, uint vertexID : SV_VertexID, uint instanceID : SV_InstanceID)
{
#else
VS_OUTPUT main(uint vertexID : SV_VertexID, uint instanceID : SV_InstanceID)
{
	VS_INPUT i;
	i.vInPositionOS = vInPositionOS[vertexID];
	i.vInNormalOS = vInNormalOS[vertexID];
	i.vInBinormalOS = vInBinormalOS[vertexID];
	i.vInTangentOS = vInTangentOS[vertexID];
#ifdef HAS_TEXCOORD
	i.vInTexCoord = vInTexCoord[vertexID];
#endif
#ifdef SKIN_MESH
	i.vBlendIndices1 = vBlendIndices1[vertexID];
	i.vBlendWeights1 = vBlendWeights1[vertexID];
	i.vBlendIndices2 = vBlendIndices2[vertexID];
	i.vBlendWeights2 = vBlendWeights2[vertexID];
#endif
#ifdef MORPH_MESH
	i.vInBlendPositionOS0 = vInBlendPositionOS0[vertexID];
	i.vInBlendNormalOS0 = vInBlendNormalOS0[vertexID];
#endif

#endif
	VS_OUTPUT output;

	float4 vPositionWS;
	float3 vNormalWS;
	float3 vBinormalWS;
	float3 vTangentWS;
	float2 vTexCoord;

	float4 vPositionOS = float4(i.vInPositionOS, 1.0f);
	float3 vNormalOS = i.vInNormalOS;
#ifdef HAS_TEXCOORD
	vTexCoord = i.vInTexCoord.xy;
#else
	vTexCoord = 0;
#endif
//#ifdef MORPH_MESH
//	if (modelCustomUB.timeElapsed != 0)
//	{
//		for (int blendShapeIndex = 0; blendShapeIndex < BLEND_SHAPE_DEFORMER_COUNT; blendShapeIndex++)
//		{
//			vPositionOS += (float4(i.vInBlendPositionOS0, 1.0f) - float4(i.vInPositionOS, 1.0f)) * 1.0f * modelCustomUB.timeElapsed;
//			vNormalOS += (i.vInBlendNormalOS0.xyz - i.vInNormalOS) * 1.0f * modelCustomUB.timeElapsed;
//			//vPositionOS += (g_influenceBuffer[blendShapeIndex][vertexID].position - float4(i.vInPositionOS, 1.0f)) * 1.0f * modelCustomUB.timeElapsed;
//			//vNormalOS += (g_influenceBuffer[blendShapeIndex][vertexID].normal.xyz - i.vInNormalOS) * 1.0f * modelCustomUB.timeElapsed;
//		}
//	}
//#endif
//#ifdef SKIN_MESH
//	float4 vPositionSkinDeform;
//	vPositionSkinDeform = mul(modelCustomUB.mConstBone[i.vBlendIndices1.x] * i.vBlendWeights1.x, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.y] * i.vBlendWeights1.y, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.z] * i.vBlendWeights1.z, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.w] * i.vBlendWeights1.w, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.x] * i.vBlendWeights2.x, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.y] * i.vBlendWeights2.y, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.z] * i.vBlendWeights2.z, vPositionOS);
//	vPositionSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.w] * i.vBlendWeights2.w, vPositionOS);
//	vPositionOS = vPositionSkinDeform;
//
//	float3 vNormalSkinDeform;
//	vNormalSkinDeform = mul(modelCustomUB.mConstBone[i.vBlendIndices1.x] * i.vBlendWeights1.x, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.y] * i.vBlendWeights1.y, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.z] * i.vBlendWeights1.z, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices1.w] * i.vBlendWeights1.w, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.x] * i.vBlendWeights2.x, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.y] * i.vBlendWeights2.y, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.z] * i.vBlendWeights2.z, vNormalOS);
//	vNormalSkinDeform += mul(modelCustomUB.mConstBone[i.vBlendIndices2.w] * i.vBlendWeights2.w, vNormalOS);
//	vNormalOS = vNormalSkinDeform;
//#endif
	vPositionWS = mul(transformUB.mObject2World, vPositionOS);
	vNormalWS = mul(transformUB.mObject2World, vNormalOS);
	vBinormalWS = mul(transformUB.mObject2World, i.vInBinormalOS);
	vTangentWS = mul(transformUB.mObject2World, i.vInTangentOS);

	output.vPosition = 0;
#ifdef BASE_RENDER
	output.vPosition = mul(cameraUB.viewProjMatrix, vPositionWS);
#endif
	output.vWorldPos = vPositionWS;
	output.vNormal = normalize(vNormalWS);
	output.vBinormal = normalize(vBinormalWS);
	output.vTangent = normalize(vTangentWS);
	output.texCoord = vTexCoord;
	output.RTIndex = instanceID;

	return output;
}