import os
import glob
import subprocess

path = "../../../radixsort/src/kernels/glsl"

shaderfiles = []
for exts in ('*.vert', '*.frag', '*.comp', '*.geom', '*.tesc', '*.tese', '*.comp', '*.glsl'):
	shaderfiles.extend(glob.glob(os.path.join(path, exts)))

failedshaders = []
for shaderfile in shaderfiles:
		print("\n-------- %s --------\n" % shaderfile)
		if subprocess.call("glslangValidator -V %s -o glsl/spv/%s.spv --target-env spirv1.3" % (shaderfile, os.path.splitext(os.path.basename(shaderfile))[0]), shell=True) != 0:
			failedshaders.append(shaderfile)

print("\n-------- Compilation result --------\n")

if len(failedshaders) == 0:
	print("SUCCESS: All shaders compiled to SPIR-V")
else:
	print("ERROR: %d shader(s) could not be compiled:\n" % len(failedshaders))
	for failedshader in failedshaders:
		print("\t" + failedshader)
