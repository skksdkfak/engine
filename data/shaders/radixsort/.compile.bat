@echo off

cd %CD%

SET src_dir="../../../../radixsort/src/kernels/glsl"

for %%i in (%src_dir%/*.glsl) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%%~ni.spv --target-env spirv1.3
for %%i in (%src_dir%/*.vert) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%%i.spv --target-env spirv1.3
for %%i in (%src_dir%/*.geom) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%%i.spv --target-env spirv1.3
for %%i in (%src_dir%/*.frag) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%%i.spv --target-env spirv1.3
for %%i in (%src_dir%/*.comp) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%%i.spv --target-env spirv1.3

pause