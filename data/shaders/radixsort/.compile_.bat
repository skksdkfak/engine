@echo off

cd %CD%

SET src_dir="../../../../radixsort/src/kernels/glsl"
SET src_file="hrsaddoffset.comp"

for %%i in (%src_dir%/%src_file%.glsl) do glslangValidator.exe %src_dir%/%%i -V -o glsl/spv/%src_file%.spv --target-env spirv1.3

pause