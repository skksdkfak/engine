cbuffer TransformUB : register(b0)
{
	struct
	{
		float4x4 mObject2World;
		matrix mWorld2Object;
		matrix mModelView;
		matrix mModelViewProjection;
		matrix mTransposeModelView;
		matrix mInverseTransposeModelView;
	}transformUB;
};

struct VS_INPUT
{
	float3 vInPositionOS		: LOCATION0;
	float3 vInNormalOS			: LOCATION1;
	float3 vInBinormalOS		: LOCATION2;
	float3 vInTangentOS			: LOCATION3;
	float2 vInTexCoord			: LOCATION4;
};

struct VS_OUTPUT
{
	float3 vWorldPos	: POSITION;
	float3 vNormal		: NORMAL;
	float3 vBinormal	: BINORMAL;
	float3 vTangent		: TANGENT;
	float2 texCoord		: TEXCOORD0;
};

VS_OUTPUT main(VS_INPUT i)
{
	VS_OUTPUT output;

	output.vWorldPos = mul((float3x3)transformUB.mObject2World, i.vInPositionOS);
	output.vNormal = normalize(mul((float3x3)transformUB.mObject2World, i.vInNormalOS));
	output.vBinormal = normalize(mul((float3x3)transformUB.mObject2World, i.vInBinormalOS));
	output.vTangent = normalize(mul((float3x3)transformUB.mObject2World, i.vInTangentOS));
	output.texCoord = i.vInTexCoord.xy;

	return output;
}