#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable



#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define AtomInc(x) atomicAdd(x, 1)

#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

#define CHECK_BOUNDARY 1

//#define NV_GPU 1

#define GET_GROUP_SIZE WG_SIZE

layout(local_size_x = WG_SIZE) in;

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(std430, binding = 1) readonly buffer Data
{
    uint gSrc[];
};

layout(std430, binding = 2) writeonly buffer Histogram
{
	uint histogramOut[];
};

shared uint localHistogramMat[NUM_BUCKET*WG_SIZE];

#define MY_HISTOGRAM(idx) localHistogramMat[(idx)*WG_SIZE+lIdx]

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;

	for (uint i = 0; i<NUM_BUCKET; i++)
	{
		MY_HISTOGRAM(i) = 0;
	}

	GROUP_LDS_BARRIER;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM*WG_SIZE;
	uint localKey;

	uint nBlocks = (nAligned) / blockSize - nBlocksPerWG*wgIdx;

	uint addr = blockSize*nBlocksPerWG*wgIdx + ELEMENTS_PER_WORK_ITEM*lIdx;

	for (uint iblock = 0; iblock<min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		//	MY_HISTOGRAM( localKeys.x ) ++ is much expensive than atomic add as it requires read and write while atomics can just add on AMD
		//	Using registers didn't perform well. It seems like use localKeys to address requires a lot of alu ops
		//	AMD: AtomInc performs better while NV prefers ++
		for (uint i = 0; i<ELEMENTS_PER_WORK_ITEM; i++)
		{
#if defined(CHECK_BOUNDARY)
			if (addr + i < n)
#endif
			{
				localKey = (gSrc[addr + i] >> startBit) & 0xf;
#if defined(NV_GPU)
				MY_HISTOGRAM(localKey)++;
#else
				AtomInc(MY_HISTOGRAM(localKey));
#endif
			}
		}
	}
	GROUP_LDS_BARRIER;

	if (lIdx < NUM_BUCKET)
	{
		uint sum = 0;
		for (uint i = 0; i < GET_GROUP_SIZE; i++)
		{
			sum += localHistogramMat[lIdx*WG_SIZE + (i + lIdx) % GET_GROUP_SIZE];
		}
		histogramOut[lIdx*nWGs + wgIdx] = sum;
	}
}