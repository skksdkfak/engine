#define COLOR_TEX_BP t0
#define NORMAL_TEX_BP t1
#define SPECULAR_TEX_BP t2
#define CUSTOM0_TEX_BP t3
#define CUSTOM1_TEX_BP t4
#define CUSTOM2_TEX_BP t5
#define CUSTOM3_TEX_BP t6
#define CUSTOM4_TEX_BP t7
#define CUSTOM5_TEX_BP t8
#define CUSTOM6_TEX_BP t9

#define CUSTOM0_SB_BP t10
#define CUSTOM1_SB_BP t11

#define CUSTOM0_UAV_BP u0
#define CUSTOM1_UAV_BP u1
#define CUSTOM2_UAV_BP u2
#define CUSTOM3_UAV_BP u3
#define CUSTOM4_UAV_BP u4
#define CUSTOM5_UAV_BP u5
#define CUSTOM6_UAV_BP u6
#define CUSTOM7_UAV_BP u7

#define COLOR_SAM_BP s0
#define NORMAL_SAM_BP s1
#define SPECULAR_SAM_BP s2
#define CUSTOM0_SAM_BP s3
#define CUSTOM1_SAM_BP s4
#define CUSTOM2_SAM_BP s5
#define CUSTOM3_SAM_BP s6
#define CUSTOM4_SAM_BP s7
#define CUSTOM5_SAM_BP s8
#define CUSTOM6_SAM_BP s9

#define CAMERA_UB_BP b0
#define TRANSFORM_UB_BP b1
#define VARIOUS_UB_BP b2
#define LIGHT_UB_BP b3
#define CUSTOM0_UB_BP b4
#define CUSTOM1_UB_BP b5

#define GLOBAL_CAMERA_UB(x) \
 cbuffer CameraUB: register(CAMERA_UB_BP) \
 { \
   struct \
   { \
     matrix viewMatrix; \
	 matrix projMatrix; \
	 matrix viewProjMatrix; \
     matrix invViewProjMatrix; \
     vector position; \
     vector direction; \
	 float nearClipDistance; \
	 float farClipDistance; \
	 float nearFarClipDistance; \
   }x; \
 };

#define GLOBAL_TRANSFORM_UB(x) \
 cbuffer TransformUB: register(TRANSFORM_UB_BP) \
 { \
   struct \
   { \
	matrix mObject2World; \
	matrix mWorld2Object; \
	matrix mModelView; \
	matrix mModelViewProjection; \
	matrix mTransposeModelView; \
	matrix mInverseTransposeModelView; \
   }x; \
 };

#define GLOBAL_VARIOUS_UB(x) \
 cbuffer VariousUB: register(VARIOUS_UB_BP) \
 { \
   struct \
   { \
     vector	vTime; \
     vector	vSinTime; \
     vector	vCosTime; \
     vector	vDeltaTime; \
     vector	vScreenResolution; \
   }x; \
 };

#define GLOBAL_POINT_LIGHT_UB(x) \
 cbuffer PointLightUB: register(LIGHT_UB_BP) \
 { \
   struct \
   { \
     vector position; \
     float radius; \
     vector color; \
     matrix worldMatrix; \
     float multiplier; \
   }x; \
 };

#define GLOBAL_SPOT_LIGHT_UB(x) \
 cbuffer SpotLightUB: register(LIGHT_UB_BP) \
 { \
   struct \
   { \
     vector position; \
     float radius; \
     vector color; \
     float multiplier; \
	 matrix viewProjMatrix; \
     matrix worldMatrix; \
   }x; \
 };

#define GLOBAL_DIR_LIGHT_UB(x) \
 cbuffer DirectionalLightUB: register(LIGHT_UB_BP) \
 { \
   struct \
   { \
	vector			color; \
	float			multiplier; \
	matrix          mWorldViewProjection; \
	matrix          mWorld; \
	matrix          mWorldView; \
	matrix          mShadow; \
	float4          vCascadeOffset[8]; \
	float4          vCascadeScale[8]; \
	int             nCascadeLevels; \
	int             iVisualizeCascades; \
	int             iPCFBlurForLoopStart; \
	int             iPCFBlurForLoopEnd; \
	float           fMinBorderPadding; \
	float           fMaxBorderPadding; \
	float           fShadowBiasFromGUI; \
	float           fShadowPartitionSize; \
	float           fCascadeBlendArea; \
	float           fTexelSize; \
	float           fNativeTexelSizeInX; \
	float           fPaddingForCB3; \
	float4          fCascadeFrustumsEyeSpaceDepthsFloat[2]; \
	float4          fCascadeFrustumsEyeSpaceDepthsFloat4[8]; \
	float4          vLightDir; \
   }x; \
 };

#define PI 3.1415926536f

#define PCF_NUM_SAMPLES 16

static const float2 PoissonDisc2D[PCF_NUM_SAMPLES] =
{
	float2(-0.94201624f, -0.39906216f),
	float2(0.94558609f, -0.76890725f),
	float2(-0.094184101f, -0.92938870f),
	float2(0.34495938f, 0.29387760f),
	float2(-0.91588581f, 0.45771432f),
	float2(-0.81544232f, -0.87912464f),
	float2(-0.38277543f, 0.27676845f),
	float2(0.97484398f, 0.75648379f),
	float2(0.44323325f, -0.97511554f),
	float2(0.53742981f, -0.47373420f),
	float2(-0.26496911f, -0.41893023f),
	float2(0.79197514f, 0.19090188f),
	float2(-0.24188840f, 0.99706507f),
	float2(-0.81409955f, 0.91437590f),
	float2(0.19984126f, 0.78641367f),
	float2(0.14383161f, -0.14100790f)
};

static const float3 PoissonDisc3D[PCF_NUM_SAMPLES] =
{
	float3(0.66332f, 0.231147f, 0.0332347f),
	float3(0.403241f, 0.468825f, 0.814081f),
	float3(0.927396f, 0.315683f, 0.940153f),
	float3(0.159459f, 0.93881f, 0.298044f),
	float3(0.820307f, 0.824671f, 0.528611f),
	float3(0.00338755f, 0.000122074f, 0.299783f),
	float3(0.934782f, 0.755425f, 0.0302744f),
	float3(0.914975f, 0.00677511f, 0.488632f),
	float3(0.122868f, 0.034669f, 0.944762f),
	float3(0.16538f, 0.898404f, 0.971862f),
	float3(0.050264f, 0.458022f, 0.0875576f),
	float3(0.719443f, 0.997528f, 0.993255f),
	float3(0.893887f, 0.412946f, 0.345134f),
	float3(0.486679f, 0.511185f, 0.393994f),
	float3(0.178014f, 0.230567f, 0.620655f),
	float3(0.0288705f, 0.680197f, 0.555345f)
};

float3 DecodeColor(in uint colorMask)
{
	float3 color;
	color.r = (colorMask >> 16u) & 0x000000ff;
	color.g = (colorMask >> 8u) & 0x000000ff;
	color.b = colorMask & 0x000000ff;
	color /= 255.0f;
	return color;
}

float3 DecodeNormal(in uint normalMask)
{
	int3 iNormal;
	iNormal.x = (normalMask >> 18) & 0x000000ff;
	iNormal.y = (normalMask >> 9) & 0x000000ff;
	iNormal.z = normalMask & 0x000000ff;
	int3 iNormalSigns;
	iNormalSigns.x = (normalMask >> 25) & 0x00000002;
	iNormalSigns.y = (normalMask >> 16) & 0x00000002;
	iNormalSigns.z = (normalMask >> 7) & 0x00000002;
	iNormalSigns = 1 - iNormalSigns;
	float3 normal = float3(iNormal) / 255.0f;
	normal *= iNormalSigns;
	return normal;
}