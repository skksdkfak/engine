#include "globals.hlsl"
#include "PathTracing.hlsl"

#define WG_SIZE 64

RWStructuredBuffer<Bound>	input	: register(u0);
RWStructuredBuffer<Bound>	output	: register(u1);
RWStructuredBuffer<uint>	counter	: register(u2);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numElements;
		uint stride;
	}findingBoundUB;
};

groupshared Bound localBounds[WG_SIZE];

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	const uint thxId = dispatchThreadID.x;
	const uint numElements = findingBoundUB.numElements;
	const uint stride = findingBoundUB.stride;

	Bound bound;
	Bound localBound;
	localBound.min = FLT_MAX;
	localBound.max = FLT_MIN;
	for (uint i = dispatchThreadID.x; i < findingBoundUB.numElements; i += stride)
	{
		bound = input[i];

		localBound.min.x = min(localBound.min.x, bound.min.x);
		localBound.max.x = max(localBound.max.x, bound.max.x);
		localBound.min.y = min(localBound.min.y, bound.min.y);
		localBound.max.y = max(localBound.max.y, bound.max.y);
		localBound.min.z = min(localBound.min.z, bound.min.z);
		localBound.max.z = max(localBound.max.z, bound.max.z);
	}

	localBounds[groupThreadID.x] = localBound;

	GroupMemoryBarrierWithGroupSync();

	uint j = WG_SIZE / 2;
	while (j != 0)
	{
		if (groupThreadID.x < j)
		{
			bound = localBounds[groupThreadID.x + j];

			localBound.min.x = min(localBound.min.x, bound.min.x);
			localBound.max.x = max(localBound.max.x, bound.max.x);
			localBound.min.y = min(localBound.min.y, bound.min.y);
			localBound.max.y = max(localBound.max.y, bound.max.y);
			localBound.min.z = min(localBound.min.z, bound.min.z);
			localBound.max.z = max(localBound.max.z, bound.max.z);

			localBounds[groupThreadID.x] = localBound;
		}

		GroupMemoryBarrierWithGroupSync();
		j /= 2;
	}

	if (groupThreadID.x == 0)
	{
		uint mutexValue;
		do
		{
			InterlockedCompareExchange(counter[groupID.x], 0, 1, mutexValue);
		} while (mutexValue != 0);

		bound = output[groupID.x];
		localBound = localBounds[0];

		localBound.min.x = min(localBound.min.x, bound.min.x);
		localBound.max.x = max(localBound.max.x, bound.max.x);
		localBound.min.y = min(localBound.min.y, bound.min.y);
		localBound.max.y = max(localBound.max.y, bound.max.y);
		localBound.min.z = min(localBound.min.z, bound.min.z);
		localBound.max.z = max(localBound.max.z, bound.max.z);

		output[groupID.x] = localBound;

		InterlockedExchange(counter[groupID.x], 0, mutexValue);
	}
}