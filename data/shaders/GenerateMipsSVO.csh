#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint>	octreeIdx		: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint>	octreeKd		: register(CUSTOM1_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

float4 convRGBA8ToFloat4(in uint val);
uint convFloat4ToRGBA8(in float4 val);
void imageAtomicRGBA8Avg(float4 val, int coord, RWStructuredBuffer<uint> buf);

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numThread)
		return;

	//get child pointer
	uint childIdx = uint(octreeIdx[octreeCustomUB.nodeOffset + thxId] & 0x7FFFFFFF);
	float4 color = 0;
	uint ucolor = 0;
	uint noneEmptyNodes = 0;	

	for (int i = 0; i < 8; ++i)
	{
		ucolor = octreeKd[childIdx + i];
		if (ucolor)
		{
			color += convRGBA8ToFloat4(ucolor);
			noneEmptyNodes++;
		}
	}
	color /= noneEmptyNodes;

	imageAtomicRGBA8Avg(color, octreeCustomUB.nodeOffset + thxId, octreeKd);
}

//UINT atomic running average method
//From OpenGL Insight ch. 22
float4 convRGBA8ToFloat4(in uint val)
{
	return float4(float((val & 0x000000FF)), float((val & 0x0000FF00) >> 8U),
		float((val & 0x00FF0000) >> 16U), float((val & 0xFF000000) >> 24U));
}

uint convFloat4ToRGBA8(in float4 val)
{
	return (uint(val.w) & 0x000000FF) << 24U | (uint(val.z) & 0x000000FF) << 16U | (uint(val.y) & 0x000000FF) << 8U | (uint(val.x) & 0x000000FF);
}

void imageAtomicRGBA8Avg(float4 val, int coord, RWStructuredBuffer<uint> buf)
{
	//val.rgb *= 255.0;
	//val.a = 1;

	uint newVal = convFloat4ToRGBA8(val);
	uint prev = 0;
	uint cur;
	InterlockedCompareExchange(buf[coord], prev, newVal, cur);

	while (cur != prev)
	{
		prev = cur;
		float4 rval = convRGBA8ToFloat4(cur);
		rval.xyz = rval.xyz*rval.w;
		float4 curVal = rval + val;
		curVal.xyz /= curVal.w;
		newVal = convFloat4ToRGBA8(curVal);
		InterlockedCompareExchange(buf[coord], prev, newVal, cur);
	}
}