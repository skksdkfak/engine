struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

StructuredBuffer<CompactTreeNode>	radixTreeNodes			: register(t0);
StructuredBuffer<CompactTriangle>	triangles				: register(t1);
RWBuffer<float4>					triWoop					: register(u0);
RWBuffer<int>						triIndex				: register(u1);

cbuffer CustomUB : register(b0)
{
	uint numTriangles;
};

float matrixCollapse(float2x2 inpMatrix, int row, int column)
{
	float result = 0;
	int i;
	int m = 0;
	for (i = 0; i < 1; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 1; j += 1) {
			if (n == column) n += 1;
			result = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

float2x2 matrixCollapse(float3x3 inpMatrix, int row, int column)
{
	float2x2 result = (float2x2)0;
	int i;
	int m = 0;
	for (i = 0; i < 2; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 2; j += 1) {
			if (n == column) n += 1;
			result[i][j] = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

float3x3 matrixCollapse(float4x4 inpMatrix, int row, int column)
{
	float3x3 result = (float3x3)0;
	int i;
	int m = 0;
	for (i = 0; i < 3; i += 1) {
		if (m == row) m += 1;
		int j;
		int n = 0;
		for (j = 0; j < 3; j += 1) {
			if (n == column) n += 1;
			result[i][j] = inpMatrix[m][n];
			n += 1;
		}
		m += 1;
	}
	return result;
}

// Returns a matrix which is the Adjoint of inpMatrix.
float4x4 adjoint(float4x4 inpMatrix)
{
	float4x4 result = (float4x4)0;
	result[0][0] = determinant(matrixCollapse(inpMatrix, 0, 0));
	result[0][1] = -determinant(matrixCollapse(inpMatrix, 0, 1));
	result[0][2] = determinant(matrixCollapse(inpMatrix, 0, 2));
	result[1][0] = -determinant(matrixCollapse(inpMatrix, 1, 0));
	result[1][1] = determinant(matrixCollapse(inpMatrix, 1, 1));
	result[1][2] = -determinant(matrixCollapse(inpMatrix, 1, 2));
	result[2][0] = determinant(matrixCollapse(inpMatrix, 2, 0));
	result[2][1] = -determinant(matrixCollapse(inpMatrix, 2, 1));
	result[2][2] = determinant(matrixCollapse(inpMatrix, 2, 2));
	// Cofactor of 4th column
	result[0][3] = -determinant(matrixCollapse(inpMatrix, 0, 3));
	result[1][3] = determinant(matrixCollapse(inpMatrix, 1, 3));
	result[2][3] = -determinant(matrixCollapse(inpMatrix, 2, 3));
	// Cofactor of 4th row
	result[3][0] = -determinant(matrixCollapse(inpMatrix, 3, 0));
	result[3][1] = determinant(matrixCollapse(inpMatrix, 3, 1));
	result[3][2] = -determinant(matrixCollapse(inpMatrix, 3, 2));
	result[3][3] = determinant(matrixCollapse(inpMatrix, 3, 3));
	// Adjoint is TRANSPOSE of matrix containing cofactors
	return transpose(result);
}

// Returns a matrix which is the Adjoint of inpMatrix.
float3x3 adjoint(float3x3 inpMatrix)
{
	float3x3 result = (float3x3)0;
	// Cofactor of top-left 3x3 matrix
	result[0][0] = determinant(matrixCollapse(inpMatrix, 0, 0));
	result[0][1] = -determinant(matrixCollapse(inpMatrix, 0, 1));
	result[0][2] = determinant(matrixCollapse(inpMatrix, 0, 2));
	result[1][0] = -determinant(matrixCollapse(inpMatrix, 1, 0));
	result[1][1] = determinant(matrixCollapse(inpMatrix, 1, 1));
	result[1][2] = -determinant(matrixCollapse(inpMatrix, 1, 2));
	result[2][0] = determinant(matrixCollapse(inpMatrix, 2, 0));
	result[2][1] = -determinant(matrixCollapse(inpMatrix, 2, 1));
	result[2][2] = determinant(matrixCollapse(inpMatrix, 2, 2));
	// Adjoint is TRANSPOSE of matrix containing cofactors
	return transpose(result);
}

// Returns a matrix which is the Inverse of inpMatrix.
float4x4 inverse(float4x4 inpMatrix)
{
	float4x4 outMatrix = (float4x4)0;
	float det = determinant(inpMatrix);
	if (det != 0.0)
		outMatrix = (1.0 / det) * adjoint(inpMatrix);
	return outMatrix;
}

// Returns a matrix which is the Inverse of inpMatrix.
float3x3 inverse(float3x3 inpMatrix)
{
	float3x3 outMatrix = (float3x3)0;
	float det = determinant(inpMatrix);
	if (det != 0.0)
		outMatrix = (1.0 / det) * adjoint(inpMatrix);
	return outMatrix;
}

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint threadIndex = dispatchThreadID.x;
	if (threadIndex >= numTriangles)
		return;

	const uint triangleID = radixTreeNodes[threadIndex + numTriangles - 1].field0.w;
	const float3 v0 = triangles[triangleID].field0.xyz;
	const float3 v1 = triangles[triangleID].field1.xyz;
	const float3 v2 = triangles[triangleID].field2.xyz;

	float3 col0 = v0 - v2;
	float3 col1 = v1 - v2;
	float3 col2 = cross(v0 - v2, v1 - v2);
	float3 col3 = v2;
	float4x4 mtx;
	mtx[0] = float4(col0.x, col1.x, col2.x, col3.x);
	mtx[1] = float4(col0.y, col1.y, col2.y, col3.y);
	mtx[2] = float4(col0.z, col1.z, col2.z, col3.z);
	mtx[3] = float4(0.0f, 0.0f, 0.0f, 1.0f);
	mtx = inverse(mtx);

	if (mtx[2][0] == 0.0f)
		mtx[2][0] = 0.0f;  // avoid degenerate coordinates

	const int dataIndex = threadIndex * 4;
	triWoop[dataIndex + 0] = float4(mtx[2][0], mtx[2][1], mtx[2][2], -mtx[2][3]);
	triWoop[dataIndex + 1] = mtx[0];
	triWoop[dataIndex + 2] = mtx[1];
	triWoop[dataIndex + 3] = asint(0x80000000); // Terminator

	triIndex[dataIndex + 0] = triangleID;
	triIndex[dataIndex + 1] = 0;
	triIndex[dataIndex + 2] = 0;
	triIndex[dataIndex + 3] = 0;
}