#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inBinormal;
layout (location = 3) in vec3 inTangent;
layout (location = 4) in vec2 inUV;

layout (binding = 0) uniform UBO 
{
	mat4 mObject2World;
	mat4 mWorld2Object;
	mat4 mModelView;
	mat4 mModelViewProjection;
	mat4 mTransposeModelView;
	mat4 mInverseTransposeModelView;
} transformUBO;

layout (location = 0) out vec3 outWorldPos;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec3 outBinormal;
layout (location = 3) out vec3 outTangent;
layout (location = 4) out vec2 outUV;

void main()
{
	outWorldPos = mat3(transformUBO.mObject2World) * inPos;
	outNormal = normalize(mat3(transformUBO.mObject2World) * inNormal.xyz);
	outBinormal = normalize(mat3(transformUBO.mObject2World) * inBinormal.xyz);
	outTangent = normalize(mat3(transformUBO.mObject2World) * inTangent.xyz);
	outUV = inUV;
}