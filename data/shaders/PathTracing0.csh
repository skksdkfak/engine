#include "globals.hlsl"
#include "PathTracing.hlsl"

struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

cbuffer PushConstants: register(b0)
{
	uint samples;
	uint bounces;
	uint materialID;
	uint primitiveCount;
};

cbuffer cameraUB: register(b1)
{
	matrix viewMatrix;
	matrix projMatrix;
	matrix viewProjMatrix;
	matrix invViewProjMatrix;
	vector position;
	vector direction;
	float nearClipDistance;
	float farClipDistance;
	float nearFarClipDistance;
};

cbuffer VariousUB: register(b2)
{
	vector	vTime;
	vector	vSinTime;
	vector	vCosTime;
	vector	vDeltaTime;
	vector	vScreenResolution;
};

StructuredBuffer<CompactTreeNode>	radixTreeNodes			: register(t0);
StructuredBuffer<CompactTriangle>	triangles				: register(t1);
RWTexture2D<float4>					raySampleTexture		: register(u0);

float random(float2 p)
{
	return frac(cos(dot(p, float2(23.14069263277926, 2.665144142690225)))*123456.);
}

float IntersectBoxF(in Ray r, in float3 invdir, in Bound box, in float maxt)
{
	const float3 f = (box.max - r.origin) * invdir;
	const float3 n = (box.min - r.origin) * invdir;

	const float3 tmax = max(f, n);
	const float3 tmin = min(f, n);

	const float t1 = min(min(tmax.x, min(tmax.y, tmax.z)), maxt);
	const float t0 = max(max(tmin.x, max(tmin.y, tmin.z)), 0.f);

	return (t1 >= t0) ? (t0 > 0.f ? t0 : t1) : -1.f;
}

bool IntersectTriangle(in Ray r, in float3 v1, in float3 v2, in float3 v3, inout float2 barycentricUV, inout float distance)
{
	const float3 e1 = v2 - v1;
	const float3 e2 = v3 - v1;
	const float3 d = r.origin - v1;
	const float3 s1 = cross(r.direction, e2);
	const float3 s2 = cross(d, e1);
	const float invd = 1.0f / (dot(s1, e1));
	const float t = dot(e2, s2) * invd;
	if (t > 0.f && t < distance)
	{
		const float b1 = dot(d, s1) * invd;
		if (b1 >= 0.0f)
		{
			const float b2 = dot(r.direction, s2) * invd;
			if (b2 >= 0.0f && b1 + b2 <= 1.0f)
			{
				barycentricUV = float2(b1, b2);
				distance = t;
				return true;
			}
		}
	}

	return false;
}

bool RayBoxIntersection(float3 ray_pos, in float3 inv_dir, in Bound box, inout float mint, inout float maxt)
{
	const float3 n = (box.min - ray_pos) * inv_dir;
	const float3 f = (box.max - ray_pos) * inv_dir;

	const float3 tmin = min(f, n);
	const float3 tmax = max(f, n);

	mint = max(tmin.x, max(tmin.y, tmin.z));
	maxt = min(tmax.x, min(tmax.y, tmax.z));

	return (mint <= maxt) && (maxt > 0.f);
}

bool RayBoxIntersection1(float3 ray_pos, float3 inv_dir, in Bound box, inout float tmin, inout float tmax)
{
	float lo = inv_dir.x * (box.min.x - ray_pos.x);
	float hi = inv_dir.x * (box.max.x - ray_pos.x);
	tmin = min(lo, hi);
	tmax = max(lo, hi);

	lo = inv_dir.y * (box.min.y - ray_pos.y);
	hi = inv_dir.y * (box.max.y - ray_pos.y);
	tmin = max(tmin, min(lo, hi));
	tmax = min(tmax, max(lo, hi));

	lo = inv_dir.z * (box.min.z - ray_pos.z);
	hi = inv_dir.z * (box.max.z - ray_pos.z);
	tmin = max(tmin, min(lo, hi));
	tmax = min(tmax, max(lo, hi));

	return (tmin <= tmax) && (tmax > 0.f);
}

#define INTERSECT_STACK_SIZE (64)
bool intersectBVH(in Ray r, inout Hit hit)
{
	bool intersected = false;
	uint stack[INTERSECT_STACK_SIZE];
	int ptr = 0;
	stack[ptr++] = 0xFFFFFFFF;
	uint idx = 0;
	const float3 invdir = 1.0f / r.direction;
	hit.distance = INFINITY;
	const uint leafs = primitiveCount;

	uint triangleID;
	Bound lbox;
	lbox.pad0 = 0;
	lbox.pad1 = 0;
	Bound rbox;
	rbox.pad0 = 0;
	rbox.pad1 = 0;
	float leftMin;
	float rightMin;
	float leftMax;
	float rightMax;
	CompactTreeNode node;
	while (idx != 0xFFFFFFFF)
	{
		node = radixTreeNodes[idx];
		if (idx < leafs)
		{
			lbox.min = radixTreeNodes[node.field0.y].field1.xyz;
			lbox.max = radixTreeNodes[node.field0.y].field2.xyz;
			rbox.min = radixTreeNodes[node.field0.z].field1.xyz;
			rbox.max = radixTreeNodes[node.field0.z].field2.xyz;

			bool traverseLeftChild = RayBoxIntersection(r.origin, invdir, lbox, leftMin, leftMax);
			bool traverseRightChild = RayBoxIntersection(r.origin, invdir, rbox, rightMin, rightMax);
			traverseLeftChild = traverseLeftChild && (hit.distance >= leftMin);
			traverseRightChild = traverseRightChild && (hit.distance >= rightMin);

			if (traverseLeftChild && traverseRightChild)
			{
				uint deferred;
				if (leftMin > rightMin)
				{
					idx = node.field0.z;
					deferred = node.field0.y;
				}
				else
				{
					idx = node.field0.y;
					deferred = node.field0.z;
				}
				stack[ptr++] = deferred;
				continue;
			}
			else if (traverseLeftChild)
			{
				idx = node.field0.y;
				continue;
			}
			else if (traverseRightChild)
			{
				idx = node.field0.z;
				continue;
			}
		}
		else
		{
			triangleID = node.field0.w;
			if (IntersectTriangle(r,
				triangles[triangleID].field0.xyz,
				triangles[triangleID].field1.xyz,
				triangles[triangleID].field2.xyz,
				hit.barycentricUV,
				hit.distance))
			{
				hit.primitiveID = triangleID;
				intersected = true;
			}
		}
		idx = stack[--ptr];
	}

	return intersected;
}

[numthreads(16, 16, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID)
{
	const uint screenWidth = vScreenResolution.x;
	const uint screenHeight = vScreenResolution.y;
	if (dispatchThreadID.x >= screenWidth || dispatchThreadID.y >= screenHeight)
		return;

	const uint thxId = dispatchThreadID.x + (screenWidth * dispatchThreadID.y);

	float2 uv = { float(dispatchThreadID.x) / screenWidth, float(dispatchThreadID.y) / screenHeight };
	uv = 2.0 * uv - 1.0;
	uv.y = -uv.y;
	float4 rayDirection = mul(invViewProjMatrix, float4(uv, 0.0f, 1.0f));
	rayDirection /= rayDirection.w;

	float4 origin = float4(position.xyz, 0.0f);
	float4 direction = float4(normalize(rayDirection.xyz - position.xyz), INFINITY);

	float3 accucolor = 0.0f;

	Hit hit;
	const uint samples = 1;
	const uint bounces = 2;

	for (int s = 0; s < samples; s++)
	{
		Ray ray;
		ray.origin = origin.xyz;
		ray.direction = direction.xyz;
		ray.tmin = origin.w;
		ray.tmax = direction.w;

		float3 mask = 1.0f;

		for (int b = 0; b < bounces; b++)
		{
			if (!intersectBVH(ray, hit))
			{
				float3 emit = float3(1.0f, 1.0f, 1.0f);
				accucolor += mask * emit;
				break;
			}

			float3 unitVector;
			float e = 1.0f;
			float f = 2 * PI * random(dispatchThreadID.xy + vCosTime.w + b);
			float cosTheta = pow(1.0f - random(dispatchThreadID.xy + vCosTime.w + b + 1), 1.0f / (e + 1.0f));
			float sinTheta = sqrt(1.0f - cosTheta * cosTheta);
			unitVector.x = sinTheta * cos(f);
			unitVector.y = sinTheta * sin(f);
			unitVector.z = cosTheta;

			ray.origin += ray.direction * hit.distance;
			ray.direction = normalize(mul(triangles[hit.primitiveID].tbn, unitVector));
			ray.origin += ray.direction * 0.001f;

			float3 emit = float3(0.0f, 0.0f, 0.0f);
			float4 diffuse = float4(0.9f, 0.3f, 0.0f, 1.0f);

			accucolor += mask * emit;
			mask *= diffuse.rgb;
		}
	}

	raySampleTexture[dispatchThreadID.xy] = float4(accucolor / samples, 1.0f);
}