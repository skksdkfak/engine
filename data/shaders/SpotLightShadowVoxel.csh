#include "globals.hlsl"

//Texture3D<float4>	g_VoxelGridNormalTexture		: register(NORMAL_TEX_BP);
Texture2D<float>	depthMapTexture					: register(CUSTOM0_TEX_BP);

RWTexture3D<float4>	g_VoxelGridShadedOutputTexture	: register(CUSTOM0_UAV_BP);
RWTexture3D<float4>	g_VoxelGridNormalTexture		: register(CUSTOM1_UAV_BP);

SamplerComparisonState SamplerTypeComparisionPoint	: register(CUSTOM0_SAM_BP);

GLOBAL_SPOT_LIGHT_UB(spotLightUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}customUB;
};

[numthreads(8, 8, 8)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	float4 normal = g_VoxelGridNormalTexture[dispatchThreadID.xyz];
	//if (normal.w == 0)
	//	return;

	float4 outputColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float3 position = dispatchThreadID * customUB.voxelSize + customUB.voxelizePosition.xyz - customUB.voxelizeSize / 2;
	float shadowTerm = 0;
	float diffuse;
	float3 lightDirection;
	float lightVecLen;

	lightDirection = spotLightUB.position - position.xyz;
	lightVecLen = length(lightDirection);

	float4 lightViewPosition = mul(float4(position.xyz, 1.0f), spotLightUB.viewProjMatrix);
	float2 projectTexCoord;
	projectTexCoord.x = lightViewPosition.x / lightViewPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -lightViewPosition.y / lightViewPosition.w / 2.0f + 0.5f;

	float bias = 0.0001f;

	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		[unroll]
		for (int i = 0; i < PCF_NUM_SAMPLES; i++)
		{
			shadowTerm += depthMapTexture.SampleCmpLevelZero(
				SamplerTypeComparisionPoint,
				projectTexCoord + (PoissonDisc2D[i] * 0.1f),
				lightViewPosition.z / lightViewPosition.w - bias
				).r;
		}
		shadowTerm /= PCF_NUM_SAMPLES;
	}

	diffuse = saturate(dot(normal.xyz, normalize(lightDirection)));
	if (diffuse > 0.0f)
	{
		outputColor += diffuse * spotLightUB.color * spotLightUB.multiplier;
	}
	outputColor = 1;
	outputColor *= shadowTerm * saturate(1.0f - (lightVecLen / spotLightUB.radius));

	g_VoxelGridShadedOutputTexture[dispatchThreadID.xyz] += float4(outputColor.rgb, 0.0f);
}