#include "globals.hlsl"

RWTexture3D<float4> gridColor				: register(CUSTOM0_UAV_BP);
RWTexture3D<float4> gridNormal				: register(CUSTOM1_UAV_BP);
RWTexture3D<float4> gridColorIlluminated	: register(CUSTOM2_UAV_BP);

[numthreads(8, 8, 8)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	gridColor[dispatchThreadID.xyz] = 0;
	gridNormal[dispatchThreadID.xyz] = 0;
	gridColorIlluminated[dispatchThreadID.xyz] = 0;
}