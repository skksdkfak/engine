#include "PathTracing.hlsl"

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

RWStructuredBuffer<CompactTriangle>	triangles	: register(u0);
RWStructuredBuffer<uint>			counter		: register(u1);

struct VS_OUTPUT
{
	float3 vWorldPos	: POSITION;
	float3 vNormal		: NORMAL;
	float3 vBinormal	: BINORMAL;
	float3 vTangent		: TANGENT;
	float2 texCoord		: TEXCOORD0;
};

[maxvertexcount(3)]
void main(triangle VS_OUTPUT input[3])
{
	uint primitiveID;
	InterlockedAdd(counter[0], 1, primitiveID);

	float3 normal = 0;
	float3 binormal = 0;
	float3 tangent = 0;
	[unroll]
	for (uint i = 0; i < 3; i++)
	{
		normal += input[i].vNormal;
		binormal += input[i].vBinormal;
		tangent += input[i].vTangent;
	}

	CompactTriangle tri;
	tri.field0 = float4(input[0].vWorldPos, input[0].texCoord.x);
	tri.field1 = float4(input[1].vWorldPos, input[0].texCoord.y);
	tri.field2 = float4(input[2].vWorldPos, asfloat(0));
	tri.field3 = float4(input[1].texCoord, input[2].texCoord);
	tri.tbn = float3x4(
		tangent.x, binormal.x, normal.x, 0.0f,
		tangent.y, binormal.y, normal.y, 0.0f,
		tangent.z, binormal.z, normal.z, 0.0f);

	triangles[primitiveID] = tri;
}