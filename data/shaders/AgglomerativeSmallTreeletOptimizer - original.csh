#include "globals.hlsl"
#include "PathTracing.hlsl"

#define NV_SHADER_EXTN_SLOT u7
#define NV_SHADER_EXTN_REGISTER_SPACE space0

#include "nvHLSLExtns.h"

struct CompactTreeNode
{
	uint4 field0; // triangleID, left, right, parent
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

StructuredBuffer<int>							schedule				: register(t0);
RWStructuredBuffer<CompactTreeNode>				radixTreeNodes			: register(u0);
RWStructuredBuffer<uint>						nodeCounter				: register(u1);
RWStructuredBuffer<uint>						subtreeTriangles		: register(u2);

cbuffer CustomUB : register(b0)
{
	uint numTriangles;
	int gamma;
	int scheduleSize;
};

#define treeletSize 9
#define distanceMatrixSize 36
#define WG_SIZE 128
#define WARP_SIZE 32
#define NUM_OF_WARPS (WG_SIZE / WARP_SIZE)

#define WARP_ARRAY_INDEX(index, elementsPerWarp) (warp_index * (elementsPerWarp) + (index))

// Get the number of elements which can be stored in a diagonal square matrix of size 'dim,
// excluding the main diagonal.
#define TRM_SIZE(dim) (((dim - 1) * (dim)) / 2)

// Get the index of an element in an array that represents a lower triangular matrix.
// The main diagonal elements are not included in the array.
#define LOWER_TRM_INDEX(row, column) \
        (TRM_SIZE((row)) + (column))

#define WARP_SYNC \
do { \
    int _sync = 0; \
    NvShfl(_sync, 0); \
} while (0)

#define SHFL_FLOAT3(destination, source, index) \
do { \
    (destination).x = asfloat(NvShfl(asuint((source)[0]), (index))); \
    (destination).y = asfloat(NvShfl(asuint((source)[1]), (index))); \
    (destination).z = asfloat(NvShfl(asuint((source)[2]), (index))); \
} while (0);

bool isInternalNode(uint index, uint numberOfTriangles)
{
	return (index < numberOfTriangles - 1);
}

bool isLeaf(unsigned int index, unsigned int numberOfTriangles)
{
	return !isInternalNode(index, numberOfTriangles);
}

int sumArithmeticSequence(int numberOfElements, int firstElement, int lastElement)
{
	return numberOfElements * (firstElement + lastElement) / 2;
}

void floatArrayFromFloat3(float3 source, inout float destination[3])
{
	destination[0] = source.x;
	destination[1] = source.y;
	destination[2] = source.z;
}

int ffs(int n)
{
	return log2(n&-n) + 1;
}

float calculateBoundingBoxAndSurfaceArea(const float3 bbMin1, const float3 bbMax1, const float3 bbMin2, const float3 bbMax2)
{
	float3 size = max(bbMax1, bbMax2) - min(bbMin1, bbMin2);
	return 2 * (size.x * size.y + size.x * size.z + size.y * size.z);
}

float calculateBoundingBoxSurfaceArea(float3 bbMin, float3 bbMax)
{
	float3 size = bbMax - bbMin;
	return 2 * (size.x * size.y + size.x * size.z + size.y * size.z);
}

groupshared uint treeletInternalNodes[(treeletSize - 1) * (WG_SIZE / WARP_SIZE)];
groupshared uint treeletLeaves[treeletSize * (WG_SIZE / WARP_SIZE)];
groupshared float treeletLeavesAreas[treeletSize * (WG_SIZE / WARP_SIZE)];
groupshared float distancesMatrix[distanceMatrixSize * (WG_SIZE / WARP_SIZE)];

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	const uint threadIndex = dispatchThreadID.x;
	const uint global_warp_index = threadIndex / WARP_SIZE;
	const uint warp_index = groupThreadID.x / WARP_SIZE;
	const uint thread_warp_index = groupThreadID.x & (WARP_SIZE - 1);
	const uint numberOfTriangles = numTriangles;

	// If this flag is set, the thread will be excluded from the bottom up traversal, but will 
	// still be available to help form and optimize treelets
	uint currentNodeIndex;
	if (threadIndex < numberOfTriangles)
	{
		uint leafIndex = threadIndex + numberOfTriangles - 1;
		subtreeTriangles[leafIndex] = 1;
		currentNodeIndex = radixTreeNodes[leafIndex].field0.w;
		float area = radixTreeNodes[leafIndex].field2.w;
		radixTreeNodes[leafIndex].field1.w = Ct * area;
	}
	else
	{
		currentNodeIndex = 0xFFFFFFFF;
	}

	GroupMemoryBarrierWithGroupSync();

	while (NvBallot(currentNodeIndex != 0xFFFFFFFF) != 0)
	{
		// Number of threads who already have processed the current node
		uint counter = 0;

		if (currentNodeIndex != 0xFFFFFFFF)
		{
			InterlockedAdd(nodeCounter[currentNodeIndex], 1, counter);

			// Only the last thread to arrive is allowed to process the current node. This ensures 
			// that both its children will already have been processed
			if (counter == 0)
			{
				currentNodeIndex = 0xFFFFFFFF;
			}
		}

		// How many triangles can be reached by the subtree with root at the current node
		uint triangleCount = 0;
		if (counter != 0)
		{
			// Throughout the code, blocks that have loads separated from stores are so organized 
			// in order to increase ILP (Instruction level parallelism)
			uint left = radixTreeNodes[currentNodeIndex].field0.y;
			uint right = radixTreeNodes[currentNodeIndex].field0.z;
			float area = radixTreeNodes[currentNodeIndex].field2.w;
			uint trianglesLeft = subtreeTriangles[left];
			float sahLeft = radixTreeNodes[left].field1.w;
			uint trianglesRight = subtreeTriangles[right];
			float sahRight = radixTreeNodes[right].field1.w;

			triangleCount = trianglesLeft + trianglesRight;
			subtreeTriangles[currentNodeIndex] = triangleCount;
			radixTreeNodes[currentNodeIndex].field1.w = Ci * area + sahLeft + sahRight;
		}

		// Check which threads in the warp have treelets to be processed. We are only going to 
		// process a treelet if the current node is the root of a subtree with at least gamma 
		// triangles
		uint vote = NvBallot(triangleCount >= gamma);

		while (vote != 0)
		{
			// Get the thread index for the treelet that will be processed
			int rootThreadIndex = ffs(vote) - 1;

			// Get the treelet root by reading the corresponding thread's currentNodeIndex private 
			// variable
			int treeletRootIndex = NvShfl(currentNodeIndex, rootThreadIndex);

			// Initialize treelet
			uint left = radixTreeNodes[treeletRootIndex].field0.y;
			uint right = radixTreeNodes[treeletRootIndex].field0.z;
			float areaLeft = radixTreeNodes[left].field2.w;
			float areaRight = radixTreeNodes[right].field2.w;
			if (thread_warp_index == 0)
			{
				treeletInternalNodes[warp_index * (treeletSize - 1)] = treeletRootIndex;
				treeletLeaves[warp_index * treeletSize] = left;
				treeletLeaves[warp_index * treeletSize + 1] = right;
				treeletLeavesAreas[warp_index * treeletSize] = areaLeft;
				treeletLeavesAreas[warp_index * treeletSize + 1] = areaRight;
			}

			WARP_SYNC;

			// Find the treelet's internal nodes. On each iteration we choose the leaf with 
			// largest surface area and add move it to the list of internal nodes, adding its
			// two children as leaves in its place
			for (int iteration = 0; iteration < treeletSize - 2; ++iteration)
			{
				// Choose leaf with the largest area
				int largestLeafIndex;
				float largestLeafArea;
				if (thread_warp_index < 2 + iteration)
				{
					largestLeafIndex = thread_warp_index;
					largestLeafArea = treeletLeavesAreas[warp_index * treeletSize + thread_warp_index];
					if (isLeaf(treeletLeaves[warp_index * treeletSize + largestLeafIndex], numberOfTriangles))
					{
						largestLeafArea = -FLT_MAX;
					}
				}

				int numberOfElements = 2 + iteration;
				int shflAmount = numberOfElements / 2;
				while (numberOfElements > 1)
				{
					int otherIndex = NvShflDown(largestLeafIndex, shflAmount);
					float otherArea = asfloat(NvShflDown(asuint(largestLeafArea), shflAmount));

					if (otherArea > largestLeafArea)
					{
						largestLeafArea = otherArea;
						largestLeafIndex = otherIndex;
					}
					numberOfElements = (numberOfElements + 1) / 2;
					shflAmount = numberOfElements / 2;
				}

				// Update treelet
				if (thread_warp_index == 0)
				{
					uint replace = treeletLeaves[warp_index * treeletSize + largestLeafIndex];
					uint left = radixTreeNodes[replace].field0.y;
					uint right = radixTreeNodes[replace].field0.z;
					float areaLeft = radixTreeNodes[left].field2.w;
					float areaRight = radixTreeNodes[right].field2.w;

					treeletInternalNodes[warp_index * (treeletSize - 1) + iteration + 1] = replace;
					treeletLeaves[warp_index * treeletSize + largestLeafIndex] = left;
					treeletLeaves[warp_index * treeletSize + iteration + 2] = right;
					treeletLeavesAreas[warp_index * treeletSize + largestLeafIndex] = areaLeft;
					treeletLeavesAreas[warp_index * treeletSize + iteration + 2] = areaRight;
				}

				WARP_SYNC;
			}

			// Load bounding boxes
			float bbMin[3], bbMax[3];
			if (thread_warp_index < treeletSize)
			{
				int index = WARP_ARRAY_INDEX(thread_warp_index, treeletSize);
				Bound bound;
				bound.pad0 = 0;
				bound.pad1 = 0;
				bound.min = radixTreeNodes[treeletLeaves[index]].field1.xyz;
				bound.max = radixTreeNodes[treeletLeaves[index]].field2.xyz;

				bbMin[0] = bound.min.x;
				bbMin[1] = bound.min.y;
				bbMin[2] = bound.min.z;

				bbMax[0] = bound.max.x;
				bbMax[1] = bound.max.y;
				bbMax[2] = bound.max.z;
			}

			int numberOfIterations = (scheduleSize + (WARP_SIZE - 1)) / WARP_SIZE;
			for (int j = 0; j < numberOfIterations; ++j)
			{
				int element = 0;
				int elementIndex = thread_warp_index + j * WARP_SIZE;
				if (elementIndex < scheduleSize)
				{
					element = schedule[elementIndex];
				}
				int a = element >> 24;
				int b = ((element >> 16) & 0xFF);

				// Read bounding boxes                    
				float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
				SHFL_FLOAT3(bbMinA, bbMin, a);
				SHFL_FLOAT3(bbMaxA, bbMax, a);
				SHFL_FLOAT3(bbMinB, bbMin, b);
				SHFL_FLOAT3(bbMaxB, bbMax, b);

				if (a != 0 || b != 0)
				{
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					distancesMatrix[LOWER_TRM_INDEX(a, b) + warp_index * distanceMatrixSize] = distance;
				}

				a = ((element >> 8) & 0xFF);
				b = (element & 0xFF);

				// Read bounding boxes
				SHFL_FLOAT3(bbMinA, bbMin, a);
				SHFL_FLOAT3(bbMaxA, bbMax, a);
				SHFL_FLOAT3(bbMinB, bbMin, b);
				SHFL_FLOAT3(bbMaxB, bbMax, b);

				if (a != 0 || b != 0)
				{
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					distancesMatrix[LOWER_TRM_INDEX(a, b) + warp_index * distanceMatrixSize] = distance;
				}
			}

			uint threadNode;
			float threadSah;
			if (thread_warp_index < treeletSize)
			{
				int index = WARP_ARRAY_INDEX(thread_warp_index, treeletSize);
				threadNode = treeletLeaves[index];
				threadSah = radixTreeNodes[threadNode].field1.w;
			}

			for (int lastRow = treeletSize - 1; lastRow > 0; --lastRow)
			{
				// Find pair with minimum distance                
				int minIndex = 0;
				float minDistance = FLT_MAX;
				int matrixSize = sumArithmeticSequence(lastRow, 1, lastRow);
				for (int j = thread_warp_index; j < matrixSize; j += WARP_SIZE)
				{
					float distance = distancesMatrix[j + warp_index * distanceMatrixSize];
					if (distance < minDistance)
					{
						minDistance = distance;
						minIndex = j;
					}
				}

				int numberOfElements = WARP_SIZE;
				int shflAmount = numberOfElements / 2;
				while (numberOfElements > 1)
				{
					int otherIndex = NvShflDown(minIndex, shflAmount);
					float otherArea = asfloat(NvShflDown(asuint(minDistance), shflAmount));

					if (otherArea < minDistance)
					{
						minDistance = otherArea;
						minIndex = otherIndex;
					}
					numberOfElements = (numberOfElements + 1) / 2;
					shflAmount = numberOfElements / 2;
				}

				minIndex = NvShfl(minIndex, 0);

				// Add modifications to a list
				int joinRow = LOWER_TRM_ROW(minIndex);
				int joinCol = LOWER_TRM_COL(minIndex);

				// Update 'joinCol' bounding box and update treelet. The left and right indices 
				// and the bounding boxes must be read outside the conditional or else __shfl is 
				// not going to work
				float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
				float sah = asfloat(NvShfl(asuint(threadSah), joinRow)) + asfloat(NvShfl(asuint(threadSah), joinCol));
				int leftIndex = NvShfl(threadNode, joinRow);
				int rightIndex = NvShfl(threadNode, joinCol);
				SHFL_FLOAT3(bbMinA, bbMin, joinRow);
				SHFL_FLOAT3(bbMaxA, bbMax, joinRow);
				SHFL_FLOAT3(bbMinB, bbMin, joinCol);
				SHFL_FLOAT3(bbMaxB, bbMax, joinCol);

				bbMinA.x = min(bbMinB.x, bbMinA.x);
				bbMinA.y = min(bbMinB.y, bbMinA.y);
				bbMinA.z = min(bbMinB.z, bbMinA.z);

				bbMaxA.x = max(bbMaxB.x, bbMaxA.x);
				bbMaxA.y = max(bbMaxB.y, bbMaxA.y);
				bbMaxA.z = max(bbMaxB.z, bbMaxA.z);

				if (thread_warp_index == joinCol)
				{
					threadNode = treeletInternalNodes[warp_index * (treeletSize - 1) + lastRow - 1];
					floatArrayFromFloat3(bbMinA, bbMin);
					floatArrayFromFloat3(bbMaxA, bbMax);
					float area = calculateBoundingBoxSurfaceArea(bbMinA, bbMaxA);
					treeletLeavesAreas[warp_index * treeletSize + lastRow] = sah + Ci * area;
					threadSah = treeletLeavesAreas[warp_index * treeletSize + lastRow];
				}

				// Update 'joinRow' node and bounding box. The last block only modified 'joinCol', 
				// which won't conflict with this block, so we can synchronize only once after 
				// both blocks
				int lastIndex = NvShfl(threadNode, lastRow);
				float sahLast = asfloat(NvShfl(asuint(threadSah), lastRow));
				SHFL_FLOAT3(bbMinB, bbMin, lastRow);
				SHFL_FLOAT3(bbMaxB, bbMax, lastRow);
				if (thread_warp_index == joinRow)
				{
					threadNode = lastIndex;
					threadSah = sahLast;
					floatArrayFromFloat3(bbMinB, bbMin);
					floatArrayFromFloat3(bbMaxB, bbMax);
				}

				// Update lastRow with the information required to update the treelet
				if (thread_warp_index == lastRow)
				{
					threadNode = leftIndex;
					treeletLeaves[warp_index * treeletSize + lastRow] = rightIndex;
					floatArrayFromFloat3(bbMinA, bbMin);
					floatArrayFromFloat3(bbMaxA, bbMax);
				}

				// Update distances matrix
				if (lastRow > 1)
				{
					// Copy last row to 'joinRow' row and columns
					int destinationRow = thread_warp_index;
					int destinationCol = destinationRow;
					if (thread_warp_index < lastRow && thread_warp_index != joinRow)
					{
						destinationRow = max(joinRow, destinationRow);
						destinationCol = min(joinRow, destinationCol);
						int indexSource = LOWER_TRM_INDEX(lastRow, thread_warp_index);
						float distance = distancesMatrix[distanceMatrixSize * warp_index + indexSource];
						int indexDestination = LOWER_TRM_INDEX(destinationRow, destinationCol);
						distancesMatrix[distanceMatrixSize * warp_index + indexDestination] = distance;
					}

					// Update row and column 'joinCol'
					destinationRow = thread_warp_index;
					destinationCol = destinationRow;
					if (thread_warp_index < lastRow && thread_warp_index != joinCol)
					{
						destinationRow = max(joinCol, destinationRow);
						destinationCol = min(joinCol, destinationCol);
					}
					float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
					SHFL_FLOAT3(bbMinA, bbMin, destinationRow);
					SHFL_FLOAT3(bbMaxA, bbMax, destinationRow);
					SHFL_FLOAT3(bbMinB, bbMin, destinationCol);
					SHFL_FLOAT3(bbMaxB, bbMax, destinationCol);
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					if (thread_warp_index < lastRow && thread_warp_index != joinCol)
					{
						int indexDestination = LOWER_TRM_INDEX(destinationRow, destinationCol);
						distancesMatrix[distanceMatrixSize * warp_index + indexDestination] = distance;
					}
				}
			}

			WARP_SYNC;

			if (treeletLeavesAreas[warp_index * treeletSize + 1] < radixTreeNodes[treeletInternalNodes[warp_index * (treeletSize - 1)]].field1.w)
			{
				if (thread_warp_index >= 1 && thread_warp_index < treeletSize)
				{
					int nodeIndex = treeletInternalNodes[warp_index * (treeletSize - 1) + thread_warp_index - 1];
					radixTreeNodes[nodeIndex].field0.y = threadNode;
					radixTreeNodes[nodeIndex].field0.z = treeletLeaves[warp_index * treeletSize + thread_warp_index];
					radixTreeNodes[threadNode].field0.w = nodeIndex;
					radixTreeNodes[treeletLeaves[warp_index * treeletSize + thread_warp_index]].field0.w = nodeIndex;
					radixTreeNodes[nodeIndex].field1.w = treeletLeavesAreas[warp_index * treeletSize + thread_warp_index];
					Bound bound;
					bound.min.x = bbMin[0];
					bound.min.y = bbMin[1];
					bound.min.z = bbMin[2];
					bound.max.x = bbMax[0];
					bound.max.y = bbMax[1];
					bound.max.z = bbMax[2];
					radixTreeNodes[nodeIndex].field1.xyz = bound.min;
					radixTreeNodes[nodeIndex].field2.xyz = bound.max;
					radixTreeNodes[nodeIndex].field2.w = calculateBoundingBoxSurfaceArea(bound.min, bound.max);
				}
			}

			// Update vote so each treelet is only processed once (set the bit that represents the 
			// treelet that will be processed back to 0)
			vote &= ~(1 << rootThreadIndex);

			WARP_SYNC;
		}

		// Update current node pointer
		if (currentNodeIndex != 0xFFFFFFFF)
		{
			currentNodeIndex = radixTreeNodes[currentNodeIndex].field0.w;
		}
	}
}