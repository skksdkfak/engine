#include "PathTracing.hlsl"

RWStructuredBuffer<Triangle>	triangles	: register(u0);
RWStructuredBuffer<Bound>		bounds		: register(u1);

cbuffer CustomUB: register(b0)
{
	Material materialCustomUB;
};

cbuffer MaterialIndex : register(b1)
{
	uint materialID;
};

struct GS_Input
{
	float4 vPosition	: SV_POSITION0;
	float3 vWorldPos	: POSITION0;
	float3 vNormal		: NORMAL0;
	float3 vBinormal	: BINORMAL0;
	float3 vTangent		: TANGENT0;
	float2 texCoord		: TEXCOORD0;
	float3 vWorldPosArray[3]: POSITION1;
	float3 vNormalArray[3]	: NORMAL1;
	float3 vBinormalArray[3]: BINORMAL1;
	float3 vTangentArray[3]	: TANGENT1;
	float2 texCoordArray[3]	: TEXCOORD1;
	uint primID : SV_PrimitiveID;
};

void main(GS_Input input)
{
	uint primitiveID = triangles.IncrementCounter(); //baseVertex + input.primID;
	
	Bound b;
	b.min = FLT_MAX;
	b.max = FLT_MIN;

	[unroll]
	for (uint i = 0; i < 3; i++)
	{
		triangles[primitiveID].position[i] = input.vWorldPosArray[i];
		triangles[primitiveID].normal[i] = input.vNormalArray[i];
		triangles[primitiveID].binormal[i] = input.vBinormalArray[i];
		triangles[primitiveID].tangent[i] = input.vTangentArray[i];
		triangles[primitiveID].texCoord[i] = input.texCoordArray[i];

		b.min = min(b.min, input.vWorldPosArray[i]);
		b.max = max(b.max, input.vWorldPosArray[i]);
	}
	triangles[primitiveID].materialID = materialID;
	triangles[primitiveID].tbn = float4x4(input.vTangent.x, input.vBinormal.x, input.vNormal.x, 1.0f,
		input.vTangent.y, input.vBinormal.y, input.vNormal.y, 1.0f,
		input.vTangent.z, input.vBinormal.z, input.vNormal.z, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f);
	
	bounds[primitiveID] = b;
}