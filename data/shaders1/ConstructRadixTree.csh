#include "globals.hlsl"
#include "PathTracing.hlsl"

#define UINT_MAX 0xFFFFFFFF

struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

struct RadixTreeDebugInfo
{
	int i;
	int d;
	int lmax;
	int l;
	int j;
	int s;
	int gamma;
};

StructuredBuffer<uint>				sortedKeys				: register(t0);
StructuredBuffer<uint>				sortedIndices			: register(t1);
RWStructuredBuffer<CompactTreeNode>	radixTree				: register(u0);
//RWStructuredBuffer<RadixTreeDebugInfo>	radixTreeDebugInfo		: register(CUSTOM1_UAV_BP);

cbuffer CustomUB: register(b0)
{
	uint numTriangles;
};

// From http://embeddedgurus.com/state-space/2014/09/fast-deterministic-and-portable-counting-leading-zeros/
//int clz(int x)
//{
//	x |= x >> 1;
//	// the next 5 statements will turn all bits after the
//	// leading zero's into a 1-bit
//	x |= x >> 2;
//	x |= x >> 4;
//	x |= x >> 8;
//	x |= x >> 16;
//	// compute the Hamming weight of x ... and return 32
//	// minus the computed result'
//	x -= ((x >> 1) & 0x55555555U);
//	x = (x & 0x33333333U) + ((x >> 2) & 0x33333333U);
//	return 32U - ((((x + (x >> 4)) & 0x0F0F0F0FU) * 0x01010101U) >> 24);
//}

//int clz(int x)
//{
//	return 31 - uint(log2(x));
//}

int clz(int x)
{
	return 31 - firstbithigh(x);
}

//#define clz(x) (31 - firstbithigh(x))

// Longest common prefix for morton code
int longestCommonPrefix(uint numberOfElements, int index1, int index2, uint key1)
{
	// No need to check the upper bound, since i+1 will be at most numberOfElements - 1 (one 
	// thread per internal node)
	if (index2 < 0 || index2 >= numberOfElements)
		return 0; 

	uint key2 = sortedKeys[index2];

	if (key1 == key2)
		return 32 + clz(index1 ^ index2);
	
	return clz(key1 ^ key2);
}

[numthreads(256, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const int i = dispatchThreadID.x;
	const int numberOfTriangles = numTriangles;
	const int leavesOffset = numberOfTriangles - 1;
	if (i >= leavesOffset)
		return;

	const uint key1 = sortedKeys[i];

	const int lcp1 = longestCommonPrefix(numberOfTriangles, i, i + 1, key1);
	const int lcp2 = longestCommonPrefix(numberOfTriangles, i, i - 1, key1);

	const int direction = sign(lcp1 - lcp2);

	// Compute upper bound for the length of the range
	const int minLcp = longestCommonPrefix(numberOfTriangles, i, i - direction, key1);
	int lMax = 128;
	while (longestCommonPrefix(numberOfTriangles, i, i + lMax * direction, key1) > minLcp)
	{
		lMax *= 4;
	}

	// Find other end using binary search
	int l = 0;
	int t = lMax;
	while (t > 1)
	{
		t = t / 2;
		if (longestCommonPrefix(numberOfTriangles, i, i + (l + t) * direction, key1) > minLcp)
		{
			l += t;
		}
	}
	const int j = i + l * direction;

	// Find the split position using binary search
	const int nodeLcp = longestCommonPrefix(numberOfTriangles, i, j, key1);
	int s = 0;
	int divisor = 2;
	t = l;
	const int maxDivisor = 1 << (32 - clz(l));
	while (divisor <= maxDivisor)
	{
		t = (l + divisor - 1) / divisor;
		if (longestCommonPrefix(numberOfTriangles, i, i + (s + t) * direction, key1) > nodeLcp)
		{
			s += t;
		}
		divisor *= 2;
	}
	const int splitPosition = i + s * direction + min(direction, 0);

	int leftIndex;
	int rightIndex;

	// Update left child pointer
	if (min(i, j) == splitPosition)
	{
		// Children is a leaf, add the number of internal nodes to the index
		int leafIndex = splitPosition + leavesOffset;
		leftIndex = leafIndex;

		// Set the leaf data index
		radixTree[leafIndex].field0.w = sortedIndices[splitPosition];
	}
	else
	{
		leftIndex = splitPosition;
	}

	// Update right child pointer
	if (max(i, j) == (splitPosition + 1))
	{
		// Children is a leaf, add the number of internal nodes to the index
		int leafIndex = splitPosition + 1 + leavesOffset;
		rightIndex = leafIndex;

		// Set the leaf data index
		radixTree[leafIndex].field0.w = sortedIndices[splitPosition + 1];
	}
	else
	{
		rightIndex = splitPosition + 1;
	}

	// Update children indices
	radixTree[i].field0.y = leftIndex;
	radixTree[i].field0.z = rightIndex;

	// Set parent nodes
	radixTree[leftIndex].field0.x = i;
	radixTree[rightIndex].field0.x = i;

	radixTree[i].field0.w = 0xFFFFFFFF;

	// Set the parent of the root node to 0xFFFFFFFF
	if (i == 0)
	{
		radixTree[0].field0.x = 0xFFFFFFFF;
	}
}