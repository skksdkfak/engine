@echo off

cd %CD%

for %%i in (*.glsl) do glslangValidator.exe %%i -V -o %%i.spv
for %%i in (*.vert) do glslangValidator.exe %%i -V -o %%i.spv
for %%i in (*.geom) do glslangValidator.exe %%i -V -o %%i.spv
for %%i in (*.frag) do glslangValidator.exe %%i -V -o %%i.spv
for %%i in (*.comp) do glslangValidator.exe %%i -V -o %%i.spv

pause
