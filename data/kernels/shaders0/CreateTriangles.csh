#include "globals.hlsl"
#include "PathTracing.hlsl"

StructuredBuffer<Vertex>		vertices	: register(t0);
RWStructuredBuffer<Triangle>	triangles	: register(u0);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint primitiveID = dispatchThreadID.x;
	if (primitiveID >= bvhUB.numTriangles)
		return;

	float3 normal = 0;
	float3 binormal = 0;
	float3 tangent = 0;

	uint vertexID;
	[unroll]
	for (uint i = 0; i < 3; i++)
	{
		vertexID = primitiveID * 3 + i;
		normal += vertices[vertexID].normal.xyz;
		binormal += vertices[vertexID].binormal.xyz;
		tangent += vertices[vertexID].tangent.xyz;
	}

	triangles[primitiveID].normal = normal = normalize(normal);
	triangles[primitiveID].binormal = binormal = normalize(binormal);
	triangles[primitiveID].tangent = tangent = normalize(tangent);
	triangles[primitiveID].tbn = matrix(
		tangent.x, binormal.x, normal.x, 1.0f,
		tangent.y, binormal.y, normal.y, 1.0f,
		tangent.z, binormal.z, normal.z, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f
		);
}