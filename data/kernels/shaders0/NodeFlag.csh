#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint4> fragList : register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint> octreeBuf : register(CUSTOM1_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

[numthreads(8, 8, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.y * 1024 + dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numVoxelFrag)
		return;

	uint3 umin, umax;
	uint4 loc;
	int childIdx = 0;
	uint node, subnode;
	uint voxelDim = voxelCustomUB.voxelizeDimension;
	bool bFlag = true;

	//Get the voxel coordinate of voxel loaded by this thread
	loc = fragList[thxId];

	//decide max and min coord for the root node
	umin = uint3(0, 0, 0);
	umax = uint3(voxelDim, voxelDim, voxelDim);

	node = octreeBuf[childIdx];

	for (int i = 0; i < octreeCustomUB.level; ++i)
	{
		voxelDim /= 2;
		if ((node & 0x80000000) == 0)
		{
			bFlag = false;
			break;
		}
		childIdx = int(node & 0x7FFFFFFF);  //mask out flag bit to get child idx

		subnode = clamp(int(1 + loc.x - umin.x - voxelDim), 0, 1);
		subnode += 4 * clamp(int(1 + loc.y - umin.y - voxelDim), 0, 1);
		subnode += 2 * clamp(int(1 + loc.z - umin.z - voxelDim), 0, 1);
		childIdx += int(subnode);

		umin.x += voxelDim * clamp(int(1 + loc.x - umin.x - voxelDim), 0, 1);
		umin.y += voxelDim * clamp(int(1 + loc.y - umin.y - voxelDim), 0, 1);
		umin.z += voxelDim * clamp(int(1 + loc.z - umin.z - voxelDim), 0, 1);

		node = octreeBuf[childIdx];
	}
	if (bFlag)
	{
		node |= 0x80000000; //set the most significant bit
		octreeBuf[childIdx] = node;
	}
}