#include "globals.hlsl"
#include "PathTracing.hlsl"

Texture3D<uint>						nodeGrid			: register(CUSTOM0_TEX_BP);

RWStructuredBuffer<uint4>			voxelPosList		: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint4>			voxelKdList			: register(CUSTOM1_UAV_BP);
RWStructuredBuffer<Triangle>		triangles			: register(CUSTOM2_UAV_BP);
RWStructuredBuffer<Ray>				raysBuffer			: register(CUSTOM3_UAV_BP);
RWStructuredBuffer<Hit>				hits				: register(CUSTOM4_UAV_BP);

GLOBAL_CAMERA_UB(cameraUB);

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		uint voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

[numthreads(8, 8, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint screenWidth = 1024;
	uint screenHeight = 720;
	if (dispatchThreadID.x >= screenWidth || dispatchThreadID.y >= screenHeight)
		return;

	const uint thxId = dispatchThreadID.x + (screenWidth * dispatchThreadID.y);

	Ray ray = raysBuffer[thxId];

	if (ray.direction.x == INFINITY)
		return;

	float3 offset = (ray.origin - voxelCustomUB.voxelizePosition.xyz);
	float3 voxelPos = (offset / voxelCustomUB.voxelSize + voxelCustomUB.voxelizeDimension / 2) / voxelCustomUB.voxelizeDimension;
	uint nodeIdx;
	uint curPrimIdx, primIdx;
	const float stepSize = 1.0f / voxelCustomUB.voxelizeDimension;
	float traceDistance = 0.0f;
	float maxDist = 1.0f;
	float3 samplePos = 0;
	float3 res = 0;
	float distance = INFINITY;
	float2 barycentricUV = 0;
	bool result = false;

	while (traceDistance <= maxDist && !result)
	{
		samplePos = ray.direction * traceDistance + voxelPos;
		nodeIdx = nodeGrid[samplePos * voxelCustomUB.voxelizeDimension];
		traceDistance += stepSize;
		res = 0;
		while (nodeIdx != 0)
		{
			curPrimIdx = voxelPosList[nodeIdx].w;
			nodeIdx = voxelKdList[nodeIdx].w;
			if (TriangleIntersect(ray, triangles[curPrimIdx].position, res) > 0.0f)
			{
				if (res.z < distance)
				{
					primIdx = curPrimIdx;
					barycentricUV = res.xy;
					distance = res.z;
					result = true;
				}
			}
		}
	}

	if (result)
	{
		//float2 texCoord = triangles[primIdx].texCoord[0] * (1.0f - uv.x - uv.y);
		//texCoord += triangles[primIdx].texCoord[1] * uv.x;
		//texCoord += triangles[primIdx].texCoord[2] * uv.y;
		//texCoord.y = 1.0f - texCoord.y;

		//float3 normal = triangles[primIdx].normal[0] * (1.0f - uv.x - uv.y);
		//normal += triangles[primIdx].normal[1] * uv.x;
		//normal += triangles[primIdx].normal[2] * uv.y;
		//normal = normalize(normal);

		//float3 binormal = triangles[primIdx].binormal[0] * (1.0f - uv.x - uv.y);
		//binormal += triangles[primIdx].binormal[1] * uv.x;
		//binormal += triangles[primIdx].binormal[2] * uv.y;
		//binormal = normalize(binormal);

		//float3 tangent = triangles[primIdx].tangent[0] * (1.0f - uv.x - uv.y);
		//tangent += triangles[primIdx].tangent[1] * uv.x;
		//tangent += triangles[primIdx].tangent[2] * uv.y;
		//tangent = normalize(tangent);

		Hit hit;
		hit.triangleID = primIdx;
		hit.distance = distance;
		hit.barycentricUV = barycentricUV;

		hits[thxId] = hit;
	}
	else
	{
		hits[thxId].distance = INFINITY;
	}
}