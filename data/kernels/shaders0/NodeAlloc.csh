#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint> octreeBuf : register(CUSTOM0_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numThread)
		return;

	//get child pointer
	uint childIdx = octreeBuf[octreeCustomUB.nodeOffset + thxId];

	if ((childIdx & 0x80000000) != 0) //need to allocate
	{
		uint offset;
		offset = octreeBuf.IncrementCounter();
		offset *= 8; //one tile has eight nodes
		offset += octreeCustomUB.allocOffset; //Add allocation offset 
		offset |= 0x80000000;    //Set the most significant bit
		octreeBuf[octreeCustomUB.nodeOffset + thxId] = offset;
	}
}