#include "globals.hlsl"

#define WG_COUNT 64
#define WG_SIZE 256
#define BLOCK_SIZE 1024  // (4 * WG_SIZE)
#define BITS_PER_PASS 4
#define RADICES 16       // (1 << BITS_PER_PASS)
#define RADICES_MASK 0xf // (RADICES - 1)
#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x

cbuffer SortCB : register(CUSTOM0_UB_BP)
{
	uint n;
	uint shift;
	bool descending;
	bool is_signed;
	bool key_index;
};

RWStructuredBuffer<uint> histogram : register(u0);
RWStructuredBuffer<uint> keyIn : register(u1);
RWStructuredBuffer<uint> keyOut : register(u2);
RWStructuredBuffer<uint> valueIn : register(u3);
RWStructuredBuffer<uint> valueOut : register(u4);

#define TO_MASK(n) ((1 << (n)) - 1)
#define BFE(src, s, n) ((src >> s) & TO_MASK(n))
#define BFE_SIGN(src, s, n)	(((((src >> s) & TO_MASK(n - 1)) ^ TO_MASK(n - 1)) & TO_MASK(n - 1)) | ((src >> s) &  (1 << (n - 1))))
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define GET_BY4(T, src, idx) T(src[idx.x], src[idx.y], src[idx.z], src[idx.w])
#define INC_BY4_CHECKED(dest, idx, flag) do { InterlockedAdd(dest[idx.x], uint(flag.x)); InterlockedAdd(dest[idx.y], uint(flag.y)); InterlockedAdd(dest[idx.z], uint(flag.z)); InterlockedAdd(dest[idx.w], uint(flag.w)); } while (false)

uint4 lessThan(in uint4 x, in uint4 y)
{
	uint4 lessThan;
	lessThan.x = x.x < y.x;
	lessThan.y = x.y < y.y;
	lessThan.z = x.z < y.z;
	lessThan.w = x.w < y.w;

	return lessThan;
}

struct BlocksInfo
{
	uint count;
	uint offset;
};

BlocksInfo GetBlocksInfo(const uint n, const uint wg_idx)
{
	BlocksInfo blocksInfo;
	const uint aligned = n + BLOCK_SIZE - (n % BLOCK_SIZE);
	const uint blocks = (n + BLOCK_SIZE - 1) / BLOCK_SIZE;
	const uint blocks_per_wg = (blocks + WG_COUNT - 1) / WG_COUNT;
	const int n_blocks = int(aligned / BLOCK_SIZE) - int(blocks_per_wg * wg_idx);
	blocksInfo.count = uint(clamp(n_blocks, 0, int(blocks_per_wg)));
	blocksInfo.offset = blocks_per_wg * BLOCK_SIZE * wg_idx;
	return blocksInfo;
}

groupshared uint local_histogram[WG_SIZE * RADICES];

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	uint gIdx = GET_GLOBAL_IDX;
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;

	for (int i = 0; i < RADICES; i++)
		local_histogram[i * WG_SIZE + lIdx] = 0;
	GroupMemoryBarrierWithGroupSync();

	const BlocksInfo blocks = GetBlocksInfo(n, wgIdx);
	uint4 addr = blocks.offset + 4 * lIdx + uint4(0, 1, 2, 3);
	for (int i = 0; i < blocks.count; i++)
	{
		const uint4 less_than = lessThan(addr, n);
		//const uint4 less_than = 1 - step(addr, n); // WARNING
		const uint4 data_vec = GET_BY4(uint4, keyIn, addr);
		const uint4 k = is_signed
			? BFE_SIGN(data_vec, shift, BITS_PER_PASS)
			: BFE(data_vec, shift, BITS_PER_PASS);
		const uint4 key = descending != is_signed ? (RADICES_MASK - k) : k;
		const uint4 local_key = key * WG_SIZE + lIdx;
		INC_BY4_CHECKED(local_histogram, local_key, less_than);
		addr += BLOCK_SIZE;
	}
	GroupMemoryBarrierWithGroupSync();

	if (lIdx < RADICES)
	{
		uint sum = 0;
		for (int i = 0; i < WG_SIZE; i++)
			sum += local_histogram[lIdx * WG_SIZE + i];
		histogram[lIdx * WG_COUNT + wgIdx] = sum;
	}
	GroupMemoryBarrierWithGroupSync();
}