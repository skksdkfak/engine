#include "globals.hlsl"
#include "PathTracing.hlsl"

#define NV_SHADER_EXTN_SLOT u7
#define NV_SHADER_EXTN_REGISTER_SPACE space0

#include "nvHLSLExtns.h"

#define treeletSize 21
#define distanceMatrixSize 210
#define WG_SIZE 128
#define WARP_SIZE 32

struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

StructuredBuffer<uint>							schedule				: register(t0);
RWStructuredBuffer<CompactTreeNode>				radixTreeNodes			: register(u0);
RWStructuredBuffer<uint>						nodeCounter				: register(u1);
RWStructuredBuffer<uint>						subtreeTriangles		: register(u2);
RWStructuredBuffer<float>						distancesMatrix			: register(u3);

cbuffer CustomUB : register(b0)
{
	uint numTriangles;
	uint gamma;
	uint scheduleSize;
};

#define WARP_ARRAY_INDEX(index, elementsPerWarp) (warp_index * (elementsPerWarp) + (index))

// Get the row number an element in a lower triangular matrix represented as an array. 
// The main diagonal elements are not included in the array.
#define LOWER_TRM_ROW(index) rowIndex((index))
uint rowIndex(uint index)
{
	return (int)((-1 + sqrt(8.0f * index + 1)) / 2) + 1;
}

// Get the column number an element in a lower triangular matrix represented as an array. 
// The main diagonal elements are not included in the array.
#define LOWER_TRM_COL(index) columnIndex((index))
uint columnIndex(uint index)
{
	uint y = rowIndex(index);
	return (index)-(y * (y - 1)) / 2;
}

// Get the number of elements which can be stored in a diagonal square matrix of size 'dim,
// excluding the main diagonal.
#define TRM_SIZE(dim) (((dim - 1) * (dim)) / 2)

// Get the index of an element in an array that represents a lower triangular matrix.
// The main diagonal elements are not included in the array.
#define LOWER_TRM_INDEX(row, column) \
        (TRM_SIZE((row)) + (column))

#define WARP_SYNC \
do { \
    int _sync = 0; \
    NvShfl(_sync, 0); \
} while (0)

#define SHFL_FLOAT3(destination, source, index) \
do { \
    (destination).x = asfloat(NvShfl(asuint((source)[0]), (index))); \
    (destination).y = asfloat(NvShfl(asuint((source)[1]), (index))); \
    (destination).z = asfloat(NvShfl(asuint((source)[2]), (index))); \
} while (0);

bool isInternalNode(uint index, uint numberOfTriangles)
{
	return (index < numberOfTriangles - 1);
}

bool isLeaf(uint index, uint numberOfTriangles)
{
	return !isInternalNode(index, numberOfTriangles);
}

uint sumArithmeticSequence(uint numberOfElements, uint firstElement, uint lastElement)
{
	return numberOfElements * (firstElement + lastElement) / 2;
}

void floatArrayFromFloat3(float3 source, inout float destination[3])
{
	destination[0] = source.x;
	destination[1] = source.y;
	destination[2] = source.z;
}

uint ffs(uint n)
{
	return log2(n&-n) + 1;
}

float calculateBoundingBoxAndSurfaceArea(const float3 bbMin1, const float3 bbMax1, const float3 bbMin2, const float3 bbMax2)
{
	float3 size = max(bbMax1, bbMax2) - min(bbMin1, bbMin2);
	return 2 * (size.x * size.y + size.x * size.z + size.y * size.z);
}

float calculateBoundingBoxSurfaceArea(float3 bbMin, float3 bbMax)
{
	float3 size = bbMax - bbMin;
	return 2 * (size.x * size.y + size.x * size.z + size.y * size.z);
}

groupshared uint treeletInternalNodes[(treeletSize - 1) * (WG_SIZE / WARP_SIZE)];
groupshared uint treeletLeaves[treeletSize * (WG_SIZE / WARP_SIZE)];
groupshared float treeletLeavesAreas[treeletSize * (WG_SIZE / WARP_SIZE)];

[numthreads(128, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	const uint threadIndex = dispatchThreadID.x;
	const uint global_warp_index = threadIndex / WARP_SIZE;
	const uint warp_index = groupThreadID.x / WARP_SIZE;
	const uint thread_warp_index = groupThreadID.x & (WARP_SIZE - 1);
	const uint numberOfTriangles = numTriangles;

	// If this flag is set, the thread will be excluded from the bottom up traversal, but will 
	// still be available to help form and optimize treelets
	uint currentNodeIndex;
	if (threadIndex < numberOfTriangles)
	{
		uint leafIndex = threadIndex + numberOfTriangles - 1;
		subtreeTriangles[leafIndex] = 1;
		currentNodeIndex = radixTreeNodes[leafIndex].field0.x;
		float area = radixTreeNodes[leafIndex].field2.w;
		radixTreeNodes[leafIndex].field1.w = Ct * area;
	}
	else
	{
		currentNodeIndex = 0xFFFFFFFF;
	}
	
	while (NvBallot(currentNodeIndex != 0xFFFFFFFF) != 0)
	{
		// Number of threads who already have processed the current node
		uint counter = 0;

		if (currentNodeIndex != 0xFFFFFFFF)
		{
			InterlockedAdd(nodeCounter[currentNodeIndex], 1, counter);

			// Only the last thread to arrive is allowed to process the current node. This ensures 
			// that both its children will already have been processed
			if (counter == 0)
			{
				currentNodeIndex = 0xFFFFFFFF;
			}
		}

		// How many triangles can be reached by the subtree with root at the current node
		uint triangleCount = 0;
		if (counter != 0)
		{
			// Throughout the code, blocks that have loads separated from stores are so organized 
			// in order to increase ILP (Instruction level parallelism)
			uint left = radixTreeNodes[currentNodeIndex].field0.y;
			uint right = radixTreeNodes[currentNodeIndex].field0.z;
			float area = radixTreeNodes[currentNodeIndex].field2.w;
			uint trianglesLeft = subtreeTriangles[left];
			float sahLeft = radixTreeNodes[left].field1.w;
			uint trianglesRight = subtreeTriangles[right];
			float sahRight = radixTreeNodes[right].field1.w;

			triangleCount = trianglesLeft + trianglesRight;
			subtreeTriangles[currentNodeIndex] = triangleCount;
			radixTreeNodes[currentNodeIndex].field1.w = Ci * area + sahLeft + sahRight;
		}

		// Check which threads in the warp have treelets to be processed. We are only going to 
		// process a treelet if the current node is the root of a subtree with at least gamma 
		// triangles
		uint vote = NvBallot(triangleCount >= gamma);

		while (vote != 0)
		{
			// Get the thread index for the treelet that will be processed
			uint rootThreadIndex = ffs(vote) - 1;

			// Get the treelet root by reading the corresponding thread's currentNodeIndex private 
			// variable
			uint treeletRootIndex = NvShfl(currentNodeIndex, rootThreadIndex);

			// Initialize treelet
			uint left = radixTreeNodes[treeletRootIndex].field0.y;
			uint right = radixTreeNodes[treeletRootIndex].field0.z;
			float areaLeft = radixTreeNodes[left].field2.w;
			float areaRight = radixTreeNodes[right].field2.w;
			if (thread_warp_index == 0)
			{
				treeletInternalNodes[warp_index * (treeletSize - 1)] = treeletRootIndex;
				treeletLeaves[warp_index * treeletSize] = left;
				treeletLeaves[warp_index * treeletSize + 1] = right;
				treeletLeavesAreas[warp_index * treeletSize] = areaLeft;
				treeletLeavesAreas[warp_index * treeletSize + 1] = areaRight;
			}

			WARP_SYNC;

			// Find the treelet's internal nodes. On each iteration we choose the leaf with 
			// largest surface area and add move it to the list of internal nodes, adding its
			// two children as leaves in its place
			for (uint iteration = 0; iteration < treeletSize - 2; ++iteration)
			{
				// Choose leaf with the largest area
				uint largestLeafIndex;
				float largestLeafArea;
				if (thread_warp_index < 2 + iteration)
				{
					largestLeafIndex = thread_warp_index;
					largestLeafArea = treeletLeavesAreas[warp_index * treeletSize + thread_warp_index];
					if (isLeaf(treeletLeaves[warp_index * treeletSize + largestLeafIndex], numberOfTriangles))
					{
						largestLeafArea = -FLT_MAX;
					}
				}

				uint numberOfElements = 2 + iteration;
				uint shflAmount = numberOfElements / 2;
				while (numberOfElements > 1)
				{
					uint otherIndex = NvShflDown(largestLeafIndex, shflAmount);
					float otherArea = asfloat(NvShflDown(asuint(largestLeafArea), shflAmount));

					if (otherArea > largestLeafArea)
					{
						largestLeafArea = otherArea;
						largestLeafIndex = otherIndex;
					}
					numberOfElements = (numberOfElements + 1) / 2;
					shflAmount = numberOfElements / 2;
				}

				// Update treelet
				if (thread_warp_index == 0)
				{
					uint replace = treeletLeaves[warp_index * treeletSize + largestLeafIndex];
					uint left = radixTreeNodes[replace].field0.y;
					uint right = radixTreeNodes[replace].field0.z;
					float areaLeft = radixTreeNodes[left].field2.w;
					float areaRight = radixTreeNodes[right].field2.w;

					treeletInternalNodes[warp_index * (treeletSize - 1) + iteration + 1] = replace;
					treeletLeaves[warp_index * treeletSize + largestLeafIndex] = left;
					treeletLeaves[warp_index * treeletSize + iteration + 2] = right;
					treeletLeavesAreas[warp_index * treeletSize + largestLeafIndex] = areaLeft;
					treeletLeavesAreas[warp_index * treeletSize + iteration + 2] = areaRight;
				}

				WARP_SYNC;
			}

			// Load bounding boxes
			float bbMin[3], bbMax[3];
			if (thread_warp_index < treeletSize)
			{
				uint index = WARP_ARRAY_INDEX(thread_warp_index, treeletSize);
				float3 boundMin = radixTreeNodes[treeletLeaves[index]].field1.xyz;
				float3 boundMax = radixTreeNodes[treeletLeaves[index]].field2.xyz;

				bbMin[0] = boundMin.x;
				bbMin[1] = boundMin.y;
				bbMin[2] = boundMin.z;

				bbMax[0] = boundMax.x;
				bbMax[1] = boundMax.y;
				bbMax[2] = boundMax.z;
			}

			uint numberOfIterations = (scheduleSize + (WARP_SIZE - 1)) / WARP_SIZE;
			for (uint j = 0; j < numberOfIterations; ++j)
			{
				uint element = 0;
				uint elementIndex = thread_warp_index + j * WARP_SIZE;
				if (elementIndex < scheduleSize)
				{
					element = schedule[elementIndex];
				}
				uint a = element >> 24;
				uint b = ((element >> 16) & 0xFF);

				// Read bounding boxes                    
				float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
				SHFL_FLOAT3(bbMinA, bbMin, a);
				SHFL_FLOAT3(bbMaxA, bbMax, a);
				SHFL_FLOAT3(bbMinB, bbMin, b);
				SHFL_FLOAT3(bbMaxB, bbMax, b);

				if (a != 0 || b != 0)
				{
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					distancesMatrix[LOWER_TRM_INDEX(a, b) + global_warp_index * distanceMatrixSize] = distance;
				}

				a = ((element >> 8) & 0xFF);
				b = (element & 0xFF);

				// Read bounding boxes
				SHFL_FLOAT3(bbMinA, bbMin, a);
				SHFL_FLOAT3(bbMaxA, bbMax, a);
				SHFL_FLOAT3(bbMinB, bbMin, b);
				SHFL_FLOAT3(bbMaxB, bbMax, b);

				if (a != 0 || b != 0)
				{
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					distancesMatrix[LOWER_TRM_INDEX(a, b) + global_warp_index * distanceMatrixSize] = distance;
				}
			}

			uint threadNode;
			float threadSah;
			if (thread_warp_index < treeletSize)
			{
				uint index = WARP_ARRAY_INDEX(thread_warp_index, treeletSize);
				threadNode = treeletLeaves[index];
				threadSah = radixTreeNodes[threadNode].field1.w;
			}

			for (uint lastRow = treeletSize - 1; lastRow > 0; --lastRow)
			{
				// Find pair with minimum distance                
				uint minIndex = 0;
				float minDistance = FLT_MAX;
				uint matrixSize = sumArithmeticSequence(lastRow, 1, lastRow);
				for (uint j = thread_warp_index; j < matrixSize; j += WARP_SIZE)
				{
					float distance = distancesMatrix[j + global_warp_index * distanceMatrixSize];
					if (distance < minDistance)
					{
						minDistance = distance;
						minIndex = j;
					}
				}

				uint numberOfElements = WARP_SIZE;
				uint shflAmount = numberOfElements / 2;
				while (numberOfElements > 1)
				{
					uint otherIndex = NvShflDown(minIndex, shflAmount);
					float otherArea = asfloat(NvShflDown(asuint(minDistance), shflAmount));

					if (otherArea < minDistance)
					{
						minDistance = otherArea;
						minIndex = otherIndex;
					}
					numberOfElements = (numberOfElements + 1) / 2;
					shflAmount = numberOfElements / 2;
				}

				minIndex = NvShfl(minIndex, 0);

				// Update treelet
				uint joinRow = LOWER_TRM_ROW(minIndex);
				uint joinCol = LOWER_TRM_COL(minIndex);

				// Copy last row to 'joinRow' row and columns
				if (thread_warp_index < lastRow && thread_warp_index != joinRow && lastRow > 1)
				{
					uint destinationRow = max(joinRow, thread_warp_index);
					uint destinationCol = min(joinRow, thread_warp_index);
					uint indexSource = distanceMatrixSize * global_warp_index + LOWER_TRM_INDEX(lastRow, thread_warp_index);
					float distance = distancesMatrix[indexSource];
					uint indexDestination = distanceMatrixSize * global_warp_index + LOWER_TRM_INDEX(destinationRow, destinationCol);
					distancesMatrix[indexDestination] = distance;
				}

				// Update 'joinCol' bounding box and update treelet. The left and right indices 
				// and the bounding boxes must be read outside the conditional or else __shfl is 
				// not going to work
				float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
				float sah = asfloat(NvShfl(asuint(threadSah), joinRow)) + asfloat(NvShfl(asuint(threadSah), joinCol));
				uint leftIndex = NvShfl(threadNode, joinRow);
				uint rightIndex = NvShfl(threadNode, joinCol);
				SHFL_FLOAT3(bbMinA, bbMin, joinRow);
				SHFL_FLOAT3(bbMaxA, bbMax, joinRow);
				SHFL_FLOAT3(bbMinB, bbMin, joinCol);
				SHFL_FLOAT3(bbMaxB, bbMax, joinCol);

				bbMinA.x = min(bbMinB.x, bbMinA.x);
				bbMinA.y = min(bbMinB.y, bbMinA.y);
				bbMinA.z = min(bbMinB.z, bbMinA.z);

				bbMaxA.x = max(bbMaxB.x, bbMaxA.x);
				bbMaxA.y = max(bbMaxB.y, bbMaxA.y);
				bbMaxA.z = max(bbMaxB.z, bbMaxA.z);

				if (thread_warp_index == joinCol)
				{
					threadNode = treeletInternalNodes[warp_index * (treeletSize - 1) + lastRow - 1];
					floatArrayFromFloat3(bbMinA, bbMin);
					floatArrayFromFloat3(bbMaxA, bbMax);
					float area = calculateBoundingBoxSurfaceArea(bbMinA, bbMaxA);
					treeletLeavesAreas[warp_index * treeletSize + lastRow] = sah + Ci * area;
					threadSah = treeletLeavesAreas[warp_index * treeletSize + lastRow];
				}

				// Update 'joinRow' node and bounding box. The last block only modified 'joinCol', 
				// which won't conflict with this block, so we can synchronize only once after 
				// both blocks
				uint lastIndex = NvShfl(threadNode, lastRow);
				float sahLast = asfloat(NvShfl(asuint(threadSah), lastRow));
				SHFL_FLOAT3(bbMinB, bbMin, lastRow);
				SHFL_FLOAT3(bbMaxB, bbMax, lastRow);
				if (thread_warp_index == joinRow)
				{
					threadNode = lastIndex;
					threadSah = sahLast;
					floatArrayFromFloat3(bbMinB, bbMin);
					floatArrayFromFloat3(bbMaxB, bbMax);
				}

				// Update lastRow with the information required to update the treelet
				if (thread_warp_index == lastRow)
				{
					threadNode = leftIndex;
					treeletLeaves[warp_index * treeletSize + lastRow] = rightIndex;
					floatArrayFromFloat3(bbMinA, bbMin);
					floatArrayFromFloat3(bbMaxA, bbMax);
				}

				// Update row and column 'joinCol'
				if (lastRow > 1)
				{
					// Copy last row to 'joinRow' row and columns
					uint destinationRow = thread_warp_index;
					uint destinationCol = destinationRow;
					if (thread_warp_index < lastRow && thread_warp_index != joinRow)
					{
						destinationRow = max(joinRow, destinationRow);
						destinationCol = min(joinRow, destinationCol);
						uint indexSource = LOWER_TRM_INDEX(lastRow, thread_warp_index);
						float distance = distancesMatrix[distanceMatrixSize * global_warp_index + indexSource];
						uint indexDestination = LOWER_TRM_INDEX(destinationRow, destinationCol);
						distancesMatrix[distanceMatrixSize * global_warp_index + indexDestination] = distance;
					}

					// Update row and column 'joinCol'
					destinationRow = thread_warp_index;
					destinationCol = destinationRow;
					if (thread_warp_index < lastRow && thread_warp_index != joinCol)
					{
						destinationRow = max(joinCol, destinationRow);
						destinationCol = min(joinCol, destinationCol);
					}
					float3 bbMinA, bbMaxA, bbMinB, bbMaxB;
					SHFL_FLOAT3(bbMinA, bbMin, destinationRow);
					SHFL_FLOAT3(bbMaxA, bbMax, destinationRow);
					SHFL_FLOAT3(bbMinB, bbMin, destinationCol);
					SHFL_FLOAT3(bbMaxB, bbMax, destinationCol);
					float distance = calculateBoundingBoxAndSurfaceArea(bbMinA, bbMaxA, bbMinB, bbMaxB);
					if (thread_warp_index < lastRow && thread_warp_index != joinCol)
					{
						uint indexDestination = LOWER_TRM_INDEX(destinationRow, destinationCol);
						distancesMatrix[distanceMatrixSize * global_warp_index + indexDestination] = distance;
					}
				}
			}

			WARP_SYNC;

			if (treeletLeavesAreas[warp_index * treeletSize + 1] < radixTreeNodes[treeletInternalNodes[warp_index * (treeletSize - 1)]].field1.w)
			{
				if (thread_warp_index >= 1 && thread_warp_index < treeletSize)
				{
					uint nodeIndex = treeletInternalNodes[warp_index * (treeletSize - 1) + thread_warp_index - 1];
					radixTreeNodes[nodeIndex].field0.y = threadNode;
					radixTreeNodes[nodeIndex].field0.z = treeletLeaves[warp_index * treeletSize + thread_warp_index];
					radixTreeNodes[threadNode].field0.x = nodeIndex;
					radixTreeNodes[treeletLeaves[warp_index * treeletSize + thread_warp_index]].field0.x = nodeIndex;
					radixTreeNodes[nodeIndex].field1.w = treeletLeavesAreas[warp_index * treeletSize + thread_warp_index];
					float3 bbMin3 = float3(bbMin[0], bbMin[1], bbMin[2]);
					float3 bbMax3 = float3(bbMax[0], bbMax[1], bbMax[2]);
					radixTreeNodes[nodeIndex].field1.xyz = bbMin3;
					radixTreeNodes[nodeIndex].field2.xyz = bbMax3;
					radixTreeNodes[nodeIndex].field2.w = calculateBoundingBoxSurfaceArea(bbMin3, bbMax3);
				}
			}

			// Update vote so each treelet is only processed once (set the bit that represents the 
			// treelet that will be processed back to 0)
			vote &= ~(1 << rootThreadIndex);

			WARP_SYNC;
		}

		// Update current node pointer
		if (currentNodeIndex != 0xFFFFFFFF)
		{
			currentNodeIndex = radixTreeNodes[currentNodeIndex].field0.x;
		}
	}
}