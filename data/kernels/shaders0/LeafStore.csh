#include "globals.hlsl"
#include "globalIllum.shi"

RWStructuredBuffer<uint>	octreeIdx		: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint>	octreeKd		: register(CUSTOM1_UAV_BP);
RWStructuredBuffer<uint4>	voxelPosList	: register(CUSTOM2_UAV_BP);
RWStructuredBuffer<uint4>	voxelKdList		: register(CUSTOM3_UAV_BP);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numVoxelFrag;
		uint level;
		int numThread;
		int nodeOffset;
		int allocOffset;
	}octreeCustomUB;
};

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

float4 convRGBA8ToFloat4(in uint val);
uint convFloat4ToRGBA8(in float4 val);
void imageAtomicRGBA8Avg(float4 val, int coord, RWStructuredBuffer<uint> buf);

[numthreads(8, 8, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.y * 1024 + dispatchThreadID.x;
	if (thxId >= octreeCustomUB.numVoxelFrag)
		return;

	uint4 loc = voxelPosList[thxId];
	int childIdx = 0;
	uint node;
	bool bFlag = true;

	uint voxelDim = voxelCustomUB.voxelizeDimension;
	uint3 umin = uint3(0, 0, 0);

	node = octreeIdx[childIdx];

	for (int i = 0; i < octreeCustomUB.level; ++i)
	{
		voxelDim /= 2;
		if ((node & 0x80000000) == 0)
		{
			bFlag = false;
			break;
		}
		childIdx = int(node & 0x7FFFFFFF);  //mask out flag bit to get child idx

		if ((loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
			(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
			(loc.z >= umin.z && loc.z < umin.z + voxelDim)
			)
		{

		}
		else if (
			(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
			(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
			(loc.z >= umin.z && loc.z < umin.z + voxelDim)
			)
		{
			childIdx += 1;
			umin.x = umin.x + voxelDim;
		}
		else if (
			(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
			(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
			(loc.z >= umin.z + voxelDim && loc.z < umin.z + 2 * voxelDim)
			)
		{
			childIdx += 2;
			umin.z += voxelDim;
		}
		else if (
			(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
			(loc.y >= umin.y && loc.y < umin.y + voxelDim) &&
			(loc.z >= umin.z + voxelDim && loc.z < umin.z + 2 * voxelDim)
			)
		{
			childIdx += 3;
			umin.x += voxelDim;
			umin.z += voxelDim;
		}
		else if (
			(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
			(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
			(loc.z >= umin.z && loc.z < umin.z + voxelDim)
			)
		{
			childIdx += 4;
			umin.y += voxelDim;

		}
		else if (
			(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
			(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
			(loc.z >= umin.z && loc.z < umin.z + voxelDim)
			)
		{
			childIdx += 5;
			umin.x += voxelDim;
			umin.y += voxelDim;
		}
		else if (
			(loc.x >= umin.x && loc.x < umin.x + voxelDim) &&
			(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
			(loc.z >= umin.z + voxelDim && loc.z < umin.z + voxelDim * 2)
			)
		{
			childIdx += 6;
			umin.z += voxelDim;
			umin.y += voxelDim;
		}
		else if (
			(loc.x >= umin.x + voxelDim && loc.x < umin.x + 2 * voxelDim) &&
			(loc.y >= umin.y + voxelDim && loc.y < umin.y + 2 * voxelDim) &&
			(loc.z >= umin.z + voxelDim && loc.z < umin.z + voxelDim * 2)
			)
		{
			childIdx += 7;
			umin += voxelDim;
		}
		else
		{
			bFlag = false;
			break;
		}
		node = octreeIdx[childIdx];
	}

	float4 color = voxelKdList[thxId];

	//Use a atomic running average method to prevent buffer saturation
	//From OpenGL Insight ch. 22
	imageAtomicRGBA8Avg(color, childIdx, octreeKd);
}

//UINT atomic running average method
//From OpenGL Insight ch. 22
float4 convRGBA8ToFloat4(in uint val)
{
	return float4(float((val & 0x000000FF)), float((val & 0x0000FF00) >> 8U),
		float((val & 0x00FF0000) >> 16U), float((val & 0xFF000000) >> 24U));
}

uint convFloat4ToRGBA8(in float4 val)
{
	return (uint(val.w) & 0x000000FF) << 24U | (uint(val.z) & 0x000000FF) << 16U | (uint(val.y) & 0x000000FF) << 8U | (uint(val.x) & 0x000000FF);
}

void imageAtomicRGBA8Avg(float4 val, int coord, RWStructuredBuffer<uint> buf)
{
	uint newVal = convFloat4ToRGBA8(val);
	uint prev = 0;
	uint cur;
	[allow_uav_condition] while (true)
	{
		InterlockedCompareExchange(buf[coord], prev, newVal, cur);
		if (cur == prev)
			break;
		prev = cur;
		float4 rval = convRGBA8ToFloat4(cur);
		rval.xyz = rval.xyz*rval.w;
		float4 curVal = rval + val;
		curVal.xyz /= curVal.w;
		newVal = convFloat4ToRGBA8(curVal);
	}
}