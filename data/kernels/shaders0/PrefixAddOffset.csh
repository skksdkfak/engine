#include "globals.hlsl"

#define WG_SIZE 128
#define GET_GROUP_IDX groupIdx.x
#define GET_GROUP_SIZE WG_SIZE
#define GET_LOCAL_IDX localIdx.x

RWStructuredBuffer<uint> dst : register(u0);
RWStructuredBuffer<uint> blockSum2 : register(u1);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	int m_numElems;
	int m_numBlocks;
	int m_numScanBlocks;
};

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID)
{
	const uint blockSize = WG_SIZE * 2;

	int myIdx = GET_GROUP_IDX + 1;
	int llIdx = GET_LOCAL_IDX;

	uint iBlockSum = blockSum2[myIdx];

	int endValue = min((myIdx + 1)*(blockSize), m_numElems);
	for (int i = myIdx*blockSize + llIdx; i<endValue; i += GET_GROUP_SIZE)
	{
		dst[i] += iBlockSum;
	}
}