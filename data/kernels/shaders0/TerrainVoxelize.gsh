#include "globals.hlsl"

GLOBAL_CAMERA_UB(cameraUB);

cbuffer CustomUB: register(CUSTOM1_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}voxelCustomUB;
};

struct GS_INPUT
{
	float4 vPosition				: SV_Position;
	centroid float2 texCoord		: TEXCOORD0;
	centroid float3 vNormal			: NORMAL;
	centroid float3 vPositionWS		: TEXCOORD1;
	centroid float4 layerdef		: TEXCOORD2;
	centroid float4 depthmap_scaler	: TEXCOORD3;
	uint RTIndex					: RTARRAYINDEX;
};

struct GS_OUTPUT
{
	float4 vPosition				: SV_Position;
	centroid float2 texCoord		: TEXCOORD0;
	centroid float3 vNormal			: NORMAL;
	centroid float3 vPositionWS		: TEXCOORD1;
	centroid float4 layerdef		: TEXCOORD2;
	centroid float4 depthmap_scaler	: TEXCOORD3;
	uint RTIndex					: SV_RenderTargetArrayIndex;
};

float4 TransformPos(float4 In)
{
	return float4((In.xyz - voxelCustomUB.voxelizePosition.xyz + voxelCustomUB.voxelizeSize / 2) / voxelCustomUB.voxelizeSize, In.w);
}
float4 TransformPosition(float2 In)
{
	return float4((In)* 2 - 1, 0, 1);
}

[maxvertexcount(3)]
void main(triangle GS_INPUT input[3], inout TriangleStream<GS_OUTPUT> outputStream)
{
	GS_OUTPUT output[3];

	float4 vLastPos[3];

	vLastPos[0] = TransformPos(float4(input[0].vPositionWS.xyz, 0));
	vLastPos[1] = TransformPos(float4(input[1].vPositionWS.xyz, 0));
	vLastPos[2] = TransformPos(float4(input[2].vPositionWS.xyz, 0));

	float3 Delta1 = vLastPos[0].xyz - vLastPos[1].xyz;
	float3 Delta2 = vLastPos[0].xyz - vLastPos[2].xyz;

	float S1 = length(cross(float3(Delta1.xy, 0), float3(Delta2.xy, 0)));
	float S2 = length(cross(float3(Delta1.xz, 0), float3(Delta2.xz, 0)));
	float S3 = length(cross(float3(Delta1.yz, 0), float3(Delta2.yz, 0)));

	if (S1 > S2)
	{
		if (S1 > S3)
		{
			output[0].vPosition = TransformPosition(vLastPos[0].xy);
			output[1].vPosition = TransformPosition(vLastPos[1].xy);
			output[2].vPosition = TransformPosition(vLastPos[2].xy);
		}
		else
		{
			output[0].vPosition = TransformPosition(vLastPos[0].yz);
			output[1].vPosition = TransformPosition(vLastPos[1].yz);
			output[2].vPosition = TransformPosition(vLastPos[2].yz);
		}
	}
	else
	{
		if (S2 > S3)
		{
			output[0].vPosition = TransformPosition(vLastPos[0].xz);
			output[1].vPosition = TransformPosition(vLastPos[1].xz);
			output[2].vPosition = TransformPosition(vLastPos[2].xz);
		}
		else
		{
			output[0].vPosition = TransformPosition(vLastPos[0].yz);
			output[1].vPosition = TransformPosition(vLastPos[1].yz);
			output[2].vPosition = TransformPosition(vLastPos[2].yz);
		}
	}

	[unroll]
	for (uint i = 0; i < 3; i++)
	{
		output[i].vPositionWS = input[i].vPositionWS;
		output[i].vNormal = input[i].vNormal;
		output[i].layerdef = input[i].layerdef;
		output[i].depthmap_scaler = input[i].depthmap_scaler;
		output[i].texCoord = input[i].texCoord;
		output[i].RTIndex = input[i].RTIndex;
	}

	// Bloat triangle in normalized device space with the texel size of the currently bound 
	// render-target. In this way pixels, which would have been discarded due to the low 
	// resolution of the currently bound render-target, will still be rasterized. 
	float2 side0N = normalize(output[1].vPosition.xy - output[0].vPosition.xy);
	float2 side1N = normalize(output[2].vPosition.xy - output[1].vPosition.xy);
	float2 side2N = normalize(output[0].vPosition.xy - output[2].vPosition.xy);
	const float texelSize = 1.0f / voxelCustomUB.voxelizeDimension;
	output[0].vPosition.xy += normalize(-side0N + side2N)*texelSize;
	output[1].vPosition.xy += normalize(side0N - side1N)*texelSize;
	output[2].vPosition.xy += normalize(side1N - side2N)*texelSize;

	outputStream.Append(output[0]);
	outputStream.Append(output[1]);
	outputStream.Append(output[2]);
	outputStream.RestartStrip();
}