#include "globals.hlsl"
#include "PathTracing.hlsl"

RWBuffer<float4>			rayOrigin		: register(u0);
RWBuffer<float4>			rayDirection	: register(u1);
RWBuffer<float3>			colorMask		: register(u2);

GLOBAL_CAMERA_UB(cameraUB);
GLOBAL_VARIOUS_UB(variousUB);

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint screenWidth = variousUB.vScreenResolution.x;
	const uint screenHeight = variousUB.vScreenResolution.y;
	if (dispatchThreadID.x >= screenWidth * screenHeight)
		return;

	float x = dispatchThreadID.x % screenWidth;
	float y = dispatchThreadID.x / screenWidth;

	float2 uv = { x / screenWidth, y / screenHeight };
	uv = 2.0 * uv - 1.0;
	uv.y = -uv.y;
	float4 direction = mul(cameraUB.invViewProjMatrix, float4(uv, 0.0f, 1.0f));
	direction /= direction.w;

	rayOrigin[dispatchThreadID.x] = float4(cameraUB.position.xyz, 0.0f);
	rayDirection[dispatchThreadID.x] = float4(normalize(direction.xyz - cameraUB.position.xyz), INFINITY);
	colorMask[dispatchThreadID.x] = float3(1.0f, 1.0f, 1.0f);
}