#include "globals.hlsl"

#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define WG_SIZE 128
#define GET_GLOBAL_IDX globalIdx.x

RWStructuredBuffer<uint> dst : register(u0);
RWStructuredBuffer<uint> src : register(u1);

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	dst[GET_GLOBAL_IDX] = src[GET_GLOBAL_IDX];
}
