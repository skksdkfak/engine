#include "globals.hlsl"
#include "PathTracing.hlsl"

RWStructuredBuffer<BBoxOverTriangle>	bBoxes					: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<BBox>				intermediateBBoxStack	: register(CUSTOM1_UAV_BP);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

[numthreads(8, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.x * 128;
	if (thxId >= bvhUB.numTriangles)
		return;

	BBox bbox;
	bbox.bottom = FLT_MAX;
	bbox.top = -FLT_MAX;

	for (int i = thxId; i < dispatchThreadID.x * 128 + 128 || i < bvhUB.numTriangles; i++)
	{
		bbox.bottom = min(bbox.bottom, bBoxes[i].bottom);
		bbox.top = max(bbox.top, bBoxes[i].top);
	}

	intermediateBBoxStack[intermediateBBoxStack.IncrementCounter()] = bbox;
}