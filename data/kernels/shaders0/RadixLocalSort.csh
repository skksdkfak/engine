#include "globals.hlsl"

#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
uint4 SELECT_UINT4(uint4 b, uint4 a, uint4 condition) { return  uint4(((condition).x) ? a.x : b.x, ((condition).y) ? a.y : b.y, ((condition).z) ? a.z : b.z, ((condition).w) ? a.w : b.w); }

#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define AtomInc(x) InterlockedAdd(x, 1)
#define AtomInc1(x, out) InterlockedAdd(x, 1, out)
#define WG_SIZE 128
#define NUM_PER_WI 4
#define BITS_PER_PASS 4

#define GET_GROUP_SIZE WG_SIZE

struct SortData
{
	uint m_key;
	uint m_value;
};

RWStructuredBuffer<SortData>	sortDataIn			: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<uint>		ldsHistogramOut0	: register(CUSTOM1_UAV_BP);
RWStructuredBuffer<uint>		ldsHistogramOut1	: register(CUSTOM2_UAV_BP);

groupshared uint ldsSortData[WG_SIZE*NUM_PER_WI + 16];

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	uint m_startBit;
	uint m_numGroups;
};

uint prefixScanVectorEx(inout uint4 data)
{
	uint4 backup = data;
	data.y += data.x;
	data.w += data.z;
	data.z += data.y;
	data.w += data.y;
	uint sum = data.w;
	data -= backup;
	return sum;
}

uint4 localPrefixSum128V(uint4 pData, uint lIdx, inout uint totalSum)
{
	{	//	Set data
		ldsSortData[lIdx] = 0;
		ldsSortData[lIdx + WG_SIZE] = prefixScanVectorEx(pData);
	}

	GroupMemoryBarrierWithGroupSync();

	{	//	Prefix sum
		int idx = 2 * lIdx + (WG_SIZE + 1);
		if (lIdx < 64)
		{
			ldsSortData[idx] += ldsSortData[idx - 1];
			ldsSortData[idx] += ldsSortData[idx - 2];
			ldsSortData[idx] += ldsSortData[idx - 4];
			ldsSortData[idx] += ldsSortData[idx - 8];
			ldsSortData[idx] += ldsSortData[idx - 16];
			ldsSortData[idx] += ldsSortData[idx - 32];
			ldsSortData[idx] += ldsSortData[idx - 64];
			ldsSortData[idx - 1] += ldsSortData[idx - 2];
		}
	}

	GroupMemoryBarrierWithGroupSync();

	totalSum = ldsSortData[WG_SIZE * 2 - 1];
	uint addValue = ldsSortData[lIdx + 127];
	return pData + uint4(addValue, addValue, addValue, addValue);
}

void generateHistogram(uint lIdx, uint wgIdx, uint4 sortedData)
{
	if (lIdx < (1 << BITS_PER_PASS))
	{
		ldsSortData[lIdx] = 0;
	}

	int mask = ((1 << BITS_PER_PASS) - 1);
	uint4 keys = uint4((sortedData.x)&mask, (sortedData.y)&mask, (sortedData.z)&mask, (sortedData.w)&mask);

	GroupMemoryBarrierWithGroupSync();

	AtomInc(ldsSortData[keys.x]);
	AtomInc(ldsSortData[keys.y]);
	AtomInc(ldsSortData[keys.z]);
	AtomInc(ldsSortData[keys.w]);
}

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	int nElemsPerWG = WG_SIZE*NUM_PER_WI;
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	uint wgSize = GET_GROUP_SIZE;

	uint4 localAddr = uint4(lIdx * 4 + 0, lIdx * 4 + 1, lIdx * 4 + 2, lIdx * 4 + 3);

	SortData sortData[NUM_PER_WI];
	{
		uint offset = nElemsPerWG*wgIdx;
		sortData[0] = sortDataIn[offset + localAddr.x];
		sortData[1] = sortDataIn[offset + localAddr.y];
		sortData[2] = sortDataIn[offset + localAddr.z];
		sortData[3] = sortDataIn[offset + localAddr.w];
	}

	int bitIdx = m_startBit;
	do
	{
		//	what is this?
		//		if( lIdx == wgSize-1 ) ldsSortData[256] = sortData[3].m_key;
		uint mask = (1 << bitIdx);
		uint4 cmpResult = uint4(sortData[0].m_key & mask, sortData[1].m_key & mask, sortData[2].m_key & mask, sortData[3].m_key & mask);
		uint4 prefixSum = SELECT_UINT4(uint4(1, 1, 1, 1), uint4(0, 0, 0, 0), cmpResult != uint4(0, 0, 0, 0));
		uint total;
		prefixSum = localPrefixSum128V(prefixSum, lIdx, total);

		{
			uint4 dstAddr = localAddr - prefixSum + uint4(total, total, total, total);
			dstAddr = SELECT_UINT4(prefixSum, dstAddr, cmpResult != uint4(0, 0, 0, 0));

			GroupMemoryBarrierWithGroupSync();

			ldsSortData[dstAddr.x] = sortData[0].m_key;
			ldsSortData[dstAddr.y] = sortData[1].m_key;
			ldsSortData[dstAddr.z] = sortData[2].m_key;
			ldsSortData[dstAddr.w] = sortData[3].m_key;

			GroupMemoryBarrierWithGroupSync();

			sortData[0].m_key = ldsSortData[localAddr.x];
			sortData[1].m_key = ldsSortData[localAddr.y];
			sortData[2].m_key = ldsSortData[localAddr.z];
			sortData[3].m_key = ldsSortData[localAddr.w];

			GroupMemoryBarrierWithGroupSync();

			ldsSortData[dstAddr.x] = sortData[0].m_value;
			ldsSortData[dstAddr.y] = sortData[1].m_value;
			ldsSortData[dstAddr.z] = sortData[2].m_value;
			ldsSortData[dstAddr.w] = sortData[3].m_value;

			GroupMemoryBarrierWithGroupSync();

			sortData[0].m_value = ldsSortData[localAddr.x];
			sortData[1].m_value = ldsSortData[localAddr.y];
			sortData[2].m_value = ldsSortData[localAddr.z];
			sortData[3].m_value = ldsSortData[localAddr.w];

			GroupMemoryBarrierWithGroupSync();
		}
		bitIdx++;
	} while (bitIdx <(m_startBit + BITS_PER_PASS));

	{	//	generate historgram
		uint4 localKeys = uint4(sortData[0].m_key >> m_startBit, sortData[1].m_key >> m_startBit,
			sortData[2].m_key >> m_startBit, sortData[3].m_key >> m_startBit);

		generateHistogram(lIdx, wgIdx, localKeys);

		GroupMemoryBarrierWithGroupSync();

		int nBins = (1 << BITS_PER_PASS);
		if (lIdx < nBins)
		{
			uint histValues = ldsSortData[lIdx];

			uint globalAddresses = nBins*wgIdx + lIdx;
			uint globalAddressesRadixMajor = m_numGroups*lIdx + wgIdx;

			ldsHistogramOut0[globalAddressesRadixMajor] = histValues;
			ldsHistogramOut1[globalAddresses] = histValues;
		}
	}

	{	//	write
		uint offset = nElemsPerWG*wgIdx;
		uint4 dstAddr = uint4(offset + localAddr.x, offset + localAddr.y, offset + localAddr.z, offset + localAddr.w);

		sortDataIn[dstAddr.x + 0] = sortData[0];
		sortDataIn[dstAddr.x + 1] = sortData[1];
		sortDataIn[dstAddr.x + 2] = sortData[2];
		sortDataIn[dstAddr.x + 3] = sortData[3];
	}
}