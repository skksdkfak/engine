#include "globals.hlsl"

//	takahiro end
#define WG_SIZE 128
#define NUM_PER_WI 4
#define BITS_PER_PASS 4

#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID

struct SortData
{
	uint m_key;
	uint m_value;
};

StructuredBuffer<SortData> src : register(t0);
StructuredBuffer<uint> histogramGlobalRadixMajor : register(t1);
StructuredBuffer<uint> histogramLocalGroupMajor : register(t2);

RWStructuredBuffer<SortData> dst : register(CUSTOM0_UAV_BP);

groupshared uint ldsLocalHistogram[2 * (1 << BITS_PER_PASS)];
groupshared uint ldsGlobalHistogram[(1 << BITS_PER_PASS)];

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	uint m_startBit;
	uint m_numGroups;
};

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	uint ldsOffset = (1 << BITS_PER_PASS);

	//	load and prefix scan local histogram
	if (lIdx < ((1 << BITS_PER_PASS) / 2))
	{
		uint2 myIdx = uint2(lIdx, lIdx + 8);

		ldsLocalHistogram[ldsOffset + myIdx.x] = histogramLocalGroupMajor[(1 << BITS_PER_PASS)*wgIdx + myIdx.x];
		ldsLocalHistogram[ldsOffset + myIdx.y] = histogramLocalGroupMajor[(1 << BITS_PER_PASS)*wgIdx + myIdx.y];
		ldsLocalHistogram[ldsOffset + myIdx.x - (1 << BITS_PER_PASS)] = 0;
		ldsLocalHistogram[ldsOffset + myIdx.y - (1 << BITS_PER_PASS)] = 0;

		int idx = ldsOffset + 2 * lIdx;
		ldsLocalHistogram[idx] += ldsLocalHistogram[idx - 1];
		ldsLocalHistogram[idx] += ldsLocalHistogram[idx - 2];
		ldsLocalHistogram[idx] += ldsLocalHistogram[idx - 4];
		ldsLocalHistogram[idx] += ldsLocalHistogram[idx - 8];

		// Propagate intermediate values through
		ldsLocalHistogram[idx - 1] += ldsLocalHistogram[idx - 2];

		// Grab and propagate for whole WG - loading the - 1 value
		uint2 localValues;
		localValues.x = ldsLocalHistogram[ldsOffset + myIdx.x - 1];
		localValues.y = ldsLocalHistogram[ldsOffset + myIdx.y - 1];

		ldsLocalHistogram[myIdx.x] = localValues.x;
		ldsLocalHistogram[myIdx.y] = localValues.y;


		ldsGlobalHistogram[myIdx.x] = histogramGlobalRadixMajor[m_numGroups*myIdx.x + wgIdx];
		ldsGlobalHistogram[myIdx.y] = histogramGlobalRadixMajor[m_numGroups*myIdx.y + wgIdx];
	}

	GroupMemoryBarrierWithGroupSync();

	uint4 localAddr = uint4(lIdx * 4 + 0, lIdx * 4 + 1, lIdx * 4 + 2, lIdx * 4 + 3);

	SortData sortData[4];
	{
		uint4 globalAddr = wgIdx*WG_SIZE*NUM_PER_WI + localAddr;
		sortData[0] = src[globalAddr.x];
		sortData[1] = src[globalAddr.y];
		sortData[2] = src[globalAddr.z];
		sortData[3] = src[globalAddr.w];
	}

	uint cmpValue = ((1 << BITS_PER_PASS) - 1);
	uint4 radix = uint4((sortData[0].m_key >> m_startBit)&cmpValue, (sortData[1].m_key >> m_startBit)&cmpValue,
		(sortData[2].m_key >> m_startBit)&cmpValue, (sortData[3].m_key >> m_startBit)&cmpValue);;

	//	data is already sorted. So simply subtract local prefix sum
	uint4 dstAddr;
	dstAddr.x = ldsGlobalHistogram[radix.x] + (localAddr.x - ldsLocalHistogram[radix.x]);
	dstAddr.y = ldsGlobalHistogram[radix.y] + (localAddr.y - ldsLocalHistogram[radix.y]);
	dstAddr.z = ldsGlobalHistogram[radix.z] + (localAddr.z - ldsLocalHistogram[radix.z]);
	dstAddr.w = ldsGlobalHistogram[radix.w] + (localAddr.w - ldsLocalHistogram[radix.w]);

	dst[dstAddr.x] = sortData[0];
	dst[dstAddr.y] = sortData[1];
	dst[dstAddr.z] = sortData[2];
	dst[dstAddr.w] = sortData[3];
}