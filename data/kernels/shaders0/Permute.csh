#include "globals.hlsl"

#define WG_COUNT 64
#define WG_SIZE 256
#define BLOCK_SIZE 1024  // (4 * WG_SIZE)
#define BITS_PER_PASS 4
#define RADICES 16       // (1 << BITS_PER_PASS)
#define RADICES_MASK 0xf // (RADICES - 1)
#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x

cbuffer SortCB : register(CUSTOM0_UB_BP)
{
	uint n;
	uint shift;
	bool descending;
	bool is_signed;
	bool key_index;
};

RWStructuredBuffer<uint> histogram : register(u0);
RWStructuredBuffer<uint> keyIn : register(u1);
RWStructuredBuffer<uint> keyOut : register(u2);
RWStructuredBuffer<uint> valueIn : register(u3);
RWStructuredBuffer<uint> valueOut : register(u4);

#define EACH(i, count) for (int i = 0; i < count; i++)
#define TO_MASK(n) ((1 << (n)) - 1)
#define BFE(src, s, n) ((src >> s) & TO_MASK(n))
#define BFE_SIGN(src, s, n)	(((((src >> s) & TO_MASK(n - 1)) ^ TO_MASK(n - 1)) & TO_MASK(n - 1)) | ((src >> s) &  (1 << (n - 1))))
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define MIX(T, x, y, a) (x)* T(a) + (y) * (1 - T(a))
#define GET_BY4(T, src, idx) T(src[idx.x], src[idx.y], src[idx.z], src[idx.w])
#define SET_BY4(dest, idx, val) do { dest[idx.x] = val.x; dest[idx.y] = val.y; dest[idx.z] = val.z; dest[idx.w] = val.w; } while (false)
#define SET_BY4_CHECKED(dest, idx, val, flag) \
do { \
	if (flag.x != 0) \
		dest[idx.x] = val.x; \
	if (flag.y != 0) \
		dest[idx.y] = val.y; \
	if (flag.z != 0) \
		dest[idx.z] = val.z; \
	if (flag.w != 0) \
		dest[idx.w] = val.w; \
} while (false)

#define INC_BY4_CHECKED(dest, idx, flag) do { InterlockedAdd(dest[idx.x], flag.x); InterlockedAdd(dest[idx.y], flag.y); InterlockedAdd(dest[idx.z], flag.z); InterlockedAdd(dest[idx.w], flag.w); } while (false)

uint4 lessThan(in uint4 x, in uint4 y)
{
	uint4 lessThan;
	lessThan.x = x.x < y.x;
	lessThan.y = x.y < y.y;
	lessThan.z = x.z < y.z;
	lessThan.w = x.w < y.w;

	return lessThan;
}

uint prefix_scan(inout uint4 v)
{
	uint sum = 0;
	uint tmp;
	tmp = v.x; v.x = sum; sum += tmp;
	tmp = v.y; v.y = sum; sum += tmp;
	tmp = v.z; v.z = sum; sum += tmp;
	tmp = v.w; v.w = sum; sum += tmp;
	return sum;
}

struct BlocksInfo
{
	uint count;
	uint offset;
};

BlocksInfo GetBlocksInfo(const uint n, const uint wg_idx)
{
	BlocksInfo blocksInfo;
	const uint aligned = n + BLOCK_SIZE - (n % BLOCK_SIZE);
	const uint blocks = (n + BLOCK_SIZE - 1) / BLOCK_SIZE;
	const uint blocks_per_wg = (blocks + WG_COUNT - 1) / WG_COUNT;
	const int n_blocks = int(aligned / BLOCK_SIZE) - int(blocks_per_wg * wg_idx);
	blocksInfo.count = uint(clamp(n_blocks, 0, int(blocks_per_wg)));
	blocksInfo.offset = blocks_per_wg * BLOCK_SIZE * wg_idx;
	return blocksInfo;
}

groupshared uint local_sort[BLOCK_SIZE];
uint prefix_sum(uint data, inout uint total_sum, uint lIdx)
{
	const uint lc_idx = lIdx + WG_SIZE;
	local_sort[lIdx] = 0;
	local_sort[lc_idx] = data;
	GroupMemoryBarrierWithGroupSync();
	for (uint i = 1; i < WG_SIZE; i <<= 1)
	{
		uint tmp = local_sort[lc_idx - i];
		GroupMemoryBarrierWithGroupSync();
		local_sort[lc_idx] += tmp;
		GroupMemoryBarrierWithGroupSync();
	}
	total_sum = local_sort[WG_SIZE * 2 - 1];
	return local_sort[lc_idx - 1];
}

groupshared uint local_sort_val[BLOCK_SIZE];

void sort_bits(inout uint4 sort, inout uint4 sort_val, uint lIdx)
{
	uint4 signs = BFE_SIGN(sort, shift, BITS_PER_PASS);
	const uint4 addr = 4 * lIdx + uint4(0, 1, 2, 3);
	EACH(i_bit, BITS_PER_PASS)
	{
		const uint mask = (1 << i_bit);
		const uint4 cmpL = (is_signed ? signs : (sort >> shift)) & mask;
		const uint cmpR = uint(descending != is_signed) * mask;
		const uint4 cmp = uint4(cmpL.x == cmpR, cmpL.y == cmpR, cmpL.z == cmpR, cmpL.w == cmpR);
		uint4 key = uint4(cmp);
		uint total;
		key += prefix_sum(1, total, lIdx);
		GroupMemoryBarrierWithGroupSync();

		const uint4 dest_addr = MIX(uint4, key, addr - key + total, cmp);
		//const uint4 dest_addr = key * cmp + (addr - key + total) * (1 - cmp);
		SET_BY4(local_sort, dest_addr, sort);
		SET_BY4(local_sort_val, dest_addr, sort_val);
		GroupMemoryBarrierWithGroupSync();

		sort = GET_BY4(uint4, local_sort, addr);
		sort_val = GET_BY4(uint4, local_sort_val, addr);
		GroupMemoryBarrierWithGroupSync();

		if (is_signed)
		{
			SET_BY4(local_sort, dest_addr, signs);
			GroupMemoryBarrierWithGroupSync();

			signs = GET_BY4(uint4, local_sort, addr);
			GroupMemoryBarrierWithGroupSync();
		}
	}
}

groupshared uint local_histogram_to_carry[RADICES];
groupshared uint local_histogram[RADICES * 2];

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	uint gIdx = GET_GLOBAL_IDX;
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	const uint carry_idx = (descending && !is_signed ? (RADICES_MASK - lIdx) : lIdx);
	if (lIdx < RADICES)
		local_histogram_to_carry[lIdx] = histogram[carry_idx * WG_COUNT + wgIdx];
	GroupMemoryBarrierWithGroupSync();

	const uint def = (uint(!descending) * 0xffffffff) ^ (uint(is_signed) * 0x80000000);
	const BlocksInfo blocks = GetBlocksInfo(n, wgIdx);
	uint4 addr = blocks.offset + 4 * lIdx + uint4(0, 1, 2, 3);
	EACH(i_block, blocks.count)
	{
		const uint4 less_than = lessThan(addr, n);
		const uint4 less_than_val = lessThan(addr, key_index ? n : 0);
		//const uint4 less_than = 1 - step(addr, n);
		//const uint4 less_than_val = 1 - step(addr, key_index ? n : 0);
		const uint4 data_vec = GET_BY4(uint4, keyIn, addr);
		const uint4 data_val_vec = GET_BY4(uint4, valueIn, addr);

		uint4 sort = MIX(uint4, data_vec, def, less_than);
		uint4 sort_val = MIX(uint4, data_val_vec, 0, less_than_val);
		//uint4 sort = data_vec * less_than + def * (1 - less_than);
		//uint4 sort_val = data_val_vec * less_than_val + uint4(0, 0, 0, 0) * (1 - less_than_val);
		sort_bits(sort, sort_val, lIdx); // lIdx??
		uint4 k = is_signed
			? BFE_SIGN(sort, shift, BITS_PER_PASS)
			: BFE(sort, shift, BITS_PER_PASS);
		const uint4 key = (descending != is_signed) ? (RADICES_MASK - k) : k;
		const uint4 hist_key = key + RADICES;
		const uint4 local_key = key + (lIdx / RADICES) * RADICES;
		k = is_signed ? key : k;
		const uint4 offset = GET_BY4(uint4, local_histogram_to_carry, k) + 4 * lIdx + uint4(0, 1, 2, 3);
		local_sort[lIdx] = 0;
		GroupMemoryBarrierWithGroupSync();

		INC_BY4_CHECKED(local_sort, local_key, less_than);
		GroupMemoryBarrierWithGroupSync();

		const uint lc_idx = lIdx + RADICES;
		if (lIdx < RADICES)
		{
			local_histogram[lIdx] = 0;
			uint sum = 0;
			EACH(i, WG_SIZE / RADICES)
				sum += local_sort[i * RADICES + lIdx];
			local_histogram[lc_idx] = sum;
			local_histogram_to_carry[carry_idx] += sum;
		}
		GroupMemoryBarrierWithGroupSync();

		uint tmp = 0;
		if (lIdx < RADICES)
			local_histogram[lc_idx] = local_histogram[lc_idx - 1];
		GroupMemoryBarrierWithGroupSync();
		if (lIdx < RADICES)
			tmp = local_histogram[lc_idx - 3]
			+ local_histogram[lc_idx - 2]
			+ local_histogram[lc_idx - 1];
		GroupMemoryBarrierWithGroupSync();
		if (lIdx < RADICES)
			local_histogram[lc_idx] += tmp;
		GroupMemoryBarrierWithGroupSync();
		if (lIdx < RADICES)
			tmp = local_histogram[lc_idx - 12]
			+ local_histogram[lc_idx - 8]
			+ local_histogram[lc_idx - 4];
		GroupMemoryBarrierWithGroupSync();
		if (lIdx < RADICES)
			local_histogram[lc_idx] += tmp;
		GroupMemoryBarrierWithGroupSync();

		const uint4 out_key = offset - GET_BY4(uint4, local_histogram, hist_key);
		SET_BY4_CHECKED(keyOut, out_key, sort, less_than);
		SET_BY4_CHECKED(valueOut, out_key, sort_val, less_than_val);
		GroupMemoryBarrierWithGroupSync();
		addr += BLOCK_SIZE;
	}
}