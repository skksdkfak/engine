#include "globals.hlsl"

struct VS_INPUT
{
	float4 inPatchParams	: POSITION;
};

struct HSIn_Heightfield
{
	float2 origin	: ORIGIN;
	float2 size		: SIZE;
	uint RTIndex	: RTARRAYINDEX;
};

HSIn_Heightfield main(VS_INPUT i, uint instanceID : SV_InstanceID)
{
	HSIn_Heightfield output;

	output.origin = i.inPatchParams.xy;
	output.size = i.inPatchParams.zw;
	output.RTIndex = instanceID;

	return output;
}