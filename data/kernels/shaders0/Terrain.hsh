#include "globals.hlsl"

Texture2D		g_HeightfieldTexture	: register(NORMAL_TEX_BP);

SamplerState	g_SamplerLinearWrap		: register(COLOR_SAM_BP);

GLOBAL_CAMERA_UB(cameraUB);

#ifdef CASCADED_SHADOW_MAP
cbuffer cbCascadedMatrices : register(CUSTOM1_UB_BP)
{
	matrix g_mViewProjCSM[8];
};
#endif

#ifdef SPOTLIGHT_SHADOW_MAP
cbuffer cbSpotLightMatrices : register(CUSTOM1_UB_BP)
{
	matrix g_mViewProjSM;
};
#endif


struct HSIn_Heightfield
{
	float2 origin	: ORIGIN;
	float2 size		: SIZE;
	uint RTIndex	: RTARRAYINDEX;
};

struct DUMMY
{
	float Dummmy : DUMMY;
};

struct PatchData
{
	float Edges[4]  : SV_TessFactor;
	float Inside[2]	: SV_InsideTessFactor;

	float2 origin   : ORIGIN;
	float2 size     : SIZE;
	uint RTIndex	: RTARRAYINDEX;
};

// calculating tessellation factor. It is either constant or hyperbolic depending on g_UseDynamicLOD switch
float CalculateTessellationFactor(float distance)
{
	float g_UseDynamicLOD = true;
	float g_DynamicTessFactor = 15.0f;
	float g_StaticTessFactor = 12.0f;
	return lerp(g_StaticTessFactor, g_DynamicTessFactor*(1 / (0.015*distance)), g_UseDynamicLOD);
}

PatchData PatchConstantHS(InputPatch<HSIn_Heightfield, 1> inputPatch)
{
	PatchData output;

	float g_DynamicTessFactor = 15.0f;
	float g_StaticTessFactor = 12.0f;
	float g_UseDynamicLOD = true;
	float g_FrustumCullInHS = true;
	float g_HeightFieldSize = 512;
	float g_TerrainBeingRendered = 1.0f;

	float distance_to_camera;
	float tesselation_factor;
	float inside_tessellation_factor = 0;
	float in_frustum = 0;

	output.origin = inputPatch[0].origin;
	output.size = inputPatch[0].size;
	output.RTIndex = (float)inputPatch[0].RTIndex;

	float2 texcoord0to1 = (inputPatch[0].origin + inputPatch[0].size / 2.0) / g_HeightFieldSize;
	texcoord0to1.y = 1 - texcoord0to1.y;

	// conservative frustum culling
	float3 patch_center = float3(inputPatch[0].origin.x + inputPatch[0].size.x*0.5, g_TerrainBeingRendered*g_HeightfieldTexture.SampleLevel(g_SamplerLinearWrap, texcoord0to1, 0).w, inputPatch[0].origin.y + inputPatch[0].size.y*0.5);
	float3 camera_to_patch_vector = patch_center - cameraUB.position.xyz;
	float3 patch_to_camera_direction_vector = cameraUB.direction.xyz*dot(camera_to_patch_vector, cameraUB.direction.xyz) - camera_to_patch_vector;
	float3 patch_center_realigned = patch_center + normalize(patch_to_camera_direction_vector)*min(2 * inputPatch[0].size.x, length(patch_to_camera_direction_vector));
#ifdef CASCADED_SHADOW_MAP
	float4 patch_screenspace_center = mul(float4(patch_center_realigned, 1.0), g_mViewProjCSM[inputPatch[0].RTIndex]);
#endif
#ifdef SPOTLIGHT_SHADOW_MAP
	float4 patch_screenspace_center = mul(float4(patch_center_realigned, 1.0), g_mViewProjSM);
#endif
#ifdef BASE_RENDER
	float4 patch_screenspace_center = mul(float4(patch_center_realigned, 1.0), cameraUB.viewProjMatrix);
#endif

#ifndef POINT_LIGHT_SHADOW_MAP
#ifndef VOXELIZATION
	if (((patch_screenspace_center.x / patch_screenspace_center.w > -1.0) && (patch_screenspace_center.x / patch_screenspace_center.w<1.0)
		&& (patch_screenspace_center.y / patch_screenspace_center.w>-1.0) && (patch_screenspace_center.y / patch_screenspace_center.w < 1.0)
		&& (patch_screenspace_center.w>0)) || (length(patch_center - cameraUB.position.xyz) < 2 * inputPatch[0].size.x))
	{
		in_frustum = 1;
	}

	if ((in_frustum) || (g_FrustumCullInHS == 0))
	{
#endif
#endif
		distance_to_camera = length(cameraUB.position.xz - inputPatch[0].origin - float2(0, inputPatch[0].size.y*0.5));
		tesselation_factor = CalculateTessellationFactor(distance_to_camera);
		output.Edges[0] = tesselation_factor;
		inside_tessellation_factor += tesselation_factor;


		distance_to_camera = length(cameraUB.position.xz - inputPatch[0].origin - float2(inputPatch[0].size.x*0.5, 0));
		tesselation_factor = CalculateTessellationFactor(distance_to_camera);
		output.Edges[1] = tesselation_factor;
		inside_tessellation_factor += tesselation_factor;

		distance_to_camera = length(cameraUB.position.xz - inputPatch[0].origin - float2(inputPatch[0].size.x, inputPatch[0].size.y*0.5));
		tesselation_factor = CalculateTessellationFactor(distance_to_camera);
		output.Edges[2] = tesselation_factor;
		inside_tessellation_factor += tesselation_factor;

		distance_to_camera = length(cameraUB.position.xz - inputPatch[0].origin - float2(inputPatch[0].size.x*0.5, inputPatch[0].size.y));
		tesselation_factor = CalculateTessellationFactor(distance_to_camera);
		output.Edges[3] = tesselation_factor;
		inside_tessellation_factor += tesselation_factor;
		output.Inside[0] = output.Inside[1] = inside_tessellation_factor*0.25;
#ifndef POINT_LIGHT_SHADOW_MAP
#ifndef VOXELIZATION
	}
	else
	{
		output.Edges[0] = -1;
		output.Edges[1] = -1;
		output.Edges[2] = -1;
		output.Edges[3] = -1;
		output.Inside[0] = -1;
		output.Inside[1] = -1;
	}
#endif
#endif

	return output;
}

[domain("quad")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(1)]
[patchconstantfunc("PatchConstantHS")]
DUMMY main(InputPatch<HSIn_Heightfield, 1> inputPatch)
{
	return (DUMMY)0;
}