#include "globals.hlsl"

//Texture3D<float4>		g_texVoxelGridNormal			: register(NORMAL_TEX_BP);
TextureCube<float>		g_texShadowCubeMap				: register(CUSTOM0_TEX_BP);

RWTexture3D<float4>		g_VoxelGridShadedOutputTexture	: register(CUSTOM0_UAV_BP);
RWTexture3D<float4>		g_VoxelGridNormalTexture		: register(CUSTOM1_UAV_BP);

SamplerComparisonState	SamplerTypeComparisionPoint		: register(CUSTOM0_SAM_BP);

GLOBAL_POINT_LIGHT_UB(pointLightUB);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		float voxelizeSize;
		float voxelSize;
		float voxelizeDimension;
		vector voxelizePosition;
	}customUB;
};

[numthreads(8, 8, 8)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	float4 normal = g_VoxelGridNormalTexture[dispatchThreadID.xyz];
	//if (normal.w == 0)
	//	return;

	float4 outputColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float diffuse;
	float shadowTerm = 0.0f;
	float3 position = dispatchThreadID * customUB.voxelSize + customUB.voxelizePosition.xyz - customUB.voxelizeSize / 2;
	float3 lightDirection = pointLightUB.position - position.xyz;
	float depthValue;
	float lightVecLen = length(lightDirection);
	float bias = customUB.voxelSize;

	[unroll]
	for (int i = 0; i < PCF_NUM_SAMPLES; i++)
	{
		shadowTerm += g_texShadowCubeMap.SampleCmpLevelZero(
			SamplerTypeComparisionPoint,
			-normalize(lightDirection) + (PoissonDisc3D[i] * 0.05f),
			lightVecLen - bias
			).r;
	}

	shadowTerm /= PCF_NUM_SAMPLES;
	
	diffuse = saturate(dot(normal.xyz, normalize(lightDirection)));
	if (diffuse > 0.0f)
	{
		outputColor += pointLightUB.color * pointLightUB.multiplier * diffuse;
	}

	outputColor *= shadowTerm * saturate(1.0f - (lightVecLen / pointLightUB.radius));

	g_VoxelGridShadedOutputTexture[dispatchThreadID.xyz] += float4(outputColor.rgb, 0.0f);
}