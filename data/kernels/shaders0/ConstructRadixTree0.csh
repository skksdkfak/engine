#include "globals.hlsl"
#include "PathTracing.hlsl"

struct RadixTreeDebugInfo
{
	int i;
	int d;
	int lmax;
	int l;
	int j;
	int s;
	int gamma;
};

RWStructuredBuffer<TreeNode>			radixTreeNodes			: register(CUSTOM0_UAV_BP);
//RWStructuredBuffer<RadixTreeDebugInfo>	radixTreeDebugInfo		: register(CUSTOM1_UAV_BP);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

//int clz(int x)
//{
//	int n = 32;
//	uint y;
//	y = x >> 16; if (y != 0) { n = n - 16; x = y; }
//	y = x >> 8; if (y != 0) { n = n - 8; x = y; }
//	y = x >> 4; if (y != 0) { n = n - 4; x = y; }
//	y = x >> 2; if (y != 0) { n = n - 2; x = y; }
//	y = x >> 1; if (y != 0) return n - 2;
//	return n - x;
//}

// From http://embeddedgurus.com/state-space/2014/09/fast-deterministic-and-portable-counting-leading-zeros/
int clz(int x)
{
	x |= x >> 1;
	// the next 5 statements will turn all bits after the
	// leading zero's into a 1-bit
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	// compute the Hamming weight of x ... and return 32
	// minus the computed result'
	x -= ((x >> 1) & 0x55555555U);
	x = (x & 0x33333333U) + ((x >> 2) & 0x33333333U);
	return 32U - ((((x + (x >> 4)) & 0x0F0F0F0FU) * 0x01010101U) >> 24);
}

// Longest common prefix for morton code
int longestCommonPrefix(int i, int j, int len)
{
	if (0 <= j && j < len)
		return clz(i ^ j);
	else
		return -1;
}

[numthreads(256, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	int i = dispatchThreadID.x;
	int len = bvhUB.numTriangles - 1;
	int leavesOffset = bvhUB.numTriangles - 1;
	if (i >= len)
		return;

	// Run radix tree construction algorithm
	// Determine direction of the range (+1 or -1)
	//int d = longestCommonPrefix(i, i + 1, len + 1) -
	//	longestCommonPrefix(i, i - 1, len + 1) > 0 ? 1 : -1;
	int d = sign(longestCommonPrefix(i, i + 1, len + 1) -
		longestCommonPrefix(i, i - 1, len + 1));

	// Compute upper bound for the length of the range
	int sigMin = longestCommonPrefix(i, i - d, len + 1);
	int lmax = 2;

	while (longestCommonPrefix(i, i + lmax * d, len + 1) > sigMin)
	{
		lmax *= 2;
	}

	// Find the other end using binary search
	int l = 0;
	int divider = 2;
	for (int t = lmax / divider; t >= 1; divider *= 2)
	{
		if (longestCommonPrefix(i, i + (l + t) * d, len + 1) > sigMin)
		{
			l += t;
		}
		t = lmax / divider;
	}

	int j = i + l * d;

	// Find the split position using binary search
	int sigNode = longestCommonPrefix(i, j, len + 1);
	int s = 0;
	divider = 2;
	for (int t = (l + (divider - 1)) / divider; t >= 1; divider *= 2)
	{
		if (longestCommonPrefix(i, i + (s + t) * d, len + 1) > sigNode)
		{
			s += t;
		}
		t = (l + (divider - 1)) / divider;
	}

	int gamma = i + s * d + min(d, 0);

	//radixTreeDebugInfo[i].i = i;
	//radixTreeDebugInfo[i].d = d;
	//radixTreeDebugInfo[i].lmax = lmax;
	//radixTreeDebugInfo[i].l = l;
	//radixTreeDebugInfo[i].j = j;
	//radixTreeDebugInfo[i].s = s;
	//radixTreeDebugInfo[i].gamma = gamma;

	if (min(i, j) == gamma)
	{
		radixTreeNodes[i].left = leavesOffset + gamma;
		radixTreeNodes[leavesOffset + gamma].parent = i;
		radixTreeNodes[leavesOffset + gamma].leaf = true;
	}
	else
	{
		radixTreeNodes[i].left = gamma;
		radixTreeNodes[gamma].parent = i;
	}

	if (max(i, j) == gamma + 1)
	{
		radixTreeNodes[i].right = leavesOffset + gamma + 1;
		radixTreeNodes[leavesOffset + gamma + 1].parent = i;
		radixTreeNodes[leavesOffset + gamma + 1].leaf = true;
	}
	else
	{
		radixTreeNodes[i].right = gamma + 1;
		radixTreeNodes[gamma + 1].parent = i;
	}

	radixTreeNodes[i].min = min(i, j);
	radixTreeNodes[i].max = max(i, j);
}