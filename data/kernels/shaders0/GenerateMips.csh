#include "globals.hlsl"

RWTexture3D<float4>	g_dstTexture: register(CUSTOM0_UAV_BP);
RWTexture3D<float4>	g_srcTexture: register(CUSTOM1_UAV_BP);
//Texture3D<float4>	g_srcTexture	: register(CUSTOM0_TEX_BP);

[numthreads(2, 2, 2)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	float4 newTexel = 0;
	uint3 srcPos = dispatchThreadID.xyz * 2;

	newTexel += g_srcTexture[srcPos];
	newTexel += g_srcTexture[srcPos + uint3(1, 0, 0)];
	newTexel += g_srcTexture[srcPos + uint3(0, 1, 0)];
	newTexel += g_srcTexture[srcPos + uint3(0, 0, 1)];
	newTexel += g_srcTexture[srcPos + uint3(1, 1, 0)];
	newTexel += g_srcTexture[srcPos + uint3(0, 1, 1)];
	newTexel += g_srcTexture[srcPos + uint3(1, 0, 1)];
	newTexel += g_srcTexture[srcPos + uint3(1, 1, 1)];

	newTexel /= 8;

	g_dstTexture[dispatchThreadID.xyz] = newTexel;
}