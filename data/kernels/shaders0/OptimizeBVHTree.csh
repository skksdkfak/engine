#include "globals.hlsl"
#include "PathTracing.hlsl"

//#define DEBUG_CHECK
#define RESTRUCT_STACK_SIZE (16)

RWStructuredBuffer<TreeNode>			radixTreeNodes			: register(CUSTOM0_UAV_BP);
RWStructuredBuffer<int>					nodeCounter				: register(CUSTOM1_UAV_BP);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

struct PartitionEntry
{
	uint partition;
	bool left;
	uint parent;
};

uint getFirstSetBitPos(int n)
{
	return log2(n&-n) + 1;
}

/* Function to get no of set bits in binary
representation of passed binary no. */
int countSetBits(int n)
{
	uint count = 0;
	while (n)
	{
		n &= (n - 1);
		count++;
	}
	return count;
}

float getArea(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z)
{
	float dx = max_x - min_x;
	float dy = max_y - min_y;
	float dz = max_z - min_z;

	return 2 * (dx * dy + dx * dz + dy * dz);
}

float get_total_area(int n, in uint leaves[7], uint s)
{
	float lmin_x, lmin_y, lmin_z, lmax_x, lmax_y, lmax_z;
	float min_x = FLT_MAX;
	float max_x = FLT_MIN;
	float min_y = FLT_MAX;
	float max_y = FLT_MIN;
	float min_z = FLT_MAX;
	float max_z = FLT_MIN;
	for (int i = 0; i < n; i++)
	{
		if ((s >> i) & 1 == 1)
		{
			Bound bound = radixTreeNodes[leaves[i]].bound;
			lmin_x = bound.min.x;
			lmin_y = bound.min.y;
			lmin_z = bound.min.z;
			lmax_x = bound.max.x;
			lmax_y = bound.max.y;
			lmax_z = bound.max.z;
			if (lmin_x < min_x) min_x = lmin_x;
			if (lmin_y < min_y) min_y = lmin_y;
			if (lmin_z < min_z) min_z = lmin_z;
			if (lmax_x > max_x) max_x = lmax_x;
			if (lmax_y > max_y) max_y = lmax_y;
			if (lmax_z > max_z) max_z = lmax_z;
		}
	}
	return getArea(min_x, max_x, min_y, max_y, min_z, max_z);
}

void propagateAreaCost(in uint root, in uint leaves[7], int num_leaves)
{
	for (int i = 0; i < num_leaves; i++)
	{
		uint cur = leaves[i];
		cur = radixTreeNodes[cur].parent;
		while (cur != root)
		{
			TreeNode current = radixTreeNodes[cur];
			if (current.cost == 0.0)
			{
				if (radixTreeNodes[current.left].cost != 0.0 && radixTreeNodes[current.right].cost != 0.0)
				{
					// Both left & right propagated
					merge_bounds(radixTreeNodes[current.left].bound, radixTreeNodes[current.right].bound, radixTreeNodes[cur].bound);
					Bound bound = radixTreeNodes[cur].bound;
					float area = radixTreeNodes[cur].area = getArea(bound.min.x, bound.max.x, bound.min.y,
						bound.max.y, bound.min.z, bound.max.z);
					radixTreeNodes[cur].cost = Ci * area + radixTreeNodes[current.left].cost + radixTreeNodes[current.right].cost;
				}
				else
				{
					// Only one side propagated
					break;
				}
			}
			cur = current.parent;
		}
	}

	// Propagate root
	TreeNode rootNode = radixTreeNodes[root];
	merge_bounds(radixTreeNodes[rootNode.left].bound, radixTreeNodes[rootNode.right].bound, radixTreeNodes[root].bound);
	Bound bound = radixTreeNodes[root].bound;
	float area = radixTreeNodes[root].area = getArea(bound.min.x, bound.max.x, bound.min.y,
		bound.max.y, bound.min.z, bound.max.z);
	radixTreeNodes[root].cost = Ci * area + radixTreeNodes[rootNode.left].cost + radixTreeNodes[rootNode.right].cost;
}

void restructTree(in uint parent,
	in uint leaves[7], in uint nodes[5],
	in uint partition, in uint optimal[128],
	inout int index, in bool left,
	int num_leaves)
{
	PartitionEntry stack[RESTRUCT_STACK_SIZE];
	int topIndex = RESTRUCT_STACK_SIZE;
	PartitionEntry tmp = { partition, left, parent };
	stack[--topIndex] = tmp;

	// Do while stack is not empty
	while (topIndex != RESTRUCT_STACK_SIZE)
	{
		PartitionEntry pe = stack[topIndex++];
		partition = pe.partition;
		left = pe.left;
		parent = pe.parent;

#ifdef DEBUG_CHECK
		if (partition == 0)
			return;
#endif

		if (countSetBits(partition) == 1)
		{
			// Leaf
			int leaf_index = getFirstSetBitPos(partition) - 1;

			uint leaf = leaves[leaf_index];
			if (left)
				radixTreeNodes[parent].left = leaf;
			else
				radixTreeNodes[parent].right = leaf;

			radixTreeNodes[leaf].parent = parent;
		}
		else
		{
#ifdef DEBUG_CHECK
			if (index >= 7)
				return;
#endif

			// Internal node
			uint node = nodes[index++];

			// Set cost to 0 as a mark
			radixTreeNodes[node].cost = 0.0;

			if (left)
				radixTreeNodes[parent].left = node;
			else
				radixTreeNodes[parent].right = node;

			radixTreeNodes[node].parent = parent;

#ifdef DEBUG_CHECK
			if (partition >= 128)
				return;
#endif

			uint left_partition = optimal[partition];
			uint right_partition = (~left_partition) & partition;

#ifdef DEBUG_CHECK
			if ((left_partition | partition) != partition)
				return;
			if ((right_partition | partition) != partition)
				return;
#endif

#ifdef DEBUG_CHECK
			//if (topIndex < 2)
			//	printf("restructTree stack not big enough. Increase RESTRUCT_STACK_SIZE!\n");
#endif

			PartitionEntry tmp1 = { left_partition, true, node };
			stack[--topIndex] = tmp1;
			PartitionEntry tmp2 = { right_partition, false, node };
			stack[--topIndex] = tmp2;
		}
	}

	propagateAreaCost(parent, leaves, num_leaves);
}

/**
* treeletOptimize
* Find the treelet and optimize
*/
void treeletOptimize(in uint root)
{
	TreeNode rootNode = radixTreeNodes[root];
	// Don't need to optimize if root is a leaf
	if (rootNode.leaf)
		return;

	// Find a treelet with max number of leaves being 7
	uint leaves[7];
	int counter = 0;
	leaves[counter++] = rootNode.left;
	leaves[counter++] = rootNode.right;

	// Also remember the internal nodes
	// Max 7 (leaves) - 1 (root doesn't count) - 1
	uint nodes[5];
	int nodes_counter = 0;

	float max_area;
	int max_index = 0;

	while (counter < 7 && max_index != -1)
	{
		max_index = -1;
		max_area = -1.0f;

		for (int i = 0; i < counter; i++)
		{
			if (!(radixTreeNodes[leaves[i]].leaf))
			{
				float area = radixTreeNodes[leaves[i]].area;
				if (area > max_area)
				{
					max_area = area;
					max_index = i;
				}
			}
		}

		if (max_index != -1)
		{
			TreeNode tmp = radixTreeNodes[leaves[max_index]];

			// Put this node in nodes array
			nodes[nodes_counter++] = leaves[max_index];

			// Replace the max node with its children
			leaves[max_index] = leaves[counter - 1];
			leaves[counter - 1] = tmp.left;
			leaves[counter++] = tmp.right;
		}
	}

	uint optimal[128];
	
	// Call calculateOptimalCost here
	{
		int num_subsets = pow(2, counter) - 1;
		// 0th element in array should not be used
		float a[128];
		float c_opt[128];
		// Calculate surface area for each subset
		for (uint s = 1; s <= num_subsets; s++)
		{
			a[s] = get_total_area(counter, leaves, s);
		}
		// Initialize costs of individual leaves
		for (uint i = 0; i <= (counter - 1); i++)
		{
			c_opt[pow(2, i)] = radixTreeNodes[leaves[i]].cost;
		}
		// Optimize every subset of leaves
		for (uint k = 2; k <= counter; k++)
		{
			for (uint s = 1; s <= num_subsets; s++)
			{
				if (countSetBits(s) == k)
				{
					// Try each way of partitioning the leaves
					float c_s = FLT_MAX;
					uint p_s = 0;
					uint d = (s - 1) & s;
					uint p = (-d) & s;
					while (p != 0)
					{
						float c = c_opt[p] + c_opt[s ^ p];
						if (c < c_s)
						{
							c_s = c;
							p_s = p;
						}
						p = (p - d) & s;
					}
					// Calculate final SAH cost
					c_opt[s] = Ci * a[s] + c_s;
					optimal[s] = p_s;
				}
			}
		}
	}

	// Use complement on right tree, and use original on left tree
	uint mask = (1 << counter) - 1;		// mask = max index
	int index = 0;						// index for free nodes
	uint leftIndex = mask;
	uint left = optimal[leftIndex];
	restructTree(root, leaves, nodes, left, optimal, index, true, counter);

	uint right = (~left) & mask;
	restructTree(root, leaves, nodes, right, optimal, index, false, counter);

	// Calculate current node's area & cost
	rootNode = radixTreeNodes[root];
	merge_bounds(radixTreeNodes[rootNode.left].bound, radixTreeNodes[rootNode.right].bound, radixTreeNodes[root].bound);
	Bound bound = radixTreeNodes[root].bound;
	float area = radixTreeNodes[root].area = getArea(bound.min.x, bound.max.x, bound.min.y,
		bound.max.y, bound.min.z, bound.max.z);
	radixTreeNodes[root].cost = Ci * area + radixTreeNodes[rootNode.left].cost + radixTreeNodes[rootNode.right].cost;
}

[numthreads(256, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint index = dispatchThreadID.x;
	uint len = bvhUB.numTriangles;
	if (index >= len)
		return;

	// Handle leaf first
	// Leaf's cost is just its bounding volumn's cost
	uint leafIndex = bvhUB.numTriangles - 1 + index;
	Bound bound = radixTreeNodes[leafIndex].bound;
	float area = radixTreeNodes[leafIndex].area = getArea(bound.min.x, bound.max.x, bound.min.y,
		bound.max.y, bound.min.z, bound.max.z);
	radixTreeNodes[leafIndex].cost = Ct * area;

	int currentIndex = radixTreeNodes[leafIndex].parent;
	int res;
	InterlockedAdd(nodeCounter[currentIndex], 1, res);

	// Go up and handle internal nodes
	while (1)
	{
		if (res == 0)
			break;

		treeletOptimize(currentIndex);

		// If current is root, return
		if (currentIndex == 0)
			break;

		currentIndex = radixTreeNodes[currentIndex].parent;
		InterlockedAdd(nodeCounter[currentIndex], 1, res);
	}
}