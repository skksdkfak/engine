#include "globals.hlsl"
#include "PathTracing.hlsl"

#define CODE_OFFSET (1<<21)
#define CODE_LENGTH (21)

StructuredBuffer<Bound>		bBoxes							: register(t0);
StructuredBuffer<Bound>		intermediate_bounds				: register(t1);
RWStructuredBuffer<uint>	morton_code_lower_bits			: register(u0);
RWStructuredBuffer<uint>	morton_code_heigh_bits			: register(u1);
RWStructuredBuffer<uint>	primitive_indices_lower_bits	: register(u2);
RWStructuredBuffer<uint>	primitive_indices_heigh_bits	: register(u3);

cbuffer CustomUB: register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint thxId = dispatchThreadID.x;
	if (thxId >= bvhUB.numTriangles)
		return;
	
	Bound global_bound = intermediate_bounds[0];
	Bound bound = bBoxes[thxId];
	float3 boundCentroid = (bound.max + bound.min) * 0.5f;

	// get the first 21 bits of each coordinate
	uint morton_code_xyz[3] =
	{
		(uint)(((boundCentroid.x - global_bound.min.x) / (global_bound.max.x - global_bound.min.x)) * CODE_OFFSET),
		(uint)(((boundCentroid.y - global_bound.min.y) / (global_bound.max.y - global_bound.min.y)) * CODE_OFFSET),
		(uint)(((boundCentroid.z - global_bound.min.z) / (global_bound.max.z - global_bound.min.z)) * CODE_OFFSET)
	};

	uint morton_code_low_high_bits[2] = { 0x00, 0x00 };

	// combine into 63 bits morton code
	for (uint i = 0; i < 3; i++)
		for (uint j = 0; j < CODE_LENGTH; j++)
		{
			int currentBitPos = (CODE_LENGTH - j) * 3 - (i + 1);
			morton_code_low_high_bits[currentBitPos / 32] |= ((morton_code_xyz[i] >> (CODE_LENGTH - 1 - j)) & 1) << currentBitPos;
		}

	morton_code_lower_bits[thxId] = morton_code_low_high_bits[0] & 0x00FFFFFFFF;
	morton_code_heigh_bits[thxId] = morton_code_low_high_bits[1] & 0x00FFFFFFFF;

	primitive_indices_lower_bits[thxId] = thxId;
	primitive_indices_heigh_bits[thxId] = 0x00;
}