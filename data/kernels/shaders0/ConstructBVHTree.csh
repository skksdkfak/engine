#include "globals.hlsl"
#include "PathTracing.hlsl"

StructuredBuffer<Bound>			bounds					: register(t0);
StructuredBuffer<uint>			sorted_primitive_indices: register(t1);
RWStructuredBuffer<TreeNode>	radixTreeNodes			: register(u0);
RWStructuredBuffer<int>			nodeCounter				: register(u1);
//RWStructuredBuffer<Bound>		bBoxes					: register(CUSTOM1_UAV_BP);
//RWStructuredBuffer<Sphere>	spheres					: register(CUSTOM1_UAV_BP);

cbuffer CustomUB : register(CUSTOM0_UB_BP)
{
	struct
	{
		uint numTriangles;
	}bvhUB;
};

[numthreads(256, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint index = dispatchThreadID.x;
	uint len = bvhUB.numTriangles;
	uint leavesOffset = bvhUB.numTriangles - 1;
	if (index >= len)
		return;
	
	// Handle leaf first
	uint primitive_index = sorted_primitive_indices[index];
	radixTreeNodes[leavesOffset + index].bound = bounds[primitive_index];
	radixTreeNodes[leavesOffset + index].triangleID = primitive_index;

	int currentIndex = radixTreeNodes[leavesOffset + index].parent;
	TreeNode current = radixTreeNodes[currentIndex];
	int res;
	InterlockedAdd(nodeCounter[currentIndex], 1, res);

	// Go up and handle internal nodes
	while (1)
	{
		if (res == 0)
			break;

		merge_bounds(radixTreeNodes[current.left].bound,
			radixTreeNodes[current.right].bound,
			radixTreeNodes[currentIndex].bound);

		// If current is root, return
		if (currentIndex == 0)
			break;

		currentIndex = current.parent;
		current = radixTreeNodes[currentIndex];
		InterlockedAdd(nodeCounter[currentIndex], 1, res);
	}
}