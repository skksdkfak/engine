#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inBinormal;
layout (location = 3) in vec3 inTangent;
layout (location = 4) in vec2 inUV;

//layout(std140, binding = 0) uniform buf
//{
//	vec4 color;
//} ubuf;

layout (std140, push_constant) uniform PushConsts 
{
	vec4 color;
} pushConsts;

layout (set = 0, binding = 1) uniform samplerBuffer myUniformTexelBuffer;
layout (set = 0, binding = 2) uniform sampler2D samplerColor;
//layout (set = 0, binding = 1, rgba32f) readonly uniform imageBuffer myStorageTexelBuffer;

layout(location = 0) out vec4 oColor;

void main()
{
	oColor = texture(samplerColor, inUV);
	//oColor = pushConsts.color;
	//vec4 meshColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//oColor = vec4(meshColor.rgb * (max(0.0f, dot(vec3(0.556433320f, 0.489277601f, 0.671557486f), normalize(inNormal)))), 1.0f);
	//oColor = texelFetch(myUniformTexelBuffer, 0) * pushConsts.color;
	//oColor = imageLoad(myStorageTexelBuffer, 1);
}