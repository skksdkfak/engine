#include "globals.hlsl"

cbuffer cbPerCubeRender : register(b3)
{
	matrix g_mViewProjPSM[6];
	vector g_vLightPos;
};

struct VS_OUTPUT_GS_INPUT
{
	float4 vPosition    : SV_POSITION;
	float4 vWorldPos	: POSITION;
	float3 vNormal		: NORMAL;
	float3 vBinormal	: BINORMAL;
	float3 vTangent		: TANGENT;
	float2 texCoord		: TEXCOORD0;
	uint RTIndex : RTARRAYINDEX;
};

struct PS_INPUT_INSTCUBEMAP
{
	float4 vPosition        : SV_POSITION;
	float4 vWorldPos		: POSITION;
	float3 vNormal			: NORMAL;
	float3 vBinormal		: BINORMAL;
	float3 vTangent			: TANGENT;
	float2 texCoord			: TEXCOORD0;
	uint RTIndex : SV_RenderTargetArrayIndex;
};

[maxvertexcount(3)]
void main(triangle VS_OUTPUT_GS_INPUT input[3], inout TriangleStream<PS_INPUT_INSTCUBEMAP> CubeMapStream)
{
	PS_INPUT_INSTCUBEMAP output;
	for (int v = 0; v < 3; v++)
	{
		output.vPosition = mul(input[v].vWorldPos, g_mViewProjPSM[input[v].RTIndex]);
		output.vWorldPos = input[v].vWorldPos;
		output.vNormal = input[v].vNormal;
		output.vBinormal = input[v].vBinormal;
		output.vTangent = input[v].vTangent;
		output.texCoord = input[v].texCoord;
		output.RTIndex = input[v].RTIndex;
		CubeMapStream.Append(output);
	}
}