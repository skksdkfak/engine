#include "globals.hlsl"

#define WG_COUNT 64
#define WG_SIZE 256
#define BLOCK_SIZE 1024  // (4 * WG_SIZE)
#define BITS_PER_PASS 4
#define RADICES 16       // (1 << BITS_PER_PASS)
#define RADICES_MASK 0xf // (RADICES - 1)
#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x

cbuffer SortCB : register(CUSTOM0_UB_BP)
{
	uint n;
	uint shift;
	bool descending;
	bool is_signed;
	bool key_index;
};

RWStructuredBuffer<uint> histogram : register(u0);
RWStructuredBuffer<uint> keyIn : register(u1);
RWStructuredBuffer<uint> keyOut : register(u2);
RWStructuredBuffer<uint> valueIn : register(u3);
RWStructuredBuffer<uint> valueOut : register(u4);

#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID

groupshared uint local_sort[BLOCK_SIZE];

uint prefix_sum(uint data, inout uint total_sum, uint lIdx)
{
	const uint lc_idx = lIdx + WG_SIZE;
	local_sort[lIdx] = 0;
	local_sort[lc_idx] = data;
	GroupMemoryBarrierWithGroupSync();
	for (uint i = 1; i < WG_SIZE; i <<= 1)
	{
		uint tmp = local_sort[lc_idx - i];
		GroupMemoryBarrierWithGroupSync();
		local_sort[lc_idx] += tmp;
		GroupMemoryBarrierWithGroupSync();
	}
	total_sum = local_sort[WG_SIZE * 2 - 1];
	return local_sort[lc_idx - 1];
}

groupshared uint seed;

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	uint gIdx = GET_GLOBAL_IDX;
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	seed = 0;
	GroupMemoryBarrierWithGroupSync();
	for (int i = 0; i < RADICES; i++)
	{
		uint val = 0;
		uint idx = i * WG_COUNT + lIdx;
		if (lIdx < WG_COUNT)
			val = histogram[idx];
		uint total;
		uint res = prefix_sum(val, total, lIdx);
		if (lIdx < WG_COUNT)
			histogram[idx] = res + seed;
		if (lIdx == WG_COUNT - 1)
			seed += res + val;
		GroupMemoryBarrierWithGroupSync();
	}
}